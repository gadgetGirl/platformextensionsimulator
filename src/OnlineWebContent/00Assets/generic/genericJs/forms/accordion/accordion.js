///-------------------------------------------///
/// developer: CB Lombard
///
/// Accordion Form Object
///-------------------------------------------///
(function() {
    //Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.accGroupItemWrapper', handler: 'fnb.hyperion.forms.accordion.handleOpenClose(event);'}];
        //Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Accordion Parent function
	///-------------------------------------------///
	function accordion() {

	};
	///-------------------------------------------///
	///          Accordion Methods				  ///
	///-------------------------------------------///
	accordion.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
        //set Type
        closetype: '',
        //setCurrentGroup
        currentGroup: '',
		//Init FNB Banking button
    	init: function () {
    		console.log('Forms Accordion init');
            //Bind Events
            bindEvents();
        }, 
        // handle Accorion click event
        handleOpenClose : function(event){

         //Get target groupWrapper element 
         element = fnb.hyperion.$(event.currentTarget);
         elementOpenStatus = element.attr('data-state');

         //get parrent (accordianContainer) of event target
         var parentContainer =  element.parent();
             this.closetype = parentContainer.attr('data-closetype'); 
             this.currentGroup = parentContainer;

         //  if closeType equals closeAll find all open accordions and close it
         if(this.closetype == 'closeAll' && elementOpenStatus != 'open'){ this.closeAll();}
         //open close elment selected.
         this.toggleOpenClose(element);

        },
        //toggel open close of of groupWrapper element 
        toggleOpenClose: function (element) {
 
           	//Get button settings
        	var groupContentWrapperEl = element.find('! .accGroupItemContentWrapper'),
                           accArrowEl = element.find('! .accArrow'),
                            arrowIsUp = accArrowEl.hasClass('accArrow-up'),
                          arrowIsDown = accArrowEl.hasClass('accArrow-down'),
        	       hasShowClassResult = groupContentWrapperEl.hasClass('accShow');

        	//Accordion is open -> close it
            if(hasShowClassResult){
                //set open status
                element.attr('data-state', 'closed');  
                groupContentWrapperEl.removeClass('accShow');
                if(!arrowIsDown){
                    accArrowEl.addClass('accArrow-down');
                }
                if(arrowIsUp){
                    accArrowEl.removeClass('accArrow-up');
                }

            }else{
                //set open status
                element.attr('data-state', 'open');  
                groupContentWrapperEl.addClass('accShow');
                if(arrowIsDown){
                    accArrowEl.removeClass('accArrow-down');
                }
                if(!arrowIsUp){
                    accArrowEl.addClass('accArrow-up');
                }
            }
        },
        //find all open accordion items and close it.s
        closeAll : function(){
            _this = fnb.hyperion.forms.accordion;
            var elements =  _this.currentGroup.find('* .accGroupItemWrapper[data-state="open"]');
            if(elements.length() > 0){
                elements.each(function(element, index){
                    _this.toggleOpenClose(element);
                });
            }
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.accordion = {};
        }
	};
	//Namespace accordion
	fnb.namespace('forms.accordion', accordion, true);

})();