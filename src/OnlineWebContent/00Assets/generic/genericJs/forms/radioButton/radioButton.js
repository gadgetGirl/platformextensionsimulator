///-------------------------------------------///
/// developer: Donovan
///
/// Radio Button Object
///-------------------------------------------///
(function() {
	//Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.radioGroupWrapper .radioButton', handler: 'fnb.hyperion.forms.radioButton.select(event);'},
    	              {type: 'frame', listener: document, events:'click', selector: '* .checkRadioButtons  .radioGroupWrapper .radioButton', handler: 'fnb.hyperion.forms.radioButton.selectCheckRadio(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Radio Button Parent function
	///-------------------------------------------///
	function radioButton() {

	};
	///-------------------------------------------///
	/// Radio Button Methods
	///-------------------------------------------///
	radioButton.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Checkbox
    	init: function () {
    		console.log('Forms RadioButton init');
    		bindEvents();
        }, 
        selectCheckRadio : function(event){
            //alert("Working!!!");
		//Get parent function
		var _this = this;
		//Get target Element.
        var	target = fnb.hyperion.$(event.currentTarget),
        	groupContainer = target.parent().parent().parent().parent(),
        	tragetGroupContainer =target.parent(),
        	defaultSelect = target.attr('data-defaultselected');
        	
	        if(defaultSelect != undefined && defaultSelect != '' && defaultSelect != false && defaultSelect != 'false'){
	        	
	        }else{
	        	//get all default buttons
	        	defaultButtons = groupContainer.find('*[data-defaultselected="true"]');
	        	//get total number of defaultSelected buttons that is NOT SELCTE.
	        	totaldefaultNotSelected = groupContainer.find('*[data-defaultselected="true"][data-state="unChecked"]');
	        	totaldefaultNotSelectedLen = totaldefaultNotSelected.length(); 
	        	
		        	if(totaldefaultNotSelectedLen > 1 ){
		            	//Uncheck all radio buttons
		                groupContainer.find('* .radioButton').attr('data-state', 'unChecked');
		            	//Uncheck all radio buttons
		                groupContainer.find('=input').attr('checked','');
		                defaultButtons.each(function(element){
		        			//Deselect element
		                	_this.setSelected(element);
		    			});
		                //unSelect all other radio buttons in tragetgroup.
		                tragetGroupContainer.find('* .radioButton').attr('data-state', 'unChecked');
		                tragetGroupContainer.find('=input').attr('checked','');
		                //set target as selected 
		                _this.setSelected(target);
		        	}
	        }
        },
        //Check current state of checkbox
        select: function (event) {
        	//Get parent function
        	var _this = this;
        	//Get current checkbox
    		var target = fnb.hyperion.$(event.currentTarget);
        	//Get current checkbox parent
    		var targetParent = target.parent();
        	//Get disabled state
    		var idDisabled = (targetParent.attr('data-disabled')) ? (targetParent.attr('data-disabled')=="true") ? true : false : false;
    		//Dont run if disabled
    		if(!idDisabled){
        		//Get checkbox state
        		target.parent().children().each(function(element){
        			//Deselect element
        			_this.setUnSelected(element);
    			});
        		//Select current item
        		_this.setSelected(target);
    		}
    		
        },
        //Set the state of target checkbox to checked
        setSelected: function (target) {
        	//Change data attribute of checkbox
        	target.attr('data-state', 'checked');
        	//Change form element value
        	target.find('=input').attr('checked',true);
        },
        //Set the state of target checkbox to unchecked
        setUnSelected: function (target) {
        	//Change data attribute of checkbox
        	target.attr('data-state', 'unChecked');
        	//Change form element value
        	target.find('=input').attr('checked','');
        },
        //Set the state of target checkbox to disabled
        setDisabled: function (target) {
        	//Change data attribute of checkbox
        	target.attr('data-state', 'disabled');
        },
        //Get object current value
        getValue: function (element) {
        	//return checkbox state
        	return element.attr('data-state');
        },
        //Set object current value
        setValue: function (element,value) {
        	//Change form element value
        	switch(value) {
				case 'selected':
					this.setSelected(element);
				break;
				case 'unSelected':
					this.setUnSelected(element);
				break;
				default:
				
			};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.radioButton = {};
        }
	};

	//Namespace forms.radioButton
	fnb.namespace('forms.radioButton', radioButton, true);

})();