///-------------------------------------------///
/// developer: Donovan
///
/// Dropdown Object
///-------------------------------------------///
(function() {
	//Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.dropdownSelected', handler: 'fnb.hyperion.forms.dropDown.checkState(event);', focusOutEvent: 'fnb.hyperion.forms.dropDown.close(event);'},
    	              {type: 'frame', listener: document, events:'click', selector: '.dropdownItem', handler: 'fnb.hyperion.forms.dropDown.select(event);'},
    	              {type: 'frame', listener: document, events:'click', selector: '.dropdownCarat', handler: 'fnb.hyperion.forms.dropDown.checkState(event);'},
    	              {type: 'frame', listener: document, events:'change', selector: '.dropdownItemSelectWrapper', handler: 'fnb.hyperion.forms.dropDown.select(event);'},
    	              {type: 'frame', listener: document, events:'keyup', selector: '.dropdownInput', handler: 'fnb.hyperion.forms.dropDown.filter(event);'},
    	              {type: 'frame', listener: document, events:'focus', selector: '.dropdownInput', handler: 'fnb.hyperion.forms.dropDown.dropdownInputFocusIn(event);'},
    	              {type: 'frame', listener: document, events:'blur', selector: '.dropdownInput', handler: 'fnb.hyperion.forms.dropDown.dropdownInputFocusOut(event);'}
    	              ];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// dropDown Parent function
	///-------------------------------------------///
	function dropDown() {

	};
	///-------------------------------------------///
	/// dropDown Methods
	///-------------------------------------------///
	dropDown.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Dropdown Target parent
		dropdownSelect: '',
		//Dropdown Target parent
		dropdownBottomOffset: 78,
		//Dropdown Target parent
		dropdownParent: '',
		//Dropdown Target parent
		dropdownTarget: '',
		//Dropdown Caret
		dropdownCaret: '',
		//Dropdown datarows attribute
		dataRows: '',
		//Dropdown selected dropdown element
		dropdownSelectedElement: '',
		//dropdownTarget Parent
		dropdownOptionsParent: '',
		//dropdownOptionsParent Parent
		dropdownOptionsParentWrapper: '',
		//dropDown selected value
		dropdownSelectedValue: '',
		//Dropdown options list
		dropdownOptions: {},
		//Dropdown filtered flag
		filtered: false,
		//Var for dropdown is active
		active: false,
		//Init FNB Dropdown
    	init: function () {
    		console.log('Forms Dropdown init');
    		//Bind Dropdown events
    		bindEvents();
    		//Attach event to page load complete event sequence
    		fnb.hyperion.controller.attachPageEvent('fnb.hyperion.forms.dropDown.setDropdowns()','');
        },
        //Open dropdown
        checkState: function (event) {
        	//Get the target that triggered the event
        	var tempTarget = fnb.hyperion.$(event.currentTarget);
        	//Get the correct dropdown target
        	this.dropdownTarget = (tempTarget) ? (tempTarget.hasClass('dropdownCarat')) ? tempTarget.parent().find('.dropdownSelected') : tempTarget : fnb.hyperion.$(event.target);
        	//Get the state of the current selected dropdown
        	var dropdownState = (this.dropdownTarget.hasClass('closed')) ? 'closed' : 'open';
        	//Get the disabled state
        	var dropdownDisabledState = (this.dropdownTarget.attr('data-disabled')) ? (this.dropdownTarget.attr('data-disabled') == "true") ? true : false : false;
        	//Get the amount of rows to display when the dropdown opens
        	this.dataRows = parseInt(this.dropdownTarget.attr('data-rows'));
        	//Test for number else make 0
        	this.dataRows = isNaN(this.dataRows) ? 0 : this.dataRows;
        	//Check if dropdown is disabled
        	if(!dropdownDisabledState){
            	//Switch Dropdown States to open or close the dropdown
            	switch(dropdownState) {
    				case 'open':
    					this.close(event);
    				break;
    				case 'closed':
    					this.open(event);
    				break;
    				default:
    				
    			};
        	}

        },
        //Open dropdown
        open: function () {
        	//Check if another dropdown is open
        	if(this.active) this.close();
        	//Set dropdoen has active element
        	this.active = true;
        	//Main dropdown wrapper seletion
        	this.dropdownParent = this.dropdownTarget.parent();
        	//Select dropdownCaret
        	this.dropdownCaret = this.dropdownParent.find('.dropdownCarat');
    		//Select ul element to target
    		this.dropdownOptionsParent = this.dropdownParent.find('=ul');
    		//Select dropdown select element
    		this.dropdownSelect = this.dropdownParent.find('=select');
    		//Select dropdown ul parent
    		this.dropdownOptionsParentWrapper = this.dropdownOptionsParent.parent();
    		//Set Dropdown content height
    		this.setContentHeight();
    		//Remove transparency from ul parent and add border and shadow
    		this.dropdownOptionsParentWrapper.addClass('open');
    		this.dropdownOptionsParentWrapper.attr('data-transparent', 'false');
    		//Change dropdown state to open
    		this.dropdownTarget.removeClass('closed');
    		this.dropdownTarget.addClass('open');
    		//Change caret state
    		this.dropdownCaret.removeClass('closed');
    		this.dropdownCaret.addClass('open');
    		
        },
        //Close dropdown
        close: function (target) {
        	//Declare var for dropdown input
        	var isDropdownInput = false;
        	//Test for valid event
        	if(target){
        		//Get target dropdown item
            	var target = target.currentTarget;
            	//Test to see if the dropdown input was selected
            	isDropdownInput = (fnb.hyperion.$(target).hasClass('dropdownInput')) ? true : false;
        	}
        	//Test for dropdownOptionsParentWrapper declaration
        	if(this.active==true&&this.dropdownOptionsParentWrapper!=''&&!isDropdownInput){
        		//Reset dropdown
        		this.resetDropdown();
        	}
        },
        //Select dropdown value
        select: function (target) {
        	//Target li
        	this.dropdownSelectedElement = fnb.hyperion.$(target.currentTarget);
        	//Select dropdown parent
        	var dropdownParent = this.dropdownSelectedElement.parent().parent();
        	//Switch touch and dropdown types
        	switch(fnb.hyperion.controller.isMobile) {
				case true:
					var htmlString = '';
					//Switch Dropdown types
		        	switch(dropdownParent.attr('data-type')) {
						case 'singleTier':
							//Create html wrapper for single tier dropdown options
							htmlString = this.dropdownSelectedElement.prop('options')[this.dropdownSelectedElement.prop('selectedIndex')].html();
						break;
						case 'threeTier':
							//Create html wrapper for three tier dropdown options
							htmlString = '<span data-role="dropdownItemLabel">'+this.dropdownSelectedElement.prop('options')[this.dropdownSelectedElement.prop('selectedIndex')].parent().getAttribute('label')+'</span><span data-role="dropdownItemLabel">'+this.dropdownSelectedElement.prop('options')[this.dropdownSelectedElement.prop('selectedIndex')].html()+'</span>';
						break;
						default:
						
					};
					dropdownParent.parent().find('=span').get(1).html(htmlString);
				break;
				default:           	
					//Check if the dropdown search input was selected
		        	if(this.dropdownSelectedElement.attr('data-type')) return;
					//Deselct other options
	        		this.dropdownSelectedElement.parent().find('*li').attr('data-selected', 'false');
				    this.dropdownSelectedElement.parent().find('*li').show();
					//Add Selected attr
	        		this.dropdownSelectedElement.attr('data-selected', 'true');
					//Get the value of the selected item
	        		var selectedValue = this.dropdownSelectedElement.attr('data-value');
		        	//Set the dropdown value
		        	this.dropdownSelect.val(selectedValue);
		        	//Get the content of the selected item
		        	var selectedContent = this.dropdownSelectedElement.html();
		        	//Set the content of the display label
		        	this.dropdownTarget.html(selectedContent);
		        	//Test for three tier dropdown
					if(dropdownParent.parent().attr('data-type')=='threeTier'){
						//Get amount
						fnb.hyperion.forms.dropDown.checkForBalance(dropdownParent.parent());
					}
					//Close dropdown
					fnb.hyperion.forms.dropDown.close(target);
			};
        },
        //Set Dropdown Content height
        setContentHeight: function () {
    		//Select list of all options that are going to be displayed
        	var visibleDropdownOptions = this.dropdownParent.find('*li[data-visible="true"]');
        	//Test for number else make 0
        	var dropdownOptionsLength = isNaN(visibleDropdownOptions.length()) ? 1 : visibleDropdownOptions.length();
        	//Var for dropdown height
        	var dropdownHeight = 0;
        	//Get dropdown search
        	var firstDropdownHeight = visibleDropdownOptions.first().outerHeight();
    		//Add scrollable if the options are more that the rows to be displayed & is not touch device
        	if(dropdownOptionsLength>this.dataRows&&this.dataRows!=0){
        		//Add Scrollable data attribute to selected ul
        		this.dropdownOptionsParent.attr('data-scrollable', 'scrollable');
        		//Calculate scrollable height
        		dropdownHeight = (visibleDropdownOptions.last().outerHeight()*(this.dataRows-1))+firstDropdownHeight;
        		//Apply new height to ul parent
        		this.dropdownOptionsParentWrapper.css('height',dropdownHeight+'px');
        	}else if(this.dataRows==0||dropdownOptionsLength<=this.dataRows){
        		//Calculate scrollable height
        		dropdownHeight = (visibleDropdownOptions.last().outerHeight()*(dropdownOptionsLength-1))+firstDropdownHeight;
        		//Apply new height to ul parent
        		this.dropdownOptionsParentWrapper.css('height',dropdownHeight+'px');
        	}
        	//Get dropdown position
        	var dropdownPosition = this.dropdownParent.position();
        	//Get document height
    		var documentHeight = fnb.hyperion.getDocumentHeight();
        	//Calculate overflow position
    		var totalPosition = dropdownPosition.objectPosY+this.dropdownBottomOffset+dropdownHeight;
        	//Add reverese class if dropdown overflows
    		if(totalPosition>documentHeight) this.dropdownOptionsParentWrapper.addClass('dropdownReverse');
        },
        //Set Dropdowns
        setDropdowns: function () {
			//Select all dropdowns
			var pageDropdowns = fnb.hyperion.$("=select");
			//Loop dropdowns and set data attribute for devices
			if(pageDropdowns.length()>0){
				//Test if only one dropdown has been found
				if(pageDropdowns.type()!='SELECT'){
					//Select all the dropdowns on the page
					pageDropdowns.each(function(element){
						//Get element to change
						fnb.hyperion.forms.dropDown.checkAttributes(element);
					});
				}else{
					//Get element to change
					fnb.hyperion.forms.dropDown.checkAttributes(pageDropdowns);
				}
				
			};
        },
        //Check element attributes
        checkAttributes: function (element) {
        	//Get element to change
			var dropdownParent = element.parent().parent();
			//Get parent type
			var dropdownParentType = dropdownParent.attr('data-type');
			//Get if amounts has been set
			var dropdownParentSet = dropdownParent.attr('data-set');
			//Test for three tier dropdown
			if(dropdownParentType=='threeTier'&&dropdownParentSet!="true"){
				//Add data set attribute
				dropdownParent.attr('data-set','true');
				//Get amount
				fnb.hyperion.forms.dropDown.checkForBalance(dropdownParent);
			}
        	/*If not a touch device*/
			if(!fnb.hyperion.controller.isMobile){
				//Test element for attribute
				if(dropdownParent.attr("data-event") != null){
					//Set attribute
					if(dropdownParent.attr('data-event')!='click') dropdownParent.attr('data-event', 'click');
				};
			};
        },
        //Load balance into dropdown if needed
        checkForBalance: function (dropdownParent) {
        	//Get amounts wrapper
			var amountsWrapper = dropdownParent.find('.dropdownSelected').find('*[data-type="amountsWrapper"]');
			//Test for amounts wrapper
			if(amountsWrapper.length()>0){
				//Get data url
				var amountsUrl = amountsWrapper.attr('data-url');
				//Test for url and load amounts
				if(amountsUrl){
					//Ajax load amount
					fnb.hyperion.forms.dropDown.loadBalance(amountsWrapper, amountsUrl);
				};
			};

        },
        //Load balance into dropdown if needed
        loadBalance: function (target,url) {
        	//Setup load object
        	var loadObject = {url: url, target: target, async: true};
        	//Request ajax load from controller
        	fnb.hyperion.controller.raiseEvent('asyncLoadContent', loadObject);
        },
        //Set Dropdowns
        resetDropdown: function () {
    		/*Apply new height to ul parent*/
    		this.dropdownOptionsParentWrapper.css('height','0px');
    		//add transparency from ul parent and remove border and drop shadow
    		this.dropdownOptionsParentWrapper.attr('data-transparent', 'true');
    		this.dropdownOptionsParentWrapper.removeClass('open');        		
    		//Change dropdown state to open
    		this.dropdownTarget.removeClass('open');
    		this.dropdownTarget.addClass('closed');
    		//Change caret state
    		this.dropdownCaret.removeClass('open');
    		this.dropdownCaret.addClass('closed');
    		//Reset dropdown if filtered
    		if(this.filtered) this.resetFilter();
        	//Set dropdown has active element
    		this.active = false;
        },
        //Filter dropdown options
        filter: function (event) {
        	//Select all dropdown options
        	var dropdownOptions = this.dropdownParent.find('*li');
        	//Get target
        	var target = fnb.hyperion.$(event.boundTarget);
           	//Get value to filter
            var searchValue = target.val().toLowerCase();
        	//Loop dropdown options
            for (var i = 1; i < dropdownOptions.length(); i++) {
            	//Select element
                var element = dropdownOptions[i];
                var contentContainers = element;
                //Check if the element has children
                if(element.children().length() > 0) contentContainers = element.find('=span'); 
            
                var filterParent = false;
            	
               if(contentContainers.length() > 0){
            	 //Loop through content and try match
            	   for (var x = 0; x < contentContainers.length(); x++) {
            		   var content = contentContainers[x].html().toLowerCase();
            		   if(content.indexOf(searchValue)>-1||searchValue=='') filterParent = true;
            	   }
            	   
               }else{
            	   var content = contentContainers.html().toLowerCase();
            	   if(content.indexOf(searchValue)>-1 || searchValue=='') filterParent = true;
               }
				//Set item wrapper visibility true or false
                if (filterParent) {
                	element.show();
                } else {
                	element.hide();
                }
            }
            //Set filtered flag
            this.filtered = true;
            //Reset content height
            this.setContentHeight();
        },
        //Reset dropdown filter
        resetFilter: function () {
        	//Select all dropdown options
        	var dropdownOptions = this.dropdownParent.find('*li');
        	//Reset dropdown input
        	var dropdownInput = this.dropdownParent.find('.dropdownInput');
        	//Get dropdown input mask value
        	var maskValue = dropdownInput.attr('data-placeholderValue');
        	//Reset dropdown input mask value
        	dropdownInput.val(maskValue);
        	//Reset dropdown input placeholder attr
        	dropdownInput.attr('data-placeholderis','true');
        	//Loop dropdown options
            for (var i = 1; i < dropdownOptions.length; i++) {
            	//Select element
                var element = dropdownOptions[i];
                //Add visible attribute
                element.show();
            }
            //Set filtered flag
            this.filtered = false;
            //Reset content height
            this.setContentHeight();
        },
        //Method for dropdown input focus in
        dropdownInputFocusIn: function (event) {
        	//Get target
        	var target = fnb.hyperion.$(event.boundTarget);
        	//Get input value
        	var value = target.val();
        	//Get mask value
        	var maskValue = target.attr('data-placeholder');
        	//Test if mask value is = to value
        	if(value==maskValue){
        		//Set data-mask attribute
        		target.attr('data-placeholderIs','false');
        		//Clear input value
        		target.val("");
        	}
        },
        //Method for dropdown input focus out
        dropdownInputFocusOut: function (event) {
        	//get target
        	var target = fnb.hyperion.$(event.boundTarget);
        	//Get input value
        	var value = target.val();
        	//Get mask value
        	var maskValue = target.attr('data-placeholderValue');
        	//Test if mask value is = nothing
        	if(value==''){
        		//Set data-mask attribute
        		target.attr('data-placeholderIs','true');
        		//Set input value
        		target.val(maskValue);
        	}
        	//Reset dropdown
    		this.resetDropdown();
        },
        //Get object current value
        getValue: function (element, handler) {
        	//Get value
        	var value = element.val();
        	//Create dropdown callback
        	return handler(value);
        },
        //Set object current value
        setValue: function (element, value) {
        	element.value = value;
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.dropDown = {};
        }
	};

	//Namespace dropDown
	fnb.namespace('forms.dropDown', dropDown, true);

})();