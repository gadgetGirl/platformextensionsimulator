///-------------------------------------------///
/// developer: Mike Stott
///
/// Text Area Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'keyup', selector: 'textarea', handler: 'return fnb.hyperion.forms.textArea.updateChars(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
    ///-------------------------------------------///
	/// Text Area Parent function
	///-------------------------------------------///
	function textArea() {


	};
	///-------------------------------------------///
	/// Text Area Methods
	///-------------------------------------------///
	textArea.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Textarea
    	init: function () {
    	   //Bind our events to the object
    		bindEvents();
        },
        //Function called to update remaining characters
        updateChars : function(event) {
        	//Store the current textarea element
        	var _element =  fnb.hyperion.$(event.currentTarget);
        	var maxChars = _element.attr('data-charcount');
        	//If the textareas value is greater than the maximum allowed amount...
        	if(_element.prop('value').length > maxChars) {
          		//Don't let the user input any more caracters...
        		 _element.prop('value', _element.prop('value').slice(0,maxChars)); 
        	}else{
        		//Remaining caracters left
        		var charsLeft = maxChars - _element.prop('value').length;
        		//Update the remaining caracters text 
        		_element.parent().find('.characterCount').html(charsLeft + " characters remaining");
        	}
         },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.textArea = {};
        }
	};

	//Namespace textArea
	fnb.namespace('forms.textArea', textArea, true);

})();