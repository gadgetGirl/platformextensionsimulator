///-------------------------------------------///
/// developer: Donovan
///
/// Checkbox Object
///-------------------------------------------///
(function() {
	//Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.checkBox', handler: 'fnb.hyperion.forms.checkBox.checkState(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Checkbox Parent function
	///-------------------------------------------///
	function checkBox() {

	};
	///-------------------------------------------///
	/// Checkbox Methods
	///-------------------------------------------///
	checkBox.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Checkbox
    	init: function () {
    		console.log('Forms Checkbox init');
    		bindEvents();
        }, 
        //Check current state of checkbox
        checkState: function (event) {
        	//Get current checkbox
    		var target = fnb.hyperion.$(event.currentTarget);
    		//Get checkbox state
    		var checkBoxState = target.hasClass('checked');
    		//Get disabled state
    		var idDisabled = (target.attr('data-disabled')) ? (target.attr('data-disabled')=="true") ? true : false : false;
    		//Dont run if disabled
    		if(!idDisabled){
	    		//Switch checkBox States to checked or unchecked
	        	switch(checkBoxState) {
					case true:
						this.setUnChecked(target);
					break;
					default:
						this.setChecked(target);
					break;
					
				};
    		}
        },
        //Set the state of target checkbox to checked
        setChecked: function (target) {
        	//Change data attribute of checkbox
        	target.addClass('checked');
        	//Change form element value
        	target.children(0).attr('checked', "checked");
        },
        //Set the state of target checkbox to unchecked
        setUnChecked: function (target) {
        	//Change data attribute of checkbox
        	target.removeClass('checked');
        	//Change form element value
        	target.children(0).attr('checked','');
        },
        //Set the state of target checkbox to disabled
        setDisabled: function (target) {
        	//Change data attribute of checkbox
        	target.attr('data-state', 'disabled');
        },
        //Get object current value
        getValue: function (element) {
        	//return checkbox state
            var getVal = element.hasClass('checked');
        	return getVal;
        },
        //Set object current value
        setValue: function (element,value) {
        	//Change form element value
        	switch(value) {
				case 'checked':
					this.setChecked(element);
				break;
				default:
					this.setChecked(target);
				break;
			};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.checkBox = {};
        }
	};

	//Namespace checkBox
	fnb.namespace('forms.checkBox', checkBox, true);

})();