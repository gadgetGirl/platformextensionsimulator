///-------------------------------------------///
/// developer: Donovan
///
/// Main Forms Object
///-------------------------------------------///
(function() { 
	//Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'onsubmit, submit', selector: '[data-role="form"]', handler: 'return fnb.hyperion.forms.submitForm(event);', preventDefault: true}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
    //Init all form modules
	function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.forms) {
    		if(fnb.hyperion.forms[module].autoInit){
				fnb.hyperion.forms[module].init();
			}
    	}
    };
    //Trim values of whitespaces
    function trim(value)
    {
      return value.replace(/^\s+|\s+$/, '');
    } 
    ///-------------------------------------------///
	/// Main Forms Parent function
	///-------------------------------------------///
	function forms() {

	};
	///-------------------------------------------///
	/// Main Forms Methods
	///-------------------------------------------///
	forms.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Froms
    	init: function () {
    		console.log('Forms init');
    		//Initialize Form modules
    		initModules();
    		//Bind form events
    		bindEvents();
        },
        submitForm: function(event){
        	//Select bound target
        	var form = fnb.hyperion.$(event.currentTarget);
        	//Test for from action
        	if (form.action != '')  fnb.hyperion.forms.validateAndSubmitForm({url: form.prop("action"), dataTarget: '#'+form.prop("action")});
        },
        //Validate the input fields
        validateAndSubmitForm: function(loadObj){
        	//Test for datatarget
        	if(loadObj.dataTarget){
        		//Set valid form flag
            	validForm = true;
            	//Find all form elements with data required and validate
    			fnb.hyperion.$(loadObj.dataTarget).find('*[data-required="true"]').each(function(element) {
    				//Get target val
    				var checked = false;
    				//Get closest form container
    	        	var validationTarget = element.find('.validationTarget');
    	        	//Target value var
    	        	var targetValue = '';
    	        	//Test if validation target was found
    	        	if(validationTarget.length()>0){
    	        		//Test if more than one target is available
        	        	if(validationTarget.length()==1){
        	        		//Set target value
        	        		targetValue = validationTarget.val();
        	        		//Set checked flag
        	        		checked = true;
        	        	}else if(validationTarget.length()>1){
        	        		//Loop target and check selected
        	        		validationTarget.each(function(element){
        	        			if(element.attr('checked')=='true'||element.attr('checked')=='checked'){
        	        				checked = true;
        	        				targetValue = element.val().toString();
        	        			}
        					});
        	        	}
        				//Find error wrapper
        	        	var validationErrorTarget = element.find('.formInlineError');
        	        	//Simple validation...
        				if (targetValue == null || targetValue == '' || targetValue == 0 || checked == false) {
        					//Value failed add error class
        					element.addClass("inputError");
        					//Set valid form flag false
        					validForm = false;
        				}else{
        					if(validationTarget.attr("data-type")=="email"){
        						//Test email address
        						var message = fnb.hyperion.forms.validateEmail(validationTarget);
        						if(message!=""){
        							//Value failed add error class
        	    					element.addClass("inputError");
        							//Set valid form flag
        							validForm = false;
        							//Set email message
        							validationErrorTarget.html(message);
        						}else{
        							//Remove error if visible
                					element.removeClass("inputError");
        						}
        					}else{
            					//Remove error if visible
            					element.removeClass("inputError");
        					}
        				}
    	        	}
    	        	
    			});
    			//If valid from submit
    			if (validForm) {
    				fnb.hyperion.controller.raiseEvent("submit",loadObj);
    			};
        	}
		},
		//submit to clear bean
        clearAndSubmitForm: function(loadObj){
        	
        	fnb.hyperion.$('#command').attr('value', 'DEFAULT');
        	
			fnb.hyperion.controller.raiseEvent("submit",loadObj);

		},
		////Validate email
		validateEmail: function(input){
        	
			//Error var
			var error="";
			//Value without whitespaces
		    var timmedValue = trim(input.val());
		    //Regex for email filter
		    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
		    //Regex for illegal chars
		    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
		    
		    if (!emailFilter.test(timmedValue)) {
		        error = "Please enter a valid email address.";
		    } else if (input.val().match(illegalChars)) {
		        error = "The email address contains illegal characters.";
		    }
		    
		    return error;

		},		
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms = {};
        }
	};

	//Namespace forms
	fnb.namespace('forms', forms, true);

})();