///-------------------------------------------///
/// developer: Donovan
///
/// Main Forms Button Object
///-------------------------------------------///
(function() { 
    //Init all form modules
	function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.forms.button) {
    		if(fnb.hyperion.forms.button[module].autoInit){
				fnb.hyperion.forms.button[module].init();
			}
    	}
    };
    ///-------------------------------------------///
	/// Main Forms Parent function
	///-------------------------------------------///
	function button() {

	};
	///-------------------------------------------///
	/// Main Forms Button Methods
	///-------------------------------------------///
	button.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Forms button
    	init: function () {
    		console.log('Forms Main Button init');
    		initModules();
        },		
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms = {};
        }
	};

	//Namespace forms
	fnb.namespace('forms.button', button, true);

})();