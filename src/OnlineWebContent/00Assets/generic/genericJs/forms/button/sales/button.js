///-------------------------------------------///
/// developer: Donovan
///
/// Web Button Object
///-------------------------------------------///
(function() {
    //Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '[type="button"]', handler: 'fnb.hyperion.forms.button.web.select(event);'},
    	              			{type: 'frame', listener: document, events:'click', selector: '[data-role="submitButton"]', handler: 'fnb.hyperion.forms.button.web.select(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Button Parent function
	///-------------------------------------------///
	function button() {

	};
	///-------------------------------------------///
	/// Button Methods
	///-------------------------------------------///
	button.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Web button
    	init: function () {
    		console.log('Forms Button init');
    		bindEvents();
        }, 
        //Execute button event
        select: function (event) {
        	//Get target button
        	target = fnb.hyperion.$(event.currentTarget);
        	//Get button settings
        	var dataSettingsString = target.attr("data-settings");
        	//Test if data settings exist
        	if(dataSettingsString){
        		//Convert settings string to object
            	var dataSettingsObject = JSON.parse(dataSettingsString);
            	//Wrap target in selector
            	if(dataSettingsObject[0].target!="") dataSettingsObject[0].target = fnb.hyperion.$(dataSettingsObject[0].target);
            	//Get event that needs to be raised
            	var event = dataSettingsObject[0].event;
            	//Raise specified event
            	if(event!='') fnb.hyperion.controller.raiseEvent(event, dataSettingsObject[0]);
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.button.web = {};
        }
	};
	//Namespace button
	fnb.namespace('forms.button.web', button, true);

})();