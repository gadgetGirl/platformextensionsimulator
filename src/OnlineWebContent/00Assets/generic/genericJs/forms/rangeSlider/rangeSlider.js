
///-------------------------------------------///
/// developer: CB Lombard
///
///
/// RangeSlider
///-------------------------------------------///
(function() {
  //Bind event for current object
  function bindEvents() {
    //Add listener for click on login button  data-role="btnLabel"
    var events = [ {type: 'frame', listener: document, events:'mouseup', selector: '#bodyContainer', handler: 'fnb.hyperion.forms.rangeSlider.sliderBtnMouseUp(event,"mouse");'},
                   {type: 'frame', listener: document, events:'touchend', selector: '#bodyContainer', handler: 'fnb.hyperion.forms.rangeSlider.sliderBtnMouseUp(event,"touch");'},
                      {type: 'frame', listener: document, events:'click', selector: '.point', handler: 'fnb.hyperion.forms.rangeSlider.snapToPointer(event);'},
                      {type: 'frame', listener: document, events:'click', selector: '#setSlider', handler: 'fnb.hyperion.forms.rangeSlider.testFunc();'},
                      {type: 'frame', listener: document, events:'mousedown', selector: '[data-role="btnLabel"]', handler: 'fnb.hyperion.forms.rangeSlider.sliderBtnMouseDown(event,"mouse")'},
                      {type: 'frame', listener: document, events:'touchstart', selector: '[data-role="btnLabel"]', handler: 'fnb.hyperion.forms.rangeSlider.sliderBtnMouseDown(event,"touch")'}];

        //Append events to actions module
        fnb.hyperion.actions.addEvents(events);
    };
  ///-------------------------------------------///
  /// RangeSlider Parent function
  ///-------------------------------------------///
  function rangeSlider() {

  };
  ///-------------------------------------------///
  /// Subtabs Methods
  ///-------------------------------------------///
  rangeSlider.prototype = {
    //Var for frame to auto initialize module
    autoInit: true,
    //active sub tab
    activeSliderId: '', 
        // sets the mouse down event as active ... waits for mouse up to to cancel mouse move event
        mousedown: false,
        touchstarted: false, 
        offSet : false,
        rangeSliderGroup: '',
        sliderType: 'banking',  
        browserSize: '',
        body:'',
        screenOffset : '',
        reInt : '',
        maxValue : '1300',
        trusted : '',
        availableCredit : '',
    
      init: function () {
        //Bind subTabs events
        console.log('Forms rangeSlider init');
        bindEvents();

        // setOffSet
        this.browserSize = this.getBrowserSize();
        },
        checkRenit : function(){
          _this = fnb.hyperion.forms.rangeSlider;
          var element = fnb.hyperion.$('#mvnoSettings');
          //check if Dom element was found
          if(element.length() > 0){
            var reInt = element.attr('data-reint');
             // if found and data-ReInt equals to true then destroy or reInt object.
              if(reInt  == 'true' || reInt == true){
                //Clear all Slider Objects
                fnb.hyperion.forms.rangeSlider.destroy();
                //set reInt to False.
                fnb.hyperion.$('#mvnoSettings').attr('data-reint', 'false');
              }
            _this.trusted = element.attr('data-trusted');
            _this.availableCredit = element.attr('data-availableCredit');
          }
        },
        //Remove current object from rangeSlider JS objects
        destroy: function () {
            var _this = fnb.hyperion.forms.rangeSlider;
                fnb.hyperion.forms.rangeSlider.activeSliderId ='';
                fnb.hyperion.forms.rangeSlider.mousedown= false;
                fnb.hyperion.forms.rangeSlider.offSet = false;
                fnb.hyperion.forms.rangeSlider.rangeSliderGroup = {};
                fnb.hyperion.forms.rangeSlider.sliderGroup = {};
                _this.body = fnb.hyperion.$('#bodyContainer');
        },
        //MouseDown Event
        sliderBtnMouseDown: function(event, eventType){
            var _this = fnb.hyperion.forms.rangeSlider;
            _this.checkRenit();
           //check if object has been init
           //Get current sliderBtnSelected
           var target = fnb.hyperion.$(event.currentTarget),
               targetID = target.attr("data-sliderid");

           //if not init it
           
            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
                _this.browserSize = _this.getBrowserSize();
                _this.body = fnb.hyperion.$('#bodyContainer');
                this.browserSize = _this.getBrowserSize();
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                    //  -- update to active
                      _this.activeSliderId = targetID; 
                      _this.browserSize = _this.getBrowserSize();
                      _this.sliderGroup[targetID].rangeSliderWidth = _this.sliderGroup[targetID].rangeSliderContainer.outerWidth();
                   }
            }else{
               _this.browserSize = _this.getBrowserSize();
               _this.sliderGroup[targetID].rangeSliderWidth = _this.sliderGroup[targetID].rangeSliderContainer.outerWidth();
            }


            //then perform mouse down event and add mouse move event.

            if(this.mousedown == false && eventType == 'mouse'){

               this.mousedown = true;
               fnb.hyperion.delegate.on(_this.body.elem, 'mousemove', fnb.hyperion.forms.rangeSlider.bindToMouseMove);

            }else if(this.touchstarted == false && eventType == 'touch' ){

               this.touchstarted = true;
               fnb.hyperion.delegate.on(_this.body.elem, 'touchmove', fnb.hyperion.forms.rangeSlider.BlockMove);
               fnb.hyperion.delegate.on(_this.body.elem, 'touchmove', fnb.hyperion.forms.rangeSlider.bindToTouchMove);
               
            }
        },
        //init new slider and add to object Group
        addToSliderGroup:function(targetID){
            var _this  = fnb.hyperion.forms.rangeSlider,
                groupId = parseInt(targetID)
                // select everyting from current slected slider and build object
                _this.sliderGroup[targetID] ={};
                _this.sliderGroup[targetID].rangeSliderContainer = fnb.hyperion.$('#rangeSliderContainer'+targetID);
                _this.sliderGroup[targetID].rangeSliderWidth = _this.sliderGroup[targetID].rangeSliderContainer.outerWidth();
                _this.sliderGroup[targetID].snapPointsElements = _this.sliderGroup[targetID].rangeSliderContainer.find(".point");
                _this.sliderGroup[targetID].snapPointsArr = [];
         
                // get snappoint lenght
                snapPointsElementsLen = _this.sliderGroup[targetID].snapPointsElements.length() - 1;
                
                //build array of all snap points
                 _this.sliderGroup[targetID].snapPointsElements.each(function(element, index){
                    console.log(element);
                    var NewSnapPointObj ={
                            "percentage": parseInt(element.attr("data-percentage")) , 
                            "value":  element.attr("data-value"),
                            "pointLabel": element.find('! .pointLabel').html(),
                            "itemCode" : element.attr("data-itemCode"),
                            "itemSkuCode": element.attr("data-itemskucode"),
                            "dataBtnLabel": element.attr("data-btnlabel"),
                            "rate": element.attr("data-rate")};
                        // add object to array
                  if(element.attr("data-snappointselceted") == "true") { 
                      _this.sliderGroup[targetID].activeSnapPoint =  index;
                      _this.sliderGroup[targetID].maxSnappoint =  (_this.trusted == "false" || _this.trusted == false ) ? index : snapPointsElementsLen;

                   }
                   _this.sliderGroup[targetID].snapPointsArr.push(NewSnapPointObj);
                 });   

                 console.log(_this.sliderGroup[targetID].snapPointsArr);
                _this.sliderGroup[targetID].rangeSliderBtn = _this.sliderGroup[targetID].rangeSliderContainer.find('*[data-role="sliderBtn"]');
                _this.sliderGroup[targetID].rangeSliderBtnLabel = _this.sliderGroup[targetID].rangeSliderContainer.find('*[data-role="btnLabel"]'); 
                _this.sliderGroup[targetID].rangeSlideProgress = _this.sliderGroup[targetID].rangeSliderContainer.find('*[data-role="sliderProgress"]');
                _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor=_this.sliderGroup[targetID].rangeSlideProgress.attr('style');
                _this.sliderGroup[targetID].progressPercentage= "";

                _this.activeSliderId = targetID;

        },
        //MouseUp Event
        sliderBtnMouseUp:function(event){
            var _this  = fnb.hyperion.forms.rangeSlider;

            if(_this.mousedown){

                _this.mousedown = false; 

                var targetID = _this.activeSliderId 
                
                fnb.hyperion.delegate.off(this.body.elem, 'mousemove', fnb.hyperion.forms.rangeSlider.bindToMouseMove);

                var i=0, len = _this.sliderGroup[targetID].snapPointsArr.length, closestPointIndex =0, pointVal = 0, newPointVal =0;
                  
               var  curentActiveSlider = fnb.hyperion.$('#rangeSliderContainer'+targetID);
                //var element = document.getElementById('some-id');
                var position = curentActiveSlider.elem.getBoundingClientRect();
                var screenOffSet = position.left;
               //get the offset of the mouse cursor on mouse move
               //have to use clientX - pageX not working in IE8
                var xOffSet  = xOffSet  = event.clientX - screenOffSet,
                //current percentage of slider
                percentage = Math.round(xOffSet /  _this.sliderGroup[targetID].rangeSliderWidth * 100),
                sliderPercentage = (percentage > 100)? 100 :  percentage;

                // Loop through all snapPoint and find snapPoint closestPointIndex.
                  for(i ; i < len; i++){
                    newPointVal = Math.abs(sliderPercentage - _this.sliderGroup[targetID].snapPointsArr[i].percentage);
                      if(newPointVal < pointVal || pointVal == 0 ){
                          closestPointIndex = i;
                          pointVal = newPointVal;
                      }
                  }

                //get percentage value from snapPointsArr Object   
                sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex].percentage;
                //get Label percentage value from snapPointsArr Object   
               var snapPointLabel = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex].dataBtnLabel; 

                  _this.setSnapValueToYouPay(targetID,closestPointIndex);
                  // change slider label to snapPoint Label 
                 _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(snapPointLabel);
                  
                 sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
                 //change btn label position if greater that 50%;
                 _this.doChangeLabelAlign(targetID, sliderPercentage );
                _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
                _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

                // calculate new total and update html
                _this.calculatePackageTotal(targetID, closestPointIndex);
            }
            if(this.touchstarted){
//
                _this.touchstarted = false; 
                var targetID = _this.activeSliderId 
                var target = fnb.hyperion.$(event.currentTarget).find('! #sliderBtn'+targetID);
                  // _this.debugDiv.html("<br/> <p> Mouse Up!!!</p>");
                fnb.hyperion.delegate.off(this.body.elem, 'touchmove', fnb.hyperion.forms.rangeSlider.bindToTouchMove);
                fnb.hyperion.delegate.off(_this.body.elem, 'touchmove', fnb.hyperion.forms.rangeSlider.BlockMove);

                var i=0, len = _this.sliderGroup[targetID].snapPointsArr.length, closestPointIndex =0, pointVal = 0, newPointVal =0;
                  
               var  curentActiveSlider = fnb.hyperion.$('#rangeSliderContainer'+targetID);
                //var element = document.getElementById('some-id');
                var position = curentActiveSlider.elem.getBoundingClientRect();
                var screenOffSet = position.left


                var targetPosition = target.elem.getBoundingClientRect();
               //get the offset of the mouse cursor on mouse move
               //have to use clientX - pageX not working in IE8
                var xOffSet  =  targetPosition.left - screenOffSet,
                //current percentage of slider
                percentage = Math.round(xOffSet /  _this.sliderGroup[targetID].rangeSliderWidth * 100),
                sliderPercentage = (percentage > 100)? 100 :  percentage;

                // Loop through all snapPoint and find snapPoint closestPointIndex.
                  for(i ; i < len; i++){
                    newPointVal = Math.abs(sliderPercentage - _this.sliderGroup[targetID].snapPointsArr[i].percentage);
                      if(newPointVal < pointVal || pointVal == 0 ){
                          closestPointIndex = i;
                          pointVal = newPointVal;
                      }
                  }

                //get percentage value from snapPointsArr Object   
                sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex].percentage;
                //get Label percentage value from snapPointsArr Object   
               var snapPointLabel = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex].dataBtnLabel; 

                  _this.setSnapValueToYouPay(targetID,closestPointIndex);
                  // change slider label to snapPoint Label 
                 _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(snapPointLabel);
                  
                 sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
                 //change btn label position if greater that 50%;
                 _this.doChangeLabelAlign(targetID, sliderPercentage );
                _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
                _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

                // calculate new total and update html
                _this.calculatePackageTotal(targetID, closestPointIndex);

            }


        },
        //bind to mouseMove event ... get position of mouse
        bindToTouchMove:function(event){
          var _this  = fnb.hyperion.forms.rangeSlider,
            element = event.currentTarget,
            targetID = _this.activeSliderId;
                   
           var  curentActiveSlider = fnb.hyperion.$('#rangeSliderContainer'+targetID);
           //var element = document.getElementById('some-id');
           var position = curentActiveSlider.elem.getBoundingClientRect();
           var screenOffSet = position.left;
          //get the offset of the mouse cursor on mouse move
          //have to use clientX - pageX not working in IE8
          var xOffSet  = event.touches[0].pageX - screenOffSet,
              percentage =  Math.round( xOffSet / _this.sliderGroup[targetID].rangeSliderWidth * 100),
              percentage = ( percentage < 0 ) ? 0 : percentage;
            console.log('Offset :'+xOffSet+' and percentage : '+percentage);
           _this.sliderGroup[targetID].progressPercentage = ( percentage > 100) ?  100 : percentage;

           // setting the width of the progress bar and the slider Btn
           _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

           //var currentVal =  Math.round(percentage * _this.interValValue);
           //_this.showValDiv.html(currentVal);
           _this.body.addClass("hideSelect");
        },
        BlockMove : function (event) {
          // Tell Safari not to move the window.
            event.stop();
        },
        //bind to mouseMove event ... get position of mouse
        bindToMouseMove:function(event){
          event.stop();
          var _this  = fnb.hyperion.forms.rangeSlider,
             element = event.currentTarget,
            targetID = _this.activeSliderId;
                   
           var  curentActiveSlider = fnb.hyperion.$('#rangeSliderContainer'+targetID);
           //var element = document.getElementById('some-id');
           var position = curentActiveSlider.elem.getBoundingClientRect();
           var screenOffSet = position.left;
          //get the offset of the mouse cursor on mouse move
          //have to use clientX - pageX not working in IE8
          var xOffSet  = event.clientX - screenOffSet,
              percentage =  Math.round( xOffSet / _this.sliderGroup[targetID].rangeSliderWidth * 100),
              percentage = ( percentage < 0 ) ? 0 : percentage;

           _this.sliderGroup[targetID].progressPercentage = ( percentage > 100) ?  100 : percentage;

           // setting the width of the progress bar and the slider Btn
           _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

           //var currentVal =  Math.round(percentage * _this.interValValue);
           //_this.showValDiv.html(currentVal);
           _this.body.addClass("hideSelect");
        },
        //snap to pointer selected.
        snapToPointer:function(event){
           var _this  = fnb.hyperion.forms.rangeSlider;
           	   _this.checkRenit();
           		
           var  currentTargetID = _this.activeSliderId,
                //pointer that was clicked
                target = fnb.hyperion.$(event.currentTarget),
                targetID = target.attr("data-sliderid"),
                SnapToPercentage = target.attr("data-percentage"),
                snapPointLabel = target.attr("data-btnlabel"),
                id = target.attr("id"),
                closestPointIndex =  id.replace(/\D/g,'') - 1;

            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
                _this.body = fnb.hyperion.$('#bodyContainer');
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                    //  -- update to active
                      _this.activeSliderId = targetID;   
                   }
            }
             
             //set you pay value equal to snapPoint Value
             _this.setSnapValueToYouPay(targetID,closestPointIndex);
          
            _this.doChangeLabelAlign(targetID, SnapToPercentage );
            // change slider label to snapPoint Label
           _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(snapPointLabel);

           _this.sliderGroup[targetID].progressPercentage =SnapToPercentage;
           _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

           // calculate new total and update html
           _this.calculatePackageTotal(targetID, closestPointIndex);
        },
        //calculate package total
        calculatePackageTotal : function(targetID,closestPointIndex){
            var _this  = fnb.hyperion.forms.rangeSlider;
            //get all you pay elements
            var allYouPayElements = fnb.hyperion.$('* .youPayValue'),
            //get total package Element
			totalPackageElement = fnb.hyperion.$('#newPackageTotal'),
            availableCreditElement = fnb.hyperion.$('#availableCredit'),
            recurringBundleElement = fnb.hyperion.$('#recurringBundles'),
            currentPackageValue  = 0,
            total = 0;

             if( _this.sliderType == 'banking'){
               var currentPackageElement = fnb.hyperion.$('#currentPackageValue'),
                   
                   currentPackageValue = parseFloat(currentPackageElement.html());
                   currentPackageValue = (currentPackageValue >0 && currentPackageValue != undefined && currentPackageValue != '')? currentPackageValue : 0;
             }
            
            //get values of each element and calculate the total.
            allYouPayElements.each(function(element, index){
                   total +=  parseFloat(element.html());
            });
            
            var newPackageTotal = total +currentPackageValue;
            lastActiveSnapPoint =_this.sliderGroup[targetID].activeSnapPoint;
            maxValue = parseFloat(totalPackageElement.attr('data-maxvalue'));
            minValue = parseFloat(totalPackageElement.attr('data-minvalue'));
            snapMaxValue = _this.sliderGroup[targetID].maxSnappoint;
            availableCredit = maxValue - total;
            //if total is greater than maxValue then snap back to previous Active Snappoint
            if(total > maxValue && maxValue >= 0 || closestPointIndex >  snapMaxValue  ){ 
                lastActiveSnapPoint = (closestPointIndex >  snapMaxValue) ? snapMaxValue : lastActiveSnapPoint;
                //availableCreditElement.html(availableCredit);
                _this.setSnapValueToYouPay(targetID,lastActiveSnapPoint);
                 //get percentage value from snapPointsArr Object   
                sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[lastActiveSnapPoint].percentage;   

                sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
                 //change btn label position if greater that 50%;  
                _this.doChangeLabelAlign(targetID, sliderPercentage );
                _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
                _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);
                
                // change slider label to snapPoint Label
                _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(_this.sliderGroup[targetID].snapPointsArr[lastActiveSnapPoint].dataBtnLabel); 
           }else if(total < minValue && minValue >= 0 ){
                  //availableCreditElement.html(availableCredit);
                 _this.setSnapValueToYouPay(targetID,lastActiveSnapPoint);
                 //get percentage value from snapPointsArr Object   
                sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[lastActiveSnapPoint].percentage;   

                sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
                 //change btn label position if greater that 50%;  
                _this.doChangeLabelAlign(targetID, sliderPercentage );
                _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
                _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);
                
                // change slider label to snapPoint Label
                _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(_this.sliderGroup[targetID].snapPointsArr[lastActiveSnapPoint].dataBtnLabel);
           }else{
             //check if availableCredit element exist. and set it
        	 if(availableCreditElement.length() > 0){ availableCreditElement.html(availableCredit);}
             //check if availableCredit element exist. and set it
             if(recurringBundleElement.length() > 0){ recurringBundleElement.html(total); } 
             _this.sliderGroup[targetID].activeSnapPoint= closestPointIndex;
             //update total value with total.
             totalPackageElement.html(newPackageTotal);
            }

        },
        //set you pay value equal to snapPoint Value
        setSnapValueToYouPay:function(targetID,closestPointIndex){
           
           var _this  = fnb.hyperion.forms.rangeSlider;
           

          //if sliderType is sales set data-settings values itemCode and SkuCode
          if(_this.sliderType === 'sales'){
            var youPayElement =  fnb.hyperion.$('#youPayValue'+targetID);
                             youPayElement.html(_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].value);
                             youPayElement.attr("data-itemcode",_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].itemCode);
                             youPayElement.attr("data-itemskucode",_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].itemSkuCode);
          }
          //if sliderType equals banking add itemCode to Input
          if(_this.sliderType === 'banking'){
             fnb.hyperion.$('#youPayInput'+targetID).val(_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].itemCode);
            // fnb.hyperion.$('#youPayRate'+targetID).html(_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].rate);
             fnb.hyperion.$('#youPayValue'+targetID).html(_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].value);
          }


        },
        // get the width and height of browser window;
        getBrowserSize:function(event){
             //get browser width and height
              var obj ={};
                if (document.body && document.body.offsetWidth) {
                 obj["winW"] = document.body.offsetWidth;
                 obj["winH"] = document.body.offsetHeight;
                }
                if (document.compatMode=='CSS1Compat' &&
                    document.documentElement &&
                    document.documentElement.offsetWidth ) {
                   obj["winW"] = document.documentElement.offsetWidth;
                   obj["winH"] = document.documentElement.offsetHeight;
                }
                if (window.innerWidth && window.innerHeight) {
                   obj["winW"] = window.innerWidth;
                   obj["winH"] = window.innerHeight;
                }
                // return object with objectName.winW for browser width and objectName.winH for browser height;
                return obj;
        },
        // this will select the slider with id specified and got snapPoint specified.
        setSliderToSnapPoint : function(sliderId, snapPoint){
            var _this  = fnb.hyperion.forms.rangeSlider;
            var targetID = sliderId;

            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                    //  -- update to active
                      _this.activeSliderId = targetID;   
                   }
            }
            //loop through snappoints get percentage of snapPoint specified and set slider to that snapPoint
            //build array of all snap points
                 _this.sliderGroup[targetID].snapPointsElements.each(function(element, index){

                        if(index+1 == snapPoint){
                            _this.sliderGroup[targetID].progressPercentage =parseInt(element.attr("data-percentage"));;
                            _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);
                        }

                  });  
        },
        setSliderSnapPointToPercentage: function(sliderId, percentage){
            var _this  = fnb.hyperion.forms.rangeSlider;
            console.log(_this);
            var targetID = sliderId;
            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                      //  -- update to active
                      _this.activeSliderId = targetID;   
                   }
            }
            //snap to closet point for percantage given
            var i=0, len = _this.sliderGroup[targetID].snapPointsArr.length, closestPointIndex =0, pointVal = 0, newPointVal =0;
                sliderPercentage = (percentage > 100)? 100 :  percentage;

                for(i ; i < len; i++){
                  newPointVal = Math.abs(sliderPercentage - _this.sliderGroup[targetID].snapPointsArr[i]);
                    if(newPointVal < pointVal || pointVal == 0 ){
                        closestPointIndex = i;
                        pointVal = newPointVal;
                    }
                }
            //get label of closest snapPoint ..snapPoints
            var snapPointLabel =  _this.sliderGroup[targetID].snapPointsElements.find('!span.snapPoints'+closestPointIndex); 

            sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex];
            sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
            _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
            _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);
        },
        // this will slect the slider Specified in the sliderId and return the value for the snapPoint specified.
        getSnapPointValue:function(sliderId, snapPoint){
            var   _this  = fnb.hyperion.forms.rangeSlider,
                targetID = sliderId;

            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                    //  -- update to active
                      _this.activeSliderId = targetID;   
                   }
            }
            //loop through snapPoints get percentage of snapPoint specified and set slider to that snapPoint
            //build array of all snap points
            var value =0;
            _this.sliderGroup[targetID].snapPointsElements.each(function(element, index){

                if(index+1 == snapPoint){
                    value = element.attr("data-value");                  
                }

            });
            return value;
        },
        //change label left or right align.
        doChangeLabelAlign: function(targetID, percentage){
           var _this  = fnb.hyperion.forms.rangeSlider;
           
           if(percentage >= 51){
              if(_this.sliderGroup[targetID].rangeSliderBtnLabel.hasClass('btnRight')){
                _this.sliderGroup[targetID].rangeSliderBtnLabel.removeClass('btnRight')
              }
              _this.sliderGroup[targetID].rangeSliderBtnLabel.addClass('btnLeft')
           }else{
              if(_this.sliderGroup[targetID].rangeSliderBtnLabel.hasClass('btnLeft')){
                _this.sliderGroup[targetID].rangeSliderBtnLabel.removeClass('btnLeft')
              }
              _this.sliderGroup[targetID].rangeSliderBtnLabel.addClass('btnRight');
           }
            
        }

  };
    //Namespace rangeSlider
    fnb.namespace('forms.rangeSlider', rangeSlider, true);
})();