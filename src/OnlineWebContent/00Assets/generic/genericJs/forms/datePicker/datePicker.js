///-------------------------------------------///
/// developer: Donovan
///
/// Datepicker Object
///-------------------------------------------///
(function() {
	//Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '#datePicker', handler: 'fnb.hyperion.controller.raiseEvent("showDatePicker",{event:event});'},
    	              {type: 'frame', listener: document, events:'click', selector: '.dateLeftScrollArrow', handler: 'fnb.hyperion.forms.datePicker.datePickerScrollLeft();'},
    	              {type: 'frame', listener: document, events:'click', selector: '.dateRightScrollArrow', handler: 'fnb.hyperion.forms.datePicker.datePickerScrollRight();'},
    	              {type: 'frame', listener: document, events:'click', selector: '.datePickerClose', handler: 'fnb.hyperion.controller.raiseEvent("hideDatePicker",{event:event});'},
    	              {type: 'frame', listener: document, events:'click', selector: '.datePickerScrollerWrapper [data-active="true"]', handler: 'fnb.hyperion.forms.datePicker.select(event);'},
    	              {type: 'frame', listener: document, events:'change', selector: '#datePickerDropdown', handler: 'fnb.hyperion.forms.datePicker.dropdownSelect(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Datepicker Parent function
	///-------------------------------------------///
	function datePicker() {

	};
	///-------------------------------------------///
	/// Datepicker Methods
	///-------------------------------------------///
	datePicker.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Var for calendar state
		active: true,
		//Datepicker wrapper
		datePickerTarget: '',
		//Current datepicker date
		currentDatePickerDate: '',
		//List of month names
		monthNames: ["January","February","March","April","May","June","July","August","September","October","November","December"],
		//List of month names
		shortMonthNames: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
		//List of week days
		weekDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
		//List of days per month
		monthDays: [31,28,31,30,31,30,31,31,30,31,30,31],
		//Get today's date
		today : new Date(),
		//Var amount of months to display
		totalMonths: 9,
		//Datepicker input selector
		datePickerValueTarget: '',
		//Instance of horizontal scroller for datepicker
		dateScroller: '',
		//Datepicker scroller selector
		datePickerWrapper: '',
		//Datepicker scroller wrapper selector
		datePickerScrollerWrapper: '',
		//Datepicker scroller selector
		datePickerScroller: '',
		//Var fordatePicker breakpoints
		breakPoints: [2,1,1],
		//Current breakpoint position
		datePickerBreakpointValue:-2,
		//Val of months in calendar
		monthCount:0,
		//Init FNB Datepicker
    	init: function () {
    		console.log('Forms Datepicker init');
        	//Global Datepicker wrapper selector
        	this.datePickerWrapper = fnb.hyperion.controller.datePickerElement.find('.datePickerContainer');
    		//Datepicker scrollable wrapper
    		this.datePickerScrollerWrapper = this.datePickerWrapper.find('.datePickerScrollerWrapper');
    		//Datepicker scrollable wrapper
    		this.datePickerScroller = this.datePickerScrollerWrapper.children(0);
    		//Bind datepicker events
    		bindEvents();
    		//Extend Controller resize function
    		fnb.hyperion.controller.addResizeFunction(this.resize);
        },
        //Select Date
        select: function (event) {
        	//End date scroll 
        	this.dateScroller.end(event);
        	//Test if event is swipe
        	if(this.dateScroller.moving == false){
        		//Select target with values
        		var target = fnb.hyperion.$(event.currentTarget);
        		//Get year 
        		var year = target.attr('data-year');
        		//Get month 
        		var month = target.attr('data-month');
        		//Get day
        		var date = target.attr('data-date');
        		//Get day name
        		var dayName = target.attr('data-dayName');
        		//Get Monthname
        		var monthName = target.attr('data-monthName');
        		//Create selected date strings
        		var topLabelString = dayName+' '+date;
        		var bottomLabelString = monthName+' '+year;
        		//Set input data value
        		this.datePickerValueTarget.attr('data-value',year+'-'+month+'-'+date);
        		//Set input value
        		this.datePickerValueTarget.val(year+'-'+month+'-'+date);
        		//Set date strings
        		this.datePickerTarget.find('.datePickerLabelTop').html(topLabelString);
        		this.datePickerTarget.find('.datePickerLabelBottom').html(bottomLabelString);
        		//Close datepicker
        		fnb.hyperion.controller.raiseEvent("hideDatePicker",{});
        	}
        },
        //Select dropdown
        dropdownSelect: function (event) {
        	//Select target with values
    		var target = fnb.hyperion.$(event.currentTarget);
    		//Get year 
    		var year = target.val();
    		//Split date
        	var dateArray = this.currentDatePickerDate.split("-");
        	var month= parseInt(dateArray[1]);
        	var day	 = parseInt(dateArray[2]);
        	//Create new date string
        	var dateString = year+'-'+month+'-'+day;
    		//Creat calendar
    		this.create(dateString);
        },
        //Show datepicker
        show: function (obj) {
        	//Get scroll position to set later
			fnb.hyperion.controller.getScrollPosition(document.body);
			//Change Ezi Panel data attribute
			fnb.hyperion.controller.clipOverflow(true);
			//Set datepicker to visible
        	fnb.hyperion.controller.datePickerElement.show();
        	//Select the datePicker target
        	this.datePickerTarget = fnb.hyperion.$(obj.event.currentTarget);
        	//Select datepicker value target
        	this.datePickerValueTarget = this.datePickerTarget.find('=input');
        	//Get datepicker selected date
        	var date = this.datePickerValueTarget.attr('data-value');

        	//Test if new calendar needs to be created
        	if(date!=this.currentDatePickerDate){
        		//Creat calendar
        		this.create(date);
        	}else{
        		//Append calendar wrapper
        		this.setCalendar();
        	}
        	//Flag that calendar is showing
        	this.active = true;
        },
        //Hide calendar
        hide: function () {
        	//Test if datepicker is visible
        	if(this.active){
        		//Flag that calendar is not showing
            	this.active = false;
    			//Change Ezi Panel data attribute
    			fnb.hyperion.controller.clipOverflow(false);
            	//Notify controller to hide the date overlay
            	fnb.hyperion.controller.datePickerElement.hide();
            	//Get scroll position to set later
    			fnb.hyperion.controller.setScrollPosition(document.body);
        	}
        },
        //Create calander
        create: function (date) {
        	//Split date
        	var dateArray = date.split("-");
        	var year = parseInt(dateArray[0]);
        	var month= parseInt(dateArray[1]);
        	var day	 = parseInt(dateArray[2]);
        	//Set datepicker dropodown
        	fnb.hyperion.controller.datePickerDropdown.val(year);
    		//Get amount of months to display
        	this.monthCount = parseInt(this.datePickerTarget.attr('data-months'));
    		//Get start before current month
        	var startBefore = this.datePickerTarget.attr('data-startBefore');
    		//Set currentDatePickerDate
    		this.currentDatePickerDate = date;
    		//Append calendar wrapper
    		this.setCalendar(this.getCalendar(year, month, day, this.monthCount, startBefore));
    		//New Instance of horizontal scroller for datepicker
			this.dateScroller = new fnb.hyperion.horizontalScroller();
			this.dateScroller.scrollableParent = this.datePickerScroller;
			this.dateScroller.scrollerChildren = '.datePickerMonthWrapper';
			this.dateScroller.scrollStopWidth = 200;
			this.dateScroller.scrollSpeed = 5;
			this.dateScroller.moveTreshold = 0.20;
			this.dateScroller.bindEvents();
			this.dateScroller.enabled = true;
			//Events to execute after datepicker stopped scrolling
			this.dateScroller.afterStop = this.checkCalendarPosition;
			//Reset datepicker breakpoint value
			this.datePickerBreakpointValue = -2;
        	//Current window width
        	var windowWidth = fnb.hyperion.controller.window.innerWidth || fnb.hyperion.controller.document.documentElement.clientWidth;
        	//Adjust datepicker onshow
			this.adjustDatePicker(windowWidth, fnb.hyperion.controller.getBreakPosition(windowWidth));
        },
        //Build datepicker
        getCalendar: function (year, month, day, monthCount, startBefore) {
        	//Ensure year, month and day is Int
        	year = parseInt(year);
    		month= parseInt(month);
    		day	 = parseInt(day);
    		//Month to start with
    		var currentMonth = month;
    		//Year to start with
    		var currentYear = year;
    		//Check if datepicker need to start before current month
    		if(startBefore>0){
        		//Get the month to start with
        		var startInt = currentMonth - startBefore;
    			//Test if year skipped
        		if(startInt<1){
        			currentMonth = 12-Math.abs(startInt);
        			currentYear--;
        		}else{
        			currentMonth = startInt;
        		}
    		}
    		//Calendar data
    		var calendarData = [];
    		//Loop total months
    		for(var i=0;i<monthCount;i++) {
    			//Set current month index
    			var currentMonthIndex = currentMonth-1;
    			//Object for month data
    			var monthData = {templateName : 'datePickerMonthWrapper', templateData: {}};
        		//Var for current day of the month increment
        		var currentMonthDaysIncrement = 1;
    			//Get the first day of this month
    			var firstDay = new Date(currentYear,currentMonthIndex,1);
    			var startDay = firstDay.getDay();
    			//Leap year support
    			if(currentYear % 4 == 0) this.monthDays[1] = 29;
    			else this.monthDays[1] = 28;
    			//Get the amount of days in this month
    			var daysInMonth = this.monthDays[currentMonthIndex];
    			//Set month name data
    			monthData.templateData.monthName = this.monthNames[currentMonthIndex];
    			//Set month data
    			monthData.templateData.month = currentMonth;
    			//Set year data
    			monthData.templateData.year = currentYear;
    			//Set current month number
    			monthData.templateData.count = i;
    			//Flag for calendar days to start
    			var startFlag = false;
    			//Rows collection wrapper
				var rowsData = '';
				//Var for month name
				var monthName = this.shortMonthNames[currentMonthIndex];
    			//Create the calender
    			//Loop rows per month
    			for(var j=0;j<5;j++) {
        			//Object for row data with templatename
        			var rowData = [];
    				//Loop days of the week
    				for(var k=0;k<7;k++) {
    					//Each calandar items data
    					var itemData = {templateName : 'datePickerItem', templateData: {}};
    					//If the days has overshooted the number of days in this month, stop writing
    					if(currentMonthDaysIncrement > daysInMonth) startFlag=false;
    					//If the first day of this month has come, start the date writing
    					else if(k >= startDay && !startFlag) startFlag=true;
    					//Set if selected date
    					var selectedVal = false;
    					//Clean date value
    					var dateVal = '&nbsp;';
    					//Clean month value
    					var monthVal = '';
    					//Clean year value
    					var yearVal = '';
    					//Set default active state
    					var activeVal = false;
    					//Start building row
    					if(startFlag) {
    						//Set day value
    						dateVal = currentMonthDaysIncrement;
    						//Set month value
    						monthVal = currentMonth;
    						//Set year value
    						yearVal = currentYear;
    						//Set active value
    						activeVal = true;
            				//Increment current day of the month
            				currentMonthDaysIncrement++;
    					}
    					//Test if selected date
            			if(dateVal==day&&monthVal==month&&yearVal==year) selectedVal = true;
    					//Set item day selected
    					itemData.templateData.dataSelected = selectedVal;
    					//Set item day value
    					itemData.templateData.dayName = this.weekDays[k];
    					//Set item Month name value
    					itemData.templateData.monthName = monthName;
    					//Set item date value
    					itemData.templateData.date = dateVal;
    					//Set item month value
    					itemData.templateData.month = monthVal;
        				//Set item year value
    					itemData.templateData.year = yearVal;
        				//Set item year value
    					itemData.templateData.dataActive = activeVal;
        				//Append item collection
    					rowData.push(itemData);
        			};
        			//Set all rows html
        			rowsData += '<tr>'+fnb.hyperion.controller.getHtmlTemplate(rowData)+'</tr>';
    			}
    			//Set month data
    			monthData.templateData.rows = rowsData;
    			//Set month to calendar
    			calendarData.push(monthData);
    			//Increment Current month
    			currentMonth++;
    			//Test if month in scope
    			if((currentMonth-1)>=12) {
    				currentMonth = 1;
    				currentYear++;
    			}
    			
    		}
    		
    		return fnb.hyperion.controller.getHtmlTemplate(calendarData);
        },
        //Set calendar to dom
        setCalendar: function (html) {
        	//Set visibility
        	this.datePickerWrapper.show();
    		//Set html of datepickerWrapper
        	if(html) this.datePickerScroller.html(html);
        },
        //Check if datepicker needs more months
        checkCalendarPosition: function () {
        	//Child var
    		var child;
    		//Month var
        	var month = '';
    		//Year var
    		var year = '';
        	//Check if datepicker is at the end
        	if(fnb.hyperion.forms.datePicker.dateScroller.currentIndex==fnb.hyperion.forms.datePicker.dateScroller.maxStops){
        		//Get child
        		child = fnb.hyperion.forms.datePicker.datePickerScroller.children[fnb.hyperion.forms.datePicker.datePickerScroller.children().length()-1];
        		//Get month for dates to continue
        		month = parseInt(child.attr('data-month'));
        		//Get year for dates to continue
        		year = parseInt(child.attr('data-year'));
        		//Increase month
        		month++;
        		if(month>12){
        			month = 1;
        			year++;
        		}
        		//Get months needed
        		var newMonths = fnb.hyperion.forms.datePicker.getCalendar(year, month, '', fnb.hyperion.forms.datePicker.datePickerBreakpointValue, 0);
        		//Create DOM node
        		var newNode = document.createElement("div");
        		//Set Node html
        		newNode.html(newMonths);
        		//Append to Calendar
        		var i = fnb.hyperion.forms.datePicker.datePickerBreakpointValue; 
        		while (i--) {
        			fnb.hyperion.forms.datePicker.removeCalendar(fnb.hyperion.forms.datePicker.datePickerScroller.children[0]);
        			//Prepend new child
        			fnb.hyperion.forms.datePicker.appendCalendar(newNode.children[0]);
        		}
        		//Setup horizontal scroller
        		//fnb.hyperion.forms.datePicker.dateScroller.scrollableParent.stop();
        		fnb.hyperion.horizontalScroller.slide(fnb.hyperion.forms.datePicker.dateScroller.scrollableParent).stop();
        		//Center scroller new index
        		fnb.hyperion.forms.datePicker.dateScroller.centerIndex(fnb.hyperion.forms.datePicker.dateScroller.maxStops-1);
        	}
        	//Check if datepicker is at the start
        	else if(fnb.hyperion.forms.datePicker.dateScroller.currentIndex==0){
        		//Get first child
        		child = fnb.hyperion.forms.datePicker.datePickerScroller.children(0);
        		//Get month for dates to continue
        		month = parseInt(child.attr('data-month'));
        		//Get year for dates to continue
        		year = parseInt(child.attr('data-year'));
        		//Test if month in scope
        		if(month==0){
        			month = 12;
        			year--;
        		}
        		//Get months needed
        		var newMonths = fnb.hyperion.forms.datePicker.getCalendar(year, month, '', fnb.hyperion.forms.datePicker.datePickerBreakpointValue, fnb.hyperion.forms.datePicker.datePickerBreakpointValue);
        		//Create DOM node
        		var newNode = document.createElement("div");
        		//Set Node html
        		newNode.innerHTML = newMonths;
        		//Append to Calendar
        		var i = fnb.hyperion.forms.datePicker.datePickerBreakpointValue; 
        		while (i--) {
        			fnb.hyperion.forms.datePicker.removeCalendar(fnb.hyperion.forms.datePicker.datePickerScroller.children(fnb.hyperion.forms.datePicker.datePickerScroller.children().length()-1));
        			//Prepend new child
        			fnb.hyperion.forms.datePicker.prependCalendar(newNode.children[i],fnb.hyperion.forms.datePicker.datePickerScroller.children(0));
        		}
        		//Setup horizontal scroller
        		//fnb.hyperion.forms.datePicker.dateScroller.scrollableParent.stop();
        		fnb.hyperion.horizontalScroller.slide(fnb.hyperion.forms.datePicker.dateScroller.scrollableParent).stop();
        		//Center scroller new index
        		fnb.hyperion.forms.datePicker.dateScroller.centerIndex(1);
        	};
        },
        //Append calendar with months
        appendCalendar: function (html) {
        	this.datePickerScroller.add(html);
        },
        //Prepend calendar with months
        prependCalendar: function (html,child) {
        	this.datePickerScroller.elem.insertBefore(html,child.elem);
        },
        //Remove child from calendar
        removeCalendar: function (child) {
        	this.datePickerScroller.remove(child.elem);
        },
        //Resize datepicker when window resizes
        adjustDatePicker: function (windowSize, breakpoint) {
        	//Var breakpoint index
        	var breakPointIndex = (breakpoint-1<0) ? 0: breakpoint-1;
        	/*Get value of the current breakpoint for amount of months to be shown*/
        	var currentBreakPointValue = this.breakPoints[breakPointIndex];
        	/*Get the scrolling container width*/
        	var currentContainerWidth = this.datePickerScrollerWrapper.outerWidth();
        	/*Calculate the new width for the Months with breakpoint value*/
        	var newMonthWidth = currentContainerWidth/currentBreakPointValue;
        	/*Calculate new width of the horizontal scroller*/
        	var newScrollingWrapperWidth =newMonthWidth*this.monthCount;
        	/*Apply styles*/
        	//Hack for pixel rounding
        	newScrollingWrapperWidth = newScrollingWrapperWidth+(this.monthCount/2);
        	/*Apply Scroller wrapper new width*/
        	this.datePickerScroller.css("width", newScrollingWrapperWidth+'px');
        	//Update the amount the scroller should scroll by
        	this.dateScroller.scrollStopWidth = currentContainerWidth;
        	//Test if breakpoint changed
        	if(this.breakPoints[breakPointIndex]!=this.datePickerBreakpointValue){
        		//Update datepicker breakpoint value
        		this.datePickerBreakpointValue = this.breakPoints[breakPointIndex];
            	//Update horizontal scroller stops
        		this.dateScroller.maxStops = (this.monthCount/this.datePickerBreakpointValue)-1;
        		//Set var to move to
        		var movePos = 4;
    			//Test if single month
    			if(this.datePickerBreakpointValue==1) movePos=8;
    			//Set datepicker horizontal scroller index
    			this.dateScroller.currentIndex = movePos;
    			//Calculate left position
    			var leftPos = -currentContainerWidth*movePos;
    			//Move to correct month
    			this.datePickerScroller.css("left", leftPos+'px');
    			//Update scroller x position
    			this.dateScroller.x = leftPos;
        	}
        },
        //Scroll months right
        datePickerScrollRight: function () {
        	this.dateScroller.next();
        },
        //Scroll months left
        datePickerScrollLeft: function () {
        	this.dateScroller.previous();
        },
        //Resize datepicker when window resizes
        resize: function (windowSize, breakpoint) {
        	//if datepicker exists do adjust
        	if(fnb.hyperion.forms.datePicker.active){
        		fnb.hyperion.forms.datePicker.adjustDatePicker(windowSize,breakpoint);
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.datePicker = {};
        }
	};

	//Namespace datePicker
	fnb.namespace('forms.datePicker', datePicker, true);

})();