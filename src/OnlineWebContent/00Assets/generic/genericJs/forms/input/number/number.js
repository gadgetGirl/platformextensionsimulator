///-------------------------------------------///
/// developer: Donovan
///
/// Number Input Object
///-------------------------------------------///
(function() {
	//Test if keycode is allowed
	function allowKeyPress(event) {
		//Get keycode
		var keyCode = event.charCode||event.keyCode||event.which;
		//Count fullstops
		var fullstop = event.currentTarget.value.split(".").length;
		//Test if shift key is down
		if(!event.shiftKey&&fullstop>2){
			//Test if valid
			if(fnb.hyperion.forms.input.numberKeyCodes.indexOf(keyCode)>-1||fnb.hyperion.forms.input.validKeyCodes.indexOf(keyCode)>-1||event.ctrlKey&&fnb.hyperion.forms.input.ctrlDownKeyCodes.indexOf(keyCode)>-1){
				return true;
			};
		};

		return false;
	};
	///-------------------------------------------///
	/// Number Parent function
	///-------------------------------------------///
	function number() {

	};
	///-------------------------------------------///
	/// Number Methods
	///-------------------------------------------///
	number.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Number
		init: function () {
			console.log('Forms input Number init');
		},
		//Test keydown
		keyDown: function (event) {
			return allowKeyPress(event);
		},
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.forms.input.number = {};
		}
	};

	//Namespace input.number
	fnb.namespace('forms.input.number', number, true);

})();