///-------------------------------------------///
/// developer: Donovan
///
/// Mask Input Object
///-------------------------------------------///
(function() {
	//Current focused input/textArea
	activeInput='';
	//Default is number
	isNumber = true;
	//Default 2 decimals
	decimals = 2;
	//Default decimal points
	decimalsPoints = '.';
	//Default thousands seperator
	thousandsSeperator = ' ';
	//Unicode decimal var
	unicodeDecimal = '';
	//Regular expression decimal number
	regexDecimalNumber = '';
	//Regular expression decimal
	regexDecimal = '';
	//Test if keycode is allowed
	function value(input,value) {
		//Get input data
		var data = input.numFormat;

		var thousandsSeperator = (data) ? (typeof data.thousandsSeperator === 'undefined') ? thousandsSeperator : data.thousandsSeperator : thousandsSeperator;
		var decimalsPoints = (data) ? (typeof data.decimalsPoints === 'undefined') ? decimalsPoints : data.decimalsPoints : decimalsPoints;
		var decimals = (data) ? (typeof data.decimals === 'undefined') ? decimals : data.decimals : decimals;
		
		//If there is no value its a set otherwise its a get
		if(value){
			return input.val(format(null,value,decimals,decimalsPoints,thousandsSeperator));
		}else{
			var number;

			if(input.val() === '') return '';
			
			number = +(input.val().replace( data.regexDecimalNumber, '' ).replace( data.regexDecimal, '.' ));
		
			return ''+(isFinite(number) ? number : 0);
		}
	}
	//Format number as string
	function format(target,number,decimals,decimalsPoints,thousandsSeperator){

		//Set the default values
		thousandsSeperator = (typeof thousandsSeperator === 'undefined') ? ' ' : thousandsSeperator;
		decimalsPoints = (typeof decimalsPoints === 'undefined') ? '.' : decimalsPoints;
		decimals = !isFinite(+decimals) ? 2 : Math.abs(decimals);

		//Get unicode representation for the decimal place and thousand sep.	
		var unicodeDecimal = ('\\u'+('0000'+(decimalsPoints.charCodeAt(0).toString(16))).slice(-4));
		var unicodeSeperator = ('\\u'+('0000'+(thousandsSeperator.charCodeAt(0).toString(16))).slice(-4));
		
		//Fix the number to actual number.
		number = (number + '')
			.replace('\.', decimalsPoints)
			.replace(new RegExp(unicodeSeperator,'g'),'')
			.replace(new RegExp(unicodeDecimal,'g'),'.')
			.replace(new RegExp('[^0-9+\-Ee.]','g'),'');
		
		var tempNumber = !isFinite(+number) ? 0 : +number,
		    formattedString = '',
		    toFixedFix = function (tempNumber, decimals) {
		        var k = Math.pow(10, decimals);
		        return '' + Math.round(tempNumber * k) / k;
		    };
		
		//Fix for IE parseFloat(0.55).toFixed(0) = 0;
		formattedString = (decimals ? toFixedFix(tempNumber, decimals) : '' + Math.round(tempNumber)).split('.');
		if (formattedString[0].length > 3) {
			formattedString[0] = formattedString[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, thousandsSeperator);
		}
		if ((formattedString[1] || '').length < decimals) {
			formattedString[1] = formattedString[1] || '';
			formattedString[1] += new Array(decimals - formattedString[1].length + 1).join('0');
		}

		if(target){
			fnb.hyperion.$('#'+target).val(formattedString.join(decimalsPoints));
		}else{
			return formattedString.join(decimalsPoints);
		}
	}
	//Test if keycode is allowed
	function allowKeyPress(event) {
		//Get keycode
		var keyCode = event.charCode||event.keyCode||event.which;
		//Test if shift key is down
		if(!event.shiftKey){
			//Test if valid
			if(fnb.hyperion.forms.input.numberKeyCodes.indexOf(keyCode)>-1||fnb.hyperion.forms.input.validKeyCodes.indexOf(keyCode)>-1||fnb.hyperion.forms.input.specialCharKeyCodes.indexOf(keyCode)>-1||event.ctrlKey&&fnb.hyperion.forms.input.ctrlDownKeyCodes.indexOf(keyCode)>-1){
				return true;
			};
		};
		return false;
	}
	//Method for selecting a range of characters in an input/textarea.
	function setSelectionRange(input,start,end)
	{
		//Check which way we need to define the text range.
		if( this.createTextRange )
		{
			var range = input.createTextRange();
				range.collapse(true);
				range.moveStart( 'character',start);
				range.moveEnd( 'character',end-start);
				range.select();
		}
		//Check browser support
		else if(input.setSelectionRange)
		{
			input.focus();
			input.setSelectionRange(start,end);
		}
	}
	//Get the selection position for the given part.
	function getSelection(input,part)
	{
		var pos	= input.val().length;
		//Get selection
		part = ( part.toLowerCase() == 'start' ? 'Start' : 'End' );

		if(document.selection){
			//The current selection
			var range = document.selection.createRange(), storedRange, selectionStart, selectionEnd;
			//Temp holder
			storedRange = range.duplicate();
			//stored_range.moveToElementText( this );
			storedRange.expand('textedit');
			//Move Temp end point to end point of original range
			storedRange.setEndPoint( 'EndToEnd', range );
			//Calculate start and end points
			selectionStart = storedRange.text.length - range.text.length;
			selectionEnd = selectionStart + range.text.length;
			return part == 'Start' ? selectionStart : selectionEnd;
		}
		else if(typeof(input['selection'+part])!="undefined")
		{
		 	pos = input['selection'+part];
		}
		return pos;
	}
	function getCaretCharacterOffsetWithin(element) {
		 // Initialize
		  var iCaretPos = 0;

		  // IE Support
		  if (document.selection) {

		    // Set focus on the element
			element.focus ();

		    // To get cursor position, get empty selection range
		    var oSel = document.selection.createRange ();

		    // Move selection start to 0 position
		    oSel.moveStart ('character', -element.val().length);

		    // The caret position is selection length
		    iCaretPos = oSel.text.length;
		  }

		  // Firefox support
		  else if (element.selectionStart || element.selectionStart == '0')
		    iCaretPos = element.selectionStart;

		  // Return results
		  return (iCaretPos);
	}
	///-------------------------------------------///
	/// Mask Parent function
	///-------------------------------------------///
	function mask() {

	};
	///-------------------------------------------///
	/// Mask Methods
	///-------------------------------------------///
	mask.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Currency
		init: function () {
			console.log('Forms input mask init');
		},
		//Get key up
		focus: function (event) {
			//Get input
			activeInput = fnb.hyperion.$(event.currentTarget);
			//Get caret start position
			var start = getCaretCharacterOffsetWithin(activeInput);

			//Test if input has been initialized
			if(!activeInput.numFormat){
				//Get active input
				activeInput.numFormat = {init:0,counter:0};
				//Get if is number
				activeInput.numFormat.isNumber = (activeInput.attr('data-isNumber')=='true') ? true : false;
				//Get decimals
				activeInput.numFormat.decimals = activeInput.attr('data-decimals') ? parseInt(activeInput.attr('data-decimals')) : decimals;
				//Get decimal point value
				activeInput.numFormat.decimalsPoints = activeInput.attr('data-decimalsPoints') ? activeInput.attr('data-decimalsPoints') : decimalsPoints;
				//Get thousands seperator value
				activeInput.numFormat.thousandsSeperator = activeInput.attr('data-thousandsSeperator') ? activeInput.attr('data-thousandsSeperator') : thousandsSeperator;
				//Work out the unicode character for the decimal placeholder.
				activeInput.numFormat.unicodeDecimal = ('\\u'+('0000'+(activeInput.numFormat.decimalsPoints.charCodeAt(0).toString(16))).slice(-4)),
				activeInput.numFormat.regexDecimalNumber = new RegExp('[^'+activeInput.numFormat.unicodeDecimal+'0-9]','g'),
				activeInput.numFormat.regexDecimal = new RegExp(activeInput.numFormat.unicodeDecimal,'g');
			}

		},
		//Get key down
		keyDown: function (event) {
			//Test if key is allowed
			if(!allowKeyPress(event)) return false;
			//Get active input
			var currentInput = fnb.hyperion.$(event.currentTarget);
			var startfg = getCaretCharacterOffsetWithin(currentInput);

			//Get input data
			var data = currentInput.numFormat;
			//Get keycode
			var code = (event.keyCode ? event.keyCode : event.which);

			//Get character from code
			var character = String.fromCharCode(code);
			//Get caret start position
			var start = getSelection(currentInput,'start');

			//Get caret end position
			var end = getSelection(currentInput,'end');

			//Declare new val variable
			var val	= '';
			//Declare new setPos variable
			var setPos = false;
			//Everything is selected or its emptry
			if(start == 0 && end == currentInput.val().length||value(currentInput) == 0)
			{

				if(code === 8)
				{

					//Blank out the input
					start = end = 1;
					currentInput.val('');
					//Reset the cursor position.
					data.init = (data.decimals>0?-1:0);
					data.counter = (data.decimals>0?-(data.decimals+1):0);
					setSelectionRange(0,0);
				}
				else if(character === data.decimalsPoints)
				{

					start = end = 1;
					currentInput.val('0'+ data.decimalsPoints + (new Array(data.decimals+1).join('0')));
					//Reset the cursor position.
					data.init = (data.decimals>0?1:0);
					data.counter = (data.decimals>0?-(data.decimals+1):0);
				}
				else if(currentInput.val().length === 0 )
				{

					//Reset the cursor position.
					data.init = (data.decimals>0?-1:0);
					data.counter = (data.decimals>0?-(data.decimals):0);
				}
			}
			//Reset the caret position based on the users selection.
			else
			{

				data.counter = end-currentInput.val().length;
			}
			//If the start position is before the decimal point,
			if(data.decimals > 0 && character == data.decimalsPoints && start == currentInput.val().length-data.decimals-1 )
			{

				data.counter++;
				data.init = Math.max(0,data.init);
	        	//Prevent default event
				event.preventDefault();
				// Set the selection position.
				setPos = currentInput.val().length+data.counter;
			}
			//If the user is just typing the decimal place
			else if(character == data.decimalsPoints)
			{
				data.init = Math.max(0,data.init);
	        	//Prevent default event
				event.preventDefault();
			}
			//If hitting the delete key, and the cursor is behind a decimal place,
			else if(data.decimals > 0 && code == 8 && start == currentInput.val().length-data.decimals)
			{
	        	//Prevent default event
				event.preventDefault();
				data.counter--;
				// Set the selection position.
				setPos = currentInput.val().length+data.counter;
			}
			//If hitting the delete key, and the cursor is to the right of the decimal
			else if(data.decimals > 0 && code == 8 && start > currentInput.val().length-data.decimals)
			{
				if(currentInput.val('')) return;
				// If the character preceeding is not already a 0,
				if(currentInput.val().slice(start-1, start) != '0' )
				{
					val = currentInput.val().slice(0, start-1) + '0' + currentInput.val().slice(start);
					value(currentInput,val.replace(data.regexDecimalNumber,'').replace(data.regexDecimal,data.decimalsPoints));
				}
	        	//Prevent default event
				event.preventDefault();
				data.counter--;
				// Set the selection position.
				setPos = currentInput.val().length+data.counter;
			}
			// Step over thousand seperator
			else if(code == 8 && currentInput.val().slice(start-1, start) == data.thousandsSeperator)
			{
	        	//Prevent default event
				event.preventDefault();
				data.counter--;
				// Set the selection position.
				setPos = currentInput.val().length+data.counter;
			}
			// If the caret is to the right of the decimal place, and the user is entering a number, remove the following character before putting in the new one. 
			else if(data.decimals > 0 && start == end && currentInput.val().length>data.decimals+1 &&start>currentInput.val().length-data.decimals-1 && isFinite(+character) &&!event.metaKey && !event.ctrlKey && !event.altKey && character.length === 1)
			{
				// If the character preceeding is not already a 0,
				// replace it with one.
				if( end === currentInput.val().length )
				{
					val = currentInput.val().slice(0, start-1);
				}
				else
				{
					val = currentInput.val().slice(0, start)+currentInput.val().slice(start+1);
				}
				// Reset the position.
				currentInput.val(val);
				setPos = start;
			}
			// If we need to re-position the characters.
			if(setPos!==false)
			{
				setSelectionRange(currentInput,setPos,setPos);
			}
			// Store the data on the element.
			currentInput.numFormat = data;
		},
		//Get key up
		keyUp: function (event) {
			//Get active input
			var currentInput = fnb.hyperion.$(event.currentTarget);
			//Get input data
			var data = currentInput.numFormat;
			//Get keycode
			var code = (event.keyCode ? event.keyCode : event.which);
			//Get caret start position
			var start = getSelection(currentInput,'start');
			//Declare new setPos variable
			var setPos;
			//Re-format the input
			value(currentInput,value(currentInput));
			//Test for decimals
			if(data.decimals>0)
			{
				//If we haven't marked this item as 'initialised'
				if(data.init<1)
				{
					start = currentInput.val().length-data.decimals-( data.init < 0 ? 1 : 0 );
					data.counter = start-currentInput.val().length;
					data.init = 1;
					
					currentInput.numFormat = data;
				}
				//Increase the cursor position if the caret is to the right
				else if( start > currentInput.val().length-data.decimals && code != 8 ||code == 39)
				{

					data.counter++;
					// Store the data
					currentInput.numFormat = data;
				}else if(code == 37){
					data.counter--;
					// Store the data
					currentInput.numFormat = data;
				}
			}
			//Calculate new position
			setPos = currentInput.val().length+data.counter;
			//Set the selection position.
			setSelectionRange(currentInput,setPos,setPos);
		},
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.forms.input.mask = {};
		}
	};
	//Namespace forms.input.mask
	fnb.namespace('forms.input.mask', mask, true);
	//Expose format
	fnb.hyperion.forms.input.mask.value = value;
})();