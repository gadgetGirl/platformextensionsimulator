///-------------------------------------------///
/// developer: Donovan
///
/// Input Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click keypress keyup keydown blur focusout', selector: '.input', handler: 'fnb.hyperion.forms.input.delegate(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    	
    };
    //Init input modules
    function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.forms.input) {
    		if(fnb.hyperion.forms.input[module].autoInit){
    			fnb.hyperion.forms.input[module].init();
			}
    	}
    };
    //Set input mask
    function setMask(target) {
    	//Get input value
    	var value = target.val();
    	//Get mask value
    	var maskValue = target.attr('data-placeholder');
    	//Test if mask value is = nothing
    	if(value==''){
    		//Set data-mask attribute
    		target.attr('data-placeholderIs','true');
    		//Set input value
    		target.val(maskValue);
    	}
    };
    //Clear input mask
    function clearMask(target) {
    	//Get input value
    	var value = target.val();
    	//Get mask value
    	var maskValue = target.attr('data-placeholder');
    	//Test if mask value is = to value
    	if(value==maskValue){
    		//Set data-mask attribute
    		target.attr('data-placeholderIs','false');
    		//Clear input value
    		target.val("");
    	}
    };
	///-------------------------------------------///
	/// Input Parent function
	///-------------------------------------------///
	function input() {

	};
	///-------------------------------------------///
	/// Input Methods
	///-------------------------------------------///
	input.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Array with list of always valid keycodes e.g Del
		validKeyCodes: [8,9,35,36,37,39,46],
		//Array with list of Numbers keycodes
		numberKeyCodes: [110,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105],
		//Array with list of Valid special characters
		specialCharKeyCodes: [190],
		//Array with list of Valid keycodes when the ctrl key is down
		ctrlDownKeyCodes: [67,86,88],
		//Init FNB Input
    	init: function () {
    		console.log('Forms Input init');
    		//Bind innput events
    		bindEvents();
    		//Init child modules
    		initModules();
        },
        //Bind event for current object
        delegate: function (event) {
        	//Valid event flag
        	var validEvent = true;
        	//Get the current event type
        	var eventType = event.type;
        	//Get target
        	var target = event.target || event.srcElement;
        	//Get keycode
    		var keyCode = event.charCode||event.keyCode||event.which;
    		//Reset event type
    		eventType = (eventType=="keyup"&&keyCode==9) ? "focusin" : eventType;
        	//Select target
        	target = fnb.hyperion.$(target);
        	//Get current target type of input
        	var targetType = target.attr('data-type');
        	//Get current target type of input
        	var targetHasPlaceholder = target.attr('data-placeholderIs');
        	//Switch input type
        	switch(targetType)
			{
				case 'masked':
					//Switch event type
					switch(eventType)
					{
						case  'click':
							//validEvent = fnb.hyperion.forms.input.mask.focus(event);
						break;
						case 'keypress':
							
						break;
						case  'keyup':
							//validEvent = fnb.hyperion.forms.input.mask.keyUp(event);
						break;
						case  'keydown':
							//validEvent = fnb.hyperion.forms.input.mask.keyDown(event);
							validEvent = fnb.hyperion.forms.input.number.keyDown(event);
						break;
					}
					
				break;
				case  'number':
					//Switch event type
					switch(eventType)
					{
						case 'keypress':
							
						break;
						case  'keyup':
							
						break;
						case  'keydown':
							validEvent = fnb.hyperion.forms.input.number.keyDown(event);
						break;
						case  'click':
							
						break;
						case  'blur':
							
						break;
					}
				break;
			};

			//Do focus events
        	switch(eventType)
			{
				case  'focusin':
				case  'click':
					
					if(targetHasPlaceholder=="true") clearMask(target); return true;
					
				break;
				case  'focusout':
				case  'blur':

					if(targetHasPlaceholder=="false") setMask(target);
					
				break;
			}
        	
			if(!validEvent) event.preventDefault();
        	
        },
        //Get object current value
        getValue: function () {
        	
        },
        //Set object current value
        setValue: function () {

        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.input = {};
        }
	};

	//Namespace input
	fnb.namespace('forms.input', input, true);

})();