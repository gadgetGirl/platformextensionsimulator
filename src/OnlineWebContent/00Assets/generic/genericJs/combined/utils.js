///-------------------------------------------///
/// developer: Donovan
///
/// Main Utils Object
///-------------------------------------------///
(function() {
    //Init all utils prototype modules
	function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.utils) {
    		if(fnb.hyperion.utils[module].autoInit){
				fnb.hyperion.utils[module].init();
			}
    	}
    };
	//FNB Utils 
	function utils() {

	};
	//FNB Utils methods
	utils.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Utils
    	init: function () {
    		console.log('Utils init');
    		initModules();
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils = {};
        }
	};

	//Namespace utils
	fnb.namespace('utils', utils, true);

})();
///-------------------------------------------///
/// developer: Mike Stott
///
/// Action Menu Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="actionMenuBttLabelWrapper"]', handler: 'fnb.hyperion.controller.raiseEvent("showActionMenu");', preventDefault: true},
    	{type: 'frame', listener: document, events:'click', selector: '[data-role="actionMenuGridCol"] a', handler: 'fnb.hyperion.utils.actionMenu.navigateToUrl(event)', preventDefault: true}];
    	
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Action Menu Parent function
	///-------------------------------------------///
	function actionMenu() {
		
	};
	///-------------------------------------------///
	/// Action Menu Methods
	///-------------------------------------------///
	actionMenu.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Is Action Menu currently active
		active:false,
		//Does the actionmenu have content
		hasMenu:false,
		//Does the actionmenu have content
		buttonVisible:false,
		//Does the actionmenu have content
		actionMenu:false,
		//Init Action Menu
    	init: function () {
    		console.log('Utils Action Menu init');
    		//Bind events for action menu
    		bindEvents();
        },
		//Show Action Menu
		show : function() {
			//If Action Menu is not active display it
			if(!this.active){
				//Update flag
				this.active = true;
				//Change label wrapper Styling
				fnb.hyperion.controller.actioMenuButtonLabelWrapper.parent().addClass('actionMenuBttLabelWrapperExpanded');
				//Get page scroll position to reset later
				fnb.hyperion.controller.getScrollPosition(document.body);
				//Check for page wrapper
				var pageWrapper = fnb.hyperion.controller.pageContentContainerElement.find('.pageWrapper');
				//Change pageContentContainerElement visibility data attribute
				if(pageWrapper.length()>0) pageWrapper.hide();
				//Scroll window to top
				fnb.hyperion.controller.scrollTo(0,0);
				//Change actionMenuElement visibility data attribute
				this.actionMenu.show();
				//Select Url Wrapper
				var actionMenuColumns = this.actionMenu.find('.actionMenuCol');
				//Select Url Wrapper
				var urlWrapper = this.actionMenu.find('.actionMenuUrlWrapper');
				//Developer: Donovan for old frame
				if(urlWrapper.length()>0) urlWrapper.removeClass('visibleContent');
				if(actionMenuColumns.length()>0) actionMenuColumns.removeClass('displayNone');

			}else{
				//Hide action menu
				fnb.hyperion.controller.raiseEvent("hideActionMenu");
				//Prevent default
				return false;
			}
		},
		//Hide Action Menu
		hide : function() {
			//If Action Menu is active hide it
			if(this.active){
				//Update flag
				this.active = false;
				//Change label wrapper Styling
				fnb.hyperion.controller.actioMenuButtonLabelWrapper.parent().removeClass('actionMenuBttLabelWrapperExpanded');
				//Change actionMenuElement visibility data attribute
				this.actionMenu.hide();
				//Check for page wrapper
				var pageWrapper = fnb.hyperion.controller.pageContentContainerElement.find('.pageWrapper');
				//Change pageContentContainerElement visibility data attribute
				if(pageWrapper.length()>0) pageWrapper.show();
				//Reset page scroll position 
				fnb.hyperion.controller.setScrollPosition(document.body);
			}
		},
		//Show Action Menu button
		showButton : function() {
			//Test if button is not visible and action menu has content
			if(this.hasActionMenu()&&!this.buttonVisible){
				//Set button visible flag
				this.buttonVisible = true;
				//Show action menu button
				fnb.hyperion.controller.actioMenuButtonLabelWrapper.removeClass('HoffScreen');
			}
		},
		//Hide Action Menu button
		hideButton : function() {
			//Test if button is visible
			if(this.buttonVisible){
				//Set button visible flag
				this.buttonVisible = false;
				//Hide action menu button
				fnb.hyperion.controller.actioMenuButtonLabelWrapper.addClass('HoffScreen');
			}
		},
		//Navigate to url
		navigateToUrl : function(event) {
			//Grab the url off the element
			var url = event.target.attr('href');
			if(url && url != '#') fnb.hyperion.controller.raiseEvent("loadPage",{url: url});
		},
		//Show Action Menu button
		hasActionMenu : function() {
        	//Select action menu
        	this.actionMenu = fnb.hyperion.$("#actionMenu");
			//Test if actionmenu was found
			if(this.actionMenu.length()>0){
				return true;
			}
			
			return false;
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.actionMenu = {};
        }
	};

	//Namespace actionMenu
	fnb.namespace('utils.actionMenu', actionMenu, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Ajax result fragment builder
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax result fragment builder Parent function
	///-------------------------------------------///
	function content() {

	};
	///-------------------------------------------///
	/// Ajax result fragment builder Methods
	///-------------------------------------------///
	content.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init fragment utility
    	init: function () {
    		console.log('Utils fragment init');
        },
        //Create fragment method
        set: function(loadObj,css){
        	//Test if target exists
        	if(loadObj.target.length()>0){
        		//Create html container
            	var htmlContainer = document.createElement('div');

    			//Add ajax returned data to a temporary container
            	htmlContainer.innerHTML = css.html;

    			//Create  a documentFragment.
    			var fragment = document.createDocumentFragment();
    			
    			//Add temp container to fragment
    			fragment.appendChild(htmlContainer);
    			
    			//Create wrapper for contents
    			var fragmentTarget = fragment.childNodes[0];

    			//Add styles to fragment
    			fnb.hyperion.load.styles(fragmentTarget, css.styles);
    			
            	//Split children
            	var fragmentTargetChildren = [].slice.call(fragmentTarget.children);
            	
            	//Clear target
    			loadObj.target.html('');

    			//Loop and add children
    			for(var i=0; i<fragmentTargetChildren.length; i++) {
    				loadObj.target.add(fragmentTargetChildren[i]);
    			}
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.content = {};
        }
	};

	//Namespace ajax getParameters
	fnb.namespace('utils.ajax.content', content, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Ajax css utility
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax css utility Parent function
	///-------------------------------------------///
	function css() {

	};
	///-------------------------------------------///
	/// Ajax getcss utility Methods
	///-------------------------------------------///
	css.prototype = {
		//Var for storing list if link in head
		headCss: [],
		//Var for storing amount of pagescripts in head
		cssCounter: 0,
		//Var for frame to auto initialize module
		autoInit: false,
		//Init Ajax filter utility
    	init: function () {
    		console.log('Ajax getcss init');
        },
        //Get css from filterAjaxResponse to append later
        getCss: function(html){
        	// Array which will store the styles
	    	var links = new Array();
        	//Find all script tags in the filtered html and store them in array
        	var targetCss = html.match(/(<link\b.+href=")(?!http)([^"]*)(".*>)/gi) != null ? html.match(/(<link\b.+href=")(?!http)([^"]*)(".*>)/gi) : [] ;
        	//Add the filtered HTML back into the ajax target but remove all script tags	
        	html = html.replace(/(<link\b.+href=")(?!http)([^"]*)(".*>)/gi,'');
        	
        	for (var i=0;i<targetCss.length;i++)	{
        		var url = targetCss[i].match(/href="[\s\S]*?"/g);
        		//Script has src
				var rawUrl = url[0].replace('href="','').replace('"','');
				//Add script id to headscripts
				links.push(rawUrl);
        	}
			//Return formatted loadObj
			return {css: links, cleanHtml: html};
        },
        //Collect style tags
        getStyles: function(html){
	        // Array which will store the styles
	    	var styles = new Array();
			//Loop html and strip out tags
			while(html.indexOf("<style") > -1 || html.indexOf("</style") > -1) {
				var startStyleTag = html.indexOf("<style");
				var endStyleTag = html.indexOf(">", startStyleTag);
				var closedStartStyleTag = html.indexOf("</style", startStyleTag);
				var closedEndStyleTag = html.indexOf(">", closedStartStyleTag);
				// Add to styles array
				styles.push(html.substring(endStyleTag+1, closedStartStyleTag));
				// Strip from strcode
			    html = html.substring(0, startStyleTag) + html.substring(closedEndStyleTag+1);
			}
			//Return formatted loadObj
			return {styles: styles, cleanHtml: html};
        },
        //Set css to page
        setCss: function (css, handler) {
        	//Create script callback
        	function callback(){
        		return handler();
        	}
        	//Test for css
        	if(css.length===0){
        		callback();
        		return;
        	}
        	//Var loaded css counter
        	var loadedCss = 0;
        	//Loop through the css array created above and create a script tag object for each one
			for (var i=0;i<css.length;i++)	{
				//Get current css count in head
				var cssHeadCount = fnb.hyperion.controller.head.find('=link').length();
				//Do head count method flag
				var doCssHeadCount = false;
				//Increase scripts counter
				this.cssCounter++;
				//Create a script object					
				var importCss = document.createElement('link');
				//Set the type on the script object
				importCss.type ="text/css";
				//Set the rel on the script object
				importCss.rel ="stylesheet";
				//Add a class to the link tag
				importCss.className = "pageCSS";
				//Create script id
				var newId = "pageCSS"+this.cssCounter;
				//Add a id to the script tag
				importCss.setAttribute("id", newId);
				//Add script id to headscripts
				this.headCss.push(newId);
				//Check the stored script for a src attribute
				var url = css[i].match(/href="[\s\S]*?"/g);
				//Add script event listeners
				if(importCss.readyState){//IE
					importCss.onreadystatechange = function(){
			            if (importCss.readyState == "loaded"||importCss.readyState == "complete"){
			            	importCss.onreadystatechange = null;
			            	//Increase loaded script count
			                loadedCss++;
			                //Test if all css have been loaded
			                if(loadedCss==css.length){
			            		callback();
							}
			            }
			        };
			    }else{//Others
			    	doCssHeadCount = true;
			    }

				//Test for url
				if(url) {
					//Script has src. Add the src to the newly created script object
					var rawUrl = url[0].replace('href="','').replace('"','');
					importCss.href = rawUrl;					
				}
				//Add and execute script tags to the ajax target
				fnb.hyperion.controller.head.add(importCss);
				//Check if headcount needs to be done
				if(doCssHeadCount){
					//Create headcount timer
					var cssHeadCountInterval = setInterval(function() {
						//Check if css has been added
					    if (document.styleSheets.length > cssHeadCount) {
					    	//Increase css coiunter
					    	loadedCss++;
					    	//Check if all scripts have been loaded and trigger callback
					    	if(loadedCss==css.length){
				        		callback();
							}
					    	//Clear cssHeadCountInterval
					    	clearInterval(cssHeadCountInterval);
					    }
					 }, 10);
				}
				
			}
        },
        //prepend css to element
        prependCss: function (element, css) {
        	//Loop css and prepend links
        	for (var i=0;i<css.length;i++)	{
        		//Create a script object					
    			var importCss = document.createElement('link');
    			//Set the type on the script object
    			importCss.type ="text/css";
    			//Set the rel on the script object
    			importCss.rel ="stylesheet";
				//Check the stored script for a src attribute
				var url = css[i].match(/href="[\s\S]*?"/g);
				//Test for url
				if(url) {
					//Script has src. Add the src to the newly created script object
					var rawUrl = url[0].replace('href="','').replace('"','');
					importCss.href = rawUrl;					
				}
				//Prepend element
				element.insertBefore(importCss,(element.hasChildNodes()) ? element.childNodes[0] : null);
        	};
        },
        //prepend styles to element
        prependStyles: function (element, styles) {
        	// Loop through every script collected and eval it
			for(var i=0; i<styles.length; i++) {
				//Craete new style tag
				var style = document.createElement('style');
				//Add type
				style.type = 'text/css';
				//Add styles
				if (style.styleSheet){
					style.styleSheet.cssText = styles[i];
				} else {
					style.appendChild(document.createTextNode(styles[i]));
				}
				//Prepend element
				element.insertBefore(style,(element.hasChildNodes()) ? element.childNodes[0] : null);
			};
        },
        //Remove css from head
        clear: function () {
        	//Test if there are css in the head
        	if(this.cssCounter>0){
            	//Loop scripts and remove
            	this.headCss.forEach(function(id){
            		var currentLink = fnb.hyperion.$('#'+id).elem;
            		fnb.hyperion.controller.head.remove(currentLink);
            	});
            	//Reset head list
            	this.headCss = [];
        		//Reset script counter
            	this.cssCounter = 0;
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.css = {};
        }
	};

	//Namespace ajax
	fnb.namespace('utils.ajax.css', css, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Ajax filter utility
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax filter utility Parent function
	///-------------------------------------------///
	function filter() {

	};
	///-------------------------------------------///
	/// Ajax filter utility Methods
	///-------------------------------------------///
	filter.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init Ajax filter utility
    	init: function () {
    		console.log('Ajax filter init');
        },
        //Filter response.
        get: function(target,data,filter){
        	//Setup filter content
        	var filterContent = (filter) ? filter : target;
        	//Page title var
        	var pageTitle = "";
        	//Page category var
        	var pageCategory = "";
        	//Filter var
        	var filteredContent = "";
        	//Test for filter
        	if(filterContent){
               	//Create temp container for ajax result
            	var htmlInDiv = document.createElement('div');
    			//Add ajax returned data to a temporary container
    			htmlInDiv.innerHTML = data;
    			//Create  a documentFragment. This is a light weight DOM object which allows DOM manipulation methods to be executed on the ajax response
    			var fragment = document.createDocumentFragment();
    			//Add temp container to fragment
    			fragment.appendChild(htmlInDiv);
    			//Get the title tag
            	var title = fragment.querySelector('title');
            	//Get page title
            	pageTitle = (title) ? title.innerHTML : '';
            	//Get the body tag
            	var category = fragment.querySelector('[data-category]');
            	//Get page category
            	var pageCategory = (category) ? fnb.hyperion.$(category).attr("data-category") : '';
    			//filter the content...	
    			filteredContent =  fragment.querySelector(filterContent) ? fragment.querySelector(filterContent).innerHTML : data;
        	}else{
        		filteredContent = data;
        	}
			//Return filtered content
			return {content: filteredContent, pageTitle: pageTitle, pageCategory: pageCategory};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.filter = {};
        }
	};

	//Namespace ajax
	fnb.namespace('utils.ajax.filter', filter, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Ajax test response for otp
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax test response for otp Parent function
	///-------------------------------------------///
	function otp() {

	};
	///-------------------------------------------///
	/// Ajax test response for otp Methods
	///-------------------------------------------///
	otp.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Var for otp primary response
		primaryResponse: '',
		//Var for otp secondary response
		secondaryResponse: '',
		//Init Otp utility
    	init: function () {
    		console.log('Utils OTP init');
        },
        //Test xhr for otp
        test: function(loadObj){
        	
        	//Get xhr
        	var xhr = loadObj.xhr;
        	
        	//Get screen type
        	var screenType = xhr.getResponseHeader("SCREEN_TYPE");
        	
        	//Test screen type
        	if(screenType!=null && screenType=="OTP"){
        		
        		//Parse response if OTP
        		var parsedResponse = JSON.parse(loadObj.data);
        		
        		//Store primary responses
        		this.primaryResponse = parsedResponse.otpPrimary;
        		
        		//Store secondary response
        		this.secondaryResponse = parsedResponse.otpSecondary;
        		
        		//Notify controller to raise otp
        		fnb.hyperion.controller.otp();
        		
        		return true;
        	};

    		return false;
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.otp = {};
        }
	};

	//Namespace ajax getParameters
	fnb.namespace('utils.ajax.otp', otp, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Get target parameters
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Get ajax target parameters Parent function
	///-------------------------------------------///
	function parameters() {

	};
	///-------------------------------------------///
	/// Get ajax target parameters Methods
	///-------------------------------------------///
	parameters.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init parameters utility
    	init: function () {
    		console.log('Utils parameters init');
        },
        //Get target parameters
        get: function(loadObj){
        	//Set default action
			var dataAction = '';
			
			//Get data target
			var dataTargetArray = loadObj.dataTarget.split(',');
			
			//Declare datatarget var
			var dataTarget = "";
			
			//Loop data target array
			dataTargetArray.each(function(element){
				
				//Select target
				dataTarget = fnb.hyperion.$(element);
				
				//Test data length
				if(dataTarget.length() > 0){
					
					//Serialize data target
					var dataTargetParams = fnb.hyperion.controller.serialize(dataTarget);
	
					//Get target data
					loadObj.params = (loadObj.params) ? loadObj.params + '&' + dataTargetParams : dataTargetParams;
	
					//Get dataTarget action
					if(dataTarget.attr('data-action')){
						action = dataTarget.attr('data-action');
						dataAction = action;
					};
					
					//Get dataTarget nav
					if(dataTarget.attr('data-nav')){
						loadObj.params = loadObj.params+"&nav="+dataTarget.attr('data-nav');
					};
					
					//Get target method
					if(dataTarget.attr('data-method')){
						loadObj.type = dataTarget.attr('data-method');
					};
					
				}
				
			});

			//Switch url to action
			if(dataAction!=""){
				loadObj.url = dataAction;
			}
			
			loadObj = this.getUrlParameters(loadObj);
			
			//Return appended loadObj
			return loadObj;
        },
        //Append url with backend parameters
        getUrlParameters: function (loadObj) {
        	//Test if target exists
        	if(loadObj.target.length()>0){
        		//Add target div for the backend
    			if (typeof loadObj.url != 'undefined') {
    				if(loadObj.url.indexOf("?")>-1){
    					loadObj.url += "&targetDiv=" + loadObj.target.attr("id");	
    				}else{
    					loadObj.url += "?targetDiv=" + loadObj.target.attr("id");
    				}
    			}else{
    				loadObj.url = loadObj.params['alternateUrl'];
    				loadObj.url += "?targetDiv=" + loadObj.target.attr("id");
    			}
        	}
        				
			return loadObj;
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.parameters = {};
        }
	};

	//Namespace ajax getParameters
	fnb.namespace('utils.ajax.parameters', parameters, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Ajax scripts utility
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax scripts utility Parent function
	///-------------------------------------------///
	function scripts() {

	};
	///-------------------------------------------///
	/// Ajax getScripts utility Methods
	///-------------------------------------------///
	scripts.prototype = {
		//Var for storing list if scripts in head
		headScripts: [],
		//Var for storing amount of pagescripts in head
		scriptsCounter: 0,
		//Var for frame to auto initialize module
		autoInit: false,
		//Init Ajax filter utility
    	init: function () {
    		console.log('Ajax getScripts init');
        },
        //Get scripts from filterAjaxResponse to append later
        getScripts: function(html){
        	//Find all script tags in the filtered html and store them in array
        	var targetScripts = html.match(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi) != null ? html.match(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi) : [] ;
        	//Add the filtered HTML back into the ajax target but remove all script tags	
        	html = html.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,'');
			//Return formatted loadObj
			return {scripts: targetScripts, cleanHtml: html};
        },
        //Get scripts contents
        getScriptsContents: function(html,container){
        	// Array which will store the styles
	    	var scripts = new Array();
	    	// Array which will store the styles
	    	var importScripts = new Array();
			//Loop html and strip out tags
			while(html.indexOf("<script") > -1 || html.indexOf("</script") > -1) {
				var startScriptTag = html.indexOf("<script");
				var endScriptTag = html.indexOf(">", startScriptTag);
				var closedStartScriptTag = html.indexOf("</script", startScriptTag);
				var closedEndScriptTag = html.indexOf(">", closedStartScriptTag);
				//Create string to test if import
				var isImport = html.substring(startScriptTag, closedEndScriptTag+1);
				//look for url
				var url = isImport.match(/src="[\s\S]*?"/g);
				if(url) {
					//Script has src
					var rawUrl = url[0].replace('src="','').replace('"','');
					//Add script id to headscripts
					importScripts.push(rawUrl);
				}else{
					// Add to styles array
					scripts.push(html.substring(endScriptTag+1, closedStartScriptTag));
				}

				// Strip from strcode
			    html = html.substring(0, startScriptTag) + html.substring(closedEndScriptTag+1);
			}
			//Return formatted loadObj
			return {scripts: scripts, cleanHtml: html, importScripts: importScripts};
        },
        //Set scripts to page
        setScripts: function (scripts, handler) {
        	//Create script callback
        	function callback(){
        		return handler();
        	}
        	//Test for scripts
        	if(scripts.length===0){
        		callback();
        		return;
        	}
        	//Var loaded scripts counter
        	var loadedScripts = 0;
        	//Loop through the scripts array created above and create a script tag object for each one
			for (var i=0;i<scripts.length;i++)	{
				//Increase scripts counter
				this.scriptsCounter++;
				//Create a script object					
				var importScript = document.createElement('script');
				//Set the type on the script object
				importScript.type ="text/javascript";
				//Add a class to the script tag
				importScript.className = "pageScript";
				//Create script id
				var newId = "pageScript"+this.scriptsCounter;
				//Add a id to the script tag
				importScript.setAttribute("id", newId);
				//Add script id to headscripts
				this.headScripts.push(newId);
				//Check the stored script for a src attribute
				var url = scripts[i].match(/src="[\s\S]*?"/g);
				//Declare code variable
				var code = undefined;
				//Test for url
				if(url) {
					//Script has src. Add the src to the newly created script object
					var rawUrl = url[0].replace('src="','').replace('"','');
					importScript.src = rawUrl;					
				}else{
					//Script has no src and is an inline script. Add the code to the newly created script object
					code = scripts[i].replace(/<script[^>]*?[^>]*>([\s\S]*?)/gi,'').replace(/<\/script>/gi,'');
				}
				//Add and execute script tags to the ajax target
				fnb.hyperion.controller.head.add(importScript);
				//Inject code if needed
				if(code){
					importScript.text = code;
					//Increase loaded script count
					loadedScripts++;
	                //Test if all scripts have been loaded
					if(loadedScripts==scripts.length){
		        		callback();
					}
				}
				//Add script event listeners
				if(importScript.readyState){//IE
					importScript.onreadystatechange = function(){
			            if (importScript.readyState == "loaded"||importScript.readyState == "complete"){
			            	importScript.onreadystatechange = null;
			            	//Increase loaded script count
			                loadedScripts++;
			                //Test if all scripts have been loaded
			                if(loadedScripts==scripts.length){
			            		callback();
							}
			            }
			        };
			    }else{//Others
			    	importScript.onload = function(){
			    		//Increase loaded script count
			        	loadedScripts++;
		                //Test if all scripts have been loaded
			    		if(loadedScripts==scripts.length){
			        		callback();
						}
			        };
			    }
			}
        },
        //Remove scripts from head
        clear: function () {
        	//Test if there are scripts in the head
        	if(this.scriptsCounter>0){
            	//Loop scripts and remove
            	this.headScripts.forEach(function(id){
            		var currentScript = fnb.hyperion.$('#'+id).elem;
            		fnb.hyperion.controller.head.remove(currentScript);
            	});
            	//Reset head list
            	this.headScripts = [];
        		//Reset script counter
            	this.scriptsCounter = 0;
        	}
        },
        //prepend css to element
        prependJs: function (element, js) {
        	//Loop css and prepend links
        	for (var i=0;i<js.length;i++)	{
				//Prepend element with js
				element.insertBefore(js[i],(element.hasChildNodes()) ? element.childNodes[0] : null);
        	};
        },
        //Eval scripts method
        evalScripts: function (scripts) {
			// Loop through every script collected and eval it
			for(var i=0; i<scripts.length; i++) {
				try {
					//Eval sccript
					eval(scripts[i]);
				}catch(e) {
				    //Script error
					console.log('Error: utils.ajax.scripts: '+e);
				}
			};
        },
        //Eval script method
        evalScript: function (script) {
        	if (typeof(eval(script)) === 'function') {

        	} else {
        		setTimeout(function () {
        			fnb.hyperion.utils.ajax.scripts.evalScript(script);
        		}), 50
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.scripts = {};
        }
	};

	//Namespace ajax
	fnb.namespace('utils.ajax.scripts', scripts, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Ajax test response
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax test response Parent function
	///-------------------------------------------///
	function validResponse() {

	};
	///-------------------------------------------///
	/// Ajax test response Methods
	///-------------------------------------------///
	validResponse.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init fragment utility
    	init: function () {
    		console.log('Utils validResponse init');
        },
        //Create fragment method
        test: function(loadObj){
        	//Do data tests
        	if(typeof loadObj.data == "string"){
	    		if(loadObj.data.isStringEmpty()){
	    			//Test if errors should be shown
		        	if(loadObj.error){
		        		//Notify Controller to raise Error event
			        	fnb.hyperion.controller.error("No data returned from request.", "validResponse");
			        	
		        	}
	    			
		        	return false;
		    	}
	    	}else{
	    		//Test if errors should be shown
	        	if(loadObj.error){
	        		//Notify Controller to raise Error event
		        	fnb.hyperion.controller.error("No data returned from request.", "validResponse");
		        	
	        	}
    			
	        	return false;
	    	}
        	
        	return true;
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.validResponse = {};
        }
	};

	//Namespace ajax getParameters
	fnb.namespace('utils.ajax.validResponse', validResponse, true);

})();
///-------------------------------------------///
/// developer: Richard 
///
/// comparisonGrid Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="compareItem"]', handler: 'fnb.hyperion.utils.comparisonGrid.toggleHelpText(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
    ///-------------------------------------------///
	/// comparisonGrid Parent function
	///-------------------------------------------///
	function comparisonGrid() {

	};
	///-------------------------------------------///
	/// comparisonGrid Methods
	///-------------------------------------------///
	comparisonGrid.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		compareItems: "",
		//Init FNB comparisonGrid
    	init: function () {
    		this.compareItems = fnb.hyperion.$("#compareItems"),
    		bindEvents();
        },
        toggleHelpText: function (event) {
        	//Hide all popup messages
        	this.compareItems.find(".popupMessageAnchor").hide();
        	//Get row clicked
        	fnb.hyperion.$(event.currentTarget).find(".popupMessageAnchor").show();
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.comparisonGrid = {};
        }
	};

	//Namespace comparisonGrid
	fnb.namespace('utils.comparisonGrid', comparisonGrid, true);

})();
///-------------------------------------------///
/// developer: Mike
///
/// Design Grid Object
///-------------------------------------------///
(function() {
	function bindEvents() {
    	//List of events for this module
    	var events = [
    	{type: 'frame', listener: document, events:'click', selector: '[data-role="toggleGridBtt"]', handler: 'fnb.hyperion.utils.designGrid.toggleState(event)'},
    	{type: 'frame', listener: document, events:'click', selector: 'a[data-role="subMenu"]', handler: 'fnb.hyperion.utils.designGrid.toggleState(event)'}];
    	//Append events to actions module
    	//fnb.hyperion.controls.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Design Grid Parent function
	///-------------------------------------------///
	function designGrid() {

	};
	///-------------------------------------------///
	/// Design Grid Methods
	///-------------------------------------------///
	designGrid.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		
		gridParent: '',
		
		init: function () {
    	    //Set designGrid default property
    	   	this.gridParent = fnb.hyperion.$('#grid');
    	   	//Bind Design Grid events
    	   	bindEvents();
		},
        toggleState: function() {
        	
        	var gridParent = this.gridParent;
        	
          	if(gridParent.is(':visible')) {
          		gridParent.hide();
        	}else{
        		gridParent.show();
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.designGrid = {};
        }
	};

	//Namespace delegate
	fnb.namespace('utils.designGrid', designGrid, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Errors Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '#errorClose', handler: 'fnb.hyperion.utils.error.hide(event);', preventDefault: true}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Error Parent function
	///-------------------------------------------///
	function error() {

	};
	///-------------------------------------------///
	/// Error Methods
	///-------------------------------------------///
	error.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Selector for error text wrapper
		errorText: '',
		//Active var
		active: false,
		//Init FNB Errors
    	init: function () {
    		console.log('Utils Error init');
    		//Bind Error events
    		bindEvents();
    		//Select Error text wrapper
    		this.errorText = fnb.hyperion.controller.errorElement.find('.errorInner');
	    },
        //Show error message
        show: function (error) {
        	//Test if error is already active
        	if(!this.active){
        		//Set active flag
        		this.active = true;
        		//Test for error object
    			if(typeof error != "string"){
    				var errorsString = "";
    				//Loop errors
    				for(var i=0;i<error.errors.length;i++) {
    					//Build error string
    					errorsString = errorsString + error.errors[i].error;
    				};
    				//Setup error object for templating
    				var errorObject = {'templateName': 'errorMessage', templateData: {message: error.message , errors: errorsString}};
    				//Get html
    				error = fnb.hyperion.controller.getHtmlTemplate(errorObject);  
    			}
            	//Set error wrapper visibility
            	fnb.hyperion.controller.errorElement.show();
            	//Set error text
            	this.errorText.html(error);
        	};
        },
        //Hide error message
        hide: function (event) {
        	//Set error wrapper visibility
        	if(this.active){
        		//Set active flag
        		this.active = false;
        		//Hide error element
        		fnb.hyperion.controller.errorElement.hide();
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.error = {};
        }
	};

	//Namespace utils.error
	fnb.namespace('utils.error', error, true);

})();
///-------------------------------------------///
/// developer: Richard
///
/// expandables Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '.expander', handler: 'fnb.hyperion.utils.expandables.expand(event);', preventDefault: true}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// expandables Parent function
	///-------------------------------------------///
	function expandables() {

	};
	///-------------------------------------------///
	/// expandables Methods
	///-------------------------------------------///
	expandables.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB expandables
		init: function () {
	  		//Bind expandables events
	  		bindEvents();
	      },
	      expand: function (event) {
	  		//Var for the selected item
	  		var item = fnb.hyperion.$(event.currentTarget);
	  		//Get the parent of the bound item
	  		var parent = item.parent();
	  		//toggle expanded class to show and hide the content
	  		parent.toggleClass("expanded");
	      },
	      //Remove current object from dom
	      destroy: function () {
	      	fnb.hyperion.utils.expandables = {};
	      }
	};

	//Namespace utils.expandables
	fnb.namespace('utils.expandables', expandables, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Overlay Object
///-------------------------------------------///
(function() {
	//Check css state and apply styles
	function cssState() {
		//Check whether to open or close
		if(fnb.hyperion.utils.eziPanel.active){
			setTimeout(function(){
				fnb.hyperion.controller.eziElement.find('*[data-type="eziWrapperLeft"]').attr('data-width','60');
				fnb.hyperion.controller.eziElement.find('*[data-type="eziWrapperRight"]').css('right','auto');
			},50);
		}else{
			setTimeout(function(){
				fnb.hyperion.controller.clipOverflow(false);
				fnb.hyperion.controller.eziElement.hide();
				fnb.hyperion.controller.setScrollPosition(document.body);
			},200);
		}
	};
	///-------------------------------------------///
	/// Overlay Parent function
	///-------------------------------------------///
	function eziPanel() {

	};
	///-------------------------------------------///
	/// Overlay Methods
	///-------------------------------------------///
	eziPanel.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Is Ezi Panel currently active
		active:false,
		//Init FNB Ezi Panel
    	init: function () {
    		console.log('Ezi Panel init');
        },
		//Show overlay
		show : function() {
			//If Ezi Panel is not active display it
			if(!this.active){
				//Update flag
				this.active = true;
				fnb.hyperion.controller.getScrollPosition(document.body);
				//Change Ezi Panel data attribute
				fnb.hyperion.controller.clipOverflow(true);
				//Set ezi wrapper visibility
				fnb.hyperion.controller.eziElement.show();
				//Check css state
				cssState();
			}
		},
		//Hide Ezi Panel
		hide : function() {
			//If Ezi Panel is active hide it
			if(this.active){
				//Update flag
				this.active = false;
				//Check css state
				cssState();
				//Change Ezi Panel data attribute
				fnb.hyperion.controller.eziElement.find('*[data-type="eziWrapperLeft"]').attr('data-width','100');
				fnb.hyperion.controller.eziElement.find('*[data-type="eziWrapperRight"]').css('right','-40%');
				//Remove Ezipanel contents
				fnb.hyperion.controller.eziPageContentElement.html("");
			}
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.eziPanel = {};
        }
	};

	//Namespace utils.eziPanel
	fnb.namespace('utils.eziPanel', eziPanel, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Footer Buttons Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Footer Buttons Parent function
	///-------------------------------------------///
	function footer() {

	};
	///-------------------------------------------///
	/// Footer Buttons Methods
	///-------------------------------------------///
	footer.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Var for max footer buttons allowed
		maxButtons: 10,
		//Init FNB Footer Buttons
    	init: function () {
    		console.log('Utils Footer init');
    		//Add resize function
    		fnb.hyperion.controller.addResizeFunction(fnb.hyperion.utils.footer.configFooterButtons);
	    },
        //Hide footer
        hide: function (target) {
        	//Test for target otherwise hide all
    		if(fnb.hyperion.controller.is(target)) {
        			if(target.length()>0) target.parent().hide();
        	}else{
        		if(fnb.hyperion.controller.footerContainer.length()>0){
    				//Loop footer containers hide
    				fnb.hyperion.controller.footerContainer.children().each(function(element){
    					element.hide();
    				});
    			}
        	}
        },
        //Show footer
        show: function (target) {
    		//Test if any buttons are present
        	if(fnb.hyperion.controller.is(target)) {
        		//Test If the target is selected
        		if(target.length()>0){
        			//Show container
            		target.parent().show();
            		//Adjust footer buttons after show
            		this.configFooterButtons();
        		}
        		
        	}
        },
        configFooterButtons: function(){
        	//Test for footer container
        	if (fnb.hyperion.controller.footerContainer.length() > 0) {
	        	//Get footer container width
	        	var footerContainerWidth = fnb.hyperion.controller.footerContainer.outerWidth();
				//Loop footer containers an align buttons if any
				fnb.hyperion.controller.footerContainer.children().each(function(element){
					//Declare margin left var
		        	var footerLeft = 0;
		        	//Select childrenParent
		        	var childrenParent = element.children(0);
		        	//Reset margin left
		        	childrenParent.css('marginLeft','');
		        	//Select children
		        	var children = childrenParent.children();
		        	//Test for footer buttons
		        	if(children.length()>0){
			        	//Loop children
			        	children.each(function(child){
							footerLeft += child.outerWidth();
						});
			        	//Set margin left var
			        	var marginLeft = footerContainerWidth-footerLeft;
			        	
			        	//Set footer position
			        	if(marginLeft>0) childrenParent.css('marginLeft',marginLeft+'px');
		        	}
				});
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.footer = {};
        }
	};

	//Namespace utils.eziPanel
	fnb.namespace('utils.footer', footer, true);

})();
///-------------------------------------------///
/// developer: Richard
///
/// headerButtonMenu Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [	{type: 'frame', listener: document, events:'click', selector: '#ContactButton', handler: 'fnb.hyperion.utils.headerButtonMenu.toggle();'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
    headerButtonMenu = function() {
		
	};
	headerButtonMenu.prototype = {
		//Set autoInit to true
		autoInit: true,
		//Initialize variables
		headerButtonMenuWrapper: '',
		init: function () {
			//Select header button menu
			this.headerButtonMenuWrapper = fnb.hyperion.$("#headerButtonMenu");
			//Bind events
			bindEvents();
		},
		toggle: function () {
			//Test if menu is off screen (hidden)
			if (this.headerButtonMenuWrapper.attr("data-position") != 'offscreen') {
				//If menu is not off screen (hidden) then hide it.
				fnb.hyperion.utils.headerButtonMenu.hide();
			}
			else {
				//If menu is off screen (hidden) then show it.
				fnb.hyperion.utils.headerButtonMenu.show();
			}
		},
		show: function () {
			//Clear header menu position
			this.headerButtonMenuWrapper.attr("data-position","");
		},
		hide: function () {
			this.headerButtonMenuWrapper.attr("data-position","offscreen");
		},
		destroy: function () {
		}
	};
	//Namespace utils.headerButtonMenuWrapper
	fnb.namespace('utils.headerButtonMenu', headerButtonMenu, true);

})();
///-------------------------------------------///
/// developer: Richard
///
/// hyperlink Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="hyperlink"]', handler: 'fnb.hyperion.utils.hyperlink.hyperlinkToUrl(event);', preventDefault: true}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// hyperlink Parent function
	///-------------------------------------------///
	function hyperlink() {
			
	};
	///-------------------------------------------///
	/// hyperlink Buttons Methods
	///-------------------------------------------///
	hyperlink.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB hyperlink
		init: function () {
	  		//Bind hyperlink events
	  		bindEvents();
		},
		hyperlinkToUrl: function (event) {
			//Prevent default event
			event.stopImmediatePropagation();
	  		//Var for the selected item
	  		var item = fnb.hyperion.$(event.currentTarget);
	  		
	  		//Get the url bound to the item
	  		var url = item.attr('href');
	  		//Get button settings
        	var dataSettingsString = item.attr("data-settings");
        	
        	if(dataSettingsString){
        		//Convert settings string to object
            	var dataSettingsObject = JSON.parse(dataSettingsString);
            	//Wrap target in selector
            	if(dataSettingsObject[0].target) dataSettingsObject[0].target = fnb.hyperion.$(dataSettingsObject[0].target);
            	//Get event that needs to be raised
            	var event = dataSettingsObject[0].event;
            	//Raise specified event
            	fnb.hyperion.controller.raiseEvent(event, dataSettingsObject[0]);
        	}else if(url != ''){
	  			//Notify Controller to raise loadUrl event
	  			fnb.hyperion.controller.raiseEvent('loadPage',{url: url,target:fnb.hyperion.controller.pageContentElement});
	  		}else{
    			fnb.hyperion.controller.redirect(url);
    		};

		},
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.utils.hyperlink = {};
		}
	};

	//Namespace utils.hyperlink
	fnb.namespace('utils.hyperlink', hyperlink, true);

})();
///-------------------------------------------///
/// developer: Richard
///
/// login Object
///-------------------------------------------///
(function() {
	//Login form element selectors
	//Var for login form
	var loginForm;
	//Var for login button
	var loginButton;
	//Var for username input
	var usernameInput;
	//Var for username error
	var usernameError;
	//Var for password input
	var passwordInput;
	//Var for password error
	var passwordError;
	//Var mobile test
	var mobileLoginWrapper;

	//Var for active submittion
	var submitting = false;
	//Var for popoup visibility
	var popUpVisible = false;
	//Var for error visibility
	var errorVisible = false;
	//Var for mutilple select timeout
	var timeOut;
	//Select elements that are going to be reused
	function doSelections() {
		//Select mobile login wrapper
		mobileLoginWrapper = fnb.hyperion.$("#mobileLogin");
		//Select login form
		loginForm = fnb.hyperion.$("#LOGIN_FORM");
		//Select the login button
		loginButton = fnb.hyperion.$("#loginButton");
		//Select the username input
		usernameInput = fnb.hyperion.$("#Username");
		//Select the username error wrapper
		usernameError = fnb.hyperion.$("#inlineError_Username");
		//Select the username input
		passwordInput = fnb.hyperion.$("#Password");
		//Select the password error wrapper
		passwordError = fnb.hyperion.$("#inlineError_Password");
    };
	//Do login form submittion
	function submit() {
		//Validate login form
		if(valid()){
			//Set submitting flag
			submitting = true;
			//Submit form
			loginForm.elem.submit();
			//Disable login fields
			enableDisableLoginFields();		
			//Set timeout for submitting
			timeOut = setTimeout(function(){
				//Set submitting flag
				submitting = false;
				//Enable login fields
				enableDisableLoginFields();
			}, 5000);
		}
    };
	//Show mobile login form
    function enableDisableLoginFields() {
    	timeOut = setTimeout(function(){
    		//Test if fields are disabled
    		if(submitting){
    			//Disable login button
    			loginButton.attr('data-state','disabled');
    			loginButton.disabled = true;
    			//Disable Username input
    			usernameInput.disabled = true;
    			//Disable Password input
    			passwordInput.disabled = true;
    		}else{
    			//Enable login button
    			loginButton.attr('data-state','');
    			loginButton.disabled = false;
    			//Enable Username input
    			usernameInput.disabled = false;
    			//Enable Password input
    			passwordInput.disabled = false;
    		}
		}, 300);
	}
    //Validate login form
    function valid() {
    	//Test username field
		if(usernameInput.value == "Username"||usernameInput.value==""){
			fnb.hyperion.utils.login.showHideError('Username',true);
			return false;
		}
		//Test password field
		if(passwordInput.value == "Password"||passwordInput.value==""){
			fnb.hyperion.utils.login.showHideError('Password',true);
			return false;
		}
		
		return true;
	}
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
      	var events = [{type: 'frame', listener: document, events:'click', selector: '#popupLoginButton', handler: 'fnb.hyperion.utils.login.showHideLoginPopup(event);'},
      	              {type: 'frame', listener: document, events:'click', selector: 'div[data-role="blackCloseButton"]', handler: 'fnb.hyperion.utils.login.showHideLoginPopup(event);'},
      	              {type: 'frame', listener: document, events:'click', selector: '#loginButton', handler: 'fnb.hyperion.utils.login.doLogin(event);'},
      	              {type: 'frame', listener: document, events:'keyup', selector: '#loginButton', handler: 'fnb.hyperion.utils.login.doLogin(event);'},
      	              {type: 'frame', listener: document, events:'keyup', selector: '#Username', handler: 'fnb.hyperion.utils.login.doLogin(event);'},
      	              {type: 'frame', listener: document, events:'keyup', selector: '#Password', handler: 'fnb.hyperion.utils.login.doLogin(event);'},
      	              {type: 'frame', listener: document, events:'blur', selector: '#Username', handler: 'fnb.hyperion.utils.login.showHideError(["Username","Password"],false);'},
      	              {type: 'frame', listener: document, events:'blur', selector: '#Password', handler: 'fnb.hyperion.utils.login.showHideError(["Username","Password"],false);'}
      	              ];

      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Login Parent function
	///-------------------------------------------///
	function login() {
		
	};
	///-------------------------------------------///
	/// Login Buttons Methods
	///-------------------------------------------///
	login.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB login
		init: function () {
			console.log('Utils Login init')
			//Do login element selections
			doSelections();
			//Bind login events
			bindEvents();
		},
		//Do login method
		doLogin: function (event) {
			//Test if from keypress
			var doSubmit = (event.type == 'keyup') ? (event.keyCode == 13) ? true: false : true;
			//Test if login in progress and login is valid e.g. from ENTER key
			if(!submitting&&doSubmit){
				submit();
			}
		},
		//Show mobile login form
		showHideLoginPopup: function (event) {
			//Test if popup is visible
			if(popUpVisible){
				//Change pageContentContainerElement visibility data attribute
				fnb.hyperion.controller.pageContentContainerElement.show();
				//Change mobile popup visible state
				mobileLoginWrapper.hide();
				//Set popup visible flag
				popUpVisible = false;
			}else{
				//Change pageContentContainerElement visibility data attribute
				fnb.hyperion.controller.pageContentContainerElement.hide();
				//Change mobile popup visible state
				mobileLoginWrapper.show();
				//Set popup visible flag
				popUpVisible = true;
			}
		},
		//Show hide errors
		showHideError: function (field,show) {
			if(show){
				if(errorVisible===false){
					//Test for type of field
					if(typeof field == 'object'){
						//Loop Object
						field.each(function(item){
							//Switch fields
				        	switch(item) {
								case 'Username':
									usernameError.show();
								break;
								case 'Password':
									passwordError.show();
								break;
								default:

							};
						});
					}else{
						//Switch fields
			        	switch(field) {
							case 'Username':
								usernameError.show();
							break;
							case 'Password':
								passwordError.show();
							break;
							default:

						};
					}
					//Set error visible flag
					errorVisible=true;
				}			
			}else{
				if(errorVisible===true){
					//Test for type of field
					if(typeof field == 'object'){
						//Loop Object
						field.each(function(item){
							//Switch fields
				        	switch(item) {
								case 'Username':
									usernameError.hide();
								break;
								case 'Password':
									passwordError.hide();
								break;
								default:
			
							};
						});
					}else{
						//Switch fields
			        	switch(field) {
							case 'Username':
								usernameError.hide();
							break;
							case 'Password':
								passwordError.hide();
							break;
							default:
		
						};
					}
					//Set error visible flag
					errorVisible=false;
				}
			}
		},
		submitFormFromLink: function(action, form){
			//Select form
			var loginForm = fnb.hyperion.$('#'+form);
			//Get banking utl
			var environment = fnb.hyperion.$("#bankingUrl").val();
			environmentStr = environment.toString();
			//Update form action
			loginForm.attr('action',environmentStr + "/banking/main.jsp");
			//Change form target
			loginForm.attr('target',"_top");
			//Enable dimple input field for submittion
			if (action == 'verifyRemAdv') {
				fnb.hyperion.$("#simple").disabled = false;
			}
			//Update form action
			fnb.hyperion.$('#formAction').val(action);
			//Update nav value
			fnb.hyperion.$('#nav').val(action);
			//Submit form
			loginForm.elem.submit();
		},
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.utils.login = {};
		}
	};

	//Namespace utils.login
	fnb.namespace('utils.login', login, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Notifications Container Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.notificationsClose', handler: 'fnb.hyperion.utils.notifications.hide(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Notifications Container Parent function
	///-------------------------------------------///
	function notifications() {

	};
	///-------------------------------------------///
	/// Notifications Container Buttons Methods
	///-------------------------------------------///
	notifications.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Is notifications currently active
		active:false,
		//Selector for notification text wrapper
		notificationText: '',
		//Selector for notification inner wrapper
		notificationInner: '',
		//Init FNB Errors
    	init: function () {
    		console.log('Utils notifications init');
    		//Bind Error events
    		bindEvents();
    		//Select Error text wrapper
    		this.notificationsText = fnb.hyperion.controller.notificationsElement.find('.notificationsMessages');
    		//Select Error text wrapper
    		this.notificationInner = fnb.hyperion.controller.notificationsElement.find('.notificationsInner');
	    },
        //Show notification message
        show: function (notification) {
        	//If notifications is not active display it
			if(!this.active){
				//Update flag
				this.active = true;
	        	//Set notification wrapper visibility
				this.notificationsText.html('');
	        	fnb.hyperion.controller.notificationsElement.show();
	        	//Set notification text
	        	if(notification) this.notificationsText.html(notification);
				//Change overlay data attribute
				fnb.hyperion.controller.clipOverflow(true);
			}
        },
        //Hide notification message
        hide: function () {
        	//If overlay is active hide it
			if(this.active){
				//Update flag
				this.active = false;
	        	//Set notification wrapper visibility
	        	fnb.hyperion.controller.notificationsElement.hide();
				//Change overlay data attribute
				fnb.hyperion.controller.clipOverflow(false);
				//Clear notifications inner
				fnb.hyperion.utils.notifications.notificationInner.html("");
			}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.notifications = {};
        }
	};

	//Namespace utils.notifications
	fnb.namespace('utils.notifications', notifications, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Overlay Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Overlay Parent function
	///-------------------------------------------///
	function overlay() {

	};
	///-------------------------------------------///
	/// Overlay Methods
	///-------------------------------------------///
	overlay.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Is overlay currently active
		active:false,
		//Init FNB Overlay
    	init: function () {
    		console.log('Utils Overlay init');
        },
		//Show overlay
		show : function() {
			//If overlay is not active display it
			if(!this.active){
				//Set page position
				fnb.hyperion.controller.getScrollPosition(document.body);
				//Update flag
				this.active = true;
				//Change overlay data attribute
				fnb.hyperion.controller.clipOverflow(true);
				//Change overlay visibility
				fnb.hyperion.controller.overlayElement.show();
			}
		},
		//Hide overlay
		hide : function() {
			//If overlay is active hide it
			if(this.active){
				//Update flag
				this.active = false;
				//Change overlay data attribute
				fnb.hyperion.controller.clipOverflow(false);
				//Change overlay visibility
				fnb.hyperion.controller.overlayElement.hide();
				//Change children data attribute
				fnb.hyperion.controller.overlayElement.get(0).hide();
			}
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.overlay = {};
        }
	};

	//Namespace utils.expandables
	fnb.namespace('utils.overlay', overlay, true);

})();
///-------------------------------------------///
/// developer: Richard 
///
/// Page Menu Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="pageMenuItem"]', handler: 'fnb.hyperion.utils.pageMenu.select(event);', preventDefault: true},
	              		{type: 'frame', listener: document, events:'click', selector: '[data-role="pageMenuHeading2"]', handler: 'fnb.hyperion.utils.pageMenu.toggleLevel3(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Page Menu Parent function
	///-------------------------------------------///
	function pageMenu() {

	};
	///-------------------------------------------///
	/// Page Menu Methods
	///-------------------------------------------///
	pageMenu.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,

		//Init FNB Page Menu
    	init: function () {
    		//Bind pageMenu events
    		bindEvents();
        },
        //Show level 3 items
        toggleLevel3: function (event) {
        	var level2Item = fnb.hyperion.$(event.currentTarget);
          	if(level2Item.attr('data-state') ==  'inactive') {
          		level2Item.attr('data-state','active');
        	}else{
        		level2Item.attr('data-state','inactive');
        	}
        },
        select: function (event) {
    		//Var for the selected item
    		var item = fnb.hyperion.$(event.currentTarget);
    		//Get the url bound to the item
    		var url = item.attr('href');
    		if (url != '') {
    			//Notify Controller to raise loadUrl event
    			fnb.hyperion.controller.raiseEvent('loadPage',{url: url,target:fnb.hyperion.controller.pageContentElement});
    		}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.pageMenu = {};
        }
	};

	//Namespace utils.pageMenu
	fnb.namespace('utils.pageMenu', pageMenu, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Progress bar Object Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Progress bar Parent function
	///-------------------------------------------///
	function progress() {

	};
	///-------------------------------------------///
	/// Progress bar Methods
	///-------------------------------------------///
	progress.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//List of all intervals
		intervals: [],
		//Active interval
		currentInterval: '',
		//Initial progress bar speed
		speed: 300,
		//Progress bar lowest value
		minValue: 10,
		//Progress bar highest value
		maxValue: 99,
		//Progress bar Deceleration amount
		decel: .50,
		//Progress bar Deceleration devision
		decelDiv: 2,
		//Progress selector
		loader:'',
		//Progress selector
		loaderProgress:'',
		//mainLoader selector
		mainLoader:'',
		//mainLoaderProgress selector
		mainLoaderProgress:'',
		//eziLoader selector
		eziLoader:'',
		//eziLoaderProgress selector
		eziLoaderProgress:'',
		//Initialize progress bar and do selections
		init: function () {
			//Global Loader Wrapper selector
			this.mainLoader = fnb.hyperion.controller.overlayElement.find('.loader');
			//Global Progress Wrapper selector
			this.mainLoaderProgress = fnb.hyperion.controller.overlayElement.find('.loaderProgress');
			//Global Loader Wrapper selector
			this.eziLoader = fnb.hyperion.controller.eziElement.find('.eziLoader');
			//Global Progress Wrapper selector
			this.eziLoaderProgress = fnb.hyperion.controller.eziElement.find('.eziLoaderProgress');
		},
		//Start new instance of progress bar
		startEziLoader: function (type) {
			//Set ezi loader
			this.loader = this.eziLoader;
			//Set ezi loader
			this.loaderProgress = this.eziLoaderProgress;
			//Start ezi loader
			this.start('eziLoader');
		},
		//Start new instance of progress bar
		start: function (type) {
			//Test if instance already exists and destroy
			if (typeof this.intervals[type] !== 'undefined'||this.intervals[type]!=''){
				this.reset(type);
			}
			//Set loader if no type was specified
			if(!type){
				//Set mainLoader loader
				this.loader = this.mainLoader;
				//Set mainLoader loader
				this.loaderProgress = this.mainLoaderProgress;
			}
			//Show Loader
			this.showHideLoader(true);
			//Make current interval
			this.currentInterval = type;
			//Reset current progress
			var percent = this.minValue;
			//Create new interval
			this.createProgressBar(this.speed,percent);
		},
		//Stop instance of progress bar
		stop: function () {
			//Hide Loader
			this.showHideLoader(false);
			//Reset progress bar
			this.reset(this.currentInterval);
		},
		//Reset active progress bar
		reset: function (type) {
			//Reset progress bar to 0
			this.update(this.minValue);
			//Clear current instance
			clearInterval(this.intervals[type]);
			//Clear current instance
			this.intervals[type] == '';
		},
		//Create new interval for progress bar
		createProgressBar: function (speed,percent) {
			var _this = this;
			this.intervals[this.currentInterval] = setInterval (function (){
				//Calculate percent to increase
				percent = percent + Math.floor(Math.random()*10);
				//Test if max value has been reached then kill otherwise continue
				if(percent >= _this.maxValue){
					_this.clearInterval(this.currentInterval);
				}else{
					_this.update(percent,speed);
				}
			},speed);
		},
		//Clear instance of interval
		clearInterval: function (type) {
			//Clear interval of type
			clearInterval(this.intervals[this.currentInterval]);
		},
		//Update progress bar wit new value
		update: function (percent,speed) {
			//Test if loader exists
			if(!this.loaderProgress){
				//Set mainLoader loader
				this.loader = this.mainLoader;
				//Set mainLoader loader
				this.loaderProgress = this.mainLoaderProgress;
			}
			if(this.loaderProgress!=''){
				//Test for max value then reset to max
				percent = percent > this.maxValue ? this.maxValue : percent < this.minValue ? this.minValue : percent;
				//Set progress bar div width
				this.loaderProgress.width(percent + '%');
				//Check if speed needs to decrease
				if(typeof speed!= 'undefined') this.checkSpeed(percent,speed,percent);
			}

		},
		//Show or hide loader
		showHideLoader: function (visible) {
			//Change loader visibility data attribute
			if(this.loader!=''){
				if(visible){
					this.loader.show();
				}else{
					this.loader.hide();
				}
			}

		},
		//Check if speed needs to decrease method
		checkSpeed: function (percent,speed) {
			//Do devision
			var progress = Math.floor(this.maxValue/percent);
			//Current speed
			var decelSpeed = speed;
			//Test if slow down devision has been reached
			if(progress==this.decelDiv){
			    //Clear current instance
				this.clearInterval(this.currentInterval);
			    //New speed calculation
				decelSpeed += speed*this.decel;
			    //Create new instance with new speed
				this.createProgressBar(decelSpeed,percent);
			}
		},
		destroy: function () {
			fnb.hyperion.progress = {};
		}
	};

	//Namespace progress
	fnb.namespace('progress', progress, true);

})();
///-------------------------------------------///
/// developer: Richard
///
/// Developer 2: Donovan
///
/// sideMenu Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [	
		            {type: 'frame', listener: document, events:'click', selector: '[data-role="sideMenuItem"]', handler: 'fnb.hyperion.utils.sideMenu.sideMenuItem(event);'},
          			{type: 'frame', listener: document, events:'click', selector: '#sideMenuButton', handler: 'fnb.hyperion.utils.sideMenu.show(event);'},
          			{type: 'frame', listener: document, events:'click', selector: '#sideMenuClose', handler: 'fnb.hyperion.utils.sideMenu.hide(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
    sideMenu = function() {
		
	};
	sideMenu.prototype = {
		autoInit: true,
		sideMenu: '',
		sideMenuButton: '',
		sideMenuContent: '',
		activeGroupId: '',
		sideMenuGroups: {},
		sideMenuItems: [],
		//Init FNB Top Tabs
		init: function () {
			//Select sidemenu
			this.sideMenu = fnb.hyperion.$("#sideMenu");
			//Select sidemenuButton
    		this.sideMenuButton = fnb.hyperion.$("#sideMenuButton");
    		//Select sidemenuContent
    		this.sideMenuContent = fnb.hyperion.$("#sideMenuContent");
    		//Create object for side menu
    		this.createGroupObject();

    		bindEvents();
    	},
    	createGroupObject: function () {
    		//Select side menu
    		var sideMenuGroups = fnb.hyperion.$(".sideMenuGroup");
    		//Create reference to group object
    		var parentObj = this;
    		//Test if any were found
    		if (sideMenuGroups.length() > 0) {
    			//Loop side menu and add to group object
    			sideMenuGroups.each(function(elem) {
	    			parentObj.sideMenuGroups[elem.attr('id')] = {'elem':elem};
	    		});
    		};
    	},
    	show: function () {
    		//Set indicator to show menu is active in order to hide page content in phone mode
    		fnb.hyperion.controller.bodyElement.addClass('menuActive');
    		//Move Button offscreen
			this.sideMenuButton.attr("data-position", "offscreen");
			//Move menu onscreen
			this.sideMenuContent.attr("data-position", "");
		},
		hide: function () {
    		//Set indicator to show menu is inactive in order to show page content in phone mode
    		fnb.hyperion.controller.bodyElement.removeClass('menuActive');
			//bring menuButton onto screen
			this.sideMenuButton.attr("data-position", "");
			//take menuContent off screen
			this.sideMenuContent.attr("data-position", "offscreen");
			//hide all sidemenu groups
			if (this.activeGroupId != '') { this.sideMenuGroups[this.activeGroupId].elem.hide(); }
			this.activeGroupId = '';
			//set the menu inactive
			this.sideMenu.attr("data-active","false");
		},
		sideMenuItem: function (event) {
			var parentObject = this;
			//the selected SideMenu 
			var selectedSideMenu = fnb.hyperion.$(event.currentTarget);
			//id of the selected SideMenu
		    var selectedSideMenuId = "sideMenuGroup_" + selectedSideMenu.attr('id');
		    //test if incoming id and the active id are the same
		    if(selectedSideMenuId == this.activeGroupId){
	    		//Set indicator to show menu is active in order to hide page content in phone mode
	    		fnb.hyperion.controller.pageContentContainerElement.show();
		    	//hide incoming elem
		    	parentObject.sideMenuGroups[this.activeGroupId].elem.hide();
		    	//clear stored id
		    	this.activeGroupId = '';
		    	// set menu inactive
		    	this.sideMenu.attr("data-active","false");
		    }else {
		    	//find active group and hide active group
		    	if (this.activeGroupId !='' ) {
		    		//hide active group
		    		parentObject.sideMenuGroups[this.activeGroupId].elem.hide();
		    		//show incoming group 
		    		parentObject.sideMenuGroups[selectedSideMenuId].elem.show();
		    	}
		    	else {
		    		//Set indicator to show menu is active in order to hide page content in phone mode
		    		fnb.hyperion.controller.pageContentContainerElement.hide();
		    		//show incoming group
		    		parentObject.sideMenuGroups[selectedSideMenuId].elem.show();
		    	}
		    	//store incoming id
		    	this.activeGroupId = selectedSideMenuId;
		    	// set menu active
		    	this.sideMenu.attr("data-active","true");
		    }
		
		},
		destroy: function () {
		}
	};
		
	//Namespace sideMenu
	fnb.namespace('utils.sideMenu', sideMenu, true);

})();
///-------------------------------------------///
/// developer: Richard
///
/// developer 2: Donovan
///
/// subMenu Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [	{type: 'frame', listener: document, events:'click', selector: '[data-role="subMenuItem"]', handler: 'fnb.hyperion.utils.subMenu.selectSubMenuItem(event);', preventDefault: true},
	              		{type: 'frame', listener: document, events:'click', selector: '[data-role="subMenuAtricleItem"]', handler: 'fnb.hyperion.utils.subMenu.selectSubMenuItem(event);'},
	              		{type: 'frame', listener: document, events:'click', selector: '[data-role="subMenuHeading2"]', handler: 'fnb.hyperion.utils.subMenu.toggleLevel(event);'},
	              		{type: 'frame', listener: document, events:'click', selector: '[data-role="subMenuSetLink"]', handler: 'fnb.hyperion.utils.subMenu.selectSubMenuSet(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// subMenu Parent function
	///-------------------------------------------///
    function subMenu() {
		
	};
	///-------------------------------------------///
	/// subMenu Methods
	///-------------------------------------------///
	subMenu.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Subtab active flag
		active: false,
		//Subtab active element
		activeSubtab: '',
		//
		init: function () {
			bindEvents();
		},
		select: function (event) {
			//Get di of selected toptab
			var toptabId = event.selectedTabId;
    		//Select subtab
    		var subMenu = fnb.hyperion.$("#subMenu_" + toptabId);
    		//Get subMenu id
    		var submenuId = subMenu.prop('id');
    		//Test if 
			if(this.active&&submenuId!=this.activeSubtab){
				//Hide op subtab
				fnb.hyperion.controller.raiseEvent('hideSubMenu');
				//Set current actibe subtab
				this.activeSubtab = submenuId;
				//Show subtab
				fnb.hyperion.controller.raiseEvent('showSubMenu');
			}else if(this.active&&submenuId==this.activeSubtab){
				//Hide op subtab
				fnb.hyperion.controller.raiseEvent('hideSubMenu');
				//Set current actibe subtab
				this.activeSubtab = '';
			}else{
				//Set current actibe subtab
				this.activeSubtab = submenuId;
				//Show subtab
				fnb.hyperion.controller.raiseEvent('showSubMenu');
			};
		},
		selectSubMenuItem: function (event) {
			//Create a holder for a our item
    		var item = fnb.hyperion.$(event.currentTarget);
    		//Store the href attribute of our item.
    		var url = item.attr('href');
    		//Get third party data attribute
    		var thirdParty = item.attr('data-thirdParty');
    		//If the url is not empty load the url
    		if (url != '' && thirdParty != 'true') {
    			fnb.hyperion.controller.raiseEvent('loadPage',{url: url,target:fnb.hyperion.controller.pageContentElement});
    		}else{
    			fnb.hyperion.controller.redirect(url);
    		};
        },
        //Toggle children items
        toggleLevel: function (event) {
        	//Find Target item
        	var expandableItem = fnb.hyperion.$(event.currentTarget);
        	//Switch sate attribute
          	if(expandableItem.attr('data-state') ==  'inactive') {
          		expandableItem.attr('data-state','active');
        	}else{
        		expandableItem.attr('data-state','inactive');
        	}
        },
		show: function () {
			//Show subtab parent wrapper
			fnb.hyperion.controller.subTabWrapper.show();
			//Hide page contant
			fnb.hyperion.controller.pageContentContainerElement.hide();
			//Hide submenu
			fnb.hyperion.$("#" + this.activeSubtab).show();
			//Set active flag
			this.active = true;
		},
		hide: function () {
			if(this.active){
				//Hide subtab parent wrapper
				fnb.hyperion.controller.subTabWrapper.hide();
				//Show page contant
				fnb.hyperion.controller.pageContentContainerElement.show();
				//Hide submenu
				fnb.hyperion.$("#" + this.activeSubtab).hide();
				//Set active flag
				this.active = false;
			}
		},
		destroy: function () {
		}
	};

	//Namespace utils.subMenu
	fnb.namespace('utils.subMenu', subMenu, true);

})();
///-------------------------------------------///
/// developer: Mike 
///
///
/// Subtabs
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="subTab"]', handler: 'fnb.hyperion.utils.subTabs.select(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Subtabs Parent function
	///-------------------------------------------///
	function subTabs() {

	};
	///-------------------------------------------///
	/// Subtabs Methods
	///-------------------------------------------///
	subTabs.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//window size breakpoints
		breakPoints: [3,2,1],
		//active sub tab
		activeGroupId: '',
    	init: function () {
    		//Bind subTabs events
    		bindEvents();
    		//Attach an event on every page load to check for subtabs
    		fnb.hyperion.controller.attachPageEvent('fnb.hyperion.utils.subTabs.initSubTabsScroller()','');
    		
        },
        initSubTabsScroller: function() {
        	this.subTabs = undefined; 
        	//Try and select our subTabs scroller div
        	this.subTabsScrollerParent = fnb.hyperion.$('#subTabsScrollerInner'); 
        	
        	//if the selection is succesful, we have scollable subtabs so inti them
        	if(this.subTabsScrollerParent.length() > 0){
        		
        		//Store a collection of our subtabs
        		this.subTabsChildren = this.subTabsScrollerParent.find('*[data-role="subTab"]');
        		//How many subtabs we have
        		this.subTabsCount = this.subTabsChildren.length();
        		
        		//New Instance of horizontal scroller
    			this.subTabsScroller = new fnb.hyperion.horizontalScroller();
    			this.subTabsScroller.scrollableParent = this.subTabsScrollerParent;
    			this.subTabsScroller.scrollerChildren = '[data-role="subTab"]';
    			this.subTabsScroller.scrollStopWidth = 200;
    			this.subTabsScroller.scrollSpeed = 5;
    			this.subTabsScroller.maxStops = 1;
    			this.subTabsScroller.moveTreshold = 0.20;
    			this.subTabsScroller.bindEvents();
    			this.subTabsScroller.enabled = false;
    			//Add the resize event for the subtabs
    			fnb.hyperion.controller.addResizeFunction(this.resize);
				//Adjust subtabs
    			this.adjustSubTabs(window.innerWidth || document.documentElement.clientWidth,fnb.hyperion.controller.getBreakPosition(window.innerWidth || document.documentElement.clientWidth));
        		
        	}
        	
        },
        resize: function(windowSize, breakpoint) {
        	
        	fnb.hyperion.utils.subTabs.adjustSubTabs(windowSize, breakpoint);
        	
        },
        adjustSubTabs: function(windowSize, breakpoint) {
        	console.log('windowSize')
        	console.log(windowSize)
        	        	
        	/*Get value of the current breakpoint for amount of tabs to be shown*/
        	var currentBreakPointValue = (this.breakPoints[breakpoint-1]) ? this.breakPoints[breakpoint-1] : this.breakPoints[breakpoint];
        	/*Get the scrolling container width*/
        	var currentContainerWidth = this.subTabsScrollerParent.outerWidth();
        	console.log('scrollerWidth')
        	console.log(currentContainerWidth)
        	
        	/*Calculate the new width for the tabs with breakpoint value*/
        	var newTabWidth = Math.round(currentContainerWidth/currentBreakPointValue);
           	/*Calculate new width of the horizontal scroller*/
        	var newScrollingWrapperWidth = Math.round(newTabWidth*this.subTabsCount);
        	/*Apply styles*/
			this.subTabsScroller.enabled = false;
					
			/*Apply Scroller wrapper new width*/
        	if(!isNaN(newScrollingWrapperWidth)){
        		console.log('not isNan')
        		if((currentContainerWidth+(newTabWidth/2))>(newTabWidth*this.subTabsCount)){
        			
        			console.log('1:')
        			console.log(currentContainerWidth+(newTabWidth/2));
        			console.log('2:')      			
        			console.log(newTabWidth*this.subTabsCount);
        			
        			this.subTabsScrollerParent.css('width',"");
            		this.subTabsScrollerParent.css('left',"");
        		}else{
        			this.subTabsScrollerParent.css('width',newScrollingWrapperWidth+'px');
        			this.subTabsScroller.enabled = true;
        		}
        	}else{
        		console.log('isNan')
    			this.subTabsScrollerParent.css('width',"");
        		this.subTabsScrollerParent.css('left',"");
        	}
        	
        },        
        //Do our initial selections once off and create an object to store our selected nodes.
        createGroupObject: function (parentContainer) {
        	var parentObj = this;
        	var subTabs = parentContainer.find('*[data-role="subTab"]')
        	//Loop through our selected subTabs and create an object
        	subTabs.each(function(elem) {
        		var id = elem.attr('id')
        		parentObj.subTabs[id] = {};
        		parentObj.subTabs[id].subTab = elem;
        		parentObj.subTabs[id].subTabContent = fnb.hyperion.$('#'+elem.attr('data-content'));
        		//Check if the sub tab is selected and store it as the current selected tab if it is.
        		if(elem.attr('data-selected') == "true"){
	    		   parentObj.activeGroupId = elem.attr('id');
   
        		};
	    	});
    	},
        select: function (event) {
        	var parentObj = this;
        	var selectedItem = fnb.hyperion.$(event.currentTarget);
        	var selectedItemId = selectedItem.attr('id');
        	console.log(parentObj.subTabs)
         	//Check if we have created our subTabs object and create if we haven't yet.
        	if (parentObj.subTabs === undefined) {
        		parentObj.subTabs = {};
				parentObj.createGroupObject(selectedItem.parent());
			}
        	// Check which subTab is selected and hide it and show the new selection
        	if (parentObj.activeGroupId != selectedItemId) {
        		if(parentObj.subTabs[parentObj.activeGroupId].subTabContent.length() > 0 ){
				parentObj.subTabs[parentObj.activeGroupId].subTabContent.hide();
        		}
				parentObj.subTabs[parentObj.activeGroupId].subTab.attr('data-selected','false');
				if(parentObj.subTabs[selectedItemId].subTabContent.length() > 0){
				parentObj.subTabs[selectedItemId].subTabContent.show();
				}
				parentObj.subTabs[selectedItemId].subTab.attr('data-selected','true');
				parentObj.activeGroupId = selectedItemId;
			} 
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.subTabs = {};
        }
	};

	//Namespace utils.subTabs
	fnb.namespace('utils.subTabs', subTabs, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Table action buttons Module
///-------------------------------------------///
(function() {
    ///-------------------------------------------///
	/// ActionButtons Parent function
	///-------------------------------------------///
	function actionButton() {
		
	};
	///-------------------------------------------///
	/// ActionButtons Methods
	///-------------------------------------------///
	actionButton.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Var for when tooltip is already active
		active: false,
		//Var for active tooltip
		activeTooltip: "",
		//Init FNB ActionButtons
    	init: function () {
    		//Attach event to page load complete event sequence
    		fnb.hyperion.controller.attachPageEvent('fnb.hyperion.utils.table.actionButton.reset()','');
        },
        //Show tooltip with button options
        show : function(target) {

        	//Test if tooltip is active
        	if(this.active==false){
        		
            	//Select action button target
            	var actionTarget = fnb.hyperion.$(target);
            	
            	//Get target id
            	var targetId = actionTarget.attr("id");
            	
            	//Select action button parent
            	var actionTargetParent = actionTarget.parent();

            	//Find tooltip and show it
            	this.activeTooltip = actionTargetParent.find('#'+targetId+'toolTipListMessage');

            	//Test if a tooltip was found
            	if(this.activeTooltip.length()>0){
            		
               		//Show tooltip
            		this.activeTooltip.show();
            		
            		//Get button position
            		var buttonPos = actionTarget.position();

            		//Get tooltip height to calculate position
            		var activeTooltipHeight = this.activeTooltip.outerHeight();
            		
            		//Get tooltip width to calculate position
            		var activeTooltipWidth = this.activeTooltip.outerWidth();
 
            		//Get button heught
            		var targetHeight = actionTarget.outerHeight();
            		
            		//Calculate bottom styling offset
            		var bottomPos = (activeTooltipHeight/2)-(targetHeight/2);

            		//Calculate final bottom pos
            		var yPos =(buttonPos.objectPosY+buttonPos.pageScroll.pageScrollY)-bottomPos;

            		//Calculate final left pos
            		var xPos = buttonPos.objectPosX-activeTooltipWidth;
            		
            		//Add positions
            		this.activeTooltip.css({'left': xPos+'px', 'top': yPos+'px'})

                	//Set Active flag
                	this.active = true;
            	};

        	}else{
        		//If you click again hide the tooltip
        		//this.hide();
        	}

        },
        //Hide tooltip with button options
        hide : function() {
        	
        	//Hide Tooltip
        	if(this.active){
        		
            	//Hide tooltip
            	this.activeTooltip.hide();
            	
            	//Set Active flag
            	this.active = false;
        	}

        },
        //Reset active state
        reset : function() {

        	//Set Active flag
        	this.active = false;
        	
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.table.actionButton = {};
        }
	};

	//Namespace textArea
	fnb.namespace('utils.table.actionButton', actionButton, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Table Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '#errorClose', handler: 'fnb.hyperion.utils.error.hide(event);', preventDefault: true}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	//Init all table modules
	function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.utils.table) {
    		if(fnb.hyperion.utils.table[module].autoInit){
    			fnb.hyperion.utils.table[module].init();
			}
    	}
    };
	///-------------------------------------------///
	/// Table Parent function
	///-------------------------------------------///
	function table() {

	};
	///-------------------------------------------///
	/// Table Methods
	///-------------------------------------------///
	table.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Table
    	init: function () {
    		console.log('Utils Table init');
    		//Initialize Form modules
    		initModules();
    		//Bind Error events
    		bindEvents();
	    },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.table = {};
        }
	};
	//Namespace utils.table
	fnb.namespace('utils.table', table, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Templates Object
///-------------------------------------------///
(function() {
    //Build template html
	function getHTML(elements) {
    	var html = '';
		//Test if elements are more than one element loop and build html
		if(elements.length){
		    //Loop each of the template elements and get the html
			for (var i = 0; i < elements.length; i++) {
				//Append individual element html to group html
				html += getElementHTML(elements[i]);
			}
		}else{
			//Set html for a single element
			html = getElementHTML(elements);
		}
    	//Return complete set of html
    	return html;
    };
    //Return element html
    function getElementHTML(element) {
		//Get template by name
    	var elementHtml = fnb.hyperion.htmlTemplates[element.templateName];
		//Set parameters to template html
        return replaceParams(elementHtml,element.templateData);
    };
    //Replace element parameters
    function replaceParams(template, data) {
		//Declare htmlSnippet
    	var htmlSnippet = '';
    	//Declare prop
    	var prop;
    	//Declare regex
    	var regex;
    	//Loop data object and replace parameters
        for (prop in data) {
        	//Create regular expression
            regex = new RegExp('{{' + prop + '}}', 'ig');
        	//Replace param using regular expression
            htmlSnippet = (htmlSnippet || template).replace(regex, data[prop]);
        }
 
        return htmlSnippet;
    };
	///-------------------------------------------///
	/// Templates Parent function
	///-------------------------------------------///
	function templates() {

	};
	///-------------------------------------------///
	/// Templates Methods
	///-------------------------------------------///
	templates.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
        //Add template to dom
        addTemplate: function (object) {
        	//Select target for template
        	var templateTarget = fnb.hyperion.$(object.templateTarget);
        	//Test if target got selected
        	if(templateTarget.length()>0){
            	//Set parent visibilitynb.$(
            	templateTarget.show();
            	//Get template html and set html to template target
            	this.setTarget(templateTarget, getHTML(object.templateElements));
        	}
        },
        //Return html template
	    getTemplate: function (object) {
	    	//Get template html and return to sender
	    	return getHTML(object);
	    },
        //Set the html for a target element
        setTarget: function (target, html) {
        	//Set target html
        	target.html(html);
        },
        //Clear the html from a target element
        clearTarget: function (target) {
        	//Select target
        	var target = fnb.hyperion.$(target);
        	//Clear target
        	if(target.length()>0) target.html("");
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.templates = {};
        }
	};

	//Namespace utils.templates
	fnb.namespace('utils.templates', templates, true);

})();
///-------------------------------------------///
/// developer: Mike
///
/// Design Grid Object
///-------------------------------------------///
(function() {
	function bindEvents() {
    	//List of events for this module
    	var events = [
    	{type: 'frame', listener: document, events:'click', selector: '[data-role="theGridToggle"]', handler: 'fnb.hyperion.utils.theGrid.toggleState(event)'}
    	];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Design Grid Parent function
	///-------------------------------------------///
	function theGrid() {

	};
	///-------------------------------------------///
	/// Design Grid Methods
	///-------------------------------------------///
	theGrid.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		
		gridRows: '',
		gridCols: '',
		
		init: function () {
    	    //Set theGrid default property
    	   	this.gridRows = 'theGridOverlayRows';
    	   	this.gridCols = 'theGridOverlayCols';
    	   	//Bind Design Grid events
    	   	bindEvents();
		},
        toggleState: function(event) {
        	//Select grid rows
        	var gridRows = fnb.hyperion.$('#'+this.gridRows);
        	//Select grid columns
        	var gridCols = fnb.hyperion.$('#'+this.gridCols);
        	
        	//Toggle visibility
          	if(gridRows.is(':visible')) {
          		gridRows.hide();
        	}else{
        		gridRows.show();
        	}
          	if(gridCols.is(':visible')) {
          		gridCols.hide();
          	}else{
          		gridCols.show();
          	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.theGrid = {};
        }
	};

	//Namespace utils.theGrid
	fnb.namespace('utils.theGrid', theGrid, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// TimeOut Object
///-------------------------------------------///
(function() {
	//TimeOut element selectors
	//Var for timer wrapper
	var timerWrapper;
	
	//TimeOut Vars
	//Global Timeout seconds 300 = 5mins
	var timeOutSeconds = 300;
	//Popup disaply seconds
	var displaySeconds = 60;
	//Global var for single timeout for all timeouts
	var timerInterval;
	//Page title
	var title = 'Alfie';
	//Logoff url
	var logOffUrl  = '/Alfie/Controller?formname=WELCOME&action=logoff';
	
	//Select elements that are going to be reused
	function doSelections() {
	
		//Select timer wrapper
		timerWrapper = fnb.hyperion.controller.timeOutElement.find(".clock");
		
    };
	
	//Create timer
	function createTimer(callback) {
	
		//Create display interval
		timerInterval = setInterval(callback, 1000);
		
    };
	
	//Clear timer
	function clearTimer() {
	
		//Clear display interval
		clearInterval(timerInterval);
		
    };
	
	//Start timout timer
	function startTimeOut() {
		
		//Create timout timer
		createTimer(updateTimer);
		
    };

	//Update timeoutTimer
	function updateTimer() {
	
		//Update second var
		timeOutSeconds--;
		
		//Check if coundown is finished
		if(timeOutSeconds==0){
		
			//Reset timer
			resetTimer(updateDisplayTimer);
			//Show popup
			fnb.hyperion.controller.raiseEvent("showTimeOut");
			
		};
		
    };
	
	//Reset timeout vars
	function resetTimer(callback) {
		if(!fnb.hyperion.controller.isMobile){
			//Test for callbal otherwise set one
			if(!callback) callback = updateTimer;
			//Reset display seconds
			displaySeconds = 60;
			//Reset timeOut seconds
			timeOutSeconds = 300;
			//Reset display timer
			timerWrapper.html(displaySeconds);
			//Clear old timer
			clearTimer();
			//Start new timer
			createTimer(callback);
		}
		
	};
	
	//Update display timer
	function updateDisplayTimer() {
	
		//Update second var
		displaySeconds--;
		
		//Check if coundown is finished
		if(displaySeconds==0) logOff();
		
		//Update display clock
		timerWrapper.html(displaySeconds);

	};
		
	//Log user off
	function logOff() {
		//Clear Timer
		clearTimer();
		//Load userlogoff
//		fnb.hyperion.controller.raiseEvent("loadPage",{url:logOffUrl});
		window.parent.location=logOffUrl;
		
	};
	
	//Bind event for current object
	function bindEvents() {

      	var events = [{type: 'frame', listener: document, events:'click', selector: ".timeOutClose", handler: 'fnb.hyperion.controller.raiseEvent("logOffTimeOut");'},
      	              {type: 'frame', listener: document, events:'click', selector: ".timeOutContinue", handler: 'fnb.hyperion.controller.raiseEvent("hideTimeOut");'}
      	              ];

      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	
	///-------------------------------------------///
	/// TimeOut Parent function
	///-------------------------------------------///
	function timeOut() {
		
	};
	///-------------------------------------------///
	/// TimeOut Methods
	///-------------------------------------------///
	timeOut.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Active var for timeout
		active: false,
		//Init FNB timeout
		init: function () {
			console.log('OVERRIDING Utils TimeOut init');
			//Check for if mobile device
			if(!fnb.hyperion.controller.isMobile){
				//Do timeOut element selections
				doSelections();
				//Bind timeOut events
				bindEvents();
				//Start timout timer
				startTimeOut();
			}
		},
		//Show timeOut popup
		show: function () {
			//Show
			fnb.hyperion.controller.timeOutElement.show();
			//Set active flag
			this.active = true;
		},
		//Hide timeOut popup
		hide: function () {
			//Reset timer
			resetTimer(updateTimer);
			//Hide
			fnb.hyperion.controller.timeOutElement.hide();
			//Set active flag
			this.active = false;
		},
		//Public Logoff
		logOff: logOff,
		//Public Reset
		reset: resetTimer,
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.utils.timeOut = {};
		}
	};

	//Namespace utils.timeOut
	fnb.namespace('utils.timeOut', timeOut, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Top Tabs Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
		var events = [{type: 'frame', listener: fnb.hyperion.utils.topTabs.topTabsNav, events:'click', selector: fnb.hyperion.utils.topTabs.topScroller.scrollerChildren, handler: 'fnb.hyperion.utils.topTabs.select(event);', preventDefault: true},
    	              {type: 'frame', listener: document, events:'click', selector: '#topTabScrollButtonRight', handler: 'fnb.hyperion.utils.topTabs.tabsScrollRight();'},
    	              {type: 'frame', listener: document, events:'click', selector: '#topTabScrollButtonLeft', handler: 'fnb.hyperion.utils.topTabs.tabsScrollLeft();'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Top Tabs Parent function
	///-------------------------------------------///
	function topTabs() {

	};
	///-------------------------------------------///
	/// Top Tabs Methods
	///-------------------------------------------///
	topTabs.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Var for topTabs breakpoints
		breakPoints: [10,5,4],
		//Var declarations
		//Current breakpoint position
		topTabBreakpointValue:-2,
		//Instance of horizontal slider
		topScroller:'',
		//Total of toptabs
		tabsCount:0,
		//Toptabs parent for width adjustment
		topTabsNavParent: '',
		//HeaderInner selecteor
		headerInner: '',
		//Toptabs nav wrapper
		topTabsWrapper: '',
		//Toptabs scrollable wrapper
		topTabsNav: '',
		//Toptabs indicator
		topTabsSlider: '',
		//Toptabs indicator index position
		topTabsSliderIndex: -1,
		//Collection of all toptabs
		topTabsChildren:'',
		//Left scroll button selector
		leftScrollButton:'',
		//Right scroll button selector
		rightScrollButton:'',
		//Left scroll button display var
		topTabsLeftScrollShow:false,
		//Right scroll button display var
		topTabsRightScrollShow:false,
		//Toptabs amount of time it scrolls before stopping
		topScrollerStops: 0,
		//Init FNB Top Tabs
    	init: function () {
    		console.log('Utils Top Tabs init');
    		//var Toptabs scrollable wrapper
    		this.topTabsWrapper = fnb.hyperion.$('#topTabs');
    		//Test if toptabs exist
    		if(this.topTabsWrapper.length()>0){
        		//var Toptabs scrollable wrapper
        		this.topTabsNav = this.topTabsWrapper.first();
        		//var topTabsNav parent for width adjustment if horizontal scroller enabled
        		this.topTabsNavParent = this.topTabsNav.parent();
        		//Create Global selector for header inner
        		this.headerInner = this.topTabsNavParent.parent();
        		//Collection of toptab children
        		this.topTabsChildren = this.topTabsNav.find('.mainTab');
    			//Get toptabs count
    			this.tabsCount = this.topTabsChildren.length();
    			//New Instance of horizontal scroller
    			this.topScroller = new fnb.hyperion.horizontalScroller();
    			this.topScroller.scrollableParent = this.topTabsNav;
    			this.topScroller.scrollerChildren = '.mainTab';
    			this.topScroller.scrollStopWidth = 200;
    			this.topScroller.scrollSpeed = 5;
    			this.topScroller.maxStops = 1;
    			this.topScroller.moveTreshold = 0.20;
    			this.topScroller.bindEvents();
    			this.topScroller.enabled = false;
        		//Bind Top tab events
        		bindEvents();
        		//Extend Controller resize function
        		fnb.hyperion.controller.addResizeFunction(this.resize);
				//Adjust toptabs
				this.adjustTopTabs(window.innerWidth || document.documentElement.clientWidth,fnb.hyperion.controller.getBreakPosition(window.innerWidth || document.documentElement.clientWidth));
				//Enable topmenu
				this.enable();
    		}

        },
        //Select functionality for topmenu
        select: function (event) {
    		//Var for the selected tab
    		var selectedTab = fnb.hyperion.$(event.currentTarget);
        	//If the topmenu is not moving or the user is not dragging it - execute body of function
        	if(this.topScroller.moving == false){
        		//Var for the hasSubMenu
        		var subMenu = selectedTab.attr("data-submenu");
        		//Test for subtab
        		if (subMenu === 'true') {
        			//Get the id of the tab
            		var selectedTabId = selectedTab.prop('id');
            		//Add Selected submenu to event
            		event.selectedTabId = selectedTabId;
            		//Raise Show hide submenu event
            		fnb.hyperion.controller.raiseEvent('showHideSubMenu', event);
        		}else{
        			//If selectedTab has a href load the url
	        		//Get the url bound to the tab
	        		var url = selectedTab.attr('data-url');
	        		//Highlight selected tab
	        		fnb.hyperion.utils.topTabs.highlightSelected(event);
	            	//Notify Controller to raise loadUrl event
	            	fnb.hyperion.controller.raiseEvent('loadPage',{url: url,target:fnb.hyperion.controller.pageContentElement});
        		}
        	}
        },
        highlightSelected: function (event) {
    		//Var for the selected tab
    		var selectedTab = (event.currentTarget) ? fnb.hyperion.$(event.currentTarget) : fnb.hyperion.$(event.target);
    		//Select toptab
    		var selectedTopTab = this.topTabsNav.find('.topTabSelected');
    		//Deselect previously selected tab
    		if(selectedTopTab.length()>0) selectedTopTab.removeClass('topTabSelected');
    		//Add selection to current tab
    		selectedTab.addClass('topTabSelected');
        },
        //Resize top tab when window resizes
        resize: function (windowSize, breakpoint) {
        	//Adjust top tabs
        	fnb.hyperion.utils.topTabs.adjustTopTabs(windowSize,breakpoint);
        },
        //Resize top tab when window resizes
        adjustTopTabs: function (windowSize, breakpoint) {
        	//Test if toptabs exist
        	if(this.topTabsChildren.length()>0){
        		/*Get value of the current breakpoint for amount of tabs to be shown*/
            	var currentBreakPointValue = (this.breakPoints[breakpoint-1]) ? this.breakPoints[breakpoint-1] : this.breakPoints[breakpoint];
            	/*Get the scrolling container width*/
            	var currentContainerWidth = this.topTabsNavParent.outerWidth();
            	/*Calculate the new width for the tabs with breakpoint value*/
            	var newTabWidth = Math.round(currentContainerWidth/currentBreakPointValue);
            	/*Calculate new width of the horizontal scroller*/
            	var newScrollingWrapperWidth = Math.round(newTabWidth*this.tabsCount);
            	/*Apply styles*/
    			this.topScroller.enabled = false;
            	/*Apply Scroller wrapper new width*/
            	if(!isNaN(newScrollingWrapperWidth)){
            		if((currentContainerWidth+(newTabWidth/2))>(newTabWidth*this.tabsCount)){
            			this.topTabsNav.css('width',"");
                		this.topTabsNav.css('left',"");
            		}else{
            			this.topTabsNav.css('width',newScrollingWrapperWidth+'px');
            			this.topScroller.enabled = true;
            		}
            	}else{
        			this.topTabsNav.css('width',"");
            		this.topTabsNav.css('left',"");
            	}
    			//Update tab widths if needed
    			this.adjustTabsWidth(newTabWidth+'px');
    			//Update global breakpoint value
    			this.topTabBreakpointValue = currentBreakPointValue;
    			/*Apply new horizontal scroller settings*/
            	/*Calculate the amount of stops for the horizontal scroller*/
            	this.topScrollerStops = Math.floor(this.tabsCount/currentBreakPointValue);
            	//Update horizontal scroller stops
    			this.topScroller.maxStops = this.topScrollerStops;
            	//Update horizontal scroller stops
				this.topScroller.moveTo(0,1);
				//Events to execute after topmenu stopped scrolling
				this.topScroller.afterStop = this.adjustScrollButtonRoles;
				//Check the roles for the topmenu scrolling buttons
				this.adjustScrollButtonRoles();
				//Set scroller scroll stop width
    			this.topScroller.scrollStopWidth = currentContainerWidth;
            	
        	}
        	
        },
        //Change toptab widths
        adjustTabsWidth: function (width) {
        	//Parse tab width
        	var newVal = parseInt(width, 10);
        	if(!isNaN(newVal)){
            	if(this.topScroller.enabled){
                	//Set topTab widths
                	this.topTabsChildren.css('width',width);
            	}else{
            		//Set topTab widths
                	this.topTabsChildren.css('width','');
            	}
        	}else{
        		if(this.tabsCount<11&&this.topScroller.enabled==false){
                	//Set topTab widths
                	this.topTabsChildren.css('width','');
            	}else if(this.topScroller.enabled==true){
            		this.topTabsChildren.css('width',width);
            	};
        	}
        },
        //Change toptab roles
        adjustTabsRoles: function (breakPointValue) {
        	/*Remove Last Child Border*/
        	this.applyDataAttribute('date-position','');
    		/*Apply new last child border*/
    		this.topTabsChildren[breakPointValue].attr('data-position', 'last');
        },
        //Reset topTabs css if scroller was running
        resetTopTabs: function () {
        	this.topScroller.moveTo(0,1);
        	/*Reset Scroller wrapper no width*/
        	this.topTabsNav.css('width','100%');
        	//Reset Left of Tabs container
        	this.topTabsNav.css('left','0');
        	//Disable horizontal Scroller
			this.topScroller.enabled = false;
			//Reset left Scrollbar button
			if(this.topTabsLeftScrollShow == true){
				fnb.hyperion.utils.topTabs.leftScrollButton.hide();
				this.topTabsLeftScrollShow = false;
			}
			//Reset right Scrollbar button
			if(this.topTabsRightScrollShow == true&&this.tabsCount<=fnb.hyperion.utils.topTabs.breakPoints[0]){
				fnb.hyperion.utils.topTabs.rightScrollButton.hide();
				this.topTabsRightScrollShow = false;
			}
        },
        //Set topTab left right scroll buttons
        adjustScrollButtonRoles: function () {
        	//Setup Global selectors for left and right scroll buttons if not done already
        	if(fnb.hyperion.utils.topTabs.leftScrollButton =='') fnb.hyperion.utils.topTabs.leftScrollButton = fnb.hyperion.$('#topTabScrollButtonLeft');
        	if(fnb.hyperion.utils.topTabs.rightScrollButton =='') fnb.hyperion.utils.topTabs.rightScrollButton = fnb.hyperion.$('#topTabScrollButtonRight');
        	//Test if scroller is active
        	if(fnb.hyperion.utils.topTabs.topScroller.enabled == true){
        		//Show hide scroller buttons on index of scroller
        		if(fnb.hyperion.utils.topTabs.topScroller.currentIndex == 0){

            		fnb.hyperion.utils.topTabs.leftScrollButton.hide();
            		fnb.hyperion.utils.topTabs.rightScrollButton.show();
            		
            	}else if(fnb.hyperion.utils.topTabs.topScroller.currentIndex > 0&&fnb.hyperion.utils.topTabs.topScroller.currentIndex<fnb.hyperion.utils.topTabs.topScrollerStops){
            		
            		fnb.hyperion.utils.topTabs.leftScrollButton.show();
            		fnb.hyperion.utils.topTabs.rightScrollButton.show();
            		
            	}else if(fnb.hyperion.utils.topTabs.topScroller.currentIndex == fnb.hyperion.utils.topTabs.topScrollerStops){
        			
            		fnb.hyperion.utils.topTabs.rightScrollButton.hide();
        			fnb.hyperion.utils.topTabs.leftScrollButton.show();
         
            	};
            	
        	}else{
        		//Hide all scroller buttons
    			fnb.hyperion.utils.topTabs.leftScrollButton.hide();
    			fnb.hyperion.utils.topTabs.rightScrollButton.hide();
        	}
        	
         },
        //Scroll toptab right
        tabsScrollRight: function () {
        	this.topScroller.next();
        },
        //Scroll toptab left
        tabsScrollLeft: function () {
        	this.topScroller.previous();
        },
        //Enable toptabs
        enable: function () {
        	//Add enabled data attribute
        	this.topTabsWrapper.attr("data-disbaled",false);
        },
        //Disable toptabs
        disable: function () {
        	//Remove enabled data attribute
        	this.topTabsWrapper.attr("data-disbaled",true);
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.topTabs = {};
        }
	};

	//Namespace utils.topTabs
	fnb.namespace('utils.topTabs', topTabs, true);

})();

///-------------------------------------------///
/// developer: Mike
///
/// Banner manager Object
///-------------------------------------------///
(function() {
	var bannerInstances = {};
	var bannerScript = {};
	var bannerInstanceId = 0;
	var controllerClick = "/Controller?nav=banners.BannerEngine&name=";
    var controllerNav = "/Controller?nav=banners.BannerEngine&action=t&name=";
  
    function  getRandomNumber(lower,upper){
     	var rnd = lower + Math.random() * (upper-lower);
    	rnd = Math.floor(rnd);
    	return rnd;
    };
    
	fnb.hyperion.bannerController = function(id) {
		
		if (!(this instanceof fnb.hyperion.bannerController)) {
		    for (var key in bannerInstances) {
		       if (bannerInstances[key].id === id) {
		    	   return bannerInstances[key];
		       }
		    }
		    bannerInstanceId++;
		    bannerInstances[bannerInstanceId] = new fnb.hyperion.bannerController(id);
		    return bannerInstances[bannerInstanceId];
		}
		this.id = id;
		this.selectedId = fnb.hyperion.$('#'+id);
		this.settings = this.selectedId.attr('data-settings');
		this.settingsObject = JSON.parse(this.settings);
		this.contextPath = this.selectedId.attr('data-contextPath');
		this.banners = this.selectedId.attr('data-banners');
		this.bannerList = [];
		this.random = this.settingsObject["random"] ? 'true' : 'false';
		this.type = this.settingsObject["type"];
		this.currentPos=0;
		this.rotationInterval=this.settingsObject["rotationInterval"];
	
		this.currentBannerNumber = 0;
		this.timeOut;
	   	//bindEvents(this.id);
	   	if(this.banners=="empty"){
			this.bannersObject = {};
			console.log('No banners found!')
		}else{
			this.bannersObject = JSON.parse(this.banners);
			this.addBannersToGroup(this.bannersObject);
		}
	   		   
	};
	fnb.hyperion.bannerController.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		addBannersToGroup : function(bannersObject) {
			var parentObject = this;
			//Looping through banner object and adding to array
			for (var key in bannersObject) {
				parentObject.bannerList.push({
	            	bannerName:bannersObject[key]["bannerName"],
	            	bannerUrl:bannersObject[key]["bannerUrl"]
	            })
	         }
			parentObject.loadBanner();
        },
        loadBanner:function() {

           	var parentObject = this;
           	var bannerName = "";
           	
           	if(parentObject.random == 'true') {
        		parentObject.currentBannerNumber=getRandomNumber(0,parentObject.bannerList.length);
        		parentObject.random= false;
        	};
 
        	if(parentObject.bannerList.length>0 && parentObject.currentBannerNumber<=parentObject.bannerList.length){
           		if(parentObject.bannerList[parentObject.currentBannerNumber]["bannerName"]!=""){
        			bannerName = parentObject.bannerList[parentObject.currentBannerNumber]["bannerName"];
        			
        			var getBannerFromUrl =parentObject.contextPath+'/04Banners/'+ parentObject.bannerList[parentObject.currentBannerNumber]["bannerName"]+"/banner.html";
        			var bannerURL = parentObject.bannerList[parentObject.currentBannerNumber]["bannerUrl"]
       			    
        			fnb.hyperion.controller.raiseEvent("loadBanner",{url:getBannerFromUrl,target:parentObject.selectedId,bannerManagerId:parentObject.id,params:'url='+bannerURL,type:'get'})
        			fnb.hyperion.controller.raiseEvent("sendImpressions",bannerName);		    
        		}else{
        			console.log("empty banner source for banner: " + parentObject.bannerList[parentObject.currentBannerNumber]["bannerName"]);
        		};
        	};        	
        	parentObject.currentBannerNumber++;
        	if(parentObject.currentBannerNumber>=parentObject.bannerList.length)parentObject.currentBannerNumber=0;
        	
        },
        createTimeOut: function() {
        	var parentObject = this;
        	
        	if(fnb.hyperion.bannerObject) fnb.hyperion.bannerControllerManager.bannerScript[parentObject.id] = new fnb.hyperion.bannerObject(parentObject.id);
        	
        	if(parentObject.type!="" && !isNaN(parentObject.rotationInterval)){
        		parentObject.timeOut = setTimeout(function(){
        			 parentObject.loadBanner();
        		},parentObject.rotationInterval);
    		}
        }
	};
	fnb.hyperion.bannerControllerManager = function(){
			this.bannerControllerInstances = {}
	};
	fnb.hyperion.bannerControllerManager.prototype = {
			//Var for frame to auto initialize module
			bannerScript: {},
			autoInit: true,
			init : function() {
	            fnb.hyperion.controller.attachPageEvent('fnb.hyperion.bannerControllerManager.initBannerControllerInstances()','');
	            //Both banners and thumbnails need to send click events to b tracked
	            var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="bannerLink"],[data-role="thumbnailBanner"]', handler: 'fnb.hyperion.bannerControllerManager.bannerNavToUrl(event)', preventDefault: true}];
	        	fnb.hyperion.actions.addEvents(events);	
	        },
	        initBannerControllerInstances: function() {
	        	
	        	var bannerControllers = fnb.hyperion.$(".bannerContainer");
	        	
	        	var length = 0;
	        	try{
	        		length = bannerControllers.length()
	        	}catch(e){
	        		length = bannerControllers.length;
	        	}
	        	if(length > 0 ){	        		
	        			bannerControllers.each(function(element){
	        				var id = element.attr('id');
	        				fnb.hyperion.bannerController(id);
	        			});
	        		
	        	}else{
	        		this.destroy();	
	        	}
	        },
	        sendImpressions:function(impressionList) {
	        	//Build up the url to send impressions
	        	var impressionUrl = '/Controller?nav=banners.BannerEngine&action=t&name='+impressionList+'&adLocation='+top.location;
	        	//Raise the event to send the above mentioned url
				fnb.hyperion.controller.raiseEvent("ajaxSend",{url:impressionUrl}); 
	        },
	        bannerNavToUrl: function(event){

	        	var target = fnb.hyperion.$(event.currentTarget);

	        	var url= target.attr('href');
	        	//get he name of the baner to to pass to the contoller
	        	var bannerName= target.attr('id');
	        	//Log theclick event an load the resultinto te workspace
	        	fnb.hyperion.controller.raiseEvent("loadPage",{url:'/Controller?nav=banners.BannerEngine&name='+bannerName+'&&adLocation='+top.location,type:'get'});
	        },	        
	        bindEvents: function(id){
	        
	        	
	        },
	        destroy: function(){
	        		        	
	        	for (var key in bannerInstances) {
	        		clearTimeout(bannerInstances[key].timeOut);
	        		delete bannerInstances[key];
		        }
	        	bannerInstances = {};
	        }
	};
	fnb.hyperion.bannerControllerManager = new fnb.hyperion.bannerControllerManager();
})();


///-------------------------------------------///
/// developer: Mike
///
/// Banner manager Object
///-------------------------------------------///
(function() {
	fnb.hyperion.utils.contentSlider = function(id) {
		
		if (!(this instanceof fnb.hyperion.utils.contentSlider)) {
		    for (var key in fnb.hyperion.utils.contentSliderManager.contentSliderInstances) {
		       if (fnb.hyperion.utils.contentSliderManager.contentSliderInstances[key].id === id) {
		    	   return fnb.hyperion.utils.contentSliderManager.contentSliderInstances[key];
		       }
		    }
		    fnb.hyperion.utils.contentSliderManager.contentSliderInstanceId++;
		    fnb.hyperion.utils.contentSliderManager.contentSliderInstances[fnb.hyperion.utils.contentSliderManager.contentSliderInstanceId] = new fnb.hyperion.utils.contentSlider(id);
		    return fnb.hyperion.utils.contentSliderManager.contentSliderInstances[fnb.hyperion.utils.contentSliderManager.contentSliderInstanceId];
		}
		this.id = id;
		this.container;
		this.init();  		   
	};
	fnb.hyperion.utils.contentSlider.prototype = {
		autoInit: false,
		init: function() {

			this.container = fnb.hyperion.$('#'+this.id);
			this.containerWidth = this.container.offsetWidth;
			this.childrenItems = this.container.find('.singleViewSlide');
			var length = this.childrenItems.length;
			this.contentSliderScroller = new fnb.hyperion.horizontalScroller();
			this.contentSliderScroller.scrollableParent = this.container.find('.singleViewSlider');
			this.contentSliderScroller.scrollerChildren = '.singleViewSlide';
			this.contentSliderScroller.scrollSpeed = 5;
			this.contentSliderScroller.maxStops = length-1;
			this.contentSliderScroller.moveTreshold = 0.20;
			this.contentSliderScroller.bindEvents();
			this.contentSliderScroller.enabled = true;
			//this.setWidths();
		},
		getMaxHeight: function(){
			var parentObject = this
			this.childrenItems.each(function(element){
				parentObject.heights.push(fnb.hyperion.$(element).offsetHeight);
			});
			var max = parentObject.heights[0];
			var maxIndex = 0;

			for (var i = 1; i < parentObject.heights.length; i++) {
			    if (parentObject.heights[i] >= max) {
			        maxIndex = i;
			        max = parentObject.heights[i];
			     }
			}

			return max;
		},
		setWidths: function(){
			var parentObject = this
			
			this.containerWidth = this.container.offsetWidth;
			var scrollerWidth = parentObject.containerWidth * this.childrenItems.length + 'px';
			this.container.find('.singleViewSlider').css('width', scrollerWidth);
			this.heights = [];
			this.maxHeight = this.getMaxHeight();
			this.container.css('height',this.maxHeight+40+'px')
			this.contentSliderScroller.scrollableParent.css('height',this.maxHeight+20+'px')
			this.childrenItems.each(function(element){
				var el = fnb.hyperion.$(element);
				el.css('width',(parentObject.containerWidth)+'px');
						
			});
			
			
		},
		setScrollStopWidth: function(id){
			var parentObject = this;
			fnb.hyperion.utils.contentSlider(id).contentSliderScroller.scrollStopWidth = fnb.hyperion.utils.contentSlider(id).container.offsetWidth;
			
			
		},
		adjustOnResize: function(windowSize, breakpoint,params) {
			fnb.hyperion.ready(function() {
				var parentObject = this;
				fnb.hyperion.utils.contentSlider(params).setScrollStopWidth(params);
				fnb.hyperion.utils.contentSlider(params).setWidths(params);
				parentObject.contentSliderScroller.moveTo(0,1);
				
			})

		}
		
	};
	fnb.hyperion.utils.contentSliderManager = function(){
			this.contentSliderInstances = {};
			
	};
	fnb.hyperion.utils.contentSliderManager.prototype = {
			autoInit: false,
			contentSliderInstanceId: 0,
			init : function() {
	            fnb.hyperion.controller.attachPageEvent('fnb.hyperion.utils.contentSliderManager.initContentSliderInstances()','');
	        },
	        initContentSliderInstances: function() {
	  
	        	var contentSliders = fnb.hyperion.$(".singleView");
	        	if(typeof contentSliders.length == 'function'){
	        	var length = (typeof contentSliders.length == 'undefined') ? 1: contentSliders.length(); 
	        		if(length > 0){	        		
	        			contentSliders.each(function(element){
	        				var id = element.attr('id');
	        				fnb.hyperion.utils.contentSlider(id);
	        				fnb.hyperion.controller.addResizeFunction(fnb.hyperion.utils.contentSlider(id).adjustOnResize,id);
	        			});
	        		}
	        	}else{
	        	this.destroy();	
	        	}
	        },
	        destroy: function(){
	        	for (var key in fnb.hyperion.utils.contentSliderManager.contentSliderInstances) {
	        		delete fnb.hyperion.utils.contentSliderManager.contentSliderInstances[key];
		         }
	        	fnb.hyperion.utils.contentSliderManager.contentSliderInstances = {};
	        }
	};
	//fnb.hyperion.utils.contentSliderManager = new fnb.hyperion.utils.contentSliderManager();
})();
///-------------------------------------------///
/// developer: Richard 
///
/// Widgets Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Widgets Parent function
	///-------------------------------------------///
	function widgets() {

	};
	///-------------------------------------------///
	/// Widgets Methods
	///-------------------------------------------///
	widgets.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init FNB Widgets
    	init: function () {

        },
        //load widget into target
        loadWidgets: function (widgets) {
        	//For each widget on the page get the url and load the result into the taget 
			for(var i=0;i<widgets.length;i++) {
				//Get the widget target
    			var widgetTarget = fnb.hyperion.$("#"+widgets[i]);
    			//Get the widgets url
    			var url = widgetTarget.attr("data-application");
    			//Populate the load object with the url and target
    			var loadObj = {url:url, target:widgetTarget, async: true};
    			//Raise event to load the url into the target
    			fnb.hyperion.controller.raiseEvent("loadApp",loadObj);
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.widgets = {};
        }
	};

	//Namespace utils.widgets
	fnb.namespace('utils.widgets', widgets, true);

})();
