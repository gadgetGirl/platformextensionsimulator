///-------------------------------------------///
/// developer: Donovan
///
/// Main Forms Object
///-------------------------------------------///
(function() { 
	//Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'onsubmit, submit', selector: '[data-role="form"]', handler: 'return fnb.hyperion.forms.submitForm(event);', preventDefault: true}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
    //Init all form modules
	function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.forms) {
    		if(fnb.hyperion.forms[module].autoInit){
				fnb.hyperion.forms[module].init();
			}
    	}
    };
    //Trim values of whitespaces
    function trim(value)
    {
      return value.replace(/^\s+|\s+$/, '');
    } 
    ///-------------------------------------------///
	/// Main Forms Parent function
	///-------------------------------------------///
	function forms() {

	};
	///-------------------------------------------///
	/// Main Forms Methods
	///-------------------------------------------///
	forms.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Froms
    	init: function () {
    		console.log('Forms init');
    		//Initialize Form modules
    		initModules();
    		//Bind form events
    		bindEvents();
        },
        submitForm: function(event){
        	//Select bound target
        	var form = fnb.hyperion.$(event.currentTarget);
        	//Test for from action
        	if (form.action != '')  fnb.hyperion.forms.validateAndSubmitForm({url: form.prop("action"), dataTarget: '#'+form.prop("action")});
        },
        //Validate the input fields
        validateAndSubmitForm: function(loadObj){
        	//Test for datatarget
        	if(loadObj.dataTarget){
        		//Set valid form flag
            	validForm = true;
            	//Find all form elements with data required and validate
    			fnb.hyperion.$(loadObj.dataTarget).find('*[data-required="true"]').each(function(element) {
    				//Get target val
    				var checked = false;
    				//Get closest form container
    	        	var validationTarget = element.find('.validationTarget');
    	        	//Target value var
    	        	var targetValue = '';
    	        	//Test if validation target was found
    	        	if(validationTarget.length()>0){
    	        		//Test if more than one target is available
        	        	if(validationTarget.length()==1){
        	        		//Set target value
        	        		targetValue = validationTarget.val();
        	        		//Set checked flag
        	        		checked = true;
        	        	}else if(validationTarget.length()>1){
        	        		//Loop target and check selected
        	        		validationTarget.each(function(element){
        	        			if(element.attr('checked')=='true'||element.attr('checked')=='checked'){
        	        				checked = true;
        	        				targetValue = element.val().toString();
        	        			}
        					});
        	        	}
        				//Find error wrapper
        	        	var validationErrorTarget = element.find('.formInlineError');
        	        	//Simple validation...
        				if (targetValue == null || targetValue == '' || targetValue == 0 || checked == false) {
        					//Value failed add error class
        					element.addClass("inputError");
        					//Set valid form flag false
        					validForm = false;
        				}else{
        					if(validationTarget.attr("data-type")=="email"){
        						//Test email address
        						var message = fnb.hyperion.forms.validateEmail(validationTarget);
        						if(message!=""){
        							//Value failed add error class
        	    					element.addClass("inputError");
        							//Set valid form flag
        							validForm = false;
        							//Set email message
        							validationErrorTarget.html(message);
        						}else{
        							//Remove error if visible
                					element.removeClass("inputError");
        						}
        					}else{
            					//Remove error if visible
            					element.removeClass("inputError");
        					}
        				}
    	        	}
    	        	
    			});
    			//If valid from submit
    			if (validForm) {
    				fnb.hyperion.controller.raiseEvent("submit",loadObj);
    			};
        	}
		},
		//submit to clear bean
        clearAndSubmitForm: function(loadObj){
        	
        	fnb.hyperion.$('#command').attr('value', 'DEFAULT');
        	
			fnb.hyperion.controller.raiseEvent("submit",loadObj);

		},
		////Validate email
		validateEmail: function(input){
        	
			//Error var
			var error="";
			//Value without whitespaces
		    var timmedValue = trim(input.val());
		    //Regex for email filter
		    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
		    //Regex for illegal chars
		    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
		    
		    if (!emailFilter.test(timmedValue)) {
		        error = "Please enter a valid email address.";
		    } else if (input.val().match(illegalChars)) {
		        error = "The email address contains illegal characters.";
		    }
		    
		    return error;

		},		
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms = {};
        }
	};

	//Namespace forms
	fnb.namespace('forms', forms, true);

})();
///-------------------------------------------///
/// developer: CB Lombard
///
/// Accordion Form Object
///-------------------------------------------///
(function() {
    //Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.accGroupItemWrapper', handler: 'fnb.hyperion.forms.accordion.handleOpenClose(event);'}];
        //Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Accordion Parent function
	///-------------------------------------------///
	function accordion() {

	};
	///-------------------------------------------///
	///          Accordion Methods				  ///
	///-------------------------------------------///
	accordion.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
        //set Type
        closetype: '',
        //setCurrentGroup
        currentGroup: '',
		//Init FNB Banking button
    	init: function () {
    		console.log('Forms Accordion init');
            //Bind Events
            bindEvents();
        }, 
        // handle Accorion click event
        handleOpenClose : function(event){

         //Get target groupWrapper element 
         element = fnb.hyperion.$(event.currentTarget);
         elementOpenStatus = element.attr('data-state');

         //get parrent (accordianContainer) of event target
         var parentContainer =  element.parent();
             this.closetype = parentContainer.attr('data-closetype'); 
             this.currentGroup = parentContainer;

         //  if closeType equals closeAll find all open accordions and close it
         if(this.closetype == 'closeAll' && elementOpenStatus != 'open'){ this.closeAll();}
         //open close elment selected.
         this.toggleOpenClose(element);

        },
        //toggel open close of of groupWrapper element 
        toggleOpenClose: function (element) {
 
           	//Get button settings
        	var groupContentWrapperEl = element.find('! .accGroupItemContentWrapper'),
                           accArrowEl = element.find('! .accArrow'),
                            arrowIsUp = accArrowEl.hasClass('accArrow-up'),
                          arrowIsDown = accArrowEl.hasClass('accArrow-down'),
        	       hasShowClassResult = groupContentWrapperEl.hasClass('accShow');

        	//Accordion is open -> close it
            if(hasShowClassResult){
                //set open status
                element.attr('data-state', 'closed');  
                groupContentWrapperEl.removeClass('accShow');
                if(!arrowIsDown){
                    accArrowEl.addClass('accArrow-down');
                }
                if(arrowIsUp){
                    accArrowEl.removeClass('accArrow-up');
                }

            }else{
                //set open status
                element.attr('data-state', 'open');  
                groupContentWrapperEl.addClass('accShow');
                if(arrowIsDown){
                    accArrowEl.removeClass('accArrow-down');
                }
                if(!arrowIsUp){
                    accArrowEl.addClass('accArrow-up');
                }
            }
        },
        //find all open accordion items and close it.s
        closeAll : function(){
            _this = fnb.hyperion.forms.accordion;
            var elements =  _this.currentGroup.find('* .accGroupItemWrapper[data-state="open"]');
            if(elements.length() > 0){
                elements.each(function(element, index){
                    _this.toggleOpenClose(element);
                });
            }
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.accordion = {};
        }
	};
	//Namespace accordion
	fnb.namespace('forms.accordion', accordion, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Main Forms Button Object
///-------------------------------------------///
(function() { 
    //Init all form modules
	function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.forms.button) {
    		if(fnb.hyperion.forms.button[module].autoInit){
				fnb.hyperion.forms.button[module].init();
			}
    	}
    };
    ///-------------------------------------------///
	/// Main Forms Parent function
	///-------------------------------------------///
	function button() {

	};
	///-------------------------------------------///
	/// Main Forms Button Methods
	///-------------------------------------------///
	button.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Forms button
    	init: function () {
    		console.log('Forms Main Button init');
    		initModules();
        },		
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms = {};
        }
	};

	//Namespace forms
	fnb.namespace('forms.button', button, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Web Button Object
///-------------------------------------------///
(function() {
    //Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '[type="button"]', handler: 'fnb.hyperion.forms.button.web.select(event);'},
    	              			{type: 'frame', listener: document, events:'click', selector: '[data-role="submitButton"]', handler: 'fnb.hyperion.forms.button.web.select(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Button Parent function
	///-------------------------------------------///
	function button() {

	};
	///-------------------------------------------///
	/// Button Methods
	///-------------------------------------------///
	button.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Web button
    	init: function () {
    		console.log('Forms Button init');
    		bindEvents();
        }, 
        //Execute button event
        select: function (event) {
        	//Get target button
        	target = fnb.hyperion.$(event.currentTarget);
        	//Get button settings
        	var dataSettingsString = target.attr("data-settings");
        	//Test if data settings exist
        	if(dataSettingsString){
        		//Convert settings string to object
            	var dataSettingsObject = JSON.parse(dataSettingsString);
            	//Wrap target in selector
            	if(dataSettingsObject[0].target!="") dataSettingsObject[0].target = fnb.hyperion.$(dataSettingsObject[0].target);
            	//Get event that needs to be raised
            	var event = dataSettingsObject[0].event;
            	//Raise specified event
            	if(event!='') fnb.hyperion.controller.raiseEvent(event, dataSettingsObject[0]);
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.button.web = {};
        }
	};
	//Namespace button
	fnb.namespace('forms.button.web', button, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Checkbox Object
///-------------------------------------------///
(function() {
	//Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.checkBox', handler: 'fnb.hyperion.forms.checkBox.checkState(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Checkbox Parent function
	///-------------------------------------------///
	function checkBox() {

	};
	///-------------------------------------------///
	/// Checkbox Methods
	///-------------------------------------------///
	checkBox.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Checkbox
    	init: function () {
    		console.log('Forms Checkbox init');
    		bindEvents();
        }, 
        //Check current state of checkbox
        checkState: function (event) {
        	//Get current checkbox
    		var target = fnb.hyperion.$(event.currentTarget);
    		//Get checkbox state
    		var checkBoxState = target.hasClass('checked');
    		//Get disabled state
    		var idDisabled = (target.attr('data-disabled')) ? (target.attr('data-disabled')=="true") ? true : false : false;
    		//Dont run if disabled
    		if(!idDisabled){
	    		//Switch checkBox States to checked or unchecked
	        	switch(checkBoxState) {
					case true:
						this.setUnChecked(target);
					break;
					default:
						this.setChecked(target);
					break;
					
				};
    		}
        },
        //Set the state of target checkbox to checked
        setChecked: function (target) {
        	//Change data attribute of checkbox
        	target.addClass('checked');
        	//Change form element value
        	target.children(0).attr('checked', "checked");
        },
        //Set the state of target checkbox to unchecked
        setUnChecked: function (target) {
        	//Change data attribute of checkbox
        	target.removeClass('checked');
        	//Change form element value
        	target.children(0).attr('checked','');
        },
        //Set the state of target checkbox to disabled
        setDisabled: function (target) {
        	//Change data attribute of checkbox
        	target.attr('data-state', 'disabled');
        },
        //Get object current value
        getValue: function (element) {
        	//return checkbox state
            var getVal = element.hasClass('checked');
        	return getVal;
        },
        //Set object current value
        setValue: function (element,value) {
        	//Change form element value
        	switch(value) {
				case 'checked':
					this.setChecked(element);
				break;
				default:
					this.setChecked(target);
				break;
			};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.checkBox = {};
        }
	};

	//Namespace checkBox
	fnb.namespace('forms.checkBox', checkBox, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Datepicker Object
///-------------------------------------------///
(function() {
	//Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '#datePicker', handler: 'fnb.hyperion.controller.raiseEvent("showDatePicker",{event:event});'},
    	              {type: 'frame', listener: document, events:'click', selector: '.dateLeftScrollArrow', handler: 'fnb.hyperion.forms.datePicker.datePickerScrollLeft();'},
    	              {type: 'frame', listener: document, events:'click', selector: '.dateRightScrollArrow', handler: 'fnb.hyperion.forms.datePicker.datePickerScrollRight();'},
    	              {type: 'frame', listener: document, events:'click', selector: '.datePickerClose', handler: 'fnb.hyperion.controller.raiseEvent("hideDatePicker",{event:event});'},
    	              {type: 'frame', listener: document, events:'click', selector: '.datePickerScrollerWrapper [data-active="true"]', handler: 'fnb.hyperion.forms.datePicker.select(event);'},
    	              {type: 'frame', listener: document, events:'change', selector: '#datePickerDropdown', handler: 'fnb.hyperion.forms.datePicker.dropdownSelect(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Datepicker Parent function
	///-------------------------------------------///
	function datePicker() {

	};
	///-------------------------------------------///
	/// Datepicker Methods
	///-------------------------------------------///
	datePicker.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Var for calendar state
		active: true,
		//Datepicker wrapper
		datePickerTarget: '',
		//Current datepicker date
		currentDatePickerDate: '',
		//List of month names
		monthNames: ["January","February","March","April","May","June","July","August","September","October","November","December"],
		//List of month names
		shortMonthNames: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
		//List of week days
		weekDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
		//List of days per month
		monthDays: [31,28,31,30,31,30,31,31,30,31,30,31],
		//Get today's date
		today : new Date(),
		//Var amount of months to display
		totalMonths: 9,
		//Datepicker input selector
		datePickerValueTarget: '',
		//Instance of horizontal scroller for datepicker
		dateScroller: '',
		//Datepicker scroller selector
		datePickerWrapper: '',
		//Datepicker scroller wrapper selector
		datePickerScrollerWrapper: '',
		//Datepicker scroller selector
		datePickerScroller: '',
		//Var fordatePicker breakpoints
		breakPoints: [2,1,1],
		//Current breakpoint position
		datePickerBreakpointValue:-2,
		//Val of months in calendar
		monthCount:0,
		//Init FNB Datepicker
    	init: function () {
    		console.log('Forms Datepicker init');
        	//Global Datepicker wrapper selector
        	this.datePickerWrapper = fnb.hyperion.controller.datePickerElement.find('.datePickerContainer');
    		//Datepicker scrollable wrapper
    		this.datePickerScrollerWrapper = this.datePickerWrapper.find('.datePickerScrollerWrapper');
    		//Datepicker scrollable wrapper
    		this.datePickerScroller = this.datePickerScrollerWrapper.children(0);
    		//Bind datepicker events
    		bindEvents();
    		//Extend Controller resize function
    		fnb.hyperion.controller.addResizeFunction(this.resize);
        },
        //Select Date
        select: function (event) {
        	//End date scroll 
        	this.dateScroller.end(event);
        	//Test if event is swipe
        	if(this.dateScroller.moving == false){
        		//Select target with values
        		var target = fnb.hyperion.$(event.currentTarget);
        		//Get year 
        		var year = target.attr('data-year');
        		//Get month 
        		var month = target.attr('data-month');
        		//Get day
        		var date = target.attr('data-date');
        		//Get day name
        		var dayName = target.attr('data-dayName');
        		//Get Monthname
        		var monthName = target.attr('data-monthName');
        		//Create selected date strings
        		var topLabelString = dayName+' '+date;
        		var bottomLabelString = monthName+' '+year;
        		//Set input data value
        		this.datePickerValueTarget.attr('data-value',year+'-'+month+'-'+date);
        		//Set input value
        		this.datePickerValueTarget.val(year+'-'+month+'-'+date);
        		//Set date strings
        		this.datePickerTarget.find('.datePickerLabelTop').html(topLabelString);
        		this.datePickerTarget.find('.datePickerLabelBottom').html(bottomLabelString);
        		//Close datepicker
        		fnb.hyperion.controller.raiseEvent("hideDatePicker",{});
        	}
        },
        //Select dropdown
        dropdownSelect: function (event) {
        	//Select target with values
    		var target = fnb.hyperion.$(event.currentTarget);
    		//Get year 
    		var year = target.val();
    		//Split date
        	var dateArray = this.currentDatePickerDate.split("-");
        	var month= parseInt(dateArray[1]);
        	var day	 = parseInt(dateArray[2]);
        	//Create new date string
        	var dateString = year+'-'+month+'-'+day;
    		//Creat calendar
    		this.create(dateString);
        },
        //Show datepicker
        show: function (obj) {
        	//Get scroll position to set later
			fnb.hyperion.controller.getScrollPosition(document.body);
			//Change Ezi Panel data attribute
			fnb.hyperion.controller.clipOverflow(true);
			//Set datepicker to visible
        	fnb.hyperion.controller.datePickerElement.show();
        	//Select the datePicker target
        	this.datePickerTarget = fnb.hyperion.$(obj.event.currentTarget);
        	//Select datepicker value target
        	this.datePickerValueTarget = this.datePickerTarget.find('=input');
        	//Get datepicker selected date
        	var date = this.datePickerValueTarget.attr('data-value');

        	//Test if new calendar needs to be created
        	if(date!=this.currentDatePickerDate){
        		//Creat calendar
        		this.create(date);
        	}else{
        		//Append calendar wrapper
        		this.setCalendar();
        	}
        	//Flag that calendar is showing
        	this.active = true;
        },
        //Hide calendar
        hide: function () {
        	//Test if datepicker is visible
        	if(this.active){
        		//Flag that calendar is not showing
            	this.active = false;
    			//Change Ezi Panel data attribute
    			fnb.hyperion.controller.clipOverflow(false);
            	//Notify controller to hide the date overlay
            	fnb.hyperion.controller.datePickerElement.hide();
            	//Get scroll position to set later
    			fnb.hyperion.controller.setScrollPosition(document.body);
        	}
        },
        //Create calander
        create: function (date) {
        	//Split date
        	var dateArray = date.split("-");
        	var year = parseInt(dateArray[0]);
        	var month= parseInt(dateArray[1]);
        	var day	 = parseInt(dateArray[2]);
        	//Set datepicker dropodown
        	fnb.hyperion.controller.datePickerDropdown.val(year);
    		//Get amount of months to display
        	this.monthCount = parseInt(this.datePickerTarget.attr('data-months'));
    		//Get start before current month
        	var startBefore = this.datePickerTarget.attr('data-startBefore');
    		//Set currentDatePickerDate
    		this.currentDatePickerDate = date;
    		//Append calendar wrapper
    		this.setCalendar(this.getCalendar(year, month, day, this.monthCount, startBefore));
    		//New Instance of horizontal scroller for datepicker
			this.dateScroller = new fnb.hyperion.horizontalScroller();
			this.dateScroller.scrollableParent = this.datePickerScroller;
			this.dateScroller.scrollerChildren = '.datePickerMonthWrapper';
			this.dateScroller.scrollStopWidth = 200;
			this.dateScroller.scrollSpeed = 5;
			this.dateScroller.moveTreshold = 0.20;
			this.dateScroller.bindEvents();
			this.dateScroller.enabled = true;
			//Events to execute after datepicker stopped scrolling
			this.dateScroller.afterStop = this.checkCalendarPosition;
			//Reset datepicker breakpoint value
			this.datePickerBreakpointValue = -2;
        	//Current window width
        	var windowWidth = fnb.hyperion.controller.window.innerWidth || fnb.hyperion.controller.document.documentElement.clientWidth;
        	//Adjust datepicker onshow
			this.adjustDatePicker(windowWidth, fnb.hyperion.controller.getBreakPosition(windowWidth));
        },
        //Build datepicker
        getCalendar: function (year, month, day, monthCount, startBefore) {
        	//Ensure year, month and day is Int
        	year = parseInt(year);
    		month= parseInt(month);
    		day	 = parseInt(day);
    		//Month to start with
    		var currentMonth = month;
    		//Year to start with
    		var currentYear = year;
    		//Check if datepicker need to start before current month
    		if(startBefore>0){
        		//Get the month to start with
        		var startInt = currentMonth - startBefore;
    			//Test if year skipped
        		if(startInt<1){
        			currentMonth = 12-Math.abs(startInt);
        			currentYear--;
        		}else{
        			currentMonth = startInt;
        		}
    		}
    		//Calendar data
    		var calendarData = [];
    		//Loop total months
    		for(var i=0;i<monthCount;i++) {
    			//Set current month index
    			var currentMonthIndex = currentMonth-1;
    			//Object for month data
    			var monthData = {templateName : 'datePickerMonthWrapper', templateData: {}};
        		//Var for current day of the month increment
        		var currentMonthDaysIncrement = 1;
    			//Get the first day of this month
    			var firstDay = new Date(currentYear,currentMonthIndex,1);
    			var startDay = firstDay.getDay();
    			//Leap year support
    			if(currentYear % 4 == 0) this.monthDays[1] = 29;
    			else this.monthDays[1] = 28;
    			//Get the amount of days in this month
    			var daysInMonth = this.monthDays[currentMonthIndex];
    			//Set month name data
    			monthData.templateData.monthName = this.monthNames[currentMonthIndex];
    			//Set month data
    			monthData.templateData.month = currentMonth;
    			//Set year data
    			monthData.templateData.year = currentYear;
    			//Set current month number
    			monthData.templateData.count = i;
    			//Flag for calendar days to start
    			var startFlag = false;
    			//Rows collection wrapper
				var rowsData = '';
				//Var for month name
				var monthName = this.shortMonthNames[currentMonthIndex];
    			//Create the calender
    			//Loop rows per month
    			for(var j=0;j<5;j++) {
        			//Object for row data with templatename
        			var rowData = [];
    				//Loop days of the week
    				for(var k=0;k<7;k++) {
    					//Each calandar items data
    					var itemData = {templateName : 'datePickerItem', templateData: {}};
    					//If the days has overshooted the number of days in this month, stop writing
    					if(currentMonthDaysIncrement > daysInMonth) startFlag=false;
    					//If the first day of this month has come, start the date writing
    					else if(k >= startDay && !startFlag) startFlag=true;
    					//Set if selected date
    					var selectedVal = false;
    					//Clean date value
    					var dateVal = '&nbsp;';
    					//Clean month value
    					var monthVal = '';
    					//Clean year value
    					var yearVal = '';
    					//Set default active state
    					var activeVal = false;
    					//Start building row
    					if(startFlag) {
    						//Set day value
    						dateVal = currentMonthDaysIncrement;
    						//Set month value
    						monthVal = currentMonth;
    						//Set year value
    						yearVal = currentYear;
    						//Set active value
    						activeVal = true;
            				//Increment current day of the month
            				currentMonthDaysIncrement++;
    					}
    					//Test if selected date
            			if(dateVal==day&&monthVal==month&&yearVal==year) selectedVal = true;
    					//Set item day selected
    					itemData.templateData.dataSelected = selectedVal;
    					//Set item day value
    					itemData.templateData.dayName = this.weekDays[k];
    					//Set item Month name value
    					itemData.templateData.monthName = monthName;
    					//Set item date value
    					itemData.templateData.date = dateVal;
    					//Set item month value
    					itemData.templateData.month = monthVal;
        				//Set item year value
    					itemData.templateData.year = yearVal;
        				//Set item year value
    					itemData.templateData.dataActive = activeVal;
        				//Append item collection
    					rowData.push(itemData);
        			};
        			//Set all rows html
        			rowsData += '<tr>'+fnb.hyperion.controller.getHtmlTemplate(rowData)+'</tr>';
    			}
    			//Set month data
    			monthData.templateData.rows = rowsData;
    			//Set month to calendar
    			calendarData.push(monthData);
    			//Increment Current month
    			currentMonth++;
    			//Test if month in scope
    			if((currentMonth-1)>=12) {
    				currentMonth = 1;
    				currentYear++;
    			}
    			
    		}
    		
    		return fnb.hyperion.controller.getHtmlTemplate(calendarData);
        },
        //Set calendar to dom
        setCalendar: function (html) {
        	//Set visibility
        	this.datePickerWrapper.show();
    		//Set html of datepickerWrapper
        	if(html) this.datePickerScroller.html(html);
        },
        //Check if datepicker needs more months
        checkCalendarPosition: function () {
        	//Child var
    		var child;
    		//Month var
        	var month = '';
    		//Year var
    		var year = '';
        	//Check if datepicker is at the end
        	if(fnb.hyperion.forms.datePicker.dateScroller.currentIndex==fnb.hyperion.forms.datePicker.dateScroller.maxStops){
        		//Get child
        		child = fnb.hyperion.forms.datePicker.datePickerScroller.children[fnb.hyperion.forms.datePicker.datePickerScroller.children().length()-1];
        		//Get month for dates to continue
        		month = parseInt(child.attr('data-month'));
        		//Get year for dates to continue
        		year = parseInt(child.attr('data-year'));
        		//Increase month
        		month++;
        		if(month>12){
        			month = 1;
        			year++;
        		}
        		//Get months needed
        		var newMonths = fnb.hyperion.forms.datePicker.getCalendar(year, month, '', fnb.hyperion.forms.datePicker.datePickerBreakpointValue, 0);
        		//Create DOM node
        		var newNode = document.createElement("div");
        		//Set Node html
        		newNode.html(newMonths);
        		//Append to Calendar
        		var i = fnb.hyperion.forms.datePicker.datePickerBreakpointValue; 
        		while (i--) {
        			fnb.hyperion.forms.datePicker.removeCalendar(fnb.hyperion.forms.datePicker.datePickerScroller.children[0]);
        			//Prepend new child
        			fnb.hyperion.forms.datePicker.appendCalendar(newNode.children[0]);
        		}
        		//Setup horizontal scroller
        		//fnb.hyperion.forms.datePicker.dateScroller.scrollableParent.stop();
        		fnb.hyperion.horizontalScroller.slide(fnb.hyperion.forms.datePicker.dateScroller.scrollableParent).stop();
        		//Center scroller new index
        		fnb.hyperion.forms.datePicker.dateScroller.centerIndex(fnb.hyperion.forms.datePicker.dateScroller.maxStops-1);
        	}
        	//Check if datepicker is at the start
        	else if(fnb.hyperion.forms.datePicker.dateScroller.currentIndex==0){
        		//Get first child
        		child = fnb.hyperion.forms.datePicker.datePickerScroller.children(0);
        		//Get month for dates to continue
        		month = parseInt(child.attr('data-month'));
        		//Get year for dates to continue
        		year = parseInt(child.attr('data-year'));
        		//Test if month in scope
        		if(month==0){
        			month = 12;
        			year--;
        		}
        		//Get months needed
        		var newMonths = fnb.hyperion.forms.datePicker.getCalendar(year, month, '', fnb.hyperion.forms.datePicker.datePickerBreakpointValue, fnb.hyperion.forms.datePicker.datePickerBreakpointValue);
        		//Create DOM node
        		var newNode = document.createElement("div");
        		//Set Node html
        		newNode.innerHTML = newMonths;
        		//Append to Calendar
        		var i = fnb.hyperion.forms.datePicker.datePickerBreakpointValue; 
        		while (i--) {
        			fnb.hyperion.forms.datePicker.removeCalendar(fnb.hyperion.forms.datePicker.datePickerScroller.children(fnb.hyperion.forms.datePicker.datePickerScroller.children().length()-1));
        			//Prepend new child
        			fnb.hyperion.forms.datePicker.prependCalendar(newNode.children[i],fnb.hyperion.forms.datePicker.datePickerScroller.children(0));
        		}
        		//Setup horizontal scroller
        		//fnb.hyperion.forms.datePicker.dateScroller.scrollableParent.stop();
        		fnb.hyperion.horizontalScroller.slide(fnb.hyperion.forms.datePicker.dateScroller.scrollableParent).stop();
        		//Center scroller new index
        		fnb.hyperion.forms.datePicker.dateScroller.centerIndex(1);
        	};
        },
        //Append calendar with months
        appendCalendar: function (html) {
        	this.datePickerScroller.add(html);
        },
        //Prepend calendar with months
        prependCalendar: function (html,child) {
        	this.datePickerScroller.elem.insertBefore(html,child.elem);
        },
        //Remove child from calendar
        removeCalendar: function (child) {
        	this.datePickerScroller.remove(child.elem);
        },
        //Resize datepicker when window resizes
        adjustDatePicker: function (windowSize, breakpoint) {
        	//Var breakpoint index
        	var breakPointIndex = (breakpoint-1<0) ? 0: breakpoint-1;
        	/*Get value of the current breakpoint for amount of months to be shown*/
        	var currentBreakPointValue = this.breakPoints[breakPointIndex];
        	/*Get the scrolling container width*/
        	var currentContainerWidth = this.datePickerScrollerWrapper.outerWidth();
        	/*Calculate the new width for the Months with breakpoint value*/
        	var newMonthWidth = currentContainerWidth/currentBreakPointValue;
        	/*Calculate new width of the horizontal scroller*/
        	var newScrollingWrapperWidth =newMonthWidth*this.monthCount;
        	/*Apply styles*/
        	//Hack for pixel rounding
        	newScrollingWrapperWidth = newScrollingWrapperWidth+(this.monthCount/2);
        	/*Apply Scroller wrapper new width*/
        	this.datePickerScroller.css("width", newScrollingWrapperWidth+'px');
        	//Update the amount the scroller should scroll by
        	this.dateScroller.scrollStopWidth = currentContainerWidth;
        	//Test if breakpoint changed
        	if(this.breakPoints[breakPointIndex]!=this.datePickerBreakpointValue){
        		//Update datepicker breakpoint value
        		this.datePickerBreakpointValue = this.breakPoints[breakPointIndex];
            	//Update horizontal scroller stops
        		this.dateScroller.maxStops = (this.monthCount/this.datePickerBreakpointValue)-1;
        		//Set var to move to
        		var movePos = 4;
    			//Test if single month
    			if(this.datePickerBreakpointValue==1) movePos=8;
    			//Set datepicker horizontal scroller index
    			this.dateScroller.currentIndex = movePos;
    			//Calculate left position
    			var leftPos = -currentContainerWidth*movePos;
    			//Move to correct month
    			this.datePickerScroller.css("left", leftPos+'px');
    			//Update scroller x position
    			this.dateScroller.x = leftPos;
        	}
        },
        //Scroll months right
        datePickerScrollRight: function () {
        	this.dateScroller.next();
        },
        //Scroll months left
        datePickerScrollLeft: function () {
        	this.dateScroller.previous();
        },
        //Resize datepicker when window resizes
        resize: function (windowSize, breakpoint) {
        	//if datepicker exists do adjust
        	if(fnb.hyperion.forms.datePicker.active){
        		fnb.hyperion.forms.datePicker.adjustDatePicker(windowSize,breakpoint);
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.datePicker = {};
        }
	};

	//Namespace datePicker
	fnb.namespace('forms.datePicker', datePicker, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Dropdown Object
///-------------------------------------------///
(function() {
	//Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.dropdownSelected', handler: 'fnb.hyperion.forms.dropDown.checkState(event);', focusOutEvent: 'fnb.hyperion.forms.dropDown.close(event);'},
    	              {type: 'frame', listener: document, events:'click', selector: '.dropdownItem', handler: 'fnb.hyperion.forms.dropDown.select(event);'},
    	              {type: 'frame', listener: document, events:'click', selector: '.dropdownCarat', handler: 'fnb.hyperion.forms.dropDown.checkState(event);'},
    	              {type: 'frame', listener: document, events:'change', selector: '.dropdownItemSelectWrapper', handler: 'fnb.hyperion.forms.dropDown.select(event);'},
    	              {type: 'frame', listener: document, events:'keyup', selector: '.dropdownInput', handler: 'fnb.hyperion.forms.dropDown.filter(event);'},
    	              {type: 'frame', listener: document, events:'focus', selector: '.dropdownInput', handler: 'fnb.hyperion.forms.dropDown.dropdownInputFocusIn(event);'},
    	              {type: 'frame', listener: document, events:'blur', selector: '.dropdownInput', handler: 'fnb.hyperion.forms.dropDown.dropdownInputFocusOut(event);'}
    	              ];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// dropDown Parent function
	///-------------------------------------------///
	function dropDown() {

	};
	///-------------------------------------------///
	/// dropDown Methods
	///-------------------------------------------///
	dropDown.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Dropdown Target parent
		dropdownSelect: '',
		//Dropdown Target parent
		dropdownBottomOffset: 78,
		//Dropdown Target parent
		dropdownParent: '',
		//Dropdown Target parent
		dropdownTarget: '',
		//Dropdown Caret
		dropdownCaret: '',
		//Dropdown datarows attribute
		dataRows: '',
		//Dropdown selected dropdown element
		dropdownSelectedElement: '',
		//dropdownTarget Parent
		dropdownOptionsParent: '',
		//dropdownOptionsParent Parent
		dropdownOptionsParentWrapper: '',
		//dropDown selected value
		dropdownSelectedValue: '',
		//Dropdown options list
		dropdownOptions: {},
		//Dropdown filtered flag
		filtered: false,
		//Var for dropdown is active
		active: false,
		//Init FNB Dropdown
    	init: function () {
    		console.log('Forms Dropdown init');
    		//Bind Dropdown events
    		bindEvents();
    		//Attach event to page load complete event sequence
    		fnb.hyperion.controller.attachPageEvent('fnb.hyperion.forms.dropDown.setDropdowns()','');
        },
        //Open dropdown
        checkState: function (event) {
        	//Get the target that triggered the event
        	var tempTarget = fnb.hyperion.$(event.currentTarget);
        	//Get the correct dropdown target
        	this.dropdownTarget = (tempTarget) ? (tempTarget.hasClass('dropdownCarat')) ? tempTarget.parent().find('.dropdownSelected') : tempTarget : fnb.hyperion.$(event.target);
        	//Get the state of the current selected dropdown
        	var dropdownState = (this.dropdownTarget.hasClass('closed')) ? 'closed' : 'open';
        	//Get the disabled state
        	var dropdownDisabledState = (this.dropdownTarget.attr('data-disabled')) ? (this.dropdownTarget.attr('data-disabled') == "true") ? true : false : false;
        	//Get the amount of rows to display when the dropdown opens
        	this.dataRows = parseInt(this.dropdownTarget.attr('data-rows'));
        	//Test for number else make 0
        	this.dataRows = isNaN(this.dataRows) ? 0 : this.dataRows;
        	//Check if dropdown is disabled
        	if(!dropdownDisabledState){
            	//Switch Dropdown States to open or close the dropdown
            	switch(dropdownState) {
    				case 'open':
    					this.close(event);
    				break;
    				case 'closed':
    					this.open(event);
    				break;
    				default:
    				
    			};
        	}

        },
        //Open dropdown
        open: function () {
        	//Check if another dropdown is open
        	if(this.active) this.close();
        	//Set dropdoen has active element
        	this.active = true;
        	//Main dropdown wrapper seletion
        	this.dropdownParent = this.dropdownTarget.parent();
        	//Select dropdownCaret
        	this.dropdownCaret = this.dropdownParent.find('.dropdownCarat');
    		//Select ul element to target
    		this.dropdownOptionsParent = this.dropdownParent.find('=ul');
    		//Select dropdown select element
    		this.dropdownSelect = this.dropdownParent.find('=select');
    		//Select dropdown ul parent
    		this.dropdownOptionsParentWrapper = this.dropdownOptionsParent.parent();
    		//Set Dropdown content height
    		this.setContentHeight();
    		//Remove transparency from ul parent and add border and shadow
    		this.dropdownOptionsParentWrapper.addClass('open');
    		this.dropdownOptionsParentWrapper.attr('data-transparent', 'false');
    		//Change dropdown state to open
    		this.dropdownTarget.removeClass('closed');
    		this.dropdownTarget.addClass('open');
    		//Change caret state
    		this.dropdownCaret.removeClass('closed');
    		this.dropdownCaret.addClass('open');
    		
        },
        //Close dropdown
        close: function (target) {
        	//Declare var for dropdown input
        	var isDropdownInput = false;
        	//Test for valid event
        	if(target){
        		//Get target dropdown item
            	var target = target.currentTarget;
            	//Test to see if the dropdown input was selected
            	isDropdownInput = (fnb.hyperion.$(target).hasClass('dropdownInput')) ? true : false;
        	}
        	//Test for dropdownOptionsParentWrapper declaration
        	if(this.active==true&&this.dropdownOptionsParentWrapper!=''&&!isDropdownInput){
        		//Reset dropdown
        		this.resetDropdown();
        	}
        },
        //Select dropdown value
        select: function (target) {
        	//Target li
        	this.dropdownSelectedElement = fnb.hyperion.$(target.currentTarget);
        	//Select dropdown parent
        	var dropdownParent = this.dropdownSelectedElement.parent().parent();
        	//Switch touch and dropdown types
        	switch(fnb.hyperion.controller.isMobile) {
				case true:
					var htmlString = '';
					//Switch Dropdown types
		        	switch(dropdownParent.attr('data-type')) {
						case 'singleTier':
							//Create html wrapper for single tier dropdown options
							htmlString = this.dropdownSelectedElement.prop('options')[this.dropdownSelectedElement.prop('selectedIndex')].html();
						break;
						case 'threeTier':
							//Create html wrapper for three tier dropdown options
							htmlString = '<span data-role="dropdownItemLabel">'+this.dropdownSelectedElement.prop('options')[this.dropdownSelectedElement.prop('selectedIndex')].parent().getAttribute('label')+'</span><span data-role="dropdownItemLabel">'+this.dropdownSelectedElement.prop('options')[this.dropdownSelectedElement.prop('selectedIndex')].html()+'</span>';
						break;
						default:
						
					};
					dropdownParent.parent().find('=span').get(1).html(htmlString);
				break;
				default:           	
					//Check if the dropdown search input was selected
		        	if(this.dropdownSelectedElement.attr('data-type')) return;
					//Deselct other options
	        		this.dropdownSelectedElement.parent().find('*li').attr('data-selected', 'false');
				    this.dropdownSelectedElement.parent().find('*li').show();
					//Add Selected attr
	        		this.dropdownSelectedElement.attr('data-selected', 'true');
					//Get the value of the selected item
	        		var selectedValue = this.dropdownSelectedElement.attr('data-value');
		        	//Set the dropdown value
		        	this.dropdownSelect.val(selectedValue);
		        	//Get the content of the selected item
		        	var selectedContent = this.dropdownSelectedElement.html();
		        	//Set the content of the display label
		        	this.dropdownTarget.html(selectedContent);
		        	//Test for three tier dropdown
					if(dropdownParent.parent().attr('data-type')=='threeTier'){
						//Get amount
						fnb.hyperion.forms.dropDown.checkForBalance(dropdownParent.parent());
					}
					//Close dropdown
					fnb.hyperion.forms.dropDown.close(target);
			};
        },
        //Set Dropdown Content height
        setContentHeight: function () {
    		//Select list of all options that are going to be displayed
        	var visibleDropdownOptions = this.dropdownParent.find('*li[data-visible="true"]');
        	//Test for number else make 0
        	var dropdownOptionsLength = isNaN(visibleDropdownOptions.length()) ? 1 : visibleDropdownOptions.length();
        	//Var for dropdown height
        	var dropdownHeight = 0;
        	//Get dropdown search
        	var firstDropdownHeight = visibleDropdownOptions.first().outerHeight();
    		//Add scrollable if the options are more that the rows to be displayed & is not touch device
        	if(dropdownOptionsLength>this.dataRows&&this.dataRows!=0){
        		//Add Scrollable data attribute to selected ul
        		this.dropdownOptionsParent.attr('data-scrollable', 'scrollable');
        		//Calculate scrollable height
        		dropdownHeight = (visibleDropdownOptions.last().outerHeight()*(this.dataRows-1))+firstDropdownHeight;
        		//Apply new height to ul parent
        		this.dropdownOptionsParentWrapper.css('height',dropdownHeight+'px');
        	}else if(this.dataRows==0||dropdownOptionsLength<=this.dataRows){
        		//Calculate scrollable height
        		dropdownHeight = (visibleDropdownOptions.last().outerHeight()*(dropdownOptionsLength-1))+firstDropdownHeight;
        		//Apply new height to ul parent
        		this.dropdownOptionsParentWrapper.css('height',dropdownHeight+'px');
        	}
        	//Get dropdown position
        	var dropdownPosition = this.dropdownParent.position();
        	//Get document height
    		var documentHeight = fnb.hyperion.getDocumentHeight();
        	//Calculate overflow position
    		var totalPosition = dropdownPosition.objectPosY+this.dropdownBottomOffset+dropdownHeight;
        	//Add reverese class if dropdown overflows
    		if(totalPosition>documentHeight) this.dropdownOptionsParentWrapper.addClass('dropdownReverse');
        },
        //Set Dropdowns
        setDropdowns: function () {
			//Select all dropdowns
			var pageDropdowns = fnb.hyperion.$("=select");
			//Loop dropdowns and set data attribute for devices
			if(pageDropdowns.length()>0){
				//Test if only one dropdown has been found
				if(pageDropdowns.type()!='SELECT'){
					//Select all the dropdowns on the page
					pageDropdowns.each(function(element){
						//Get element to change
						fnb.hyperion.forms.dropDown.checkAttributes(element);
					});
				}else{
					//Get element to change
					fnb.hyperion.forms.dropDown.checkAttributes(pageDropdowns);
				}
				
			};
        },
        //Check element attributes
        checkAttributes: function (element) {
        	//Get element to change
			var dropdownParent = element.parent().parent();
			//Get parent type
			var dropdownParentType = dropdownParent.attr('data-type');
			//Get if amounts has been set
			var dropdownParentSet = dropdownParent.attr('data-set');
			//Test for three tier dropdown
			if(dropdownParentType=='threeTier'&&dropdownParentSet!="true"){
				//Add data set attribute
				dropdownParent.attr('data-set','true');
				//Get amount
				fnb.hyperion.forms.dropDown.checkForBalance(dropdownParent);
			}
        	/*If not a touch device*/
			if(!fnb.hyperion.controller.isMobile){
				//Test element for attribute
				if(dropdownParent.attr("data-event") != null){
					//Set attribute
					if(dropdownParent.attr('data-event')!='click') dropdownParent.attr('data-event', 'click');
				};
			};
        },
        //Load balance into dropdown if needed
        checkForBalance: function (dropdownParent) {
        	//Get amounts wrapper
			var amountsWrapper = dropdownParent.find('.dropdownSelected').find('*[data-type="amountsWrapper"]');
			//Test for amounts wrapper
			if(amountsWrapper.length()>0){
				//Get data url
				var amountsUrl = amountsWrapper.attr('data-url');
				//Test for url and load amounts
				if(amountsUrl){
					//Ajax load amount
					fnb.hyperion.forms.dropDown.loadBalance(amountsWrapper, amountsUrl);
				};
			};

        },
        //Load balance into dropdown if needed
        loadBalance: function (target,url) {
        	//Setup load object
        	var loadObject = {url: url, target: target, async: true};
        	//Request ajax load from controller
        	fnb.hyperion.controller.raiseEvent('asyncLoadContent', loadObject);
        },
        //Set Dropdowns
        resetDropdown: function () {
    		/*Apply new height to ul parent*/
    		this.dropdownOptionsParentWrapper.css('height','0px');
    		//add transparency from ul parent and remove border and drop shadow
    		this.dropdownOptionsParentWrapper.attr('data-transparent', 'true');
    		this.dropdownOptionsParentWrapper.removeClass('open');        		
    		//Change dropdown state to open
    		this.dropdownTarget.removeClass('open');
    		this.dropdownTarget.addClass('closed');
    		//Change caret state
    		this.dropdownCaret.removeClass('open');
    		this.dropdownCaret.addClass('closed');
    		//Reset dropdown if filtered
    		if(this.filtered) this.resetFilter();
        	//Set dropdown has active element
    		this.active = false;
        },
        //Filter dropdown options
        filter: function (event) {
        	//Select all dropdown options
        	var dropdownOptions = this.dropdownParent.find('*li');
        	//Get target
        	var target = fnb.hyperion.$(event.boundTarget);
           	//Get value to filter
            var searchValue = target.val().toLowerCase();
        	//Loop dropdown options
            for (var i = 1; i < dropdownOptions.length(); i++) {
            	//Select element
                var element = dropdownOptions[i];
                var contentContainers = element;
                //Check if the element has children
                if(element.children().length() > 0) contentContainers = element.find('=span'); 
            
                var filterParent = false;
            	
               if(contentContainers.length() > 0){
            	 //Loop through content and try match
            	   for (var x = 0; x < contentContainers.length(); x++) {
            		   var content = contentContainers[x].html().toLowerCase();
            		   if(content.indexOf(searchValue)>-1||searchValue=='') filterParent = true;
            	   }
            	   
               }else{
            	   var content = contentContainers.html().toLowerCase();
            	   if(content.indexOf(searchValue)>-1 || searchValue=='') filterParent = true;
               }
				//Set item wrapper visibility true or false
                if (filterParent) {
                	element.show();
                } else {
                	element.hide();
                }
            }
            //Set filtered flag
            this.filtered = true;
            //Reset content height
            this.setContentHeight();
        },
        //Reset dropdown filter
        resetFilter: function () {
        	//Select all dropdown options
        	var dropdownOptions = this.dropdownParent.find('*li');
        	//Reset dropdown input
        	var dropdownInput = this.dropdownParent.find('.dropdownInput');
        	//Get dropdown input mask value
        	var maskValue = dropdownInput.attr('data-placeholderValue');
        	//Reset dropdown input mask value
        	dropdownInput.val(maskValue);
        	//Reset dropdown input placeholder attr
        	dropdownInput.attr('data-placeholderis','true');
        	//Loop dropdown options
            for (var i = 1; i < dropdownOptions.length; i++) {
            	//Select element
                var element = dropdownOptions[i];
                //Add visible attribute
                element.show();
            }
            //Set filtered flag
            this.filtered = false;
            //Reset content height
            this.setContentHeight();
        },
        //Method for dropdown input focus in
        dropdownInputFocusIn: function (event) {
        	//Get target
        	var target = fnb.hyperion.$(event.boundTarget);
        	//Get input value
        	var value = target.val();
        	//Get mask value
        	var maskValue = target.attr('data-placeholder');
        	//Test if mask value is = to value
        	if(value==maskValue){
        		//Set data-mask attribute
        		target.attr('data-placeholderIs','false');
        		//Clear input value
        		target.val("");
        	}
        },
        //Method for dropdown input focus out
        dropdownInputFocusOut: function (event) {
        	//get target
        	var target = fnb.hyperion.$(event.boundTarget);
        	//Get input value
        	var value = target.val();
        	//Get mask value
        	var maskValue = target.attr('data-placeholderValue');
        	//Test if mask value is = nothing
        	if(value==''){
        		//Set data-mask attribute
        		target.attr('data-placeholderIs','true');
        		//Set input value
        		target.val(maskValue);
        	}
        	//Reset dropdown
    		this.resetDropdown();
        },
        //Get object current value
        getValue: function (element, handler) {
        	//Get value
        	var value = element.val();
        	//Create dropdown callback
        	return handler(value);
        },
        //Set object current value
        setValue: function (element, value) {
        	element.value = value;
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.dropDown = {};
        }
	};

	//Namespace dropDown
	fnb.namespace('forms.dropDown', dropDown, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Input Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click keypress keyup keydown blur focusout', selector: '.input', handler: 'fnb.hyperion.forms.input.delegate(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    	
    };
    //Init input modules
    function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.forms.input) {
    		if(fnb.hyperion.forms.input[module].autoInit){
    			fnb.hyperion.forms.input[module].init();
			}
    	}
    };
    //Set input mask
    function setMask(target) {
    	//Get input value
    	var value = target.val();
    	//Get mask value
    	var maskValue = target.attr('data-placeholder');
    	//Test if mask value is = nothing
    	if(value==''){
    		//Set data-mask attribute
    		target.attr('data-placeholderIs','true');
    		//Set input value
    		target.val(maskValue);
    	}
    };
    //Clear input mask
    function clearMask(target) {
    	//Get input value
    	var value = target.val();
    	//Get mask value
    	var maskValue = target.attr('data-placeholder');
    	//Test if mask value is = to value
    	if(value==maskValue){
    		//Set data-mask attribute
    		target.attr('data-placeholderIs','false');
    		//Clear input value
    		target.val("");
    	}
    };
	///-------------------------------------------///
	/// Input Parent function
	///-------------------------------------------///
	function input() {

	};
	///-------------------------------------------///
	/// Input Methods
	///-------------------------------------------///
	input.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Array with list of always valid keycodes e.g Del
		validKeyCodes: [8,9,35,36,37,39,46],
		//Array with list of Numbers keycodes
		numberKeyCodes: [110,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105],
		//Array with list of Valid special characters
		specialCharKeyCodes: [190],
		//Array with list of Valid keycodes when the ctrl key is down
		ctrlDownKeyCodes: [67,86,88],
		//Init FNB Input
    	init: function () {
    		console.log('Forms Input init');
    		//Bind innput events
    		bindEvents();
    		//Init child modules
    		initModules();
        },
        //Bind event for current object
        delegate: function (event) {
        	//Valid event flag
        	var validEvent = true;
        	//Get the current event type
        	var eventType = event.type;
        	//Get target
        	var target = event.target || event.srcElement;
        	//Get keycode
    		var keyCode = event.charCode||event.keyCode||event.which;
    		//Reset event type
    		eventType = (eventType=="keyup"&&keyCode==9) ? "focusin" : eventType;
        	//Select target
        	target = fnb.hyperion.$(target);
        	//Get current target type of input
        	var targetType = target.attr('data-type');
        	//Get current target type of input
        	var targetHasPlaceholder = target.attr('data-placeholderIs');
        	//Switch input type
        	switch(targetType)
			{
				case 'masked':
					//Switch event type
					switch(eventType)
					{
						case  'click':
							//validEvent = fnb.hyperion.forms.input.mask.focus(event);
						break;
						case 'keypress':
							
						break;
						case  'keyup':
							//validEvent = fnb.hyperion.forms.input.mask.keyUp(event);
						break;
						case  'keydown':
							//validEvent = fnb.hyperion.forms.input.mask.keyDown(event);
							validEvent = fnb.hyperion.forms.input.number.keyDown(event);
						break;
					}
					
				break;
				case  'number':
					//Switch event type
					switch(eventType)
					{
						case 'keypress':
							
						break;
						case  'keyup':
							
						break;
						case  'keydown':
							validEvent = fnb.hyperion.forms.input.number.keyDown(event);
						break;
						case  'click':
							
						break;
						case  'blur':
							
						break;
					}
				break;
			};

			//Do focus events
        	switch(eventType)
			{
				case  'focusin':
				case  'click':
					
					if(targetHasPlaceholder=="true") clearMask(target); return true;
					
				break;
				case  'focusout':
				case  'blur':

					if(targetHasPlaceholder=="false") setMask(target);
					
				break;
			}
        	
			if(!validEvent) event.preventDefault();
        	
        },
        //Get object current value
        getValue: function () {
        	
        },
        //Set object current value
        setValue: function () {

        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.input = {};
        }
	};

	//Namespace input
	fnb.namespace('forms.input', input, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Mask Input Object
///-------------------------------------------///
(function() {
	//Current focused input/textArea
	activeInput='';
	//Default is number
	isNumber = true;
	//Default 2 decimals
	decimals = 2;
	//Default decimal points
	decimalsPoints = '.';
	//Default thousands seperator
	thousandsSeperator = ' ';
	//Unicode decimal var
	unicodeDecimal = '';
	//Regular expression decimal number
	regexDecimalNumber = '';
	//Regular expression decimal
	regexDecimal = '';
	//Test if keycode is allowed
	function value(input,value) {
		//Get input data
		var data = input.numFormat;

		var thousandsSeperator = (data) ? (typeof data.thousandsSeperator === 'undefined') ? thousandsSeperator : data.thousandsSeperator : thousandsSeperator;
		var decimalsPoints = (data) ? (typeof data.decimalsPoints === 'undefined') ? decimalsPoints : data.decimalsPoints : decimalsPoints;
		var decimals = (data) ? (typeof data.decimals === 'undefined') ? decimals : data.decimals : decimals;
		
		//If there is no value its a set otherwise its a get
		if(value){
			return input.val(format(null,value,decimals,decimalsPoints,thousandsSeperator));
		}else{
			var number;

			if(input.val() === '') return '';
			
			number = +(input.val().replace( data.regexDecimalNumber, '' ).replace( data.regexDecimal, '.' ));
		
			return ''+(isFinite(number) ? number : 0);
		}
	}
	//Format number as string
	function format(target,number,decimals,decimalsPoints,thousandsSeperator){

		//Set the default values
		thousandsSeperator = (typeof thousandsSeperator === 'undefined') ? ' ' : thousandsSeperator;
		decimalsPoints = (typeof decimalsPoints === 'undefined') ? '.' : decimalsPoints;
		decimals = !isFinite(+decimals) ? 2 : Math.abs(decimals);

		//Get unicode representation for the decimal place and thousand sep.	
		var unicodeDecimal = ('\\u'+('0000'+(decimalsPoints.charCodeAt(0).toString(16))).slice(-4));
		var unicodeSeperator = ('\\u'+('0000'+(thousandsSeperator.charCodeAt(0).toString(16))).slice(-4));
		
		//Fix the number to actual number.
		number = (number + '')
			.replace('\.', decimalsPoints)
			.replace(new RegExp(unicodeSeperator,'g'),'')
			.replace(new RegExp(unicodeDecimal,'g'),'.')
			.replace(new RegExp('[^0-9+\-Ee.]','g'),'');
		
		var tempNumber = !isFinite(+number) ? 0 : +number,
		    formattedString = '',
		    toFixedFix = function (tempNumber, decimals) {
		        var k = Math.pow(10, decimals);
		        return '' + Math.round(tempNumber * k) / k;
		    };
		
		//Fix for IE parseFloat(0.55).toFixed(0) = 0;
		formattedString = (decimals ? toFixedFix(tempNumber, decimals) : '' + Math.round(tempNumber)).split('.');
		if (formattedString[0].length > 3) {
			formattedString[0] = formattedString[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, thousandsSeperator);
		}
		if ((formattedString[1] || '').length < decimals) {
			formattedString[1] = formattedString[1] || '';
			formattedString[1] += new Array(decimals - formattedString[1].length + 1).join('0');
		}

		if(target){
			fnb.hyperion.$('#'+target).val(formattedString.join(decimalsPoints));
		}else{
			return formattedString.join(decimalsPoints);
		}
	}
	//Test if keycode is allowed
	function allowKeyPress(event) {
		//Get keycode
		var keyCode = event.charCode||event.keyCode||event.which;
		//Test if shift key is down
		if(!event.shiftKey){
			//Test if valid
			if(fnb.hyperion.forms.input.numberKeyCodes.indexOf(keyCode)>-1||fnb.hyperion.forms.input.validKeyCodes.indexOf(keyCode)>-1||fnb.hyperion.forms.input.specialCharKeyCodes.indexOf(keyCode)>-1||event.ctrlKey&&fnb.hyperion.forms.input.ctrlDownKeyCodes.indexOf(keyCode)>-1){
				return true;
			};
		};
		return false;
	}
	//Method for selecting a range of characters in an input/textarea.
	function setSelectionRange(input,start,end)
	{
		//Check which way we need to define the text range.
		if( this.createTextRange )
		{
			var range = input.createTextRange();
				range.collapse(true);
				range.moveStart( 'character',start);
				range.moveEnd( 'character',end-start);
				range.select();
		}
		//Check browser support
		else if(input.setSelectionRange)
		{
			input.focus();
			input.setSelectionRange(start,end);
		}
	}
	//Get the selection position for the given part.
	function getSelection(input,part)
	{
		var pos	= input.val().length;
		//Get selection
		part = ( part.toLowerCase() == 'start' ? 'Start' : 'End' );

		if(document.selection){
			//The current selection
			var range = document.selection.createRange(), storedRange, selectionStart, selectionEnd;
			//Temp holder
			storedRange = range.duplicate();
			//stored_range.moveToElementText( this );
			storedRange.expand('textedit');
			//Move Temp end point to end point of original range
			storedRange.setEndPoint( 'EndToEnd', range );
			//Calculate start and end points
			selectionStart = storedRange.text.length - range.text.length;
			selectionEnd = selectionStart + range.text.length;
			return part == 'Start' ? selectionStart : selectionEnd;
		}
		else if(typeof(input['selection'+part])!="undefined")
		{
		 	pos = input['selection'+part];
		}
		return pos;
	}
	function getCaretCharacterOffsetWithin(element) {
		 // Initialize
		  var iCaretPos = 0;

		  // IE Support
		  if (document.selection) {

		    // Set focus on the element
			element.focus ();

		    // To get cursor position, get empty selection range
		    var oSel = document.selection.createRange ();

		    // Move selection start to 0 position
		    oSel.moveStart ('character', -element.val().length);

		    // The caret position is selection length
		    iCaretPos = oSel.text.length;
		  }

		  // Firefox support
		  else if (element.selectionStart || element.selectionStart == '0')
		    iCaretPos = element.selectionStart;

		  // Return results
		  return (iCaretPos);
	}
	///-------------------------------------------///
	/// Mask Parent function
	///-------------------------------------------///
	function mask() {

	};
	///-------------------------------------------///
	/// Mask Methods
	///-------------------------------------------///
	mask.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Currency
		init: function () {
			console.log('Forms input mask init');
		},
		//Get key up
		focus: function (event) {
			//Get input
			activeInput = fnb.hyperion.$(event.currentTarget);
			//Get caret start position
			var start = getCaretCharacterOffsetWithin(activeInput);

			//Test if input has been initialized
			if(!activeInput.numFormat){
				//Get active input
				activeInput.numFormat = {init:0,counter:0};
				//Get if is number
				activeInput.numFormat.isNumber = (activeInput.attr('data-isNumber')=='true') ? true : false;
				//Get decimals
				activeInput.numFormat.decimals = activeInput.attr('data-decimals') ? parseInt(activeInput.attr('data-decimals')) : decimals;
				//Get decimal point value
				activeInput.numFormat.decimalsPoints = activeInput.attr('data-decimalsPoints') ? activeInput.attr('data-decimalsPoints') : decimalsPoints;
				//Get thousands seperator value
				activeInput.numFormat.thousandsSeperator = activeInput.attr('data-thousandsSeperator') ? activeInput.attr('data-thousandsSeperator') : thousandsSeperator;
				//Work out the unicode character for the decimal placeholder.
				activeInput.numFormat.unicodeDecimal = ('\\u'+('0000'+(activeInput.numFormat.decimalsPoints.charCodeAt(0).toString(16))).slice(-4)),
				activeInput.numFormat.regexDecimalNumber = new RegExp('[^'+activeInput.numFormat.unicodeDecimal+'0-9]','g'),
				activeInput.numFormat.regexDecimal = new RegExp(activeInput.numFormat.unicodeDecimal,'g');
			}

		},
		//Get key down
		keyDown: function (event) {
			//Test if key is allowed
			if(!allowKeyPress(event)) return false;
			//Get active input
			var currentInput = fnb.hyperion.$(event.currentTarget);
			var startfg = getCaretCharacterOffsetWithin(currentInput);

			//Get input data
			var data = currentInput.numFormat;
			//Get keycode
			var code = (event.keyCode ? event.keyCode : event.which);

			//Get character from code
			var character = String.fromCharCode(code);
			//Get caret start position
			var start = getSelection(currentInput,'start');

			//Get caret end position
			var end = getSelection(currentInput,'end');

			//Declare new val variable
			var val	= '';
			//Declare new setPos variable
			var setPos = false;
			//Everything is selected or its emptry
			if(start == 0 && end == currentInput.val().length||value(currentInput) == 0)
			{

				if(code === 8)
				{

					//Blank out the input
					start = end = 1;
					currentInput.val('');
					//Reset the cursor position.
					data.init = (data.decimals>0?-1:0);
					data.counter = (data.decimals>0?-(data.decimals+1):0);
					setSelectionRange(0,0);
				}
				else if(character === data.decimalsPoints)
				{

					start = end = 1;
					currentInput.val('0'+ data.decimalsPoints + (new Array(data.decimals+1).join('0')));
					//Reset the cursor position.
					data.init = (data.decimals>0?1:0);
					data.counter = (data.decimals>0?-(data.decimals+1):0);
				}
				else if(currentInput.val().length === 0 )
				{

					//Reset the cursor position.
					data.init = (data.decimals>0?-1:0);
					data.counter = (data.decimals>0?-(data.decimals):0);
				}
			}
			//Reset the caret position based on the users selection.
			else
			{

				data.counter = end-currentInput.val().length;
			}
			//If the start position is before the decimal point,
			if(data.decimals > 0 && character == data.decimalsPoints && start == currentInput.val().length-data.decimals-1 )
			{

				data.counter++;
				data.init = Math.max(0,data.init);
	        	//Prevent default event
				event.preventDefault();
				// Set the selection position.
				setPos = currentInput.val().length+data.counter;
			}
			//If the user is just typing the decimal place
			else if(character == data.decimalsPoints)
			{
				data.init = Math.max(0,data.init);
	        	//Prevent default event
				event.preventDefault();
			}
			//If hitting the delete key, and the cursor is behind a decimal place,
			else if(data.decimals > 0 && code == 8 && start == currentInput.val().length-data.decimals)
			{
	        	//Prevent default event
				event.preventDefault();
				data.counter--;
				// Set the selection position.
				setPos = currentInput.val().length+data.counter;
			}
			//If hitting the delete key, and the cursor is to the right of the decimal
			else if(data.decimals > 0 && code == 8 && start > currentInput.val().length-data.decimals)
			{
				if(currentInput.val('')) return;
				// If the character preceeding is not already a 0,
				if(currentInput.val().slice(start-1, start) != '0' )
				{
					val = currentInput.val().slice(0, start-1) + '0' + currentInput.val().slice(start);
					value(currentInput,val.replace(data.regexDecimalNumber,'').replace(data.regexDecimal,data.decimalsPoints));
				}
	        	//Prevent default event
				event.preventDefault();
				data.counter--;
				// Set the selection position.
				setPos = currentInput.val().length+data.counter;
			}
			// Step over thousand seperator
			else if(code == 8 && currentInput.val().slice(start-1, start) == data.thousandsSeperator)
			{
	        	//Prevent default event
				event.preventDefault();
				data.counter--;
				// Set the selection position.
				setPos = currentInput.val().length+data.counter;
			}
			// If the caret is to the right of the decimal place, and the user is entering a number, remove the following character before putting in the new one. 
			else if(data.decimals > 0 && start == end && currentInput.val().length>data.decimals+1 &&start>currentInput.val().length-data.decimals-1 && isFinite(+character) &&!event.metaKey && !event.ctrlKey && !event.altKey && character.length === 1)
			{
				// If the character preceeding is not already a 0,
				// replace it with one.
				if( end === currentInput.val().length )
				{
					val = currentInput.val().slice(0, start-1);
				}
				else
				{
					val = currentInput.val().slice(0, start)+currentInput.val().slice(start+1);
				}
				// Reset the position.
				currentInput.val(val);
				setPos = start;
			}
			// If we need to re-position the characters.
			if(setPos!==false)
			{
				setSelectionRange(currentInput,setPos,setPos);
			}
			// Store the data on the element.
			currentInput.numFormat = data;
		},
		//Get key up
		keyUp: function (event) {
			//Get active input
			var currentInput = fnb.hyperion.$(event.currentTarget);
			//Get input data
			var data = currentInput.numFormat;
			//Get keycode
			var code = (event.keyCode ? event.keyCode : event.which);
			//Get caret start position
			var start = getSelection(currentInput,'start');
			//Declare new setPos variable
			var setPos;
			//Re-format the input
			value(currentInput,value(currentInput));
			//Test for decimals
			if(data.decimals>0)
			{
				//If we haven't marked this item as 'initialised'
				if(data.init<1)
				{
					start = currentInput.val().length-data.decimals-( data.init < 0 ? 1 : 0 );
					data.counter = start-currentInput.val().length;
					data.init = 1;
					
					currentInput.numFormat = data;
				}
				//Increase the cursor position if the caret is to the right
				else if( start > currentInput.val().length-data.decimals && code != 8 ||code == 39)
				{

					data.counter++;
					// Store the data
					currentInput.numFormat = data;
				}else if(code == 37){
					data.counter--;
					// Store the data
					currentInput.numFormat = data;
				}
			}
			//Calculate new position
			setPos = currentInput.val().length+data.counter;
			//Set the selection position.
			setSelectionRange(currentInput,setPos,setPos);
		},
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.forms.input.mask = {};
		}
	};
	//Namespace forms.input.mask
	fnb.namespace('forms.input.mask', mask, true);
	//Expose format
	fnb.hyperion.forms.input.mask.value = value;
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Number Input Object
///-------------------------------------------///
(function() {
	//Test if keycode is allowed
	function allowKeyPress(event) {
		//Get keycode
		var keyCode = event.charCode||event.keyCode||event.which;
		//Count fullstops
		var fullstop = event.currentTarget.value.split(".").length;
		//Test if shift key is down
		if(!event.shiftKey&&fullstop>2){
			//Test if valid
			if(fnb.hyperion.forms.input.numberKeyCodes.indexOf(keyCode)>-1||fnb.hyperion.forms.input.validKeyCodes.indexOf(keyCode)>-1||event.ctrlKey&&fnb.hyperion.forms.input.ctrlDownKeyCodes.indexOf(keyCode)>-1){
				return true;
			};
		};

		return false;
	};
	///-------------------------------------------///
	/// Number Parent function
	///-------------------------------------------///
	function number() {

	};
	///-------------------------------------------///
	/// Number Methods
	///-------------------------------------------///
	number.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Number
		init: function () {
			console.log('Forms input Number init');
		},
		//Test keydown
		keyDown: function (event) {
			return allowKeyPress(event);
		},
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.forms.input.number = {};
		}
	};

	//Namespace input.number
	fnb.namespace('forms.input.number', number, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Radio Button Object
///-------------------------------------------///
(function() {
	//Bind event for current object
    function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.radioGroupWrapper .radioButton', handler: 'fnb.hyperion.forms.radioButton.select(event);'},
    	              {type: 'frame', listener: document, events:'click', selector: '* .checkRadioButtons  .radioGroupWrapper .radioButton', handler: 'fnb.hyperion.forms.radioButton.selectCheckRadio(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Radio Button Parent function
	///-------------------------------------------///
	function radioButton() {

	};
	///-------------------------------------------///
	/// Radio Button Methods
	///-------------------------------------------///
	radioButton.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Checkbox
    	init: function () {
    		console.log('Forms RadioButton init');
    		bindEvents();
        }, 
        selectCheckRadio : function(event){
            //alert("Working!!!");
		//Get parent function
		var _this = this;
		//Get target Element.
        var	target = fnb.hyperion.$(event.currentTarget),
        	groupContainer = target.parent().parent().parent().parent(),
        	tragetGroupContainer =target.parent(),
        	defaultSelect = target.attr('data-defaultselected');
        	
	        if(defaultSelect != undefined && defaultSelect != '' && defaultSelect != false && defaultSelect != 'false'){
	        	
	        }else{
	        	//get all default buttons
	        	defaultButtons = groupContainer.find('*[data-defaultselected="true"]');
	        	//get total number of defaultSelected buttons that is NOT SELCTE.
	        	totaldefaultNotSelected = groupContainer.find('*[data-defaultselected="true"][data-state="unChecked"]');
	        	totaldefaultNotSelectedLen = totaldefaultNotSelected.length(); 
	        	
		        	if(totaldefaultNotSelectedLen > 1 ){
		            	//Uncheck all radio buttons
		                groupContainer.find('* .radioButton').attr('data-state', 'unChecked');
		            	//Uncheck all radio buttons
		                groupContainer.find('=input').attr('checked','');
		                defaultButtons.each(function(element){
		        			//Deselect element
		                	_this.setSelected(element);
		    			});
		                //unSelect all other radio buttons in tragetgroup.
		                tragetGroupContainer.find('* .radioButton').attr('data-state', 'unChecked');
		                tragetGroupContainer.find('=input').attr('checked','');
		                //set target as selected 
		                _this.setSelected(target);
		        	}
	        }
        },
        //Check current state of checkbox
        select: function (event) {
        	//Get parent function
        	var _this = this;
        	//Get current checkbox
    		var target = fnb.hyperion.$(event.currentTarget);
        	//Get current checkbox parent
    		var targetParent = target.parent();
        	//Get disabled state
    		var idDisabled = (targetParent.attr('data-disabled')) ? (targetParent.attr('data-disabled')=="true") ? true : false : false;
    		//Dont run if disabled
    		if(!idDisabled){
        		//Get checkbox state
        		target.parent().children().each(function(element){
        			//Deselect element
        			_this.setUnSelected(element);
    			});
        		//Select current item
        		_this.setSelected(target);
    		}
    		
        },
        //Set the state of target checkbox to checked
        setSelected: function (target) {
        	//Change data attribute of checkbox
        	target.attr('data-state', 'checked');
        	//Change form element value
        	target.find('=input').attr('checked',true);
        },
        //Set the state of target checkbox to unchecked
        setUnSelected: function (target) {
        	//Change data attribute of checkbox
        	target.attr('data-state', 'unChecked');
        	//Change form element value
        	target.find('=input').attr('checked','');
        },
        //Set the state of target checkbox to disabled
        setDisabled: function (target) {
        	//Change data attribute of checkbox
        	target.attr('data-state', 'disabled');
        },
        //Get object current value
        getValue: function (element) {
        	//return checkbox state
        	return element.attr('data-state');
        },
        //Set object current value
        setValue: function (element,value) {
        	//Change form element value
        	switch(value) {
				case 'selected':
					this.setSelected(element);
				break;
				case 'unSelected':
					this.setUnSelected(element);
				break;
				default:
				
			};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.radioButton = {};
        }
	};

	//Namespace forms.radioButton
	fnb.namespace('forms.radioButton', radioButton, true);

})();

///-------------------------------------------///
/// developer: CB Lombard
///
///
/// RangeSlider
///-------------------------------------------///
(function() {
  //Bind event for current object
  function bindEvents() {
    //Add listener for click on login button  data-role="btnLabel"
    var events = [ {type: 'frame', listener: document, events:'mouseup', selector: '#bodyContainer', handler: 'fnb.hyperion.forms.rangeSlider.sliderBtnMouseUp(event,"mouse");'},
                   {type: 'frame', listener: document, events:'touchend', selector: '#bodyContainer', handler: 'fnb.hyperion.forms.rangeSlider.sliderBtnMouseUp(event,"touch");'},
                      {type: 'frame', listener: document, events:'click', selector: '.point', handler: 'fnb.hyperion.forms.rangeSlider.snapToPointer(event);'},
                      {type: 'frame', listener: document, events:'click', selector: '#setSlider', handler: 'fnb.hyperion.forms.rangeSlider.testFunc();'},
                      {type: 'frame', listener: document, events:'mousedown', selector: '[data-role="btnLabel"]', handler: 'fnb.hyperion.forms.rangeSlider.sliderBtnMouseDown(event,"mouse")'},
                      {type: 'frame', listener: document, events:'touchstart', selector: '[data-role="btnLabel"]', handler: 'fnb.hyperion.forms.rangeSlider.sliderBtnMouseDown(event,"touch")'}];

        //Append events to actions module
        fnb.hyperion.actions.addEvents(events);
    };
  ///-------------------------------------------///
  /// RangeSlider Parent function
  ///-------------------------------------------///
  function rangeSlider() {

  };
  ///-------------------------------------------///
  /// Subtabs Methods
  ///-------------------------------------------///
  rangeSlider.prototype = {
    //Var for frame to auto initialize module
    autoInit: true,
    //active sub tab
    activeSliderId: '', 
        // sets the mouse down event as active ... waits for mouse up to to cancel mouse move event
        mousedown: false,
        touchstarted: false, 
        offSet : false,
        rangeSliderGroup: '',
        sliderType: 'banking',  
        browserSize: '',
        body:'',
        screenOffset : '',
        reInt : '',
        maxValue : '1300',
        trusted : '',
        availableCredit : '',
    
      init: function () {
        //Bind subTabs events
        console.log('Forms rangeSlider init');
        bindEvents();

        // setOffSet
        this.browserSize = this.getBrowserSize();
        },
        checkRenit : function(){
          _this = fnb.hyperion.forms.rangeSlider;
          var element = fnb.hyperion.$('#mvnoSettings');
          //check if Dom element was found
          if(element.length() > 0){
            var reInt = element.attr('data-reint');
             // if found and data-ReInt equals to true then destroy or reInt object.
              if(reInt  == 'true' || reInt == true){
                //Clear all Slider Objects
                fnb.hyperion.forms.rangeSlider.destroy();
                //set reInt to False.
                fnb.hyperion.$('#mvnoSettings').attr('data-reint', 'false');
              }
            _this.trusted = element.attr('data-trusted');
            _this.availableCredit = element.attr('data-availableCredit');
          }
        },
        //Remove current object from rangeSlider JS objects
        destroy: function () {
            var _this = fnb.hyperion.forms.rangeSlider;
                fnb.hyperion.forms.rangeSlider.activeSliderId ='';
                fnb.hyperion.forms.rangeSlider.mousedown= false;
                fnb.hyperion.forms.rangeSlider.offSet = false;
                fnb.hyperion.forms.rangeSlider.rangeSliderGroup = {};
                fnb.hyperion.forms.rangeSlider.sliderGroup = {};
                _this.body = fnb.hyperion.$('#bodyContainer');
        },
        //MouseDown Event
        sliderBtnMouseDown: function(event, eventType){
            var _this = fnb.hyperion.forms.rangeSlider;
            _this.checkRenit();
           //check if object has been init
           //Get current sliderBtnSelected
           var target = fnb.hyperion.$(event.currentTarget),
               targetID = target.attr("data-sliderid");

           //if not init it
           
            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
                _this.browserSize = _this.getBrowserSize();
                _this.body = fnb.hyperion.$('#bodyContainer');
                this.browserSize = _this.getBrowserSize();
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                    //  -- update to active
                      _this.activeSliderId = targetID; 
                      _this.browserSize = _this.getBrowserSize();
                      _this.sliderGroup[targetID].rangeSliderWidth = _this.sliderGroup[targetID].rangeSliderContainer.outerWidth();
                   }
            }else{
               _this.browserSize = _this.getBrowserSize();
               _this.sliderGroup[targetID].rangeSliderWidth = _this.sliderGroup[targetID].rangeSliderContainer.outerWidth();
            }


            //then perform mouse down event and add mouse move event.

            if(this.mousedown == false && eventType == 'mouse'){

               this.mousedown = true;
               fnb.hyperion.delegate.on(_this.body.elem, 'mousemove', fnb.hyperion.forms.rangeSlider.bindToMouseMove);

            }else if(this.touchstarted == false && eventType == 'touch' ){

               this.touchstarted = true;
               fnb.hyperion.delegate.on(_this.body.elem, 'touchmove', fnb.hyperion.forms.rangeSlider.BlockMove);
               fnb.hyperion.delegate.on(_this.body.elem, 'touchmove', fnb.hyperion.forms.rangeSlider.bindToTouchMove);
               
            }
        },
        //init new slider and add to object Group
        addToSliderGroup:function(targetID){
            var _this  = fnb.hyperion.forms.rangeSlider,
                groupId = parseInt(targetID)
                // select everyting from current slected slider and build object
                _this.sliderGroup[targetID] ={};
                _this.sliderGroup[targetID].rangeSliderContainer = fnb.hyperion.$('#rangeSliderContainer'+targetID);
                _this.sliderGroup[targetID].rangeSliderWidth = _this.sliderGroup[targetID].rangeSliderContainer.outerWidth();
                _this.sliderGroup[targetID].snapPointsElements = _this.sliderGroup[targetID].rangeSliderContainer.find(".point");
                _this.sliderGroup[targetID].snapPointsArr = [];
         
                // get snappoint lenght
                snapPointsElementsLen = _this.sliderGroup[targetID].snapPointsElements.length() - 1;
                
                //build array of all snap points
                 _this.sliderGroup[targetID].snapPointsElements.each(function(element, index){
                    console.log(element);
                    var NewSnapPointObj ={
                            "percentage": parseInt(element.attr("data-percentage")) , 
                            "value":  element.attr("data-value"),
                            "pointLabel": element.find('! .pointLabel').html(),
                            "itemCode" : element.attr("data-itemCode"),
                            "itemSkuCode": element.attr("data-itemskucode"),
                            "dataBtnLabel": element.attr("data-btnlabel"),
                            "rate": element.attr("data-rate")};
                        // add object to array
                  if(element.attr("data-snappointselceted") == "true") { 
                      _this.sliderGroup[targetID].activeSnapPoint =  index;
                      _this.sliderGroup[targetID].maxSnappoint =  (_this.trusted == "false" || _this.trusted == false ) ? index : snapPointsElementsLen;

                   }
                   _this.sliderGroup[targetID].snapPointsArr.push(NewSnapPointObj);
                 });   

                 console.log(_this.sliderGroup[targetID].snapPointsArr);
                _this.sliderGroup[targetID].rangeSliderBtn = _this.sliderGroup[targetID].rangeSliderContainer.find('*[data-role="sliderBtn"]');
                _this.sliderGroup[targetID].rangeSliderBtnLabel = _this.sliderGroup[targetID].rangeSliderContainer.find('*[data-role="btnLabel"]'); 
                _this.sliderGroup[targetID].rangeSlideProgress = _this.sliderGroup[targetID].rangeSliderContainer.find('*[data-role="sliderProgress"]');
                _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor=_this.sliderGroup[targetID].rangeSlideProgress.attr('style');
                _this.sliderGroup[targetID].progressPercentage= "";

                _this.activeSliderId = targetID;

        },
        //MouseUp Event
        sliderBtnMouseUp:function(event){
            var _this  = fnb.hyperion.forms.rangeSlider;

            if(_this.mousedown){

                _this.mousedown = false; 

                var targetID = _this.activeSliderId 
                
                fnb.hyperion.delegate.off(this.body.elem, 'mousemove', fnb.hyperion.forms.rangeSlider.bindToMouseMove);

                var i=0, len = _this.sliderGroup[targetID].snapPointsArr.length, closestPointIndex =0, pointVal = 0, newPointVal =0;
                  
               var  curentActiveSlider = fnb.hyperion.$('#rangeSliderContainer'+targetID);
                //var element = document.getElementById('some-id');
                var position = curentActiveSlider.elem.getBoundingClientRect();
                var screenOffSet = position.left;
               //get the offset of the mouse cursor on mouse move
               //have to use clientX - pageX not working in IE8
                var xOffSet  = xOffSet  = event.clientX - screenOffSet,
                //current percentage of slider
                percentage = Math.round(xOffSet /  _this.sliderGroup[targetID].rangeSliderWidth * 100),
                sliderPercentage = (percentage > 100)? 100 :  percentage;

                // Loop through all snapPoint and find snapPoint closestPointIndex.
                  for(i ; i < len; i++){
                    newPointVal = Math.abs(sliderPercentage - _this.sliderGroup[targetID].snapPointsArr[i].percentage);
                      if(newPointVal < pointVal || pointVal == 0 ){
                          closestPointIndex = i;
                          pointVal = newPointVal;
                      }
                  }

                //get percentage value from snapPointsArr Object   
                sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex].percentage;
                //get Label percentage value from snapPointsArr Object   
               var snapPointLabel = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex].dataBtnLabel; 

                  _this.setSnapValueToYouPay(targetID,closestPointIndex);
                  // change slider label to snapPoint Label 
                 _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(snapPointLabel);
                  
                 sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
                 //change btn label position if greater that 50%;
                 _this.doChangeLabelAlign(targetID, sliderPercentage );
                _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
                _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

                // calculate new total and update html
                _this.calculatePackageTotal(targetID, closestPointIndex);
            }
            if(this.touchstarted){
//
                _this.touchstarted = false; 
                var targetID = _this.activeSliderId 
                var target = fnb.hyperion.$(event.currentTarget).find('! #sliderBtn'+targetID);
                  // _this.debugDiv.html("<br/> <p> Mouse Up!!!</p>");
                fnb.hyperion.delegate.off(this.body.elem, 'touchmove', fnb.hyperion.forms.rangeSlider.bindToTouchMove);
                fnb.hyperion.delegate.off(_this.body.elem, 'touchmove', fnb.hyperion.forms.rangeSlider.BlockMove);

                var i=0, len = _this.sliderGroup[targetID].snapPointsArr.length, closestPointIndex =0, pointVal = 0, newPointVal =0;
                  
               var  curentActiveSlider = fnb.hyperion.$('#rangeSliderContainer'+targetID);
                //var element = document.getElementById('some-id');
                var position = curentActiveSlider.elem.getBoundingClientRect();
                var screenOffSet = position.left


                var targetPosition = target.elem.getBoundingClientRect();
               //get the offset of the mouse cursor on mouse move
               //have to use clientX - pageX not working in IE8
                var xOffSet  =  targetPosition.left - screenOffSet,
                //current percentage of slider
                percentage = Math.round(xOffSet /  _this.sliderGroup[targetID].rangeSliderWidth * 100),
                sliderPercentage = (percentage > 100)? 100 :  percentage;

                // Loop through all snapPoint and find snapPoint closestPointIndex.
                  for(i ; i < len; i++){
                    newPointVal = Math.abs(sliderPercentage - _this.sliderGroup[targetID].snapPointsArr[i].percentage);
                      if(newPointVal < pointVal || pointVal == 0 ){
                          closestPointIndex = i;
                          pointVal = newPointVal;
                      }
                  }

                //get percentage value from snapPointsArr Object   
                sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex].percentage;
                //get Label percentage value from snapPointsArr Object   
               var snapPointLabel = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex].dataBtnLabel; 

                  _this.setSnapValueToYouPay(targetID,closestPointIndex);
                  // change slider label to snapPoint Label 
                 _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(snapPointLabel);
                  
                 sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
                 //change btn label position if greater that 50%;
                 _this.doChangeLabelAlign(targetID, sliderPercentage );
                _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
                _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

                // calculate new total and update html
                _this.calculatePackageTotal(targetID, closestPointIndex);

            }


        },
        //bind to mouseMove event ... get position of mouse
        bindToTouchMove:function(event){
          var _this  = fnb.hyperion.forms.rangeSlider,
            element = event.currentTarget,
            targetID = _this.activeSliderId;
                   
           var  curentActiveSlider = fnb.hyperion.$('#rangeSliderContainer'+targetID);
           //var element = document.getElementById('some-id');
           var position = curentActiveSlider.elem.getBoundingClientRect();
           var screenOffSet = position.left;
          //get the offset of the mouse cursor on mouse move
          //have to use clientX - pageX not working in IE8
          var xOffSet  = event.touches[0].pageX - screenOffSet,
              percentage =  Math.round( xOffSet / _this.sliderGroup[targetID].rangeSliderWidth * 100),
              percentage = ( percentage < 0 ) ? 0 : percentage;
            console.log('Offset :'+xOffSet+' and percentage : '+percentage);
           _this.sliderGroup[targetID].progressPercentage = ( percentage > 100) ?  100 : percentage;

           // setting the width of the progress bar and the slider Btn
           _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

           //var currentVal =  Math.round(percentage * _this.interValValue);
           //_this.showValDiv.html(currentVal);
           _this.body.addClass("hideSelect");
        },
        BlockMove : function (event) {
          // Tell Safari not to move the window.
            event.stop();
        },
        //bind to mouseMove event ... get position of mouse
        bindToMouseMove:function(event){
          event.stop();
          var _this  = fnb.hyperion.forms.rangeSlider,
             element = event.currentTarget,
            targetID = _this.activeSliderId;
                   
           var  curentActiveSlider = fnb.hyperion.$('#rangeSliderContainer'+targetID);
           //var element = document.getElementById('some-id');
           var position = curentActiveSlider.elem.getBoundingClientRect();
           var screenOffSet = position.left;
          //get the offset of the mouse cursor on mouse move
          //have to use clientX - pageX not working in IE8
          var xOffSet  = event.clientX - screenOffSet,
              percentage =  Math.round( xOffSet / _this.sliderGroup[targetID].rangeSliderWidth * 100),
              percentage = ( percentage < 0 ) ? 0 : percentage;

           _this.sliderGroup[targetID].progressPercentage = ( percentage > 100) ?  100 : percentage;

           // setting the width of the progress bar and the slider Btn
           _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

           //var currentVal =  Math.round(percentage * _this.interValValue);
           //_this.showValDiv.html(currentVal);
           _this.body.addClass("hideSelect");
        },
        //snap to pointer selected.
        snapToPointer:function(event){
           var _this  = fnb.hyperion.forms.rangeSlider;
           	   _this.checkRenit();
           		
           var  currentTargetID = _this.activeSliderId,
                //pointer that was clicked
                target = fnb.hyperion.$(event.currentTarget),
                targetID = target.attr("data-sliderid"),
                SnapToPercentage = target.attr("data-percentage"),
                snapPointLabel = target.attr("data-btnlabel"),
                id = target.attr("id"),
                closestPointIndex =  id.replace(/\D/g,'') - 1;

            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
                _this.body = fnb.hyperion.$('#bodyContainer');
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                    //  -- update to active
                      _this.activeSliderId = targetID;   
                   }
            }
             
             //set you pay value equal to snapPoint Value
             _this.setSnapValueToYouPay(targetID,closestPointIndex);
          
            _this.doChangeLabelAlign(targetID, SnapToPercentage );
            // change slider label to snapPoint Label
           _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(snapPointLabel);

           _this.sliderGroup[targetID].progressPercentage =SnapToPercentage;
           _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);

           // calculate new total and update html
           _this.calculatePackageTotal(targetID, closestPointIndex);
        },
        //calculate package total
        calculatePackageTotal : function(targetID,closestPointIndex){
            var _this  = fnb.hyperion.forms.rangeSlider;
            //get all you pay elements
            var allYouPayElements = fnb.hyperion.$('* .youPayValue'),
            //get total package Element
			totalPackageElement = fnb.hyperion.$('#newPackageTotal'),
            availableCreditElement = fnb.hyperion.$('#availableCredit'),
            recurringBundleElement = fnb.hyperion.$('#recurringBundles'),
            currentPackageValue  = 0,
            total = 0;

             if( _this.sliderType == 'banking'){
               var currentPackageElement = fnb.hyperion.$('#currentPackageValue'),
                   
                   currentPackageValue = parseFloat(currentPackageElement.html());
                   currentPackageValue = (currentPackageValue >0 && currentPackageValue != undefined && currentPackageValue != '')? currentPackageValue : 0;
             }
            
            //get values of each element and calculate the total.
            allYouPayElements.each(function(element, index){
                   total +=  parseFloat(element.html());
            });
            
            var newPackageTotal = total +currentPackageValue;
            lastActiveSnapPoint =_this.sliderGroup[targetID].activeSnapPoint;
            maxValue = parseFloat(totalPackageElement.attr('data-maxvalue'));
            minValue = parseFloat(totalPackageElement.attr('data-minvalue'));
            snapMaxValue = _this.sliderGroup[targetID].maxSnappoint;
            availableCredit = maxValue - total;
            //if total is greater than maxValue then snap back to previous Active Snappoint
            if(total > maxValue && maxValue >= 0 || closestPointIndex >  snapMaxValue  ){ 
                lastActiveSnapPoint = (closestPointIndex >  snapMaxValue) ? snapMaxValue : lastActiveSnapPoint;
                //availableCreditElement.html(availableCredit);
                _this.setSnapValueToYouPay(targetID,lastActiveSnapPoint);
                 //get percentage value from snapPointsArr Object   
                sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[lastActiveSnapPoint].percentage;   

                sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
                 //change btn label position if greater that 50%;  
                _this.doChangeLabelAlign(targetID, sliderPercentage );
                _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
                _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);
                
                // change slider label to snapPoint Label
                _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(_this.sliderGroup[targetID].snapPointsArr[lastActiveSnapPoint].dataBtnLabel); 
           }else if(total < minValue && minValue >= 0 ){
                  //availableCreditElement.html(availableCredit);
                 _this.setSnapValueToYouPay(targetID,lastActiveSnapPoint);
                 //get percentage value from snapPointsArr Object   
                sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[lastActiveSnapPoint].percentage;   

                sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
                 //change btn label position if greater that 50%;  
                _this.doChangeLabelAlign(targetID, sliderPercentage );
                _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
                _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);
                
                // change slider label to snapPoint Label
                _this.sliderGroup[targetID].rangeSlideProgress.find('![data-role="btnLabel"]').html(_this.sliderGroup[targetID].snapPointsArr[lastActiveSnapPoint].dataBtnLabel);
           }else{
             //check if availableCredit element exist. and set it
        	 if(availableCreditElement.length() > 0){ availableCreditElement.html(availableCredit);}
             //check if availableCredit element exist. and set it
             if(recurringBundleElement.length() > 0){ recurringBundleElement.html(total); } 
             _this.sliderGroup[targetID].activeSnapPoint= closestPointIndex;
             //update total value with total.
             totalPackageElement.html(newPackageTotal);
            }

        },
        //set you pay value equal to snapPoint Value
        setSnapValueToYouPay:function(targetID,closestPointIndex){
           
           var _this  = fnb.hyperion.forms.rangeSlider;
           

          //if sliderType is sales set data-settings values itemCode and SkuCode
          if(_this.sliderType === 'sales'){
            var youPayElement =  fnb.hyperion.$('#youPayValue'+targetID);
                             youPayElement.html(_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].value);
                             youPayElement.attr("data-itemcode",_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].itemCode);
                             youPayElement.attr("data-itemskucode",_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].itemSkuCode);
          }
          //if sliderType equals banking add itemCode to Input
          if(_this.sliderType === 'banking'){
             fnb.hyperion.$('#youPayInput'+targetID).val(_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].itemCode);
            // fnb.hyperion.$('#youPayRate'+targetID).html(_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].rate);
             fnb.hyperion.$('#youPayValue'+targetID).html(_this.sliderGroup[targetID].snapPointsArr[closestPointIndex].value);
          }


        },
        // get the width and height of browser window;
        getBrowserSize:function(event){
             //get browser width and height
              var obj ={};
                if (document.body && document.body.offsetWidth) {
                 obj["winW"] = document.body.offsetWidth;
                 obj["winH"] = document.body.offsetHeight;
                }
                if (document.compatMode=='CSS1Compat' &&
                    document.documentElement &&
                    document.documentElement.offsetWidth ) {
                   obj["winW"] = document.documentElement.offsetWidth;
                   obj["winH"] = document.documentElement.offsetHeight;
                }
                if (window.innerWidth && window.innerHeight) {
                   obj["winW"] = window.innerWidth;
                   obj["winH"] = window.innerHeight;
                }
                // return object with objectName.winW for browser width and objectName.winH for browser height;
                return obj;
        },
        // this will select the slider with id specified and got snapPoint specified.
        setSliderToSnapPoint : function(sliderId, snapPoint){
            var _this  = fnb.hyperion.forms.rangeSlider;
            var targetID = sliderId;

            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                    //  -- update to active
                      _this.activeSliderId = targetID;   
                   }
            }
            //loop through snappoints get percentage of snapPoint specified and set slider to that snapPoint
            //build array of all snap points
                 _this.sliderGroup[targetID].snapPointsElements.each(function(element, index){

                        if(index+1 == snapPoint){
                            _this.sliderGroup[targetID].progressPercentage =parseInt(element.attr("data-percentage"));;
                            _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);
                        }

                  });  
        },
        setSliderSnapPointToPercentage: function(sliderId, percentage){
            var _this  = fnb.hyperion.forms.rangeSlider;
            console.log(_this);
            var targetID = sliderId;
            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                      //  -- update to active
                      _this.activeSliderId = targetID;   
                   }
            }
            //snap to closet point for percantage given
            var i=0, len = _this.sliderGroup[targetID].snapPointsArr.length, closestPointIndex =0, pointVal = 0, newPointVal =0;
                sliderPercentage = (percentage > 100)? 100 :  percentage;

                for(i ; i < len; i++){
                  newPointVal = Math.abs(sliderPercentage - _this.sliderGroup[targetID].snapPointsArr[i]);
                    if(newPointVal < pointVal || pointVal == 0 ){
                        closestPointIndex = i;
                        pointVal = newPointVal;
                    }
                }
            //get label of closest snapPoint ..snapPoints
            var snapPointLabel =  _this.sliderGroup[targetID].snapPointsElements.find('!span.snapPoints'+closestPointIndex); 

            sliderPercentage = _this.sliderGroup[targetID].snapPointsArr[closestPointIndex];
            sliderPercentage = (sliderPercentage > 100)? 100 :  sliderPercentage;
            _this.sliderGroup[targetID].progressPercentage = sliderPercentage;
            _this.sliderGroup[targetID].rangeSlideProgress.attr( 'style',  "width:"+_this.sliderGroup[targetID].progressPercentage +"%; "+ _this.sliderGroup[targetID].rangeSlideProgressBackgroundColor);
        },
        // this will slect the slider Specified in the sliderId and return the value for the snapPoint specified.
        getSnapPointValue:function(sliderId, snapPoint){
            var   _this  = fnb.hyperion.forms.rangeSlider,
                targetID = sliderId;

            if(_this.sliderGroup == undefined){
                _this.sliderGroup ={};
                _this.addToSliderGroup(targetID);
            }
            //if current selected range slider is not selected
            if(_this.activeSliderId != targetID){
                    
                   if(_this.sliderGroup[targetID] == undefined){
                      //  -- if object not added to group then add it.
                     _this.addToSliderGroup(targetID);   
                   }else{
                    //  -- update to active
                      _this.activeSliderId = targetID;   
                   }
            }
            //loop through snapPoints get percentage of snapPoint specified and set slider to that snapPoint
            //build array of all snap points
            var value =0;
            _this.sliderGroup[targetID].snapPointsElements.each(function(element, index){

                if(index+1 == snapPoint){
                    value = element.attr("data-value");                  
                }

            });
            return value;
        },
        //change label left or right align.
        doChangeLabelAlign: function(targetID, percentage){
           var _this  = fnb.hyperion.forms.rangeSlider;
           
           if(percentage >= 51){
              if(_this.sliderGroup[targetID].rangeSliderBtnLabel.hasClass('btnRight')){
                _this.sliderGroup[targetID].rangeSliderBtnLabel.removeClass('btnRight')
              }
              _this.sliderGroup[targetID].rangeSliderBtnLabel.addClass('btnLeft')
           }else{
              if(_this.sliderGroup[targetID].rangeSliderBtnLabel.hasClass('btnLeft')){
                _this.sliderGroup[targetID].rangeSliderBtnLabel.removeClass('btnLeft')
              }
              _this.sliderGroup[targetID].rangeSliderBtnLabel.addClass('btnRight');
           }
            
        }

  };
    //Namespace rangeSlider
    fnb.namespace('forms.rangeSlider', rangeSlider, true);
})();
///-------------------------------------------///
/// developer: Mike Stott
///
/// Text Area Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'keyup', selector: 'textarea', handler: 'return fnb.hyperion.forms.textArea.updateChars(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
    ///-------------------------------------------///
	/// Text Area Parent function
	///-------------------------------------------///
	function textArea() {


	};
	///-------------------------------------------///
	/// Text Area Methods
	///-------------------------------------------///
	textArea.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Textarea
    	init: function () {
    	   //Bind our events to the object
    		bindEvents();
        },
        //Function called to update remaining characters
        updateChars : function(event) {
        	//Store the current textarea element
        	var _element =  fnb.hyperion.$(event.currentTarget);
        	var maxChars = _element.attr('data-charcount');
        	//If the textareas value is greater than the maximum allowed amount...
        	if(_element.prop('value').length > maxChars) {
          		//Don't let the user input any more caracters...
        		 _element.prop('value', _element.prop('value').slice(0,maxChars)); 
        	}else{
        		//Remaining caracters left
        		var charsLeft = maxChars - _element.prop('value').length;
        		//Update the remaining caracters text 
        		_element.parent().find('.characterCount').html(charsLeft + " characters remaining");
        	}
         },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.forms.textArea = {};
        }
	};

	//Namespace textArea
	fnb.namespace('forms.textArea', textArea, true);

})();
