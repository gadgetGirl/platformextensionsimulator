///-------------------------------------------///
/// developer: Donovan
///
/// Main Functions Object
///-------------------------------------------///
(function() {
    //Init all function modules
	function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.functions) {
    		if(fnb.hyperion.functions[module].autoInit){
				fnb.hyperion.functions[module].init();
			}
    	}
    };
    ///-------------------------------------------///
	/// Functions Parent function
	///-------------------------------------------///
	function functions() {

	};
	///-------------------------------------------///
	/// Functions Methods
	///-------------------------------------------///
	functions.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Functions
    	init: function () {
    		console.log('Functions init');
    		initModules();
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions = {};
        }
	};

	//Namespace functions
	fnb.namespace('functions', functions, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Banking Async load function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Async load Parent function
	///-------------------------------------------///
	function asyncLoadContent() {
			
	};
	///-------------------------------------------///
	/// Banking Async load Methods
	///-------------------------------------------///
	asyncLoadContent.prototype = {
		//Banking Async load methods
		load : function (sender,loadObj) {
			
			//Set load object method
			loadObj.method = sender;
			
			//Set load async method
			loadObj.async = true;
			
			//Disable initHtmlTemplates
			loadObj.initHtmlTemplates = false;
			
			//Disable initPageEvents
			loadObj.initPageEvents = false;
			
			//Get ajax load defaults
			loadObj = fnb.hyperion.controller.getAjaxLoadDefaults(loadObj);
			
			//Set loadObj data-type
			loadObj.dataType = 'html';
			
			//Append loadObj.utl with target parameters
			loadObj = fnb.hyperion.utils.ajax.parameters.getUrlParameters(loadObj);

			//Run callback if needed
			fnb.hyperion.functions.callback.call(loadObj.preLoadingCallback, loadObj);

			//Make request
			fnb.hyperion.ajax.request(loadObj);
			
		},
		//Ajax standard loadPage Method
		success : function (sender,loadObj) {

			//Get data from ajax response
			loadObj.data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
			
			//Test for valid response
			if(!fnb.hyperion.controller.validResponse(loadObj)) return;
			
			//Test reposns
			if(!fnb.hyperion.controller.validateXHR(loadObj.xhr,loadObj.postLoadingCallBack,sender)) return;

			//Callback for when load has finished
			function jsLoadComplete(){
			
				//Eval js scripts
				fnb.hyperion.load.evalScripts(scripts.scripts);

				//Try send tracking information
				try{
					fnb.hyperion.utils.tracking.checkTrackingObject(sender,loadObj);
				}catch(e){
					console.log("Hyperion Tracking Error: "+e);
				}
				
				//Run callback if needed
				fnb.hyperion.functions.callback.call(loadObj.postLoadingCallback, loadObj);
				
				//Reset xhr
				fnb.hyperion.ajax.xhr = '';

				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
				
			}

			//Get html script tags and clean html
        	var scripts = fnb.hyperion.load.collectScripts(loadObj.data);

        	//Get html css tags and clean html
        	var css = fnb.hyperion.load.collectCss(scripts.html);

			//Import css
			if(css.imports.length>0) fnb.hyperion.load.css(css.imports);
				
			//Append target content
			fnb.hyperion.utils.ajax.content.set(loadObj, css);
			
			//Test for scripts to load
			if(scripts.imports.length>0){
	        	fnb.hyperion.load.js(scripts.imports, jsLoadComplete);
			}else{
				jsLoadComplete();
			}
			
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.asyncLoadContent = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.asyncLoadContent', asyncLoadContent, true);
})();
///-------------------------------------------///
/// developer: Nardo+Mike+Don
///
/// Banking Default Load Ezi Page function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Load Ezi Page Parent function
	///-------------------------------------------///
	function loadEzi() {
			
	};
	///-------------------------------------------///
	/// Banking Load Page Methods
	///-------------------------------------------///
	loadEzi.prototype = {			
		//Banking Default Load banking methods
		load : function (sender,loadObj) {	
			
			//Set load object method
			loadObj.method = sender;
			
			//Get ajax load defaults
			loadObj = fnb.hyperion.controller.getAjaxLoadDefaults(loadObj);		
			
			//Set loadObj data-type
			loadObj.dataType = 'html';
			
			//Set default target
			loadObj.target = fnb.hyperion.$('#eziPageContent');		
			
			//Stop previous request
			fnb.hyperion.ajax.stop(sender);
			
			//Reset ajax object
			fnb.hyperion.ajax.reset();
			
			//Append loadObj.utl with target parameters
			loadObj = fnb.hyperion.utils.ajax.parameters.getUrlParameters(loadObj);
			
			//Run callback if needed
			fnb.hyperion.functions.callback.call(loadObj.preLoadingCallback, loadObj);
			
			//Make request
			fnb.hyperion.ajax.request(loadObj);
			
		},
		//Ajax standard loadEzi Method
		success : function (sender, loadObj) {	

			//Get data from ajax response
			loadObj.data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
			
			//Test for valid response
			if(!fnb.hyperion.controller.validateResponse(loadObj)) return;
			
			//Validate response
			if(!fnb.hyperion.controller.validateXHR(loadObj.xhr,loadObj.postLoadingCallBack,sender)) return;
			
			//Test if otp should be raised
			if(fnb.hyperion.utils.ajax.otp.test(loadObj)) return;
			
			//Callback for when load has finished
			function jsLoadComplete(){
			
				//Eval js scripts
				fnb.hyperion.load.evalScripts(scripts.scripts);

				//Try send tracking information
				try{
					fnb.hyperion.utils.tracking.checkTrackingObject(sender,loadObj);
				}catch(e){
					console.log("Hyperion Tracking Error: "+e);
				}
				
				//Check load object initial page settings
				fnb.hyperion.controller.checkLoadObjSettings(loadObj);

				//Run callback if needed
				fnb.hyperion.functions.callback.call(loadObj.postLoadingCallback, loadObj);
				
				//Reset xhr
				fnb.hyperion.ajax.xhr = '';
				
				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
			}

			//Get html script tags and clean html
        	var scripts = fnb.hyperion.load.collectScripts(loadObj.data);

        	//Get html css tags and clean html
        	var css = fnb.hyperion.load.collectCss(scripts.html);

			//Import css
			if(css.imports.length>0) fnb.hyperion.load.css(css.imports);
				
			//Append target content
			fnb.hyperion.utils.ajax.content.set(loadObj, css);
			
			//Test for scripts to load
			if(scripts.imports.length>0){
	        	fnb.hyperion.load.js(scripts.imports, jsLoadComplete);
			}else{
				jsLoadComplete();
			}
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.loadEzi = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.loadEzi', loadEzi, true);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Banking Default Load into a function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Load into a page Parent function
	///-------------------------------------------///
	function loadIntoPage() {
			
	};
	///-------------------------------------------///
	/// Banking Load into a page Methods
	///-------------------------------------------///
	loadIntoPage.prototype = {
		//Banking Default Load into a page methods
		success : function (sender,loadObj) {

			//Get data from ajax response
			loadObj.data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
			
			//Test for valid response
			if(!fnb.hyperion.controller.validResponse(loadObj)) return;
			
			//Validate response
			if(!fnb.hyperion.controller.validateXHR(loadObj.xhr,loadObj.postLoadingCallBack,sender)) return;
			
			//Test if otp should be raised
			if(fnb.hyperion.utils.ajax.otp.test(loadObj)) return;
			
			//Callback for when load has finished
			function jsLoadComplete(){
			
				//Eval js scripts
				fnb.hyperion.load.evalScripts(scripts.scripts);

				//Try send tracking information
				try{
					fnb.hyperion.utils.tracking.checkTrackingObject(sender,loadObj);
				}catch(e){
					console.log("Hyperion Tracking Error: "+e);
				}
				
				//Check load object initial page settings
				fnb.hyperion.controller.checkLoadObjSettings(loadObj);
				
				//Run callback if needed
				fnb.hyperion.functions.callback.call(loadObj.postLoadingCallback, loadObj);
				
				//Reset xhr
				fnb.hyperion.ajax.xhr = '';
				
				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
			}

			//Get html script tags and clean html
        	var scripts = fnb.hyperion.load.collectScripts(loadObj.data);

        	//Get html css tags and clean html
        	var css = fnb.hyperion.load.collectCss(scripts.html);

			//Import css
			if(css.imports.length>0) fnb.hyperion.load.css(css.imports);
				
			//Append target content
			fnb.hyperion.utils.ajax.content.set(loadObj, css);
			
			//Test for scripts to load
			if(scripts.imports.length>0){
	        	fnb.hyperion.load.js(scripts.imports, jsLoadComplete);
			}else{
				jsLoadComplete();
			}
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.loadIntoPage = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.loadIntoPage', loadIntoPage, true);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Banking Default Load Page function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Load Page Parent function
	///-------------------------------------------///
	function loadPage() {
			
	};
	///-------------------------------------------///
	/// Banking Load Page Methods
	///-------------------------------------------///
	loadPage.prototype = {
		//Banking Default Load banking methods
		load : function (sender,loadObj) {
			
			//Set load object method
			loadObj.method = sender;
			
			//Get ajax load defaults
			loadObj = fnb.hyperion.controller.getAjaxLoadDefaults(loadObj);
			
			//Set loadObj data-type
			loadObj.dataType = 'html';
			
			//Stop previous request
			fnb.hyperion.ajax.stop(sender);
			
			//Reset ajax object
			fnb.hyperion.ajax.reset();

			//Append loadObj.utl with target parameters
			loadObj = fnb.hyperion.utils.ajax.parameters.getUrlParameters(loadObj);
			
			//Run callback if needed
			fnb.hyperion.functions.callback.call(loadObj.preLoadingCallback, loadObj);

			//Make request
			fnb.hyperion.ajax.request(loadObj);
		},
		//Ajax standard loadPage Method
		success : function (sender,loadObj) {

			//Get data from ajax response
			loadObj.data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
			
			//Test for valid response
			if(!fnb.hyperion.controller.validResponse(loadObj)) return;
			
			//Validate response
			if(!fnb.hyperion.controller.validateXHR(loadObj.xhr,loadObj.postLoadingCallBack,sender)) return;
			
			//Test if otp should be raised
			if(fnb.hyperion.utils.ajax.otp.test(loadObj)) return;
			
			//Callback for when load has finished
			function jsLoadComplete(){
				
				//Eval js scripts
				fnb.hyperion.load.evalScripts(scripts.scripts);

				//Try send tracking information
				try{
					fnb.hyperion.utils.tracking.checkTrackingObject(sender,loadObj);
				}catch(e){
					console.log("Hyperion Tracking Error: "+e);
				}
				
				//Run callback if needed
				fnb.hyperion.functions.callback.call(loadObj.postLoadingCallback, loadObj);
				
				//Check load object initial page settings
				fnb.hyperion.controller.checkLoadObjSettings(loadObj);
				
				//Reset xhr
				fnb.hyperion.ajax.xhr = '';

				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
			}
			
			//Clear js in head
			fnb.hyperion.load.clearJs();
			
			//Clear css in head
			fnb.hyperion.load.clearCss();
			
			//Clear page object
			fnb.hyperion.controller.clearPageObjects();
			
			//Clear html templates
			fnb.hyperion.controller.clearHtmlTemplates(loadObj);
        	
			//Get html script tags and clean html
        	var scripts = fnb.hyperion.load.collectScripts(loadObj.data);

        	//Get html css tags and clean html
        	var css = fnb.hyperion.load.collectCss(scripts.html);

			//Import css
			if(css.imports.length>0) fnb.hyperion.load.css(css.imports);
				
			//Append target content
			fnb.hyperion.utils.ajax.content.set(loadObj, css);
			
			//Test for scripts to load
			if(scripts.imports.length>0){
	        	fnb.hyperion.load.js(scripts.imports, jsLoadComplete, loadObj);
			}else{
				jsLoadComplete();
			}

		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.loadPage = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.loadPage', loadPage, true);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Banking Default Load Page function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Load Page Parent function
	///-------------------------------------------///
	function loadPage() {
			
	};
	///-------------------------------------------///
	/// Banking Load Page Methods
	///-------------------------------------------///
	loadPage.prototype = {
		//Banking Default Load banking methods
		load : function (sender,loadObj) {
			//Set load object method
			loadObj.method = sender;
			//Get ajax load defaults
			loadObj = fnb.hyperion.controller.getAjaxLoadDefaults(loadObj);
			//Set loadObj data-type
			loadObj.dataType = 'html';
			//Stop previous request
			fnb.hyperion.ajax.stop(sender);
			//Reset ajax object
			fnb.hyperion.ajax.reset();
			//Make request
			fnb.hyperion.ajax.request(loadObj);
		},
		//Ajax standard loadPage Method
		success : function (sender,loadObj) {
			//Clear previous page scripts
			fnb.hyperion.utils.ajax.scripts.clear();
			//Clear previous page css
			fnb.hyperion.utils.ajax.css.clear();
			//Clear page object
			fnb.hyperion.controller.clearPageObjects();
			//Clear html templates
			fnb.hyperion.controller.clearHtmlTemplates(loadObj);
			//Get data
			var data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
			//Try filter content
			var selectedContent = fnb.hyperion.utils.ajax.filter.get(loadObj.urlTarget,data);
			//Get html script tags and clean html
        	var cleanScripts = fnb.hyperion.utils.ajax.scripts.getScripts(selectedContent.content);
			//Get html css tags and clean html
        	var cleanCss = fnb.hyperion.utils.ajax.css.getCss(cleanScripts.cleanHtml);
			//Set the content
			loadObj.target.html(cleanCss.cleanHtml);
			//Set scripts
			if(cleanScripts.scripts.length>0||cleanCss.css.length>0){
				if(cleanScripts.scripts.length>0&&cleanCss.css.length===0){
					//Set scripts and wait for them to be ready
					fnb.hyperion.utils.ajax.scripts.setScripts(cleanScripts.scripts,function() {
						//Notify Controller to raise Ready event
						fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
						//Check load object initial page settings
						fnb.hyperion.controller.checkLoadObjSettings(loadObj);
					});
				}else if(cleanScripts.scripts.length>0&&cleanCss.css.length>0){
					function cssCallBack(){
						fnb.hyperion.utils.ajax.css.setCss(cleanCss.css,function() {
							//Notify Controller to raise Ready event
							fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
							//Check load object initial page settings
							fnb.hyperion.controller.checkLoadObjSettings(loadObj);
						});
					}
					//Set scripts and wait for them to be ready
					fnb.hyperion.utils.ajax.scripts.setScripts(cleanScripts.scripts,cssCallBack);
				}else if(cleanScripts.scripts.length===0&&cleanCss.css.length>0){
					fnb.hyperion.utils.ajax.css.setCss(cleanCss.css,function() {
						//Notify Controller to raise Ready event
						fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
						//Check load object initial page settings
						fnb.hyperion.controller.checkLoadObjSettings(loadObj);
					});
				};
			}else{
				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
				//Check load object initial page settings
				fnb.hyperion.controller.checkLoadObjSettings(loadObj);
			}
			//Reset xhr
			fnb.hyperion.ajax.xhr = '';
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.loadPage = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.loadPage', loadPage, true);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Banking Default Load Popup function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Load Popup Parent function
	///-------------------------------------------///
	function loadPopup() {
			
	};
	///-------------------------------------------///
	/// Banking Load Popup Methods
	///-------------------------------------------///
	loadPopup.prototype = {
		//Banking Default Load banking methods
		load : function (sender,loadObj) {
			
			//Set load object method
			loadObj.method = sender;
			
			//Get ajax load defaults
			loadObj = fnb.hyperion.controller.getAjaxLoadDefaults(loadObj);
			
			//Set loadObj data-type
			loadObj.dataType = 'html';
			
			//Set default target
			loadObj.target = fnb.hyperion.utils.notifications.notificationInner;
			
			//Stop previous request
			fnb.hyperion.ajax.stop(sender);
			
			//Reset ajax object
			fnb.hyperion.ajax.reset();
			
			//Append loadObj.utl with target parameters
			loadObj = fnb.hyperion.utils.ajax.parameters.getUrlParameters(loadObj);
			
			//Run callback if needed
			fnb.hyperion.functions.callback.call(loadObj.preLoadingCallback, loadObj);
			
			//Make request
			fnb.hyperion.ajax.request(loadObj);
			
		},
		//Ajax standard loadPage Method
		success : function (sender,loadObj) {

			//Get data from ajax response
			loadObj.data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
			
			//Test for valid response
			if(!fnb.hyperion.controller.validResponse(loadObj)) return;
			
			//Validate response
			if(!fnb.hyperion.controller.validateXHR(loadObj.xhr,loadObj.postLoadingCallBack,sender)) return;

			//Callback for when load has finished
			function jsLoadComplete(){
			
				//Eval js scripts
				fnb.hyperion.load.evalScripts(scripts.scripts);
				

				//Try send tracking information
				try{
					fnb.hyperion.utils.tracking.checkTrackingObject(sender,loadObj);
				}catch(e){
					console.log("Hyperion Tracking Error: "+e);
				}
				
				//Check load object initial page settings
				fnb.hyperion.controller.checkLoadObjSettings(loadObj);

				//Run callback if needed
				fnb.hyperion.functions.callback.call(loadObj.postLoadingCallback, loadObj);

				//Reset xhr
				fnb.hyperion.ajax.xhr = '';
				
				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
			}

			//Get html script tags and clean html
        	var scripts = fnb.hyperion.load.collectScripts(loadObj.data);

        	//Get html css tags and clean html
        	var css = fnb.hyperion.load.collectCss(scripts.html);

			//Import css
			if(css.imports.length>0) fnb.hyperion.load.css(css.imports);
				
			//Append target content
			fnb.hyperion.utils.ajax.content.set(loadObj, css);
			
			//Test for scripts to load
			if(scripts.imports.length>0){
	        	fnb.hyperion.load.js(scripts.imports, jsLoadComplete);
			}else{
				jsLoadComplete();
			}

		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.loadPopup = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.loadPopup', loadPopup, true);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Banking Submit from page function
///-------------------------------------------///
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Submit from page Parent function
	///-------------------------------------------///
	function submitFromEziToEzi() {
			
	};
	///-------------------------------------------///
	/// Banking Submit from page Methods
	///-------------------------------------------///
	submitFromEziToEzi.prototype = {
		
		//Ajax standard Submit from page Method
		submit : function (method, loadObj) {
			//Setup defaults
			loadObj = fnb.hyperion.controller.getAjaxLoadDefaults(loadObj);
			
			//Add sender to loadObj
			loadObj.method = method;
			
			//Set default async param to true
			loadObj.async = true;
			
			//Append loadObj with target parameters
			loadObj = fnb.hyperion.utils.ajax.parameters.get(loadObj);

			//Run callback if needed
			fnb.hyperion.functions.callback.call(loadObj.preLoadingCallback, loadObj);

			//Ajax post data
			fnb.hyperion.controller.ajax("loadEzi.load", method , loadObj);
		},
		//Ajax standard Submit from page Success
		success : function (sender,loadObj) {
	
			//Get data from ajax response
			loadObj.data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
			
			//Test for valid response
			if(!fnb.hyperion.controller.validResponse(loadObj)) return;
			
			//Validate response
			if(!fnb.hyperion.controller.validateXHR(loadObj.xhr,loadObj.postLoadingCallBack,sender)) return;
	
			//Test if otp should be raised
			if(fnb.hyperion.utils.ajax.otp.test(loadObj)) return;
			
			//Callback for when load has finished
			function jsLoadComplete(){
			
				//Eval js scripts
				fnb.hyperion.load.evalScripts(scripts.scripts);
				
	
				//Try send tracking information
				try{
					fnb.hyperion.utils.tracking.checkTrackingObject(sender,loadObj);
				}catch(e){
					console.log("Hyperion Tracking Error: "+e);
				}
				
				//Check load object initial page settings
				fnb.hyperion.controller.checkLoadObjSettings(loadObj);

				//Run callback if needed
				fnb.hyperion.functions.callback.call(loadObj.postLoadingCallback, loadObj);
				
				//Reset xhr
				fnb.hyperion.ajax.xhr = '';
				
				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
			}
	
			//Get html script tags and clean html
	    	var scripts = fnb.hyperion.load.collectScripts(loadObj.data);
	
	    	//Get html css tags and clean html
	    	var css = fnb.hyperion.load.collectCss(scripts.html);
	
			//Import css
			if(css.imports.length>0) fnb.hyperion.load.css(css.imports);
				
			//Append target content
			fnb.hyperion.utils.ajax.content.set(loadObj, css);
			
			//Test for scripts to load
			if(scripts.imports.length>0){
	        	fnb.hyperion.load.js(scripts.imports, jsLoadComplete);
			}else{
				jsLoadComplete();
			}
		},
	    //Remove current object from dom
	    destroy: function () {
	    	fnb.hyperion.functions.ajax.submitFromPage = {};
	    }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.submitFromEziToEzi', submitFromEziToEzi, true);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Banking Submit from page function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Submit from page Parent function
	///-------------------------------------------///
	function submitFromPage() {
			
	};
	///-------------------------------------------///
	/// Banking Submit from page Methods
	///-------------------------------------------///
	submitFromPage.prototype = {
		//Ajax standard Submit from page Method
		submit : function (method, loadObj) {
			
			//Setup defaults
			loadObj = fnb.hyperion.controller.getAjaxLoadDefaults(loadObj);
			
			//Add sender to loadObj
			loadObj.method = method;
			
			//Set default async param to true
			loadObj.async = true;
			
			//Append loadObj with target parameters
			loadObj = fnb.hyperion.utils.ajax.parameters.get(loadObj);

			//Run callback if needed
			fnb.hyperion.functions.callback.call(loadObj.preLoadingCallback, loadObj);

			//Ajax post data
			fnb.hyperion.controller.ajax("loadPage.load", method , loadObj);
			
		},
		//Ajax standard Submit from page Success
		success : function (sender,loadObj) {

			//Get data from ajax response
			loadObj.data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
			
			//Test for valid response
			if(!fnb.hyperion.controller.validResponse(loadObj)) return;
			
			//Validate response
			if(!fnb.hyperion.controller.validateXHR(loadObj.xhr,loadObj.postLoadingCallBack,sender)) return;
			
			//Test if otp should be raised
			if(fnb.hyperion.utils.ajax.otp.test(loadObj)) return;
			
			//Callback for when load has finished
			function jsLoadComplete(){
			
				//Eval js scripts
				fnb.hyperion.load.evalScripts(scripts.scripts);
				
				//Try send tracking information
				try{
					fnb.hyperion.utils.tracking.checkTrackingObject(sender,loadObj);
				}catch(e){
					console.log("Hyperion Tracking Error: "+e);
				}
				
				//Check load object initial page settings
				fnb.hyperion.controller.checkLoadObjSettings(loadObj);

				//Run callback if needed
				fnb.hyperion.functions.callback.call(loadObj.postLoadingCallback, loadObj);
				
				//Reset xhr
				fnb.hyperion.ajax.xhr = '';
				
				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
			}
			
			//Clear js in head
			fnb.hyperion.load.clearJs();
			
			//Clear css in head
			fnb.hyperion.load.clearCss();
			
			//Clear page object
			fnb.hyperion.controller.clearPageObjects();
			
			//Clear html templates
			fnb.hyperion.controller.clearHtmlTemplates(loadObj);

			//Get html script tags and clean html
        	var scripts = fnb.hyperion.load.collectScripts(loadObj.data);

        	//Get html css tags and clean html
        	var css = fnb.hyperion.load.collectCss(scripts.html);

			//Import css
			if(css.imports.length>0) fnb.hyperion.load.css(css.imports);
				
			//Append target content
			fnb.hyperion.utils.ajax.content.set(loadObj, css);
			
			//Test for scripts to load
			if(scripts.imports.length>0){
	        	fnb.hyperion.load.js(scripts.imports, jsLoadComplete);
			}else{
				jsLoadComplete();
			}
			
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.submitFromPage = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.submitFromPage', submitFromPage, true);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Callback Object
///-------------------------------------------///
(function() {
    ///-------------------------------------------///
	/// Callback Parent function
	///-------------------------------------------///
	function callback() {

	};
	///-------------------------------------------///
	/// Callback Methods
	///-------------------------------------------///
	callback.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init FNB Callback
    	init: function () {
    		
        },
        //Run callback
        call: function (callback, data) {
        	//Run Callback function if exist
			if(callback) {
				//Test to see if callback needs to be wrapped in a function
				if(typeof callback == 'string'){
					//Test if callback not an empty string
					if(callback!=''){
						//Wrap callback
						var func = new Function(callback);
						//Execute
						func(data);
					};
				}else{
					//Execute
					callback(data);
				};
			};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.callback = {};
        }
	};

	//Namespace callback
	fnb.namespace('functions.callback', callback, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Print Object
///-------------------------------------------///
(function() {
    ///-------------------------------------------///
	/// Print Parent function
	///-------------------------------------------///
	function print() {

	};
	///-------------------------------------------///
	/// Print Methods
	///-------------------------------------------///
	print.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init FNB Print
    	init: function () {
    		
        },
        //Run print
        go: function (loadObj) {
		
        	//Test if target got passed otherwise print window
        	if(loadObj.target!="" || typeof loadObj.target!="undefined"){

        		//Get target html
        		var data = (typeof loadObj.target == "string") ? (fnb.hyperion.$(loadObj.target).length()>0) ? fnb.hyperion.$(loadObj.target).html() : "" : loadObj.target.html();
        		
        		//Get window dimentions
        		var windowDimentions = this.getWindowDimentions();
        		
        		//Get window width
        		var windowWidth = windowDimentions.width;

        		//Get window height
        		var windowHeight = windowDimentions.height;
        		
        		//Create print window
        		var printWin = window.open('','FNB Print','width='+windowWidth+',height='+windowHeight);
        		
        		//Create Head and title
        		printWin.document.write('<HTML>\n<HEAD>\n');
        		printWin.document.write('<TITLE>FNB Print Page</TITLE>\n');
        		
        		//Add script to check if the window is rady to print and the window ready to close
        		printWin.document.write('<script>\n');
        		printWin.document.write('function chkstate(){\n');
        		printWin.document.write('if(document.readyState=="complete"){\n');
        		printWin.document.write('window.close()\n');
        		printWin.document.write('}\n');
        		printWin.document.write('else{\n');
        		printWin.document.write('setTimeout("chkstate()",2000)\n');
        		printWin.document.write('}\n');
        		printWin.document.write('}\n');
        		printWin.document.write('function print_win(){\n');
        		printWin.document.write('window.print();\n');
        		printWin.document.write('chkstate();\n');
        		printWin.document.write('}\n')
        		printWin.document.write('<\/script>\n');
        		
        		//Add css imports Print.css and core.css
        		printWin.document.write('<link rel="stylesheet" href="/banking/01css_new/global/core.css" type="text/css" />');
        		printWin.document.write('<link rel="stylesheet" href="/banking/01css_new/global/print.css" type="text/css" />');
        		
        		//Close head
        		printWin.document.write('</HEAD>\n');
        		
        		//Create Body
        		printWin.document.write('<BODY onload="print_win()">\n');
        		
        		//Write data
        		printWin.document.write(data);
        		
        		//Close Body and html
        		printWin.document.write('</BODY>\n');
        		printWin.document.write('</HTML>\n');
        		
        		//Close print window
        		printWin.document.close();
        		
        	}else{
        		//Print window
        		window.print();
        	}
        },
        //Get window dimentions
        getWindowDimentions: function () {
        	//Declare window width and height vars
        	var windowWidth = 0;
        	var windowHeight = 0;
        	
        	if( typeof( window.innerWidth ) == 'number' ) {
				//Non-IE
				windowWidth = window.innerWidth;
				windowHeight = window.innerHeight;
			} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
				//IE 6+ in 'standards compliant mode'
				windowWidth = document.documentElement.clientWidth;
				windowHeight = document.documentElement.clientHeight;
			} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
				//IE 4 compatible
				windowWidth = document.body.clientWidth;
				windowHeight = document.body.clientHeight;
			}
        	
        	return {width: windowWidth, height: windowHeight};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.print = {};
        }
	};

	//Namespace print
	fnb.namespace('functions.print', print, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// ScrollTo Object
///-------------------------------------------///
(function() {
    ///-------------------------------------------///
	/// ScrollTo Parent function
	///-------------------------------------------///
	function scrollTo() {

	};
	///-------------------------------------------///
	/// ScrollTo Methods
	///-------------------------------------------///
	scrollTo.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Var for scroll position
		scrollPosition: false,
		//Init FNB ScrollTo
    	init: function () {
    		
        },
        //Get target scroll position
        getPosition: function (target) {
        	this.scrollPosition = target.scrollTop;
        },
        //Set target scroll position
        setPosition: function (target) {
        	target.scrollTop = this.scrollPosition;
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.scrollTo = {};
        }
	};

	//Namespace scrollTo
	fnb.namespace('functions.scrollTo', scrollTo, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// supportPositionFixed Object
///-------------------------------------------///
(function() {
    //Test if fixed not supported
	function test() {
    	
		var w   = window,
	        ua  = navigator.userAgent,
	        isSupportFixed  = true;
 	    // Black list the following User Agents
 	    if (
 	        // IE less than 7.0
 	        (/MSIE (\d+\.\d+);/i.test(ua) && RegExp.$1 < 7)          ||
 	        // iOS less than 5
 	        (/OS [2-4]_\d(_\d)? like Mac OS X/i.test(ua))            ||
 	        // Android less than 3
 	        (/Android ([0-9]+)/i.test(ua) && RegExp.$1 < 3)          ||
 	        // Windows Phone less than 8
 	        (/Windows Phone OS ([0-9])+/i.test(ua) && RegExp.$1 < 8) ||
 	        // Opera Mini
 	        (w.operamini && ({}).toString.call( w.operamini ) === "[object OperaMini]") ||
 	        // Kindle Fire
 	        (/Kindle Fire/i.test(ua) || /Silk\//i.test(ua)) ||
 	        // Nokia Symbian, Opera Mobile, wOS
 	        (/Symbian/i.test(ua)) || (/Opera Mobi/i.test(ua)) || (/wOSBrowser/i.test(ua)) ||
 	        // Firefox Mobile less than 6
 	        (/Fennec\/([0-9]+)/i.test(ua) && RegExp.$1 < 6)
 	        // Optionally add additional browsers/devices here . . .
	 	    ){
 	        isSupportFixed = false;
 	        }
 	    return isSupportFixed;
    };
    //Set fixed attribute if fixed not supported
    function setFixedAttribute() {
    	//Set attribute to html
    	fnb.hyperion.$('=html').first().attr('data-fixed','false');
    };
    ///-------------------------------------------///
	/// supportPositionFixed Parent function
	///-------------------------------------------///
	function supportPositionFixed() {

	};
	///-------------------------------------------///
	/// supportPositionFixed Methods
	///-------------------------------------------///
	supportPositionFixed.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Is position fixed supported
		isSupportFixed: true,
		//Init FNB supportPositionFixed init
    	init: function () {
    		//Check if device supports position fixed
    		var isSupported = test();
    		//Set data attribute if fixed not supported
    		if(!isSupported) setFixedAttribute();
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.supportPositionFixed = {};
        }
	};

	//Namespace supportPositionFixed
	fnb.namespace('functions.supportPositionFixed', supportPositionFixed, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Validate XHR Response Object -- Banking
///-------------------------------------------///
(function() {
    ///-------------------------------------------///
	/// Validate XHR Response Parent function
	///-------------------------------------------///
	function validateXHR() {

	};
	///-------------------------------------------///
	/// Validate XHR Response Methods
	///-------------------------------------------///
	validateXHR.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init FNB Validate XHR
    	init: function () {
    		
        },
        //Validate xhr
        validate: function (ajaxXHR, callback, sender) {
        	
        	//Get error code
        	var errorCode = (ajaxXHR.getResponseHeader("NAV_ERROR_CODE")) ? parseInt(ajaxXHR.getResponseHeader("NAV_ERROR_CODE")) : 0;
        	//Get error message
        	var errorMessage = ajaxXHR.getResponseHeader("NAV_ERROR_MESSAGE");
        	//Get error message
        	var detailErrorMessage = (ajaxXHR.getResponseHeader("NAV_DETAIL_MESSAGE")) ? ajaxXHR.getResponseHeader("NAV_DETAIL_MESSAGE") : "";
        	//Get response text
        	var responseText =  ajaxXHR.responseText;

        	//Switch error message types
        	switch(errorCode)
			{
				case  0:
					
					//Return valid response
					return true;
					
				break;
				case 4:
				case 5:
					
					//Create error message
					if(detailErrorMessage==""||detailErrorMessage=="Invalid Parameter"){
						errorMessage = responseText;
					}else{
						errorMessage = "(E-" + errorCode +") "+ errorMessage + "<br/>" + responseText;
					}
					
					//Append error message
					errorMessage = "Some required fields are not valid..." + "<br/>" +errorMessage;
					
					//Raise error
					this.error(errorMessage, ajaxXHR, callback, sender);
					
				break;
				case  1544:

					//Create error message
					errorMessage = "OTP Sent..." + "<br/>" + detailErrorMessage + "<br/>" + responseText;

					//Raise error
					this.error(errorMessage, ajaxXHR, callback, sender);
					
				break;
				case 1190:
					
					//Create error message
					if(detailErrorMessage==""||detailErrorMessage=="Invalid Parameter"){
						errorMessage = responseText;
					}else{
						errorMessage = "(E-" + errorCode +") "+ errorMessage + "<br/>" + responseText;
					}
					
					//Append error message
					errorMessage = "Some required fields are not valid..." + "<br/>" +errorMessage;
					
					//Raise error
					this.error(errorMessage, ajaxXHR, callback, sender);
					
					//Request that controller kills ajax
		        	fnb.hyperion.controller.destroyAjax();
					
				break;
				case 96:

					//Raise error
					this.error(errorMessage, ajaxXHR, callback, sender);
					
				break;
				default:

					//Create error message
					if(detailErrorMessage!=""){
						errorMessage = "Some required fields are not valid..." + "<br/>" +"(E-" + errorCode +") "+ detailErrorMessage + "<br/>" + responseText;
					}else{
						errorMessage = "Some required fields are not valid..." + "<br/>" +"(E-" + errorCode +") "+ errorMessage + "<br/>" + responseText;
					}

					//Raise error
					this.error(errorMessage, ajaxXHR, callback, sender);
				
				break;
			};
        	
			//Return invalid response
			return false;
			
        },
        //Validation error
        error: function (message, ajaxXHR, callback, sender) {
        	
        	//Notify Controller to raise Error event
        	fnb.hyperion.controller.error(message,sender);
        	
        	//Test for callback
        	if(typeof(callback)=="function"){callback(ajaxXHR)};
        	
			//Return invalid response
			return false;
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.validateXHR = {};
        }
	};

	//Namespace scrollTo
	fnb.namespace('functions.validateXHR', validateXHR, true);

})();
