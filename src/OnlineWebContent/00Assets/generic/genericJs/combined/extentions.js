///-------------------------------------------///
/// developer: Donovan
///
/// Ajax Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax Parent function
	///-------------------------------------------///
	function ajax() {

	};
	///-------------------------------------------///
	/// Ajax Methods
	///-------------------------------------------///
	ajax.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Request object
		xhr: null,
		//Load object
		loadObject: null,
		//Timeout period (in ms) until an async request will be aborted
		timeout: null,
		//Default method of the request, either GET (default), POST, or HEAD
		method : "POST",
		//Default method asynchronous request
		async : true,
		//Parameters posted
		parameters : '',
		//Object notification
		notify : {},
		//Header types object
		headers: {
			'X-Requested-With': 'XMLHttpRequest',
	        'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
	    },
		//Default encoding
		encoding: 'utf-8',
		//Init FNB LoadUrl
    	init: function () {
    		console.log('Ajax init');
        },
        //Create new request  object
        getHttpObject: function (loadObj) {
        	var ajaxObject = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTTP');
        	ajaxObject.onreadystatechange = function(){
        		//Test state change
        		if(ajaxObject.readyState===4){
        			//Test if response was successfull
        			if(ajaxObject.status!=200){fnb.hyperion.ajax.handleErrors(ajaxObject,loadObj);}else{fnb.hyperion.ajax.success(ajaxObject,loadObj);};
        		}else if(ajaxObject.readyState===2){
        			//Test for complete response
        			fnb.hyperion.ajax.complete(ajaxObject,loadObj);
        		}
        	};
        	return ajaxObject;
        },
        //Make ajax request
        request: function (loadObj) {
        	//Set loadObect
        	this.loadObject = loadObj;
        	//Create ajax object and request
        	this.xhr = this.getHttpObject(loadObj);
        	//Raise before request event event
        	this.beforeSend(loadObj, this.xhr);
        	//Get request method
        	var method = (loadObj.type) ? loadObj.type.toUpperCase() : this.method;
        	//Get request type
        	var async = (loadObj.async) ? loadObj.async : this.async;
        	//Get data
        	var data = loadObj.params;
        	//Get url
        	var url = (method=="GET") ? (data)? loadObj.url+'?' + data : loadObj.url : loadObj.url;
        	//Open http request
        	this.xhr.open(method, url, async);
        	//Add Content type header if request method POST
        	if (method=="POST") {
            	var encoding = (this.encoding) ? '; charset=' + this.encoding : '';
            	this.xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded' + encoding);
        	}
        	//Try set other request headers
        	for (var i in this.headers){
                try {
                	xhr.setRequestHeader(i, this.headers[i]);
                } catch (e){

                }
        	}
        	//Send request
        	this.xhr.send((data) ? data : '');
        },
        //Handle ajax state change
        handleErrors: function (xhr,loadObj) {

        	//Test if errors are allowed
        	if(loadObj.error){
        		//Test for error code
            	switch (xhr.status) {
            		case 0:
            			// Connection ended
            			//Set staus
            			loadObj.status = "Error";
            			
            			//Set statusText
            			loadObj.statusText = "A result could not be retrieved from Online Banking. Please check your Internet connection before you continue. If you were performing a financial transaction, please check your transaction history to determine if the transaction was processed before you try again.";
            			
            			if(!loadObj.ajaxAborted) this.error(xhr,loadObj);
            			
            			//Check if ezi needs to be closed
            			fnb.hyperion.controller.checkEziState(loadObj.method)
            			
         	            break;	
    	            case 400:
    	            // Bad Request
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 401:	
    	            // Unauthorized
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 403:
    	            // Unauthorized
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 404:
    	            // Not Found
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 405:
    	            // Method Not Allowed
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 406:
    	            // Not Acceptable
    	            
    	            case 408:
    	            // Request Timeout
    	            	this.error(xhr,loadObj);
    	            	break;
    	            case 500:	
    	            // Internal Server Error
    	            	this.error(xhr,loadObj);
    	            break;
    	            default: this.error(xhr,loadObj);
    	         }
        	}
        	
        },
        //Reset Ajax object
        reset: function () {
        	this.xhr = null;
    		//Timeout period (in ms) until an async request will be aborted
        	this.timeout = null;
    		//Default method of the request, either GET (default), POST, or HEAD
        	this.method = "POST";
    		//Default method asynchronous request
        	this.async = true;
    		//Parameters posted
        	this.parameters = '';
    		//Header types object
        	this.headers = {
    			'X-Requested-With': 'XMLHttpRequest',
    	        'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
    	    };
    		//Default encoding
        	this.encoding = 'utf-8';
        },
        //Function executed before ajax call
        beforeSend: function (xhr,loadObj) {
        	//Check if sender wants event to be raised
        	if(loadObj.beforeSend){
        		//Get the calling method form the loadObj
        		var method = loadObj.method;
    			//Ensure there is a default method
    			if(!method) method = '';
            	//Add jqXHR to loadObj
        		loadObj.xhr = xhr;
        		//Notify Controller to raise BeforeSend event
            	fnb.hyperion.controller.raiseEvent(method+'BeforeSend', loadObj);
        	};
        },
        //Function executed when ajax request has completed
        complete: function (xhr,loadObj) {
        	//Check if sender wants event to be raised
        	if(loadObj.complete){
        		//Get the calling method form the loadObj
        		var method = loadObj.method;
    			//Ensure there is a default method
    			if(!method) method = '';
            	//Add jqXHR to loadObj
            	loadObj.xhr = xhr;
            	//Add textStatus to loadObj
            	loadObj.textStatus = xhr.textStatus;
        		//Notify Controller to raise Complete event
            	fnb.hyperion.controller.raiseEvent(method+'Complete', loadObj);
        	};
        },
        //Function executed on ajax request success
        success: function (xhr,loadObj) {
			
        	//Add jqXHR to loadObj
        	loadObj.xhr = xhr;
        	
			//Get data from ajax response
			loadObj.data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
        	
    		//Get the calling method form the loadObj
    		var method = loadObj.method;
    		
        	//Test if request was a post
        	if(loadObj.validate!=false){

    			//Validate response
    			if(!fnb.hyperion.controller.validateXHR(xhr,loadObj.postLoadingCallBack,method)) return;
    			
        		//Test for valid response
    			if(!fnb.hyperion.controller.validResponse(loadObj)) return;
    			    			
    			//Test if otp should be raised
    			if(fnb.hyperion.utils.ajax.otp.test(loadObj)) return;
        	}

        	//Check if sender wants event to be raised
        	if(loadObj.success){
        		//Get the calling method form the loadObj
        		var method = loadObj.method;
    			//Ensure there is a default method
    			if(!method) method = '';
    			//Raise Success event
    			fnb.hyperion.controller.raiseEvent(method+'Success', loadObj);
        	};
        	
        },
        //Function executed on ajax request error
        error: function (xhr,loadObj) {

    		//Get status
    		var status = (xhr.status===0) ? loadObj.status : xhr.status;
    		//status text
    		var statusText = (xhr.status===0) ? loadObj.statusText : xhr.statusText;

    		//Notify Controller to raise Error event
        	fnb.hyperion.controller.error(status + ' : '+statusText,loadObj.method);
        	
        },
        //Stop current ajax request
        stop: function (sender) {
        	//Test if there is a xhr
        	if(this.xhr){
        		console.log('Sender: '+sender+' requested an ajax abort on '+this.loadObject.method);
        		this.loadObject.ajaxAborted = true;
				this.xhr.abort();
        		console.log('Ajax aborted.');
			}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.ajax = {};
        }
	};

	//Namespace ajax
	fnb.namespace('ajax', ajax, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Animate Object
///-------------------------------------------///
/*USAGE:
	Ease types:
		ease-in
		lin
		ease
	Simple Animate:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'), {height:"90%"}, 3, "ease-in");
	Wait then simple animate:
		fnb.hyperion.animate().wait(1).animate(fnb.hyperion.$('#item1'), {height:"90%"}, 3, "ease-in");
	Simple animate multiple properties:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'), {width:"150px", height:"170px", left: "300px", top: "20px"}, 3, "ease-in");
	Simple animate with callback:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'), {height:"90%"}, 3, "ease-in").callBack(handler);
	Simple from animation:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'),{bottom:{from:"-50px", to:'auto'},opacity:{from:"0", to:1}}, 3, "ease-in");
	Chained animation:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'), {height:"90%"}, 3, "ease-in")
		.animate(fnb.hyperion.$('#item2'), {width:"150px", height:"170px", left: "300px", top: "20px"}, 0.5)
		.wait(0.5)
		.animate(fnb.hyperion.$('#item3'), {width:{to:"60px", e:"ease-in"}, height:"80px", left: 0, top: 0}, 0.5)
		.wait(0.5)
		.callBack(complete);
		
 */
(function() {
	///-------------------------------------------///
	/// Animate Parent function
	///-------------------------------------------///
	function animate(animateObject) {
		//Main animate object
		animateObject = function (node, cssGroup, time, ease) {
			//Test for element
	        node = (node) ? (node.elem) ? node.elem : node : 0.1;
			//Animate object vars
	        var attribute, object, queue = [],
	        //Setup node
			//Animate object callback function
	        callback = function (node) {
	                if (node = queue.shift()) node[1] ? animateObject.apply(this, node).animate(callback) : 0 < node[0] ? setTimeout(callback, 1E3 * node[0]) : (node[0](), callback());
	        };
			//Select Node
	        node.charAt && (node = document.getElementById(node));
			//Test if noe exists
	        if (0 < node || !node) cssGroup = {}, time = 0, callback(queue = [[node || 0]]);
			//Expand css
	        expand(cssGroup, {
	            padding: 0,
	            margin: 0,
	            border: "Width"
	        }, [top, right, bottom, left]);
	        //Expand border radius
	        expand(cssGroup, {
	            borderRadius: "Radius"
	        }, [top + left, top + right, bottom + right, bottom + left]);
	        //Increase animateObject instatnce
			++animInstance;
			//Loop attributes
	        for (attribute in cssGroup) object = cssGroup[attribute], !object.to && 0 !== object.to && (object = cssGroup[attribute] = {
	            to: object
	        }), animateObject.definitions(object, node, attribute, ease);
			//Iterate attributes
	        animateObject.iterate(cssGroup, 1E3 * time, callback);
			//Bind functions to object and return them
	        return {
	            animate: function () {
	                queue.push([].slice.call(arguments));
	                return this;
	            },
				wait: function () {
					queue.push([].slice.call(arguments));
	                return this;
	            },
				callBack: function () {
					queue.push([].slice.call(arguments));
	                return this;
	            }
	        };
	    };
		
		//Declare dimension vars for shorthand
	    var top = "Top",
        right = "Right",
        bottom = "Bottom",
        left = "Left",
        animInstance = 1,
	
        //Split shorthand css
        expand = function (node, elem, direction, attribute, index, dimention, obj) {
            for (attribute in node)
                if (attribute in elem) {
                    obj = node[attribute];
                    for (index = 0; dimention = direction[index]; index++) node[attribute.replace(elem[attribute], "") + dimention + (elem[attribute] || "")] = {
                        to: 0 === obj.to ? obj.to : obj.to || obj,
                        fr: obj.from,
                        elem: obj.e
                    };
                    delete node[attribute];
                }
        }, 
		getRequestFrame = function (win, frame) {
            return win["webkitR" + frame] || win["mozR" + frame] || win["msR" + frame] || win["r" + frame] || win["oR" + frame];
		}(window, "equestAnimationFrame");
	
		//Animation definition object
		animateObject.definitions = function (obj, elem, node, b, g) {
	        g = elem.style;
	        obj.node = node;
	        obj.bottom = elem;
	        obj.s = node in g ? g : elem;
	        obj.e = obj.e || b;
	        obj.from = obj.from || (0 === obj.from ? 0 : obj.s == elem ? elem[node] : (window.getComputedStyle ? getComputedStyle(elem, null) : elem.currentStyle)[node]);
	        obj.u = (/\d(\D+)$/.exec(obj.to) || /\d(\D+)$/.exec(obj.from) || [0, 0])[1];
	        obj.fn = /color/i.test(node) ? animateObject.effects.color : animateObject.effects[node] || animateObject.effects.func;
	        obj.mx = "anim_" + node;
	        elem[obj.mx] = obj.mxv = animInstance;
	        elem[obj.mx] != obj.mxv && (obj.mxv = null);
	    };
	    
	    //Iterate over css properties
	    animateObject.iterate = function (node, time, callback) {
	        var iter, unique, obj, newTime, effect, calcTime = +new Date + time;
	        iter = function (top) {
	            unique = calcTime - (top || (new Date).getTime());
	            if (50 > unique) {
	                for (obj in node) obj = node[obj], obj.property = 1, obj.fn(obj, obj.bottom, obj.to, obj.from, obj.node, obj.e);
	                callback && callback();
	            } else {
	                unique /= time;
	                for (obj in node) {
	                    obj = node[obj];
	                    if (obj.bottom[obj.mx] != obj.mxv) return;
	                    effect = obj.e;
	                    newTime = unique;
	                    "lin" == effect ? newTime = 1 - newTime : "ease" == effect ? (newTime = 2 * (0.5 - newTime), newTime = 1 - (newTime * newTime * newTime - 3 * newTime + 2) / 4) : "ease-in" == effect ? (newTime = 1 - newTime, newTime *= newTime * newTime) : newTime = 1 - newTime * newTime * newTime;
	                    obj.property = newTime;
	                    obj.fn(obj, obj.bottom, obj.to, obj.from, obj.node, obj.e);
	                }
	                getRequestFrame ? getRequestFrame(iter) : setTimeout(iter, 20, 0);
	            }
	        };
	        iter();
	    };
	    
		//CSS names which need special handling
	    animateObject.effects = {
	    	       
		   func: function (obj, node, to, from, attribute) {
	            from = parseFloat(from) || 0;
	            to = parseFloat(to) || 0;
	            obj.s[attribute] = (1 <= obj.property ? to : obj.property * (to - from) + from) + obj.u;
	        },
			
	        width: function (obj, node, to, from, attribute, d) {
	            0 <= obj._fr || (obj._fr = !isNaN(from = parseFloat(from)) ? from : "width" == attribute ? node.clientWidth : node.clientHeight);
	            animateObject.effects.func(obj, node, to, obj._fr, attribute, d)
	        },
			
	        opacity: function (obj, node, to, from, attribute) {
	            if (isNaN(from = from || obj.from)) from = node.style, from.zoom = 1, from = obj._fr = (/alpha\(opacity=(\d+)\from/i.exec(from.filter) || {})[1] / 100 || 1;
	            from *= 1;
	            to = obj.property * (to - from) + from;
	            node = node.style;
	            attribute in node ? node[attribute] = to : node.filter = 1 <= to ? "" : "alpha(" + attribute + "=" + Math.round(100 * to) + ")";
	        },

	        color: function (obj, node, to, from, attribute, d, c, j) {
	            obj.ok || (to = obj.to = animateObject.toRGBA(to), from = obj.from = animateObject.toRGBA(from), 0 == to[3] && (to = [].concat(from), to[3] = 0), 0 == from[3] && (from = [].concat(to), from[3] = 0), obj.ok = 1);
	            j = [0, 0, 0, obj.property * (to[3] - from[3]) + 1 * from[3]];
	            for (c = 2; 0 <= c; c--) j[c] = Math.round(obj.property * (to[c] - from[c]) + 1 * from[c]);
	            (1 <= j[3] || animateObject.rgbaIE) && j.pop();
	            try {
	                obj.s[attribute] = (3 < j.length ? "rgba(" : "rgb(") + j.join(",") + ")";
	            } catch (k) {
	                animateObject.rgbaIE = 1;
	            }
	        }
	        
	    };
	    
	    //Add height method to effects
	    animateObject.effects.height = animateObject.effects.width;
	    //Regex for colors
	    animateObject.RGBA = /#(.)(.)(.)\b|#(..)(..)(..)\b|(\d+)%,(\d+)%,(\d+)%(?:,([\d\.]+))?|(\d+),(\d+),(\d+)(?:,([\d\.]+))?\b/;
	    //Convert colors to RGBA
	    animateObject.toRGBA = function (node, colorArray) {
	        colorArray = [0, 0, 0, 0];
	        node.replace(/\s/g, "").replace(animateObject.RGBA, function (node, b, g, d, c, animateObject, k, top, right, bottom, p, q, r, s, t) {
	            k = [b + b || c, g + g || animateObject, d + d || k];
	            b = [top, right, bottom];
	            for (node = 0; 3 > node; node++) k[node] = parseInt(k[node], 16), b[node] = Math.round(2.55 * b[node]);
	            colorArray = [k[0] || b[0] || q || 0, k[1] || b[1] || r || 0, k[2] || b[2] || s || 0, p || t || 1];
	        });
	        return colorArray;
	    };

	    return animateObject;
	    
	}
	
	//Namespace animate
	fnb.namespace('animate', animate, true);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// External content loader
///-------------------------------------------///
(function() {

	//Browser info
	var browser;
	//Requests in progress
	var pending = {};
	// Number of tests performed
	var testCount = 0;
	//Get doc
	var doc = this.document;
	//Reference to head
	var head = doc.head || doc.getElementsByTagName('head')[0];
	// Queued requests.
	var queue = {css: [], js: []};
	styleSheets = doc.styleSheets;
	
	//Simple function to create a node
	function createNode(name, attrs) {
	    var node = doc.createElement(name), attr;
	    for (attr in attrs) {
	    	if (attrs.hasOwnProperty(attr)) {
	    		node.setAttribute(attr, attrs[attr]);
	    	}
	    }
	    return node;
	}
	
	//Count recources that have been loaded
	function finish(type) {
		var pendingType = pending[type];
		var callback;
		var urls;
		
		if(pendingType) {
		  callback = pendingType.callback;
		  urls = pendingType.urls;
		
		  urls.shift();
		  testCount = 0;
		
		  //Test if loading has completed
		  if (!urls.length) {
		    callback && callback.call(pendingType.context, pendingType.obj);
		    pending[type] = null;
		    queue[type].length && load(type);
		  }
		}
	}
	
	//Get browser
	function getBrowser() {
		var ua = navigator.userAgent;
		
		browser = {
		  async: doc.createElement('script').async === true
		};
		
		(browser.webkit = /AppleWebKit\//.test(ua)) || (browser.ie = /MSIE|Trident/.test(ua)) || (browser.opera = /Opera/.test(ua)) || (browser.gecko = /Gecko\//.test(ua)) || (browser.unknown = true);
	}
	
	//Load scripts
	function load(type, urls, callback, obj, context) {

		var _finish = function () { finish(type); };
		var isCSS   = type === 'css';
		var nodes   = [];
		var i;
		var len;
		var node;
		var p;
		var pendingUrls;
		var url;
		
		browser || getBrowser();
		
		if (urls) {
		 
		  urls = typeof urls === 'string' ? [urls] : urls.concat();

		  if (isCSS || browser.async || browser.gecko || browser.opera) {
		  
			queue[type].push({
			  urls    : urls,
			  callback: callback,
			  obj     : obj,
			  context : context
			});
			
		  }else{
		  
			  for (i = 0, len = urls.length; i < len; ++i) {
				//Create queue
				queue[type].push({
					urls    : [urls[i]],
					callback: i === len - 1 ? callback : null,
					obj     : obj,
					context : context
				});
				
			  }
			
		  }
		}

		if (pending[type] || !(p = pending[type] = queue[type].shift())) {
			return;
		}

		head || (head = doc.head || doc.getElementsByTagName('head')[0]);
		pendingUrls = p.urls.concat();
		
		for (i = 0, len = pendingUrls.length; i < len; ++i) {
			url = pendingUrls[i];
		
			if (isCSS) {
				node = browser.gecko ? createNode('style') : createNode('link', {
					href: url,
					rel : 'stylesheet'
				});
			} else {
				node = createNode('script', {src: url});
				node.async = false;
			}
		
			node.className = type+'Loaded';
			node.setAttribute('charset', 'utf-8');
		
			if (browser.ie && !isCSS && 'onreadystatechange' in node && !('draggable' in node)) {
				node.onreadystatechange = function () {
					if (/loaded|complete/.test(node.readyState)) {
						node.onreadystatechange = null;
						_finish();
					}
			    };
			
			} else if (isCSS && (browser.gecko || browser.webkit)) {
		  
				if (browser.webkit) {
					p.urls[i] = node.href;
					testWebKit();
				} else {
					node.innerHTML = '@import "' + url + '";';
					testGecko(node);
			    }
				
			} else {
			  
				node.onload = node.onerror = _finish;
				
			}
		
			nodes.push(node);
		  
		}

		for (i = 0, len = nodes.length; i < len; ++i) {
			head.appendChild(nodes[i]);
		}
	}
	
	//Poll to test if loaded
	function testGecko(node) {
		var hasRules;
		try {
		  hasRules = !!node.sheet.cssRules;
		} catch (ex) {
		  testCount += 1;
		  if (testCount < 200) {
			setTimeout(function () { testGecko(node); }, 50);
		  } else {
			hasRules && finish('css');
		  }
		  return;
		}

		finish('css');
	}
	
	//Poll to test if loaded
	function testWebKit() {
	    var css = pending.css, i;
	    if (css) {
	      i = styleSheets.length;
	      while (--i >= 0) {
	        if (styleSheets[i].href === css.urls[0]) {
	          finish('css');
	          break;
	        }
	      }

	      testCount += 1;

	      if (css) {
	        if (testCount < 200) {
	          setTimeout(testWebKit, 50);
	        } else {
	          finish('css');
	        }
	      }
	    }
	}
	  
	//Collect all scripts from ajax response 
	function getScripts(html) {
		
		// Array which will store the eval scripts
		var scripts = new Array();
		
		// Array which will store the import scripts
		var imports = new Array();

		//Test for html
		if(fnb.hyperion.controller.is(html)){

			//Loop html and strip out tags
			while(html.indexOf("<script") > -1 || html.indexOf("</script") > -1) {
				
				//Find scripts starts
				var startScriptTag = html.indexOf("<script");
				
				//Find end of scripts starts
				var endScriptTag = html.indexOf(">", startScriptTag);
				
				//Find scripts closing tag
				var closedStartScriptTag = html.indexOf("</script", startScriptTag);
				
				//Find end of closing tag
				var closedEndScriptTag = html.indexOf(">", closedStartScriptTag);

				//Create string to test if import
				var importTestString = html.substring(startScriptTag, closedEndScriptTag+1);
				
				//Test if script has a src and add it to imports otherwise add it to evals
				var srcUrl = importTestString.match(/src="[\s\S]*?"/g);
					
				if(srcUrl) {
				
					//Test if script has a src and add it to imports otherwise add it to evals
					var data = (importTestString.match(/data-jsp='[\s\S]*?'/g)) ? importTestString.match(/data-jsp='[\s\S]*?'/g)[0].replace("data-jsp='","").replace("'","") : null;

					//Script has src
					var rawUrl = srcUrl[0].replace('src="','').replace('"','');
					
					//Test if data is bound to the include
					if(data){
						//Get includes data key
						var dataKey = rawUrl.split("/").pop().split('.js')[0];
						//Add includes data
						fnb.hyperion.load.includesData[dataKey] = JSON.parse(data);
					}

					//Add script id to headscripts
					imports.push(rawUrl);
					
				}else{
				
					// Add to scripts array
					scripts.push(html.substring(endScriptTag+1, closedStartScriptTag));
					
				}

				//Clean html
				html = html.substring(0, startScriptTag) + html.substring(closedEndScriptTag+1);

			}
		}
		
		//Return formatted loadObj
		return {scripts: scripts, html: html, imports: imports};
	}
	
	//Collect all links from ajax response 
	function getCss(html) {

		// Array which will store the import links
		var imports = new Array();
		
		// Array which will store the style contents
		var styles = new Array();

		//Test for html
		if(fnb.hyperion.controller.is(html)){
			//Loop html and strip out link tags
			while(html.indexOf("<link") > -1 || html.indexOf("</link") > -1) {
				
				//Find link starts
				var startLinkTag = html.indexOf("<link");
				
				//Find end of link starts
				var endLinkTag = html.indexOf(">", startLinkTag);

				//Create string to test if import
				var importTestString = html.substring(startLinkTag, endLinkTag+1);
				
				//Test if link has a href 
				var linkString = importTestString.match(/href="[\s\S]*?"/g);

				// Add to styles array
				if(linkString) imports.push(linkString[0].replace('href="','').replace('"',''));

				//Clean html
				html = html.substring(0, startLinkTag) + html.substring(endLinkTag+1);

			}
			
			//Loop html and strip out style tags
			while(html.indexOf("<style") > -1 || html.indexOf("</style") > -1) {
				
				//Find style starts
				var startStyleTag = html.indexOf("<style");
				
				//Find end of style starts
				var endStyleTag = html.indexOf(">", startStyleTag);
				
				//Find style closing tag
				var closedStartStyleTag = html.indexOf("</style", startStyleTag);
				
				//Find end of closing tag
				var closedEndStyleTag = html.indexOf(">", closedStartStyleTag);

				// Add to styles array
				styles.push(html.substring(endStyleTag+1, closedStartStyleTag));

				//Clean html
				html = html.substring(0, startStyleTag) + html.substring(closedEndStyleTag+1);

			}
			
		}
		
		//Return formatted loadObj
		return {styles: styles, html: html, imports: imports};
		
	}
	
	//Add styles to fragment
	function addStyles(target,styles) {
	
		// Loop through every styles and append to target
		for(var i=0; i<styles.length; i++) {
		
			//Create new style tag
			var style = document.createElement('style');
			
			//Add type
			style.type = 'text/css';
			
			//Add styles
			if (style.styleSheet){
				style.styleSheet.cssText = styles[i];
			} else {
				style.appendChild(document.createTextNode(styles[i]));
			}
			
			//Prepend target
			target.insertBefore(style,(target.hasChildNodes()) ? target.childNodes[0] : null);
			
		};
	}
	
	//Clear all css loaded
	function clearCss() {
		var styles = head.getElementsByClassName('cssLoaded');
		for(var i=0; i<styles.length; i++) { 
			head.removeChild(styles[i]);
		}
	}
	
	//Clear all js loaded
	function clearJs() {
		var scripts = head.getElementsByClassName('jsLoaded');
		for(var i=0; i<scripts.length; i++) { 
			head.removeChild(scripts[i]);
		};
		//Clear includes data object
		fnb.hyperion.load.includesData = {};
	}
	
	///-------------------------------------------///
	/// Loader Parent function
	///-------------------------------------------///
	function loader() {

	};
	///-------------------------------------------///
	/// Loader Methods
	///-------------------------------------------///
	loader.prototype = {
		includesData: {},
		//Js loader function
        js: function (urls, callback, obj, context) {
        	load('js', urls, callback, obj, context);
        },
        //Css loader function
        css: function (urls, callback, obj, context) {
        	load('css', urls, callback, obj, context);
        },
        //Styles loader function
        styles: function (target, styles) {
        	addStyles(target, styles)
        },
        //Clear css from head
        clearCss: function (urls, callback) {
        	clearCss();
        },
        //Clear js from head
        clearJs: function () {
        	clearJs();
        },
        //Collect inline scripts and import scripts
        collectScripts: function (data) {
        	return getScripts(data);
        },
        //Collect css links
        collectCss: function (data) {
        	return getCss(data);
        },
        //Eval scripts method
        evalScripts: function (scripts) {
			// Loop through every script collected and eval it
			for(var i=0; i<scripts.length; i++) {
				try {
					//Eval sccript
					window.eval(scripts[i]);
				}catch(e) {
				    //Script error
					console.log('Error: utils.ajax.scripts: '+e);
				}
			};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.load = {};
        }
	};

	//Namespace audit
	fnb.namespace('load', loader, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Audit Object
///-------------------------------------------///
(function() {
	//String audits object
	var audits = [
	              {string: 'getElementById', error: 'Please do not use javascript getElementById. Use e.g. fnb.hyperion.$("#example")'},
	              {string: 'getElementsByClassName', error: 'Please do not use javascript getElementsByClassName. Use e.g. fnb.hyperion.$(".example")'},
	              {string: 'getElementsByName', error: 'Please do not use javascript getElementsByName. Use e.g. fnb.hyperion.$("@example")'},
	              {string: 'getElementsByTagName', error: 'Please do not use javascript getElementsByTagName. Use e.g. fnb.hyperion.$("=example")'},
	              {string: 'querySelector', error: 'Please do not use javascript querySelector. Use e.g. fnb.hyperion.$("!example")'},
	              {string: 'querySelectorAll', error: 'Please do not use javascript querySelectorAll. Use e.g. fnb.hyperion.$("*example")'},
	              {string: 'setAttribute', error: 'Please do not use javascript setAttribute. Use e.g. fnb.hyperion.$("#example").attr("attr", "attrValue")'},
	              {string: 'getAttribute', error: 'Please do not use javascript getAttribute. Use e.g. fnb.hyperion.$("#example").attr("attr")'},
	              {string: 'parentNode', error: 'Please do not use javascript parentNode. Use e.g. fnb.hyperion.$("#example").parent()'},
	              {string: '.style', error: 'Please do not use javascript .style. Use e.g. fnb.hyperion.$(".example").css("width","10px")'},
	              {string: '.classList', error: 'Please do not use javascript .classList. Use e.g. fnb.hyperion.$(".example").addClass("example")'},
	              {string: '.display ', error: 'Please do not use javascript .display . Use e.g. fnb.hyperion.$(".example").show()'},
				 ];
	///-------------------------------------------///
	/// Audit Parent function
	///-------------------------------------------///
	function audit() {

	};
	///-------------------------------------------///
	/// Audit Methods
	///-------------------------------------------///
	audit.prototype = {
		//Audit function
        test: function (fn) {
        	//Error string
        	var errors = '';
        	//Convert function to string
        	var stringFunction = fn.toString();
    		//Loop audits
        	for (var audit in audits) {
        		//Test for non native elements
        		if(audits.hasOwnProperty(audit)){
	        		//Set current audit object
	        		var auditObject = audits[audit];
	        		//Test for audit match
	        		var find = stringFunction.indexOf(auditObject.string);
	        		//Test if audit was found
	        		if(find!=-1) errors = (errors=="") ? 'Page Object Error:\n - '+auditObject.error : errors +'\n - '+ auditObject.error;
        		}
        	};
        	//Test for errors and show
        	if(errors!='') alert(errors);
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.audit = {};
        }
	};

	//Namespace audit
	fnb.namespace('audit', audit, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Cookies Object
///-------------------------------------------///
/*EXAMPLE:
	//Setup fnb.hyperion.cookies
	fnb.hyperion.utils.cookies.defaults = {namespace: 'fnb.',expires: 365};
	
	//Get cookie
	fnb.hyperion.utils.cookies.get('userId')
	
	//Set cookies
	var cookies = {
	userId: '8767896787686',
	visitDate: '2014/02/01'
	}
	
	fnb.hyperion.utils.cookies.set(cookies)
*/
(function() {
    ///-------------------------------------------///
	/// Cookies Parent function
	///-------------------------------------------///
	function cookies() {

	};
	///-------------------------------------------///
	/// Cookies Methods
	///-------------------------------------------///
	cookies.prototype = {
		//Cookies defaults wrapper
		defaults:{},
		//Formula for expiry
		expiresMultiplier : 60 * 60 * 24,
		//Do init for cookies
		init: function () {
	  	
		},
		//Set cookie
		set: function (key, value, options) {
			//Test if key is plain object
			if (fnb.hyperion.cookies.utils.isPlainObject(key)) {
				//Loop keys in object and set cookie
				for (var k in key) {
					if (key.hasOwnProperty(k)) this.set(k, key[k], value);
				}
			} else {
				//Do test for options and set defaults
				options = fnb.hyperion.cookies.utils.isPlainObject(options) ? options : { expires: options };
				//Set expiry
				var expires = options.expires !== undefined ? options.expires : (this.defaults.expires || ''),
				    expiresType = typeof(expires);
				//Calculate expiry
				if (expiresType === 'string' && expires !== '') expires = new Date(expires);
				else if (expiresType === 'number') expires = new Date(+new Date + 1000 * this.expiresMultiplier * expires);
				//Expiry to string
				if (expires !== '' && 'toGMTString' in expires) expires = ';expires=' + expires.toGMTString();
				//Setup path
				var path = options.path || this.defaults.path;
				path = path ? ';path=' + path : '';
				//Setup Domain
				var domain = options.domain || this.defaults.domain;
				domain = domain ? ';domain=' + domain : '';
				//Setup Secure
				var secure = options.secure || this.defaults.secure ? ';secure' : '';
				//Setup namespace
				var namespace = options.namespace || this.defaults.namespace ? this.defaults.namespace : '';
				//Set cookie
				document.cookie = fnb.hyperion.cookies.utils.escape(namespace+key) + '=' + fnb.hyperion.cookies.utils.escape(value) + expires + path + domain + secure;

			}

			return this;
		},
		//Remove cookie
		remove: function (keys) {
			//Convert keys to array
			keys = fnb.hyperion.cookies.utils.isArray(keys) ? keys : fnb.hyperion.cookies.utils.toArray(arguments);
			//Test for namespace
			var namespace = this.defaults.namespace ? this.defaults.namespace : '';
			//Loop keys and remove
			for (var i = 0, l = keys.length; i < l; i++) {
				this.set(namespace+keys[i], '', -1);
			}

			return this;
		},
		//Remove cookie
		empty: function () {
			//Empty all cookies
			return this.remove(fnb.hyperion.cookies.utils.getKeys(this.all()));
		},
		//Get cookie
		get: function (keys, fallback) {
			//Setup fallback
			fallback = fallback || undefined;
			//Select all cookies
			var cookies = this.all();
			//Test for namespace
			var namespace = this.defaults.namespace ? this.defaults.namespace : '';
			//Loop retrieve if array
			if (fnb.hyperion.cookies.utils.isArray(keys)) {

				var result = {};

				for (var i = 0, l = keys.length; i < l; i++) {
					var value = namespace+keys[i];
					result[value] = fnb.hyperion.cookies.utils.retrieve(cookies[value], fallback);
				}

				return result;

			} else {
				return fnb.hyperion.cookies.utils.retrieve(cookies[namespace+keys], fallback);
			}
		},
		//Return all cookies
		all: function () {
			//Test for cookies
			if (document.cookie === '') return {};
			
			var cookies = document.cookie.split('; '),
				  result = {};
			//Loop cookies and seelct
			for (var i = 0, l = cookies.length; i < l; i++) {
				var item = cookies[i].split('=');
				result[decodeURIComponent(item[0])] = decodeURIComponent(item[1]);
			}

			return result;
		},
		//Test if cookies are enabled
		enabled: function () {
			if (navigator.cookieEnabled) return true;

			var ret = fnb.hyperion.cookies.set('_', '_').get('_') === '_';
			fnb.hyperion.cookies.remove('_');
			return ret;
		},
		//Remove current object from dom
		destroy: function () {
			fnb.tracking.cookies = {};
		}
	};
	
	///-------------------------------------------///
	/// Cookies Util Methods
	///-------------------------------------------///
	cookies.prototype.utils = {
		// Test if the given value an array?
		isArray: Array.isArray || function (value) {
			return Object.prototype.toString.call(value) === '[object Array]';
		},
		// Test the given value a plain object
		isPlainObject: function (value) {
			return !!value && Object.prototype.toString.call(value) === '[object Object]';
		},
		// Convert an array-like object to an array
		toArray: function (value) {
			return Array.prototype.slice.call(value);
		},
		// Get the keys of an object
		getKeys: Object.keys || function (obj) {
			var keys = [],
				 key = '';
			for (key in obj) {
				if (obj.hasOwnProperty(key)) keys.push(key);
			}
			return keys;
		},
		// Escapes characters that are not allowed in cookies.
		escape: function (value) {
			return String(value).replace(/[,;"\\=\s%]/g, function (character) {
				return encodeURIComponent(character);
			});
		},
		// Return fallback if the value is not defined, otherwise return value.
		retrieve: function (value, fallback) {
			return value == null ? fallback : value;
		}
	};
	
	//Namespace cookies
	fnb.namespace('cookies', cookies, true);

})();
///-------------------------------------------///
/// developer: Donovan Phillips
///
/// Debounce Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Debounce function
	///-------------------------------------------///
	function debounce(func, wait, immediate) {
		//Var for debounce timeout
		var timeout;
		//Return debounce function
		return function() {
			//Get function to be debounced context
			var context = this
			//Get function to be debounced arguments
			var args = arguments;
			//Clear timeout if exists
			clearTimeout(timeout);
			//Create new timeout for debouncing
			timeout = setTimeout(function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			}, wait);
			//Test for immediate flag and execute function
			if (immediate && !timeout) func.apply(context, args);
		};
	};

	//Namespace debounce
	fnb.namespace('debounce', debounce);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Delegate events
///-------------------------------------------///
/// USAGE: 
///
/// on:
///		/* 	Simple		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), 'click', handler);
///
///		/* 	Internal function		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), 'click', function (event) { console.log('Clicked');});
///
///		/* 	Optional parameters		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), 'click', function (event, param1, param2) { console.log('params: '+param1+' - '+param2);}, 'hallo', 'test');
///
///		/* 	Multiple events		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), 'mousedown mouseup', handler);
///
///		/* 	Multiple handlers		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), {
///			click: function (event) { console.log('click event') },
///			mouseover: function (event) { console.log('click event') },
///			'focus blur': function (event) { console.log('focus blur event') }
///		});
///
/// off:
///		/* 	Remove all events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'));
///
///		/* 	Remove specific handler		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), 'click', handler);
///
///		/* 	Remove all click events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), 'click');
///
///		/* 	Remove handler for all events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), handler);
///
///		/* 	Remove multiple events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), 'mouseup mousedown');
///
///		/* 	Remove specific handlers for specific events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), { click: clickFunction, mouseover: mouseoverFunction });
///
/// trigger:
///		/* 	Trigger single event		*/
///		fnb.hyperion.delegate.trigger(fnb.hyperion.$('#numberInput'), 'click');
///
///		/* 	Trigger multiple events		*/
///		fnb.hyperion.delegate.trigger(fnb.hyperion.$('#numberInput'), 'click, mouseover');
///
/// one:
///		/* 	Trigger event once then remove handler		*/
///		fnb.hyperion.delegate.one(fnb.hyperion.$('#numberInput'), 'click', handler);
///
/// clone:
///		/* 	Clone all events of an element		*/
///		fnb.hyperion.delegate.clone(fnb.hyperion.$('#numberInput'), fnb.hyperion.$('#textInput'));
///
///		/* 	Clone specific event of an element		*/
///		fnb.hyperion.delegate.clone(fnb.hyperion.$('#numberInput'), fnb.hyperion.$('#textInput'), 'click');
///
///-------------------------------------------///
(function() {
	//Select window
	var win = window;
	//Namespace regular expression
    var namespaceExpression = /[^\.]*(?=\..*)\.|.*/;
	//Name regular expression
    var nameExpression = /\..*/;
	//Add event type
    var addEvent = 'addEventListener';
	//Remove event type
    var removeEvent = 'removeEventListener';
	//Select Doc
    var doc = document || {};
	//Select root
    var root = doc.documentElement || {};
	//Get w3c model
    var model = root[addEvent];
	//Test event support for browsers
    var eventSupport = model ? addEvent : 'attachEvent';
	//Singleton for quick matching making add() do oneObject()
    var oneObject = {};
	
	//List of native events
	var standardNativeEvents = 'click dblclick mouseup mousedown contextmenu mousewheel mousemultiwheel DOMMouseScroll mouseover mouseout mousemove selectstart selectend keydown keypress keyup orientationchange focus blur change reset select submit load unload beforeunload resize move DOMContentLoaded readystatechange message error abort scroll';
	//List of model native events
	var modelNativeEvents = 'show input invalid touchstart touchmove touchend touchcancel gesturestart gesturechange gestureend textinput readystatechange pageshow pagehide popstate hashchange offline online afterprint beforeprint dragstart dragenter dragover dragleave drag drop dragend loadstart progress suspend emptied stalled loadmetadata loadeddata canplay canplaythrough playing waiting seeking seeked ended durationchange timeupdate play pause ratechange volumechange cuechange checking noupdate downloading cached updateready obsolete ' ;
	
	//Delegate Utils
	//Slice method
    var slice = Array.prototype.slice;
	//Convert string to array
    var stringToArray = function (string, devider) { 
		return string.split(devider || ' ');
	};
	//Test if object is string
    var isString = function (obj) { 
		return typeof obj == 'string';
	};
	//Test if object is function
    var isFunction = function (obj) { 
		return typeof obj == 'function';
	};
	
	//Convert hash events for fast loops
	var convertNativeEvents = (function (hash, events, i) {
        for (i = 0; i < events.length; i++) events[i] && (hash[events[i]] = 1);
			return hash;
    }({}, stringToArray(standardNativeEvents + (model ? modelNativeEvents : ''))));
	
	//Create custom events
	var handleCustomEvents = (function () {
        var isAscendant = 'compareDocumentPosition' in root
			? function (element, container) {
				return container.compareDocumentPosition && (container.compareDocumentPosition(element) & 16) === 16;
			}
			: 'contains' in root
			? function (element, container) {
				container = container.nodeType === 9 || container === window ? root : container;
				return container !== element && container.contains(element);
			}
			: function (element, container) {
				while (element = element.parentNode) if (element === container) return 1;
				return 0;
			},
			verify = function (event) {
				var related = event.relatedTarget;
				return 	!related
							? related == null
							: (related !== this && related.prefix !== 'xul' && !/document/.test(this.toString()) && !isAscendant(related, this));
            };
        return {
            mouseenter: { base: 'mouseover', condition: verify }, 
			mouseleave: { base: 'mouseout', condition: verify }, 
			mousewheel: { base: /Firefox/.test(navigator.userAgent) ? 'DOMMouseScroll' : 'mousewheel' }
		};
    }());
	
	//Create cross browser events object
	var Event = (function () {
		// Common Events list
		var commonProps  = stringToArray('altKey attrChange attrName bubbles cancelable ctrlKey currentTarget detail eventPhase getModifierState isTrusted metaKey relatedNode relatedTarget shiftKey srcElement target timeStamp type view which propertyName');
		//Mouse events
		var mouseProps   = commonProps.concat(stringToArray('button buttons clientX clientY dataTransfer fromElement offsetX offsetY pageX pageY screenX screenY toElement'));
		//Mouse wheel events
		var mouseWheelProps = mouseProps.concat(stringToArray('wheelDelta wheelDeltaX wheelDeltaY wheelDeltaZ axis'));
		//Key events
		var keyProps     = commonProps.concat(stringToArray('char charCode key keyCode keyIdentifier keyLocation location'));
		//Text events
		var textProps    = commonProps.concat(stringToArray('data'));
		//Touch events
		var touchProps   = commonProps.concat(stringToArray('touches targetTouches changedTouches scale rotation'));
		//Message events
		var messageProps = commonProps.concat(stringToArray('data origin source'));
		//State events
		var stateProps   = commonProps.concat(stringToArray('state'));
		//Ouver & Out regular expressions
		var overOutRegex = /over|out/;
		
		// Events that need special handling
		var typeFixers   = [
			//Special key events
			{
				reg: /key/i, 
				fix: function (event, newEvent) {
					newEvent.keyCode = event.keyCode || event.which;
					return keyProps;
				}
			}, 
			 //Special mouse events
			{
				reg: /click|mouse(?!(.*wheel|scroll))|menu|drag|drop/i,
				fix: function (event, newEvent, type) {
				
					newEvent.rightClick = event.which === 3 || event.button === 2;
					newEvent.pos = { x: 0, y: 0 };
					
					if (event.pageX || event.pageY) {
						newEvent.clientX = event.pageX;
						newEvent.clientY = event.pageY;
					} else if (event.clientX || event.clientY) {
						newEvent.clientX = event.clientX + doc.body.scrollLeft + root.scrollLeft;
						newEvent.clientY = event.clientY + doc.body.scrollTop + root.scrollTop;
					}
					
					if (overOutRegex.test(type)) {
						newEvent.relatedTarget = event.relatedTarget || event[(type == 'mouseover' ? 'from' : 'to') + 'Element'];
					}
					return mouseProps;
					
				}
			},
			//Special mouse wheel events
			{
				reg: /mouse.*(wheel|scroll)/i,
				fix: function () { return mouseWheelProps;}
			},
			//Special textEvents
			{ 
				reg: /^text/i,
				fix: function () { return textProps;}
			},
			//Special touch and gesture events
			{
				reg: /^touch|^gesture/i,
				fix: function () { return touchProps;}
			},
			//Special message events
			{
				reg: /^message$/i,
				fix: function () { return messageProps;}
			},
			//Special popstate events
			{ 
				reg: /^popstate$/i,
				fix: function () { return stateProps;}
			},
			//Other special events
			{
				reg: /.*/,
				fix: function () { return commonProps;}
			}
		];
		
		// Var to map event types to fixed functions
		var fixedTypeMap = {};
		//Main Event function
		var Event = function (event, element, isNative) {
			//Test for arguments else return
			if (!arguments.length) return;
			//Get owner correct event
			event = event || ((element.ownerDocument || element.document || element).parentWindow || win).event;
			//Get original event
			this.originalEvent = event;
			//Duplicate isNave val
			this.isNative = isNative;
			//If there is no event return nothing
			if (!event) return;
			//Set type of event
			var type   = event.type;
			//Get correct souce
			var target = event.target || event.srcElement;

			//Temp vars
			var i;
			var l;
			var p;
			var properties;
			var fixed;
			
			//Test if target and nodetype is TEXT_NODE. switch parent or current target
			this.target = target && target.nodeType === 3 ? target.parentNode : target;
			
			//Test for native event
			if (isNative) {
				//Fix event type 
				fixed = fixedTypeMap[type];
				//Test if event was fixed, if not type is array
				if (!fixed) {
					//Loop type array and fix
					for (i = 0, l = typeFixers.length; i < l; i++) {
						if (typeFixers[i].reg.test(type)) {
							fixedTypeMap[type] = fixed = typeFixers[i].fix;
							break;
						}
					}
				}
				
				properties = fixed(event, this, type);
				
				for (i = properties.length; i--;) {
					if (!((p = properties[i]) in this) && p in event) this[p] = event[p];
				}
				
			}
		};
		//Event methods
		//Create cross browser preventDefault method
		Event.prototype.preventDefault = function () {
			if (this.originalEvent.preventDefault) this.originalEvent.preventDefault();
			else this.originalEvent.returnValue = false;
		};
		//Create cross browser stopPropagation method
		Event.prototype.stopPropagation = function () {
			if (this.originalEvent.stopPropagation) this.originalEvent.stopPropagation();
			else this.originalEvent.cancelBubble = true;
		};
		//Create cross browser stop method
		Event.prototype.stop = function () {
			this.preventDefault();
			this.stopPropagation();
			this.stopped = true;
		};
		//Create cross browser stopImmediatePropagation method
		Event.prototype.stopImmediatePropagation = function () {
			if (this.originalEvent.stopImmediatePropagation) this.originalEvent.stopImmediatePropagation();
			this.isImmediatePropagationStopped = function () { return true; };
		};
		//Create cross browser isImmediatePropagationStopped method
		Event.prototype.isImmediatePropagationStopped = function () {
			return this.originalEvent.isImmediatePropagationStopped && this.originalEvent.isImmediatePropagationStopped();
		};
		//Create cross browser clone method
		Event.prototype.clone = function (currentTarget) {
			var newEvent = new Event(this, this.element, this.isNative);
			newEvent.currentTarget = currentTarget;
			return newEvent;
		};
		//Return new event object
		return Event;
			
	}());
	
	//Get target element
	var targetElement = function (element, isNative) {
		return !model && !isNative && (element === doc || element === win) ? root : element;
	};
	
	//Internal registry for event listeners, there is no registry for the entite instance
	var registryEntry = (function () {
		//Wrap handler to handle delegation and custom events
		var wrappedHandler = function (element, fn, condition, args) {
			//Bind call function to handler
			var call = function (event, eargs) {
				return fn.apply(element, args ? slice.call(eargs, event ? 0 : 1).concat(args) : eargs);
			};
			//Bind findTarget function to handler
			var findTarget = function (event, eventElement) {
				return fn.__delegateDEL ? fn.__delegateDEL.ft(event.target, element) : eventElement;
			};
			//Test for function that applies to handler
			var handler = condition
				? function (event) {
					var target = findTarget(event, this);
					if (condition.apply(target, arguments)) {
						if (event) event.currentTarget = target;
						return call(event, arguments);
					}
				}
				: function (event) {
					if (fn.__delegateDEL) event = event.clone(findTarget(event));
					
					return call(event, arguments);
				};
				
			//Add  __delegateDEL to handler
			handler.__delegateDEL = fn.__delegateDEL;
			
			return handler;
		},
		registryEntry = function (element, type, handler, original, namespaces, args, root) {
			//Test for custom type of event
			var customType = handleCustomEvents[type];
			//Declare isNative var
			var isNative;
			
			//Test for unload event then clean
			if (type == 'unload') {
				handler = once(removeListener, element, type, handler, original);
			}

			//Hanle custom event
			if (customType) {
				if (customType.condition) {
					handler = wrappedHandler(element, handler, customType.condition, args);
				}
				type = customType.base || type;
			}

			this.isNative = isNative = convertNativeEvents[type] && !!element[eventSupport];
			this.customType = !model && !isNative && type;
			this.element = element;
			this.type = type;
			this.original = original;
			this.namespaces = namespaces;
			this.eventType = model || isNative ? type : 'propertychange';
			this.target = targetElement(element, isNative);
			this[eventSupport] = !!this.target[eventSupport];
			this.root = root;
			this.handler = wrappedHandler(element, handler, null, args);
		};

		//Test if namespaces are in the registry
		registryEntry.prototype.inNamespaces = function (namespaces) {
			var i, j, c = 0;
			if (!namespaces) return true;
			if (!this.namespaces) return false;
			for (i = namespaces.length; i--;) {
				for (j = this.namespaces.length; j--;) {
					if (namespaces[i] == this.namespaces[j]) c++;
				}
			}
			return namespaces.length === c;
		};

		//Math element by original handler
		registryEntry.prototype.matches = function (element, original, handler) {
			return this.element === element &&	(!original || this.original === original) && (!handler || this.handler === handler);
		};

		return registryEntry;
		
	}());
	
	var registry = (function () {
		//Map stores arrays by event types
		var map = {};
		//Search registry for listeners
		var forAll = function (element, type, original, handler, root, fn) {
			var prefix = root ? 'x' : '@';
			if (!type || type == '*') {
				//Search the entire registry
				for (var t in map) {
					if (t.charAt(0) == prefix) {
						forAll(element, t.substr(1), original, handler, root, fn);
					}
				}
			} else {
				var i = 0, l, list = map[prefix + type], all = element == '*';
				if (!list) return;
				for (l = list.length; i < l; i++) {
					if ((all || list[i].matches(element, original, handler)) && !fn(list[i], list, i, type)) return;
				}
			}
		};
		
		//Test if element matches element with original
		var has = function (element, type, original, root) {
			var i, list = map[(root ? 'x' : '@') + type];
			if (list) {
				for (i = list.length; i--;) {
					if (!list[i].root && list[i].matches(element, original, null)) return true;
				}
			}
			return false;
		};
		//Get rigistry enties
		var get = function (element, type, original, root) {
			var entries = [];
			forAll(element, type, original, null, root, function (entry) {
				return entries.push(entry);
			});
			return entries;
		};
		//Put in registry
		var put = function (entry) {
			var has = !entry.root && !this.has(entry.element, entry.type, null, false)
			, key = (entry.root ? 'x' : '@') + entry.type
			;(map[key] || (map[key] = [])).push(entry);
			return has;
		};
		//Delete entry
		var del = function (entry) {
			forAll(entry.element, entry.type, null, entry.handler, entry.root, function (entry, list, i) {
				list.splice(i, 1);
				entry.removed = true;
				if (list.length === 0) delete map[(entry.root ? 'x' : '@') + entry.type];
				return false;
			});
		};
		//Remove all entries, used for onunload
		var entries = function () {
			var t, entries = [];
			for (t in map) {
				if (t.charAt(0) == '@') entries = entries.concat(map[t]);
			}
			return entries;
		};

		return { has: has, get: get, put: put, del: del, entries: entries };
		
	}());
	 
	var Selector;
	//Set selector otherwise browser not compatible
	var setSelector = function (e) {
		if (!arguments.length) {
			Selector = doc.querySelectorAll
			? function (s, r) {
				return r.querySelectorAll(s);
			}
			: function () {
				console.log('Delegate: Couldnt select element');
			};
		} else {
			Selector = e;
		}
	};
	
	//Listener to each DOM event
	var rootListener = function (event, type) {
		if (!model && type && event && event.propertyName != '_on' + type) return;

		var listeners = registry.get(this, type || event.type, null, false);
		var l = listeners.length;
		var i = 0;

		event = new Event(event, this, true);

		if (type) event.type = type;
		
		//Iterate through handlers that have been registered and call
		for (; i < l && !event.isImmediatePropagationStopped(); i++) {
			if (!listeners[i].removed){
				listeners[i].handler.call(this, event);
			}
		}
	};
	
	//Add and remove listeners to elements
	var listener = model
	? function (element, type, add) {
		//Modern browsers
		element[add ? addEvent : removeEvent](type, rootListener, false);
	}
	: function (element, type, add, custom) {
		//IE8
		var entry;
		//Add listener
		if (add) {
		
			registry.put(entry = new registryEntry(
				element,
				custom || type,
				function (event) {
					rootListener.call(element, event, custom);
				},
				rootListener,
				null,
				null,
				true
			));
			
			if (custom && element['_on' + custom] == null) element['_on' + custom] = 0;
			entry.target.attachEvent('on' + entry.eventType, entry.handler);
			
		}
		//Remove listener
		else {
		
			entry = registry.get(element, custom || type, rootListener, true)[0];
			
			if (entry) {
			
				entry.target.detachEvent('on' + entry.eventType, entry.handler);
				registry.del(entry);
				
			}
		}
	};
	
	//Add move to handler
	var once = function (remove, element, type, fn, originalFn) {
		return function () {
			fn.apply(this, arguments);
			remove(element, type, originalFn);
		};
	};
	
	//Remove listener funvtion
	var removeListener = function (element, orgType, handler, namespaces, stringComp) {
		//Get type of listener
		var type = orgType && orgType.replace(nameExpression, '');
		//Get matching handler registry entry
		var handlers = registry.get(element, type, null, false);

		//Temp vars
		var removed = {};
		var  i;
		var l;
		//Loop bound handlers and remove
		for (i = 0, l = handlers.length; i < l; i++) {
			if(stringComp){
				if ((!handler || handlers[i].original.toString()== handler.toString()) && handlers[i].inNamespaces(namespaces)) {
					registry.del(handlers[i]);
					if (!removed[handlers[i].eventType] && handlers[i][eventSupport]) removed[handlers[i].eventType] = { t: handlers[i].eventType, c: handlers[i].type };
				}
			}else{
				if ((!handler || handlers[i].original == handler) && handlers[i].inNamespaces(namespaces)) {
					registry.del(handlers[i]);
					if (!removed[handlers[i].eventType] && handlers[i][eventSupport]) removed[handlers[i].eventType] = { t: handlers[i].eventType, c: handlers[i].type };
				}
			}

		}
		//Find the parent listener and remove
		for (i in removed) {
			if (!registry.has(element, removed[i].t, null, false)) {
				listener(element, removed[i].t, false, removed[i].c);
			}
		}
	};
	
	//Delegate events
	var delegate = function (selector, fn) {
		//Find target
		var findTarget = function (target, root) {
			var i, array = isString(selector) ? Selector(selector, root) : selector;
			for (; target && target !== root; target = target.parentNode) {
				for (i = array.length; i--;) {
				  if (array[i] === target){
					  return target;
				  }
				}
			}
		};
		var handler = function (e) {
			var match = findTarget(e.target, this);
			if (match){
				//fnb.hyperion.controller.DOMevent(match,e);
				fn.apply(match, arguments);
			}
		};
		//Private function not exposed
		handler.__delegateDEL = {
			ft : findTarget,
			selector : selector
		};
		
		return handler;
		
	};
	
	//Trigger listener events
	var triggerListener = model ? function (isNative, type, element) {
		//Modern browsers
		var evt = doc.createEvent(isNative ? 'HTMLEvents' : 'UIEvents');
		evt[isNative ? 'initEvent' : 'initUIEvent'](type, true, true, win, 1);
		element.dispatchEvent(evt);
	} : function (isNative, type, element) {
		//Old browsers
		element = targetElement(element, isNative);
		isNative ? element.fireEvent('on' + type, doc.createEventObject()) : element['_on' + type]++;
	};
	
	//DELEGATE public methods: off(), on(), add(), one(), trigger(), clone()
	//Undind event from element
	var off = function (element, typeSpec, fn, stringComp) {
		
		var isTypeStr = isString(typeSpec);
		var k;
		var type;
		var namespaces;
		var i;

		if (isTypeStr && typeSpec.indexOf(' ') > 0) {
			typeSpec = stringToArray(typeSpec);
			for (i = typeSpec.length; i--;)
			off(element, typeSpec[i], fn);
			return element;
		}

		type = isTypeStr && typeSpec.replace(nameExpression, '');
		
		if (type && handleCustomEvents[type]) type = handleCustomEvents[type].base;

		if (!typeSpec || isTypeStr) {
			if (namespaces = isTypeStr && typeSpec.replace(namespaceExpression, '')) namespaces = stringToArray(namespaces, '.');
			removeListener(element, type, fn, namespaces, stringComp);
		} else if (isFunction(typeSpec)) {
			removeListener(element, null, typeSpec, null, stringComp);
		} else {
			for (k in typeSpec) {
				if (typeSpec.hasOwnProperty(k)) off(element, k, typeSpec[k]);
			}
		}

		return element;
	};
	
	//Bind event to element
	var on = function(element, events, selector, fn) {
		
		var originalFn;
		var type;
		var types;
		var i;
		var args;
		var entry;
		var first;

		if (selector === undefined && typeof events == 'object') {
			for (type in events) {
				if (events.hasOwnProperty(type)) {
					on.call(this, element, type, events[type]);
				}
			}
			
			return;
			
		}
		
		if (!isFunction(selector)) {
			originalFn = fn;
			args = slice.call(arguments, 4);
			fn = delegate(selector, originalFn, selector);
		} else {
			args = slice.call(arguments, 3);
			fn = originalFn = selector;
		}

		types = stringToArray(events);

		if (this === oneObject) {
			fn = once(off, element, events, fn, originalFn);
		}

		for (i = types.length; i--;) {
		
			first = registry.put(entry = new registryEntry(
				element,
				types[i].replace(nameExpression, ''),
				fn,
				originalFn,
				stringToArray(types[i].replace(namespaceExpression, ''), '.'),
				args,
				false
			));
			
			if (entry[eventSupport] && first) {
				listener(element, entry.eventType, true, entry.customType);
			}
			
		}
		
		return element;
		
	};
	
	//Single instance event
	var one = function () {
		return on.apply(oneObject, arguments);
	};
	
	//Trigger event on elenemnt
	var trigger = function (element, type, args) {
		
		var types = stringToArray(type);
		var i;
		var j;
		var l;
		var names;
		var handlers;

		//Loop and trigger events
		for (i = types.length; i--;) {
			type = types[i].replace(nameExpression, '');
			
			if (names = types[i].replace(namespaceExpression, '')) names = stringToArray(names, '.');
			
			if (!names && !args && element[eventSupport]) {
				triggerListener(convertNativeEvents[type], type, element);
			} else {
				handlers = registry.get(element, type, null, false);
				args = [false].concat(args);
				for (j = 0, l = handlers.length; j < l; j++) {
					if (handlers[j].inNamespaces(names)) {
						handlers[j].handler.apply(element, args);
					}
				}
			}
		}
		
		return element;
		
	};
	
	//Clone event from element
	var clone = function (element, from, type) {
	
		var handlers = registry.get(from, type, null, false);
		var l = handlers.length;
		var i = 0;
		var args, delegateDEL;

		for (; i < l; i++) {
			if (handlers[i].original) {
				args = [ element, handlers[i].type ];
				
				if (delegateDEL = handlers[i].handler.__delegateDEL) args.push(delegateDEL.selector);
				args.push(handlers[i].original);
				on.apply(null, args);
			}
		}
		return element;
	};
	
	//IE leaks fix
	if (win.attachEvent) {
		var cleanup = function () {
			var i, entries = registry.entries();
			
			for (i in entries) {
				if (entries[i] && entries[i].type && entries[i].type !== 'unload') off(entries[i].element, entries[i].type);
			}
			win.detachEvent('onunload', cleanup);
			win.CollectGarbage && win.CollectGarbage();
		};
		win.attachEvent('onunload', cleanup);
	};
	
	//Setup selector
	setSelector();
	///-------------------------------------------///
	/// Delegate Parent function
	///-------------------------------------------///
	function delegate() {};
	///-------------------------------------------///
	/// Delegate Methods
	///-------------------------------------------///
	delegate.on = on;
	
	delegate.one = one;
	
	delegate.off = off;
	
	delegate.trigger = trigger;
	
	delegate.clone = clone;
	
	delegate.Event = Event;

	//Namespace delegate
	fnb.namespace('delegate', delegate);
})();
///-------------------------------------------///
/// developer: Donovan
///
/// horizontalScroller Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// horizontalScroller Parent function
	///-------------------------------------------///
	function horizontalScroller() {
		this.doc = fnb.hyperion.$(document).elem;
		this.startX = null;
	    this.endX = null;
	    this.startOffset = null;
	    this.scrollableParent = null;
	    this.scrollerChildren = null;
	    this.x=0;
	    this.currentIndex =0;
		this.scrollSpeed =0;
	    this.fixedStops = true;
	    this.scrollStopWidth = 0;
	    this.maxStops = 0;
	    this.moveTreshold = 0.3;
	    this.tapTreshold = 0;
	    this.tap = function(){ };
	    this.afterStop =  function(index){  };
	    this.doVertical = false;
	    this.startElement = null;
	    this.bindLive = false;
		this.enabled = false;
		this.moving = false;
		this.initialized = false;
		this.internalEvent = false;
		
	    var parent = this;

	    function isset(v){
	        return(typeof v != 'undefined');
	    }

	    function mouseX(event){
	        return (isset(event.targetTouches)) ? event.targetTouches[0].pageX : event.pageX;
	    }

	    this.bindEvents = function(){
			this.bindEvents();
	    };

	    function start(e){
	    	if(parent.enabled){
		        parent.startElement = e.target;  
		        parent.startX = parent.endX = mouseX(e);

		        var moveEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchmove' : 'mousemove';
		        var upEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchend' : 'mouseup';
		        var leaveEvent = (fnb.hyperion.controller.isMobile==true) ? null : 'mouseleave';

	        	fnb.hyperion.delegate.on(parent.doc, moveEvent,move);
	        	fnb.hyperion.delegate.on(parent.doc, upEvent,end);
	        	
	        	if(leaveEvent) fnb.hyperion.delegate.on(parent.doc, leaveEvent,cancel);
	    	}
	    };

	    function move(e){
			if(parent.enabled){
				if(Math.abs(mouseX(e)-parent.startX)>5){
					parent.moving = true;
		        	//Prevent default event
					event.preventDefault();
					if(parent.startX !== null && parent.scrollableParent.elem !== null){
						parent.endX = mouseX(e);
						var val = parent.x+(parent.endX-parent.startX);
						parent.scrollableParent.elem.style.left = val+"px";
					}
				}
				
			}
	    };

	    function end(e){

	    	if(parent.fixedStops && parent.startX !== null && parent.endX !== null && parent.scrollableParent.elem !== null){
	            parent.moveToClosest();
	            parent.startElement = e.target;  
	            var moveX = Math.abs(parent.startX - parent.endX);
	            if (moveX <= parent.tapTreshold * parent.scrollStopWidth) {
	                if( parent.startElement != null ) { parent.tap( parent.startElement ); }
	            }
	        };

	        parent.startX = parent.endX = parent.startElement = null;
	        
	        var moveEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchmove' : 'mousemove';
	        var upEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchend' : 'mouseup';
	        var leaveEvent = (fnb.hyperion.controller.isMobile==true) ? null : 'mouseleave';
	        
        	fnb.hyperion.delegate.off(parent.doc, moveEvent,move);
        	fnb.hyperion.delegate.off(parent.doc, upEvent,end);
        	
        	if(leaveEvent) fnb.hyperion.delegate.off(parent.doc, leaveEvent,cancel);
        	
	    };

	    function cancel(e){
        	//Prevent default event
        	e.preventDefault();
	        if(parent.fixedStops && parent.startX !== null && parent.endX !== null && parent.scrollableParent.elem !== null){
	            parent.moveToClosest();
	        }
	        parent.startX = parent.endX = parent.startElement = null;
	    };

	    this.moveToClosest = function (){
			var _this = this;
			
	        var moveX = this.startX-this.endX,
	            currI = Math.round((-1*this.x) / this.scrollStopWidth),
	            newI = currI,
	            newloc = this.scrollStopWidth*(currI);

	        if(moveX > this.moveTreshold*this.scrollStopWidth && currI+1 <= (this.maxStops)){
	            newI = currI+1;
	        }

	        if(((-1)*moveX) > this.moveTreshold*this.scrollStopWidth && currI-1 >= 0){
	             newI = currI-1;
	        }

	        newloc = Math.round(this.scrollStopWidth*(newI));
	        this.currentIndex = newI;
	        this.x = -1*(newloc);

	        fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).stop();
	        fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).move({x:(-1*newloc)}, this.scrollSpeed, function() {setTimeout(function() {if(_this.afterStop) _this.afterStop(_this.currentIndex);parent.moving = false;}, 200);});
	    };

	    this.moveTo = function(index, nostop){

			if(this.initialized == false||this.internalEvent == true){
				var newloc = this.scrollStopWidth*(index);
				var _this = this;
				
				fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).stop();
				fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).move({x:(-1*newloc)}, this.scrollSpeed, function() {setTimeout(function() {if(_this.afterStop) _this.afterStop(index);parent.moving = false;}, 200);});
	        	
				this.currentIndex = index;
				this.x = -1*(newloc);

				this.internalEvent = false;
				this.initialized = true;
			}
	    };

	    this.next = function(){
	        if(this.currentIndex+1 <= this.maxStops){
				this.internalEvent = true;
				this.moving = true;
	            this.moveTo(this.currentIndex+1);
	        }
	    };

	    this.previous = function(){
	        if(this.currentIndex-1 >= 0){
				this.internalEvent = true;
				this.moving = true;
	            this.moveTo(this.currentIndex-1);
	        }
	    };

	    this.centerIndex = function (index){
			if (this.scrollableParent.elem !== null){
				if(isset(index)){
					this.currentIndex = index;
				} else {
					index = this.currentIndex;
				}
				var loc = -1*((index)*this.scrollStopWidth);

				this.scrollableParent.css('left',loc+'px');
				fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).stop();
				this.x = loc;
			}
	    };
	    
	    this.end = end;
	    
	    this.bindEvents = function(){
	        if(this.scrollerChildren !== null){
	        	var startEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchstart' : 'mousedown';
	        	
	        	fnb.hyperion.delegate.on(this.scrollableParent.elem, startEvent, this.scrollerChildren, start);
	        }
	    };
	};
	
	//Namespace horizontalScroller
	fnb.namespace('horizontalScroller', horizontalScroller);
})();


///-------------------------------------------///
/// developer: Donovan
///
/// fnb.hyperion.horizontalScroller.slide Object
///-------------------------------------------///
/*USAGE:
//Move element x and y
	fnb.hyperion.horizontalScroller.slide(document.getElementById("some-div")).move({x:(-1*newloc)}, this.scrollSpeed, function() {setTimeout(function() {if(_this.afterStop) _this.afterStop(index);parent.moving = false;}, 200);});
*/
(function() {
	var animateId = 0,
	animateInstances = {};
	//Get element absolute position
	function getPosition(element) {
		
		var	position = {x:element.offsetLeft, y:element.offsetTop};

		return position;
	}
	//Set interval function
	function createInterval(element, interval, speed, start, position, style, tmp, callback) {
		clearInterval(element.fx[interval]);
		if(start[style[0]]!=position[style[0]]){
			element.fx[interval] = setInterval(function(){
				start[style[0]] += (position[style[0]] - start[style[0]]) * speed;
				start[style[1]] += (position[style[1]] - start[style[1]]) * speed;
				fnb.hyperion.horizontalScroller.slide[tmp](element,start);
				if(start[style[0]]&&start[style[1]]){
					if(Math.round(start[style[0]]) == position[style[0]] && Math.round(start[style[1]]) == position[style[1]]){
						fnb.hyperion.horizontalScroller.slide[tmp](element,position);
						callCallback(element, interval, callback);
					};
				}else if(!start[style[0]]&&start[style[1]]){
					if(Math.round(start[style[1]]) == position[style[1]]){
						fnb.hyperion.horizontalScroller.slide[tmp](element,position);
						callCallback(element, interval, callback);
					};
				}else if(start[style[0]]&&!start[style[1]]){
					if(Math.round(start[style[0]]) == position[style[0]]){
						fnb.hyperion.horizontalScroller.slide[tmp](element,position);
						callCallback(element, interval, callback);
					};
				}
				
			}, 1);
		}
		
	}
	//Add animation data to element
	function wrap(element) {
		if(!element.fx)
			element.fx = {move:0};
		return element;
	}
	//Call with element as scope on event complete
	function callCallback(element, interval, callback) {
		clearInterval(element.fx[interval]);
		if(callback) callback.call(element);
	}
	//Get end
	function end(x, y, speed){
		return x < y ? min(x + speed, y) : max(x - speed, y);
	};
	///-------------------------------------------///
	/// Slide Parent function
	///-------------------------------------------///
	function slide(element, id) {
		if (!(this instanceof fnb.hyperion.horizontalScroller.slide)) {
          for (var key in animateInstances) {
              if (animateInstances[key].element === element) {
                  return animateInstances[key];
              }
          }
          animateId++;
          animateInstances[animateId] = new fnb.hyperion.horizontalScroller.slide(element, animateId);

          return animateInstances[animateId];
      }

      this.element = wrap(element);
      this.id = id;
	};
	///-------------------------------------------///
	/// Slide Methods
	///-------------------------------------------///
	slide.prototype = {
		//Move animation x & y
		move: function ( position, speed, callback) {
			var	start = getPosition(this.element);
			createInterval(this.element, "move", speed / 100, start, position, ["x", "y"], "position", callback);
		},
		//Stop specific animation
		stop: function () {
			var	interval = ["move"],
			index = interval.length;
			while(index--)
				clearInterval(this.element.fx[interval[index]]);
		},
		destroy: function () {
			animateInstances = {};
		}
	};
	//Public Animation position method
	slide.position = function (element,position) {
		var currentElementPosAttr = window.getComputedStyle(element,null).getPropertyValue("position");
		var	style = wrap(element).style;
		style.position = (currentElementPosAttr=='relative') ? "relative" : "absolute";
		if(position.x) style.left = position.x + "px";
		if(position.y) style.top = position.y + "px";
		
	};
	//Add a public method
	slide.wrap = wrap;
	//Namespace horizontalScroller.slide
	fnb.namespace('horizontalScroller.slide', slide);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Updated Ready Object
///-------------------------------------------///
(function() {
	//Object vars
	var isReadyList;
	var DOMContentLoaded;
	var classType = {};
	//Setup class types
	classType["[object Boolean]"] = "boolean";
	classType["[object Number]"] = "number";
	classType["[object String]"] = "string";
	classType["[object Function]"] = "function";
	classType["[object Array]"] = "array";
	classType["[object Date]"] = "date";
	classType["[object RegExp]"] = "regexp";
	classType["[object Object]"] = "object";
	
	//Ready methods
	var readyObj = {
		//Is the DOM ready flag
		isReady: false,
		//Counter ready items
		readyWait: 1,
		//Wait for ready event
		waitReady: function(hold) {
			if (hold) {
				readyObj.readyWait++;
			} else {
				readyObj.ready( true );
			}
		},
		//DOM ready function
		ready: function( wait ) {
			//Test if dom is ready
			if ((wait === true && !--readyObj.readyWait) || (wait !== true && !readyObj.isReady)) {
				//Test if body exists
				if ( !document.body ) {
					return setTimeout(readyObj.ready,1);
				}
				//Set dom ready flag
				readyObj.isReady = true;
				//Test if dom ready event fired otherwise wait
				if ( wait !== true && --readyObj.readyWait > 0 ) {
					return;
				}
				//Fire callback
				isReadyList.executeCallbacks(document,[ readyObj ]);
			}
		},
		//Bind events function
		bindEvents: function() {
			//Test if ready list already exists and return
			if (isReadyList) {
				return;
			}
			//Bind callback manager to ready list
			isReadyList = readyObj.callbackManager();
			//Test if ready already fired
			if (document.readyState === "complete") {
				//Handle ready if ready already fired
				return setTimeout( readyObj.ready, 1 );
			}
			//Bind modern browser events
			if (document.addEventListener) {
				//Bind event
				document.addEventListener( "DOMContentLoaded", DOMContentLoaded, false );
				//Backup event
				window.addEventListener( "load", readyObj.ready, false );
			//IE
			} else if (document.attachEvent) {
				//Bind event
				document.attachEvent( "onreadystatechange", DOMContentLoaded );
				//Backup event
				window.attachEvent( "onload", readyObj.ready );

				var toplevel = false;
				
				//Test when document is ready
				try {
					toplevel = window.frameElement == null;
				} catch(e) {}

				if ( document.documentElement.doScroll && toplevel ) {
					doScrollCheck();
				}
			}
		},
		//Callback handler
		callbackManager: function() {
			//Callback list
			var callbacks = [];
			//Callback fired flag
			var fired;
			//Callback busy flag
			var firing;
			//Callback cancelled flag
			var cancelled;
			// the callbackHandler itself
			var callbackHandler  = {
					//Callback done method
					done: function() {
						//Test if callback was cancelled
						if (!cancelled) {
							//Done vars
							var args = arguments;
							var i;
							var length;
							var elem;
							var type;
							var _fired;
							
							if (fired) {
								_fired = fired;
								fired = 0;
							}
							
							for ( i = 0, length = args.length; i < length; i++ ) {
							
								elem = args[ i ];
								type = readyObj.type( elem );
								
								if ( type === "array" ) {
									callbackHandler.done.apply( callbackHandler, elem );
								} else if ( type === "function" ) {
									callbacks.push( elem );
								}
							}
							
							if ( _fired) {
								callbackHandler.executeCallbacks( _fired[ 0 ], _fired[ 1 ] );
							}
							
						}
						return this;
					},
					//Execute callback with args
					executeCallbacks: function( context, args ) {
						//Test if callback was fired or is busy firing
						if (!cancelled && !fired && !firing ) {
							//Mak sure args are not null
							args = args || [];
							firing = 1;
							
							try {
								while( callbacks[ 0 ] ) {
									callbacks.shift().apply( context, args );//shifts a callback, and applies it to document
								}
							}
							
							finally {
								fired = [ context, args ];
								firing = 0;
							}
						}
						
						return this;
					},
					//Resolve callback with context and args
					resolve: function() {
						callbackHandler.executeCallbacks( this, arguments );
						return this;
					},
					//Is callback resolved?
					isResolved: function() {
						return !!( firing || fired );
					},
					//Cancel
					cancel: function() {
						cancelled = 1;
						callbacks = [];
						return this;
					}
				};

			return callbackHandler;
		},
		//Return type of object that is ready
		type: function( obj ) {
			return obj == null ?	String( obj ) : classType[ Object.prototype.toString.call(obj) ] || "object";
		}
	}
	//IE dom ready test
	function doScrollCheck() {
	
		if ( readyObj.isReady ) {
			return;
		}

		try {
			document.documentElement.doScroll("left");
		} catch(e) {
			setTimeout( doScrollCheck, 1 );
			return;
		}

		//Fire ready event
		readyObj.ready();
	}
	//Remove previous ready events
	if ( document.addEventListener ) {
	    DOMContentLoaded = function() {
	        document.removeEventListener( "DOMContentLoaded", DOMContentLoaded, false );
	        readyObj.ready();
	    };
	
	} else if ( document.attachEvent ) {
	    DOMContentLoaded = function() {
	        if ( document.readyState === "complete" ) {
	            document.detachEvent( "onreadystatechange", DOMContentLoaded );
	            readyObj.ready();
	        }
	    };
	}
	
	//Main ready function
	function ready(func) {
	    //Bind events
	    readyObj.bindEvents();
		//Get func type
	    var type = readyObj.type(func);
	
	    //Add callback
	    isReadyList.done(func);
	}
	
	//Namespace ready
	fnb.namespace('ready', ready);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// Serialize Object
///-------------------------------------------///
(function() {
	var serializedObject;
	//Main serialize method
	function serializeTarget(target) {
		//Reset serializedObject
		serializedObject = [];
		//Select all from elements
		var formElements = target.find('*input,select,button,textarea');
		//Test if elements was found
		if(formElements.length()==0){
			serializeObject(target.elem);
		}else{
			formElements.each(function(element){
				serializeObject(element.elem);
			});
		}
		return serializedObject.join("&");
    };
    //Serialize element
	function serializeObject(element) {
		switch (element.nodeName) {
		case 'INPUT':
			//Process input data and push into object
			var data = processInput(element);
			if(data) {
				serializedObject.push(data);
			}
			break;
		case 'TEXTAREA':
			//Process text area data and push into object
			serializedObject.push(processTextArea(element));
			break;
		case 'SELECT':
			//Process select data and push into object
			if(processSelect(element)) serializedObject.push(processSelect(element));
			break;
		case 'BUTTON':
			//Process button data and push into object
			if(processButton(element)) serializedObject.push(processButton(element));
			break;
		}
	};
	//Process inputs function
	function processInput(element) {
		//Declare new data string
		var data = "";
		///Switch input types
		switch (element.type) {
			case 'text':
			case 'hidden':
			case 'password':
			case 'button':
			case 'reset':
			case 'submit':
				if (element.getAttribute("data-type") == "masked") {
					var simpleDataValue = element.value.replace(new RegExp(/ /g), "");
					//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					simpleDataValue = (simpleDataValue == "")? undefined : simpleDataValue;
					//if name value equals undefined return nothing.
					if(name == undefined || simpleDataValue == undefined ){
					  data = '';
					}else{
					  data = name + "=" + encodeURIComponent(simpleDataValue);	
					}					
				}else if(element.getAttribute("data-placeholder")==element.value){
					//check if inupt has name value if not use id.If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					var value = (element.value == "")? undefined : element.value;
					//if name value equals undefined return nothing.
					if(name == undefined || value == undefined ){
					  data = '';
					}else{
					  data = name + "=" + encodeURIComponent(value);
					}
					
				}else{
					//check if inupt has name value if not use id.If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					var value = (element.value == "")? undefined : element.value;
					//if name value equals undefined return nothing.
					if(name == undefined || value == undefined ){
					  data = '';
					}else{
					  data = name + "=" + encodeURIComponent(value);
					}
				}
				return data;
			break;
			case 'checkbox':
				if (element.getAttribute("checked") == 'true'|| element.getAttribute("checked")=="checked"|| element.getAttribute("checked")=="CHECKED" || element.getAttribute("data-unCheckedValue")) {
					//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					// set values from checkbox.
					var valueAttr = element.value;
					//get the checked value form the checkbox.
					var unCheckedValue = element.getAttribute("data-unCheckedValue");
					//get the Unchecked value form the checkbox.
					var checkedValue = element.getAttribute("data-checkedValue");
					// set value if checked use check value if unchecked use unCheckedValue else use valueAttr or leave empty.
					var value = (unCheckedValue&&!element.getAttribute("checked")) ? unCheckedValue : (checkedValue&&element.getAttribute("checked")) ? checkedValue : (!checkedValue&&element.getAttribute("checked")) ? (valueAttr == "on") ? "true": '' : valueAttr;
					// only submit data if value and name is not empty.
					data = (value!=''&&name) ? name + "=" + encodeURIComponent(value) : '';

				}
				return data;
			break;
			case 'radio':
				if (element.getAttribute("checked") == 'true'|| element.getAttribute("checked")=="checked"|| element.getAttribute("checked")=="CHECKED") {
					//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					// set value if checked use check value if unchecked use unCheckedValue else use valueAttr or leave empty.
					var value = element.value;
					// only submit data if value and name is not empty.
					data = (value!=''&&name) ? name + "=" + encodeURIComponent(value) : '';
				}
				return data;
			break;
			case 'file':
			break;
		}
		//Return nothing if no data collected
		return;
	}
	//Process text areas function
	function processTextArea(element) {
		//Declare new data string
		var data = element.name + "=" + encodeURIComponent(element.value);
		//Return data
		return data;
	}
	//Process selects function
	function processSelect(element) {
		//Declare new data string
		var data = "";
		var j;
		switch (element.type) {
			case 'select-one':
				data = element.name + "=" + encodeURIComponent(element.value);
				return data;
			break;
			case 'select-multiple':
				for (j = element.options.length - 1; j >= 0; j = j - 1) {
					if (element.options[j].selected) {
						//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
						var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
						var value = (element.value == "")? undefined : element.options[j].value;
						//if name value equals undefined return nothing.
						if(name == undefined || value == undefined ){
						  data = '';
						}else{
						  data = name + "=" + encodeURIComponent(value);	
						}
						return data;
					}
				}
			break;
		}
		//Return nothing if no data collected
		return;
	}

	//Process button function
	function processButton(element) {
		//Declare new data string
		var data = "";
		switch (element.type) {
			case 'reset':
			case 'submit':
			case 'button':
					//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					var value = (element.value == "")? undefined : element.value;
					//if name value equals undefined return nothing.
					if(name == undefined || value == undefined){
					  data = '';
					}else{
					  data = name + "=" + encodeURIComponent(value);	
					}
				return data;
			break;
		}
		//Return nothing if no data collected
		return;
	}
	///-------------------------------------------///
	/// Serialize Parent function
	///-------------------------------------------///
	function serialize() {

	};
	///-------------------------------------------///
	/// Serialize Methods
	///-------------------------------------------///
	serialize.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init method
		init: function () {
    	   
		},
        //Serialize target
        get: function (target) {
        	return serializeTarget(target);
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.serialize = {};
        }
	};

	//Namespace utils.serialize
	fnb.namespace('serialize', serialize, true);

})();
///-------------------------------------------///
/// developer: Donovan
///
/// XML Builder Object
///-------------------------------------------///
(function() {
	//Clean utility
	function clean(node){
		var l = node.c.length;
		while( l-- ){
			if( typeof node.c[l] == 'object' )
				clean( node.c[l] );
		}
		node.n = node.a = node.c = null;	
	};

	//Format utility
	function format(node, indent, achar, buffer){
		var 
			xml = indent + '<' + node.n,
			nc = node.c.length,
			attr, child, i = 0;
			
		for(attr in node.a)
			xml += ' ' + attr + '="' + node.a[attr] + '"';
		
		xml += nc ? '>' : ' />';

		buffer.push(xml);
			
		if(nc){
			do{
				child = node.c[i++];
				if( typeof child == 'string' ){
					if( nc == 1 )//single text node
						return buffer.push( buffer.pop() + child + '</'+node.n+'>' );					
					else //regular text node
						buffer.push(indent+achar+child);
				}else if( typeof child == 'object' ) //element node
					format(child, indent+achar, achar, buffer);
			}while( i < nc );
			buffer.push(indent + '</'+node.n+'>');
		}
	};
	///-------------------------------------------///
	/// XML Builder Parent function
	///-------------------------------------------///
	function XMLWriter(encoding, version) {
		if(encoding)
			this.encoding = encoding;
		if(version)
			this.version = version;
	};
	///-------------------------------------------///
	/// XML Builder Methods
	///-------------------------------------------///
	XMLWriter.prototype = {
		//Default encoding
		encoding:'ISO-8859-1',
		//XML VERSION
		version:'1.0',
		//Default for,atting
		formatting: 'indented',
		//Default indent char
		indentChar:'\t',
		//Default indetation
		indentation: 1,
		//Default new line char
		newLine: '\n',
		//Var for frame to auto initialize module
		autoInit: false,
		//Do init for tracking
        init: function () {
        	
        },
        //Start a new document
        startDocument: function(standalone){
    		this.close();//cleanup
    		this.stack = [];
    		this.standalone = standalone;
    	},
    	//Get back to the root
    	endDocument: function(){
    		this.active = this.root;
    		this.stack = [ ];
    	},
    	//Set the text of the doctype
    	docType: function(dt){
    		this.doctype = dt;
    	},
    	//Start a new node with this name
    	startElement: function(name, namespace){
    		if( namespace ) name = namespace + ':' + name;
    		
    		var node = { n:name, a:{ }, c: [ ] };
    		
    		if( this.active ){
    			this.active.c.push(node);
    			this.stack.push(this.active);
    		}else
    			this.root = node;
    		this.active = node;
    	},
    	//Go up one node
    	endElement: function(){
    		this.active = this.stack.pop() || this.root;
    	},
    	//Add an attribute to the active node
    	attributeString: function(name, value){
    		if( this.active )
    			this.active.a[name] = value;
    	},
    	//Add a text node to the active node
    	string: function( text ){
    		if( this.active )
    			this.active.c.push(text);
    	},
    	//Shortcut, open an element, write the text and close
    	elementString: function(name, text, namespace){
    		this.startElement(name, namespace);
    		this.string( text );
    		this.endElement();
    	},
    	//Add a text node wrapped with CDATA
    	CDATA: function(text){
    		this.string( '<![CDATA[' + text + ']]>' );
    	},
    	//Add a text node wrapped in a comment
    	comment: function(text){
    		this.string('<!-- ' + text + ' -->');
    	},
    	//Generate the xml string, you can skip closing the last nodes
    	flush: function(){		
    		if(this.stack && this.stack[0]) this.endDocument();
    		
    		var achar = '', indent = '', num = this.indentation,
    			formatting = this.formatting.toLowerCase() == 'indented',
    			buffer = '<&#63;xml version="'+this.version+'" encoding="'+this.encoding+'"';

    			buffer = buffer.replace( '&#63;', '?' );
    			
    		if( this.standalone !== undefined )
    			buffer += ' standalone="'+!!this.standalone+'"';
    		buffer += ' ?>';
    		
    		buffer = [buffer];
    		
    		if( this.doctype && this.root )
    			buffer.push('<!DOCTYPE '+ this.root.n + ' ' + this.doctype+'>'); 
    		
    		if( formatting ){
    			while( num-- )
    				achar += this.indentChar;
    		}
    		
    		if( this.root )	format( this.root, indent, achar, buffer );
    		
    		return buffer.join( formatting ? this.newLine : '' );
    	},
    	//Cleanup
    	close: function(){
    		if( this.root )
    			clean( this.root );
    		this.active = this.root = this.stack = null;
    	},
    	getDocument: window.ActiveXObject
    		? function(){ //MSIE
    			var doc = new ActiveXObject('Microsoft.XMLDOM');
    			doc.async = false;
    			doc.loadXML(this.flush());
    			return doc;
    		}
    		: function(){// Mozilla, Firefox, Opera, etc.
    			return (new DOMParser()).parseFromString(this.flush(),'text/xml');
    	},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.XMLWriter = {};
        }
	};

	//Namespace XMLWriter
	fnb.namespace('XMLWriter', XMLWriter);
})();
