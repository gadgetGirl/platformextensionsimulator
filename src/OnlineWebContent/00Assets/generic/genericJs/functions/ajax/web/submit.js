///-------------------------------------------///
/// developer: Donovan
///
/// Web Default Submit function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Web Submit Parent function
	///-------------------------------------------///
	function submit() {
			
	};
	///-------------------------------------------///
	/// Web Submit Methods
	///-------------------------------------------///
	submit.prototype = {
		//Ajax standard Submit Method
		submitTarget : function (method, loadObj) {
			//Add sender to loadObj
			loadObj.method = method;
			//Set default async param to true
			loadObj.async = true;
			//Switch Submit methods
        	switch(method) {
				case 'submit':
					//Get from data
					loadObj.params = fnb.hyperion.controller.serialize(fnb.hyperion.$(loadObj.dataTarget));
					//Ajax post data
					fnb.hyperion.controller.ajax("loadPage.load", method , fnb.hyperion.controller.getAjaxLoadDefaults(loadObj))
				break;
				default:
				
			};
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.submit = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.submit', submit, true);
})();