///-------------------------------------------///
/// developer: Donovan
///
/// Banking Default Load into a function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Load into a page Parent function
	///-------------------------------------------///
	function loadIntoPage() {
			
	};
	///-------------------------------------------///
	/// Banking Load into a page Methods
	///-------------------------------------------///
	loadIntoPage.prototype = {
		//Ajax standard Loadinto Method
		success : function (sender,loadObj) {

			//Callback for when load has finished
			function jsLoadComplete(){
			
				//Eval js scripts
				fnb.hyperion.load.evalScripts(scripts.scripts);
				
				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);

				//Try send tracking information
				try{
					fnb.hyperion.utils.tracking.checkTrackingObject(sender,loadObj);
				}catch(e){
					console.log("Hyperion Tracking Error: "+e);
				}
				
				//Check load object initial page settings
				fnb.hyperion.controller.checkLoadObjSettings(loadObj);
				
				//Reset xhr
				fnb.hyperion.ajax.xhr = '';
			}
			
			//Get data from ajax response
			var data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;

			//Try filter content
			var selectedContent = fnb.hyperion.utils.ajax.filter.get(loadObj.urlTarget,data);
			
			//Set Page title
			if(selectedContent.pageTitle) this.setWindowTitle(selectedContent.pageTitle);
			
			//Set Page category
			if(selectedContent.pageCategory) fnb.hyperion.$("=body").attr("data-pageCategory",selectedContent.pageCategory);
			
			//Get html script tags and clean html
        	var scripts = fnb.hyperion.load.collectScripts(selectedContent.content);

        	//Get html css tags and clean html
        	var css = fnb.hyperion.load.collectCss(scripts.html);

			//Import css
			if(css.imports.length>0) fnb.hyperion.load.css(css.imports);
				
        	//Create html container
        	var htmlContainer = document.createElement('div');

			//Add ajax returned data to a temporary container
        	htmlContainer.innerHTML = css.html;

			//Create  a documentFragment.
			var fragment = document.createDocumentFragment();
			
			//Add temp container to fragment
			fragment.appendChild(htmlContainer);
			
			//Create wrapper for contents
			var fragmentTarget = fragment.childNodes[0];

			//Add styles to fragment
			fnb.hyperion.load.styles(fragmentTarget, css.styles);
			
        	//Split children
        	var fragmentTargetChildren = [].slice.call(fragmentTarget.children);
        	
        	//Clear target
			loadObj.target.html('');

			//Loop and add children
			for(var i=0; i<fragmentTargetChildren.length; i++) {
				loadObj.target.add(fragmentTargetChildren[i]);
			}
			
			//Test for scripts to load
			if(scripts.imports.length>0){
	        	fnb.hyperion.load.js(scripts.imports, jsLoadComplete);
			}else{
				jsLoadComplete();
			}
			
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.loadIntoPage = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.loadIntoPage', loadIntoPage, true);
})();