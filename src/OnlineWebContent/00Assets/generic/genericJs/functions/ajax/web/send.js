///-------------------------------------------///
/// developer: Donovan
///
/// Web Default Send function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Web Submit Send function
	///-------------------------------------------///
	function send() {
			
	};
	///-------------------------------------------///
	/// Web Send Methods
	///-------------------------------------------///
	send.prototype = {
		//Ajax standard Send Method
		submit : function (loadObj) {
			//Set default async param to true
			loadObj.async = true;
			//do not  initialize page on ready
			loadObj.initPageModules = false;
			//do not  initialize page on ready
			loadObj.initHtmlTemplates = false;
			//do not  initialize page on ready
			loadObj.initPageEvents = false;
			//do not  initialize page on ready
			loadObj.initPage = false;
			//Set loadObj data-type
			loadObj.dataType = 'html';
			//Stop previous request if not async
			if(!loadObj.async||loadObj.async==false) fnb.hyperion.utils.ajax.stop();
			//Reset ajax object
			fnb.hyperion.ajax.reset();
			//Make request
			fnb.hyperion.ajax.request(loadObj);	
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.send = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.send', send, true);
})();