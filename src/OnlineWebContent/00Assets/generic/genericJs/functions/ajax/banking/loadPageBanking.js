///-------------------------------------------///
/// developer: Donovan
///
/// Banking Default Load Page function
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Banking Load Page Parent function
	///-------------------------------------------///
	function loadPage() {
			
	};
	///-------------------------------------------///
	/// Banking Load Page Methods
	///-------------------------------------------///
	loadPage.prototype = {
		//Banking Default Load banking methods
		load : function (sender,loadObj) {
			//Set load object method
			loadObj.method = sender;
			//Get ajax load defaults
			loadObj = fnb.hyperion.controller.getAjaxLoadDefaults(loadObj);
			//Set loadObj data-type
			loadObj.dataType = 'html';
			//Stop previous request
			fnb.hyperion.ajax.stop(sender);
			//Reset ajax object
			fnb.hyperion.ajax.reset();
			//Make request
			fnb.hyperion.ajax.request(loadObj);
		},
		//Ajax standard loadPage Method
		success : function (sender,loadObj) {
			//Clear previous page scripts
			fnb.hyperion.utils.ajax.scripts.clear();
			//Clear previous page css
			fnb.hyperion.utils.ajax.css.clear();
			//Clear page object
			fnb.hyperion.controller.clearPageObjects();
			//Clear html templates
			fnb.hyperion.controller.clearHtmlTemplates(loadObj);
			//Get data
			var data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
			//Try filter content
			var selectedContent = fnb.hyperion.utils.ajax.filter.get(loadObj.urlTarget,data);
			//Get html script tags and clean html
        	var cleanScripts = fnb.hyperion.utils.ajax.scripts.getScripts(selectedContent.content);
			//Get html css tags and clean html
        	var cleanCss = fnb.hyperion.utils.ajax.css.getCss(cleanScripts.cleanHtml);
			//Set the content
			loadObj.target.html(cleanCss.cleanHtml);
			//Set scripts
			if(cleanScripts.scripts.length>0||cleanCss.css.length>0){
				if(cleanScripts.scripts.length>0&&cleanCss.css.length===0){
					//Set scripts and wait for them to be ready
					fnb.hyperion.utils.ajax.scripts.setScripts(cleanScripts.scripts,function() {
						//Notify Controller to raise Ready event
						fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
						//Check load object initial page settings
						fnb.hyperion.controller.checkLoadObjSettings(loadObj);
					});
				}else if(cleanScripts.scripts.length>0&&cleanCss.css.length>0){
					function cssCallBack(){
						fnb.hyperion.utils.ajax.css.setCss(cleanCss.css,function() {
							//Notify Controller to raise Ready event
							fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
							//Check load object initial page settings
							fnb.hyperion.controller.checkLoadObjSettings(loadObj);
						});
					}
					//Set scripts and wait for them to be ready
					fnb.hyperion.utils.ajax.scripts.setScripts(cleanScripts.scripts,cssCallBack);
				}else if(cleanScripts.scripts.length===0&&cleanCss.css.length>0){
					fnb.hyperion.utils.ajax.css.setCss(cleanCss.css,function() {
						//Notify Controller to raise Ready event
						fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
						//Check load object initial page settings
						fnb.hyperion.controller.checkLoadObjSettings(loadObj);
					});
				};
			}else{
				//Notify Controller to raise Ready event
				fnb.hyperion.controller.raiseEvent(loadObj.method+'Ready', loadObj);
				//Check load object initial page settings
				fnb.hyperion.controller.checkLoadObjSettings(loadObj);
			}
			//Reset xhr
			fnb.hyperion.ajax.xhr = '';
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.ajax.loadPage = {};
        }
	};
	//Namespace ajax functions
	fnb.namespace('functions.ajax.loadPage', loadPage, true);
})();