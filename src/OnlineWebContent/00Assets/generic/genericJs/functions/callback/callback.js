///-------------------------------------------///
/// developer: Donovan
///
/// Callback Object
///-------------------------------------------///
(function() {
    ///-------------------------------------------///
	/// Callback Parent function
	///-------------------------------------------///
	function callback() {

	};
	///-------------------------------------------///
	/// Callback Methods
	///-------------------------------------------///
	callback.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init FNB Callback
    	init: function () {
    		
        },
        //Run callback
        call: function (callback, data) {
        	//Run Callback function if exist
			if(callback) {
				//Test to see if callback needs to be wrapped in a function
				if(typeof callback == 'string'){
					//Test if callback not an empty string
					if(callback!=''){
						//Wrap callback
						var func = new Function(callback);
						//Execute
						func(data);
					};
				}else{
					//Execute
					callback(data);
				};
			};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.callback = {};
        }
	};

	//Namespace callback
	fnb.namespace('functions.callback', callback, true);

})();