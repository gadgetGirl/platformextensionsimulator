///-------------------------------------------///
/// developer: Donovan
///
/// ScrollTo Object
///-------------------------------------------///
(function() {
    ///-------------------------------------------///
	/// ScrollTo Parent function
	///-------------------------------------------///
	function scrollTo() {

	};
	///-------------------------------------------///
	/// ScrollTo Methods
	///-------------------------------------------///
	scrollTo.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Var for scroll position
		scrollPosition: false,
		//Init FNB ScrollTo
    	init: function () {
    		
        },
        //Get target scroll position
        getPosition: function (target) {
        	this.scrollPosition = target.scrollTop;
        },
        //Set target scroll position
        setPosition: function (target) {
        	target.scrollTop = this.scrollPosition;
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.scrollTo = {};
        }
	};

	//Namespace scrollTo
	fnb.namespace('functions.scrollTo', scrollTo, true);

})();