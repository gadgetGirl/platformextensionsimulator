///-------------------------------------------///
/// developer: Donovan
///
/// Main Functions Object
///-------------------------------------------///
(function() {
    //Init all function modules
	function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.functions) {
    		if(fnb.hyperion.functions[module].autoInit){
				fnb.hyperion.functions[module].init();
			}
    	}
    };
    ///-------------------------------------------///
	/// Functions Parent function
	///-------------------------------------------///
	function functions() {

	};
	///-------------------------------------------///
	/// Functions Methods
	///-------------------------------------------///
	functions.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Functions
    	init: function () {
    		console.log('Functions init');
    		initModules();
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions = {};
        }
	};

	//Namespace functions
	fnb.namespace('functions', functions, true);

})();