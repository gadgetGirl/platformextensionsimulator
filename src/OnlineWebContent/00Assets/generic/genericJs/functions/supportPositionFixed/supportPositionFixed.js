///-------------------------------------------///
/// developer: Donovan
///
/// supportPositionFixed Object
///-------------------------------------------///
(function() {
    //Test if fixed not supported
	function test() {
    	
		var w   = window,
	        ua  = navigator.userAgent,
	        isSupportFixed  = true;
 	    // Black list the following User Agents
 	    if (
 	        // IE less than 7.0
 	        (/MSIE (\d+\.\d+);/i.test(ua) && RegExp.$1 < 7)          ||
 	        // iOS less than 5
 	        (/OS [2-4]_\d(_\d)? like Mac OS X/i.test(ua))            ||
 	        // Android less than 3
 	        (/Android ([0-9]+)/i.test(ua) && RegExp.$1 < 3)          ||
 	        // Windows Phone less than 8
 	        (/Windows Phone OS ([0-9])+/i.test(ua) && RegExp.$1 < 8) ||
 	        // Opera Mini
 	        (w.operamini && ({}).toString.call( w.operamini ) === "[object OperaMini]") ||
 	        // Kindle Fire
 	        (/Kindle Fire/i.test(ua) || /Silk\//i.test(ua)) ||
 	        // Nokia Symbian, Opera Mobile, wOS
 	        (/Symbian/i.test(ua)) || (/Opera Mobi/i.test(ua)) || (/wOSBrowser/i.test(ua)) ||
 	        // Firefox Mobile less than 6
 	        (/Fennec\/([0-9]+)/i.test(ua) && RegExp.$1 < 6)
 	        // Optionally add additional browsers/devices here . . .
	 	    ){
 	        isSupportFixed = false;
 	        }
 	    return isSupportFixed;
    };
    //Set fixed attribute if fixed not supported
    function setFixedAttribute() {
    	//Set attribute to html
    	fnb.hyperion.$('=html').first().attr('data-fixed','false');
    };
    ///-------------------------------------------///
	/// supportPositionFixed Parent function
	///-------------------------------------------///
	function supportPositionFixed() {

	};
	///-------------------------------------------///
	/// supportPositionFixed Methods
	///-------------------------------------------///
	supportPositionFixed.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Is position fixed supported
		isSupportFixed: true,
		//Init FNB supportPositionFixed init
    	init: function () {
    		//Check if device supports position fixed
    		var isSupported = test();
    		//Set data attribute if fixed not supported
    		if(!isSupported) setFixedAttribute();
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.functions.supportPositionFixed = {};
        }
	};

	//Namespace supportPositionFixed
	fnb.namespace('functions.supportPositionFixed', supportPositionFixed, true);

})();