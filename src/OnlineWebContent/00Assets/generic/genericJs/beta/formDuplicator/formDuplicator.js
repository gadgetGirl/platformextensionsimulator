// Developer : Nardo
(function(){
	function formDuplicator(){
		console.log('Running formDuplicator');		
	};
	
	function bindEvents() {
		//List of events for this module
    	var events = [
    	              {type: 'frame', 
    	            	  listener: document, 
    	            	  events:'click', 
    	            	  selector: '.formDuplicatorAddDupliator', 
    	            	  handler: ' fnb.hyperion.forms.formDuplicator.runDuplicator(event);'
    	               },    	              
    	               {
    	            	  type: 'frame', 
     	            	  listener: document, 
     	            	  events:'click', 
     	            	  selector: '.formDuplicatorRemoveButton', 
     	            	  handler: 'fnb.hyperion.forms.formDuplicator.removeWithfragment(event);'
    	               },
    	               {type:'frame',
    	            	   listener:document, 
    	            	   events:'click',
    	            	   selector:'.updateModel',
    	            	   handler: 'fnb.hyperion.forms.formDuplicator.updateModel(event);' 
    	               },    	              
    	               {
    	                    type: 'frame',
    	                    listener: document,
    	                    events:'click',
    	                    selector: '[data-role="dropdownItem"]',
    	                    handler: 'fnb.hyperion.forms.formDuplicator.selectedValue(event);'    	                
    	               }

        ];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
	}

	formDuplicator.prototype = {
		//Keep track of number of duplicates made.
	    counter: 0,	    
	    maxItems : 10,	 
	    autoInit: true,
	    formDuplicate  : [],
	    formDuplicateArray : [],
	    duplicationParent  : '',
	    duplicationParentParent: '0',
	    duplicationTarget: '',
	    initialModel: {},
	    dataModel: {},
	    fragment: document.createDocumentFragment(),
		init: function () {			
			bindEvents();
			console.log('init');	    	
	    },
	    updateModel: function(event) {
	    	// Check if the container for duplication has been setup
	    	if (this.duplicationParentParent == 0)
	    		{
	    		this.duplicationParentParent = fnb.hyperion.$(event.currentTarget).parent().parent();
	    		}	 
	    	
	    	var modelKey = this.duplicationParentParent.children(0).attr('data-model');
	    	this.dataModel[modelKey] = {};
	    	this.dataModel[modelKey]['entry'] = {};	    	
	    	var ghostDocument=document.createDocumentFragment();
   			ghostDocument.appendChild(this.duplicationParentParent.elem.cloneNode(true));
   			   			
   			//INPUT
   			var list = ghostDocument.querySelectorAll('input');   			
   			for (key in list)
			{					
			if (list.hasOwnProperty(key) && key != 'length')
				{					
					this.dataModel[modelKey]['entry'][list[key].id] = list[key].value;	  
				}
			}

   			//SELECT   		
   			list =  ghostDocument.querySelectorAll('select');   			
   			for (key in list)
			{					
			if (list.hasOwnProperty(key) && key != 'length')
				{
					this.dataModel[modelKey]['entry'][list[key].id] = list[key].value;
					var dataVal = fnb.hyperion.$('#'+list[key].id).attr('data-value');				
					if (dataVal == null) {						
						fnb.hyperion.forms.dropDown.getValue(fnb.hyperion.$('#'+list[key].id),function(value){
							dataVal = value;
						});						
					}					
					this.dataModel[modelKey]['entry'][list[key].id] = dataVal;	  					
				}
			}
   			
   			//TEXTAREA   			
   			list =  ghostDocument.querySelectorAll('textarea');   			
   			for (key in list)
			{					
			if (list.hasOwnProperty(key) && key != 'length')
				{		
				    this.dataModel[modelKey]['entry'][list[key].id] = fnb.hyperion.$('#'+list[key].id).val();
				}
			}			
   			
	   	    console.log(this.dataModel);
	    },
	    setItemsMax: function(max) {
	    	this.maxItems = max;	    	
	    },
	    getMax : function()
	    {
	    	return this.maxItems;
	    },
	    runDuplicator: function (event) {
	    	// Create backbone starting point for duplication
	    	this.duplicationParentParent = fnb.hyperion.$(event.currentTarget).parent().parent();
	    	// Get div container to duplicate #
	   	    this.duplicationTarget = this.duplicationParentParent.children(0);           	   	   
	   	    var modelKey = this.duplicationTarget.attr('data-model');
	   	    var maxItems = this.duplicationTarget.attr('data-maxitems');
      	   	//Build Model base structure	   	    	   	    
	   	    console.log(modelKey);
	    	if(!this.dataModel[modelKey])
	    		{	    		
	    		    this.dataModel[modelKey] = this.generateModel(this.duplicationTarget);	   	 	   	 	
	    		    this.dataModel[modelKey] = {maxItems:maxItems};		   	
			   	    this.dataModel[modelKey] = {itemsCount:0};
			   	    this.dataModel[modelKey] = {duplicateName:0};
			   	    //this.dataModel[modelKey]['entries'] = [];
			   	    this.dataModel[modelKey]['entry'] = {};
			   	    console.log('initialised model');
	    		}
	    	console.log(modelKey);
	    	console.log(this.dataModel);
	    	
	    	if (!this.formDuplicate[modelKey]) { this.formDuplicate[modelKey] = ''; }
	    	
	    	//Determine the duplication tag name	    	
	    	var inputName = '';
	    	inputName = fnb.hyperion.forms.formDuplicator.determineInputName(this.duplicationTarget.children(0).html());
	    	
	    		//Duplicate the specified DOM form elements if it hasn't been done yet.
		   		if(this.formDuplicate[modelKey] == '')
	    			{
		   			//console.log(this.duplicationTarget.html().replace(/>\s+</g, "><").trim());
		   			console.log('entering the first condition');		   
		   			this.formDuplicate[modelKey] = fnb.hyperion.forms.formDuplicator.doTemplateFragment(this.duplicationTarget.children(0), inputName,modelKey);
		   			this.duplicationTarget.html("");		   			
		   			this.duplicationTarget.add(this.formDuplicate[modelKey]);		   			
		   			var secondTemplate = fnb.hyperion.forms.formDuplicator.doTemplateFragment(this.duplicationTarget.children(0), inputName,modelKey);
		   			this.duplicationTarget.add(secondTemplate);		   			
	    			}
	    		else
	    			{
	    			console.log('entering the second condition');	  
	    			var newTemplate = fnb.hyperion.forms.formDuplicator.doTemplateFragment(this.duplicationTarget.children(0), inputName,modelKey);
		   			this.duplicationTarget.add(newTemplate);
	    			}   		
		   		
	    },
	    selectedValue: function(event) {   
	    	// Check if the container for duplication has been setup
	    	if (this.duplicationParentParent == 0)
	    		{
	    		this.duplicationParentParent = fnb.hyperion.$(event.currentTarget).parent().parent();
	    		}	 
	    	var modelKey = this.duplicationParentParent.children(0).attr('data-model');
	   	    
	    	var dropdownSelectedElement = fnb.hyperion.$(event.currentTarget).parent().parent().parent();
	    	var currentID = dropdownSelectedElement.children(0).children(0).attr('id');
	    	
	    	var selectElement = dropdownSelectedElement.children(0).find('=select');
	    	
	    	var tmp = '';
	    	fnb.hyperion.forms.dropDown.getValue(selectElement,function(value) {                
                tmp = value;                
            });
	    },
	    doTemplateFragment: function(obj, inputName, modelKey) {
	    	console.log('Do Template Fragment');
	    	var size = 0;	    	
	    	var ghostDocument=document.createDocumentFragment();
   			ghostDocument.appendChild(obj.elem.cloneNode(true));   			
   			var list =  ghostDocument.querySelectorAll('input, select, textarea');  
   			
   			this.fragment.appendChild(ghostDocument.cloneNode(true));
      		
			for (key in list)
				{					
				if (list.hasOwnProperty(key))
					{
					   if (key !== 'length') {					       
					       size = Object.keys(this.dataModel[modelKey]['entry']).length;
					       list[key].name = inputName+	size;
					       list[key].id = inputName+size;
					       this.dataModel[modelKey]['entry'][inputName+size] = list[key].value;
					    }
					}
				}
   			return ghostDocument;
	    },
	    //Rename name and id field to be numerically consistent when duplicating fields for back-end
	    determineInputName: function(evaluateString){	    		    	
	    	var tmp = evaluateString.match(/name="([^\'\"]+)/);
	    	tmp = tmp[0].substr(6,tmp[0].length);	    		    	
	    	var i = tmp.length-1;	
	    	for (i; i > 0 ; i--)	    	
	    		{	 		
	    		if (!isNaN(tmp.substr(i,i+1)))
	    			{	    			   
	    			   tmp = tmp.substr(0,i);
	    			}
	    		else
	    			{	    			
	    			// Stop processing
	    			break;
	    			}
	    		}
	    	//if tag was just made-up of numbers resolve to general naming.
	    	if (!isNaN(tmp)) {tmp ='inputName';}
	    	return tmp;
	    },
	    determineInputNameFromName: function(tmp){	    	    	
	    	var i = tmp.length-1;	
	    	for (i; i > 0 ; i--)	    	
	    		{	    		 		
	    		if (!isNaN(tmp.substr(i,i+1)))
	    			{	    			   
	    			   tmp = tmp.substr(0,i);
	    			}
	    		else
	    			{	    			
	    			// Stop processing
	    			break;
	    			}
	    		}
	    	//if tag was just made-up of numbers resolve to general naming.
	    	if (!isNaN(tmp)) {tmp ='inputName';}
	    	return tmp;
	    },
	    updateSingleModelItems: function(event) {
	    	var fieldId = fnb.hyperion.$(event.currentTarget).attr('id');
	    	var previousfieldValue = fnb.hyperion.$(event.currentTarget).attr('value');
	    	var newfieldValue = fnb.hyperion.$(event.currentTarget).val();
	    	this.duplicationParentParent = fnb.hyperion.$(event.currentTarget).parent().parent().parent().parent();
	    	var modelKey = this.duplicationParentParent.attr('data-model');
	    	
	    	// Create backbone starting point for duplication	    
	    	// Get div container to duplicate #	   	    
	    	/*	    	
	    	this.dataModel[modelKey]['entries'][fieldId] = fieldValue;
	    	*/	    
	    	console.clear();	    	
	    	console.log(fieldId);	    	
	    	console.log(previousfieldValue);
	    	console.log(newfieldValue);	    	
	    	console.log(this.dataModel);
	    	this.dataModel[modelKey]['entry'][fieldId] = newfieldValue;	    	
	    	
	    },
	    updateSingleModelItems: function(event) {
	    	
	    },
	    removeWithfragment: function(event) {
	    	this.duplicationParentParent = fnb.hyperion.$(event.currentTarget).parent();	    	
	    	// Get div container to duplicate #	   	    
	    	var modelKey = this.duplicationParentParent.parent().attr('data-model');	    	
	    	//fnb.hyperion.forms.formDuplicator.decreaseCounter(this.dataModel[modelKey]);
	    	var ghostDocument=document.createDocumentFragment();
   			ghostDocument.appendChild(this.duplicationParentParent.elem.cloneNode(true)); 
         	var list = ghostDocument.querySelectorAll('input, select');   		
   			
	    	var templateName = '';
   			for (key in list)
			{					
			if (list.hasOwnProperty(key))
				{				
				   console.log('key');				  
				   if (key !== 'length') {
					   var keyTrim = list[key].id;
					   templateName =  list[key].id;
					   console.log(keyTrim);
                       delete this.dataModel[modelKey]['entry'][list[key].id];				     
				    }
				}
			}
   			this.duplicationParentParent.html("");
   			
   			var ghostDocumentParent=document.createDocumentFragment();
   			ghostDocumentParent.appendChild(this.duplicationParentParent.parent().parent().elem.cloneNode(true)); 
   			var listParent = ghostDocumentParent.querySelectorAll('input, select');   		
	    	console.log('list Parent 1');	
   			console.log(listParent);
   			//this.duplicationParentParent.remove();   			
   			/*
   			 * Rename all tags in dom
   			 */
   			var inputName = fnb.hyperion.forms.formDuplicator.determineInputNameFromName(templateName);
   			console.log('list 2');	
   			console.log(list);
   			var counter = 0;
   			for (key in listParent)
			{					
			if (listParent.hasOwnProperty(key))
				{				
				   //console.log('key');				  
				   if (key !== 'length') {
					   //size = Object.keys(this.dataModel[modelKey]['entry']).length;
					   fnb.hyperion.$('#'+listParent[key].id).attr('name',inputName+counter);
				       fnb.hyperion.$('#'+listParent[key].id).attr('id',inputName+counter);
					   listParent[key].name = inputName+counter;
					   listParent[key].id = inputName+counter;
				       console.log('-----------');
				       console.log('#'+listParent[key].id);
				       //this.dataModel[modelKey]['entry'][inputName+counter] = list[key].value;		     
				    }
				   counter += 1;
				}
			}   			
   			console.log(this.dataModel); 
	    	
	    },
	    generateModel: function (target) {	      	
	    	//model.parent = target.parent().parent();	    	
	    	return model = {};
	    },  	   
	    increaseCounter: function(obj){
	    	obj.itemsCount += 1;
	    },
	    decreaseCounter: function(obj){
	    	    obj.itemsCount -= 1;	    	
	    },
	    //Remove current object from dom
	    destroy: function () {
	    	fnb.hyperion.forms.formDuplicator = {};
	    }
	};
		
	Object.size = function(obj) {
	    var size = 0;
	    var key = 0;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) size++;
	    }
	    return size;
	};
	
	
	// Namespace Form Duplicator Object
	fnb.namespace('forms.formDuplicator', formDuplicator, true);
})();