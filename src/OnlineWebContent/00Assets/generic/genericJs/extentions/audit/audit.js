///-------------------------------------------///
/// developer: Donovan
///
/// Audit Object
///-------------------------------------------///
(function() {
	//String audits object
	var audits = [
	              {string: 'getElementById', error: 'Please do not use javascript getElementById. Use e.g. fnb.hyperion.$("#example")'},
	              {string: 'getElementsByClassName', error: 'Please do not use javascript getElementsByClassName. Use e.g. fnb.hyperion.$(".example")'},
	              {string: 'getElementsByName', error: 'Please do not use javascript getElementsByName. Use e.g. fnb.hyperion.$("@example")'},
	              {string: 'getElementsByTagName', error: 'Please do not use javascript getElementsByTagName. Use e.g. fnb.hyperion.$("=example")'},
	              {string: 'querySelector', error: 'Please do not use javascript querySelector. Use e.g. fnb.hyperion.$("!example")'},
	              {string: 'querySelectorAll', error: 'Please do not use javascript querySelectorAll. Use e.g. fnb.hyperion.$("*example")'},
	              {string: 'setAttribute', error: 'Please do not use javascript setAttribute. Use e.g. fnb.hyperion.$("#example").attr("attr", "attrValue")'},
	              {string: 'getAttribute', error: 'Please do not use javascript getAttribute. Use e.g. fnb.hyperion.$("#example").attr("attr")'},
	              {string: 'parentNode', error: 'Please do not use javascript parentNode. Use e.g. fnb.hyperion.$("#example").parent()'},
	              {string: '.style', error: 'Please do not use javascript .style. Use e.g. fnb.hyperion.$(".example").css("width","10px")'},
	              {string: '.classList', error: 'Please do not use javascript .classList. Use e.g. fnb.hyperion.$(".example").addClass("example")'},
	              {string: '.display ', error: 'Please do not use javascript .display . Use e.g. fnb.hyperion.$(".example").show()'},
				 ];
	///-------------------------------------------///
	/// Audit Parent function
	///-------------------------------------------///
	function audit() {

	};
	///-------------------------------------------///
	/// Audit Methods
	///-------------------------------------------///
	audit.prototype = {
		//Audit function
        test: function (fn) {
        	//Error string
        	var errors = '';
        	//Convert function to string
        	var stringFunction = fn.toString();
    		//Loop audits
        	for (var audit in audits) {
        		//Test for non native elements
        		if(audits.hasOwnProperty(audit)){
	        		//Set current audit object
	        		var auditObject = audits[audit];
	        		//Test for audit match
	        		var find = stringFunction.indexOf(auditObject.string);
	        		//Test if audit was found
	        		if(find!=-1) errors = (errors=="") ? 'Page Object Error:\n - '+auditObject.error : errors +'\n - '+ auditObject.error;
        		}
        	};
        	//Test for errors and show
        	if(errors!='') alert(errors);
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.audit = {};
        }
	};

	//Namespace audit
	fnb.namespace('audit', audit, true);

})();