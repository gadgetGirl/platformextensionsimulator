///-------------------------------------------///
/// developer: Donovan Phillips
///
/// Debounce Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Debounce function
	///-------------------------------------------///
	function debounce(func, wait, immediate) {
		//Var for debounce timeout
		var timeout;
		//Return debounce function
		return function() {
			//Get function to be debounced context
			var context = this
			//Get function to be debounced arguments
			var args = arguments;
			//Clear timeout if exists
			clearTimeout(timeout);
			//Create new timeout for debouncing
			timeout = setTimeout(function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			}, wait);
			//Test for immediate flag and execute function
			if (immediate && !timeout) func.apply(context, args);
		};
	};

	//Namespace debounce
	fnb.namespace('debounce', debounce);
})();