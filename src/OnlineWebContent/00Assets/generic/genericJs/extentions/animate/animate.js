///-------------------------------------------///
/// developer: Donovan
///
/// Animate Object
///-------------------------------------------///
/*USAGE:
	Ease types:
		ease-in
		lin
		ease
	Simple Animate:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'), {height:"90%"}, 3, "ease-in");
	Wait then simple animate:
		fnb.hyperion.animate().wait(1).animate(fnb.hyperion.$('#item1'), {height:"90%"}, 3, "ease-in");
	Simple animate multiple properties:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'), {width:"150px", height:"170px", left: "300px", top: "20px"}, 3, "ease-in");
	Simple animate with callback:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'), {height:"90%"}, 3, "ease-in").callBack(handler);
	Simple from animation:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'),{bottom:{from:"-50px", to:'auto'},opacity:{from:"0", to:1}}, 3, "ease-in");
	Chained animation:
		fnb.hyperion.animate(fnb.hyperion.$('#item1'), {height:"90%"}, 3, "ease-in")
		.animate(fnb.hyperion.$('#item2'), {width:"150px", height:"170px", left: "300px", top: "20px"}, 0.5)
		.wait(0.5)
		.animate(fnb.hyperion.$('#item3'), {width:{to:"60px", e:"ease-in"}, height:"80px", left: 0, top: 0}, 0.5)
		.wait(0.5)
		.callBack(complete);
		
 */
(function() {
	///-------------------------------------------///
	/// Animate Parent function
	///-------------------------------------------///
	function animate(animateObject) {
		//Main animate object
		animateObject = function (node, cssGroup, time, ease) {
			//Test for element
	        node = (node) ? (node.elem) ? node.elem : node : 0.1;
			//Animate object vars
	        var attribute, object, queue = [],
	        //Setup node
			//Animate object callback function
	        callback = function (node) {
	                if (node = queue.shift()) node[1] ? animateObject.apply(this, node).animate(callback) : 0 < node[0] ? setTimeout(callback, 1E3 * node[0]) : (node[0](), callback());
	        };
			//Select Node
	        node.charAt && (node = document.getElementById(node));
			//Test if noe exists
	        if (0 < node || !node) cssGroup = {}, time = 0, callback(queue = [[node || 0]]);
			//Expand css
	        expand(cssGroup, {
	            padding: 0,
	            margin: 0,
	            border: "Width"
	        }, [top, right, bottom, left]);
	        //Expand border radius
	        expand(cssGroup, {
	            borderRadius: "Radius"
	        }, [top + left, top + right, bottom + right, bottom + left]);
	        //Increase animateObject instatnce
			++animInstance;
			//Loop attributes
	        for (attribute in cssGroup) object = cssGroup[attribute], !object.to && 0 !== object.to && (object = cssGroup[attribute] = {
	            to: object
	        }), animateObject.definitions(object, node, attribute, ease);
			//Iterate attributes
	        animateObject.iterate(cssGroup, 1E3 * time, callback);
			//Bind functions to object and return them
	        return {
	            animate: function () {
	                queue.push([].slice.call(arguments));
	                return this;
	            },
				wait: function () {
					queue.push([].slice.call(arguments));
	                return this;
	            },
				callBack: function () {
					queue.push([].slice.call(arguments));
	                return this;
	            }
	        };
	    };
		
		//Declare dimension vars for shorthand
	    var top = "Top",
        right = "Right",
        bottom = "Bottom",
        left = "Left",
        animInstance = 1,
	
        //Split shorthand css
        expand = function (node, elem, direction, attribute, index, dimention, obj) {
            for (attribute in node)
                if (attribute in elem) {
                    obj = node[attribute];
                    for (index = 0; dimention = direction[index]; index++) node[attribute.replace(elem[attribute], "") + dimention + (elem[attribute] || "")] = {
                        to: 0 === obj.to ? obj.to : obj.to || obj,
                        fr: obj.from,
                        elem: obj.e
                    };
                    delete node[attribute];
                }
        }, 
		getRequestFrame = function (win, frame) {
            return win["webkitR" + frame] || win["mozR" + frame] || win["msR" + frame] || win["r" + frame] || win["oR" + frame];
		}(window, "equestAnimationFrame");
	
		//Animation definition object
		animateObject.definitions = function (obj, elem, node, b, g) {
	        g = elem.style;
	        obj.node = node;
	        obj.bottom = elem;
	        obj.s = node in g ? g : elem;
	        obj.e = obj.e || b;
	        obj.from = obj.from || (0 === obj.from ? 0 : obj.s == elem ? elem[node] : (window.getComputedStyle ? getComputedStyle(elem, null) : elem.currentStyle)[node]);
	        obj.u = (/\d(\D+)$/.exec(obj.to) || /\d(\D+)$/.exec(obj.from) || [0, 0])[1];
	        obj.fn = /color/i.test(node) ? animateObject.effects.color : animateObject.effects[node] || animateObject.effects.func;
	        obj.mx = "anim_" + node;
	        elem[obj.mx] = obj.mxv = animInstance;
	        elem[obj.mx] != obj.mxv && (obj.mxv = null);
	    };
	    
	    //Iterate over css properties
	    animateObject.iterate = function (node, time, callback) {
	        var iter, unique, obj, newTime, effect, calcTime = +new Date + time;
	        iter = function (top) {
	            unique = calcTime - (top || (new Date).getTime());
	            if (50 > unique) {
	                for (obj in node) obj = node[obj], obj.property = 1, obj.fn(obj, obj.bottom, obj.to, obj.from, obj.node, obj.e);
	                callback && callback();
	            } else {
	                unique /= time;
	                for (obj in node) {
	                    obj = node[obj];
	                    if (obj.bottom[obj.mx] != obj.mxv) return;
	                    effect = obj.e;
	                    newTime = unique;
	                    "lin" == effect ? newTime = 1 - newTime : "ease" == effect ? (newTime = 2 * (0.5 - newTime), newTime = 1 - (newTime * newTime * newTime - 3 * newTime + 2) / 4) : "ease-in" == effect ? (newTime = 1 - newTime, newTime *= newTime * newTime) : newTime = 1 - newTime * newTime * newTime;
	                    obj.property = newTime;
	                    obj.fn(obj, obj.bottom, obj.to, obj.from, obj.node, obj.e);
	                }
	                getRequestFrame ? getRequestFrame(iter) : setTimeout(iter, 20, 0);
	            }
	        };
	        iter();
	    };
	    
		//CSS names which need special handling
	    animateObject.effects = {
	    	       
		   func: function (obj, node, to, from, attribute) {
	            from = parseFloat(from) || 0;
	            to = parseFloat(to) || 0;
	            obj.s[attribute] = (1 <= obj.property ? to : obj.property * (to - from) + from) + obj.u;
	        },
			
	        width: function (obj, node, to, from, attribute, d) {
	            0 <= obj._fr || (obj._fr = !isNaN(from = parseFloat(from)) ? from : "width" == attribute ? node.clientWidth : node.clientHeight);
	            animateObject.effects.func(obj, node, to, obj._fr, attribute, d)
	        },
			
	        opacity: function (obj, node, to, from, attribute) {
	            if (isNaN(from = from || obj.from)) from = node.style, from.zoom = 1, from = obj._fr = (/alpha\(opacity=(\d+)\from/i.exec(from.filter) || {})[1] / 100 || 1;
	            from *= 1;
	            to = obj.property * (to - from) + from;
	            node = node.style;
	            attribute in node ? node[attribute] = to : node.filter = 1 <= to ? "" : "alpha(" + attribute + "=" + Math.round(100 * to) + ")";
	        },

	        color: function (obj, node, to, from, attribute, d, c, j) {
	            obj.ok || (to = obj.to = animateObject.toRGBA(to), from = obj.from = animateObject.toRGBA(from), 0 == to[3] && (to = [].concat(from), to[3] = 0), 0 == from[3] && (from = [].concat(to), from[3] = 0), obj.ok = 1);
	            j = [0, 0, 0, obj.property * (to[3] - from[3]) + 1 * from[3]];
	            for (c = 2; 0 <= c; c--) j[c] = Math.round(obj.property * (to[c] - from[c]) + 1 * from[c]);
	            (1 <= j[3] || animateObject.rgbaIE) && j.pop();
	            try {
	                obj.s[attribute] = (3 < j.length ? "rgba(" : "rgb(") + j.join(",") + ")";
	            } catch (k) {
	                animateObject.rgbaIE = 1;
	            }
	        }
	        
	    };
	    
	    //Add height method to effects
	    animateObject.effects.height = animateObject.effects.width;
	    //Regex for colors
	    animateObject.RGBA = /#(.)(.)(.)\b|#(..)(..)(..)\b|(\d+)%,(\d+)%,(\d+)%(?:,([\d\.]+))?|(\d+),(\d+),(\d+)(?:,([\d\.]+))?\b/;
	    //Convert colors to RGBA
	    animateObject.toRGBA = function (node, colorArray) {
	        colorArray = [0, 0, 0, 0];
	        node.replace(/\s/g, "").replace(animateObject.RGBA, function (node, b, g, d, c, animateObject, k, top, right, bottom, p, q, r, s, t) {
	            k = [b + b || c, g + g || animateObject, d + d || k];
	            b = [top, right, bottom];
	            for (node = 0; 3 > node; node++) k[node] = parseInt(k[node], 16), b[node] = Math.round(2.55 * b[node]);
	            colorArray = [k[0] || b[0] || q || 0, k[1] || b[1] || r || 0, k[2] || b[2] || s || 0, p || t || 1];
	        });
	        return colorArray;
	    };

	    return animateObject;
	    
	}
	
	//Namespace animate
	fnb.namespace('animate', animate, true);
})();