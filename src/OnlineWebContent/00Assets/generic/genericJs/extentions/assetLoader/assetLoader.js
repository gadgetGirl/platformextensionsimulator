///-------------------------------------------///
/// developer: Donovan
///
/// External content loader
///-------------------------------------------///
(function() {

	//Browser info
	var browser;
	//Requests in progress
	var pending = {};
	// Number of tests performed
	var testCount = 0;
	//Get doc
	var doc = this.document;
	//Reference to head
	var head = doc.head || doc.getElementsByTagName('head')[0];
	// Queued requests.
	var queue = {css: [], js: []};
	styleSheets = doc.styleSheets;
	
	//Simple function to create a node
	function createNode(name, attrs) {
	    var node = doc.createElement(name), attr;
	    for (attr in attrs) {
	    	if (attrs.hasOwnProperty(attr)) {
	    		node.setAttribute(attr, attrs[attr]);
	    	}
	    }
	    return node;
	}
	
	//Count recources that have been loaded
	function finish(type) {
		var pendingType = pending[type];
		var callback;
		var urls;
		
		if(pendingType) {
		  callback = pendingType.callback;
		  urls = pendingType.urls;
		
		  urls.shift();
		  testCount = 0;
		
		  //Test if loading has completed
		  if (!urls.length) {
		    callback && callback.call(pendingType.context, pendingType.obj);
		    pending[type] = null;
		    queue[type].length && load(type);
		  }
		}
	}
	
	//Get browser
	function getBrowser() {
		var ua = navigator.userAgent;
		
		browser = {
		  async: doc.createElement('script').async === true
		};
		
		(browser.webkit = /AppleWebKit\//.test(ua)) || (browser.ie = /MSIE|Trident/.test(ua)) || (browser.opera = /Opera/.test(ua)) || (browser.gecko = /Gecko\//.test(ua)) || (browser.unknown = true);
	}
	
	//Load scripts
	function load(type, urls, callback, obj, context) {

		var _finish = function () { finish(type); };
		var isCSS   = type === 'css';
		var nodes   = [];
		var i;
		var len;
		var node;
		var p;
		var pendingUrls;
		var url;
		
		browser || getBrowser();
		
		if (urls) {
		 
		  urls = typeof urls === 'string' ? [urls] : urls.concat();

		  if (isCSS || browser.async || browser.gecko || browser.opera) {
		  
			queue[type].push({
			  urls    : urls,
			  callback: callback,
			  obj     : obj,
			  context : context
			});
			
		  }else{
		  
			  for (i = 0, len = urls.length; i < len; ++i) {
				//Create queue
				queue[type].push({
					urls    : [urls[i]],
					callback: i === len - 1 ? callback : null,
					obj     : obj,
					context : context
				});
				
			  }
			
		  }
		}

		if (pending[type] || !(p = pending[type] = queue[type].shift())) {
			return;
		}

		head || (head = doc.head || doc.getElementsByTagName('head')[0]);
		pendingUrls = p.urls.concat();
		
		for (i = 0, len = pendingUrls.length; i < len; ++i) {
			url = pendingUrls[i];
		
			if (isCSS) {
				node = browser.gecko ? createNode('style') : createNode('link', {
					href: url,
					rel : 'stylesheet'
				});
			} else {
				node = createNode('script', {src: url});
				node.async = false;
			}
		
			node.className = type+'Loaded';
			node.setAttribute('charset', 'utf-8');
		
			if (browser.ie && !isCSS && 'onreadystatechange' in node && !('draggable' in node)) {
				node.onreadystatechange = function () {
					if (/loaded|complete/.test(node.readyState)) {
						node.onreadystatechange = null;
						_finish();
					}
			    };
			
			} else if (isCSS && (browser.gecko || browser.webkit)) {
		  
				if (browser.webkit) {
					p.urls[i] = node.href;
					testWebKit();
				} else {
					node.innerHTML = '@import "' + url + '";';
					testGecko(node);
			    }
				
			} else {
			  
				node.onload = node.onerror = _finish;
				
			}
		
			nodes.push(node);
		  
		}

		for (i = 0, len = nodes.length; i < len; ++i) {
			head.appendChild(nodes[i]);
		}
	}
	
	//Poll to test if loaded
	function testGecko(node) {
		var hasRules;
		try {
		  hasRules = !!node.sheet.cssRules;
		} catch (ex) {
		  testCount += 1;
		  if (testCount < 200) {
			setTimeout(function () { testGecko(node); }, 50);
		  } else {
			hasRules && finish('css');
		  }
		  return;
		}

		finish('css');
	}
	
	//Poll to test if loaded
	function testWebKit() {
	    var css = pending.css, i;
	    if (css) {
	      i = styleSheets.length;
	      while (--i >= 0) {
	        if (styleSheets[i].href === css.urls[0]) {
	          finish('css');
	          break;
	        }
	      }

	      testCount += 1;

	      if (css) {
	        if (testCount < 200) {
	          setTimeout(testWebKit, 50);
	        } else {
	          finish('css');
	        }
	      }
	    }
	}
	  
	//Collect all scripts from ajax response 
	function getScripts(html) {
		
		// Array which will store the eval scripts
		var scripts = new Array();
		
		// Array which will store the import scripts
		var imports = new Array();

		//Test for html
		if(fnb.hyperion.controller.is(html)){

			//Loop html and strip out tags
			while(html.indexOf("<script") > -1 || html.indexOf("</script") > -1) {
				
				//Find scripts starts
				var startScriptTag = html.indexOf("<script");
				
				//Find end of scripts starts
				var endScriptTag = html.indexOf(">", startScriptTag);
				
				//Find scripts closing tag
				var closedStartScriptTag = html.indexOf("</script", startScriptTag);
				
				//Find end of closing tag
				var closedEndScriptTag = html.indexOf(">", closedStartScriptTag);

				//Create string to test if import
				var importTestString = html.substring(startScriptTag, closedEndScriptTag+1);
				
				//Test if script has a src and add it to imports otherwise add it to evals
				var srcUrl = importTestString.match(/src="[\s\S]*?"/g);
					
				if(srcUrl) {
				
					//Test if script has a src and add it to imports otherwise add it to evals
					var data = (importTestString.match(/data-jsp='[\s\S]*?'/g)) ? importTestString.match(/data-jsp='[\s\S]*?'/g)[0].replace("data-jsp='","").replace("'","") : null;

					//Script has src
					var rawUrl = srcUrl[0].replace('src="','').replace('"','');
					
					//Test if data is bound to the include
					if(data){
						//Get includes data key
						var dataKey = rawUrl.split("/").pop().split('.js')[0];
						//Add includes data
						fnb.hyperion.load.includesData[dataKey] = JSON.parse(data);
					}

					//Add script id to headscripts
					imports.push(rawUrl);
					
				}else{
				
					// Add to scripts array
					scripts.push(html.substring(endScriptTag+1, closedStartScriptTag));
					
				}

				//Clean html
				html = html.substring(0, startScriptTag) + html.substring(closedEndScriptTag+1);

			}
		}
		
		//Return formatted loadObj
		return {scripts: scripts, html: html, imports: imports};
	}
	
	//Collect all links from ajax response 
	function getCss(html) {

		// Array which will store the import links
		var imports = new Array();
		
		// Array which will store the style contents
		var styles = new Array();

		//Test for html
		if(fnb.hyperion.controller.is(html)){
			//Loop html and strip out link tags
			while(html.indexOf("<link") > -1 || html.indexOf("</link") > -1) {
				
				//Find link starts
				var startLinkTag = html.indexOf("<link");
				
				//Find end of link starts
				var endLinkTag = html.indexOf(">", startLinkTag);

				//Create string to test if import
				var importTestString = html.substring(startLinkTag, endLinkTag+1);
				
				//Test if link has a href 
				var linkString = importTestString.match(/href="[\s\S]*?"/g);

				// Add to styles array
				if(linkString) imports.push(linkString[0].replace('href="','').replace('"',''));

				//Clean html
				html = html.substring(0, startLinkTag) + html.substring(endLinkTag+1);

			}
			
			//Loop html and strip out style tags
			while(html.indexOf("<style") > -1 || html.indexOf("</style") > -1) {
				
				//Find style starts
				var startStyleTag = html.indexOf("<style");
				
				//Find end of style starts
				var endStyleTag = html.indexOf(">", startStyleTag);
				
				//Find style closing tag
				var closedStartStyleTag = html.indexOf("</style", startStyleTag);
				
				//Find end of closing tag
				var closedEndStyleTag = html.indexOf(">", closedStartStyleTag);

				// Add to styles array
				styles.push(html.substring(endStyleTag+1, closedStartStyleTag));

				//Clean html
				html = html.substring(0, startStyleTag) + html.substring(closedEndStyleTag+1);

			}
			
		}
		
		//Return formatted loadObj
		return {styles: styles, html: html, imports: imports};
		
	}
	
	//Add styles to fragment
	function addStyles(target,styles) {
	
		// Loop through every styles and append to target
		for(var i=0; i<styles.length; i++) {
		
			//Create new style tag
			var style = document.createElement('style');
			
			//Add type
			style.type = 'text/css';
			
			//Add styles
			if (style.styleSheet){
				style.styleSheet.cssText = styles[i];
			} else {
				style.appendChild(document.createTextNode(styles[i]));
			}
			
			//Prepend target
			target.insertBefore(style,(target.hasChildNodes()) ? target.childNodes[0] : null);
			
		};
	}
	
	//Clear all css loaded
	function clearCss() {
		var styles = head.getElementsByClassName('cssLoaded');
		for(var i=0; i<styles.length; i++) { 
			head.removeChild(styles[i]);
		}
	}
	
	//Clear all js loaded
	function clearJs() {
		var scripts = head.getElementsByClassName('jsLoaded');
		for(var i=0; i<scripts.length; i++) { 
			head.removeChild(scripts[i]);
		};
		//Clear includes data object
		fnb.hyperion.load.includesData = {};
	}
	
	///-------------------------------------------///
	/// Loader Parent function
	///-------------------------------------------///
	function loader() {

	};
	///-------------------------------------------///
	/// Loader Methods
	///-------------------------------------------///
	loader.prototype = {
		includesData: {},
		//Js loader function
        js: function (urls, callback, obj, context) {
        	load('js', urls, callback, obj, context);
        },
        //Css loader function
        css: function (urls, callback, obj, context) {
        	load('css', urls, callback, obj, context);
        },
        //Styles loader function
        styles: function (target, styles) {
        	addStyles(target, styles)
        },
        //Clear css from head
        clearCss: function (urls, callback) {
        	clearCss();
        },
        //Clear js from head
        clearJs: function () {
        	clearJs();
        },
        //Collect inline scripts and import scripts
        collectScripts: function (data) {
        	return getScripts(data);
        },
        //Collect css links
        collectCss: function (data) {
        	return getCss(data);
        },
        //Eval scripts method
        evalScripts: function (scripts) {
			// Loop through every script collected and eval it
			for(var i=0; i<scripts.length; i++) {
				try {
					//Eval sccript
					window.eval(scripts[i]);
				}catch(e) {
				    //Script error
					console.log('Error: utils.ajax.scripts: '+e);
				}
			};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.load = {};
        }
	};

	//Namespace audit
	fnb.namespace('load', loader, true);

})();