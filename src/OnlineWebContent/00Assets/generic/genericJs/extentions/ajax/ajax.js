///-------------------------------------------///
/// developer: Donovan
///
/// Ajax Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax Parent function
	///-------------------------------------------///
	function ajax() {

	};
	///-------------------------------------------///
	/// Ajax Methods
	///-------------------------------------------///
	ajax.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Request object
		xhr: null,
		//Load object
		loadObject: null,
		//Timeout period (in ms) until an async request will be aborted
		timeout: null,
		//Default method of the request, either GET (default), POST, or HEAD
		method : "POST",
		//Default method asynchronous request
		async : true,
		//Parameters posted
		parameters : '',
		//Object notification
		notify : {},
		//Header types object
		headers: {
			'X-Requested-With': 'XMLHttpRequest',
	        'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
	    },
		//Default encoding
		encoding: 'utf-8',
		//Init FNB LoadUrl
    	init: function () {
    		console.log('Ajax init');
        },
        //Create new request  object
        getHttpObject: function (loadObj) {
        	var ajaxObject = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTTP');
        	ajaxObject.onreadystatechange = function(){
        		//Test state change
        		if(ajaxObject.readyState===4){
        			//Test if response was successfull
        			if(ajaxObject.status!=200){fnb.hyperion.ajax.handleErrors(ajaxObject,loadObj);}else{fnb.hyperion.ajax.success(ajaxObject,loadObj);};
        		}else if(ajaxObject.readyState===2){
        			//Test for complete response
        			fnb.hyperion.ajax.complete(ajaxObject,loadObj);
        		}
        	};
        	return ajaxObject;
        },
        //Make ajax request
        request: function (loadObj) {
        	//Set loadObect
        	this.loadObject = loadObj;
        	//Create ajax object and request
        	this.xhr = this.getHttpObject(loadObj);
        	//Raise before request event event
        	this.beforeSend(loadObj, this.xhr);
        	//Get request method
        	var method = (loadObj.type) ? loadObj.type.toUpperCase() : this.method;
        	//Get request type
        	var async = (loadObj.async) ? loadObj.async : this.async;
        	//Get data
        	var data = loadObj.params;
        	//Get url
        	var url = (method=="GET") ? (data)? loadObj.url+'?' + data : loadObj.url : loadObj.url;
        	//Open http request
        	this.xhr.open(method, url, async);
        	//Add Content type header if request method POST
        	if (method=="POST") {
            	var encoding = (this.encoding) ? '; charset=' + this.encoding : '';
            	this.xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded' + encoding);
        	}
        	//Try set other request headers
        	for (var i in this.headers){
                try {
                	xhr.setRequestHeader(i, this.headers[i]);
                } catch (e){

                }
        	}
        	//Send request
        	this.xhr.send((data) ? data : '');
        },
        //Handle ajax state change
        handleErrors: function (xhr,loadObj) {

        	//Test if errors are allowed
        	if(loadObj.error){
        		//Test for error code
            	switch (xhr.status) {
            		case 0:
            			// Connection ended
            			//Set staus
            			loadObj.status = "Error";
            			
            			//Set statusText
            			loadObj.statusText = "A result could not be retrieved from Online Banking. Please check your Internet connection before you continue. If you were performing a financial transaction, please check your transaction history to determine if the transaction was processed before you try again.";
            			
            			if(!loadObj.ajaxAborted) this.error(xhr,loadObj);
            			
            			//Check if ezi needs to be closed
            			fnb.hyperion.controller.checkEziState(loadObj.method)
            			
         	            break;	
    	            case 400:
    	            // Bad Request
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 401:	
    	            // Unauthorized
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 403:
    	            // Unauthorized
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 404:
    	            // Not Found
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 405:
    	            // Method Not Allowed
    	            	this.error(xhr,loadObj);
    	 	            break;
    	            case 406:
    	            // Not Acceptable
    	            
    	            case 408:
    	            // Request Timeout
    	            	this.error(xhr,loadObj);
    	            	break;
    	            case 500:	
    	            // Internal Server Error
    	            	this.error(xhr,loadObj);
    	            break;
    	            default: this.error(xhr,loadObj);
    	         }
        	}
        	
        },
        //Reset Ajax object
        reset: function () {
        	this.xhr = null;
    		//Timeout period (in ms) until an async request will be aborted
        	this.timeout = null;
    		//Default method of the request, either GET (default), POST, or HEAD
        	this.method = "POST";
    		//Default method asynchronous request
        	this.async = true;
    		//Parameters posted
        	this.parameters = '';
    		//Header types object
        	this.headers = {
    			'X-Requested-With': 'XMLHttpRequest',
    	        'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
    	    };
    		//Default encoding
        	this.encoding = 'utf-8';
        },
        //Function executed before ajax call
        beforeSend: function (xhr,loadObj) {
        	//Check if sender wants event to be raised
        	if(loadObj.beforeSend){
        		//Get the calling method form the loadObj
        		var method = loadObj.method;
    			//Ensure there is a default method
    			if(!method) method = '';
            	//Add jqXHR to loadObj
        		loadObj.xhr = xhr;
        		//Notify Controller to raise BeforeSend event
            	fnb.hyperion.controller.raiseEvent(method+'BeforeSend', loadObj);
        	};
        },
        //Function executed when ajax request has completed
        complete: function (xhr,loadObj) {
        	//Check if sender wants event to be raised
        	if(loadObj.complete){
        		//Get the calling method form the loadObj
        		var method = loadObj.method;
    			//Ensure there is a default method
    			if(!method) method = '';
            	//Add jqXHR to loadObj
            	loadObj.xhr = xhr;
            	//Add textStatus to loadObj
            	loadObj.textStatus = xhr.textStatus;
        		//Notify Controller to raise Complete event
            	fnb.hyperion.controller.raiseEvent(method+'Complete', loadObj);
        	};
        },
        //Function executed on ajax request success
        success: function (xhr,loadObj) {
			
        	//Add jqXHR to loadObj
        	loadObj.xhr = xhr;
        	
			//Get data from ajax response
			loadObj.data = (loadObj.xhr.responseText) ? loadObj.xhr.responseText : (loadObj.xhr.response) ? loadObj.xhr.response : loadObj.xhr.responseXML;
        	
    		//Get the calling method form the loadObj
    		var method = loadObj.method;
    		
        	//Test if request was a post
        	if(loadObj.validate!=false){

    			//Validate response
    			if(!fnb.hyperion.controller.validateXHR(xhr,loadObj.postLoadingCallBack,method)) return;
    			
        		//Test for valid response
    			if(!fnb.hyperion.controller.validResponse(loadObj)) return;
    			    			
    			//Test if otp should be raised
    			if(fnb.hyperion.utils.ajax.otp.test(loadObj)) return;
        	}

        	//Check if sender wants event to be raised
        	if(loadObj.success){
        		//Get the calling method form the loadObj
        		var method = loadObj.method;
    			//Ensure there is a default method
    			if(!method) method = '';
    			//Raise Success event
    			fnb.hyperion.controller.raiseEvent(method+'Success', loadObj);
        	};
        	
        },
        //Function executed on ajax request error
        error: function (xhr,loadObj) {

    		//Get status
    		var status = (xhr.status===0) ? loadObj.status : xhr.status;
    		//status text
    		var statusText = (xhr.status===0) ? loadObj.statusText : xhr.statusText;

    		//Notify Controller to raise Error event
        	fnb.hyperion.controller.error(status + ' : '+statusText,loadObj.method);
        	
        },
        //Stop current ajax request
        stop: function (sender) {
        	//Test if there is a xhr
        	if(this.xhr){
        		console.log('Sender: '+sender+' requested an ajax abort on '+this.loadObject.method);
        		this.loadObject.ajaxAborted = true;
				this.xhr.abort();
        		console.log('Ajax aborted.');
			}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.ajax = {};
        }
	};

	//Namespace ajax
	fnb.namespace('ajax', ajax, true);

})();