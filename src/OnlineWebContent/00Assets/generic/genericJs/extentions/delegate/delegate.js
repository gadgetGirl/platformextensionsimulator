///-------------------------------------------///
/// developer: Donovan
///
/// Delegate events
///-------------------------------------------///
/// USAGE: 
///
/// on:
///		/* 	Simple		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), 'click', handler);
///
///		/* 	Internal function		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), 'click', function (event) { console.log('Clicked');});
///
///		/* 	Optional parameters		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), 'click', function (event, param1, param2) { console.log('params: '+param1+' - '+param2);}, 'hallo', 'test');
///
///		/* 	Multiple events		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), 'mousedown mouseup', handler);
///
///		/* 	Multiple handlers		*/
///		fnb.hyperion.delegate.on(fnb.hyperion.$('#numberInput'), {
///			click: function (event) { console.log('click event') },
///			mouseover: function (event) { console.log('click event') },
///			'focus blur': function (event) { console.log('focus blur event') }
///		});
///
/// off:
///		/* 	Remove all events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'));
///
///		/* 	Remove specific handler		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), 'click', handler);
///
///		/* 	Remove all click events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), 'click');
///
///		/* 	Remove handler for all events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), handler);
///
///		/* 	Remove multiple events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), 'mouseup mousedown');
///
///		/* 	Remove specific handlers for specific events		*/
///		fnb.hyperion.delegate.off(fnb.hyperion.$('#numberInput'), { click: clickFunction, mouseover: mouseoverFunction });
///
/// trigger:
///		/* 	Trigger single event		*/
///		fnb.hyperion.delegate.trigger(fnb.hyperion.$('#numberInput'), 'click');
///
///		/* 	Trigger multiple events		*/
///		fnb.hyperion.delegate.trigger(fnb.hyperion.$('#numberInput'), 'click, mouseover');
///
/// one:
///		/* 	Trigger event once then remove handler		*/
///		fnb.hyperion.delegate.one(fnb.hyperion.$('#numberInput'), 'click', handler);
///
/// clone:
///		/* 	Clone all events of an element		*/
///		fnb.hyperion.delegate.clone(fnb.hyperion.$('#numberInput'), fnb.hyperion.$('#textInput'));
///
///		/* 	Clone specific event of an element		*/
///		fnb.hyperion.delegate.clone(fnb.hyperion.$('#numberInput'), fnb.hyperion.$('#textInput'), 'click');
///
///-------------------------------------------///
(function() {
	//Select window
	var win = window;
	//Namespace regular expression
    var namespaceExpression = /[^\.]*(?=\..*)\.|.*/;
	//Name regular expression
    var nameExpression = /\..*/;
	//Add event type
    var addEvent = 'addEventListener';
	//Remove event type
    var removeEvent = 'removeEventListener';
	//Select Doc
    var doc = document || {};
	//Select root
    var root = doc.documentElement || {};
	//Get w3c model
    var model = root[addEvent];
	//Test event support for browsers
    var eventSupport = model ? addEvent : 'attachEvent';
	//Singleton for quick matching making add() do oneObject()
    var oneObject = {};
	
	//List of native events
	var standardNativeEvents = 'click dblclick mouseup mousedown contextmenu mousewheel mousemultiwheel DOMMouseScroll mouseover mouseout mousemove selectstart selectend keydown keypress keyup orientationchange focus blur change reset select submit load unload beforeunload resize move DOMContentLoaded readystatechange message error abort scroll';
	//List of model native events
	var modelNativeEvents = 'show input invalid touchstart touchmove touchend touchcancel gesturestart gesturechange gestureend textinput readystatechange pageshow pagehide popstate hashchange offline online afterprint beforeprint dragstart dragenter dragover dragleave drag drop dragend loadstart progress suspend emptied stalled loadmetadata loadeddata canplay canplaythrough playing waiting seeking seeked ended durationchange timeupdate play pause ratechange volumechange cuechange checking noupdate downloading cached updateready obsolete ' ;
	
	//Delegate Utils
	//Slice method
    var slice = Array.prototype.slice;
	//Convert string to array
    var stringToArray = function (string, devider) { 
		return string.split(devider || ' ');
	};
	//Test if object is string
    var isString = function (obj) { 
		return typeof obj == 'string';
	};
	//Test if object is function
    var isFunction = function (obj) { 
		return typeof obj == 'function';
	};
	
	//Convert hash events for fast loops
	var convertNativeEvents = (function (hash, events, i) {
        for (i = 0; i < events.length; i++) events[i] && (hash[events[i]] = 1);
			return hash;
    }({}, stringToArray(standardNativeEvents + (model ? modelNativeEvents : ''))));
	
	//Create custom events
	var handleCustomEvents = (function () {
        var isAscendant = 'compareDocumentPosition' in root
			? function (element, container) {
				return container.compareDocumentPosition && (container.compareDocumentPosition(element) & 16) === 16;
			}
			: 'contains' in root
			? function (element, container) {
				container = container.nodeType === 9 || container === window ? root : container;
				return container !== element && container.contains(element);
			}
			: function (element, container) {
				while (element = element.parentNode) if (element === container) return 1;
				return 0;
			},
			verify = function (event) {
				var related = event.relatedTarget;
				return 	!related
							? related == null
							: (related !== this && related.prefix !== 'xul' && !/document/.test(this.toString()) && !isAscendant(related, this));
            };
        return {
            mouseenter: { base: 'mouseover', condition: verify }, 
			mouseleave: { base: 'mouseout', condition: verify }, 
			mousewheel: { base: /Firefox/.test(navigator.userAgent) ? 'DOMMouseScroll' : 'mousewheel' }
		};
    }());
	
	//Create cross browser events object
	var Event = (function () {
		// Common Events list
		var commonProps  = stringToArray('altKey attrChange attrName bubbles cancelable ctrlKey currentTarget detail eventPhase getModifierState isTrusted metaKey relatedNode relatedTarget shiftKey srcElement target timeStamp type view which propertyName');
		//Mouse events
		var mouseProps   = commonProps.concat(stringToArray('button buttons clientX clientY dataTransfer fromElement offsetX offsetY pageX pageY screenX screenY toElement'));
		//Mouse wheel events
		var mouseWheelProps = mouseProps.concat(stringToArray('wheelDelta wheelDeltaX wheelDeltaY wheelDeltaZ axis'));
		//Key events
		var keyProps     = commonProps.concat(stringToArray('char charCode key keyCode keyIdentifier keyLocation location'));
		//Text events
		var textProps    = commonProps.concat(stringToArray('data'));
		//Touch events
		var touchProps   = commonProps.concat(stringToArray('touches targetTouches changedTouches scale rotation'));
		//Message events
		var messageProps = commonProps.concat(stringToArray('data origin source'));
		//State events
		var stateProps   = commonProps.concat(stringToArray('state'));
		//Ouver & Out regular expressions
		var overOutRegex = /over|out/;
		
		// Events that need special handling
		var typeFixers   = [
			//Special key events
			{
				reg: /key/i, 
				fix: function (event, newEvent) {
					newEvent.keyCode = event.keyCode || event.which;
					return keyProps;
				}
			}, 
			 //Special mouse events
			{
				reg: /click|mouse(?!(.*wheel|scroll))|menu|drag|drop/i,
				fix: function (event, newEvent, type) {
				
					newEvent.rightClick = event.which === 3 || event.button === 2;
					newEvent.pos = { x: 0, y: 0 };
					
					if (event.pageX || event.pageY) {
						newEvent.clientX = event.pageX;
						newEvent.clientY = event.pageY;
					} else if (event.clientX || event.clientY) {
						newEvent.clientX = event.clientX + doc.body.scrollLeft + root.scrollLeft;
						newEvent.clientY = event.clientY + doc.body.scrollTop + root.scrollTop;
					}
					
					if (overOutRegex.test(type)) {
						newEvent.relatedTarget = event.relatedTarget || event[(type == 'mouseover' ? 'from' : 'to') + 'Element'];
					}
					return mouseProps;
					
				}
			},
			//Special mouse wheel events
			{
				reg: /mouse.*(wheel|scroll)/i,
				fix: function () { return mouseWheelProps;}
			},
			//Special textEvents
			{ 
				reg: /^text/i,
				fix: function () { return textProps;}
			},
			//Special touch and gesture events
			{
				reg: /^touch|^gesture/i,
				fix: function () { return touchProps;}
			},
			//Special message events
			{
				reg: /^message$/i,
				fix: function () { return messageProps;}
			},
			//Special popstate events
			{ 
				reg: /^popstate$/i,
				fix: function () { return stateProps;}
			},
			//Other special events
			{
				reg: /.*/,
				fix: function () { return commonProps;}
			}
		];
		
		// Var to map event types to fixed functions
		var fixedTypeMap = {};
		//Main Event function
		var Event = function (event, element, isNative) {
			//Test for arguments else return
			if (!arguments.length) return;
			//Get owner correct event
			event = event || ((element.ownerDocument || element.document || element).parentWindow || win).event;
			//Get original event
			this.originalEvent = event;
			//Duplicate isNave val
			this.isNative = isNative;
			//If there is no event return nothing
			if (!event) return;
			//Set type of event
			var type   = event.type;
			//Get correct souce
			var target = event.target || event.srcElement;

			//Temp vars
			var i;
			var l;
			var p;
			var properties;
			var fixed;
			
			//Test if target and nodetype is TEXT_NODE. switch parent or current target
			this.target = target && target.nodeType === 3 ? target.parentNode : target;
			
			//Test for native event
			if (isNative) {
				//Fix event type 
				fixed = fixedTypeMap[type];
				//Test if event was fixed, if not type is array
				if (!fixed) {
					//Loop type array and fix
					for (i = 0, l = typeFixers.length; i < l; i++) {
						if (typeFixers[i].reg.test(type)) {
							fixedTypeMap[type] = fixed = typeFixers[i].fix;
							break;
						}
					}
				}
				
				properties = fixed(event, this, type);
				
				for (i = properties.length; i--;) {
					if (!((p = properties[i]) in this) && p in event) this[p] = event[p];
				}
				
			}
		};
		//Event methods
		//Create cross browser preventDefault method
		Event.prototype.preventDefault = function () {
			if (this.originalEvent.preventDefault) this.originalEvent.preventDefault();
			else this.originalEvent.returnValue = false;
		};
		//Create cross browser stopPropagation method
		Event.prototype.stopPropagation = function () {
			if (this.originalEvent.stopPropagation) this.originalEvent.stopPropagation();
			else this.originalEvent.cancelBubble = true;
		};
		//Create cross browser stop method
		Event.prototype.stop = function () {
			this.preventDefault();
			this.stopPropagation();
			this.stopped = true;
		};
		//Create cross browser stopImmediatePropagation method
		Event.prototype.stopImmediatePropagation = function () {
			if (this.originalEvent.stopImmediatePropagation) this.originalEvent.stopImmediatePropagation();
			this.isImmediatePropagationStopped = function () { return true; };
		};
		//Create cross browser isImmediatePropagationStopped method
		Event.prototype.isImmediatePropagationStopped = function () {
			return this.originalEvent.isImmediatePropagationStopped && this.originalEvent.isImmediatePropagationStopped();
		};
		//Create cross browser clone method
		Event.prototype.clone = function (currentTarget) {
			var newEvent = new Event(this, this.element, this.isNative);
			newEvent.currentTarget = currentTarget;
			return newEvent;
		};
		//Return new event object
		return Event;
			
	}());
	
	//Get target element
	var targetElement = function (element, isNative) {
		return !model && !isNative && (element === doc || element === win) ? root : element;
	};
	
	//Internal registry for event listeners, there is no registry for the entite instance
	var registryEntry = (function () {
		//Wrap handler to handle delegation and custom events
		var wrappedHandler = function (element, fn, condition, args) {
			//Bind call function to handler
			var call = function (event, eargs) {
				return fn.apply(element, args ? slice.call(eargs, event ? 0 : 1).concat(args) : eargs);
			};
			//Bind findTarget function to handler
			var findTarget = function (event, eventElement) {
				return fn.__delegateDEL ? fn.__delegateDEL.ft(event.target, element) : eventElement;
			};
			//Test for function that applies to handler
			var handler = condition
				? function (event) {
					var target = findTarget(event, this);
					if (condition.apply(target, arguments)) {
						if (event) event.currentTarget = target;
						return call(event, arguments);
					}
				}
				: function (event) {
					if (fn.__delegateDEL) event = event.clone(findTarget(event));
					
					return call(event, arguments);
				};
				
			//Add  __delegateDEL to handler
			handler.__delegateDEL = fn.__delegateDEL;
			
			return handler;
		},
		registryEntry = function (element, type, handler, original, namespaces, args, root) {
			//Test for custom type of event
			var customType = handleCustomEvents[type];
			//Declare isNative var
			var isNative;
			
			//Test for unload event then clean
			if (type == 'unload') {
				handler = once(removeListener, element, type, handler, original);
			}

			//Hanle custom event
			if (customType) {
				if (customType.condition) {
					handler = wrappedHandler(element, handler, customType.condition, args);
				}
				type = customType.base || type;
			}

			this.isNative = isNative = convertNativeEvents[type] && !!element[eventSupport];
			this.customType = !model && !isNative && type;
			this.element = element;
			this.type = type;
			this.original = original;
			this.namespaces = namespaces;
			this.eventType = model || isNative ? type : 'propertychange';
			this.target = targetElement(element, isNative);
			this[eventSupport] = !!this.target[eventSupport];
			this.root = root;
			this.handler = wrappedHandler(element, handler, null, args);
		};

		//Test if namespaces are in the registry
		registryEntry.prototype.inNamespaces = function (namespaces) {
			var i, j, c = 0;
			if (!namespaces) return true;
			if (!this.namespaces) return false;
			for (i = namespaces.length; i--;) {
				for (j = this.namespaces.length; j--;) {
					if (namespaces[i] == this.namespaces[j]) c++;
				}
			}
			return namespaces.length === c;
		};

		//Math element by original handler
		registryEntry.prototype.matches = function (element, original, handler) {
			return this.element === element &&	(!original || this.original === original) && (!handler || this.handler === handler);
		};

		return registryEntry;
		
	}());
	
	var registry = (function () {
		//Map stores arrays by event types
		var map = {};
		//Search registry for listeners
		var forAll = function (element, type, original, handler, root, fn) {
			var prefix = root ? 'x' : '@';
			if (!type || type == '*') {
				//Search the entire registry
				for (var t in map) {
					if (t.charAt(0) == prefix) {
						forAll(element, t.substr(1), original, handler, root, fn);
					}
				}
			} else {
				var i = 0, l, list = map[prefix + type], all = element == '*';
				if (!list) return;
				for (l = list.length; i < l; i++) {
					if ((all || list[i].matches(element, original, handler)) && !fn(list[i], list, i, type)) return;
				}
			}
		};
		
		//Test if element matches element with original
		var has = function (element, type, original, root) {
			var i, list = map[(root ? 'x' : '@') + type];
			if (list) {
				for (i = list.length; i--;) {
					if (!list[i].root && list[i].matches(element, original, null)) return true;
				}
			}
			return false;
		};
		//Get rigistry enties
		var get = function (element, type, original, root) {
			var entries = [];
			forAll(element, type, original, null, root, function (entry) {
				return entries.push(entry);
			});
			return entries;
		};
		//Put in registry
		var put = function (entry) {
			var has = !entry.root && !this.has(entry.element, entry.type, null, false)
			, key = (entry.root ? 'x' : '@') + entry.type
			;(map[key] || (map[key] = [])).push(entry);
			return has;
		};
		//Delete entry
		var del = function (entry) {
			forAll(entry.element, entry.type, null, entry.handler, entry.root, function (entry, list, i) {
				list.splice(i, 1);
				entry.removed = true;
				if (list.length === 0) delete map[(entry.root ? 'x' : '@') + entry.type];
				return false;
			});
		};
		//Remove all entries, used for onunload
		var entries = function () {
			var t, entries = [];
			for (t in map) {
				if (t.charAt(0) == '@') entries = entries.concat(map[t]);
			}
			return entries;
		};

		return { has: has, get: get, put: put, del: del, entries: entries };
		
	}());
	 
	var Selector;
	//Set selector otherwise browser not compatible
	var setSelector = function (e) {
		if (!arguments.length) {
			Selector = doc.querySelectorAll
			? function (s, r) {
				return r.querySelectorAll(s);
			}
			: function () {
				console.log('Delegate: Couldnt select element');
			};
		} else {
			Selector = e;
		}
	};
	
	//Listener to each DOM event
	var rootListener = function (event, type) {
		if (!model && type && event && event.propertyName != '_on' + type) return;

		var listeners = registry.get(this, type || event.type, null, false);
		var l = listeners.length;
		var i = 0;

		event = new Event(event, this, true);

		if (type) event.type = type;
		
		//Iterate through handlers that have been registered and call
		for (; i < l && !event.isImmediatePropagationStopped(); i++) {
			if (!listeners[i].removed){
				listeners[i].handler.call(this, event);
			}
		}
	};
	
	//Add and remove listeners to elements
	var listener = model
	? function (element, type, add) {
		//Modern browsers
		element[add ? addEvent : removeEvent](type, rootListener, false);
	}
	: function (element, type, add, custom) {
		//IE8
		var entry;
		//Add listener
		if (add) {
		
			registry.put(entry = new registryEntry(
				element,
				custom || type,
				function (event) {
					rootListener.call(element, event, custom);
				},
				rootListener,
				null,
				null,
				true
			));
			
			if (custom && element['_on' + custom] == null) element['_on' + custom] = 0;
			entry.target.attachEvent('on' + entry.eventType, entry.handler);
			
		}
		//Remove listener
		else {
		
			entry = registry.get(element, custom || type, rootListener, true)[0];
			
			if (entry) {
			
				entry.target.detachEvent('on' + entry.eventType, entry.handler);
				registry.del(entry);
				
			}
		}
	};
	
	//Add move to handler
	var once = function (remove, element, type, fn, originalFn) {
		return function () {
			fn.apply(this, arguments);
			remove(element, type, originalFn);
		};
	};
	
	//Remove listener funvtion
	var removeListener = function (element, orgType, handler, namespaces, stringComp) {
		//Get type of listener
		var type = orgType && orgType.replace(nameExpression, '');
		//Get matching handler registry entry
		var handlers = registry.get(element, type, null, false);

		//Temp vars
		var removed = {};
		var  i;
		var l;
		//Loop bound handlers and remove
		for (i = 0, l = handlers.length; i < l; i++) {
			if(stringComp){
				if ((!handler || handlers[i].original.toString()== handler.toString()) && handlers[i].inNamespaces(namespaces)) {
					registry.del(handlers[i]);
					if (!removed[handlers[i].eventType] && handlers[i][eventSupport]) removed[handlers[i].eventType] = { t: handlers[i].eventType, c: handlers[i].type };
				}
			}else{
				if ((!handler || handlers[i].original == handler) && handlers[i].inNamespaces(namespaces)) {
					registry.del(handlers[i]);
					if (!removed[handlers[i].eventType] && handlers[i][eventSupport]) removed[handlers[i].eventType] = { t: handlers[i].eventType, c: handlers[i].type };
				}
			}

		}
		//Find the parent listener and remove
		for (i in removed) {
			if (!registry.has(element, removed[i].t, null, false)) {
				listener(element, removed[i].t, false, removed[i].c);
			}
		}
	};
	
	//Delegate events
	var delegate = function (selector, fn) {
		//Find target
		var findTarget = function (target, root) {
			var i, array = isString(selector) ? Selector(selector, root) : selector;
			for (; target && target !== root; target = target.parentNode) {
				for (i = array.length; i--;) {
				  if (array[i] === target){
					  return target;
				  }
				}
			}
		};
		var handler = function (e) {
			var match = findTarget(e.target, this);
			if (match){
				//fnb.hyperion.controller.DOMevent(match,e);
				fn.apply(match, arguments);
			}
		};
		//Private function not exposed
		handler.__delegateDEL = {
			ft : findTarget,
			selector : selector
		};
		
		return handler;
		
	};
	
	//Trigger listener events
	var triggerListener = model ? function (isNative, type, element) {
		//Modern browsers
		var evt = doc.createEvent(isNative ? 'HTMLEvents' : 'UIEvents');
		evt[isNative ? 'initEvent' : 'initUIEvent'](type, true, true, win, 1);
		element.dispatchEvent(evt);
	} : function (isNative, type, element) {
		//Old browsers
		element = targetElement(element, isNative);
		isNative ? element.fireEvent('on' + type, doc.createEventObject()) : element['_on' + type]++;
	};
	
	//DELEGATE public methods: off(), on(), add(), one(), trigger(), clone()
	//Undind event from element
	var off = function (element, typeSpec, fn, stringComp) {
		
		var isTypeStr = isString(typeSpec);
		var k;
		var type;
		var namespaces;
		var i;

		if (isTypeStr && typeSpec.indexOf(' ') > 0) {
			typeSpec = stringToArray(typeSpec);
			for (i = typeSpec.length; i--;)
			off(element, typeSpec[i], fn);
			return element;
		}

		type = isTypeStr && typeSpec.replace(nameExpression, '');
		
		if (type && handleCustomEvents[type]) type = handleCustomEvents[type].base;

		if (!typeSpec || isTypeStr) {
			if (namespaces = isTypeStr && typeSpec.replace(namespaceExpression, '')) namespaces = stringToArray(namespaces, '.');
			removeListener(element, type, fn, namespaces, stringComp);
		} else if (isFunction(typeSpec)) {
			removeListener(element, null, typeSpec, null, stringComp);
		} else {
			for (k in typeSpec) {
				if (typeSpec.hasOwnProperty(k)) off(element, k, typeSpec[k]);
			}
		}

		return element;
	};
	
	//Bind event to element
	var on = function(element, events, selector, fn) {
		
		var originalFn;
		var type;
		var types;
		var i;
		var args;
		var entry;
		var first;

		if (selector === undefined && typeof events == 'object') {
			for (type in events) {
				if (events.hasOwnProperty(type)) {
					on.call(this, element, type, events[type]);
				}
			}
			
			return;
			
		}
		
		if (!isFunction(selector)) {
			originalFn = fn;
			args = slice.call(arguments, 4);
			fn = delegate(selector, originalFn, selector);
		} else {
			args = slice.call(arguments, 3);
			fn = originalFn = selector;
		}

		types = stringToArray(events);

		if (this === oneObject) {
			fn = once(off, element, events, fn, originalFn);
		}

		for (i = types.length; i--;) {
		
			first = registry.put(entry = new registryEntry(
				element,
				types[i].replace(nameExpression, ''),
				fn,
				originalFn,
				stringToArray(types[i].replace(namespaceExpression, ''), '.'),
				args,
				false
			));
			
			if (entry[eventSupport] && first) {
				listener(element, entry.eventType, true, entry.customType);
			}
			
		}
		
		return element;
		
	};
	
	//Single instance event
	var one = function () {
		return on.apply(oneObject, arguments);
	};
	
	//Trigger event on elenemnt
	var trigger = function (element, type, args) {
		
		var types = stringToArray(type);
		var i;
		var j;
		var l;
		var names;
		var handlers;

		//Loop and trigger events
		for (i = types.length; i--;) {
			type = types[i].replace(nameExpression, '');
			
			if (names = types[i].replace(namespaceExpression, '')) names = stringToArray(names, '.');
			
			if (!names && !args && element[eventSupport]) {
				triggerListener(convertNativeEvents[type], type, element);
			} else {
				handlers = registry.get(element, type, null, false);
				args = [false].concat(args);
				for (j = 0, l = handlers.length; j < l; j++) {
					if (handlers[j].inNamespaces(names)) {
						handlers[j].handler.apply(element, args);
					}
				}
			}
		}
		
		return element;
		
	};
	
	//Clone event from element
	var clone = function (element, from, type) {
	
		var handlers = registry.get(from, type, null, false);
		var l = handlers.length;
		var i = 0;
		var args, delegateDEL;

		for (; i < l; i++) {
			if (handlers[i].original) {
				args = [ element, handlers[i].type ];
				
				if (delegateDEL = handlers[i].handler.__delegateDEL) args.push(delegateDEL.selector);
				args.push(handlers[i].original);
				on.apply(null, args);
			}
		}
		return element;
	};
	
	//IE leaks fix
	if (win.attachEvent) {
		var cleanup = function () {
			var i, entries = registry.entries();
			
			for (i in entries) {
				if (entries[i] && entries[i].type && entries[i].type !== 'unload') off(entries[i].element, entries[i].type);
			}
			win.detachEvent('onunload', cleanup);
			win.CollectGarbage && win.CollectGarbage();
		};
		win.attachEvent('onunload', cleanup);
	};
	
	//Setup selector
	setSelector();
	///-------------------------------------------///
	/// Delegate Parent function
	///-------------------------------------------///
	function delegate() {};
	///-------------------------------------------///
	/// Delegate Methods
	///-------------------------------------------///
	delegate.on = on;
	
	delegate.one = one;
	
	delegate.off = off;
	
	delegate.trigger = trigger;
	
	delegate.clone = clone;
	
	delegate.Event = Event;

	//Namespace delegate
	fnb.namespace('delegate', delegate);
})();