///-------------------------------------------///
/// developer: Donovan
///
/// Serialize Object
///-------------------------------------------///
(function() {
	var serializedObject;
	//Main serialize method
	function serializeTarget(target) {
		//Reset serializedObject
		serializedObject = [];
		//Select all from elements
		var formElements = target.find('*input,select,button,textarea');
		//Test if elements was found
		if(formElements.length()==0){
			serializeObject(target.elem);
		}else{
			formElements.each(function(element){
				serializeObject(element.elem);
			});
		}
		return serializedObject.join("&");
    };
    //Serialize element
	function serializeObject(element) {
		switch (element.nodeName) {
		case 'INPUT':
			//Process input data and push into object
			var data = processInput(element);
			if(data) {
				serializedObject.push(data);
			}
			break;
		case 'TEXTAREA':
			//Process text area data and push into object
			serializedObject.push(processTextArea(element));
			break;
		case 'SELECT':
			//Process select data and push into object
			if(processSelect(element)) serializedObject.push(processSelect(element));
			break;
		case 'BUTTON':
			//Process button data and push into object
			if(processButton(element)) serializedObject.push(processButton(element));
			break;
		}
	};
	//Process inputs function
	function processInput(element) {
		//Declare new data string
		var data = "";
		///Switch input types
		switch (element.type) {
			case 'text':
			case 'hidden':
			case 'password':
			case 'button':
			case 'reset':
			case 'submit':
				if (element.getAttribute("data-type") == "masked") {
					var simpleDataValue = element.value.replace(new RegExp(/ /g), "");
					//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					simpleDataValue = (simpleDataValue == "")? undefined : simpleDataValue;
					//if name value equals undefined return nothing.
					if(name == undefined || simpleDataValue == undefined ){
					  data = '';
					}else{
					  data = name + "=" + encodeURIComponent(simpleDataValue);	
					}					
				}else if(element.getAttribute("data-placeholder")==element.value){
					//check if inupt has name value if not use id.If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					var value = (element.value == "")? undefined : element.value;
					//if name value equals undefined return nothing.
					if(name == undefined || value == undefined ){
					  data = '';
					}else{
					  data = name + "=" + encodeURIComponent(value);
					}
					
				}else{
					//check if inupt has name value if not use id.If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					var value = (element.value == "")? undefined : element.value;
					//if name value equals undefined return nothing.
					if(name == undefined || value == undefined ){
					  data = '';
					}else{
					  data = name + "=" + encodeURIComponent(value);
					}
				}
				return data;
			break;
			case 'checkbox':
				if (element.getAttribute("checked") == 'true'|| element.getAttribute("checked")=="checked"|| element.getAttribute("checked")=="CHECKED" || element.getAttribute("data-unCheckedValue")) {
					//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					// set values from checkbox.
					var valueAttr = element.value;
					//get the checked value form the checkbox.
					var unCheckedValue = element.getAttribute("data-unCheckedValue");
					//get the Unchecked value form the checkbox.
					var checkedValue = element.getAttribute("data-checkedValue");
					// set value if checked use check value if unchecked use unCheckedValue else use valueAttr or leave empty.
					var value = (unCheckedValue&&!element.getAttribute("checked")) ? unCheckedValue : (checkedValue&&element.getAttribute("checked")) ? checkedValue : (!checkedValue&&element.getAttribute("checked")) ? (valueAttr == "on") ? "true": '' : valueAttr;
					// only submit data if value and name is not empty.
					data = (value!=''&&name) ? name + "=" + encodeURIComponent(value) : '';

				}
				return data;
			break;
			case 'radio':
				if (element.getAttribute("checked") == 'true'|| element.getAttribute("checked")=="checked"|| element.getAttribute("checked")=="CHECKED") {
					//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					// set value if checked use check value if unchecked use unCheckedValue else use valueAttr or leave empty.
					var value = element.value;
					// only submit data if value and name is not empty.
					data = (value!=''&&name) ? name + "=" + encodeURIComponent(value) : '';
				}
				return data;
			break;
			case 'file':
			break;
		}
		//Return nothing if no data collected
		return;
	}
	//Process text areas function
	function processTextArea(element) {
		//Declare new data string
		var data = element.name + "=" + encodeURIComponent(element.value);
		//Return data
		return data;
	}
	//Process selects function
	function processSelect(element) {
		//Declare new data string
		var data = "";
		var j;
		switch (element.type) {
			case 'select-one':
				data = element.name + "=" + encodeURIComponent(element.value);
				return data;
			break;
			case 'select-multiple':
				for (j = element.options.length - 1; j >= 0; j = j - 1) {
					if (element.options[j].selected) {
						//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
						var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
						var value = (element.value == "")? undefined : element.options[j].value;
						//if name value equals undefined return nothing.
						if(name == undefined || value == undefined ){
						  data = '';
						}else{
						  data = name + "=" + encodeURIComponent(value);	
						}
						return data;
					}
				}
			break;
		}
		//Return nothing if no data collected
		return;
	}

	//Process button function
	function processButton(element) {
		//Declare new data string
		var data = "";
		switch (element.type) {
			case 'reset':
			case 'submit':
			case 'button':
					//check if inupt has name value if not use id. If there is no name or id value, set name to undefined.
					var name = (element.name == "" || element.name== undefined) ? (element.id == "" || element.id== undefined) ? undefined: element.id : element.name;
					var value = (element.value == "")? undefined : element.value;
					//if name value equals undefined return nothing.
					if(name == undefined || value == undefined){
					  data = '';
					}else{
					  data = name + "=" + encodeURIComponent(value);	
					}
				return data;
			break;
		}
		//Return nothing if no data collected
		return;
	}
	///-------------------------------------------///
	/// Serialize Parent function
	///-------------------------------------------///
	function serialize() {

	};
	///-------------------------------------------///
	/// Serialize Methods
	///-------------------------------------------///
	serialize.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init method
		init: function () {
    	   
		},
        //Serialize target
        get: function (target) {
        	return serializeTarget(target);
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.serialize = {};
        }
	};

	//Namespace utils.serialize
	fnb.namespace('serialize', serialize, true);

})();