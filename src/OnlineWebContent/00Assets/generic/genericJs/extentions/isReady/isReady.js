///-------------------------------------------///
/// developer: Donovan
///
/// Updated Ready Object
///-------------------------------------------///
(function() {
	//Object vars
	var isReadyList;
	var DOMContentLoaded;
	var classType = {};
	//Setup class types
	classType["[object Boolean]"] = "boolean";
	classType["[object Number]"] = "number";
	classType["[object String]"] = "string";
	classType["[object Function]"] = "function";
	classType["[object Array]"] = "array";
	classType["[object Date]"] = "date";
	classType["[object RegExp]"] = "regexp";
	classType["[object Object]"] = "object";
	
	//Ready methods
	var readyObj = {
		//Is the DOM ready flag
		isReady: false,
		//Counter ready items
		readyWait: 1,
		//Wait for ready event
		waitReady: function(hold) {
			if (hold) {
				readyObj.readyWait++;
			} else {
				readyObj.ready( true );
			}
		},
		//DOM ready function
		ready: function( wait ) {
			//Test if dom is ready
			if ((wait === true && !--readyObj.readyWait) || (wait !== true && !readyObj.isReady)) {
				//Test if body exists
				if ( !document.body ) {
					return setTimeout(readyObj.ready,1);
				}
				//Set dom ready flag
				readyObj.isReady = true;
				//Test if dom ready event fired otherwise wait
				if ( wait !== true && --readyObj.readyWait > 0 ) {
					return;
				}
				//Fire callback
				isReadyList.executeCallbacks(document,[ readyObj ]);
			}
		},
		//Bind events function
		bindEvents: function() {
			//Test if ready list already exists and return
			if (isReadyList) {
				return;
			}
			//Bind callback manager to ready list
			isReadyList = readyObj.callbackManager();
			//Test if ready already fired
			if (document.readyState === "complete") {
				//Handle ready if ready already fired
				return setTimeout( readyObj.ready, 1 );
			}
			//Bind modern browser events
			if (document.addEventListener) {
				//Bind event
				document.addEventListener( "DOMContentLoaded", DOMContentLoaded, false );
				//Backup event
				window.addEventListener( "load", readyObj.ready, false );
			//IE
			} else if (document.attachEvent) {
				//Bind event
				document.attachEvent( "onreadystatechange", DOMContentLoaded );
				//Backup event
				window.attachEvent( "onload", readyObj.ready );

				var toplevel = false;
				
				//Test when document is ready
				try {
					toplevel = window.frameElement == null;
				} catch(e) {}

				if ( document.documentElement.doScroll && toplevel ) {
					doScrollCheck();
				}
			}
		},
		//Callback handler
		callbackManager: function() {
			//Callback list
			var callbacks = [];
			//Callback fired flag
			var fired;
			//Callback busy flag
			var firing;
			//Callback cancelled flag
			var cancelled;
			// the callbackHandler itself
			var callbackHandler  = {
					//Callback done method
					done: function() {
						//Test if callback was cancelled
						if (!cancelled) {
							//Done vars
							var args = arguments;
							var i;
							var length;
							var elem;
							var type;
							var _fired;
							
							if (fired) {
								_fired = fired;
								fired = 0;
							}
							
							for ( i = 0, length = args.length; i < length; i++ ) {
							
								elem = args[ i ];
								type = readyObj.type( elem );
								
								if ( type === "array" ) {
									callbackHandler.done.apply( callbackHandler, elem );
								} else if ( type === "function" ) {
									callbacks.push( elem );
								}
							}
							
							if ( _fired) {
								callbackHandler.executeCallbacks( _fired[ 0 ], _fired[ 1 ] );
							}
							
						}
						return this;
					},
					//Execute callback with args
					executeCallbacks: function( context, args ) {
						//Test if callback was fired or is busy firing
						if (!cancelled && !fired && !firing ) {
							//Mak sure args are not null
							args = args || [];
							firing = 1;
							
							try {
								while( callbacks[ 0 ] ) {
									callbacks.shift().apply( context, args );//shifts a callback, and applies it to document
								}
							}
							
							finally {
								fired = [ context, args ];
								firing = 0;
							}
						}
						
						return this;
					},
					//Resolve callback with context and args
					resolve: function() {
						callbackHandler.executeCallbacks( this, arguments );
						return this;
					},
					//Is callback resolved?
					isResolved: function() {
						return !!( firing || fired );
					},
					//Cancel
					cancel: function() {
						cancelled = 1;
						callbacks = [];
						return this;
					}
				};

			return callbackHandler;
		},
		//Return type of object that is ready
		type: function( obj ) {
			return obj == null ?	String( obj ) : classType[ Object.prototype.toString.call(obj) ] || "object";
		}
	}
	//IE dom ready test
	function doScrollCheck() {
	
		if ( readyObj.isReady ) {
			return;
		}

		try {
			document.documentElement.doScroll("left");
		} catch(e) {
			setTimeout( doScrollCheck, 1 );
			return;
		}

		//Fire ready event
		readyObj.ready();
	}
	//Remove previous ready events
	if ( document.addEventListener ) {
	    DOMContentLoaded = function() {
	        document.removeEventListener( "DOMContentLoaded", DOMContentLoaded, false );
	        readyObj.ready();
	    };
	
	} else if ( document.attachEvent ) {
	    DOMContentLoaded = function() {
	        if ( document.readyState === "complete" ) {
	            document.detachEvent( "onreadystatechange", DOMContentLoaded );
	            readyObj.ready();
	        }
	    };
	}
	
	//Main ready function
	function ready(func) {
	    //Bind events
	    readyObj.bindEvents();
		//Get func type
	    var type = readyObj.type(func);
	
	    //Add callback
	    isReadyList.done(func);
	}
	
	//Namespace ready
	fnb.namespace('ready', ready);

})();
