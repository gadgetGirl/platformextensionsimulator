///-------------------------------------------///
/// developer: Donovan
///
/// horizontalScroller Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// horizontalScroller Parent function
	///-------------------------------------------///
	function horizontalScroller() {
		this.doc = fnb.hyperion.$(document).elem;
		this.startX = null;
	    this.endX = null;
	    this.startOffset = null;
	    this.scrollableParent = null;
	    this.scrollerChildren = null;
	    this.x=0;
	    this.currentIndex =0;
		this.scrollSpeed =0;
	    this.fixedStops = true;
	    this.scrollStopWidth = 0;
	    this.maxStops = 0;
	    this.moveTreshold = 0.3;
	    this.tapTreshold = 0;
	    this.tap = function(){ };
	    this.afterStop =  function(index){  };
	    this.doVertical = false;
	    this.startElement = null;
	    this.bindLive = false;
		this.enabled = false;
		this.moving = false;
		this.initialized = false;
		this.internalEvent = false;
		
	    var parent = this;

	    function isset(v){
	        return(typeof v != 'undefined');
	    }

	    function mouseX(event){
	        return (isset(event.targetTouches)) ? event.targetTouches[0].pageX : event.pageX;
	    }

	    this.bindEvents = function(){
			this.bindEvents();
	    };

	    function start(e){
	    	if(parent.enabled){
		        parent.startElement = e.target;  
		        parent.startX = parent.endX = mouseX(e);

		        var moveEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchmove' : 'mousemove';
		        var upEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchend' : 'mouseup';
		        var leaveEvent = (fnb.hyperion.controller.isMobile==true) ? null : 'mouseleave';

	        	fnb.hyperion.delegate.on(parent.doc, moveEvent,move);
	        	fnb.hyperion.delegate.on(parent.doc, upEvent,end);
	        	
	        	if(leaveEvent) fnb.hyperion.delegate.on(parent.doc, leaveEvent,cancel);
	    	}
	    };

	    function move(e){
			if(parent.enabled){
				if(Math.abs(mouseX(e)-parent.startX)>5){
					parent.moving = true;
		        	//Prevent default event
					event.preventDefault();
					if(parent.startX !== null && parent.scrollableParent.elem !== null){
						parent.endX = mouseX(e);
						var val = parent.x+(parent.endX-parent.startX);
						parent.scrollableParent.elem.style.left = val+"px";
					}
				}
				
			}
	    };

	    function end(e){

	    	if(parent.fixedStops && parent.startX !== null && parent.endX !== null && parent.scrollableParent.elem !== null){
	            parent.moveToClosest();
	            parent.startElement = e.target;  
	            var moveX = Math.abs(parent.startX - parent.endX);
	            if (moveX <= parent.tapTreshold * parent.scrollStopWidth) {
	                if( parent.startElement != null ) { parent.tap( parent.startElement ); }
	            }
	        };

	        parent.startX = parent.endX = parent.startElement = null;
	        
	        var moveEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchmove' : 'mousemove';
	        var upEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchend' : 'mouseup';
	        var leaveEvent = (fnb.hyperion.controller.isMobile==true) ? null : 'mouseleave';
	        
        	fnb.hyperion.delegate.off(parent.doc, moveEvent,move);
        	fnb.hyperion.delegate.off(parent.doc, upEvent,end);
        	
        	if(leaveEvent) fnb.hyperion.delegate.off(parent.doc, leaveEvent,cancel);
        	
	    };

	    function cancel(e){
        	//Prevent default event
        	e.preventDefault();
	        if(parent.fixedStops && parent.startX !== null && parent.endX !== null && parent.scrollableParent.elem !== null){
	            parent.moveToClosest();
	        }
	        parent.startX = parent.endX = parent.startElement = null;
	    };

	    this.moveToClosest = function (){
			var _this = this;
			
	        var moveX = this.startX-this.endX,
	            currI = Math.round((-1*this.x) / this.scrollStopWidth),
	            newI = currI,
	            newloc = this.scrollStopWidth*(currI);

	        if(moveX > this.moveTreshold*this.scrollStopWidth && currI+1 <= (this.maxStops)){
	            newI = currI+1;
	        }

	        if(((-1)*moveX) > this.moveTreshold*this.scrollStopWidth && currI-1 >= 0){
	             newI = currI-1;
	        }

	        newloc = Math.round(this.scrollStopWidth*(newI));
	        this.currentIndex = newI;
	        this.x = -1*(newloc);

	        fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).stop();
	        fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).move({x:(-1*newloc)}, this.scrollSpeed, function() {setTimeout(function() {if(_this.afterStop) _this.afterStop(_this.currentIndex);parent.moving = false;}, 200);});
	    };

	    this.moveTo = function(index, nostop){

			if(this.initialized == false||this.internalEvent == true){
				var newloc = this.scrollStopWidth*(index);
				var _this = this;
				
				fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).stop();
				fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).move({x:(-1*newloc)}, this.scrollSpeed, function() {setTimeout(function() {if(_this.afterStop) _this.afterStop(index);parent.moving = false;}, 200);});
	        	
				this.currentIndex = index;
				this.x = -1*(newloc);

				this.internalEvent = false;
				this.initialized = true;
			}
	    };

	    this.next = function(){
	        if(this.currentIndex+1 <= this.maxStops){
				this.internalEvent = true;
				this.moving = true;
	            this.moveTo(this.currentIndex+1);
	        }
	    };

	    this.previous = function(){
	        if(this.currentIndex-1 >= 0){
				this.internalEvent = true;
				this.moving = true;
	            this.moveTo(this.currentIndex-1);
	        }
	    };

	    this.centerIndex = function (index){
			if (this.scrollableParent.elem !== null){
				if(isset(index)){
					this.currentIndex = index;
				} else {
					index = this.currentIndex;
				}
				var loc = -1*((index)*this.scrollStopWidth);

				this.scrollableParent.css('left',loc+'px');
				fnb.hyperion.horizontalScroller.slide(this.scrollableParent.elem).stop();
				this.x = loc;
			}
	    };
	    
	    this.end = end;
	    
	    this.bindEvents = function(){
	        if(this.scrollerChildren !== null){
	        	var startEvent = (fnb.hyperion.controller.isMobile==true) ? 'touchstart' : 'mousedown';
	        	
	        	fnb.hyperion.delegate.on(this.scrollableParent.elem, startEvent, this.scrollerChildren, start);
	        }
	    };
	};
	
	//Namespace horizontalScroller
	fnb.namespace('horizontalScroller', horizontalScroller);
})();


///-------------------------------------------///
/// developer: Donovan
///
/// fnb.hyperion.horizontalScroller.slide Object
///-------------------------------------------///
/*USAGE:
//Move element x and y
	fnb.hyperion.horizontalScroller.slide(document.getElementById("some-div")).move({x:(-1*newloc)}, this.scrollSpeed, function() {setTimeout(function() {if(_this.afterStop) _this.afterStop(index);parent.moving = false;}, 200);});
*/
(function() {
	var animateId = 0,
	animateInstances = {};
	//Get element absolute position
	function getPosition(element) {
		
		var	position = {x:element.offsetLeft, y:element.offsetTop};

		return position;
	}
	//Set interval function
	function createInterval(element, interval, speed, start, position, style, tmp, callback) {
		clearInterval(element.fx[interval]);
		if(start[style[0]]!=position[style[0]]){
			element.fx[interval] = setInterval(function(){
				start[style[0]] += (position[style[0]] - start[style[0]]) * speed;
				start[style[1]] += (position[style[1]] - start[style[1]]) * speed;
				fnb.hyperion.horizontalScroller.slide[tmp](element,start);
				if(start[style[0]]&&start[style[1]]){
					if(Math.round(start[style[0]]) == position[style[0]] && Math.round(start[style[1]]) == position[style[1]]){
						fnb.hyperion.horizontalScroller.slide[tmp](element,position);
						callCallback(element, interval, callback);
					};
				}else if(!start[style[0]]&&start[style[1]]){
					if(Math.round(start[style[1]]) == position[style[1]]){
						fnb.hyperion.horizontalScroller.slide[tmp](element,position);
						callCallback(element, interval, callback);
					};
				}else if(start[style[0]]&&!start[style[1]]){
					if(Math.round(start[style[0]]) == position[style[0]]){
						fnb.hyperion.horizontalScroller.slide[tmp](element,position);
						callCallback(element, interval, callback);
					};
				}
				
			}, 1);
		}
		
	}
	//Add animation data to element
	function wrap(element) {
		if(!element.fx)
			element.fx = {move:0};
		return element;
	}
	//Call with element as scope on event complete
	function callCallback(element, interval, callback) {
		clearInterval(element.fx[interval]);
		if(callback) callback.call(element);
	}
	//Get end
	function end(x, y, speed){
		return x < y ? min(x + speed, y) : max(x - speed, y);
	};
	///-------------------------------------------///
	/// Slide Parent function
	///-------------------------------------------///
	function slide(element, id) {
		if (!(this instanceof fnb.hyperion.horizontalScroller.slide)) {
          for (var key in animateInstances) {
              if (animateInstances[key].element === element) {
                  return animateInstances[key];
              }
          }
          animateId++;
          animateInstances[animateId] = new fnb.hyperion.horizontalScroller.slide(element, animateId);

          return animateInstances[animateId];
      }

      this.element = wrap(element);
      this.id = id;
	};
	///-------------------------------------------///
	/// Slide Methods
	///-------------------------------------------///
	slide.prototype = {
		//Move animation x & y
		move: function ( position, speed, callback) {
			var	start = getPosition(this.element);
			createInterval(this.element, "move", speed / 100, start, position, ["x", "y"], "position", callback);
		},
		//Stop specific animation
		stop: function () {
			var	interval = ["move"],
			index = interval.length;
			while(index--)
				clearInterval(this.element.fx[interval[index]]);
		},
		destroy: function () {
			animateInstances = {};
		}
	};
	//Public Animation position method
	slide.position = function (element,position) {
		var currentElementPosAttr = window.getComputedStyle(element,null).getPropertyValue("position");
		var	style = wrap(element).style;
		style.position = (currentElementPosAttr=='relative') ? "relative" : "absolute";
		if(position.x) style.left = position.x + "px";
		if(position.y) style.top = position.y + "px";
		
	};
	//Add a public method
	slide.wrap = wrap;
	//Namespace horizontalScroller.slide
	fnb.namespace('horizontalScroller.slide', slide);

})();
