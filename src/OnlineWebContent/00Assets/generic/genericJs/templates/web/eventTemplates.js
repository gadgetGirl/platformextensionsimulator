///-------------------------------------------///
/// developer: Donovan
///
/// APP Event Templates Object
///-------------------------------------------///
fnb.hyperion.appTemplates = {
	'historyBack' : [ 
	        {
				'callBack' : 'window.history.back()',
				'params' : 'sender, loadObj'
			}],
	'loadApp' : [ 
	        {
	        	'callBack' : 'fnb.hyperion.controller.raiseEvent("loadIntoPage",loadObj)',
	        	'params' : 'sender, loadObj'
			}],
   'callmeback' : [ 
            {
            	'callBack' : 'fnb.hyperion.controller.raiseEvent("loadPage",loadObj)',
            	'params' : 'sender, loadObj'
			}],
	'apply' : [
	        {
	        	'callBack' : 'fnb.hyperion.controller.redirect(loadObj)',
	        	'params' : 'sender, loadObj'
			}],
	'validateAndSubmit' : [ 
	        {
	        	'callBack' : 'fnb.hyperion.forms.validateAndSubmitForm(loadObj)',
				'params' : 'sender, loadObj'
			}],
	'clear' : [ 
	   	    {
	   	        'callBack' : 'fnb.hyperion.forms.clearAndSubmitForm(loadObj)',
	   			'params' : 'sender, loadObj'
	   	    }],			
	'submit' : [ 
            {
            	'callBack' : 'fnb.hyperion.controller.ajax("submit.submitTarget",sender,loadObj)',
            	'params' : 'sender, loadObj'
          
            },
			{
            	'callBack' : 'fnb.hyperion.controller.raiseEvent("showOverlay")'
			}, 
			{
				'callBack' : 'fnb.hyperion.progress.start()'
			}],
    'submitSuccess' : [ 
	        {
	        	'callBack' : 'fnb.hyperion.controller.ajax("loadPage.success", sender,loadObj)',
	        	'params' : 'sender, loadObj'
			}],
	'submitReady' : [
 			{
 				'callBack' : 'fnb.hyperion.controller.raiseEvent("hideOverlay")'
 			},
 			{
 				'callBack' : 'fnb.hyperion.progress.stop()'
 			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			}],
	'submitError' : [ 
  	        {
  				'callBack' : 'fnb.hyperion.controller.error("Error",loadObj)',
  				'params' : 'sender, loadObj'
  			}, 
  	        {
  				'callBack' : 'fnb.hyperion.controller.raiseEvent("hideOverlay")'
  			},
  			{
  				'callBack' : 'fnb.hyperion.progress.stop()'
  			}],
	'loadBanner' : [
			{
				'callBack' : 'fnb.hyperion.controller.ajax("loadPage.load",sender,loadObj)',
				'params' : 'sender, loadObj'
			}],
	'loadBannerSuccess' : [
			{
			     'callBack' : 'fnb.hyperion.controller.ajax("loadIntoPage.success",sender,loadObj)',
			     'params' : 'sender, loadObj'
			}],
	'loadBannerReady' : [
		    {
			     'callBack' : 'fnb.hyperion.bannerController(loadObj.bannerManagerId).createTimeOut()',
			     'params' : 'sender, loadObj'
			}],
	'sendImpressions' : [
	        {
				'callBack' : 'fnb.hyperion.bannerControllerManager.sendImpressions(impressionList)',
				'params' : 'sender, impressionList'
	        }],
	'ajaxSend' : [
			{
				'callBack' : 'fnb.hyperion.controller.ajax("send.submit",loadObj)',
				'params' : 'sender, loadObj'
			}]
			
};	
