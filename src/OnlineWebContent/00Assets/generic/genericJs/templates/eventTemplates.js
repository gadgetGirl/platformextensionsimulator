///-------------------------------------------///
/// developer: Donovan
///
/// Event Templates Object
///-------------------------------------------///
fnb.hyperion.eventTemplates = {
	'loadSite' : [
	 			{
	 				'callBack' : 'fnb.hyperion.utils.topTabs.init();'
	 			}],
	'loadUrl' : [
			{
				'callBack' : 'fnb.hyperion.controller.ajax("loadPage.load", sender , loadObj)',
				'params' : 'sender, loadObj'
			}],
	'loadUrlSuccess' : [ 
	        {
	        	'callBack' : 'fnb.hyperion.controller.ajax("loadPage.success", sender,loadObj)',
	        	'params' : 'sender, loadObj'
			}],
	'loadPage' : [
	         {
	        	 'callBack' : 'fnb.hyperion.controller.raiseEvent("hideError")'	        	 
	         },
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
			},
	        {
			   	'callBack' : 'fnb.hyperion.controller.raiseEvent("hideEzi")'
			 },
			 {
			   	'callBack' : 'fnb.hyperion.controller.raiseEvent("hideActionMenu")'
			 },
			 {
			    'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
			 },
			 {
			    'callBack' : 'fnb.hyperion.controller.raiseEvent("hideSubMenu")'
			 }, 
			 {
			    'callBack' : 'fnb.hyperion.controller.raiseEvent("showOverlay")'
			 }, 
			 {
				'callBack' : 'fnb.hyperion.progress.start()'
			 }, 
			 {
				'callBack' : 'fnb.hyperion.controller.ajax("loadPage.load", sender , loadObj)',
				'params' : 'sender, loadObj'
			 }],
	'loadPageSuccess' : [
	         {
	        	 'callBack' : 'fnb.hyperion.controller.ajax("loadPage.success", sender, loadObj)',
	        	 'params' : 'sender, loadObj'
	         },
	         {
	        	 'callBack' : 'fnb.hyperion.controller.updateUrl(loadObj);',
	        	 'params' : 'sender, loadObj'
	         }],
	'loadPageReady' : [
			{
				'callBack' : 'fnb.hyperion.controller.raiseEvent("hideOverlay")'
			},
			{
				'callBack' : 'fnb.hyperion.progress.stop()'
			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			}],
	'loadPageError' : [
	        {
	             'callBack' : 'fnb.hyperion.controller.raiseEvent("hideOverlay")'
	        },
	        {
	             'callBack' : 'fnb.hyperion.progress.stop()'
	        },
	        {
	        	'callBack' : 'fnb.hyperion.controller.error("Error",Error)',
	        	'params'   : 'sender, Error'
	        }],
	'loadIntoPage' : [
 			 {
 				'callBack' : 'fnb.hyperion.controller.ajax("loadPage.load", sender , loadObj)',
 				'params' : 'sender, loadObj'
 			 }],
 	'loadIntoPageSuccess' : [
 	         {
 	        	 'callBack' : 'fnb.hyperion.controller.ajax("loadIntoPage.success", sender, loadObj)',
 	        	 'params' : 'sender, loadObj'
 	         }],
	'topMenuLoadUrl' : [
			{
				'callBack' : 'fnb.hyperion.controller.raiseEvent("loadUrl",loadObj)',
				'params' : 'sender, loadObj'
			}],
	'showDatePicker' : [ 
	        {
				'callBack' : 'fnb.hyperion.forms.datePicker.show(loadObj)',
				'params' : 'sender, loadObj'
			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
			}],
	'hideDatePicker' : [ 
	        {
				'callBack' : 'fnb.hyperion.forms.datePicker.hide()',
				'params' : 'sender, loadObj'
			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			}],
	'hideOverlay' : [ 
	        {
	            'callBack' : 'fnb.hyperion.utils.overlay.hide()'
			}],
	'showOverlay' : [ 
	        {
	        	'callBack' : 'fnb.hyperion.utils.overlay.show()'
			}],
	'hideActionMenu' : [ 
	        {
	        	'callBack' : 'fnb.hyperion.utils.actionMenu.hide()'
			},
	        {
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			}],
	'hideActionMenuButtonAndClear' : [ 
 	        {
 				'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
 			}],
	'showActionMenu' : [
	        {
				'callBack':'fnb.hyperion.controller.raiseEvent("pageHideEzi")'
			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
			},
			{
				'callBack':'fnb.hyperion.utils.actionMenu.show()'
			}],
	'pageHideEzi' : [
			{
				'callBack' : 'fnb.hyperion.controller.raiseEvent("hideEzi")'
			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			}],
	'hideEzi' : [
    		{
 				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.eziFooterButtonGroup)'
 			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			},
 			{
 				'callBack' : 'fnb.hyperion.utils.eziPanel.hide()'
 			}],
	'hideEziResetPage' : [
			{
				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.eziFooterButtonGroup)'
			},
			{
				'callBack' : 'fnb.hyperion.utils.eziPanel.hide()'
			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			}],
	'loadEzi' : [
			{
				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
			},
			{
				'callBack' : 'fnb.hyperion.controller.raiseEvent("loadIntoEzi",loadObj)',
				'params' : 'sender, loadObj'
			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.eziFooterButtonGroup)'
			}, 
			{
				'callBack' : 'fnb.hyperion.utils.eziPanel.show()'
			}],
    'loadIntoEzi' : [
			{
 				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.eziFooterButtonGroup)'
 			},
 			{
				'callBack' : 'fnb.hyperion.controller.eziPageContentElement.hide()'
			},    
			{
				'callBack' : 'fnb.hyperion.progress.startEziLoader()'
			},
			{
			    'callBack' : 'fnb.hyperion.controller.ajax("loadEzi.load", sender , loadObj)','params' : 'sender, loadObj'
		    }],
    'loadIntoEziSuccess' : [
			{
			    'callBack' : 'fnb.hyperion.controller.ajax("loadIntoPage.success", sender,loadObj)','params' : 'sender, loadObj'
            }],
	'loadIntoEziReady' : [
	        {
			    'callBack' : 'fnb.hyperion.progress.stop()'
			},
			{
				'callBack' : 'fnb.hyperion.controller.eziPageContentElement.show()'
			},
			{
 				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.eziFooterButtonGroup)'
 			}],				     			       		                  
	'Error' : [
			{
				'callBack' : 'fnb.hyperion.controller.raiseEvent("hideOverlay")'
			},
			{
				'callBack' : 'fnb.hyperion.progress.stop()'
			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			},
			{
				'callBack' : 'fnb.hyperion.utils.error.show(Error)',
				'params' : 'sender, Error'
			}],				     			       		                  
	'EziError' : [
			{
				'callBack' : 'fnb.hyperion.progress.stop()'
			},
			{
				'callBack' : 'fnb.hyperion.controller.eziPageContentElement.show()'
		    },
		    {
		    	'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.eziFooterButtonGroup)'
		    },
	        {
				'callBack' : 'fnb.hyperion.utils.error.show(Error)',
				'params' : 'sender, Error'
			}],
	'hideError' :
			 [
			 {
			    'callBack' : 'fnb.hyperion.utils.error.hide()'
			 }],
	'showHideSubMenu' : [ 
 	        {
 	        	'callBack' : 'fnb.hyperion.controller.raiseEvent("hideError")'	        	 
 	    	},
 			{
 				'callBack' : 'fnb.hyperion.controller.raiseEvent("hideOverlay")'
 			},
 			{
 				'callBack' : 'fnb.hyperion.utils.subMenu.select(event)',
 				'params' : 'sender, event'
 			}],
 	'showSubMenu' : [
 			{
 				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
 			},
  	        {
  	        	'callBack' : 'fnb.hyperion.utils.subMenu.show();'
  			}],
 	'hideSubMenu' : [
 			{
 				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
 			},
  	        {
  	        	'callBack' : 'fnb.hyperion.utils.subMenu.hide();'
  			}],
  	'postToUrl' : [
  			{
 				'callBack' : 'fnb.hyperion.controller.ajax("post.send", sender , loadObj)',
 				'params' : 'sender, loadObj'
 			}],
  	'postToUrlSuccess' : [
  			{
 				'callBack' : 'fnb.hyperion.functions.callback.call(loadObj.postLoadingCallback, loadObj);',
 				'params' : 'sender, loadObj'
 			}]
};	
