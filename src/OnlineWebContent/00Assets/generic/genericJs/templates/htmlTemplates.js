///-------------------------------------------///
/// developer: Donovan
///
/// Templates Object
///-------------------------------------------///
fnb.hyperion.htmlTemplates = {};

/*Developer: Richard
Template for footerContactInfo
This is for the contactInfomation that links to a site such as iContract
*/
//PARAMS:
//label : Label of the button
//event : Type of event to be bound
//action : Action to be performed on event
fnb.hyperion.htmlTemplates['footerContactInfo'] = [
          		'<a id="{{id}}" type="button" class="footerContactInfo" data-settings=\'[{\"event\": \"{{event}}\",\"url\": \"{{url}}\",\"target\": \"{{target}}\", \"urlTarget\": \"{{targetElement}}\",\"dataTarget\": \"{{dataTarget}}\", \"clearHtmlTemplates\": {{clearHtmlTemplates}}, \"clearPageModuleObject\": {{clearPageModuleObject}}, \"clearPageEventsArray\": {{clearPageEventsArray}}, \"clearPageTemplatesArray\": {{clearPageTemplatesArray}}}]\'>',
          		'<span class="footerContactInfoLabel">{{label}}</span>', '</a>' ].join("\n");

/*Developer: Richard
Template for footerContactInfoSimple*/
//PARAMS:
//label : Label of the button
//event : Type of event to be bound
//action : Action to be performed on event
fnb.hyperion.htmlTemplates['footerContactInfoSimple'] = [
                                                   '<span class="footerContactInfo" data-settings=\'[{\"clearHtmlTemplates\": {{clearHtmlTemplates}}, \"clearPageModuleObject\": {{clearPageModuleObject}}, \"clearPageEventsArray\": {{clearPageEventsArray}}, \"clearPageTemplatesArray\": {{clearPageTemplatesArray}}}]\'>',
                                                   '<span class="footerContactInfoLabel">{{label}}</span>', '</span>' ].join("\n");

/*Developer: Donovan
Template for footerButton*/
//PARAMS:
//label : Label of the button
//event : Type of event to be bound
//action : Action to be performed on event
fnb.hyperion.htmlTemplates['footerButton'] = [
                                              '<a id="{{id}}" type="button" class="footerButton" data-settings=\'[{\"event\": \"{{event}}\",\"url\": \"{{url}}\",\"target\": \"{{target}}\", \"urlTarget\": \"{{targetElement}}\",\"dataTarget\": \"{{dataTarget}}\", \"clearHtmlTemplates\": {{clearHtmlTemplates}}, \"clearPageModuleObject\": {{clearPageModuleObject}}, \"clearPageEventsArray\": {{clearPageEventsArray}}, \"clearPageTemplatesArray\": {{clearPageTemplatesArray}}, \"preLoadingCallback\": \"{{preLoadingCallback}}\", \"postLoadingCallback\": \"{{postLoadingCallback}}\", \"onClick\": \"{{onClick}}\"}]\'>',
                                              '<span class="footerButtonLabel">{{label}}</span>', '</a>' ].join("\n");

/*Developer: Donovan
Template for ezi panels in old banking*/
//PARAMS:
//label : Label of the button
//event : Type of event to be bound
//action : Action to be performed on event
fnb.hyperion.htmlTemplates['footerButton'] = [
                                              '<a id="{{id}}" type="button" class="footerButton" data-settings=\'[{\"event\": \"{{event}}\",\"url\": \"{{url}}\",\"target\": \"{{target}}\", \"urlTarget\": \"{{targetElement}}\",\"dataTarget\": \"{{dataTarget}}\", \"clearHtmlTemplates\": {{clearHtmlTemplates}}, \"clearPageModuleObject\": {{clearPageModuleObject}}, \"clearPageEventsArray\": {{clearPageEventsArray}}, \"clearPageTemplatesArray\": {{clearPageTemplatesArray}}, \"preLoadingCallback\": \"{{preLoadingCallback}}\", \"postLoadingCallback\": \"{{postLoadingCallback}}\", \"onClick\": \"{{onClick}}\"}]\'>',
                                              '<span class="footerButtonLabel">{{label}}</span>', '</a>' ].join("\n");

/*
 * Developer: Donovan Template for datePicker
 */
// MonthWrapper
// PARAMS:
// monthName : Month name header
// year : Year header
// row : Each row for the month wrapper
fnb.hyperion.htmlTemplates['datePickerMonthWrapper'] = [
	"<div class='datePickerMonthWrapper' data-itemCount='{{count}}' data-month='{{month}}' data-year='{{year}}'>",
		"<div>",
			"<div class='datePickerTitle'><span class='datePickerMonth'>{{monthName}}</span>&nbsp;<span class='datePickerYear'>{{year}}</span></div>",
		"</div>",
		"<table class='datePickerCalendar'>",
			"<thead>",
				"<tr>",
					"<th class='datePickerWeekEndLabel'><div class='dateHeaderLabelWrapper'>S</div></th>",
					"<th class='datePickerDayLabel'><div class='dateHeaderLabelWrapper'>M</div></th>",
					"<th class='datePickerDayLabel'><div class='dateHeaderLabelWrapper'>T</div></th>",
					"<th class='datePickerDayLabel'><div class='dateHeaderLabelWrapper'>W</div></th>",
					"<th class='datePickerDayLabel'><div class='dateHeaderLabelWrapper'>T</div></th>",
					"<th class='datePickerDayLabel'><div class='dateHeaderLabelWrapper'>F</div></th>",
					"<th class='datePickerWeekEndLabel'><div class='dateHeaderLabelWrapper'>S</div></th>",
				"</tr>",
			"</thead>",
			"<tbody>",
				"{{rows}}",
			"</tbody>",
		"</table>",
	"</div>"
].join("\n");
/*Developer: Donovan
Template for DatePicker item*/
//PARAMS:
//month : Current month
//year : Current month
//day : Current day of the week
//date : Current date
//dataActive : Is date active
fnb.hyperion.htmlTemplates['datePickerItem'] = [
	"<td class='datePickerDate' data-month='{{month}}' data-year='{{year}}' data-date='{{date}}' data-dayName='{{dayName}}' data-monthName='{{monthName}}' data-active='{{dataActive}}' data-selected='{{dataSelected}}'><div data-role='dateLabelWrapper'>{{date}}</div></td>"
].join("\n");

/*Developer: Donovan
Template for error messages*/
//PARAMS:
//message : Main error message
//errors : list of error messages
fnb.hyperion.htmlTemplates['errorMessage'] = [
                                              '<div data-role="errorMessage">{{message}}</div>',
                                              '<div data-role="errors">{{errors}}</div>'].join("\n");