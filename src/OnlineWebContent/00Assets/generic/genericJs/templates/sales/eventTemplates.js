///-------------------------------------------///
/// developer: Donovan
///
/// APP Event Templates Object
///-------------------------------------------///
fnb.hyperion.appTemplates = {
	'siteLoaded' : [
 			{
 				'callBack' : 'fnb.hyperion.controller.initBrowserExtentions();'
 			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			}
 			
 			],
	'loadEzi' : [
 			{
 				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
 			},
 			{
 				'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
 			},
 			{
 				'callBack' : 'fnb.hyperion.controller.raiseEvent("loadIntoEzi",loadObj)',
 				'params' : 'sender, loadObj'
 			},
 			{
 				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.eziFooterButtonGroup)'
 			}, 
 			{
 				'callBack' : 'fnb.hyperion.utils.eziPanel.show()'
 			}],
	'pageHideEzi' : [
			{
				'callBack' : 'fnb.hyperion.controller.raiseEvent("hideEzi")'
			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			}],
	'hideEzi' : [
   			{
				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.eziFooterButtonGroup)'
			},
			{
				'callBack' : 'fnb.hyperion.utils.eziPanel.hide()'
			}],
	'loadPage' : [
	        {
			    'callBack' : 'fnb.hyperion.utils.notifications.hide()'
			},
			{
	        	 'callBack' : '_datePicker.hide(false,false)'	        	 
	        },
	        {
	        	 'callBack' : 'fnb.hyperion.controller.raiseEvent("hideError")'	        	 
	        },               
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
			},           
	        {
				'callBack' : 'fnb.functions.footer.clear()'
			},
	        {
			   	'callBack' : 'fnb.hyperion.controller.raiseEvent("hideEzi")'
			},
			{
			   	'callBack' : 'fnb.hyperion.controller.raiseEvent("hideActionMenu")'
			},
			{
			    'callBack' : 'fnb.hyperion.utils.footer.hide()'
			},
			{
			    'callBack' : 'fnb.hyperion.controller.raiseEvent("showOverlay")'
			}, 
			{
				'callBack' : 'fnb.hyperion.progress.start()'
			},
			{
				'callBack' : 'fnb.hyperion.controller.ajax("loadPage.load", sender , loadObj)',
				'params' : 'sender, loadObj'
			}],
	'loadPageSuccess' : [
			 {
	        	 'callBack' : 'fnb.hyperion.controller.ajax("loadPage.success", sender,loadObj)',
	        	 'params' : 'sender, loadObj'
	         }],
	'loadPageReady' : [
	         {
			    'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			 },
			 {
				'callBack' : 'fnb.hyperion.controller.raiseEvent("hideOverlay")'
			 },
			 {
				'callBack' : 'fnb.hyperion.progress.stop()'
			 },
			 {
 				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
 			 }],
	'loadPageError' : [
	         {
			    'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			 },
			 {
	             'callBack' : 'fnb.hyperion.controller.raiseEvent("hideOverlay")'
	       	 },
	         {
	             'callBack' : 'fnb.hyperion.progress.stop()'
	         },
	         {
	        	'callBack' : 'fnb.hyperion.controller.error("Error",Error)',
	        	'params'   : 'sender, Error'
	         }],
	 'loadPopup' : [
 			{
 				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
 			},
 	        {
 	        	 'callBack' : 'fnb.hyperion.controller.raiseEvent("hideError")'	        	 
 	        },               
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
			},
 			{
 			    'callBack' : 'fnb.hyperion.utils.notifications.show()'
 			},
 			{
 				'callBack' : 'fnb.hyperion.controller.ajax("loadPopup.load", sender , loadObj)',
 				'params' : 'sender, loadObj'
 			}],
	'loadPopupSuccess' : [
   	         {
   	        	 'callBack' : 'fnb.hyperion.controller.ajax("loadPopup.success", sender,loadObj)',
   	        	 'params' : 'sender, loadObj'
   	         }],
	'popupHide' : [
			{
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
			},
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			},		
			{
			    'callBack' : 'fnb.hyperion.utils.notifications.hide()'
			}],
	'submitFromPage' : [
			{
	        	 'callBack' : '_datePicker.hide(false,false)'	        	 
	        },
	        {
	        	 'callBack' : 'fnb.hyperion.controller.raiseEvent("hideError")'	        	 
	        },
	        {
			   	'callBack' : 'fnb.hyperion.controller.raiseEvent("hideEzi")'
			},
			{
			   	'callBack' : 'fnb.hyperion.controller.raiseEvent("hideActionMenu")'
			},
 			{
 				'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
 			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
			},
			{
			    'callBack' : 'fnb.hyperion.controller.raiseEvent("showOverlay")'
			}, 
			{
				'callBack' : 'fnb.hyperion.progress.start()'
			},
			{
				'callBack' : 'fnb.hyperion.controller.ajax("submitFromPage.submit", sender , loadObj)',
				'params' : 'sender, loadObj'
			}],
	'submitFromPageSuccess' : [         
   			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
			},
	        {
	        	 'callBack' : 'fnb.hyperion.controller.ajax("submitFromPage.success", sender,loadObj)',
	        	 'params' : 'sender, loadObj'
	        }],
	'submitFromPageReady' : [
 	         {
 			    'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
 			 },
 			 {
 				'callBack' : 'fnb.hyperion.controller.raiseEvent("hideOverlay")'
 			 },
 			 {
 				'callBack' : 'fnb.hyperion.progress.stop()'
 			 },
			 {
 				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
 			 }],
	'submitFromPageError' : [
			{
				'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
			}],
		
	'submitFromEziToEzi' : [			       
			{
 				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.eziFooterButtonGroup)'
 			},
 			{
				'callBack' : 'fnb.hyperion.controller.eziPageContentElement.hide()'
			},
			{
				'callBack' : 'fnb.hyperion.progress.startEziLoader()'
			},
			{
				'callBack' : 'fnb.hyperion.controller.ajax("submitFromEziToEzi.submit", sender , loadObj)',
				'params' : 'sender, loadObj'
			},
 			{
 				'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.eziFooterButtonGroup)'
 			}],
	'submitFromEziToEziSuccess' : [         
   	      {
        	 'callBack' : 'fnb.hyperion.controller.ajax("submitFromEziToEzi.success", sender,loadObj)',
        	 'params' : 'sender, loadObj'
          }],
	'submitFromEziToEziSuccessError': [         
	      {
	        	 'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
	      },
		{
			'callBack' : 'fnb.hyperion.controller.eziPageContentElement.show()'
		},
	      {
	        	 'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.eziFooterButtonGroup)'
	      }],
	'submitFromEziToEziReady' : [
			{
                 'callBack' : 'fnb.hyperion.progress.stop()'
			},
 			{
				'callBack' : 'fnb.hyperion.controller.eziPageContentElement.show()'
			},
			{
				'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.eziFooterButtonGroup)'
			}],
	'asyncLoadContent' : [
   			{
   				'callBack' : 'fnb.hyperion.controller.ajax("asyncLoadContent.load", sender , loadObj)',
   				'params' : 'sender, loadObj'
   			}],
   	'asyncLoadContentSuccess' : [
   	         {
   	        	 'callBack' : 'fnb.hyperion.controller.ajax("asyncLoadContent.success", sender,loadObj)',
   	        	 'params' : 'sender, loadObj'
   	         }],
 	'showTimeOut' : [
			 {
				 'callBack' : 'fnb.hyperion.controller.raiseEvent("hideEzi")'
			 },
 	    	 {
	        	 'callBack' : '_datePicker.hide(false,false)'	        	 
	         },
	         {
 	   			'callBack' : 'fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'
 	   		 },
 	    	 {
	   			'callBack' : 'fnb.hyperion.utils.actionMenu.hideButton()'
 	    	 },
 	         {
			  	 'callBack' : 'fnb.hyperion.controller.disbaleTopMenu();',
			  	 'params' : 'sender, loadObj'
			 },
   	         {
   	        	 'callBack' : 'fnb.hyperion.utils.timeOut.show();',
   	        	 'params' : 'sender, loadObj'
   	         }],
   	'hideTimeOut' : [
	    	 {
	   			'callBack' : 'fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'
	   		 },
	    	 {
	   			'callBack' : 'fnb.hyperion.utils.actionMenu.showButton()'
	    	 },
  	         {
			  	 'callBack' : 'fnb.hyperion.controller.enableTopMenu();',
			  	 'params' : 'sender, loadObj'
			 },
   	         {
   	        	 'callBack' : 'fnb.hyperion.utils.timeOut.hide();',
   	        	 'params' : 'sender, loadObj'
   	         }],
 	'logOffTimeOut' : [
   	         {
   	        	 'callBack' : 'fnb.hyperion.utils.timeOut.hide();',
   	        	 'params' : 'sender, loadObj'
   	         },
   	         {
   	        	 'callBack' : 'fnb.hyperion.utils.timeOut.logOff();',
   	        	 'params' : 'sender, loadObj'
   	         }]
};
