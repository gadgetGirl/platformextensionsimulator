///-------------------------------------------///
/// developer: Richard
///
/// Override web defaults
///-------------------------------------------///
fnb.hyperion.appTemplates['loadPage'] = [
         {
        	 'callBack' : 'fnb.hyperion.controller.pageContentContainerElement.show()',
        	 'params' : 'sender, loadObj'
         },
         {
        	 'callBack' : 'fnb.hyperion.utils.headerButtonMenu.hide()',
        	 'params' : 'sender, loadObj'
         },
		 {
			'callBack' : 'fnb.hyperion.utils.sideMenu.hide()',
			'params' : 'sender, loadObj'
		 },
		 {
			'callBack' : 'fnb.hyperion.controller.ajax("loadPage.load", sender , loadObj)',
			'params' : 'sender, loadObj'
		 }];

fnb.hyperion.appTemplates['loadPageReady'] = [];

fnb.hyperion.appTemplates['submit'] = [ 
        {
        	'callBack' : 'fnb.hyperion.controller.ajax("submit.submitTarget",sender,loadObj)',
        	'params' : 'sender, loadObj'
      
        }];

fnb.hyperion.appTemplates['submitReady'] = [];