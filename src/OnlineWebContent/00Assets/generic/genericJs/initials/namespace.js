///Debug mode var
var debug = (window.location.port == '8080') ? true : false;
///-------------------------------------------///
/// developer: Donovan
///
/// Namespace object
///-------------------------------------------///
(function() {
	//Project namespace
	var projectNamespace = "fnb.hyperion";
	//Namespace functions
	function namespace(namespace,handler,newInstance) {
		//Create full namespace
		namespace = projectNamespace + '.' + namespace;
	    //Split namespace string into parts
		var parts = namespace.split('.'),
		//Get window
	    parent = window,
	    //Var for active namespace part
	    currentPart = '';
	    //Loop parts and create namespace
        for(var i = 0, length = parts.length; i < length; i++) {
        	//Get part
            currentPart = parts[i];
            //Test if part exists
            parent[currentPart] = parent[currentPart] || {};
            //Add parts to namespace
            if((i+1)==parts.length){
                parent[currentPart] = handler;
                if(newInstance){
                	parent[currentPart] = new parent[currentPart]();
                }
            }else{
                parent = parent[currentPart];
            }
        }
        //Return window with namespace
        return parent;
	};
	//Namespace this object
	namespace('namespace', namespace);
	//Create quick reference to namespace
	fnb.namespace = namespace;
	
})();

///-------------------------------------------///
/// developer: Donovan
///
/// Logging Object
///-------------------------------------------///
(function() {
	//SHIM ALL BROWSERS TO LOG TO CONSOLE IF IN DEBUG MODE
	if(debug){
		//IE9 built-in console
		if (Function.prototype.bind && /^object$|^function$/.test(typeof console) && typeof console.log === 'object' && typeof window.addEventListener === 'function') {
		    //Loop logging types and call
			['log', 'info', 'warn', 'error', 'assert', 'dir', 'clear', 'profile', 'profileEnd'].forEach(function(method) {
		    	console[method] = this.call(console[method], console);
		    }, Function.prototype.bind);
		};
		
		// Test for log in window
		if (!window.log) {
			
			//Create window.log
		    window.log = function() {
		    	
		        var args = arguments;
		        var isIECompatibilityView = false;
		        var i;
		        var sliced;
		        
		        // Test if the browser is IE8
		        var isIE8 = function _isIE8() {
		        	return (!Function.prototype.bind || (Function.prototype.bind && typeof window.addEventListener === 'undefined')) &&
		                    typeof console === 'object' &&
		                    typeof console.log === 'object';
		        };

		        // Store logs to an array
		        log.history = log.history || [];
		        log.history.push(arguments);

		        // Check for IE10 in some different mode
		        if (log.detailPrint && log.needsDetailPrint) {
		            (function() {
		            	//Get UA string
		                var ua = navigator.userAgent,
		                    winRegexp = /Windows\sNT\s(\d+\.\d+)/;

		                // Test UA string for accurate IE
		                if (console && console.log && /MSIE\s(\d+)/.test(ua) && winRegexp.test(ua)) {
		                    if (parseFloat(winRegexp.exec(ua)[1]) >= 6.1) {
		                        isIECompatibilityView = true;
		                    }
		                }
		                
		            }());
		        }

		        // Browser with a console
		        if (isIECompatibilityView || typeof console.log === 'function') {
		            sliced = Array.prototype.slice.call(args);

		            // Test for detailPrint plugin
		            if (log.detailPrint && log.needsDetailPrint) {
		                // List separator
		                console.log('-----------------');
		                args = log.detailPrint(args);
		                i = 0;

		                while (i < args.length) {
		                    console.log(args[i]);
		                    i++;
		                }
		            }
		            // Log String
		            else if (sliced.length === 1 && typeof sliced[0] === 'string') {
		                console.log(sliced.toString());
		            }
		            else {
		                console.log(sliced);
		            }
		        }
		        // IE8
		        else if (isIE8()) {
		            if (log.detailPrint) {
		                // Prettify arguments
		                args = log.detailPrint(args);

		                // List separator
		                args.unshift('-----------------');

		                // Loop and log
		                i = 0;
		                while (i < args.length) {
		                    Function.prototype.call.call(console.log, console, Array.prototype.slice.call([args[i]]));
		                    i++;
		                }
		            }
		            else {
		                Function.prototype.call.call(console.log, console, Array.prototype.slice.call(args));
		            }
		        };
		    };
		}
	}else{
		//Store console
		if(window.console) window.debug = window.console || undefined;
		//If not debug mode or no support disable console
		window.console = {
			log: function(message){
			},
			info: function(message){
			},
			error: function(message){
			},
			clear: function(message){
			},
			dir: function(message){
			},
			assert: function(message){
			},
			table: function(message){
			}
		};
	}
	
	///-------------------------------------------///
	/// Collect Window Errors
	///-------------------------------------------///
	window.onerror = function(message, filename, lineno, colno, error) {
		
		//Inform logger of error
		var errorObject = {message: message, filename: filename, lineno: lineno, colno: colno, error: error};
		/*Test for debug mode*/
		if(debug){
			
			console.info("JavaScript Error : " + errorObject.message + " on line " + errorObject.lineno + " for " + errorObject.filename);
			
			/*Developer: Donovan - For modern browsers
			Stack Trace*/
			
			try{
				console.info("Error Stack Trace : %s", errorObject.error.stack);
			}catch(e){
				console.info('No stack trace support.');
			}
		}
		
		//Make log request
		try{
			fnb.hyperion.log.log(errorObj);
		}catch(e){
			if(debug) console.info('FNB Logger not available.');
		}

		return true;
	};
	///-------------------------------------------///
	/// Logging Parent function
	///-------------------------------------------///
	function log() {

	};
	///-------------------------------------------///
	/// Logging Methods
	///-------------------------------------------///
	log.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Logging navigator url
		navigator: 'testingUrl',
		//Init FNB Logging
		init: function () {
			console.log('Utils Logging init');
	    },
	    //Get error info for log
	    log: function (errorObject) {
      	
      	
	    },
	    //Make request to the navigator
	    request: function (event) {

	    },
	    //Remove current object from dom
	    destroy: function () {
	    	fnb.hyperion.log = {};
	    }
	};

	//Namespace log
	fnb.namespace('log', log, true);

})();