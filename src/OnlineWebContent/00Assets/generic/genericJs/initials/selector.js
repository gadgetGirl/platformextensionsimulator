///-------------------------------------------///
/// developer: Donovan
///
/// Selector Object
///-------------------------------------------///
/*USAGE:
	Select element
		--By id
		fnb.hyperion.$('#id');
		--By class name
		fnb.hyperion.$('.class');
		--By element name
		fnb.hyperion.$('@name');
		--By tag name
		fnb.hyperion.$('=nav');
		--By query selector
		fnb.hyperion.$('*div div.inside');
	
	Find element
		fnb.hyperion.$('#id').find('.inside');
	Each loop
		fnb.hyperion.$('.inside').each(function(element){

		});
	Set attribute
		fnb.hyperion.$('#id').attr('prop', 'value');
	Get attribute
		fnb.hyperion.$('#id').attr('data-role');
	Set css
		fnb.hyperion.$('#id').css('left', '10px');
	Add class
		fnb.hyperion.$('#id').addClass('name');
	Remove class
		fnb.hyperion.$('#id').removeClass('name');
	Has class
		fnb.hyperion.$('#id').hasClass('name');
	Toggle class
		fnb.hyperion.$('#id').toggleClass('name');
	Returns first element with class
		fnb.hyperion.$('#id').find('.inside').first();
	Returns last element with class
		fnb.hyperion.$('#id').find('.inside').last();
	Returns parent element
		fnb.hyperion.$('#id').parent();
	Returns Outerheight of an element
		fnb.hyperion.$('#id').outerHeight();
	Returns Outerwidth of an element
		fnb.hyperion.$('#id').outerWidth();
	Returns height of an element
		fnb.hyperion.$('#id').height();
	Returns width of an element
		fnb.hyperion.$('#id').width();
	Show element
		fnb.hyperion.$('#id').show();
	Hide element
		fnb.hyperion.$('#id').hide();
	Returns element children
		fnb.hyperion.$('#id').children();
	Returns element of index in list
		fnb.hyperion.$('.id').get(0);
	Returns length of element or list
		fnb.hyperion.$('.id').length();
	Add child element
		fnb.hyperion.$('.id').add(element);
	Remove child element
		fnb.hyperion.$('.id').remove(element);
	Add html to element
		fnb.hyperion.$('.id').html(markup);
	Get html from element
		fnb.hyperion.$('.id').html();
	Add text to element
		fnb.hyperion.$('.id').text(string);
	Get text from element
		fnb.hyperion.$('.id').text();
	Get element position
		fnb.hyperion.$('.id').position();
	Set element proprty
		fnb.hyperion.$('.id').prop('checked',true);
	Get element property
		fnb.hyperion.$('.id').prop('checked');
	Bind data to element
		fnb.hyperion.$('.id').data({line1: 'test string 1',line2: 'test string 2'});
	Get data bound to element
		fnb.hyperion.$('.id').data();
	Get value of key in data bound to element
		fnb.hyperion.$('.id').data('line1');
	Set value of key in data bound to element
		fnb.hyperion.$('.id').data('line1', 'test string 3');
	Set element value
		fnb.hyperion.$('#testInput').val('Hallo');
	Get element value
		fnb.hyperion.$('#testInput').val();
*/
(function() {
	//------------------------
	//DOM ELEMENT METHODS
	//------------------------
	var isTests = {
			":visible" : {attr: "data-visible" , match: "false"},
			":hidden" : {prop: "visibility", match: "hidden"}
	};
	//Test for data attribure
	function is(value){
		for (var prop in isTests) {
    		if(prop = value){
    			if(isTests[prop].attr){
    				if(this.elem.getAttribute(isTests[prop].attr)!=isTests[prop].match){
    					return true;
    				}else{
    					return false;
    				}
    			}else if(isTests[prop].prop){
    				var style = this.elem.currentStyle[prop] || this.elem.style[prop];
    				if(style==isTests[prop].match){
    					return true;
    				}else{
    					return false;
    				}
    			};
			};
    	};
	};
	//Get parent node
	function parent(){
		if(!this.elem||!this.elem.parentNode) return;
		return new fnb.hyperion.selector.singleElementMethods(this.elem.parentNode);
	};
	//Find element
	function find(selector){
		if(!this.elem) return;
		return fnb.hyperion.selector(selector, this.elem);
	};
	//Find closest parent matching selector
	function closest(selector){
		//Find closest parent matching selector
		function closest(selector){
			
			if(!this.elem) return;
			
			var elem = this.elem;
			
			var matchesSelector = elem.matches || elem.webkitMatchesSelector || elem.mozMatchesSelector || elem.msMatchesSelector;
			
			if(matchesSelector){
				
				while (elem) {
			        if (matchesSelector.bind(elem)(selector)) {
			            return new fnb.hyperion.selector.singleElementMethods(elem);
			        } else {
			        	elem = elem.parentNode;
			        }
			    }
				
			}else{
				
				//Developer: Donovan - IE8 matchesSelector shim
				var count = 10;

				var parentElem = elem.parentNode;
				
			    for (var i = 0; i < count; i++) {
			    	
			    	parentElem = parentElem.parentNode;
			    	
			    	var elems = parentElem.querySelectorAll(selector);

			    	if (elems.length>0) {
			        	return new fnb.hyperion.selector.singleElementMethods(elems[0]);
			        }
			    }
			    
			}
		   
		    return false;
		};
	};
	//Call Action on element
	function callAction(action, scope){
		if(!this.elem) return;
		action.call(scope, this, 1, this);
	};
	//Add attribute to element
	function attr(name, value){
		if(!this.elem) return;
		if(typeof value!="undefined"||value=='') {
			if(value.toString()==''){
				this.elem.removeAttribute(name);
			}else{
				this.elem.setAttribute(name, value);
			}
		    
			return this.elem;
		} else {
		    return this.elem.getAttribute(name);
		}
		return undefined;
	};
	//Loop and add attribute to element
	function loopAttr(name, value){
		if(!this.elem) return;
		this.elem.each(function(element) {
			if(value||value=='') {
				element.setAttribute(name, value);
			    
				return element;
			} else {
			    return element.getAttribute(name);
			}
		});
		return undefined;
	};
	//Add css style attribute
	function css(prop, value){
		if(!this.elem) return;
		
		if(typeof prop == "object"){
			//Loop poperties and add
        	for (var property in prop) {
        		if(prop.hasOwnProperty(property)){
        			this.elem.style[property] = prop[property];
        		}
        	};
		}else{
			if (value||value=='') {
				this.elem.style[prop] = value;
				return this;
			} else {
				return this.elem.style[prop];
			}
		}
		
		return this;
	};
	//Loop and add css style attribute
	function loopCss(prop, value){
		if(!this.elem) return;
		this.each(function(element) {
			if (value||value=='') {
				element.elem.style[prop] = value;
				return element;
			} else {
				return element.elem.style[prop];
			}
		});

		return this;
	};
	//Add a class to an element
	function addClass(className){
		if(!this.elem) return;
		if (!this.hasClass(className, this.elem)) {
	        if (document.documentElement.classList) {
	        	this.elem.classList.add(className);
	        } else {
	        	this.elem.className += (this.elem.className ? ' ' : '') + className;
	        }
	    }
		return this;
	};
	//Loop Add a class to an element
	function loopAddClass(className){
		if(!this.elem) return;
		this.elem.each(function(element) {
			if (!this.hasClass(className, element)) {
		        if (document.documentElement.classList) {
		        	element.classList.add(className);
		        } else {
		        	element.className += (element.className ? ' ' : '') + className;
		        }
		    }
		});
		return this;
	};
	//Remove Class from element
	function removeClass(className){
		if(!this.elem) return;
		if (this.hasClass(className, this.elem)) {
	        if (document.documentElement.classList) {
	        	this.elem.classList.remove(className);
	        } else {
	        	this.elem.className = this.elem.className.replace(new RegExp('(^|\\s)*' + className + '(\\s|$)*', 'g'), '');
	        }
	    }
		return this;
	};
	//Loop Remove Class from element
	function loopRemoveClass(className){
		if(!this.elem) return;
		this.each(function(element) {
			if (element.hasClass(className, element)) {
		        if (document.documentElement.classList) {
		        	element.elem.classList.remove(className);
		        } else {
		        	element.elem.className = element.elem.className.replace(new RegExp('(^|\\s)*' + className + '(\\s|$)*', 'g'), '');
		        }
		    }
		});
		return this;
	};
	//Test if an element has a class
	function hasClass(className,element){
		if(!this.elem&&!element) return;
		var elem = (element) ? (element.elem) ? element.elem : element : this.elem;
		if (document.documentElement.classList) {
	        return elem.classList.contains(className);
	    } else {
	        return new RegExp('(^|\\s)' + className + '(\\s|$)').test(elem.className);
	    }
	};
	//Toggle a class on an element
	function toggleClass(className) {
		if(!this.elem) return;
		if (document.documentElement.classList) {
			this.elem.classList.toggle(className);
	    } else {
	        if (this.hasClass(className)) {
	        	this.removeClass(className);
	        } else {
	        	this.addClass(className);
	        }
	    }
		return this;
	};
	//Loop and toggle a class on an element
	function loopToggleClass(className) {
		if(!this.elem) return;
		this.elem.each(function(element) {
			if (document.documentElement.classList) {
				element.classList.toggle(className);
		    } else {
		    	if (this.hasClass(className, element)) {
		        	element.removeClass(className);
		        } else {
	    	        if (document.documentElement.classList) {
	    	        	element.classList.add(className);
	    	        } else {
	    	        	element.className += (element.className ? ' ' : '') + className;
	    	        }
		        }
		    }
		});
		return this;
	};
	//Return first element in list elements
	function first(){
		if(!this.elem) return;
		return (this.elem.length) ? (this.elem.length < 2) ? new fnb.hyperion.selector.singleElementMethods(this.elem) : new fnb.hyperion.selector.singleElementMethods(this.elem[0]) : new fnb.hyperion.selector.singleElementMethods(this.elem);
	};
	//Return Last element in list elements
	function last(){
		if(!this.elem) return;
		return (this.elem.length) ? (this.elem.length > 1) ? new fnb.hyperion.selector.singleElementMethods(this.elem[this.elem.length - 1]) : new fnb.hyperion.selector.singleElementMethods(this.elem) : new fnb.hyperion.selector.singleElementMethods(this.elem);
	};
	//Return Index of element in list elements
	function index(){
		if(!this.elem) return;
		var children = this.elem.parentNode.childNodes;
	    var num = 0;
	    for (var i=0; i<children.length; i++) {
	         if (children[i]==this.elem) return num;
	         if (children[i].nodeType==1) num++;
	    }
	    return -1;
	};
	//Get the outerheight of an element
	function outerHeight(){
		if(!this.elem) return;
		var elementHeight, elementMargin, borderTop, borderBottom;
		var height = 0;

	    if(document.documentMode) {// IE

	    	var attributeHeight = 0;
		    var attributes = [
		        'height', 
		        'border-top-width', 
		        'border-bottom-width', 
		        'padding-top', 
		        'padding-bottom', 
		        'margin-top', 
		        'margin-bottom'
		    ];

		    for (var i = 0; i < attributes.length; i++) {

		      attributeHeight = parseInt(document.defaultView.getComputedStyle(this.elem, '').getPropertyValue(attributes[i]), 10);

		      if (!isNaN(attributeHeight)) {
		        height += attributeHeight;
		      }
		    }
	        
		    height = isNaN(height) ? 0 : height;
		    
	    } else {// Modern Browser

	    	borderTop = parseInt(document.defaultView.getComputedStyle(this.elem, '').getPropertyValue('border-top-width'), 10);
	    	borderBottom = parseInt(document.defaultView.getComputedStyle(this.elem, '').getPropertyValue('border-bottom-width'), 10);
	        elementHeight = parseInt(document.defaultView.getComputedStyle(this.elem, '').getPropertyValue('height'), 10);
	        elementMargin = parseInt(document.defaultView.getComputedStyle(this.elem, '').getPropertyValue('margin-top')) + parseInt(document.defaultView.getComputedStyle(this.elem, '').getPropertyValue('margin-bottom'));
	        
	        height = elementHeight+elementMargin+borderBottom+borderTop;
	    }
	    
	    return height;
	};
	//Get the outerWidth of an element
	function outerWidth(){
		if(!this.elem) return;
		
	    return this.elem.offsetWidth;
	};
	//Get index of element
	function indexOf(find, i){
		if(!this.elem) return;
		if (!this.elem.length) return -1;
		if (i===undefined) i= 0;
		if (i<0) i+= this.elem.length;
		if (i<0) i= 0;
		for (var n= this.elem.length; i<n; i++){
			if (i in this.elem && this.elem[i]===find)
				return i;
	        return -1;
		}
		return this;
	};
	//Show element
	function show(){
		if(!this.elem) return;
		this.attr('data-visible', 'true');
		this.removeClass('Hhide');
		return this;
	};
	//Loop and show element
	function loopShow(){
		if(!this.elem) return;
		this.each(function(element) {
			element.attr('data-visible', 'true');
		    element.removeClass('Hhide');
		});
		return this.elem;
	};
	//Hide element
	function hide(){
		if(!this.elem) return;
		this.attr('data-visible', 'false');
		this.addClass('Hhide');
		return this;
	};
	//Loop and Hide element
	function loopHide(){
		if(!this.elem) return;
		this.each(function(element) {
			element.attr('data-visible', 'false');
		    element.addClass('Hhide');
		});
		return this;
	};
	//Toggle Visiblility
	function toggleVisible(){
		if(!this.elem) return;
		if (this.attr('data-visible') == 'true') {
			this.elem.attr('data-visible', 'false');
			this.elem.addClass('Hhide');
		}else {
			this.elem.attr('data-visible', 'true');
			this.elem.removeClass('Hhide');
		}
		return this;
	};
	//Loop and Toggle Visiblility
	function loopToggleVisible(){
		if(!this.elem) return;
		this.each(function(element) {
			if (this.attr('data-visible') == 'true') {
				element.attr('data-visible', 'false');
				element.addClass('Hhide');
			}else {
				element.attr('data-visible', 'true');
				element.removeClass('Hhide');
			}
		});
		return this;
	};
	//Select children
	function children(child){
		if(!this.elem) return;
		var children = this.elem.children;
		if(children.length==1){
			return (typeof child!='undefined') ? new fnb.hyperion.selector.singleElementMethods(children[child]) : new fnb.hyperion.selector.singleElementMethods(children[0]);
		}else if(children.length>1){
			return (typeof child!='undefined') ? new fnb.hyperion.selector.singleElementMethods(children[child]) : new fnb.hyperion.selector.listElementMethods(children);
		}else{
			return new fnb.hyperion.selector.singleElementMethods();
		}
		return this;
	};
	//Get element
	function get(number){
		if(!this.elem) return;
		if(typeof number==='number'){
			if(this.length()>0){
				return new fnb.hyperion.selector.singleElementMethods(this.elem[number]);
			}else{
				return new fnb.hyperion.selector.singleElementMethods(this.elem[0]);
			}
		}else{
			return new fnb.hyperion.selector.listElementMethods(this.elem);
		}
		return this;
	};
	//Clone a node
	function clone(children){
		if(!this.elem) return;
		
		var cloneChildren = (children) ? children : true;
		
		var clonedNode = this.elem.cloneNode(cloneChildren);
		
		return new fnb.hyperion.selector.singleElementMethods(clonedNode);
	};
	//Return Set element width
	function width(width){
		if(!this.elem) return;
		if(typeof width==='number'||typeof width==='string'){
			this.elem.style.width = width;
		}else{
			return this.elem.style.width;
		}
		return this;
	};
	//Return Set element height
	function height(height){
		if(!this.elem) return;
		if(typeof height==='number'||typeof height==='string'){
			this.elem.style.height = height;
		}else{
			return this.elem.style.height;
		}
		return this;
	};
	//Prepend elemnt to element
	function prepend(element){
		if(!this.elem) return;
		if(typeof element=="string"){
			this.elem.innerHTML = element + this.elem.innerHTML;
		}else{
			if(element.nodeType){
				this.elem.insertBefore(element,(this.elem.hasChildNodes()) ? this.elem.childNodes[0] : null);
			}else{
				this.elem.insertBefore(element.elem,(this.elem.hasChildNodes()) ? this.elem.childNodes[0] : null);
			}
			
		}
		
		return this;
	};
	//Add elemnt to element
	function add(element){
		if(!this.elem) return;
		if(typeof element=="string"){
			this.elem.innerHTML = this.elem.innerHTML + element;
		}else{
			if(element.nodeType){
				this.elem.appendChild(element);
			}else{
				this.elem.appendChild(element.elem);
			}
		}
		
		return this;
	};
	//Remove elemnt to element
	function remove(element){
		if(!this.elem) return;
		this.elem.removeChild(element);
		return this;
	};
	//Single Element length
	function onelength(){
		return 1;
	};
	//Element length
	function length(){
		if(this.elem){
			if(this.elem.length){
				return this.elem.length;
			}else{
				return 1;
			}
		}else{
			return 0;
		}
		return this;
	};
	//Set Get html
	function html(html){
		if(!this.elem) return;
		if(typeof html!="undefined"){
			this.elem.innerHTML = html;
		}else{
			return this.elem.innerHTML;
		}
		return this;
	};
	
	//Set Get html
	function outerHtml(){
		return this.elem.outerHTML;
	};
	//Set Get html
	function text(text){
		if(!this.elem) return;
		if(text){
			this.elem.innerTEXT = text;
		}else{
			return this.elem.innerTEXT;
		}
		return this;
	};
	//Set Get html
	function prop(prop, value){
		if(!this.elem) return;
		if(value){
			this.elem[prop] = value;
		}else{
			return this.elem[prop];
		}
		return this;
	};
	//Set Get data on object
	function data(data,value){
		if(data&&value){
			this.dataObj[data] = value;
		}else if(data&&!value){
			if(typeof data === "string"){
				return this.dataObj[data];
			}else{
				this.dataObj = data;
			}
		}else if(!data){
			return this.dataObj;
		}
		return this;
	};
	//Get element type
	function type(){
		return this.elem.tagName;
	};
	//Get element position
	function position(){
		var element = this.elem;
		var curleft = curtop = 0;

		if(element.getBoundingClientRect){
	      var position = element.getBoundingClientRect();
			  curleft  = position.left;
			  curtop   = position.top;	
		}
		
		var xScroll, yScroll;
		if (window.pageYOffset) {
			yScroll = window.pageYOffset;
			xScroll = window.pageXOffset;
		} else if (document.documentElement && document.documentElement.scrollTop) {
			yScroll = document.documentElement.scrollTop;
			xScroll = document.documentElement.scrollLeft;
		} else if (document.body) {// all other Explorers
			yScroll = document.body.scrollTop;
			xScroll = document.body.scrollLeft;
		}
	    return {objectPosX: curleft,objectPosY: curtop, pageScroll: {pageScrollX: xScroll,pageScrollY: yScroll}, relativeViewportPos: {viewPortX: curleft-xScroll,viewPortY: curtop-yScroll}};
		return this;
	};
	//Each loop
	function each(action, that){
		for (var i= 0, n= this.elem.length; i<n; i++){
			if (i in this.elem){
				var elem = new fnb.hyperion.selector.singleElementMethods(this.elem[i]);
				action.call(that, elem, i, this.elem);
			}
		}
		return this;
	};
	//Set or get element value
	function val(value){
		if(!this.elem) return;
		if(typeof value!="undefined"){
			this.elem.value = value;
		}else{
			return (this.elem.options) ? (typeof this.elem.options[this.elem.selectedIndex]=="undefined") ? "" : this.elem.options[this.elem.selectedIndex].value : this.elem.value;
		}
		return this;
	};
	///-------------------------------------------///
	/// Selector Parent function
	///-------------------------------------------///
	selector = function(selector, context, undefined) {
		//Test if selector already selected
		if(typeof selector==="object"){
			var element = (selector.length&&selector.tagName!='SELECT') ? new fnb.hyperion.selector.listElementMethods(selector) : new fnb.hyperion.selector.singleElementMethods(selector);
			return element;
		}
		//Get selector
		var selectorZero = selector[0];
		//Get selection method
		var searches = {
				'#': 'getElementById',
				'.': 'getElementsByClassName',
				'@': 'getElementsByName',
				'=': 'getElementsByTagName',
				'!': 'querySelector',
				'*': 'querySelectorAll'
		}[selectorZero];

		//Do selection
		var element = (((context === undefined || selectorZero === "#") ? document: context)[searches](selector.slice(1)));

		return ((element) ?
				(element.length < 2) 
				? (typeof element[0] == 'undefined') ? new fnb.hyperion.selector.empty({})
				:  (selectorZero=="#") ? 
						new fnb.hyperion.selector.singleElementMethods(element) : 
							new fnb.hyperion.selector.singleElementMethods(element[0])
				: (element.length) ?
					  (selectorZero=="#") ? 
							  new fnb.hyperion.selector.singleElementMethods(element) 
							: new fnb.hyperion.selector.listElementMethods(element)
					: new fnb.hyperion.selector.singleElementMethods(element)
			    : new fnb.hyperion.selector.empty({})
				);
	};
	
	//------------------------
	//ELEMENT METHOD GROUPS
	//------------------------
	
	//Single element methods
	selector.singleElementMethods = function(element) {
		//Add element
		this.elem = element;
		//Find parent node
        this.parent = parent;
        //Find child
        this.find = find;
        //Find closest parent matching selector
        this.closest = closest;
        //Each loop on single element
        this.each = callAction;
        //Add attribute to elements
        this.attr = attr;
        //Add attribute to element
        this.css = css;
        //Add class to element
        this.addClass = addClass;
        //Remove class to element
        this.removeClass = removeClass;
        //Test if an element has a class
        this.hasClass = hasClass;
        //Toggle a class on an element
        this.toggleClass = toggleClass;
        //Return first element in list elements
        this.first = first;
        //Return Last element in list elements
        this.last = last;
        //Return Index of element in list elements
        this.index = index;
        //Get the outerheight of an element
        this.outerHeight = outerHeight;
        //Get the outerWidth of an element
        this.outerWidth = outerWidth;
        //Get the index of a item
        this.indexOf = indexOf;
        //Show element
        this.show = show;
        //Hide element
        this.hide = hide;
        //Get element children
        this.children = children;
        //Get element from list
        this.get = get;
        //Get element children
        this.width = width;
        //Get element children
        this.height = height;
        //Get element length
        this.length = onelength;
        //Add element
        this.add = add;
        //Remove element
        this.remove = remove;
        //Add remove html
        this.html = html;
        //get outerHtml
        this.outerHtml = outerHtml;
        //Add remove text
        this.text = text;
        //Add property to element
        this.prop = prop;
        //Set get data
        this.data = data;
        //Data storage
		this.dataObj = {};
        //Get element position
		this.position = position;
		//Is tests
		this.is = is;
		//Get element value
		this.val = val;
		//Get element type
		this.type = type;
		//Clone node
		this.clone = clone;
	};
	//List elements methods
	selector.listElementMethods = function(element) {
		//Add elements
		this.elem = element;
		//Find parent node
        this.parent = parent;
        //Find child
        this.find = find;
        //Each loop on list elements
        this.each = each;
        //Add attribute to element
        this.attr = loopAttr;
        //Add attribute to elements
        this.css = loopCss;
        //Add class to elements
        this.addClass = loopAddClass;
        //Remove class to elements
        this.removeClass = loopRemoveClass;
        //Toggle a class on an elements
        this.toggleClass = loopToggleClass;
        //Get element length
        this.length = length;
        //Return first element in list elements
        this.first = first;
        //Return Last element in list elements
        this.last = last;
        //Return Index of element in list elements
        this.index = index;
        //Get the index of a item
        this.indexOf = indexOf;
        //Add property to element
        this.prop = prop;
        //Show elements
        this.show = loopShow;
        //Hide elements
        this.hide = loopHide;
        //Get element children
        this.children = children;
        //Get element from list
        this.get = get;
        //Set get data
        this.data = data;
        //Data storage
		this.dataObj = {};
		//Get element type
		this.type = type;
	};
	//List elements methods
	selector.empty = function(element) {
        //Get element length
        this.length = length;
	};
	//Namespace selector
	fnb.namespace('selector', selector);
	
})();