///-------------------------------------------///
/// developer: Mike
///
/// Push State Object
///-------------------------------------------///
(function() {
	//Frequency of push state update timer
	var frequency = 100;
	//Instances of push state qeue
	var instance = 0;
	//Timer var for push state timer
	var timer = null;
	//Url var for push state
	var url = null;
	//Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: window, events:'popstate', handler: 'fnb.hyperion.pushState.pop(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	//Split url and get parts for hash
    function getHistoryParts() {
		return window.location.href.split('#');
	};
	//Create timer for pushstate
	function startTiming() {
		if (!timer)
			timer = setInterval(function() {
				if (instance && url!=window.location.href) {
					url = window.location.href;
					inspect();
			}
		}, frequency);
	};
	//Stop timer for pushstate
	function stopTiming() {
		if (timer) {
			clearInterval(timer);
			timer = null;
		}
	};
	//Fragment url
	function fragmentUrl() {
		var hash = getHistoryParts();
		return (fnb.hyperion.pushState.enabled) ? window.location.pathname + ((hash[1]) ? '#' + hash[1] : '')  : hash[1] || '';
	};
	//Get url parameters
	function getUrlParameters(path, params) {
		var p, parameters = [];
		for(p in params) {
			if (!Object.prototype.hasOwnProperty(p))
				continue;
			parameters.push(encodeURIComponent(p) + '=' + encodeURIComponent(params[p]));
		}
		if (parameters.length>0)
			parameters = '?' + parameters.join('&');
		return (fnb.hyperion.pushState.enabled) ? path + parameters :  getHistoryParts()[0] + '#' + path + parameters;
	};
	//Check history support
	function support() {
		if (window.history && window.history.pushState){
			return true;
		}
		return false;
	};
	//Inspect object
	function inspect() {
		var i, 
		part,
		parts,
		fragment = this.fragment();
		for (part in fnb.hyperion.pushState.parts) {
			if (!Object.prototype.hasOwnProperty.call(fnb.hyperion.pushState.parts, part))
				continue;
			fnb.hyperion.pushState.parts[hash].regexp = fnb.hyperion.pushState.parts[part].regexp || new RegExp(part);
			if (parts = fnb.hyperion.pushState.parts[part].regexp.exec(fragment)) {
				if (fnb.hyperion.pushState.parts[part].title)
					document.title = fnb.hyperion.pushState.parts[hash].title;
	
				for (i = 0; i<fnb.hyperion.pushState.parts[hash].listeners.length; i++)
					fnb.hyperion.pushState.parts[part].listeners[i](fragment, parts);
			}
		}
		return this;
	};
	///-------------------------------------------///
	/// Push State Parent function
	///-------------------------------------------///
	function pushState() {

	};
	///-------------------------------------------///
	/// Push State Methods
	///-------------------------------------------///
	pushState.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Parts of the url
		parts: {},
		//Does browser support history
		enabled: support(),
		//Setting to enable and disable hash support
		enableHash: false,
		//initialized var
		inited: false,
		//Load Object instances wrapper
		loadObjectInstances : [],
		//Init method
		init: function () {
    		console.log('Utils PushState init');
    		//Add listeners
    		bindEvents();
		},
        //pushState method
		push: function (loadObj, params) {
			//Test if command was sent from history
			if(!loadObj.history){
				//get X-Requested-Page
				var requestedPage = (loadObj.ajaxAborted) ? null : loadObj.xhr.getResponseHeader("X-Requested-Page");
				//Get Url
				var part = (requestedPage != null) ? requestedPage : loadObj.url;
				//Split url
				var noParams = part.split("?");
				//Get url parameters
				var to = getUrlParameters(noParams[0], params);
				//Check if pushState supported
				if (this.enabled){
					window.history.pushState(null, document.title, to);
				}else{
					window.location.href =  to;
				}
				//Test if loadObj in instances
				if (!this.loadObjectInstances.hasOwnProperty(to)) {
					//Store load object
					this.loadObjectInstances[to] = fnb.hyperion.controller.loadObj ? fnb.hyperion.controller.loadObj : {url: to, method: "loadPage.load", history: false};
			    }
				//Reset histor flag
				this.loadObjectInstances[to].history = false;
			}

			return this;
        },
        //replaceState method
		pop: function (event) {
			//Split url
			var noParams = window.location.href.toString().split(window.location.host)[1].split("?");
			//Get url parameters
			var to = getUrlParameters(noParams[0]);
			//Test if loadObj in instances
			if (!this.loadObjectInstances.hasOwnProperty(to)) {
				//Store load object
				this.loadObjectInstances[to] = {url: to, method: "loadPage.load"};
		    }

			//Add history param
			this.loadObjectInstances[to].history = true;
			//Reset Load object
			this.loadObjectInstances[to] = fnb.hyperion.controller.getAjaxLoadDefaults(this.loadObjectInstances[to]);
			//Ajax load
			fnb.hyperion.controller.ajax('loadPage.load','loadPage',this.loadObjectInstances[to]);

			//Update initialized var
			this.inited = true;
        },
        //Add Event listeners
        on: function (part, callback, title) {
        	if (!this.parts[hash])
        		this.parts[hash] = {title: title, listeners: []};
        	this.parts[hash].listeners.push(callback);
        	instance++;
			startTiming();
			return this;
        },
        //Remove Event listeners
        remove: function (part, callback) {
        	if (this.parts[part]) {
				var i, 
					l = this.parts[part].listeners.length;
				for (var i = 0; i<l; i++)
					if (this.parts[part].listeners[i]==callback) {

						this.parts[part].listeners.splice(i, 1);
						
						if (--l==0)
							delete this.parts[part];
						
						if (--instance==0)
							stopTiming();

						break;
					}
			}
			return this;
        }
	};

	//Namespace utils.pushState
	fnb.namespace('pushState', pushState, true);

})();