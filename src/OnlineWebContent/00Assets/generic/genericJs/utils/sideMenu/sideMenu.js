///-------------------------------------------///
/// developer: Richard
///
/// Developer 2: Donovan
///
/// sideMenu Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [	
		            {type: 'frame', listener: document, events:'click', selector: '[data-role="sideMenuItem"]', handler: 'fnb.hyperion.utils.sideMenu.sideMenuItem(event);'},
          			{type: 'frame', listener: document, events:'click', selector: '#sideMenuButton', handler: 'fnb.hyperion.utils.sideMenu.show(event);'},
          			{type: 'frame', listener: document, events:'click', selector: '#sideMenuClose', handler: 'fnb.hyperion.utils.sideMenu.hide(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
    sideMenu = function() {
		
	};
	sideMenu.prototype = {
		autoInit: true,
		sideMenu: '',
		sideMenuButton: '',
		sideMenuContent: '',
		activeGroupId: '',
		sideMenuGroups: {},
		sideMenuItems: [],
		//Init FNB Top Tabs
		init: function () {
			//Select sidemenu
			this.sideMenu = fnb.hyperion.$("#sideMenu");
			//Select sidemenuButton
    		this.sideMenuButton = fnb.hyperion.$("#sideMenuButton");
    		//Select sidemenuContent
    		this.sideMenuContent = fnb.hyperion.$("#sideMenuContent");
    		//Create object for side menu
    		this.createGroupObject();

    		bindEvents();
    	},
    	createGroupObject: function () {
    		//Select side menu
    		var sideMenuGroups = fnb.hyperion.$(".sideMenuGroup");
    		//Create reference to group object
    		var parentObj = this;
    		//Test if any were found
    		if (sideMenuGroups.length() > 0) {
    			//Loop side menu and add to group object
    			sideMenuGroups.each(function(elem) {
	    			parentObj.sideMenuGroups[elem.attr('id')] = {'elem':elem};
	    		});
    		};
    	},
    	show: function () {
    		//Set indicator to show menu is active in order to hide page content in phone mode
    		fnb.hyperion.controller.bodyElement.addClass('menuActive');
    		//Move Button offscreen
			this.sideMenuButton.attr("data-position", "offscreen");
			//Move menu onscreen
			this.sideMenuContent.attr("data-position", "");
		},
		hide: function () {
    		//Set indicator to show menu is inactive in order to show page content in phone mode
    		fnb.hyperion.controller.bodyElement.removeClass('menuActive');
			//bring menuButton onto screen
			this.sideMenuButton.attr("data-position", "");
			//take menuContent off screen
			this.sideMenuContent.attr("data-position", "offscreen");
			//hide all sidemenu groups
			if (this.activeGroupId != '') { this.sideMenuGroups[this.activeGroupId].elem.hide(); }
			this.activeGroupId = '';
			//set the menu inactive
			this.sideMenu.attr("data-active","false");
		},
		sideMenuItem: function (event) {
			var parentObject = this;
			//the selected SideMenu 
			var selectedSideMenu = fnb.hyperion.$(event.currentTarget);
			//id of the selected SideMenu
		    var selectedSideMenuId = "sideMenuGroup_" + selectedSideMenu.attr('id');
		    //test if incoming id and the active id are the same
		    if(selectedSideMenuId == this.activeGroupId){
	    		//Set indicator to show menu is active in order to hide page content in phone mode
	    		fnb.hyperion.controller.pageContentContainerElement.show();
		    	//hide incoming elem
		    	parentObject.sideMenuGroups[this.activeGroupId].elem.hide();
		    	//clear stored id
		    	this.activeGroupId = '';
		    	// set menu inactive
		    	this.sideMenu.attr("data-active","false");
		    }else {
		    	//find active group and hide active group
		    	if (this.activeGroupId !='' ) {
		    		//hide active group
		    		parentObject.sideMenuGroups[this.activeGroupId].elem.hide();
		    		//show incoming group 
		    		parentObject.sideMenuGroups[selectedSideMenuId].elem.show();
		    	}
		    	else {
		    		//Set indicator to show menu is active in order to hide page content in phone mode
		    		fnb.hyperion.controller.pageContentContainerElement.hide();
		    		//show incoming group
		    		parentObject.sideMenuGroups[selectedSideMenuId].elem.show();
		    	}
		    	//store incoming id
		    	this.activeGroupId = selectedSideMenuId;
		    	// set menu active
		    	this.sideMenu.attr("data-active","true");
		    }
		
		},
		destroy: function () {
		}
	};
		
	//Namespace sideMenu
	fnb.namespace('utils.sideMenu', sideMenu, true);

})();
