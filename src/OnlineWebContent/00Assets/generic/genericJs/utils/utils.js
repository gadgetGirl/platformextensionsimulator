///-------------------------------------------///
/// developer: Donovan
///
/// Main Utils Object
///-------------------------------------------///
(function() {
    //Init all utils prototype modules
	function initModules() {
    	//Loop child modules and init
    	for (var module in fnb.hyperion.utils) {
    		if(fnb.hyperion.utils[module].autoInit){
				fnb.hyperion.utils[module].init();
			}
    	}
    };
	//FNB Utils 
	function utils() {

	};
	//FNB Utils methods
	utils.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB Utils
    	init: function () {
    		console.log('Utils init');
    		initModules();
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils = {};
        }
	};

	//Namespace utils
	fnb.namespace('utils', utils, true);

})();