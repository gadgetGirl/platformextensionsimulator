///-------------------------------------------///
/// developer: Donovan
///
/// Ajax result fragment builder
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax result fragment builder Parent function
	///-------------------------------------------///
	function content() {

	};
	///-------------------------------------------///
	/// Ajax result fragment builder Methods
	///-------------------------------------------///
	content.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init fragment utility
    	init: function () {
    		console.log('Utils fragment init');
        },
        //Create fragment method
        set: function(loadObj,css){
        	//Test if target exists
        	if(loadObj.target.length()>0){
        		//Create html container
            	var htmlContainer = document.createElement('div');

    			//Add ajax returned data to a temporary container
            	htmlContainer.innerHTML = css.html;

    			//Create  a documentFragment.
    			var fragment = document.createDocumentFragment();
    			
    			//Add temp container to fragment
    			fragment.appendChild(htmlContainer);
    			
    			//Create wrapper for contents
    			var fragmentTarget = fragment.childNodes[0];

    			//Add styles to fragment
    			fnb.hyperion.load.styles(fragmentTarget, css.styles);
    			
            	//Split children
            	var fragmentTargetChildren = [].slice.call(fragmentTarget.children);
            	
            	//Clear target
    			loadObj.target.html('');

    			//Loop and add children
    			for(var i=0; i<fragmentTargetChildren.length; i++) {
    				loadObj.target.add(fragmentTargetChildren[i]);
    			}
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.content = {};
        }
	};

	//Namespace ajax getParameters
	fnb.namespace('utils.ajax.content', content, true);

})();