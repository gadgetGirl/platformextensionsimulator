///-------------------------------------------///
/// developer: Donovan
///
/// Ajax scripts utility
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax scripts utility Parent function
	///-------------------------------------------///
	function scripts() {

	};
	///-------------------------------------------///
	/// Ajax getScripts utility Methods
	///-------------------------------------------///
	scripts.prototype = {
		//Var for storing list if scripts in head
		headScripts: [],
		//Var for storing amount of pagescripts in head
		scriptsCounter: 0,
		//Var for frame to auto initialize module
		autoInit: false,
		//Init Ajax filter utility
    	init: function () {
    		console.log('Ajax getScripts init');
        },
        //Get scripts from filterAjaxResponse to append later
        getScripts: function(html){
        	//Find all script tags in the filtered html and store them in array
        	var targetScripts = html.match(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi) != null ? html.match(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi) : [] ;
        	//Add the filtered HTML back into the ajax target but remove all script tags	
        	html = html.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,'');
			//Return formatted loadObj
			return {scripts: targetScripts, cleanHtml: html};
        },
        //Get scripts contents
        getScriptsContents: function(html,container){
        	// Array which will store the styles
	    	var scripts = new Array();
	    	// Array which will store the styles
	    	var importScripts = new Array();
			//Loop html and strip out tags
			while(html.indexOf("<script") > -1 || html.indexOf("</script") > -1) {
				var startScriptTag = html.indexOf("<script");
				var endScriptTag = html.indexOf(">", startScriptTag);
				var closedStartScriptTag = html.indexOf("</script", startScriptTag);
				var closedEndScriptTag = html.indexOf(">", closedStartScriptTag);
				//Create string to test if import
				var isImport = html.substring(startScriptTag, closedEndScriptTag+1);
				//look for url
				var url = isImport.match(/src="[\s\S]*?"/g);
				if(url) {
					//Script has src
					var rawUrl = url[0].replace('src="','').replace('"','');
					//Add script id to headscripts
					importScripts.push(rawUrl);
				}else{
					// Add to styles array
					scripts.push(html.substring(endScriptTag+1, closedStartScriptTag));
				}

				// Strip from strcode
			    html = html.substring(0, startScriptTag) + html.substring(closedEndScriptTag+1);
			}
			//Return formatted loadObj
			return {scripts: scripts, cleanHtml: html, importScripts: importScripts};
        },
        //Set scripts to page
        setScripts: function (scripts, handler) {
        	//Create script callback
        	function callback(){
        		return handler();
        	}
        	//Test for scripts
        	if(scripts.length===0){
        		callback();
        		return;
        	}
        	//Var loaded scripts counter
        	var loadedScripts = 0;
        	//Loop through the scripts array created above and create a script tag object for each one
			for (var i=0;i<scripts.length;i++)	{
				//Increase scripts counter
				this.scriptsCounter++;
				//Create a script object					
				var importScript = document.createElement('script');
				//Set the type on the script object
				importScript.type ="text/javascript";
				//Add a class to the script tag
				importScript.className = "pageScript";
				//Create script id
				var newId = "pageScript"+this.scriptsCounter;
				//Add a id to the script tag
				importScript.setAttribute("id", newId);
				//Add script id to headscripts
				this.headScripts.push(newId);
				//Check the stored script for a src attribute
				var url = scripts[i].match(/src="[\s\S]*?"/g);
				//Declare code variable
				var code = undefined;
				//Test for url
				if(url) {
					//Script has src. Add the src to the newly created script object
					var rawUrl = url[0].replace('src="','').replace('"','');
					importScript.src = rawUrl;					
				}else{
					//Script has no src and is an inline script. Add the code to the newly created script object
					code = scripts[i].replace(/<script[^>]*?[^>]*>([\s\S]*?)/gi,'').replace(/<\/script>/gi,'');
				}
				//Add and execute script tags to the ajax target
				fnb.hyperion.controller.head.add(importScript);
				//Inject code if needed
				if(code){
					importScript.text = code;
					//Increase loaded script count
					loadedScripts++;
	                //Test if all scripts have been loaded
					if(loadedScripts==scripts.length){
		        		callback();
					}
				}
				//Add script event listeners
				if(importScript.readyState){//IE
					importScript.onreadystatechange = function(){
			            if (importScript.readyState == "loaded"||importScript.readyState == "complete"){
			            	importScript.onreadystatechange = null;
			            	//Increase loaded script count
			                loadedScripts++;
			                //Test if all scripts have been loaded
			                if(loadedScripts==scripts.length){
			            		callback();
							}
			            }
			        };
			    }else{//Others
			    	importScript.onload = function(){
			    		//Increase loaded script count
			        	loadedScripts++;
		                //Test if all scripts have been loaded
			    		if(loadedScripts==scripts.length){
			        		callback();
						}
			        };
			    }
			}
        },
        //Remove scripts from head
        clear: function () {
        	//Test if there are scripts in the head
        	if(this.scriptsCounter>0){
            	//Loop scripts and remove
            	this.headScripts.forEach(function(id){
            		var currentScript = fnb.hyperion.$('#'+id).elem;
            		fnb.hyperion.controller.head.remove(currentScript);
            	});
            	//Reset head list
            	this.headScripts = [];
        		//Reset script counter
            	this.scriptsCounter = 0;
        	}
        },
        //prepend css to element
        prependJs: function (element, js) {
        	//Loop css and prepend links
        	for (var i=0;i<js.length;i++)	{
				//Prepend element with js
				element.insertBefore(js[i],(element.hasChildNodes()) ? element.childNodes[0] : null);
        	};
        },
        //Eval scripts method
        evalScripts: function (scripts) {
			// Loop through every script collected and eval it
			for(var i=0; i<scripts.length; i++) {
				try {
					//Eval sccript
					eval(scripts[i]);
				}catch(e) {
				    //Script error
					console.log('Error: utils.ajax.scripts: '+e);
				}
			};
        },
        //Eval script method
        evalScript: function (script) {
        	if (typeof(eval(script)) === 'function') {

        	} else {
        		setTimeout(function () {
        			fnb.hyperion.utils.ajax.scripts.evalScript(script);
        		}), 50
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.scripts = {};
        }
	};

	//Namespace ajax
	fnb.namespace('utils.ajax.scripts', scripts, true);

})();