///-------------------------------------------///
/// developer: Donovan
///
/// Ajax css utility
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax css utility Parent function
	///-------------------------------------------///
	function css() {

	};
	///-------------------------------------------///
	/// Ajax getcss utility Methods
	///-------------------------------------------///
	css.prototype = {
		//Var for storing list if link in head
		headCss: [],
		//Var for storing amount of pagescripts in head
		cssCounter: 0,
		//Var for frame to auto initialize module
		autoInit: false,
		//Init Ajax filter utility
    	init: function () {
    		console.log('Ajax getcss init');
        },
        //Get css from filterAjaxResponse to append later
        getCss: function(html){
        	// Array which will store the styles
	    	var links = new Array();
        	//Find all script tags in the filtered html and store them in array
        	var targetCss = html.match(/(<link\b.+href=")(?!http)([^"]*)(".*>)/gi) != null ? html.match(/(<link\b.+href=")(?!http)([^"]*)(".*>)/gi) : [] ;
        	//Add the filtered HTML back into the ajax target but remove all script tags	
        	html = html.replace(/(<link\b.+href=")(?!http)([^"]*)(".*>)/gi,'');
        	
        	for (var i=0;i<targetCss.length;i++)	{
        		var url = targetCss[i].match(/href="[\s\S]*?"/g);
        		//Script has src
				var rawUrl = url[0].replace('href="','').replace('"','');
				//Add script id to headscripts
				links.push(rawUrl);
        	}
			//Return formatted loadObj
			return {css: links, cleanHtml: html};
        },
        //Collect style tags
        getStyles: function(html){
	        // Array which will store the styles
	    	var styles = new Array();
			//Loop html and strip out tags
			while(html.indexOf("<style") > -1 || html.indexOf("</style") > -1) {
				var startStyleTag = html.indexOf("<style");
				var endStyleTag = html.indexOf(">", startStyleTag);
				var closedStartStyleTag = html.indexOf("</style", startStyleTag);
				var closedEndStyleTag = html.indexOf(">", closedStartStyleTag);
				// Add to styles array
				styles.push(html.substring(endStyleTag+1, closedStartStyleTag));
				// Strip from strcode
			    html = html.substring(0, startStyleTag) + html.substring(closedEndStyleTag+1);
			}
			//Return formatted loadObj
			return {styles: styles, cleanHtml: html};
        },
        //Set css to page
        setCss: function (css, handler) {
        	//Create script callback
        	function callback(){
        		return handler();
        	}
        	//Test for css
        	if(css.length===0){
        		callback();
        		return;
        	}
        	//Var loaded css counter
        	var loadedCss = 0;
        	//Loop through the css array created above and create a script tag object for each one
			for (var i=0;i<css.length;i++)	{
				//Get current css count in head
				var cssHeadCount = fnb.hyperion.controller.head.find('=link').length();
				//Do head count method flag
				var doCssHeadCount = false;
				//Increase scripts counter
				this.cssCounter++;
				//Create a script object					
				var importCss = document.createElement('link');
				//Set the type on the script object
				importCss.type ="text/css";
				//Set the rel on the script object
				importCss.rel ="stylesheet";
				//Add a class to the link tag
				importCss.className = "pageCSS";
				//Create script id
				var newId = "pageCSS"+this.cssCounter;
				//Add a id to the script tag
				importCss.setAttribute("id", newId);
				//Add script id to headscripts
				this.headCss.push(newId);
				//Check the stored script for a src attribute
				var url = css[i].match(/href="[\s\S]*?"/g);
				//Add script event listeners
				if(importCss.readyState){//IE
					importCss.onreadystatechange = function(){
			            if (importCss.readyState == "loaded"||importCss.readyState == "complete"){
			            	importCss.onreadystatechange = null;
			            	//Increase loaded script count
			                loadedCss++;
			                //Test if all css have been loaded
			                if(loadedCss==css.length){
			            		callback();
							}
			            }
			        };
			    }else{//Others
			    	doCssHeadCount = true;
			    }

				//Test for url
				if(url) {
					//Script has src. Add the src to the newly created script object
					var rawUrl = url[0].replace('href="','').replace('"','');
					importCss.href = rawUrl;					
				}
				//Add and execute script tags to the ajax target
				fnb.hyperion.controller.head.add(importCss);
				//Check if headcount needs to be done
				if(doCssHeadCount){
					//Create headcount timer
					var cssHeadCountInterval = setInterval(function() {
						//Check if css has been added
					    if (document.styleSheets.length > cssHeadCount) {
					    	//Increase css coiunter
					    	loadedCss++;
					    	//Check if all scripts have been loaded and trigger callback
					    	if(loadedCss==css.length){
				        		callback();
							}
					    	//Clear cssHeadCountInterval
					    	clearInterval(cssHeadCountInterval);
					    }
					 }, 10);
				}
				
			}
        },
        //prepend css to element
        prependCss: function (element, css) {
        	//Loop css and prepend links
        	for (var i=0;i<css.length;i++)	{
        		//Create a script object					
    			var importCss = document.createElement('link');
    			//Set the type on the script object
    			importCss.type ="text/css";
    			//Set the rel on the script object
    			importCss.rel ="stylesheet";
				//Check the stored script for a src attribute
				var url = css[i].match(/href="[\s\S]*?"/g);
				//Test for url
				if(url) {
					//Script has src. Add the src to the newly created script object
					var rawUrl = url[0].replace('href="','').replace('"','');
					importCss.href = rawUrl;					
				}
				//Prepend element
				element.insertBefore(importCss,(element.hasChildNodes()) ? element.childNodes[0] : null);
        	};
        },
        //prepend styles to element
        prependStyles: function (element, styles) {
        	// Loop through every script collected and eval it
			for(var i=0; i<styles.length; i++) {
				//Craete new style tag
				var style = document.createElement('style');
				//Add type
				style.type = 'text/css';
				//Add styles
				if (style.styleSheet){
					style.styleSheet.cssText = styles[i];
				} else {
					style.appendChild(document.createTextNode(styles[i]));
				}
				//Prepend element
				element.insertBefore(style,(element.hasChildNodes()) ? element.childNodes[0] : null);
			};
        },
        //Remove css from head
        clear: function () {
        	//Test if there are css in the head
        	if(this.cssCounter>0){
            	//Loop scripts and remove
            	this.headCss.forEach(function(id){
            		var currentLink = fnb.hyperion.$('#'+id).elem;
            		fnb.hyperion.controller.head.remove(currentLink);
            	});
            	//Reset head list
            	this.headCss = [];
        		//Reset script counter
            	this.cssCounter = 0;
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.css = {};
        }
	};

	//Namespace ajax
	fnb.namespace('utils.ajax.css', css, true);

})();