///-------------------------------------------///
/// developer: Donovan
///
/// Ajax filter utility
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Ajax filter utility Parent function
	///-------------------------------------------///
	function filter() {

	};
	///-------------------------------------------///
	/// Ajax filter utility Methods
	///-------------------------------------------///
	filter.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init Ajax filter utility
    	init: function () {
    		console.log('Ajax filter init');
        },
        //Filter response.
        get: function(target,data,filter){
        	//Setup filter content
        	var filterContent = (filter) ? filter : target;
        	//Page title var
        	var pageTitle = "";
        	//Page category var
        	var pageCategory = "";
        	//Filter var
        	var filteredContent = "";
        	//Test for filter
        	if(filterContent){
               	//Create temp container for ajax result
            	var htmlInDiv = document.createElement('div');
    			//Add ajax returned data to a temporary container
    			htmlInDiv.innerHTML = data;
    			//Create  a documentFragment. This is a light weight DOM object which allows DOM manipulation methods to be executed on the ajax response
    			var fragment = document.createDocumentFragment();
    			//Add temp container to fragment
    			fragment.appendChild(htmlInDiv);
    			//Get the title tag
            	var title = fragment.querySelector('title');
            	//Get page title
            	pageTitle = (title) ? title.innerHTML : '';
            	//Get the body tag
            	var category = fragment.querySelector('[data-category]');
            	//Get page category
            	var pageCategory = (category) ? fnb.hyperion.$(category).attr("data-category") : '';
    			//filter the content...	
    			filteredContent =  fragment.querySelector(filterContent) ? fragment.querySelector(filterContent).innerHTML : data;
        	}else{
        		filteredContent = data;
        	}
			//Return filtered content
			return {content: filteredContent, pageTitle: pageTitle, pageCategory: pageCategory};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.filter = {};
        }
	};

	//Namespace ajax
	fnb.namespace('utils.ajax.filter', filter, true);

})();