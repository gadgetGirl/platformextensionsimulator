///-------------------------------------------///
/// developer: Donovan
///
/// Get target parameters
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Get ajax target parameters Parent function
	///-------------------------------------------///
	function parameters() {

	};
	///-------------------------------------------///
	/// Get ajax target parameters Methods
	///-------------------------------------------///
	parameters.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init parameters utility
    	init: function () {
    		console.log('Utils parameters init');
        },
        //Get target parameters
        get: function(loadObj){
        	//Set default action
			var dataAction = '';
			
			//Get data target
			var dataTargetArray = loadObj.dataTarget.split(',');
			
			//Declare datatarget var
			var dataTarget = "";
			
			//Loop data target array
			dataTargetArray.each(function(element){
				
				//Select target
				dataTarget = fnb.hyperion.$(element);
				
				//Test data length
				if(dataTarget.length() > 0){
					
					//Serialize data target
					var dataTargetParams = fnb.hyperion.controller.serialize(dataTarget);
	
					//Get target data
					loadObj.params = (loadObj.params) ? loadObj.params + '&' + dataTargetParams : dataTargetParams;
	
					//Get dataTarget action
					if(dataTarget.attr('data-action')){
						action = dataTarget.attr('data-action');
						dataAction = action;
					};
					
					//Get dataTarget nav
					if(dataTarget.attr('data-nav')){
						loadObj.params = loadObj.params+"&nav="+dataTarget.attr('data-nav');
					};
					
					//Get target method
					if(dataTarget.attr('data-method')){
						loadObj.type = dataTarget.attr('data-method');
					};
					
				}
				
			});

			//Switch url to action
			if(dataAction!=""){
				loadObj.url = dataAction;
			}
			
			loadObj = this.getUrlParameters(loadObj);
			
			//Return appended loadObj
			return loadObj;
        },
        //Append url with backend parameters
        getUrlParameters: function (loadObj) {
        	//Test if target exists
        	if(loadObj.target.length()>0){
        		//Add target div for the backend
    			if (typeof loadObj.url != 'undefined') {
    				if(loadObj.url.indexOf("?")>-1){
    					loadObj.url += "&targetDiv=" + loadObj.target.attr("id");	
    				}else{
    					loadObj.url += "?targetDiv=" + loadObj.target.attr("id");
    				}
    			}else{
    				loadObj.url = loadObj.params['alternateUrl'];
    				loadObj.url += "?targetDiv=" + loadObj.target.attr("id");
    			}
        	}
        				
			return loadObj;
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.ajax.parameters = {};
        }
	};

	//Namespace ajax getParameters
	fnb.namespace('utils.ajax.parameters', parameters, true);

})();