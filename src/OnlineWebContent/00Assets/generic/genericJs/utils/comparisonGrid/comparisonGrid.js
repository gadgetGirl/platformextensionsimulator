///-------------------------------------------///
/// developer: Richard 
///
/// comparisonGrid Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="compareItem"]', handler: 'fnb.hyperion.utils.comparisonGrid.toggleHelpText(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
    ///-------------------------------------------///
	/// comparisonGrid Parent function
	///-------------------------------------------///
	function comparisonGrid() {

	};
	///-------------------------------------------///
	/// comparisonGrid Methods
	///-------------------------------------------///
	comparisonGrid.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		compareItems: "",
		//Init FNB comparisonGrid
    	init: function () {
    		this.compareItems = fnb.hyperion.$("#compareItems"),
    		bindEvents();
        },
        toggleHelpText: function (event) {
        	//Hide all popup messages
        	this.compareItems.find(".popupMessageAnchor").hide();
        	//Get row clicked
        	fnb.hyperion.$(event.currentTarget).find(".popupMessageAnchor").show();
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.comparisonGrid = {};
        }
	};

	//Namespace comparisonGrid
	fnb.namespace('utils.comparisonGrid', comparisonGrid, true);

})();