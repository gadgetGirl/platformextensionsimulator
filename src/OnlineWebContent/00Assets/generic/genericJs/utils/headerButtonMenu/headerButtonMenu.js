///-------------------------------------------///
/// developer: Richard
///
/// headerButtonMenu Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [	{type: 'frame', listener: document, events:'click', selector: '#ContactButton', handler: 'fnb.hyperion.utils.headerButtonMenu.toggle();'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
    headerButtonMenu = function() {
		
	};
	headerButtonMenu.prototype = {
		//Set autoInit to true
		autoInit: true,
		//Initialize variables
		headerButtonMenuWrapper: '',
		init: function () {
			//Select header button menu
			this.headerButtonMenuWrapper = fnb.hyperion.$("#headerButtonMenu");
			//Bind events
			bindEvents();
		},
		toggle: function () {
			//Test if menu is off screen (hidden)
			if (this.headerButtonMenuWrapper.attr("data-position") != 'offscreen') {
				//If menu is not off screen (hidden) then hide it.
				fnb.hyperion.utils.headerButtonMenu.hide();
			}
			else {
				//If menu is off screen (hidden) then show it.
				fnb.hyperion.utils.headerButtonMenu.show();
			}
		},
		show: function () {
			//Clear header menu position
			this.headerButtonMenuWrapper.attr("data-position","");
		},
		hide: function () {
			this.headerButtonMenuWrapper.attr("data-position","offscreen");
		},
		destroy: function () {
		}
	};
	//Namespace utils.headerButtonMenuWrapper
	fnb.namespace('utils.headerButtonMenu', headerButtonMenu, true);

})();
