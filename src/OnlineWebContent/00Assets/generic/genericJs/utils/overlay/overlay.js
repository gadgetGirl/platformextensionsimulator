///-------------------------------------------///
/// developer: Donovan
///
/// Overlay Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Overlay Parent function
	///-------------------------------------------///
	function overlay() {

	};
	///-------------------------------------------///
	/// Overlay Methods
	///-------------------------------------------///
	overlay.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Is overlay currently active
		active:false,
		//Init FNB Overlay
    	init: function () {
    		console.log('Utils Overlay init');
        },
		//Show overlay
		show : function() {
			//If overlay is not active display it
			if(!this.active){
				//Set page position
				fnb.hyperion.controller.getScrollPosition(document.body);
				//Update flag
				this.active = true;
				//Change overlay data attribute
				fnb.hyperion.controller.clipOverflow(true);
				//Change overlay visibility
				fnb.hyperion.controller.overlayElement.show();
			}
		},
		//Hide overlay
		hide : function() {
			//If overlay is active hide it
			if(this.active){
				//Update flag
				this.active = false;
				//Change overlay data attribute
				fnb.hyperion.controller.clipOverflow(false);
				//Change overlay visibility
				fnb.hyperion.controller.overlayElement.hide();
				//Change children data attribute
				fnb.hyperion.controller.overlayElement.get(0).hide();
			}
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.overlay = {};
        }
	};

	//Namespace utils.expandables
	fnb.namespace('utils.overlay', overlay, true);

})();