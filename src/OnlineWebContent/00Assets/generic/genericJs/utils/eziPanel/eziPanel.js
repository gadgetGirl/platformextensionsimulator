///-------------------------------------------///
/// developer: Donovan
///
/// Overlay Object
///-------------------------------------------///
(function() {
	//Check css state and apply styles
	function cssState() {
		//Check whether to open or close
		if(fnb.hyperion.utils.eziPanel.active){
			setTimeout(function(){
				fnb.hyperion.controller.eziElement.find('*[data-type="eziWrapperLeft"]').attr('data-width','60');
				fnb.hyperion.controller.eziElement.find('*[data-type="eziWrapperRight"]').css('right','auto');
			},50);
		}else{
			setTimeout(function(){
				fnb.hyperion.controller.clipOverflow(false);
				fnb.hyperion.controller.eziElement.hide();
				fnb.hyperion.controller.setScrollPosition(document.body);
			},200);
		}
	};
	///-------------------------------------------///
	/// Overlay Parent function
	///-------------------------------------------///
	function eziPanel() {

	};
	///-------------------------------------------///
	/// Overlay Methods
	///-------------------------------------------///
	eziPanel.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Is Ezi Panel currently active
		active:false,
		//Init FNB Ezi Panel
    	init: function () {
    		console.log('Ezi Panel init');
        },
		//Show overlay
		show : function() {
			//If Ezi Panel is not active display it
			if(!this.active){
				//Update flag
				this.active = true;
				fnb.hyperion.controller.getScrollPosition(document.body);
				//Change Ezi Panel data attribute
				fnb.hyperion.controller.clipOverflow(true);
				//Set ezi wrapper visibility
				fnb.hyperion.controller.eziElement.show();
				//Check css state
				cssState();
			}
		},
		//Hide Ezi Panel
		hide : function() {
			//If Ezi Panel is active hide it
			if(this.active){
				//Update flag
				this.active = false;
				//Check css state
				cssState();
				//Change Ezi Panel data attribute
				fnb.hyperion.controller.eziElement.find('*[data-type="eziWrapperLeft"]').attr('data-width','100');
				fnb.hyperion.controller.eziElement.find('*[data-type="eziWrapperRight"]').css('right','-40%');
				//Remove Ezipanel contents
				fnb.hyperion.controller.eziPageContentElement.html("");
			}
		},
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.eziPanel = {};
        }
	};

	//Namespace utils.eziPanel
	fnb.namespace('utils.eziPanel', eziPanel, true);

})();