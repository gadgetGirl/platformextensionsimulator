///-------------------------------------------///
/// developer: Donovan
///
/// Errors Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '#errorClose', handler: 'fnb.hyperion.utils.error.hide(event);', preventDefault: true}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Error Parent function
	///-------------------------------------------///
	function error() {

	};
	///-------------------------------------------///
	/// Error Methods
	///-------------------------------------------///
	error.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Selector for error text wrapper
		errorText: '',
		//Active var
		active: false,
		//Init FNB Errors
    	init: function () {
    		console.log('Utils Error init');
    		//Bind Error events
    		bindEvents();
    		//Select Error text wrapper
    		this.errorText = fnb.hyperion.controller.errorElement.find('.errorInner');
	    },
        //Show error message
        show: function (error) {
        	//Test if error is already active
        	if(!this.active){
        		//Set active flag
        		this.active = true;
        		//Test for error object
    			if(typeof error != "string"){
    				var errorsString = "";
    				//Loop errors
    				for(var i=0;i<error.errors.length;i++) {
    					//Build error string
    					errorsString = errorsString + error.errors[i].error;
    				};
    				//Setup error object for templating
    				var errorObject = {'templateName': 'errorMessage', templateData: {message: error.message , errors: errorsString}};
    				//Get html
    				error = fnb.hyperion.controller.getHtmlTemplate(errorObject);  
    			}
            	//Set error wrapper visibility
            	fnb.hyperion.controller.errorElement.show();
            	//Set error text
            	this.errorText.html(error);
        	};
        },
        //Hide error message
        hide: function (event) {
        	//Set error wrapper visibility
        	if(this.active){
        		//Set active flag
        		this.active = false;
        		//Hide error element
        		fnb.hyperion.controller.errorElement.hide();
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.error = {};
        }
	};

	//Namespace utils.error
	fnb.namespace('utils.error', error, true);

})();