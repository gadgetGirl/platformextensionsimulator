///-------------------------------------------///
/// developer: Donovan
///
/// Footer Buttons Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Footer Buttons Parent function
	///-------------------------------------------///
	function footer() {

	};
	///-------------------------------------------///
	/// Footer Buttons Methods
	///-------------------------------------------///
	footer.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Var for max footer buttons allowed
		maxButtons: 10,
		//Init FNB Footer Buttons
    	init: function () {
    		console.log('Utils Footer init');
    		//Add resize function
    		fnb.hyperion.controller.addResizeFunction(fnb.hyperion.utils.footer.configFooterButtons);
	    },
        //Hide footer
        hide: function (target) {
        	//Test for target otherwise hide all
    		if(fnb.hyperion.controller.is(target)) {
        			if(target.length()>0) target.parent().hide();
        	}else{
        		if(fnb.hyperion.controller.footerContainer.length()>0){
    				//Loop footer containers hide
    				fnb.hyperion.controller.footerContainer.children().each(function(element){
    					element.hide();
    				});
    			}
        	}
        },
        //Show footer
        show: function (target) {
    		//Test if any buttons are present
        	if(fnb.hyperion.controller.is(target)) {
        		//Test If the target is selected
        		if(target.length()>0){
        			//Show container
            		target.parent().show();
            		//Adjust footer buttons after show
            		this.configFooterButtons();
        		}
        		
        	}
        },
        configFooterButtons: function(){
        	//Test for footer container
        	if (fnb.hyperion.controller.footerContainer.length() > 0) {
	        	//Get footer container width
	        	var footerContainerWidth = fnb.hyperion.controller.footerContainer.outerWidth();
				//Loop footer containers an align buttons if any
				fnb.hyperion.controller.footerContainer.children().each(function(element){
					//Declare margin left var
		        	var footerLeft = 0;
		        	//Select childrenParent
		        	var childrenParent = element.children(0);
		        	//Reset margin left
		        	childrenParent.css('marginLeft','');
		        	//Select children
		        	var children = childrenParent.children();
		        	//Test for footer buttons
		        	if(children.length()>0){
			        	//Loop children
			        	children.each(function(child){
							footerLeft += child.outerWidth();
						});
			        	//Set margin left var
			        	var marginLeft = footerContainerWidth-footerLeft;
			        	
			        	//Set footer position
			        	if(marginLeft>0) childrenParent.css('marginLeft',marginLeft+'px');
		        	}
				});
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.footer = {};
        }
	};

	//Namespace utils.eziPanel
	fnb.namespace('utils.footer', footer, true);

})();