///-------------------------------------------///
/// developer: Mike
///
/// Design Grid Object
///-------------------------------------------///
(function() {
	function bindEvents() {
    	//List of events for this module
    	var events = [
    	{type: 'frame', listener: document, events:'click', selector: '[data-role="toggleGridBtt"]', handler: 'fnb.hyperion.utils.designGrid.toggleState(event)'},
    	{type: 'frame', listener: document, events:'click', selector: 'a[data-role="subMenu"]', handler: 'fnb.hyperion.utils.designGrid.toggleState(event)'}];
    	//Append events to actions module
    	//fnb.hyperion.controls.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Design Grid Parent function
	///-------------------------------------------///
	function designGrid() {

	};
	///-------------------------------------------///
	/// Design Grid Methods
	///-------------------------------------------///
	designGrid.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		
		gridParent: '',
		
		init: function () {
    	    //Set designGrid default property
    	   	this.gridParent = fnb.hyperion.$('#grid');
    	   	//Bind Design Grid events
    	   	bindEvents();
		},
        toggleState: function() {
        	
        	var gridParent = this.gridParent;
        	
          	if(gridParent.is(':visible')) {
          		gridParent.hide();
        	}else{
        		gridParent.show();
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.designGrid = {};
        }
	};

	//Namespace delegate
	fnb.namespace('utils.designGrid', designGrid, true);

})();