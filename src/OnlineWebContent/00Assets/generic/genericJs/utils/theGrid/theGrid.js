///-------------------------------------------///
/// developer: Mike
///
/// Design Grid Object
///-------------------------------------------///
(function() {
	function bindEvents() {
    	//List of events for this module
    	var events = [
    	{type: 'frame', listener: document, events:'click', selector: '[data-role="theGridToggle"]', handler: 'fnb.hyperion.utils.theGrid.toggleState(event)'}
    	];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Design Grid Parent function
	///-------------------------------------------///
	function theGrid() {

	};
	///-------------------------------------------///
	/// Design Grid Methods
	///-------------------------------------------///
	theGrid.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		
		gridRows: '',
		gridCols: '',
		
		init: function () {
    	    //Set theGrid default property
    	   	this.gridRows = 'theGridOverlayRows';
    	   	this.gridCols = 'theGridOverlayCols';
    	   	//Bind Design Grid events
    	   	bindEvents();
		},
        toggleState: function(event) {
        	//Select grid rows
        	var gridRows = fnb.hyperion.$('#'+this.gridRows);
        	//Select grid columns
        	var gridCols = fnb.hyperion.$('#'+this.gridCols);
        	
        	//Toggle visibility
          	if(gridRows.is(':visible')) {
          		gridRows.hide();
        	}else{
        		gridRows.show();
        	}
          	if(gridCols.is(':visible')) {
          		gridCols.hide();
          	}else{
          		gridCols.show();
          	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.theGrid = {};
        }
	};

	//Namespace utils.theGrid
	fnb.namespace('utils.theGrid', theGrid, true);

})();