///-------------------------------------------///
/// developer: Donovan
///
/// Top Tabs Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
		var events = [{type: 'frame', listener: fnb.hyperion.utils.topTabs.topTabsNav, events:'click', selector: fnb.hyperion.utils.topTabs.topScroller.scrollerChildren, handler: 'fnb.hyperion.utils.topTabs.select(event);', preventDefault: true},
    	              {type: 'frame', listener: document, events:'click', selector: '#topTabScrollButtonRight', handler: 'fnb.hyperion.utils.topTabs.tabsScrollRight();'},
    	              {type: 'frame', listener: document, events:'click', selector: '#topTabScrollButtonLeft', handler: 'fnb.hyperion.utils.topTabs.tabsScrollLeft();'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Top Tabs Parent function
	///-------------------------------------------///
	function topTabs() {

	};
	///-------------------------------------------///
	/// Top Tabs Methods
	///-------------------------------------------///
	topTabs.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Var for topTabs breakpoints
		breakPoints: [10,5,4],
		//Var declarations
		//Current breakpoint position
		topTabBreakpointValue:-2,
		//Instance of horizontal slider
		topScroller:'',
		//Total of toptabs
		tabsCount:0,
		//Toptabs parent for width adjustment
		topTabsNavParent: '',
		//HeaderInner selecteor
		headerInner: '',
		//Toptabs nav wrapper
		topTabsWrapper: '',
		//Toptabs scrollable wrapper
		topTabsNav: '',
		//Toptabs indicator
		topTabsSlider: '',
		//Toptabs indicator index position
		topTabsSliderIndex: -1,
		//Collection of all toptabs
		topTabsChildren:'',
		//Left scroll button selector
		leftScrollButton:'',
		//Right scroll button selector
		rightScrollButton:'',
		//Left scroll button display var
		topTabsLeftScrollShow:false,
		//Right scroll button display var
		topTabsRightScrollShow:false,
		//Toptabs amount of time it scrolls before stopping
		topScrollerStops: 0,
		//Init FNB Top Tabs
    	init: function () {
    		console.log('Utils Top Tabs init');
    		//var Toptabs scrollable wrapper
    		this.topTabsWrapper = fnb.hyperion.$('#topTabs');
    		//Test if toptabs exist
    		if(this.topTabsWrapper.length()>0){
        		//var Toptabs scrollable wrapper
        		this.topTabsNav = this.topTabsWrapper.first();
        		//var topTabsNav parent for width adjustment if horizontal scroller enabled
        		this.topTabsNavParent = this.topTabsNav.parent();
        		//Create Global selector for header inner
        		this.headerInner = this.topTabsNavParent.parent();
        		//Collection of toptab children
        		this.topTabsChildren = this.topTabsNav.find('.mainTab');
    			//Get toptabs count
    			this.tabsCount = this.topTabsChildren.length();
    			//New Instance of horizontal scroller
    			this.topScroller = new fnb.hyperion.horizontalScroller();
    			this.topScroller.scrollableParent = this.topTabsNav;
    			this.topScroller.scrollerChildren = '.mainTab';
    			this.topScroller.scrollStopWidth = 200;
    			this.topScroller.scrollSpeed = 5;
    			this.topScroller.maxStops = 1;
    			this.topScroller.moveTreshold = 0.20;
    			this.topScroller.bindEvents();
    			this.topScroller.enabled = false;
        		//Bind Top tab events
        		bindEvents();
        		//Extend Controller resize function
        		fnb.hyperion.controller.addResizeFunction(this.resize);
				//Adjust toptabs
				this.adjustTopTabs(window.innerWidth || document.documentElement.clientWidth,fnb.hyperion.controller.getBreakPosition(window.innerWidth || document.documentElement.clientWidth));
				//Enable topmenu
				this.enable();
    		}

        },
        //Select functionality for topmenu
        select: function (event) {
    		//Var for the selected tab
    		var selectedTab = fnb.hyperion.$(event.currentTarget);
        	//If the topmenu is not moving or the user is not dragging it - execute body of function
        	if(this.topScroller.moving == false){
        		//Var for the hasSubMenu
        		var subMenu = selectedTab.attr("data-submenu");
        		//Test for subtab
        		if (subMenu === 'true') {
        			//Get the id of the tab
            		var selectedTabId = selectedTab.prop('id');
            		//Add Selected submenu to event
            		event.selectedTabId = selectedTabId;
            		//Raise Show hide submenu event
            		fnb.hyperion.controller.raiseEvent('showHideSubMenu', event);
        		}else{
        			//If selectedTab has a href load the url
	        		//Get the url bound to the tab
	        		var url = selectedTab.attr('data-url');
	        		//Highlight selected tab
	        		fnb.hyperion.utils.topTabs.highlightSelected(event);
	            	//Notify Controller to raise loadUrl event
	            	fnb.hyperion.controller.raiseEvent('loadPage',{url: url,target:fnb.hyperion.controller.pageContentElement});
        		}
        	}
        },
        highlightSelected: function (event) {
    		//Var for the selected tab
    		var selectedTab = (event.currentTarget) ? fnb.hyperion.$(event.currentTarget) : fnb.hyperion.$(event.target);
    		//Select toptab
    		var selectedTopTab = this.topTabsNav.find('.topTabSelected');
    		//Deselect previously selected tab
    		if(selectedTopTab.length()>0) selectedTopTab.removeClass('topTabSelected');
    		//Add selection to current tab
    		selectedTab.addClass('topTabSelected');
        },
        //Resize top tab when window resizes
        resize: function (windowSize, breakpoint) {
        	//Adjust top tabs
        	fnb.hyperion.utils.topTabs.adjustTopTabs(windowSize,breakpoint);
        },
        //Resize top tab when window resizes
        adjustTopTabs: function (windowSize, breakpoint) {
        	//Test if toptabs exist
        	if(this.topTabsChildren.length()>0){
        		/*Get value of the current breakpoint for amount of tabs to be shown*/
            	var currentBreakPointValue = (this.breakPoints[breakpoint-1]) ? this.breakPoints[breakpoint-1] : this.breakPoints[breakpoint];
            	/*Get the scrolling container width*/
            	var currentContainerWidth = this.topTabsNavParent.outerWidth();
            	/*Calculate the new width for the tabs with breakpoint value*/
            	var newTabWidth = Math.round(currentContainerWidth/currentBreakPointValue);
            	/*Calculate new width of the horizontal scroller*/
            	var newScrollingWrapperWidth = Math.round(newTabWidth*this.tabsCount);
            	/*Apply styles*/
    			this.topScroller.enabled = false;
            	/*Apply Scroller wrapper new width*/
            	if(!isNaN(newScrollingWrapperWidth)){
            		if((currentContainerWidth+(newTabWidth/2))>(newTabWidth*this.tabsCount)){
            			this.topTabsNav.css('width',"");
                		this.topTabsNav.css('left',"");
            		}else{
            			this.topTabsNav.css('width',newScrollingWrapperWidth+'px');
            			this.topScroller.enabled = true;
            		}
            	}else{
        			this.topTabsNav.css('width',"");
            		this.topTabsNav.css('left',"");
            	}
    			//Update tab widths if needed
    			this.adjustTabsWidth(newTabWidth+'px');
    			//Update global breakpoint value
    			this.topTabBreakpointValue = currentBreakPointValue;
    			/*Apply new horizontal scroller settings*/
            	/*Calculate the amount of stops for the horizontal scroller*/
            	this.topScrollerStops = Math.floor(this.tabsCount/currentBreakPointValue);
            	//Update horizontal scroller stops
    			this.topScroller.maxStops = this.topScrollerStops;
            	//Update horizontal scroller stops
				this.topScroller.moveTo(0,1);
				//Events to execute after topmenu stopped scrolling
				this.topScroller.afterStop = this.adjustScrollButtonRoles;
				//Check the roles for the topmenu scrolling buttons
				this.adjustScrollButtonRoles();
				//Set scroller scroll stop width
    			this.topScroller.scrollStopWidth = currentContainerWidth;
            	
        	}
        	
        },
        //Change toptab widths
        adjustTabsWidth: function (width) {
        	//Parse tab width
        	var newVal = parseInt(width, 10);
        	if(!isNaN(newVal)){
            	if(this.topScroller.enabled){
                	//Set topTab widths
                	this.topTabsChildren.css('width',width);
            	}else{
            		//Set topTab widths
                	this.topTabsChildren.css('width','');
            	}
        	}else{
        		if(this.tabsCount<11&&this.topScroller.enabled==false){
                	//Set topTab widths
                	this.topTabsChildren.css('width','');
            	}else if(this.topScroller.enabled==true){
            		this.topTabsChildren.css('width',width);
            	};
        	}
        },
        //Change toptab roles
        adjustTabsRoles: function (breakPointValue) {
        	/*Remove Last Child Border*/
        	this.applyDataAttribute('date-position','');
    		/*Apply new last child border*/
    		this.topTabsChildren[breakPointValue].attr('data-position', 'last');
        },
        //Reset topTabs css if scroller was running
        resetTopTabs: function () {
        	this.topScroller.moveTo(0,1);
        	/*Reset Scroller wrapper no width*/
        	this.topTabsNav.css('width','100%');
        	//Reset Left of Tabs container
        	this.topTabsNav.css('left','0');
        	//Disable horizontal Scroller
			this.topScroller.enabled = false;
			//Reset left Scrollbar button
			if(this.topTabsLeftScrollShow == true){
				fnb.hyperion.utils.topTabs.leftScrollButton.hide();
				this.topTabsLeftScrollShow = false;
			}
			//Reset right Scrollbar button
			if(this.topTabsRightScrollShow == true&&this.tabsCount<=fnb.hyperion.utils.topTabs.breakPoints[0]){
				fnb.hyperion.utils.topTabs.rightScrollButton.hide();
				this.topTabsRightScrollShow = false;
			}
        },
        //Set topTab left right scroll buttons
        adjustScrollButtonRoles: function () {
        	//Setup Global selectors for left and right scroll buttons if not done already
        	if(fnb.hyperion.utils.topTabs.leftScrollButton =='') fnb.hyperion.utils.topTabs.leftScrollButton = fnb.hyperion.$('#topTabScrollButtonLeft');
        	if(fnb.hyperion.utils.topTabs.rightScrollButton =='') fnb.hyperion.utils.topTabs.rightScrollButton = fnb.hyperion.$('#topTabScrollButtonRight');
        	//Test if scroller is active
        	if(fnb.hyperion.utils.topTabs.topScroller.enabled == true){
        		//Show hide scroller buttons on index of scroller
        		if(fnb.hyperion.utils.topTabs.topScroller.currentIndex == 0){

            		fnb.hyperion.utils.topTabs.leftScrollButton.hide();
            		fnb.hyperion.utils.topTabs.rightScrollButton.show();
            		
            	}else if(fnb.hyperion.utils.topTabs.topScroller.currentIndex > 0&&fnb.hyperion.utils.topTabs.topScroller.currentIndex<fnb.hyperion.utils.topTabs.topScrollerStops){
            		
            		fnb.hyperion.utils.topTabs.leftScrollButton.show();
            		fnb.hyperion.utils.topTabs.rightScrollButton.show();
            		
            	}else if(fnb.hyperion.utils.topTabs.topScroller.currentIndex == fnb.hyperion.utils.topTabs.topScrollerStops){
        			
            		fnb.hyperion.utils.topTabs.rightScrollButton.hide();
        			fnb.hyperion.utils.topTabs.leftScrollButton.show();
         
            	};
            	
        	}else{
        		//Hide all scroller buttons
    			fnb.hyperion.utils.topTabs.leftScrollButton.hide();
    			fnb.hyperion.utils.topTabs.rightScrollButton.hide();
        	}
        	
         },
        //Scroll toptab right
        tabsScrollRight: function () {
        	this.topScroller.next();
        },
        //Scroll toptab left
        tabsScrollLeft: function () {
        	this.topScroller.previous();
        },
        //Enable toptabs
        enable: function () {
        	//Add enabled data attribute
        	this.topTabsWrapper.attr("data-disbaled",false);
        },
        //Disable toptabs
        disable: function () {
        	//Remove enabled data attribute
        	this.topTabsWrapper.attr("data-disbaled",true);
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.topTabs = {};
        }
	};

	//Namespace utils.topTabs
	fnb.namespace('utils.topTabs', topTabs, true);

})();

