///-------------------------------------------///
/// developer: Donovan
///
/// Progress bar Object Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Progress bar Parent function
	///-------------------------------------------///
	function progress() {

	};
	///-------------------------------------------///
	/// Progress bar Methods
	///-------------------------------------------///
	progress.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//List of all intervals
		intervals: [],
		//Active interval
		currentInterval: '',
		//Initial progress bar speed
		speed: 300,
		//Progress bar lowest value
		minValue: 10,
		//Progress bar highest value
		maxValue: 99,
		//Progress bar Deceleration amount
		decel: .50,
		//Progress bar Deceleration devision
		decelDiv: 2,
		//Progress selector
		loader:'',
		//Progress selector
		loaderProgress:'',
		//mainLoader selector
		mainLoader:'',
		//mainLoaderProgress selector
		mainLoaderProgress:'',
		//eziLoader selector
		eziLoader:'',
		//eziLoaderProgress selector
		eziLoaderProgress:'',
		//Initialize progress bar and do selections
		init: function () {
			//Global Loader Wrapper selector
			this.mainLoader = fnb.hyperion.controller.overlayElement.find('.loader');
			//Global Progress Wrapper selector
			this.mainLoaderProgress = fnb.hyperion.controller.overlayElement.find('.loaderProgress');
			//Global Loader Wrapper selector
			this.eziLoader = fnb.hyperion.controller.eziElement.find('.eziLoader');
			//Global Progress Wrapper selector
			this.eziLoaderProgress = fnb.hyperion.controller.eziElement.find('.eziLoaderProgress');
		},
		//Start new instance of progress bar
		startEziLoader: function (type) {
			//Set ezi loader
			this.loader = this.eziLoader;
			//Set ezi loader
			this.loaderProgress = this.eziLoaderProgress;
			//Start ezi loader
			this.start('eziLoader');
		},
		//Start new instance of progress bar
		start: function (type) {
			//Test if instance already exists and destroy
			if (typeof this.intervals[type] !== 'undefined'||this.intervals[type]!=''){
				this.reset(type);
			}
			//Set loader if no type was specified
			if(!type){
				//Set mainLoader loader
				this.loader = this.mainLoader;
				//Set mainLoader loader
				this.loaderProgress = this.mainLoaderProgress;
			}
			//Show Loader
			this.showHideLoader(true);
			//Make current interval
			this.currentInterval = type;
			//Reset current progress
			var percent = this.minValue;
			//Create new interval
			this.createProgressBar(this.speed,percent);
		},
		//Stop instance of progress bar
		stop: function () {
			//Hide Loader
			this.showHideLoader(false);
			//Reset progress bar
			this.reset(this.currentInterval);
		},
		//Reset active progress bar
		reset: function (type) {
			//Reset progress bar to 0
			this.update(this.minValue);
			//Clear current instance
			clearInterval(this.intervals[type]);
			//Clear current instance
			this.intervals[type] == '';
		},
		//Create new interval for progress bar
		createProgressBar: function (speed,percent) {
			var _this = this;
			this.intervals[this.currentInterval] = setInterval (function (){
				//Calculate percent to increase
				percent = percent + Math.floor(Math.random()*10);
				//Test if max value has been reached then kill otherwise continue
				if(percent >= _this.maxValue){
					_this.clearInterval(this.currentInterval);
				}else{
					_this.update(percent,speed);
				}
			},speed);
		},
		//Clear instance of interval
		clearInterval: function (type) {
			//Clear interval of type
			clearInterval(this.intervals[this.currentInterval]);
		},
		//Update progress bar wit new value
		update: function (percent,speed) {
			//Test if loader exists
			if(!this.loaderProgress){
				//Set mainLoader loader
				this.loader = this.mainLoader;
				//Set mainLoader loader
				this.loaderProgress = this.mainLoaderProgress;
			}
			if(this.loaderProgress!=''){
				//Test for max value then reset to max
				percent = percent > this.maxValue ? this.maxValue : percent < this.minValue ? this.minValue : percent;
				//Set progress bar div width
				this.loaderProgress.width(percent + '%');
				//Check if speed needs to decrease
				if(typeof speed!= 'undefined') this.checkSpeed(percent,speed,percent);
			}

		},
		//Show or hide loader
		showHideLoader: function (visible) {
			//Change loader visibility data attribute
			if(this.loader!=''){
				if(visible){
					this.loader.show();
				}else{
					this.loader.hide();
				}
			}

		},
		//Check if speed needs to decrease method
		checkSpeed: function (percent,speed) {
			//Do devision
			var progress = Math.floor(this.maxValue/percent);
			//Current speed
			var decelSpeed = speed;
			//Test if slow down devision has been reached
			if(progress==this.decelDiv){
			    //Clear current instance
				this.clearInterval(this.currentInterval);
			    //New speed calculation
				decelSpeed += speed*this.decel;
			    //Create new instance with new speed
				this.createProgressBar(decelSpeed,percent);
			}
		},
		destroy: function () {
			fnb.hyperion.progress = {};
		}
	};

	//Namespace progress
	fnb.namespace('progress', progress, true);

})();