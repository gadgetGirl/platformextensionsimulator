///-------------------------------------------///
/// developer: Mike 
///
///
/// Subtabs
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="subTab"]', handler: 'fnb.hyperion.utils.subTabs.select(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Subtabs Parent function
	///-------------------------------------------///
	function subTabs() {

	};
	///-------------------------------------------///
	/// Subtabs Methods
	///-------------------------------------------///
	subTabs.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//window size breakpoints
		breakPoints: [3,2,1],
		//active sub tab
		activeGroupId: '',
    	init: function () {
    		//Bind subTabs events
    		bindEvents();
    		//Attach an event on every page load to check for subtabs
    		fnb.hyperion.controller.attachPageEvent('fnb.hyperion.utils.subTabs.initSubTabsScroller()','');
    		
        },
        initSubTabsScroller: function() {
        	this.subTabs = undefined; 
        	//Try and select our subTabs scroller div
        	this.subTabsScrollerParent = fnb.hyperion.$('#subTabsScrollerInner'); 
        	
        	//if the selection is succesful, we have scollable subtabs so inti them
        	if(this.subTabsScrollerParent.length() > 0){
        		
        		//Store a collection of our subtabs
        		this.subTabsChildren = this.subTabsScrollerParent.find('*[data-role="subTab"]');
        		//How many subtabs we have
        		this.subTabsCount = this.subTabsChildren.length();
        		
        		//New Instance of horizontal scroller
    			this.subTabsScroller = new fnb.hyperion.horizontalScroller();
    			this.subTabsScroller.scrollableParent = this.subTabsScrollerParent;
    			this.subTabsScroller.scrollerChildren = '[data-role="subTab"]';
    			this.subTabsScroller.scrollStopWidth = 200;
    			this.subTabsScroller.scrollSpeed = 5;
    			this.subTabsScroller.maxStops = 1;
    			this.subTabsScroller.moveTreshold = 0.20;
    			this.subTabsScroller.bindEvents();
    			this.subTabsScroller.enabled = false;
    			//Add the resize event for the subtabs
    			fnb.hyperion.controller.addResizeFunction(this.resize);
				//Adjust subtabs
    			this.adjustSubTabs(window.innerWidth || document.documentElement.clientWidth,fnb.hyperion.controller.getBreakPosition(window.innerWidth || document.documentElement.clientWidth));
        		
        	}
        	
        },
        resize: function(windowSize, breakpoint) {
        	
        	fnb.hyperion.utils.subTabs.adjustSubTabs(windowSize, breakpoint);
        	
        },
        adjustSubTabs: function(windowSize, breakpoint) {
        	console.log('windowSize')
        	console.log(windowSize)
        	        	
        	/*Get value of the current breakpoint for amount of tabs to be shown*/
        	var currentBreakPointValue = (this.breakPoints[breakpoint-1]) ? this.breakPoints[breakpoint-1] : this.breakPoints[breakpoint];
        	/*Get the scrolling container width*/
        	var currentContainerWidth = this.subTabsScrollerParent.outerWidth();
        	console.log('scrollerWidth')
        	console.log(currentContainerWidth)
        	
        	/*Calculate the new width for the tabs with breakpoint value*/
        	var newTabWidth = Math.round(currentContainerWidth/currentBreakPointValue);
           	/*Calculate new width of the horizontal scroller*/
        	var newScrollingWrapperWidth = Math.round(newTabWidth*this.subTabsCount);
        	/*Apply styles*/
			this.subTabsScroller.enabled = false;
					
			/*Apply Scroller wrapper new width*/
        	if(!isNaN(newScrollingWrapperWidth)){
        		console.log('not isNan')
        		if((currentContainerWidth+(newTabWidth/2))>(newTabWidth*this.subTabsCount)){
        			
        			console.log('1:')
        			console.log(currentContainerWidth+(newTabWidth/2));
        			console.log('2:')      			
        			console.log(newTabWidth*this.subTabsCount);
        			
        			this.subTabsScrollerParent.css('width',"");
            		this.subTabsScrollerParent.css('left',"");
        		}else{
        			this.subTabsScrollerParent.css('width',newScrollingWrapperWidth+'px');
        			this.subTabsScroller.enabled = true;
        		}
        	}else{
        		console.log('isNan')
    			this.subTabsScrollerParent.css('width',"");
        		this.subTabsScrollerParent.css('left',"");
        	}
        	
        },        
        //Do our initial selections once off and create an object to store our selected nodes.
        createGroupObject: function (parentContainer) {
        	var parentObj = this;
        	var subTabs = parentContainer.find('*[data-role="subTab"]')
        	//Loop through our selected subTabs and create an object
        	subTabs.each(function(elem) {
        		var id = elem.attr('id')
        		parentObj.subTabs[id] = {};
        		parentObj.subTabs[id].subTab = elem;
        		parentObj.subTabs[id].subTabContent = fnb.hyperion.$('#'+elem.attr('data-content'));
        		//Check if the sub tab is selected and store it as the current selected tab if it is.
        		if(elem.attr('data-selected') == "true"){
	    		   parentObj.activeGroupId = elem.attr('id');
   
        		};
	    	});
    	},
        select: function (event) {
        	var parentObj = this;
        	var selectedItem = fnb.hyperion.$(event.currentTarget);
        	var selectedItemId = selectedItem.attr('id');
        	console.log(parentObj.subTabs)
         	//Check if we have created our subTabs object and create if we haven't yet.
        	if (parentObj.subTabs === undefined) {
        		parentObj.subTabs = {};
				parentObj.createGroupObject(selectedItem.parent());
			}
        	// Check which subTab is selected and hide it and show the new selection
        	if (parentObj.activeGroupId != selectedItemId) {
        		if(parentObj.subTabs[parentObj.activeGroupId].subTabContent.length() > 0 ){
				parentObj.subTabs[parentObj.activeGroupId].subTabContent.hide();
        		}
				parentObj.subTabs[parentObj.activeGroupId].subTab.attr('data-selected','false');
				if(parentObj.subTabs[selectedItemId].subTabContent.length() > 0){
				parentObj.subTabs[selectedItemId].subTabContent.show();
				}
				parentObj.subTabs[selectedItemId].subTab.attr('data-selected','true');
				parentObj.activeGroupId = selectedItemId;
			} 
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.subTabs = {};
        }
	};

	//Namespace utils.subTabs
	fnb.namespace('utils.subTabs', subTabs, true);

})();