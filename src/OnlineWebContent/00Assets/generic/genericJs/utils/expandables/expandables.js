///-------------------------------------------///
/// developer: Richard
///
/// expandables Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '.expander', handler: 'fnb.hyperion.utils.expandables.expand(event);', preventDefault: true}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// expandables Parent function
	///-------------------------------------------///
	function expandables() {

	};
	///-------------------------------------------///
	/// expandables Methods
	///-------------------------------------------///
	expandables.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB expandables
		init: function () {
	  		//Bind expandables events
	  		bindEvents();
	      },
	      expand: function (event) {
	  		//Var for the selected item
	  		var item = fnb.hyperion.$(event.currentTarget);
	  		//Get the parent of the bound item
	  		var parent = item.parent();
	  		//toggle expanded class to show and hide the content
	  		parent.toggleClass("expanded");
	      },
	      //Remove current object from dom
	      destroy: function () {
	      	fnb.hyperion.utils.expandables = {};
	      }
	};

	//Namespace utils.expandables
	fnb.namespace('utils.expandables', expandables, true);

})();