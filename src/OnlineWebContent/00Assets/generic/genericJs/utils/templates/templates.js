///-------------------------------------------///
/// developer: Donovan
///
/// Templates Object
///-------------------------------------------///
(function() {
    //Build template html
	function getHTML(elements) {
    	var html = '';
		//Test if elements are more than one element loop and build html
		if(elements.length){
		    //Loop each of the template elements and get the html
			for (var i = 0; i < elements.length; i++) {
				//Append individual element html to group html
				html += getElementHTML(elements[i]);
			}
		}else{
			//Set html for a single element
			html = getElementHTML(elements);
		}
    	//Return complete set of html
    	return html;
    };
    //Return element html
    function getElementHTML(element) {
		//Get template by name
    	var elementHtml = fnb.hyperion.htmlTemplates[element.templateName];
		//Set parameters to template html
        return replaceParams(elementHtml,element.templateData);
    };
    //Replace element parameters
    function replaceParams(template, data) {
		//Declare htmlSnippet
    	var htmlSnippet = '';
    	//Declare prop
    	var prop;
    	//Declare regex
    	var regex;
    	//Loop data object and replace parameters
        for (prop in data) {
        	//Create regular expression
            regex = new RegExp('{{' + prop + '}}', 'ig');
        	//Replace param using regular expression
            htmlSnippet = (htmlSnippet || template).replace(regex, data[prop]);
        }
 
        return htmlSnippet;
    };
	///-------------------------------------------///
	/// Templates Parent function
	///-------------------------------------------///
	function templates() {

	};
	///-------------------------------------------///
	/// Templates Methods
	///-------------------------------------------///
	templates.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
        //Add template to dom
        addTemplate: function (object) {
        	//Select target for template
        	var templateTarget = fnb.hyperion.$(object.templateTarget);
        	//Test if target got selected
        	if(templateTarget.length()>0){
            	//Set parent visibilitynb.$(
            	templateTarget.show();
            	//Get template html and set html to template target
            	this.setTarget(templateTarget, getHTML(object.templateElements));
        	}
        },
        //Return html template
	    getTemplate: function (object) {
	    	//Get template html and return to sender
	    	return getHTML(object);
	    },
        //Set the html for a target element
        setTarget: function (target, html) {
        	//Set target html
        	target.html(html);
        },
        //Clear the html from a target element
        clearTarget: function (target) {
        	//Select target
        	var target = fnb.hyperion.$(target);
        	//Clear target
        	if(target.length()>0) target.html("");
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.templates = {};
        }
	};

	//Namespace utils.templates
	fnb.namespace('utils.templates', templates, true);

})();