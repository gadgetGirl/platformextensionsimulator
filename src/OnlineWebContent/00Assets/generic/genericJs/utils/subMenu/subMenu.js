///-------------------------------------------///
/// developer: Richard
///
/// developer 2: Donovan
///
/// subMenu Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [	{type: 'frame', listener: document, events:'click', selector: '[data-role="subMenuItem"]', handler: 'fnb.hyperion.utils.subMenu.selectSubMenuItem(event);', preventDefault: true},
	              		{type: 'frame', listener: document, events:'click', selector: '[data-role="subMenuAtricleItem"]', handler: 'fnb.hyperion.utils.subMenu.selectSubMenuItem(event);'},
	              		{type: 'frame', listener: document, events:'click', selector: '[data-role="subMenuHeading2"]', handler: 'fnb.hyperion.utils.subMenu.toggleLevel(event);'},
	              		{type: 'frame', listener: document, events:'click', selector: '[data-role="subMenuSetLink"]', handler: 'fnb.hyperion.utils.subMenu.selectSubMenuSet(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// subMenu Parent function
	///-------------------------------------------///
    function subMenu() {
		
	};
	///-------------------------------------------///
	/// subMenu Methods
	///-------------------------------------------///
	subMenu.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Subtab active flag
		active: false,
		//Subtab active element
		activeSubtab: '',
		//
		init: function () {
			bindEvents();
		},
		select: function (event) {
			//Get di of selected toptab
			var toptabId = event.selectedTabId;
    		//Select subtab
    		var subMenu = fnb.hyperion.$("#subMenu_" + toptabId);
    		//Get subMenu id
    		var submenuId = subMenu.prop('id');
    		//Test if 
			if(this.active&&submenuId!=this.activeSubtab){
				//Hide op subtab
				fnb.hyperion.controller.raiseEvent('hideSubMenu');
				//Set current actibe subtab
				this.activeSubtab = submenuId;
				//Show subtab
				fnb.hyperion.controller.raiseEvent('showSubMenu');
			}else if(this.active&&submenuId==this.activeSubtab){
				//Hide op subtab
				fnb.hyperion.controller.raiseEvent('hideSubMenu');
				//Set current actibe subtab
				this.activeSubtab = '';
			}else{
				//Set current actibe subtab
				this.activeSubtab = submenuId;
				//Show subtab
				fnb.hyperion.controller.raiseEvent('showSubMenu');
			};
		},
		selectSubMenuItem: function (event) {
			//Create a holder for a our item
    		var item = fnb.hyperion.$(event.currentTarget);
    		//Store the href attribute of our item.
    		var url = item.attr('href');
    		//Get third party data attribute
    		var thirdParty = item.attr('data-thirdParty');
    		//If the url is not empty load the url
    		if (url != '' && thirdParty != 'true') {
    			fnb.hyperion.controller.raiseEvent('loadPage',{url: url,target:fnb.hyperion.controller.pageContentElement});
    		}else{
    			fnb.hyperion.controller.redirect(url);
    		};
        },
        //Toggle children items
        toggleLevel: function (event) {
        	//Find Target item
        	var expandableItem = fnb.hyperion.$(event.currentTarget);
        	//Switch sate attribute
          	if(expandableItem.attr('data-state') ==  'inactive') {
          		expandableItem.attr('data-state','active');
        	}else{
        		expandableItem.attr('data-state','inactive');
        	}
        },
		show: function () {
			//Show subtab parent wrapper
			fnb.hyperion.controller.subTabWrapper.show();
			//Hide page contant
			fnb.hyperion.controller.pageContentContainerElement.hide();
			//Hide submenu
			fnb.hyperion.$("#" + this.activeSubtab).show();
			//Set active flag
			this.active = true;
		},
		hide: function () {
			if(this.active){
				//Hide subtab parent wrapper
				fnb.hyperion.controller.subTabWrapper.hide();
				//Show page contant
				fnb.hyperion.controller.pageContentContainerElement.show();
				//Hide submenu
				fnb.hyperion.$("#" + this.activeSubtab).hide();
				//Set active flag
				this.active = false;
			}
		},
		destroy: function () {
		}
	};

	//Namespace utils.subMenu
	fnb.namespace('utils.subMenu', subMenu, true);

})();
