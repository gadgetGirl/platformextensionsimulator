///-------------------------------------------///
/// developer: Donovan
///
/// Notifications Container Object
///-------------------------------------------///
(function() {
    //Bind event for current object
	function bindEvents() {
    	//List of events for this module
    	var events = [{type: 'frame', listener: document, events:'click', selector: '.notificationsClose', handler: 'fnb.hyperion.utils.notifications.hide(event);'}];
    	//Append events to actions module
    	fnb.hyperion.controller.attachActionEvents(events);
    };
	///-------------------------------------------///
	/// Notifications Container Parent function
	///-------------------------------------------///
	function notifications() {

	};
	///-------------------------------------------///
	/// Notifications Container Buttons Methods
	///-------------------------------------------///
	notifications.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Is notifications currently active
		active:false,
		//Selector for notification text wrapper
		notificationText: '',
		//Selector for notification inner wrapper
		notificationInner: '',
		//Init FNB Errors
    	init: function () {
    		console.log('Utils notifications init');
    		//Bind Error events
    		bindEvents();
    		//Select Error text wrapper
    		this.notificationsText = fnb.hyperion.controller.notificationsElement.find('.notificationsMessages');
    		//Select Error text wrapper
    		this.notificationInner = fnb.hyperion.controller.notificationsElement.find('.notificationsInner');
	    },
        //Show notification message
        show: function (notification) {
        	//If notifications is not active display it
			if(!this.active){
				//Update flag
				this.active = true;
	        	//Set notification wrapper visibility
				this.notificationsText.html('');
	        	fnb.hyperion.controller.notificationsElement.show();
	        	//Set notification text
	        	if(notification) this.notificationsText.html(notification);
				//Change overlay data attribute
				fnb.hyperion.controller.clipOverflow(true);
			}
        },
        //Hide notification message
        hide: function () {
        	//If overlay is active hide it
			if(this.active){
				//Update flag
				this.active = false;
	        	//Set notification wrapper visibility
	        	fnb.hyperion.controller.notificationsElement.hide();
				//Change overlay data attribute
				fnb.hyperion.controller.clipOverflow(false);
				//Clear notifications inner
				fnb.hyperion.utils.notifications.notificationInner.html("");
			}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.notifications = {};
        }
	};

	//Namespace utils.notifications
	fnb.namespace('utils.notifications', notifications, true);

})();