///-------------------------------------------///
/// developer: Donovan
///
/// Tracking Object
///-------------------------------------------///
(function() {
	//Init Object
	var initObj = [
   	               /* {node: 'eVar8', data: getVisitorType},
	        		{node: 'eVar9', data: getVisitNumber}*/
	        	];
	//Standard Object
	var standardWrapper = [
		   	                {node: 'sc_xml_ver', data: '1.0'},
			        		{node: 'pageURL', data: getPageUrl},
			        		{node: 'pageName', data: getPageName},
			        		{node: 'pageType', data: getPageType},
			        		{node: 'userAgent', data: getUserAgent},
			        		{node: 'visitorID', data: getUserId},
			        		{node: 'timestamp', data: getTimeStamp},
			        		{node: 'reportSuiteID', data: getReportSuiteId}
			        	];
	//Wrapper for listener events
	var listeners = {
		'loadUrlLoadUrlComplete':{
	        params: standardWrapper
		}
	};
	//Wrapper for events
	var events = [
					{event: 'event1', test: isSearches , eVars:"eVar1", props:"prop9,prop10"},
					{event: 'event2', test: isNullSearches, eVars:"eVar1"},
					{event: 'event3', test: getSearchClick, clickListener: ".ui-searchresultPage", clickEvent: registerClick},
					{event: 'event4', test: isPageView, eVars:"eVar35,eVar3,eVar4,eVar5,eVar6,eVar7,eVar8,eVar9,eVar31", props:"channel,prop1,prop2,prop4,prop5,prop6,prop7,prop8,prop11,prop12,prop13,prop19,prop22,hier1"},
					{event: 'event5', test: isLogin},
					{event: 'event6', test: isProductView, eVars:"products"},
					{event: 'event7', test: isSelfService, eVars:"eVar34"},
					{event: 'event8', test: isToolUsage, eVars:"eVar10", props:"prop14"},
					{event: 'event9', test: isToolUsageComplete},
					{event: 'event10', test: isRegistrationStart},
					{event: 'event11', test: isRegistrationComplete},
					{event: 'event12', test: isActivationStart},
					{event: 'event13', test: isActivationComplete},
					{event: 'event14', test: isFormStart},
					{event: 'event15', test: isFormComplete},
					{event: 'event16', test: isApplicationStart},
					{event: 'event17', test: isApplicationReferred},
					{event: 'event18', test: isApplicationDeclined},
					{event: 'event19', test: isApplicationComplete},
					{event: 'event20', test: isLeadsStart , eVars:"eVar15"},
					{event: 'event21', test: isLeadsComplete},
					{event: 'event22', test: isComplaints, eVars:"eVar30", props:"prop20"},
					{event: 'event23', test: getSocialClick, clickListener: ".ui-social-btn", clickEvent: registerClick , eVars:"eVar17"},
					{event: 'event24', test: getDownloadClick, clickListener: ".ui-pdfDownload-link", clickEvent: registerClick , eVars:"eVar32"},
					{event: 'event25', test: isBranchLocator},
					{event: 'event26', test: isFormsPage},
					{event: 'event27', test: isMoreInfo},
					{event: 'event28', test: isOptionalServices},
					{event: 'event29', test: isRetrieveApplication},
					{event: 'event30', test: isLoggedInApplication},
					{event: 'event31', test: isApplicationStep1},
					{event: 'event32', test: isApplicationStep2},
					{event: 'event33', test: isApplicationStep3},
					{event: 'event34', test: isApplicationTermsDecline}
				];
	//Wrapper for evars
	var evars = {
					products : getProducts,
					campaign : getTrackingCode,
					eVar1 : getSearchValue,
					eVar2 : getInternalCampaign,
					eVar3 : getHour,
					eVar4 : getDayName,
					eVar5 : getDayType,
					eVar6 : getPageUrl,
					eVar7 : getPreviousPageName,
					eVar8 : getVisitorType,
					eVar9 : getVisitNumber,
					eVar10 : getToolType,
					eVar11 : getApplicationId,
					eVar12 : getUserId,
					eVar13 : getSegmentation,
					eVar14 : getCustomerType,
					eVar15 : getLeadFormName,
					eVar16 : getRegistrationType,
					eVar17 : getSosialPlatform,
					eVar18 : getPageName,
					eVar19 : getFunctionType,
					eVar20 : getSelfServiceType,
					eVar21 : getBranchLocatorSearchValue,
					eVar22 : getLoginPageOrigin,
					eVar23 : getApplicationFormReference,
					eVar24 : getLoggedInStatus,
					eVar25 : getOptionalServiceType,
					eVar26 : getPreviousPageLoadTime,
					eVar27 : getActivationTotalTime,
					eVar28 : getProductType,
					eVar29 : getProduct,
					eVar30 : getFormName,
					eVar31 : getUserDevice,
					eVar32 : getDownloadName,
					eVar33 : getTimeToComplete,
					eVar34 : getSelfServiceType,
					eVar35 : getBusinessUnit
				};
	//Wrapper for props
	var props = {
					channel : getChannel,
					prop1 : getCatagory,
					prop2 : getDepartment,
					prop3 : '',
					prop4 : getPageType,
					prop5 : getHour,
					prop6 : getDayName,
					prop7 : getDayType,
					prop8 : getPreviousPageName,
					prop9 : getSearchValue,
					prop10 : getSearchResultCount,
					prop11 : getPageUrl,
					prop12 : getVisitorType,
					prop13 : getVisitNumber,
					prop14 : getToolType,
					prop15 : getUserId,
					prop16 : getRegistrationType,
					prop17 : getSosialPlatform,
					prop18 : getPageName,
					prop19 : getLoginStatus,
					prop20 : getFormName,
					prop21 : getBranchLocatorSearchValue,
					prop22 : getBusinessUnit,
					hier1 : getPageName
				};
	//Setup user tracking object
	function getUserObject() {
		//Clear user object
		var userObject = {};
		//Check if repeat visitor
		if(!fnb.hyperion.cookies.get('userId')){
			//Create userid
			userObject.userId = createGuid();
    		//Create visit date
			userObject.visitDate = getDate();
    		//Create last visited
			userObject.lastVisited = getDate();
    		//Create visits
			userObject.visits = 0;
    	}else{
    		//Create userid
			userObject.userId = fnb.hyperion.cookies.get('userId');
    		//Create last visited
			userObject.lastVisited = fnb.hyperion.cookies.get('visitDate');
    		//Create visit date
			userObject.visitDate = getDate();
    		//Create visits
			userObject.visits = parseInt(fnb.hyperion.cookies.get('visits'))+1;
    	}
		//Set cookies
		setCookies(userObject);
		//retrun user object
		return userObject;
    };
    //Generate user id
    //For temp use until backend finished
	function createGuid() {
		//Generate random number
		function randomNumber() {
		   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
		}
		//Create string to return as guid
		return (randomNumber()+randomNumber()+"-"+randomNumber()+"-"+randomNumber()+"-"+randomNumber()+"-"+randomNumber()+randomNumber()+randomNumber());
    };
    //Return formatted date
	function getDate() {
		//Create date object
		var date = new Date();
		//Return correctly formatted string
		return date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();
    };
    //For temp use until backend finished
	function setCookies(obj) {
		fnb.hyperion.cookies.set(obj);
    };
    //Return xml version from xml object
	function getXMLversion() {
		//retrun xml version
		return fnb.hyperion.XMLWriter.version;
    };
    
    ///////////////////////////
    //EVAR METHODS
    ///////////////////////////
    //Return Products
	function getProducts() {
		//Select ui content
    	var selection = document.getElementById('productGrid');
		//Declare product
    	var product = '';
    	//Test if multiple products
    	if(selection){
    		var collectProducts = getElementByClass('ui-compareHeading',selection);
    		//Loop products
    		for (var i = 0, length = collectProducts.length; i < length; i++) {
    			product = (product=="") ? ';'+collectProducts[i].getAttribute('data-productname') : product + ',' + collectProducts[i].getAttribute('data-productname');
			};
    	}else{
    		var productSelected = (document.getElementById('productSelected')) ? document.getElementById('productSelected').getAttribute('selectedproduct') : '';
    		//Get single product
    		product = (productSelected!='') ? ';'+document.getElementById('productSelected').getAttribute('selectedproduct') : '';
    	}
		//Set Products
		var Products = (product) ? product : '';
		//retrun Products
		return Products;
    };
    //Return TrackingCode
	function getTrackingCode() {
		//Set TrackingCode
		var TrackingCode = '';
		//retrun TrackingCode
		return TrackingCode;
    };
    //Return SearchValue
	function getSearchValue() {
		//Search value
		var searchValueText = "";
		//Select searchInputWrapper 
    	var selection = getElementByClass('ui-searchresult-header-string');
    	//Test if selection war made
    	if(selection.length===0){
    		//No results found make new selection
    		selection = getElementByClass('noResults');
    		//Get text value
    		searchValueText = 'Zero:'+selection[0].childNodes[0].nextElementSibling.firstElementChild.innerHTML;
    	}else{
    		searchValueText = selection[0].innerHTML;
    	}
		//Set SearchValue
		var SearchValue = searchValueText;
		//retrun SearchValue
		return SearchValue;
    };
    //Return number of found results
    function getSearchResultCount() {
		//Select searchInputWrapper 
    	var selection = getElementByClass('ui-searchresult-column');
    	//Get count
    	var resultCount = 0;
    	//Test if selection was found
    	if(selection.length>0){
    		resultCount = getElementByClass('ui-searchresultPage',selection[0]);
    	}
    	//Set SearchCount
		var SearchCount = (resultCount.length>0) ? resultCount.length+'' : '';
    	//retrun SearchValue
		return SearchCount;
    }
    //Return InternalCampaign
	function getInternalCampaign() {
		//Set InternalCampaign
		var InternalCampaign = '';
		//retrun InternalCampaign
		return InternalCampaign;
    };
    //Return Year
	function getYear() {
		//Create date object
		var date = new Date();
		//Set year
		var Year = date.getFullYear();
		//retrun Year
		return Year;
    };
    //Return Month
	function getMonth() {
		//Create date object
		var date = new Date();
		//Set Month
		var Month = (date.getMonth()+1);
		//retrun Month
		return Month;
    };
    //Return Day name
	function getDateNumber() {
		//Create date object
		var date = new Date();
		//Set Date
		var DateNumber = date.getDate()+'';;
		//retrun Date
		return DateNumber;
    };
    //Return Day
	function getDay() {
		//Create date object
		var date = new Date();
		//Set Day
		var Day = date.getDay()+'';;
		//retrun Day
		return Day;
    };
    //Return getDayName
	function getDayName() {
		//List of days
		var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
		//Create date object
		var date = new Date();
		//Set Day
		var DayName = dayNames[date.getDay()];
		//retrun Day
		return DayName;
    };
    //Return getDayType
	function getDayType() {
		//Create date object
		var date = new Date();
		//Set Day
		var DayType = (date.getDay()>0&&date.getDay()<6) ? 'Weekday': 'Weekend';
		//retrun Day
		return DayType;
    };
    //Return Channel
    function getChannel() {
    	//Select ui content
    	var selection = document.getElementById('ui-contentWrap').getAttribute("data-pagehierarchy");
    	//Split Val
    	var hiearchy = selection.split(':');
    	//Declare Channel
    	var Channel = '';
    	//Test if home page
    	if(hiearchy[2].indexOf('Home')!=-1){
    		Channel = "Home";
    	}else{
    		//Set Channel
    		Channel = (hiearchy.length>0) ? hiearchy[0]: '';
    	}
		//retrun Channel
		return Channel;
    };    
    //Return Catagory
    function getCatagory() {
    	//Select ui content
    	var selection = document.getElementById('ui-contentWrap').getAttribute("data-pagehierarchy");
    	//Split Val
    	var hiearchy = selection.split(':');
    	//Declare Catagory
    	var Catagory = '';
    	//Test if home page
    	if(hiearchy[2].indexOf('Home')!=-1){
    		Catagory = "Home";
    	}else{
			//Set Catagory
			Catagory = (hiearchy.length>0) ? hiearchy[0]+':'+hiearchy[1] : '';
    	}
		//retrun Catagory
		return Catagory;
    }; 
    
    //Return Department
    function getDepartment() {
    	//Select ui content
    	var selection = document.getElementById('ui-contentWrap').getAttribute("data-pagehierarchy");
    	//Declare Catagory
    	var Department = '';
    	//Test if home page
    	if(selection.indexOf('Home')!=-1){
    		Department = "Home";
    	}else{
    		//Set Department
    		Department = (selection) ? selection : '';
    	}
		//retrun Department
		return Department;
    };  
    //Return Hour
	function getHour() {
		//Create date object
		var date = new Date();
		//Set Hour
		var Hour = date.getHours()+'';
		//retrun Hour
		return Hour;
    };
    //Return Minutes
	function getMinutes() {
		//Create date object
		var date = new Date();
		//Set Minutes
		var Minutes = date.getMinutes()+'';;
		//retrun Minutes
		return Minutes;
    };
    //Return Seconds 
	function getSeconds() {
		//Create date object
		var date = new Date();
		//Set Seconds
		var Seconds = date.getSeconds()+'';;
		//retrun Seconds
		return Seconds;
    };
    //Return Milliseconds 
	function getMilliseconds() {
		//Create date object
		var date = new Date();
		//Set Milliseconds
		var Milliseconds = date.getMilliseconds()+'';;
		//retrun Milliseconds
		return Milliseconds;
    };
    //Return Weekday
	function getWeekday() {
		//Set Weekday
		var Weekday = '';
		//retrun Weekday
		return Weekday;
    };
    //Return page url
	function getPageUrl() {
		//Set urleventArgs.url.indexOf('Controller')
		var url = (fnb.hyperion.tracking.loadObject) ? fnb.hyperion.tracking.loadObject.url.split('&')[0] : '';
		//retrun page url
		return url;
    };
    //Return login status
    function getLoginStatus() {
		//Return login status
		return 'Guest';
    };
    //Return previous page name
    function getPreviousPageName() {
		//Return previous page name
		return fnb.hyperion.tracking.previousPageName;
    };
    //Return visitor type
    function getVisitorType() {
    	//Set visitor type
		var visitorType = (fnb.hyperion.tracking.userObject.visits == '0') ? 'New' : 'Repeat';
    	//Return visitor type
		return visitorType;
    };
    //Return visit number
    function getVisitNumber() {
		//Return visit number
		return fnb.hyperion.tracking.userObject.visits+'';
    };
    //Return ToolType
	function getToolType() {
    	//Select Apply now
    	var selection = document.getElementById('productSelected');
		//Set ToolType
		var ToolType = '';
		//Test if selection was found
		if(selection){
			//Get adat attribute
			ToolType = selection.getAttribute("selectedproduct");
		}
		//retrun ToolType
		return ToolType;
    };
    //Return ApplicationId
	function getApplicationId() {
		//Set ApplicationId
		var ApplicationId = '';
		//retrun ApplicationId
		return ApplicationId;
    };
    //Return user id
    function getUserId() {
		//Return user id
		return fnb.hyperion.tracking.userObject.userId;
    };
    //Return Segmentation
	function getSegmentation() {
		//Set Segmentation
		var Segmentation = '';
		//retrun Segmentation
		return Segmentation;
    };
    //Return CustomerType
	function getCustomerType() {
		//Set CustomerType
		var CustomerType = '';
		//retrun CustomerType
		return CustomerType;
    };
    //Return LeadFormName
	function getLeadFormName() {
		//Select Lead form
    	var selection = getElementByClass('ui-leadForm');
    	//Get lead form name hilder
    	var nameHolder = getElementByClass('ui-form',selection[0]);
		//Set LeadFormName
		var LeadFormName = (nameHolder.length>0) ? nameHolder[0].getAttribute('name') : '';
		//retrun LeadFormName
		return LeadFormName;
    };
    //Return RegistrationType
	function getRegistrationType() {
		//Set RegistrationType
		var RegistrationType = '';
		//retrun RegistrationType
		return RegistrationType;
    };
    //Return SosialPlatform
	function getSosialPlatform() {
		//Set clean platform var
		var platform = "";
		//Test for platforms
		if(fnb.hyperion.tracking.whatClickEvent.className.indexOf('ui-social-fb')!=-1){
			platform = "Facebook";
		}else if(fnb.hyperion.tracking.whatClickEvent.className.indexOf('ui-social-twitter')!=-1){
			platform = "Twitter";
		}else if(fnb.hyperion.tracking.whatClickEvent.className.indexOf('ui-social-google')!=-1){
			platform = "Google";
		}else if(fnb.hyperion.tracking.whatClickEvent.className.indexOf('ui-social-linkedin')!=-1){
			platform = "LinkedIn";
		}else if(fnb.hyperion.tracking.whatClickEvent.className.indexOf('ui-social-youtube')!=-1){
			platform = "YouTube";
		}
		//Set SosialPlatform
		var SosialPlatform = platform;
		//retrun SosialPlatform
		return SosialPlatform;
    };
    //Return page name
	function getPageName() {
		//Select ui content
    	var selection = document.getElementById('ui-contentWrap').getAttribute("data-pagehierarchy");
    	//Split Val
    	var hiearchy = selection.split(':');
    	//Declare pageName
    	var pageName = selection;
    	//Test if home page
    	if(hiearchy[2].indexOf('Home')!=-1){
    		pageName = "Home";
    	}
    	//Set previous page title
    	fnb.hyperion.tracking.previousPageName = (fnb.hyperion.tracking.currentPageName!='') ? fnb.hyperion.tracking.currentPageName : '';
    	//Get page title
    	fnb.hyperion.tracking.currentPageName = '';
    	//Test for empty title
    	if(pageName!='') fnb.hyperion.tracking.currentPageName = pageName;
		//retrun page name
		return fnb.hyperion.tracking.currentPageName;
    };
    //Return page type
    function getPageType() {
    	//Select noResults 
    	var selection = getElementByClass('errorPageWrapper');
    	//Declare page type
    	var pageType = "";
    	//Test if error page was found
    	if(selection.length>0){
        	//Set page type
        	pageType = (selection.length>0) ? 'errorPage' : '';
    	}else{
    		selection = document.getElementById('ui-contentWrap').getAttribute("data-pagetype");
        	//ToolUsage
    		pageType = selection;
    	}
		//retrun page name
		return pageType;
    };
    //Return FunctionType
	function getFunctionType() {
		//Set FunctionType
		var FunctionType = '';
		//retrun FunctionType
		return FunctionType;
    };
    //Return SelfServiceType
	function getSelfServiceType() {
		//Set SelfServiceType
		var SelfServiceType = '';
		//retrun SelfServiceType
		return SelfServiceType;
    };
    //Return BranchLocatorSearchValue
	function getBranchLocatorSearchValue() {
		//Set BranchLocatorSearchValue
		var BranchLocatorSearchValue = '';
		//retrun BranchLocatorSearchValue
		return BranchLocatorSearchValue;
    };
    //Return LoginPageOrigin
	function getLoginPageOrigin() {
		//Set LoginPageOrigin
		var LoginPageOrigin = '';
		//retrun LoginPageOrigin
		return LoginPageOrigin;
    };
    //Return ApplicationFormReference
	function getApplicationFormReference() {
		//Set ApplicationFormReference
		var ApplicationFormReference = '';
		//retrun ApplicationFormReference
		return ApplicationFormReference;
    };
    //Return LoggedInStatus
	function getLoggedInStatus() {
		//Set LoggedInStatus
		var LoggedInStatus = '';
		//retrun LoggedInStatus
		return LoggedInStatus;
    };
    //Return OptionalServiceType
	function getOptionalServiceType() {
		//Set OptionalServiceType
		var OptionalServiceType = '';
		//retrun OptionalServiceType
		return OptionalServiceType;
    };
    //Return PreviousPageLoadTime
	function getPreviousPageLoadTime() {
		//Set PreviousPageLoadTime
		var PreviousPageLoadTime = '';
		//retrun PreviousPageLoadTime
		return PreviousPageLoadTime;
    };
    //Return ActivationTotalTime
	function getActivationTotalTime() {
		//Set ActivationTotalTime
		var ActivationTotalTime = '';
		//retrun ActivationTotalTime
		return ActivationTotalTime;
    };
    //Return ProductType
	function getProductType() {
		//Set ProductType
		var ProductType = '';
		//retrun ProductType
		return ProductType;
    };
    //Return Product
	function getProduct() {
		//Set Product
		var Product = '';
		//retrun Product
		return Product;
    };
    //Return FormName
	function getFormName() {
		var selection = getElementByClass('ui-form');
    	//Test if its a form
    	if(selection.length>0){
    		var teal = getElementByClass('teal7BorderBottom',selection[0])
    		name = teal[0].innerText;
    	}
		//Set FormName
		var FormName = (name) ? name.replace('\n','') : '';
		//retrun FormName
		return FormName;
    };
    //Return UserDevice
	function getUserDevice() {
		//Set UserDevice
		var UserDevice = navigator.platform.match(/(Mac68K|MacPPC|MacIntel|Win32|Linux)/i)? 'PC' : 'mobile';
		//retrun UserDevice
		return UserDevice;
    };
    //Return DownloadName
	function getDownloadName() {
		//Set DownloadName
		var DownloadName = fnb.hyperion.tracking.whatClickEvent.getAttribute('href').split('/');
		//get last element
		DownloadName = DownloadName[DownloadName.length-1];
		//retrun DownloadName
		return DownloadName;
    };
    //Return TimeToComplete
	function getTimeToComplete() {
		//Set TimeToComplete
		var TimeToComplete = '';
		//retrun TimeToComplete
		return TimeToComplete;
    };
    //Return report suite id
    function getReportSuiteId() {
		//Return report suite id
		return fnb.hyperion.tracking.id;
    };
    //Return user agent
    function getUserAgent() {
		//Return user agent
		return navigator.userAgent;
    };
    //Return timestamp
    function getTimeStamp() {
    	//Get month
    	var rawMonth = getMonth()+'';
    	var month = (rawMonth.length==1) ? '0'+rawMonth : rawMonth;
    	//Get day
    	var rawDay = getDateNumber()+'';
    	var day = (rawDay.length==1) ? '0'+rawDay : rawDay;
    	//Get hour
    	var rawHour = getHour()+'';
    	var hour = (rawHour.length==1) ? '0'+rawHour : rawHour;
    	//Get minute
    	var rawMinute = getMinutes()+'';
    	var minute = (rawMinute.length==1) ? '0'+rawMinute : rawMinute;
    	//Get second
    	var rawSecond = getSeconds()+'';
    	var second = (rawSecond.length==1) ? '0'+rawSecond : rawSecond;
    	//Get time
    	var time = getYear()+'-'+month+'-'+day+'T'+hour+':'+minute+':'+second;
    	//Return timestamp
		return time+'';
    };
    //Return events
    function getEvents() {
    	//Set date
    	var events = '';
    	//Return events
		return events;
    };
    //Return BusinessUnit
    function getBusinessUnit() {
    	//Return BusinessUnit
		return fnb.hyperion.tracking.businessUnit;
    };
    
    ///////////////////////////
    //UTILITIES
    ///////////////////////////
    
    //Get element by classname
    function getElementByClass (className, parent) {
		//Test for parent
    	parent || (parent = document);
    	//Select all children
		var descendants= parent.getElementsByTagName('*'), i=-1, e, result=[];
		//Loop children an try to match class
		while (e=descendants[++i]) {
			((' '+(e['class']||e.className)+' ').indexOf(' '+className+' ') > -1) && result.push(e);
		}
		//Return selection
		return result;
	}
    //Test if element has a specific class
    function hasClass (selector) {
    	var className = " " + selector + " ";
        for (var i = 0, l = this.length; i < l; i++) {
            if ((" " + this[i].className + " ").replace(rclass, " ").indexOf(className) > -1) {
                return true;
            };
        }
        return false;
	}

    ///////////////////////////
    //EVENTS METHODS
    ///////////////////////////
    //Register click event
    function registerClick(event) {
    	//Create blank event string
    	var eventString = '';
    	//Get target selected
    	var target = event.currentTarget
    	//Loop event for matching selector
    	for (var event in fnb.hyperion.tracking.events) {
    		//Set current events objerct
    		var eventObject = events[event];
    		//Test if click event
    		if(eventObject.clickListener){
    			//Clean selector string
    			var cleanlistener = eventObject.clickListener.replace(".","");
    			//Test if target matches selection
    			if($(target).hasClass(cleanlistener)) eventString = eventObject.event;
    		}
    		
        	//Set what clicked flag
        	fnb.hyperion.tracking.whatClickEvent = target;
    	};
    	//Set was clicked flag
    	fnb.hyperion.tracking.wasClickEvent = eventString;
    };
    //Is isSearches
    function isSearches(event) {
    	//Select searchInputWrapper 
    	var selection = getElementByClass('ui-searchresult-column');
    	//isSearches reult
    	var Searches = (selection.length>0) ? event : '';
    	//return ProductView
		return Searches;
    };
    //Is isNullSearches
    function isNullSearches(event) {
    	//Select noResults 
    	var selection = getElementByClass('noResults');
    	//isNullSearches
    	var NullSearches = (selection.length>0) ? 'event1,'+event : '';
    	//return NullSearches
		return NullSearches;
    };
    //Is getSearchClick
    function getSearchClick(event) {
    	//getSearchClick
    	var getSearchClick = (fnb.hyperion.tracking.wasClickEvent==event) ? event : '';
    	//Return getSearchClick
		return getSearchClick;
    };
    //Is isPageView
    function isPageView(event) {
    	//isPageView
    	var PageView = event;
    	//return PageView
		return PageView;
    };
    //Is isLogin
    function isLogin(event) {
    	//isLogin
    	var Login = '';
    	//return Login
		return Login;
    };
    //Is isProductView
    function isProductView(event) {
    	//Select Apply now
    	var parent = document.getElementById('productApplyNow');
    	var selection = getElementByClass('ui-applyOnlineBtt',selection);
    	//isProductView
    	var ProductView = (selection.length>0) ? 'prodView,'+event : '';
    	//return ProductView
		return ProductView;
    };
    //Is isSelfService
    function isSelfService(event) {
    	//isSelfService
    	var SelfService = '';
    	//return SelfService
		return SelfService;
    };
    //Is isToolUsage
    function isToolUsage(event) {
    	//Select Apply now
    	var selection = document.getElementById('ui-contentWrap');
    	
    	var pagetype = ""
    		
    	if(selection){
    		pagetype = selection.getAttribute("data-pagetype");
    	}
    	//ToolUsage
    	var ToolUsage = pagetype ? (pagetype=="tool") ? event : '' : '';
    	//return ToolUsage
		return ToolUsage;
    };
    //Is isToolUsageComplete
    function isToolUsageComplete(event) {
    	//isToolUsageComplete
    	var ToolUsageComplete = '';
    	//return ToolUsageComplete
		return ToolUsageComplete;
    };
    //Is isRegistrationStart
    function isRegistrationStart(event) {
    	//isRegistrationStart
    	var RegistrationStart = '';
    	//return RegistrationStart
		return RegistrationStart;
    };
    //Is isRegistrationComplete
    function isRegistrationComplete(event) {
    	//Set isRegistrationComplete
    	var RegistrationComplete = '';
    	//return RegistrationComplete
		return RegistrationComplete;
    };
    //Is isActivationStart
    function isActivationStart(event) {
    	//Set isActivationStart
    	var ActivationStart = '';
    	//return ActivationStart
		return ActivationStart;
    };
    //Is isActivationComplete
    function isActivationComplete(event) {
    	//Set isActivationComplete
    	var ActivationComplete = '';
    	//return ActivationComplete
		return ActivationComplete;
    };
    //Is isFormStart
    function isFormStart(event) {
    	//Set isFormStart
    	var FormStart = '';
    	//return FormStart
		return FormStart;
    };
    //Is isFormComplete
    function isFormComplete(event) {
    	//Set isFormComplete
    	var FormComplete = '';
    	//return FormComplete
		return FormComplete;
    };
    //Is isApplicationStart
    function isApplicationStart(event) {
    	//Set isApplicationStart
    	var ApplicationStart = '';
    	//return ApplicationStart
		return ApplicationStart;
    };
    //Is isApplicationReferred
    function isApplicationReferred(event) {
    	//Set isApplicationReferred
    	var ApplicationReferred = '';
    	//return ApplicationReferred
		return ApplicationReferred;
    };
    //Is isApplicationDeclined
    function isApplicationDeclined(event) {
    	//Set isApplicationDeclined
    	var ApplicationDeclined = '';
    	//return ApplicationDeclined
		return ApplicationDeclined;
    };
    //Is isApplicationComplete
    function isApplicationComplete(event) {
    	//Set isApplicationComplete
    	var ApplicationComplete = '';
    	//return ApplicationComplete
		return ApplicationComplete;
    };
    //Is isLeadsStart
    function isLeadsStart(event) {
    	//Select Lead form
    	var selection = getElementByClass('ui-leadForm');
    	//Set isLeadsStart
    	var LeadsStart = (selection.length>0) ? event : '';
    	//return LeadsStart
		return LeadsStart;
    };
    //Is isLeadsComplete
    function isLeadsComplete(event) {
    	//Set isLeadsComplete
    	var LeadsComplete = '';
    	//return LeadsComplete
		return LeadsComplete;
    };
    //Is isComplaints
    function isComplaints(event) {
    	//Select branchInput
    	var selection = getElementByClass('ui-form');
    	//Test if its a form
    	if(selection.length>0){
    		name = selection[0].name;
    	}
    	//Set isComplaints
    	var Complaints = (name) ? (name=="FNB Shared Services Call Me Back") ? event: '' : '';
    	//Return events
		return Complaints;
    };
    //Is getSocialClick
    function getSocialClick(event) {
    	//Set getSocialClick
    	var getSocialClick = (fnb.hyperion.tracking.wasClickEvent==event) ? event : '';
    	//Return getSocialClick
		return getSocialClick;
    };
    //Is getDownloadClick
    function getDownloadClick(event) {
    	//Set getDownloadClick
    	var getDownloadClick = (fnb.hyperion.tracking.wasClickEvent==event) ? event : '';
    	//Return getDownloadClick
		return getDownloadClick;
    };
    //Is isBranchLocator
    function isBranchLocator(event) {
    	//Select branchInput
    	var selection = document.getElementById('branchInput');
    	//Set isBranchLocator
    	var BranchLocator = selection ? event : '';
    	//return BranchLocator
		return BranchLocator;
    };
    //Is isFormsPage
    function isFormsPage(event) {
    	//Set isFormsPage
    	var FormsPage = '';
    	//return FormsPage
		return FormsPage;
    };
    //Is isMoreInfo
    function isMoreInfo(event) {
    	//Set isMoreInfo
    	var MoreInfo = '';
    	//return MoreInfo
		return MoreInfo;
    };
    //Is isOptionalServices
    function isOptionalServices(event) {
    	//Set isOptionalServices
    	var OptionalServices = '';
    	//return OptionalServices
		return OptionalServices;
    };
    //Is isRetrieveApplication
    function isRetrieveApplication(event) {
    	//Set isRetrieveApplication
    	var RetrieveApplication = '';
    	//return RetrieveApplication
		return RetrieveApplication;
    };
    //Is isLoggedInApplication
    function isLoggedInApplication(event) {
    	//Set isLoggedInApplication
    	var LoggedInApplication = '';
    	//return LoggedInApplication
		return LoggedInApplication;
    };
    //Is isApplicationStep1
    function isApplicationStep1(event) {
    	//Set isApplicationStep1
    	var ApplicationStep1 = '';
    	//return ApplicationStep1
		return ApplicationStep1;
    };
    //Is isApplicationStep2
    function isApplicationStep2(event) {
    	//Set isApplicationStep2
    	var ApplicationStep2 = '';
    	//return ApplicationStep2
		return ApplicationStep2;
    };
    //Is isApplicationStep3
    function isApplicationStep3(event) {
    	//Set isApplicationStep3
    	var ApplicationStep3 = '';
    	//return ApplicationStep3
		return ApplicationStep3;
    };
    //Is isApplicationTermsDecline
    function isApplicationTermsDecline(event) {
    	//Set isApplicationTermsDecline
    	var ApplicationTermsDecline = '';
    	//return ApplicationTermsDecline
		return ApplicationTermsDecline;
    };
	//FNB Tracking
	fnb.hyperion.tracking = function() {
		this.init();
	};
	//FNB Tracking methods
	fnb.hyperion.tracking.prototype = {
		//Is this the first event
		isStart: true,
		//Report suite id
		id: 'frbprod',
		//Business unit
		businessUnit: 'fnb.co.za',
		//User object
		userObject: {},
		//Extra params
		extraParams: {},
		//Extra params
		loadObject: {},
		//Previous url
		currentUrl: '',
		//Previous pageName
		currentPageName: '',
		//Previous url
		previousUrl: '',
		//Previous pageName
		previousPageName: '',
		//Was a click event fired
		wasClickEvent: '',
		//What click event fired
		whatClickEvent: '',
		//Path to proxy
		path: location.protocol + '//' + location.host + '/web-tracking/analytics/send?payload=',
		//Do init for tracking
        init: function () {
        	//Wait for dom
        	//Do click event bindings
        	fnb.hyperion.tracking.bindListeners();
    		//Setup fnb.hyperion.cookies
        	fnb.hyperion.cookies.defaults = {namespace: 'fnb.',expires: 365};
        	//Get persistant user details
        	fnb.hyperion.tracking.userObject = getUserObject();

        },
        //Check tracking object for event
        checkTrackingObject: function (event,type,eventArgs) {
    		//Test if the event is a tracking event
        	if(event in listeners){
        		//Test for backend calls
                if(eventArgs.url != this.previousUrl){
            		//Set load object
                	this.loadObject = eventArgs;
            		//Create request
                	var request = this.buildXML(listeners[event]);
                	//Post tracking data
                	this.postData(request);
                	//Reset was click flag
                	this.wasClickEvent = '';
                	//Reset what click flag
                	this.whatClickEvent = '';
            	}
        	};
        },
        //Build XML
        buildXML: function (obj) {
        	//Create xml builder instance
        	var xmlBuilder = new fnb.hyperion.XMLWriter('UTF-8');
        	//Set formatting
        	xmlBuilder.formatting = 'indented';
        	//Set xml indentChar
        	xmlBuilder.indentChar = '';
        	//Set xml newline char
        	xmlBuilder.newLine = '';
        	//Set Indentation
        	xmlBuilder.indentation = 1;
        	//Start new document
        	xmlBuilder.startDocument();
        	//Start new element
        	xmlBuilder.startElement('request');
        	//Loop params
        	for (var param in obj['params']) {
        		//Get param object
        		var paramObject = obj['params'][param];
        		//Test if nodeattribute string or function and set
        		var nodeAttribute = (typeof paramObject.data === 'function') ? paramObject.data() : paramObject.data;
        		//Only add if nodeAttribute not empty
        		if(nodeAttribute!==''){
                	//Start new element        			
        			xmlBuilder.startElement(paramObject.node);
                	//Add string value
        			xmlBuilder.string(nodeAttribute);
                	//End element
        			xmlBuilder.endElement();
        		}
        	};
        	//Test if this is the initial event
        	if(this.isStart){
        		//Loop init onject
        		for (var initParam in initObj) {
            		//Get param object
            		var initParamObject = initObj[initParam];
            		//Test if nodeattribute string or function and set
            		var initNodeAttribute = (typeof initParamObject.data === 'function') ? initParamObject.data() : initParamObject.data;
            		//Only add if nodeAttribute not empty
            		if(initNodeAttribute!==''){
                    	//Start new element        			
            			xmlBuilder.startElement(initParamObject.node);
                    	//Add string value
            			xmlBuilder.string(initNodeAttribute);
                    	//End element
            			xmlBuilder.endElement();
            		}
            		
            	};
            	//Set isStart flag
            	this.isStart = false;
        	}
        	//New events string
        	var eventsString;
        	//Loop events
        	for (var event in events) {
        		//Set current events objerct
        		var eventObject = events[event];
    			//Execute function that is bound
    			var eventResult = eventObject.test(eventObject.event);
    			//Test result
    			if(eventResult!=''){
    				//Test if event has evars
    				if(eventObject.eVars){
    					//Split if multiple evars
    					var eVarsList = eventObject.eVars.split(",");
    					//Loop and get evars
    					for (var i = 0, length = eVarsList.length; i < length; i++) {
    						//Get eVar Result
    						var evarResult = evars[eVarsList[i]]();
    						//Create XML node
    						xmlBuilder.startElement(eVarsList[i]);
    		            	//Add string value
    		    			xmlBuilder.string(evarResult);
    		            	//End element
    		    			xmlBuilder.endElement();
    					}
    				}
    				//Test if event has props
    				if(eventObject.props){
    					//Split if multiple evars
    					var propsList = eventObject.props.split(",");
    					//Loop and get props
    					for (var i = 0, length = propsList.length; i < length; i++) {
    						//Get prop Result
    						var propResult = props[propsList[i]]();
    						//Create XML node
    						xmlBuilder.startElement(propsList[i]);
    		            	//Add string value
    		    			xmlBuilder.string(propResult);
    		            	//End element
    		    			xmlBuilder.endElement();
    					}
    				}
    				//Build event string
    				eventsString = (!eventsString) ? eventResult : eventsString+','+eventResult;
    			}
        	};
        	//Test if events need to be added
        	if(eventsString){
				//Create XML node
        		xmlBuilder.startElement('events');
            	//Add string value
    			xmlBuilder.string(eventsString);
            	//End element
    			xmlBuilder.endElement();
        	}
        	//End request element
        	xmlBuilder.endElement();
        	//End document
        	xmlBuilder.endDocument();
        	//Get xml
        	var xml = xmlBuilder.flush();
        	//Close instance of xml builder
        	xmlBuilder.close();
        	//Reset xmlBuilder
        	xmlBuilder = undefined;
        	//Return xml
        	return xml;
        },
        //Remove current object from dom
        postData: function (data) {
        	//JQUERY DO POST
        	$.ajax({
        	    url: fnb.hyperion.tracking.path,
        	    data: data, 
        	    type: 'POST',
        	    contentType: "text/xml",
        	    dataType: "text",
        	    success : function (){  
        	    	console.log('Success sending tracking data');  
        	    },
        	    error : function (xhr, ajaxOptions, thrownError){  
        	        console.log('Error sending tracking data');  
        	    }
        	}); 
        },
        //Bind listeners for events that need them
        bindListeners: function () {
        	for (var event in events) {
        		//Set current events objerct
        		var eventObject = events[event];
        		//Test if event is click event then bind
        		if(eventObject.clickListener){
        			//Click event found do binding
        			$(document).on('click',eventObject.clickListener, eventObject.clickEvent);
        		};
        	};
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.tracking = {};
        }
	};
	
	fnb.hyperion.tracking = new fnb.hyperion.tracking();

	fnb.hyperion.tracking.events = events;
	
})();