///-------------------------------------------///
/// developer: Richard 
///
/// Page Menu Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="pageMenuItem"]', handler: 'fnb.hyperion.utils.pageMenu.select(event);', preventDefault: true},
	              		{type: 'frame', listener: document, events:'click', selector: '[data-role="pageMenuHeading2"]', handler: 'fnb.hyperion.utils.pageMenu.toggleLevel3(event);'}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Page Menu Parent function
	///-------------------------------------------///
	function pageMenu() {

	};
	///-------------------------------------------///
	/// Page Menu Methods
	///-------------------------------------------///
	pageMenu.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,

		//Init FNB Page Menu
    	init: function () {
    		//Bind pageMenu events
    		bindEvents();
        },
        //Show level 3 items
        toggleLevel3: function (event) {
        	var level2Item = fnb.hyperion.$(event.currentTarget);
          	if(level2Item.attr('data-state') ==  'inactive') {
          		level2Item.attr('data-state','active');
        	}else{
        		level2Item.attr('data-state','inactive');
        	}
        },
        select: function (event) {
    		//Var for the selected item
    		var item = fnb.hyperion.$(event.currentTarget);
    		//Get the url bound to the item
    		var url = item.attr('href');
    		if (url != '') {
    			//Notify Controller to raise loadUrl event
    			fnb.hyperion.controller.raiseEvent('loadPage',{url: url,target:fnb.hyperion.controller.pageContentElement});
    		}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.pageMenu = {};
        }
	};

	//Namespace utils.pageMenu
	fnb.namespace('utils.pageMenu', pageMenu, true);

})();