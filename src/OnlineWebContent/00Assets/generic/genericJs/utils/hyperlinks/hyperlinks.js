///-------------------------------------------///
/// developer: Richard
///
/// hyperlink Object
///-------------------------------------------///
(function() {
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
		var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="hyperlink"]', handler: 'fnb.hyperion.utils.hyperlink.hyperlinkToUrl(event);', preventDefault: true}];
      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// hyperlink Parent function
	///-------------------------------------------///
	function hyperlink() {
			
	};
	///-------------------------------------------///
	/// hyperlink Buttons Methods
	///-------------------------------------------///
	hyperlink.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB hyperlink
		init: function () {
	  		//Bind hyperlink events
	  		bindEvents();
		},
		hyperlinkToUrl: function (event) {
			//Prevent default event
			event.stopImmediatePropagation();
	  		//Var for the selected item
	  		var item = fnb.hyperion.$(event.currentTarget);
	  		
	  		//Get the url bound to the item
	  		var url = item.attr('href');
	  		//Get button settings
        	var dataSettingsString = item.attr("data-settings");
        	
        	if(dataSettingsString){
        		//Convert settings string to object
            	var dataSettingsObject = JSON.parse(dataSettingsString);
            	//Wrap target in selector
            	if(dataSettingsObject[0].target) dataSettingsObject[0].target = fnb.hyperion.$(dataSettingsObject[0].target);
            	//Get event that needs to be raised
            	var event = dataSettingsObject[0].event;
            	//Raise specified event
            	fnb.hyperion.controller.raiseEvent(event, dataSettingsObject[0]);
        	}else if(url != ''){
	  			//Notify Controller to raise loadUrl event
	  			fnb.hyperion.controller.raiseEvent('loadPage',{url: url,target:fnb.hyperion.controller.pageContentElement});
	  		}else{
    			fnb.hyperion.controller.redirect(url);
    		};

		},
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.utils.hyperlink = {};
		}
	};

	//Namespace utils.hyperlink
	fnb.namespace('utils.hyperlink', hyperlink, true);

})();