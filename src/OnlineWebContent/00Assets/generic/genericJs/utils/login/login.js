///-------------------------------------------///
/// developer: Richard
///
/// login Object
///-------------------------------------------///
(function() {
	//Login form element selectors
	//Var for login form
	var loginForm;
	//Var for login button
	var loginButton;
	//Var for username input
	var usernameInput;
	//Var for username error
	var usernameError;
	//Var for password input
	var passwordInput;
	//Var for password error
	var passwordError;
	//Var mobile test
	var mobileLoginWrapper;

	//Var for active submittion
	var submitting = false;
	//Var for popoup visibility
	var popUpVisible = false;
	//Var for error visibility
	var errorVisible = false;
	//Var for mutilple select timeout
	var timeOut;
	//Select elements that are going to be reused
	function doSelections() {
		//Select mobile login wrapper
		mobileLoginWrapper = fnb.hyperion.$("#mobileLogin");
		//Select login form
		loginForm = fnb.hyperion.$("#LOGIN_FORM");
		//Select the login button
		loginButton = fnb.hyperion.$("#loginButton");
		//Select the username input
		usernameInput = fnb.hyperion.$("#Username");
		//Select the username error wrapper
		usernameError = fnb.hyperion.$("#inlineError_Username");
		//Select the username input
		passwordInput = fnb.hyperion.$("#Password");
		//Select the password error wrapper
		passwordError = fnb.hyperion.$("#inlineError_Password");
    };
	//Do login form submittion
	function submit() {
		//Validate login form
		if(valid()){
			//Set submitting flag
			submitting = true;
			//Submit form
			loginForm.elem.submit();
			//Disable login fields
			enableDisableLoginFields();		
			//Set timeout for submitting
			timeOut = setTimeout(function(){
				//Set submitting flag
				submitting = false;
				//Enable login fields
				enableDisableLoginFields();
			}, 5000);
		}
    };
	//Show mobile login form
    function enableDisableLoginFields() {
    	timeOut = setTimeout(function(){
    		//Test if fields are disabled
    		if(submitting){
    			//Disable login button
    			loginButton.attr('data-state','disabled');
    			loginButton.disabled = true;
    			//Disable Username input
    			usernameInput.disabled = true;
    			//Disable Password input
    			passwordInput.disabled = true;
    		}else{
    			//Enable login button
    			loginButton.attr('data-state','');
    			loginButton.disabled = false;
    			//Enable Username input
    			usernameInput.disabled = false;
    			//Enable Password input
    			passwordInput.disabled = false;
    		}
		}, 300);
	}
    //Validate login form
    function valid() {
    	//Test username field
		if(usernameInput.value == "Username"||usernameInput.value==""){
			fnb.hyperion.utils.login.showHideError('Username',true);
			return false;
		}
		//Test password field
		if(passwordInput.value == "Password"||passwordInput.value==""){
			fnb.hyperion.utils.login.showHideError('Password',true);
			return false;
		}
		
		return true;
	}
	//Bind event for current object
	function bindEvents() {
		//Add listener for click on login button
      	var events = [{type: 'frame', listener: document, events:'click', selector: '#popupLoginButton', handler: 'fnb.hyperion.utils.login.showHideLoginPopup(event);'},
      	              {type: 'frame', listener: document, events:'click', selector: 'div[data-role="blackCloseButton"]', handler: 'fnb.hyperion.utils.login.showHideLoginPopup(event);'},
      	              {type: 'frame', listener: document, events:'click', selector: '#loginButton', handler: 'fnb.hyperion.utils.login.doLogin(event);'},
      	              {type: 'frame', listener: document, events:'keyup', selector: '#loginButton', handler: 'fnb.hyperion.utils.login.doLogin(event);'},
      	              {type: 'frame', listener: document, events:'keyup', selector: '#Username', handler: 'fnb.hyperion.utils.login.doLogin(event);'},
      	              {type: 'frame', listener: document, events:'keyup', selector: '#Password', handler: 'fnb.hyperion.utils.login.doLogin(event);'},
      	              {type: 'frame', listener: document, events:'blur', selector: '#Username', handler: 'fnb.hyperion.utils.login.showHideError(["Username","Password"],false);'},
      	              {type: 'frame', listener: document, events:'blur', selector: '#Password', handler: 'fnb.hyperion.utils.login.showHideError(["Username","Password"],false);'}
      	              ];

      	//Append events to actions module
      	fnb.hyperion.actions.addEvents(events);
    };
	///-------------------------------------------///
	/// Login Parent function
	///-------------------------------------------///
	function login() {
		
	};
	///-------------------------------------------///
	/// Login Buttons Methods
	///-------------------------------------------///
	login.prototype = {
		//Var for frame to auto initialize module
		autoInit: true,
		//Init FNB login
		init: function () {
			console.log('Utils Login init')
			//Do login element selections
			doSelections();
			//Bind login events
			bindEvents();
		},
		//Do login method
		doLogin: function (event) {
			//Test if from keypress
			var doSubmit = (event.type == 'keyup') ? (event.keyCode == 13) ? true: false : true;
			//Test if login in progress and login is valid e.g. from ENTER key
			if(!submitting&&doSubmit){
				submit();
			}
		},
		//Show mobile login form
		showHideLoginPopup: function (event) {
			//Test if popup is visible
			if(popUpVisible){
				//Change pageContentContainerElement visibility data attribute
				fnb.hyperion.controller.pageContentContainerElement.show();
				//Change mobile popup visible state
				mobileLoginWrapper.hide();
				//Set popup visible flag
				popUpVisible = false;
			}else{
				//Change pageContentContainerElement visibility data attribute
				fnb.hyperion.controller.pageContentContainerElement.hide();
				//Change mobile popup visible state
				mobileLoginWrapper.show();
				//Set popup visible flag
				popUpVisible = true;
			}
		},
		//Show hide errors
		showHideError: function (field,show) {
			if(show){
				if(errorVisible===false){
					//Test for type of field
					if(typeof field == 'object'){
						//Loop Object
						field.each(function(item){
							//Switch fields
				        	switch(item) {
								case 'Username':
									usernameError.show();
								break;
								case 'Password':
									passwordError.show();
								break;
								default:

							};
						});
					}else{
						//Switch fields
			        	switch(field) {
							case 'Username':
								usernameError.show();
							break;
							case 'Password':
								passwordError.show();
							break;
							default:

						};
					}
					//Set error visible flag
					errorVisible=true;
				}			
			}else{
				if(errorVisible===true){
					//Test for type of field
					if(typeof field == 'object'){
						//Loop Object
						field.each(function(item){
							//Switch fields
				        	switch(item) {
								case 'Username':
									usernameError.hide();
								break;
								case 'Password':
									passwordError.hide();
								break;
								default:
			
							};
						});
					}else{
						//Switch fields
			        	switch(field) {
							case 'Username':
								usernameError.hide();
							break;
							case 'Password':
								passwordError.hide();
							break;
							default:
		
						};
					}
					//Set error visible flag
					errorVisible=false;
				}
			}
		},
		submitFormFromLink: function(action, form){
			//Select form
			var loginForm = fnb.hyperion.$('#'+form);
			//Get banking utl
			var environment = fnb.hyperion.$("#bankingUrl").val();
			environmentStr = environment.toString();
			//Update form action
			loginForm.attr('action',environmentStr + "/banking/main.jsp");
			//Change form target
			loginForm.attr('target',"_top");
			//Enable dimple input field for submittion
			if (action == 'verifyRemAdv') {
				fnb.hyperion.$("#simple").disabled = false;
			}
			//Update form action
			fnb.hyperion.$('#formAction').val(action);
			//Update nav value
			fnb.hyperion.$('#nav').val(action);
			//Submit form
			loginForm.elem.submit();
		},
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.utils.login = {};
		}
	};

	//Namespace utils.login
	fnb.namespace('utils.login', login, true);

})();