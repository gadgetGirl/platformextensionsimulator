///-------------------------------------------///
/// developer: Mike
///
/// Banner manager Object
///-------------------------------------------///
(function() {
	fnb.hyperion.utils.contentSlider = function(id) {
		
		if (!(this instanceof fnb.hyperion.utils.contentSlider)) {
		    for (var key in fnb.hyperion.utils.contentSliderManager.contentSliderInstances) {
		       if (fnb.hyperion.utils.contentSliderManager.contentSliderInstances[key].id === id) {
		    	   return fnb.hyperion.utils.contentSliderManager.contentSliderInstances[key];
		       }
		    }
		    fnb.hyperion.utils.contentSliderManager.contentSliderInstanceId++;
		    fnb.hyperion.utils.contentSliderManager.contentSliderInstances[fnb.hyperion.utils.contentSliderManager.contentSliderInstanceId] = new fnb.hyperion.utils.contentSlider(id);
		    return fnb.hyperion.utils.contentSliderManager.contentSliderInstances[fnb.hyperion.utils.contentSliderManager.contentSliderInstanceId];
		}
		this.id = id;
		this.container;
		this.init();  		   
	};
	fnb.hyperion.utils.contentSlider.prototype = {
		autoInit: false,
		init: function() {

			this.container = fnb.hyperion.$('#'+this.id);
			this.containerWidth = this.container.offsetWidth;
			this.childrenItems = this.container.find('.singleViewSlide');
			var length = this.childrenItems.length;
			this.contentSliderScroller = new fnb.hyperion.horizontalScroller();
			this.contentSliderScroller.scrollableParent = this.container.find('.singleViewSlider');
			this.contentSliderScroller.scrollerChildren = '.singleViewSlide';
			this.contentSliderScroller.scrollSpeed = 5;
			this.contentSliderScroller.maxStops = length-1;
			this.contentSliderScroller.moveTreshold = 0.20;
			this.contentSliderScroller.bindEvents();
			this.contentSliderScroller.enabled = true;
			//this.setWidths();
		},
		getMaxHeight: function(){
			var parentObject = this
			this.childrenItems.each(function(element){
				parentObject.heights.push(fnb.hyperion.$(element).offsetHeight);
			});
			var max = parentObject.heights[0];
			var maxIndex = 0;

			for (var i = 1; i < parentObject.heights.length; i++) {
			    if (parentObject.heights[i] >= max) {
			        maxIndex = i;
			        max = parentObject.heights[i];
			     }
			}

			return max;
		},
		setWidths: function(){
			var parentObject = this
			
			this.containerWidth = this.container.offsetWidth;
			var scrollerWidth = parentObject.containerWidth * this.childrenItems.length + 'px';
			this.container.find('.singleViewSlider').css('width', scrollerWidth);
			this.heights = [];
			this.maxHeight = this.getMaxHeight();
			this.container.css('height',this.maxHeight+40+'px')
			this.contentSliderScroller.scrollableParent.css('height',this.maxHeight+20+'px')
			this.childrenItems.each(function(element){
				var el = fnb.hyperion.$(element);
				el.css('width',(parentObject.containerWidth)+'px');
						
			});
			
			
		},
		setScrollStopWidth: function(id){
			var parentObject = this;
			fnb.hyperion.utils.contentSlider(id).contentSliderScroller.scrollStopWidth = fnb.hyperion.utils.contentSlider(id).container.offsetWidth;
			
			
		},
		adjustOnResize: function(windowSize, breakpoint,params) {
			fnb.hyperion.ready(function() {
				var parentObject = this;
				fnb.hyperion.utils.contentSlider(params).setScrollStopWidth(params);
				fnb.hyperion.utils.contentSlider(params).setWidths(params);
				parentObject.contentSliderScroller.moveTo(0,1);
				
			})

		}
		
	};
	fnb.hyperion.utils.contentSliderManager = function(){
			this.contentSliderInstances = {};
			
	};
	fnb.hyperion.utils.contentSliderManager.prototype = {
			autoInit: false,
			contentSliderInstanceId: 0,
			init : function() {
	            fnb.hyperion.controller.attachPageEvent('fnb.hyperion.utils.contentSliderManager.initContentSliderInstances()','');
	        },
	        initContentSliderInstances: function() {
	  
	        	var contentSliders = fnb.hyperion.$(".singleView");
	        	if(typeof contentSliders.length == 'function'){
	        	var length = (typeof contentSliders.length == 'undefined') ? 1: contentSliders.length(); 
	        		if(length > 0){	        		
	        			contentSliders.each(function(element){
	        				var id = element.attr('id');
	        				fnb.hyperion.utils.contentSlider(id);
	        				fnb.hyperion.controller.addResizeFunction(fnb.hyperion.utils.contentSlider(id).adjustOnResize,id);
	        			});
	        		}
	        	}else{
	        	this.destroy();	
	        	}
	        },
	        destroy: function(){
	        	for (var key in fnb.hyperion.utils.contentSliderManager.contentSliderInstances) {
	        		delete fnb.hyperion.utils.contentSliderManager.contentSliderInstances[key];
		         }
	        	fnb.hyperion.utils.contentSliderManager.contentSliderInstances = {};
	        }
	};
	//fnb.hyperion.utils.contentSliderManager = new fnb.hyperion.utils.contentSliderManager();
})();
