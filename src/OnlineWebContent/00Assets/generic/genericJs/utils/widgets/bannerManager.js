///-------------------------------------------///
/// developer: Mike
///
/// Banner manager Object
///-------------------------------------------///
(function() {
	var bannerInstances = {};
	var bannerScript = {};
	var bannerInstanceId = 0;
	var controllerClick = "/Controller?nav=banners.BannerEngine&name=";
    var controllerNav = "/Controller?nav=banners.BannerEngine&action=t&name=";
  
    function  getRandomNumber(lower,upper){
     	var rnd = lower + Math.random() * (upper-lower);
    	rnd = Math.floor(rnd);
    	return rnd;
    };
    
	fnb.hyperion.bannerController = function(id) {
		
		if (!(this instanceof fnb.hyperion.bannerController)) {
		    for (var key in bannerInstances) {
		       if (bannerInstances[key].id === id) {
		    	   return bannerInstances[key];
		       }
		    }
		    bannerInstanceId++;
		    bannerInstances[bannerInstanceId] = new fnb.hyperion.bannerController(id);
		    return bannerInstances[bannerInstanceId];
		}
		this.id = id;
		this.selectedId = fnb.hyperion.$('#'+id);
		this.settings = this.selectedId.attr('data-settings');
		this.settingsObject = JSON.parse(this.settings);
		this.contextPath = this.selectedId.attr('data-contextPath');
		this.banners = this.selectedId.attr('data-banners');
		this.bannerList = [];
		this.random = this.settingsObject["random"] ? 'true' : 'false';
		this.type = this.settingsObject["type"];
		this.currentPos=0;
		this.rotationInterval=this.settingsObject["rotationInterval"];
	
		this.currentBannerNumber = 0;
		this.timeOut;
	   	//bindEvents(this.id);
	   	if(this.banners=="empty"){
			this.bannersObject = {};
			console.log('No banners found!')
		}else{
			this.bannersObject = JSON.parse(this.banners);
			this.addBannersToGroup(this.bannersObject);
		}
	   		   
	};
	fnb.hyperion.bannerController.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		addBannersToGroup : function(bannersObject) {
			var parentObject = this;
			//Looping through banner object and adding to array
			for (var key in bannersObject) {
				parentObject.bannerList.push({
	            	bannerName:bannersObject[key]["bannerName"],
	            	bannerUrl:bannersObject[key]["bannerUrl"]
	            })
	         }
			parentObject.loadBanner();
        },
        loadBanner:function() {

           	var parentObject = this;
           	var bannerName = "";
           	
           	if(parentObject.random == 'true') {
        		parentObject.currentBannerNumber=getRandomNumber(0,parentObject.bannerList.length);
        		parentObject.random= false;
        	};
 
        	if(parentObject.bannerList.length>0 && parentObject.currentBannerNumber<=parentObject.bannerList.length){
           		if(parentObject.bannerList[parentObject.currentBannerNumber]["bannerName"]!=""){
        			bannerName = parentObject.bannerList[parentObject.currentBannerNumber]["bannerName"];
        			
        			var getBannerFromUrl =parentObject.contextPath+'/04Banners/'+ parentObject.bannerList[parentObject.currentBannerNumber]["bannerName"]+"/banner.html";
        			var bannerURL = parentObject.bannerList[parentObject.currentBannerNumber]["bannerUrl"]
       			    
        			fnb.hyperion.controller.raiseEvent("loadBanner",{url:getBannerFromUrl,target:parentObject.selectedId,bannerManagerId:parentObject.id,params:'url='+bannerURL,type:'get'})
        			fnb.hyperion.controller.raiseEvent("sendImpressions",bannerName);		    
        		}else{
        			console.log("empty banner source for banner: " + parentObject.bannerList[parentObject.currentBannerNumber]["bannerName"]);
        		};
        	};        	
        	parentObject.currentBannerNumber++;
        	if(parentObject.currentBannerNumber>=parentObject.bannerList.length)parentObject.currentBannerNumber=0;
        	
        },
        createTimeOut: function() {
        	var parentObject = this;
        	
        	if(fnb.hyperion.bannerObject) fnb.hyperion.bannerControllerManager.bannerScript[parentObject.id] = new fnb.hyperion.bannerObject(parentObject.id);
        	
        	if(parentObject.type!="" && !isNaN(parentObject.rotationInterval)){
        		parentObject.timeOut = setTimeout(function(){
        			 parentObject.loadBanner();
        		},parentObject.rotationInterval);
    		}
        }
	};
	fnb.hyperion.bannerControllerManager = function(){
			this.bannerControllerInstances = {}
	};
	fnb.hyperion.bannerControllerManager.prototype = {
			//Var for frame to auto initialize module
			bannerScript: {},
			autoInit: true,
			init : function() {
	            fnb.hyperion.controller.attachPageEvent('fnb.hyperion.bannerControllerManager.initBannerControllerInstances()','');
	            //Both banners and thumbnails need to send click events to b tracked
	            var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="bannerLink"],[data-role="thumbnailBanner"]', handler: 'fnb.hyperion.bannerControllerManager.bannerNavToUrl(event)', preventDefault: true}];
	        	fnb.hyperion.actions.addEvents(events);	
	        },
	        initBannerControllerInstances: function() {
	        	
	        	var bannerControllers = fnb.hyperion.$(".bannerContainer");
	        	
	        	var length = 0;
	        	try{
	        		length = bannerControllers.length()
	        	}catch(e){
	        		length = bannerControllers.length;
	        	}
	        	if(length > 0 ){	        		
	        			bannerControllers.each(function(element){
	        				var id = element.attr('id');
	        				fnb.hyperion.bannerController(id);
	        			});
	        		
	        	}else{
	        		this.destroy();	
	        	}
	        },
	        sendImpressions:function(impressionList) {
	        	//Build up the url to send impressions
	        	var impressionUrl = '/Controller?nav=banners.BannerEngine&action=t&name='+impressionList+'&adLocation='+top.location;
	        	//Raise the event to send the above mentioned url
				fnb.hyperion.controller.raiseEvent("ajaxSend",{url:impressionUrl}); 
	        },
	        bannerNavToUrl: function(event){

	        	var target = fnb.hyperion.$(event.currentTarget);

	        	var url= target.attr('href');
	        	//get he name of the baner to to pass to the contoller
	        	var bannerName= target.attr('id');
	        	//Log theclick event an load the resultinto te workspace
	        	fnb.hyperion.controller.raiseEvent("loadPage",{url:'/Controller?nav=banners.BannerEngine&name='+bannerName+'&&adLocation='+top.location,type:'get'});
	        },	        
	        bindEvents: function(id){
	        
	        	
	        },
	        destroy: function(){
	        		        	
	        	for (var key in bannerInstances) {
	        		clearTimeout(bannerInstances[key].timeOut);
	        		delete bannerInstances[key];
		        }
	        	bannerInstances = {};
	        }
	};
	fnb.hyperion.bannerControllerManager = new fnb.hyperion.bannerControllerManager();
})();


