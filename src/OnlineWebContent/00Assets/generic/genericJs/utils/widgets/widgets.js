///-------------------------------------------///
/// developer: Richard 
///
/// Widgets Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Widgets Parent function
	///-------------------------------------------///
	function widgets() {

	};
	///-------------------------------------------///
	/// Widgets Methods
	///-------------------------------------------///
	widgets.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Init FNB Widgets
    	init: function () {

        },
        //load widget into target
        loadWidgets: function (widgets) {
        	//For each widget on the page get the url and load the result into the taget 
			for(var i=0;i<widgets.length;i++) {
				//Get the widget target
    			var widgetTarget = fnb.hyperion.$("#"+widgets[i]);
    			//Get the widgets url
    			var url = widgetTarget.attr("data-application");
    			//Populate the load object with the url and target
    			var loadObj = {url:url, target:widgetTarget, async: true};
    			//Raise event to load the url into the target
    			fnb.hyperion.controller.raiseEvent("loadApp",loadObj);
        	}
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.utils.widgets = {};
        }
	};

	//Namespace utils.widgets
	fnb.namespace('utils.widgets', widgets, true);

})();