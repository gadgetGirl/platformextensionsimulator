///-------------------------------------------///
/// developer: Donovan
///
/// Sweeper Object
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Sweeper Parent function
	///-------------------------------------------///
	function sweeper() {

	};
	///-------------------------------------------///
	/// Sweeper Methods
	///-------------------------------------------///
	sweeper.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Clean DOM of non namespaced functions
        sweep: function () {
        	for ( var x in window) {

        		try{
        			if (typeof window[x] === 'function'
    					&& window[x].toString().indexOf('[native code]') == -1
    					&& window[x].toString().search(/fnb.hyperion./i) == -1) {
        				
        				console.log('Non namespaced function found=>> '+x.toString());

        				//delete window[x];
        			}
        		}catch(e){
        			console.log(e);
        			console.log(window[x].toString());
        		}
        	}
        }
	};
	//Namespace page object
	fnb.namespace('sweeper', sweeper, true);
})();