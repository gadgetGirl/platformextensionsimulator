///-------------------------------------------///
/// developer: Donovan
///
/// Actions Object
///-------------------------------------------///
(function() {

	//Threshold to disable default event on move
	var tapMoveThreshold = 15;
	
	//Initial Touch events on touch start
	var touchesStart = {};
	
	//Tap start function for tap events
    function tapStart(event) {
		
		//Switch on tap
		fnb.hyperion.actions.touchEndEnabled = true;
		
		//Get changedTouches
    	var touches = event.changedTouches;
		
		//Loop and store touch events on touch start
		for(var j = 0; j < touches.length; j++) {
			
			 touchesStart[ "hyperion$" + touches[j].identifier ] = {
				
				//Get identifier
				identifier : touches[j].identifier,
				
				//Get x start pos
				screenX : touches[j].screenX,
				
				//Get y start pos
				screenY : touches[j].screenY

			 };
		};
		
    };
	
	//Tap move function for tap events
    function tapMove(event) {

		var touches = event.changedTouches;

		for(var j = 0; j < touches.length; j++) {
		
			//Get and match touches
			var touchInfo = touchesStart[ "hyperion$" + touches[j].identifier ];
			
			//Get x moved pos
			touchInfo.distanceX = touches[j].screenX - touchInfo.screenX;
			
			//Get y moved pos
			touchInfo.distanceY = touches[j].screenY - touchInfo.screenY;

		}
		
		//Test if threshold was reached
		if(Math.abs(touchInfo.distanceX)>tapMoveThreshold || Math.abs(touchInfo.distanceY)>tapMoveThreshold){
			
			//Switch of tap
			fnb.hyperion.actions.touchEndEnabled = false;
			
		}

    };
	///-------------------------------------------///
	/// Actions Parent function
	///-------------------------------------------///
	function actions() {

	};
	///-------------------------------------------///
	/// Actions Methods
	///-------------------------------------------///
	actions.prototype = {
	
		//Var for frame to auto initialize module
		autoInit: false,
		
		//List Events added for frame functionality
		frameEvents: [],
		
		//List Temp Events added for page functionality
		pageEvents: [],
		
		//Current Tap target
		touchEndEnabled: true,
		
		//Do event binding proc's
        addEvents: function (eventsArray) {
		
			//Loop events object
        	for (var i = 0; i < eventsArray.length; i++) {
        		
				//Test for touch devices
        		var event = (eventsArray[i].mobileEvents) ? (eventsArray[i].mobileEvents === "none") ? eventsArray[i].events : eventsArray[i].mobileEvents : (eventsArray[i].events === "click" && fnb.hyperion.controller.isMobile) ? 'tap' : eventsArray[i].events;
        		
				//Select listener
        		var listener = (typeof eventsArray[i].listener!="undefined") ? (typeof eventsArray[i].listener == 'string') ? fnb.hyperion.$(eventsArray[i].listener).elem : (eventsArray[i].listener.elem) ? eventsArray[i].listener.elem : eventsArray[i].listener : document;
        		
				//Notify controller to attach focusout event
        		if(eventsArray[i].focusOutEvent) fnb.hyperion.controller.attachFocusOutEvent({target: listener,handler: eventsArray[i].focusOutEvent});
        		
				//Select type
        		var type = (eventsArray[i].type) ? eventsArray[i].type : 'page';

        		//Return if there is no handler
        		if(!eventsArray[i].handler) return;
        		
        		//Swap events
        		if(event=="tap") event = "touchend";

        		/*Developer: Donovan - Fix for banking*/
				//Test if this is a tap event
				/*if(event=="tap"){
				
					//Reset touch end
					event = "touchend";
					
					//Add touch move
					fnb.hyperion.delegate.on(listener, "touchmove",eventsArray[i].selector, tapMove);
					
					//Add touch start
					fnb.hyperion.delegate.on(listener, "touchstart",eventsArray[i].selector, tapStart);

					//Create custom function for taps
					eventsArray[i].handler = (eventsArray[i].preventDefault==true) ? new Function('event', "event.preventDefault();if(fnb.hyperion.actions.touchEndEnabled){"+eventsArray[i].handler+"}") : new Function('event', "if(fnb.hyperion.actions.touchEndEnabled){"+eventsArray[i].handler+"}");
					
				}*/

				//Test for any mobile events and if this is not a touch device
        		var wrapperFunction = (typeof eventsArray[i].handler != 'function') ? (eventsArray[i].preventDefault==true) ? new Function('event','event.preventDefault();'+eventsArray[i].handler) : new Function('event',eventsArray[i].handler) : eventsArray[i].handler;
        		
				//Read handler
        		eventsArray[i].handler = wrapperFunction;
				
        		//Switch event types
    			//frame: permanent bindings
    			//page: temp bindings
    			switch (type) {
					
					case 'frame':
						
						//Make list of frame bindings
						this.frameEvents.push(eventsArray[i]);
						
						//Do switch on if selector
						switch (eventsArray[i].selector) {
							
							case undefined:
								
								//Do main bind
								fnb.hyperion.delegate.on(listener, event, wrapperFunction);
								
							break;
							default:
								
								//Do main bind
								fnb.hyperion.delegate.on(listener, event, eventsArray[i].selector, wrapperFunction);
								
							break;
						}
						break;
						
					case 'page':
					
					case undefined:
						
						//Make list of page bindings
						this.pageEvents.push(eventsArray[i]);
						
						//Do switch on if selector
						switch (eventsArray[i].selector) {
							
							case undefined:
								
								//Do main bind
								fnb.hyperion.delegate.on(listener, event, wrapperFunction);
								
							break;
							default:
								
								//Do main bind
								fnb.hyperion.delegate.on(listener, event, eventsArray[i].selector, wrapperFunction);
								
							break;
						}
						break;
					default:

				}
        	}
        },
        //Unbind events
        removeEventsArray: function (eventsArray) {
        	
			//isTap var
			var isTap = false;
			
			//Loop array and unbind event
        	for (var i = 0; i < eventsArray.length; i++) {
			
        		var listener =  (typeof eventsArray[i].listener!="undefined") ? (typeof eventsArray[i].listener == 'string') ? fnb.hyperion.$(eventsArray[i].listener).elem : (eventsArray[i].listener.elem) ? eventsArray[i].listener.elem : eventsArray[i].listener : document;

        		//Test for touch devices
        		var event = (eventsArray[i].mobileEvents) ? (eventsArray[i].mobileEvents === "none") ? eventsArray[i].events : eventsArray[i].mobileEvents : (eventsArray[i].events === "click" && fnb.hyperion.controller.isMobile) ? 'tap' : eventsArray[i].events;

				//Test if this is a tap event
				if(event=="tap"){
				
					//Reset touch end
					event = "touchend";

					//Create custom function for taps
					eventsArray[i].handler = new Function('event', "if(fnb.hyperion.actions.touchEndEnabled){"+eventsArray[i].handler+"}");
					
					//Set isTap var
					isTap = true;
				}
				
				//Remove listener
        		fnb.hyperion.delegate.off(listener, event, eventsArray[i].handler, isTap);
				
        	}
			
        	//Clear page events
        	fnb.hyperion.actions.pageEvents = [];
			
        },
        //Actions of method
        off: function (target , method , handler) {
			
        	fnb.hyperion.delegate.off(target);
			
        },
        //Clear temp page events
        clearPageEvents: function () {
		
        	//Clear page events array
        	this.pageEvents = [];
			
        },
        //Remove current object from dom
        destroy: function () {
        	fnb.hyperion.actions = {};
        }
	};
	//Namespace actions
	fnb.namespace('actions', actions, true);

})();