///-------------------------------------------///
/// developer: Donovan
///
///FNB Controller Object
///-------------------------------------------///
(function() {
	//Get document height
    function getDocumentHeight() {
    	//Return highest document height
        return Math.max(
        	fnb.hyperion.controller.document.body.scrollHeight, fnb.hyperion.controller.document.documentElement.scrollHeight,
        	fnb.hyperion.controller.document.body.offsetHeight, fnb.hyperion.controller.document.documentElement.offsetHeight,
        	fnb.hyperion.controller.document.body.clientHeight, fnb.hyperion.controller.document.documentElement.clientHeight
        );
    };
	///-------------------------------------------///
	/// Controller Parent function
	///-------------------------------------------///
	function controller() {
		//Is current device a touch device
		this.isMobile = '';
		//Global selectors
		//window selector
		this.window = '';
		//document selector
		this.document = '';
		//html selector
		this.html = '';
		//Head tag
		this.head;
		//Window title selector
		this.windowTitle;
		//Body tag
		this.bodyElement = '';
		//Overlay div
		this.overlayElement = '';
		//Date Picker div
		this.datePickerElement = '';
		//Date Picker dropdown
		this.datePickerDropdown = '';
		//Action menu button
		this.actionMenuButton = '';
		//Ezi Panel div
		this.eziElement = '';
		//Ezi Panel content div
		this.eziPageContentElement = '';
		//Page content container
		this.pageContentContainerElement = '';
		//Page content div
		this.pageContentElement = '';
		//Footer container that wraps footer groups
		this.footerContainer = '';
		//Footer buttons wrapper div
		this.footerButtonGroup = '';
		//Footer buttons wrapper div
		this.eziFooterButtonGroup = '';
		//Error Panel div
		this.errorElement = '';
		//SubTab wrapper
		this.subTabWrapper = '';
		//timeOut wrapper
		this.timeOutElement = '';
		//PrintDiv wrapper
		this.printDiv = '';
		//Header wrapper
		this.header = '';
		//Load object
		this.loadObj;
	};
	///-------------------------------------------///
	/// Controller Methods
	///-------------------------------------------///
	controller.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Var for list of site wide intervals
		intervals: [],
		//Site wide functions that need window resize
    	resizeFunctions: [],
    	//Site wide functions that need breakpoint changed
    	breakPointFunctions: [],
    	//All template for a specific page
    	pageTemplates: [],
    	//Site wide elements that need focusout triggers
    	elementFocusOutEvents: [],
    	//Wrapper for page extentions
    	page: [],
    	//Current break position
    	siteBreakPosition : 0,
    	//Responsive breakpoints
    	siteBreakPoints : [1280,950,480],
    	//Var for last dom element selected
    	currentActiveElement: '',
    	//Var for touch moving flag
		touchMoving: false,
    	//Init FNB Controls
    	init: function () {
    		//Check for touch devices
    		this.isMobile = (fnbIsMobile) ? (fnbIsMobile=="false") ? false : true : false;
    		//Initialise selector that are going to be used globally
    		this.initialSelections();
    		//Initialize progress bar
    		fnb.hyperion.progress.init();
    		//Initialise global modules
    		this.initModules();
    		//Do Sweep
    		this.doSweep();
    		//Check window size
    		this.resize();
    		//Create initial loadobject
    		if(typeof topMenuUrl!= "undefined") var loadObj = {url: topMenuUrl, target: fnb.hyperion.controller.header, postLoadingCallback: fnb.hyperion.controller.loadHomePage};
    		//Raise event to start site
    		fnb.hyperion.controller.raiseEvent('loadSite', loadObj);
        },
    	//Init browser Extention - space for custom browser to 
        loadHomePage: function () {
        	//Raise event to load home page
        	fnb.hyperion.controller.raiseEvent('loadHome',{url: defaultUrl})
        },
    	//Init browser Extention - space for custom browser to 
        initBrowserExtentions: function () {
    		
        },
        //Make all initial global selections
        initialSelections: function () {
        	//Global selector for document
        	this.document = document;
        	//Global selector for document
        	this.window = window;
        	//Global selector for html
        	this.html = fnb.hyperion.$("=html");
        	//Global selector for window title
        	this.windowTitle = fnb.hyperion.$("=title");
        	//Global selector for window title
        	this.head = fnb.hyperion.$("=head");
        	//Global Body selector
        	this.bodyElement = fnb.hyperion.$("#bodyContainer");
        	//Global Overlay selector
        	this.overlayElement = fnb.hyperion.$("#loaderOverlay");
        	//Global Date Picker selector
        	this.datePickerElement = fnb.hyperion.$("#dateOverlay");
        	//Global Date Picker selector
        	this.datePickerDropdown = fnb.hyperion.$("#datePickerDropdown");
        	//Global Ezi content wrapper selector
        	this.eziElement = fnb.hyperion.$("#eziOverlay");
        	//Global Ezi page content wrapper selector
        	this.eziPageContentElement = this.eziElement.find('.eziInnerTwo');
        	//Global Page content wrapper selector
        	this.pageContentContainerElement = fnb.hyperion.$("#pageContentWrapper");
        	//Global Page content wrapper selector
        	this.pageContentElement = fnb.hyperion.$("#pageContent");
        	//Global Footer container selector
        	this.footerContainer = fnb.hyperion.$("#footerContainer");
        	//Global Footer wrapper selector
        	this.footerButtonGroup = fnb.hyperion.$("#footerButtonsContainer");
        	//Global Ezi Footer wrapper selector
        	this.eziFooterButtonGroup = fnb.hyperion.$("#eziFooterButtonsContainer");
        	//Global Error Panel selector
        	this.errorElement = fnb.hyperion.$("#error");
        	//Notifications container selector
        	this.notificationsElement = fnb.hyperion.$("#notifications");
        	//Global Error Panel selector
        	this.actioMenuButton = fnb.hyperion.$("#actionMenuBtt");
        	//Global select Subtab wrapper
        	this.subTabWrapper = fnb.hyperion.$("#subMenu");
        	//Global selector for action menu button label wrapper
        	this.actioMenuButtonLabelWrapper = fnb.hyperion.$("#actionMenuBttLabelWrapper");
        	//Global select timeout wrapper
        	this.timeOutElement = fnb.hyperion.$("#timeOut");
        	//Select Print div
        	this.printDiv = fnb.hyperion.$("#hiddenPrintDiv");
        	//Select Print div
        	this.header = fnb.hyperion.$("#header");
        },
        //Init all initial modules
        initModules: function () {
    		//Attach event templates
    		this.attachPreConfiguredEvents();
    		//Counter for module inits
    		var i=0;
	    	//Loop initial modules an initiate
			for (var module in fnb.hyperion) {
				i++;
        		if(fnb.hyperion[module].autoInit){
        			fnb.hyperion[module].init();
				}
        	}
        	//Check loadObj settings
        	this.checkLoadObjSettings();
        },
        //Init page templates
        initHtmlTemplates: function (loadObj) {
        	//Add specified list if exists
        	var htmlTemplatesList = (loadObj) ? loadObj.appendTemplates: null;
        	//Set default list if none has been specified
        	if(!htmlTemplatesList) htmlTemplatesList = this.pageTemplates;
        	//Test if templates exist
        	if(htmlTemplatesList.length>0){
        		for (var i = 0; i < htmlTemplatesList.length; i++) {
        			//Inform templates module to add template
        			fnb.hyperion.utils.templates.addTemplate(htmlTemplatesList[i]);
        		}
 			}
        },
        //Init page events
        initPageEvents: function () {
        	//Wait for the dom to be ready then initialize page events
        	fnb.hyperion.ready(function() {
        		fnb.hyperion.events.raiseEvent('initPageEvents');
        	});
        },
        //Init page objects
        initPageObjects: function () {
        	//Loop page objects and init if needed
        	for (var pageObjects in this.page) {
        		if(this.page.hasOwnProperty(pageObjects)){
        			if(this.page[pageObjects]){
        				
        				if(!this.page[pageObjects].isInitialized&&this.page[pageObjects].actions){
        					fnb.hyperion.actions.addEvents(this.page[pageObjects].actions);
        				}
        				
            			if(!this.page[pageObjects].isInitialized&&this.page[pageObjects].init){
            				
            				this.page[pageObjects].init();
            				this.page[pageObjects].isInitialized = true;
            				
            			};
        			}
        		}
			}
        },
        //Init page objects
        clearPageObjects: function () {
        	//Loop page objects clear
        	for (var pageObjects in this.page) {
        		this.page[pageObjects] = undefined;
			}
        	//Clear page events
        	this.clearPageEventsArray();
        },
        //Return generated template
        getHtmlTemplate: function (obj) {
        	//Return populated html template
        	return fnb.hyperion.utils.templates.getTemplate(obj);
        },
        //Clear htmlTemplates templates from dom
        clearHtmlTemplates: function (loadObj) {
        	//Add specified list if exists
        	var htmlTemplatesList = (loadObj) ? loadObj.discardTemplates : [];
        	//Set default list if none has been specified
        	if(!htmlTemplatesList) htmlTemplatesList = this.pageTemplates;
        	//Test if templates exist
        	if(htmlTemplatesList.length>0){
        		for (var i = 0; i < htmlTemplatesList.length; i++) {
        			//Inform templates module to clear target
    				fnb.hyperion.utils.templates.clearTarget(htmlTemplatesList[i].templateTarget);
        		};
 			}
        	//Clear Page templates array
        	this.clearPageTemplatesArray();
        },
        //Clear templates on page
        clearPageTemplatesArray: function () {
        	//Clear page templates array
        	this.pageTemplates = [];
        },
        //Reset objects on page
        checkLoadObjSettings : function (loadObj) {
			//Set default for load object
        	var loadObject = (loadObj)?loadObj:{initHtmlTemplates:true,initPageEvents:true,initPage:true};
        	//Test if htmlTemplates need to be initialized
        	if(loadObject.initHtmlTemplates==true) this.initHtmlTemplates(loadObject);
        	//Test if page events need to be initialized
        	if(loadObject.initPageEvents==true) this.initPageEvents();
        	//Test if page need to be initialized
        	if(loadObject.initPage==true) this.initPageObjects();
		},
        //Attach preConfigured event sequences to events handler
        attachPreConfiguredEvents: function () {
        	//Merge two event templates
            if(fnb.hyperion.appTemplates){
            	for (var key in fnb.hyperion.appTemplates) {
                    if (fnb.hyperion.appTemplates.hasOwnProperty(key)) {
                    	fnb.hyperion.eventTemplates[key] = fnb.hyperion.appTemplates[key];
                    }
                }
            }
        	//Store our  preconfigured event templates list.
        	var preConfiguredEventList = fnb.hyperion.eventTemplates;
          	//Create an empty object to hold our new configured list.
        	var configuredEventList = {};
          	//Loop through the keys in our preconfigured event templates list.
        	for (var key in preConfiguredEventList) {
          		//Check that the object has the property before executing any code. A best practice not entirely necessary in this case.
        		if (preConfiguredEventList.hasOwnProperty(key)) {
        			//As we loop through our preconfigured object create matching keys our configured object with current data as well  
        			configuredEventList[key] = preConfiguredEventList[key];
           		    //Create an array to store our callbacks   
        			var functionRefs = [];
          		    //Loop through the objects which are stored in array at each key in our new configured object  
        			for (var i = 0; i < configuredEventList[key].length; i++) {
        				//Create a variable to hold the current object we are going to loop through
        				var subObject = configuredEventList[key][i];
         				//Create an array to store the callbacks and params we extract from the object
        				var params = [];
         				//Loop thought the object and store the keys in the params array
        		    	for (var prop in subObject) {
        		    		if (subObject.hasOwnProperty(prop)) {
        		    			params.push(subObject[prop].toString());
        		    		}
           		    	}
        		        //Create a new Function and pass the params into it from the loop abaove.	
        		    	var func = new Function((params[1]) ? params[1] : '', params[0]);
           		    	//Push the newly formatted functions into an array
        		    	functionRefs.push(func);
        			}
        			//Add our configured callback list to the new configured object at the relevant key
        			configuredEventList[key] = functionRefs;
        		}
           	}
           	//Set the fnb.hyperion.events.eventList variable to our new configured event list
        	fnb.hyperion.events.eventList = configuredEventList;
        },
        //Attach Action Events
		attachActionEvents : function (events) {
			//Notifiy actions object to attach events
			fnb.hyperion.actions.addEvents(events);
		},
        //Clear specific Action Events
		clearActionEvents : function (listener) {
			fnb.hyperion.actions.off(listener);
		},
        //Attach Form elements that need focusout
		attachFocusOutEvent : function (event) {
			this.elementFocusOutEvents.push(event);
		},
        //Attach elementFocusOutEvents array
        clearFocusOutEvents: function () {
        	//Clear focus out events array
        	this.elementFocusOutEvents = [];
        },
        //Clear page events
		clearPageEventsArray : function () {
			fnb.hyperion.actions.removeEventsArray(fnb.hyperion.actions.pageEvents);
		},
        //Attach event to events object
		attachEvent : function (event,handler,args) {
			fnb.hyperion.events.attachEvent(sender,eventArgs);
		},
        //Dettach event from events object
		detachEvent : function (sender,eventArgs) {
			fnb.hyperion.events.detachEvent(sender,eventArgs);
		},
        //Attach events that execute when page loaded
		attachPageEvent : function (handler) {
			//Attach event sequence to events handler
			fnb.hyperion.events.attachEvent('initPageEvents',handler,'event');
		},
        //Execute specific events sequence
		raiseEvent : function (sender,loadObj) {
			console.log('Hyperion Raise Event: '+sender);
			//Test if loadObj exists
			var loadObject = (loadObj) ? loadObj: {};
			//Raise Event
			fnb.hyperion.events.raiseEvent(sender,loadObject);
		},
        //Listen for DOM events
		DOMevent : function (target,event) {
			//Check if the same element is being selected
			if(target!=this.currentActiveElement){
				//Check if this.currentActiveElement has active
				this.focusOut(target,event);
			}
			
			this.currentActiveElement = target;
		},
        //Run focusout events
        focusOut: function (target,event) {
        	for (var i = this.elementFocusOutEvents.length - 1; i >= 0; i--) {
        		if(this.elementFocusOutEvents[i].target!=target){
        			var eventDummy = {currentTarget: target,event:event};
        			var func = new Function('event',this.elementFocusOutEvents[i].handler);
    		    	func(eventDummy);
        		}
			}
        },
        //redirect to URL
        redirect : function (loadObj) {
        	var url = loadObj.url ? loadObj.url : loadObj;
        	window.location = url;
        },
        //Ajax functions gateway
		ajax : function (ajaxFunction,sender,loadObj) {
			//Split functions
			var functions = ajaxFunction.split('.');
			//Run function for specified for ajax if exists
			if(fnb.hyperion.functions.ajax[functions[0]]){
				if(fnb.hyperion.functions.ajax[functions[0]][functions[1]]){
					fnb.hyperion.functions.ajax[functions[0]][functions[1]](sender,loadObj);
				}else{
					console.log('Controller Error: ajax: '+functions[0]+' doesnt have a method '+functions[1]);
				}
			}else{
				console.log('Controller Error: ajax: Function does not exist. '+functions[0]);
			}
		},
        //Serialize data
		serialize : function (target) {
			//Serialize target data
			return fnb.hyperion.serialize.get(target);
		},
        //Set default for ajaxLoad
		getAjaxLoadDefaults : function (loadObj) {
			//Set loadObj type
			loadObj.type = (typeof loadObj.type == 'undefined') ? loadObj.type = 'POST' : (typeof loadObj.type != 'boolean') ? loadObj.type = 'POST' : loadObj.type;
			//Add default param for ajax execute complete event
			loadObj.complete = (typeof loadObj.complete == 'undefined') ? loadObj.complete = false : (typeof loadObj.complete != 'boolean') ? loadObj.complete = false : loadObj.complete;
			//Add default param for ajax execute beforeSend event
			loadObj.beforeSend = (typeof loadObj.beforeSend == 'undefined') ? loadObj.beforeSend = false : (typeof loadObj.beforeSend != 'boolean') ? loadObj.beforeSend = false : loadObj.beforeSend;
			//Add default param for ajax execute success event
			loadObj.success = (typeof loadObj.success == 'undefined') ? loadObj.success = true : (typeof loadObj.success != 'boolean') ? loadObj.success = true : loadObj.success;
			//Add default param for ajax execute success event
			loadObj.target= (typeof loadObj.target == 'undefined') ? loadObj.target = this.pageContentElement : (loadObj.target == '') ? loadObj.target = this.pageContentElement : loadObj.target;
			//Add default param for ajax execute success event
			loadObj.urlTarget= (typeof loadObj.urlTarget == 'undefined') ? loadObj.urlTarget = "div[id='pageContent']" : (loadObj.urlTarget == '') ? loadObj.urlTarget = "div[id='pageContent']" : loadObj.urlTarget;
			//Add default param for ajax execute complete event
			loadObj.error = (typeof loadObj.error == 'undefined') ? loadObj.error = true : (typeof loadObj.error != 'boolean') ? loadObj.error = true : loadObj.error;
			//Add default param for page to be initialized on ready
			loadObj.initPageModules = (typeof loadObj.initPageModules == 'undefined') ? loadObj.initPageModules = true : (typeof loadObj.initPageModules != 'boolean') ? loadObj.initPageModules = true : loadObj.initPageModules;
			//Add default param for page templates to be initialized on ready
			loadObj.initHtmlTemplates = (typeof loadObj.initHtmlTemplates == 'undefined') ? loadObj.initHtmlTemplates = true : (typeof loadObj.initHtmlTemplates != 'boolean') ? loadObj.initHtmlTemplates = true : loadObj.initHtmlTemplates;
			//Add default param for page events to be initialized on ready
			loadObj.initPageEvents = (typeof loadObj.initPageEvents == 'undefined') ? loadObj.initPageEvents = true : (typeof loadObj.initPageEvents != 'boolean') ? loadObj.initPageEvents = true : loadObj.initPageEvents;
			//Add default param for page to be initialized on ready
			loadObj.initPage = (typeof loadObj.initPage == 'undefined') ? loadObj.initPage = true : (typeof loadObj.initPage != 'boolean') ? loadObj.initPage = true : loadObj.initPage;
			//Add default history flag
			loadObj.history = (typeof loadObj.history == 'undefined') ? loadObj.history = false : (typeof loadObj.history != 'boolean') ? loadObj.history = true : loadObj.history;
			//Update global load Object
			this.loadObj = loadObj;
			//Return load Object
			return loadObj;
			
		},
        //Run dom sweep
        doSweep: function () {
        	/*Run sweeper*/
        	fnb.hyperion.sweeper.sweep();
        },
        //Get page scroll position
        getScrollPosition: function (element) {
        	fnb.hyperion.functions.scrollTo.getPosition(element);
        },
        //Set page scroll position
        setScrollPosition: function (element) {
        	fnb.hyperion.functions.scrollTo.setPosition(element);
        },
        //Scroll to position
        scrollTo: function (xPos,yPos) {
        	//Scroll window to position
        	window.scrollTo(xPos,yPos);
        },
        //Test for element ready
        isReady: function (element,handler) {
        	//Trigger ready module
        	fnb.hyperion.functions.isReady.wait(element,handler);
        },
        //Get current site breakpoint position
        getBreakPosition: function (windowWidth) {
        	var currentPosition = 0;
        	for (var i = 0; i < this.siteBreakPoints.length; i++) {
				if(windowWidth>this.siteBreakPoints[i]){
					currentPosition = i;
					return currentPosition;
				}
			}
        	//Return default value
        	return this.siteBreakPoints.length;
        },
        //Append breakpoint notification function array
        addNotifyBreakPointChangedFunction: function (handler) {
        	/*Update breakpoint funtions array*/
        	this.breakPointFunctions.push(handler);
        },
        //Notify functions breakpoint changed
        notifyBreakPointChanged: function (width) {
        	//Current window width
        	var currentPosition = this.getBreakPosition(width);
        	//Test if breakPoint changed
        	if(currentPosition<this.siteBreakPosition||currentPosition>this.siteBreakPosition){
        		//Update current breakpoint
        		this.siteBreakPosition = currentPosition;
        		//Loop and execute resize functions
        		for (var index = 0; index < this.breakPointFunctions.length; ++index) {
                    this.breakPointFunctions[index](this.siteBreakPosition);
                }
        	};
        },
        //Append resize function array
        addResizeFunction: function (handler,params) {
        	/*Update resize funtions array*/
        	this.resizeFunctions.push({'handler':handler,'params':params});
        },
        //Global resize of site
        resize: function () {
        	//Current window width
        	var width = this.window.innerWidth || this.document.documentElement.clientWidth;
        	//Check and notify functions if breakpoint changed
        	this.notifyBreakPointChanged(width);
    		//Loop and execute resize functions
    		for (var index = 0; index < this.resizeFunctions.length; ++index) {
               this.resizeFunctions[index]['handler'](width, this.getBreakPosition(width), this.resizeFunctions[index]['params']);
            }
        },
        //Raise Ajax error
        error: function (message,sender) {
        	
        	//Var for error prefix
        	var prefix = (fnb.hyperion.utils.eziPanel.active) ? "Ezi" : "";

        	//Notify Controller to raise Error event
        	fnb.hyperion.controller.raiseEvent(prefix+"Error", message);
        	//Notify Controller to raise method Error event
        	fnb.hyperion.controller.raiseEvent(prefix+sender+"Error", message);
        },
        //Raise OTP
        otp: function (message,sender) {
        	
        	//Location flag for load response location
    		var intoEzi = fnb.hyperion.utils.eziPanel.active;
    		
        	//Notify Controller to raise OTP event
        	fnb.hyperion.controller.raiseEvent((intoEzi==true) ? "loadIntoEzi" : "loadEzi" , {url:  '/banking/Controller?nav=navigator.otp.OtpLanding&ezi='+intoEzi, initHtmlTemplates: false});
        },
        //Check response for erros
        validateXHR: function (ajaxXHR, callback, sender) {
        	//Validate Ajax Response
        	var valid = fnb.hyperion.functions.validateXHR.validate(ajaxXHR, callback, sender);
        	//Test if ezi needs to close
        	if(!valid) fnb.hyperion.controller.checkEziState(sender);
        	//Return valid var
        	return valid;
        },
        //Validate Ajax Response
        validResponse: function (loadObj) {
        	//Validate Ajax Response
        	var valid = fnb.hyperion.utils.ajax.validResponse.test(loadObj);
        	//Test if ezi needs to close
        	if(!valid) fnb.hyperion.controller.checkEziState(loadObj.event);
        	//Return valid var
        	return valid;
        },
        //Test ezi state
        checkEziState: function (sender) {
        	//Test if ezipanel is active and the sender is loadEzi then close the ezi panel
        	if(sender=='loadEzi'){
        		//Hide ezi
        		fnb.hyperion.controller.raiseEvent("hideEzi");
        	};
        },
        //Update pushstate
        updateUrl: function (loadObj, params) {
        	//Test for pushstate
        	if(fnb.hyperion.pushState){
        		//Test if hasing the url is enabled
            	if(fnb.hyperion.pushState.enableHash||fnb.hyperion.pushState.enabled){
                	//Call pushState to update url
                	fnb.hyperion.pushState.push(loadObj, params);
            	}
        	}
        },
        //Destroy ajax
        destroyAjax: function () {
        	//Request stop from ajax
        	fnb.hyperion.ajax.stop('Controller');
        },
        //Destroy ajax
        clipOverflow: function (val) {
        	//test to add or remove overflow
        	if(val){
        		fnb.hyperion.controller.bodyElement.addClass('HbodyClipOverflow');
        		fnb.hyperion.controller.html.addClass('HclipOverflow');
        	}else{
        		fnb.hyperion.controller.bodyElement.removeClass('HbodyClipOverflow');
        		fnb.hyperion.controller.html.removeClass('HclipOverflow');
        	}
        },
        //Destroy ajax
        is: function (obj) {
        	//test to add or remove overflow
        	return (typeof obj!= 'undefined') ? (obj) ? true : false : false;
        },
        //Add methods for a page
        extendPage: function(pageExtentions) {

			//Test for function reference
        	if(!pageExtentions.functionReference){
        		console.error('Controller Error: extendPage: No function reference added.');
        		return;
        	}

        	//Test for function reference exists
        	if(this.page[pageExtentions.functionReference]){
        		console.error('Controller Error: extendPage: Function reference already exists.');
        		return;
        	}
        	//Catch errors
        	try{
        		//Create temp page object
        		var tempObject = new fnb.hyperion.page();
        		//Append new page methods
        		for ( var method in pageExtentions) {
        			tempObject[method] = pageExtentions[method];
            		//Test for audits
            		if(debug&&typeof pageExtentions[method]=='function'&&!document.attachEvent) fnb.hyperion.audit.test(pageExtentions[method]);
    			}
        		//Add flag for initialized state
        		tempObject['isInitialized'] = false;
        		//Add to controller page object
        		this.page[pageExtentions.functionReference] = tempObject;
				//See if there are any data for this object
				this.page[pageExtentions.functionReference].pageData = (fnb.hyperion.load.includesData[pageExtentions.functionReference]) ? fnb.hyperion.load.includesData[pageExtentions.functionReference][0] : [];
        	}catch(e){
        		console.error('Controller Error: extendPage: '+e);
        	};
        },
        //Enable topMenu
        enableTopMenu: function () {
        	fnb.hyperion.utils.topTabs.enable();
        },
        //Disable topMenu
        disbaleTopMenu: function () {
        	fnb.hyperion.utils.topTabs.disable();
        },
        //open url in new window
        openWindow : function (link) {
        	//Test for url type
        	var url = (typeof link=="object") ? link.url : link;
        	//Open window
            window.open(url);
        }
	};
	//Namespace controller
	fnb.namespace('controller', controller, true);
	//Setup Selector
	fnb.hyperion.$ = fnb.hyperion.selector;
	//Setup getDocumentHeight as public methos
	fnb.hyperion.getDocumentHeight = getDocumentHeight;
})();