///-------------------------------------------///
/// developer: Donovan
///
/// Wrapper for Page Objects
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Page Parent function
	///-------------------------------------------///
	function page() {

	};
	///-------------------------------------------///
	/// Page Methods
	///-------------------------------------------///
	page.prototype = {
		//Remove current object from dom
		destroy: function () {
			fnb.hyperion.page = {};
		}
	};
	//Namespace page object
	fnb.namespace('page', page);
})();
