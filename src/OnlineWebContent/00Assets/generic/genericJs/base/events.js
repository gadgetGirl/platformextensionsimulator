///-------------------------------------------///
/// developer: Donovan
///
/// Main Events Handler
///-------------------------------------------///
(function() {
	///-------------------------------------------///
	/// Events Parent function
	///-------------------------------------------///
	function events(sender) {
		this._sender = sender;
	};
	///-------------------------------------------///
	/// Events Methods
	///-------------------------------------------///
	events.prototype = {
		//Var for frame to auto initialize module
		autoInit: false,
		//Main events list
		eventList: {},
		//Get event from list of events
		getEvent: function(eventName, create){
			if (!this.eventList[eventName]){
				if (!create) {
					return null;
				}
				this.eventList[eventName] = [];
			}
			return this.eventList[eventName];
		},
		//Manually attach event to events list
		attachEvent: function(eventName,handler,args) {
			var evt = this.getEvent(eventName, true);
			var func;
			switch(typeof(handler)) {
				case 'function':
					func = handler;
				break;
				default:
					func = new Function(args,handler);
			};
			evt.push(func);
		},
		//Manually insert event at a specific position
		insertEvent: function(eventName, handler, args, position) {
			var evt = this.getEvent(eventName, true);
			var func;
			switch(typeof(handler)) {
				case 'function':
					func = handler;
				break;
				default:
					func = new Function(args,handler);
			};
			if(!position) position = evt.length;
			evt.splice(position, 0, func);
		},
		//Manually detach a specific event
		detachEvent: function(eventName, handler) {
			var evt = this.getEvent(eventName);
			if (!evt) { return; }
			var getArrayIndex = function(array, item){
				for (var i = 0; i < array.length; i++) {
					if (array[i] && array[i] === item) {
						return i;
					}
				}
				return -1;
			};
			var index = getArrayIndex(evt, handler);
			if (index > -1) {
				evt.splice(index, 1);
			}
		},
		//Raise event sequence
		raiseEvent: function(eventName, eventArgs) {
			if(typeof(this.getEventHandler(eventName))!='undefined'){
				if (this.getEventHandler(eventName)) {
					this.getEventHandler(eventName)(eventName, eventArgs);
				}
			}
		},
		//Get event handler
		getEventHandler: function(eventName) {
			var evt = this.getEvent(eventName, false);
	
			if (!evt || evt.length === 0) { return null; }
			var _handler = function(sender, args) {
				for (var i = 0; i < evt.length; i++) {
					evt[i](sender, args);
				}
			};
			return _handler;
		}
	};
	//Namespace events
	fnb.namespace('events', events, true);

})();