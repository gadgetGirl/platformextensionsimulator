///-------------------------------------------///
/// developer: Donovan
///
/// Page object
///-------------------------------------------///
fnb.hyperion.controller.extendPage({
	
	//Function reference for this page object
	functionReference: "layoutOne",
	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.',
	
	///////////////////////////
    //Page object methods
    ///////////////////////////
	
	init : function(){
		console.log('Page object init');
    	//Get the default tab with containing a list of banners/thumbnails to make impressions of
    	thumbsList = fnb.hyperion.$("#forMeThumbs");
    	//get the list of banners/thumbnails from the above selected object
		var impressionList = thumbsList.attr("data-bannerThumbnails");
		//raise the event to send the impression list 
		fnb.hyperion.controller.raiseEvent("sendImpressions",impressionList);
		//Bind page object events
		this.bindEvents();
	},
	bindEvents : function(){
		//Build up the events for the page
		var events = [{type: 'frame', listener: document, events:'click', selector: '[data-role="subTab"]', handler: this.context+'layoutOne.sendImpressions(event)'}];
		//Bind the evens to th frame
    	fnb.hyperion.actions.addEvents(events);	
	},
	//Select selected tab and send list of banners to make impressions
	sendImpressions : function (event) {
		//get the selected tab in order to obtain the later obtain the listof bannes/thumbnails to make impressions of
		var tab = fnb.hyperion.$(event.currentTarget);
		//get the name of the object containing the list of banners/thumbnails
		tabName = tab.attr("data-content");
		//if selcted tab is the forMe tab the get the forMe thumbnails
		if (tabName == "forMeContent" ){
			//get the forMe thumbnails
			thumbsList = fnb.hyperion.$("#forMeThumbs");
		}
		//if selcted tab is the forMyBusiness tab then get the forMyBusiness thumbnails
		else if  (tabName = "forMyBusinessContent" ) {
			//get the forMyBusiness thumbnails
			thumbsList = fnb.hyperion.$("#forMyBusinessThumbs");
		}
    	//get the list of banners/thumbnails from the above selected object
		var impressionList = thumbsList.attr("data-bannerThumbnails");
		//raise the event to send the impression list 
		fnb.hyperion.controller.raiseEvent("sendImpressions",impressionList);
		
	}
});