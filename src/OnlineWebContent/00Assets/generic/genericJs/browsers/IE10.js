///-------------------------------------------///
///-------------------------------------------///
/// developer: Donovan
///
/// IE10 media queries hack
///-------------------------------------------///
(function() {
	//FNB Banner
	function resizeFix() {
		
	};
	//FNB Banner
	resizeFix.prototype = {
		//Init FNB Page
		init: function () {

		},
		//Remove current object from dom
		doIeFix: function () {
			//Make window smaller
			window.resizeTo(window.outerWidth-1, window.outerHeight-1);
			//Reset window size
			setTimeout(function(){window.resizeTo(window.outerWidth+1, window.outerHeight+1);},100);
		}
	};
	//Namespace resizeFix
	fnb.hyperion.namespace('resizeFix', resizeFix, true);
})();

///-------------------------------------------///
///-------------------------------------------///
/// developer: Donovan
///
/// Extend controller
///-------------------------------------------///
fnb.hyperion.controller.initBrowserExtentions = function () {
	fnb.hyperion.controller.attachPageEvent('fnb.hyperion.resizeFix.doIeFix()','');
};
