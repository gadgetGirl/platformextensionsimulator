///-------------------------------------------///
///-------------------------------------------///
/// developer: Nishal
///
/// IE8 Exit Overlay (Stage1)
/// IE8 Exit Login Overlay (Stage2)
/// IE8 Exit Redirect (Stage3)
///-------------------------------------------///

(function(){
	var stage = 2;
	var countdownTime = 10;
	var closePopup = true;
	var status = 0;
	var locationStatus = 0;
	var loginStatus = "";
	var ie8Exit = function() {};	
	ie8Exit.prototype = {
			init: function(){
				var parentObject = this;
				var cookieValue = parentObject.getCookie("ie8ExitStatus");
				if (cookieValue === null || cookieValue == '')
					{cookieValue=0;}
				cookieValue=parseInt(cookieValue);
				parentObject.stage=cookieValue;
				stage=cookieValue;
				parentObject.countdownTime=countdownTime;
				parentObject.loginPopup=0;
				parentObject.locationStatus=0;
				parentObject.timerVar = "";
				document.attachEvent("onclick", fnb.ie8Exit.handleClick);
				switch (stage){
				case 2: 
					parentObject.stage2();
				break;
				
				case 3:
					parentObject.stage3();
				break;
				};
				
			},	
			stage2: function(){
				this.bindStage2Events();
			},	
			stage3: function(){
				var parentObject = this;
				
				var docMode = parseInt(document.documentMode);
				var browserMode = parentObject.ieVersion();
				
				
				if(docMode < 9 || browserMode < 9)
				{
					fnb.hyperion.controller.redirect('/banking/wrongBrowser.jsp');
				}
				
	
			},
			ieVersion: function() {
				var ua = navigator.userAgent;
			
				var match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
			    return match ? parseInt(match[1]) : 0;
				
				
				
			},
			bindStage2Events: function() {
				var parentObject = this;
				setTimeout(function(){
				document.getElementById('ie8Stage2').removeAttribute('class');
				loginStatus = parentObject.setTimer();
			}, 100);
				
				
				$("#login-block-top").addClass("stage2Active");
				
				/*$("#showLogin").on("click",function(){
					$("#login-block-top").css({"height":"auto"}).find(".grid33").css({"display":"block"});
					$("#ie8Stage2").css({"display":"none"});
				})*/
					
			},
			setTimer: function(){
				clearTimeout(loginStatus);
				return setTimeout(function(){
					document.getElementById('ie8loginMessage').className += ' ie8DisplayNone';
				}, 5000);
			},
			closeStage1Overlay: function(){
				var parentObject = this;
				if(parentObject.closePopup)
				{
				var input1 = document.querySelectorAll('input[name="Username"]')[0];
				document.getElementById('ie8Stage1').className += ' ie8DisplayNone';	
				input1.focus();
				}
			},
			openStage1Overlay: function(){
				var parentObject = this;
				
				if(status < 1 && parentObject.stage == 1)	
				{
				document.getElementById('ie8Stage1').removeAttribute('class');
				document.getElementById("dateCountdown").innerHTML = parentObject.initiateDateTimer();
				parentObject.initiateTimer();
				status=1;
				}
			},
			initiateTimer: function(){
				var timerElement = document.getElementById("ie8Countdown");
				var parentObject = this;
				var counter = parseInt(parentObject.countdownTime);
				var id;
				parentObject.closePopup=false;
				parentObject.timerVar = setInterval(function() {
				    counter--;
				    if(counter < 0) {
				    	parentObject.killTimer();
				    } else {
				    	timerElement.innerHTML =counter.toString();
				    }
				}, 1000);
			
			},
			killTimer: function(){
				var parentObject = this;
				parentObject.closePopup=true;
		        clearInterval(parentObject.timerVar);
		        document.getElementById("countdownText").innerHTML="";
			},
			slideMessage: function(){
				var parentObject = this;
				var msg1 = document.getElementById("message1");
				var msg2 = document.getElementById("message2");
				
				//parentObject.killTimer();
				msg1.style.display="none";
				//msg1.style.left="0";
			
			},
			initiateDateTimer: function(){
				var now = new Date();
				var end = new Date("September 14, 2014");
				var timeLeft = end - now;
				var daysLeft = Math.floor(timeLeft / (24*60*60*1000) +1);
				return daysLeft;
			
			},
			blurStage2: function(){
				var input1 = document.querySelectorAll('input[name="Username"]')[0];
				var input2 = document.querySelectorAll('input[name="Password"]')[0];
				this.addEvent(input1,'blur',function(){
					input1.value="";
					input2.value="";
				});
				this.addEvent(input2,'blur',function(){
					input1.value="";
					input2.value="";
				});

			},
			getCookie: function(cookieName){
				var name = cookieName + "=";
			    var ca = document.cookie.split(';');
			    for(var i=0; i<ca.length; i++) {
			        var c = ca[i];
			        while (c.charAt(0)==' ') c = c.substring(1);
			        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
			    }
			    return "";
			},			
			openStage2LoginMessage: function(){
				document.getElementById('ie8Stage2').removeAttribute('class');
				document.getElementById('ie8loginMessage').removeAttribute('class');
				loginStatus = fnb.ie8Exit.setTimer();
				fnb.ie8Exit.loginPopup =1;

			},
			closeStage2LoginMessage: function(){
				document.getElementById('ie8loginMessage').className += ' ie8DisplayNone';
				fnb.ie8Exit.loginPopup =0;
				clearTimeout(loginStatus);

			},
			openStage3Message: function(){			
						document.getElementById('content1').removeAttribute('class');
						document.getElementById('content2').removeAttribute('class');
						document.getElementById('content1').className += ' ie8ContentWrapper ie8DisplayNone';		
						document.getElementById('content2').className += ' ie8ContentWrapper2';
						

			},
			addEvent: function(elem, event, fn){
			
				if (elem.attachEvent)
			     {
			 		elem.attachEvent("on" + event, function() {
			 			return(fn.call(elem, window.event));   
			        });
			     }
			 },
			 handleClick: function(){
				
				   	var event = window.event;
				    
				    var target = event.srcElement;
				    
				    switch (fnb.ie8Exit.stage){
					
					case 1:
						 switch(target.name){
					        case "Username":
					        	fnb.ie8Exit.openStage1Overlay();
					            break;
					        case "ie8PopupCloseBtn":
					        	fnb.ie8Exit.closeStage1Overlay();
					            break;
					    }
					break;
					
					case 2: 
						switch(target.name){
				        case "Username":
				        	fnb.ie8Exit.blurStage2();
				            break;
				        case "Password":
				        	fnb.ie8Exit.blurStage2();
				            break;
				        case "ie8loginButton":
				        	fnb.ie8Exit.openStage2LoginMessage();
				            break;
				        case "ie8loginCloseBtn":
				        	fnb.ie8Exit.closeStage2LoginMessage();
				            break;	
				        	
				    }
					break;
					case 3:
						switch(target.name){
				        case "downloadButton3":
				        	fnb.ie8Exit.openStage3Message();
				            break;
					break;
					};

				   
			 }
		}
	};	

	window['fnb'] = window['fnb'] || {};
	fnb['ie8Exit'] = new ie8Exit();	
	
	//fnb.ie8Exit.init();
	
})();
