///-------------------------------------------///
/// developer: Donovan
///
///window.HTMLCollection mappings///
///-------------------------------------------///
if ('HTMLCollection' in window) {
	//each
	if (!('each' in window.HTMLCollection)) {
		window.HTMLCollection.prototype.each = Array.prototype.forEach;
	}
}

if ('StaticNodeList' in window) {
	//each
	if (!('each' in window.StaticNodeList)) {
		window.StaticNodeList.prototype.each = Array.prototype.forEach;
	}
}

///-------------------------------------------///
/// developer: Donovan
///
/// Add getElementsByClassName
///-------------------------------------------///
(function() {
    if (!document.getElementsByClassName) {
        var indexOf = [].indexOf || function(prop) {
            for (var i = 0; i < this.length; i++) {
                if (this[i] === prop) return i;
            }
            return -1;
        };
        getElementsByClassName = function(className,context) {
            var elems = document.querySelectorAll ? context.querySelectorAll("." + className) : (function() {
                var all = context.getElementsByTagName("*"),
                    elements = [],
                    i = 0;
                for (; i < all.length; i++) {
                    if (all[i].className && (" " + all[i].className + " ").indexOf(" " + className + " ") > -1 && indexOf.call(elements,all[i]) === -1) elements.push(all[i]);
                }
                return elements;
            })();
            return elems;
        };
        document.getElementsByClassName = function(className) {
            return getElementsByClassName(className,document);
        };
        Element.prototype.getElementsByClassName = function(className) {
            return getElementsByClassName(className,this);
        };
    }
})();

///-------------------------------------------///
/// developer: Donovan
///
/// Add getComputedStyle
///-------------------------------------------///
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            };
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        };
        return this;
    };
}

///-------------------------------------------///
///-------------------------------------------///
/// developer: Donovan
///
/// IE8 Repaint
///-------------------------------------------///
fnb.hyperion.selector.repaint = function (element) {
	try{
		element.addClass('repaint');
		//Repaint object
		var redraw = element.elem.offsetHeight;
		element.removeClass('repaint');
	}catch(e){
		
	}

};

///-------------------------------------------///
/// developer: Donovan
///
/// Object keys polyfill
///-------------------------------------------///
if (!Object.keys) {
	  Object.keys = (function () {
	    'use strict';
	    var hasOwnProperty = Object.prototype.hasOwnProperty,
	        hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
	        dontEnums = [
	          'toString',
	          'toLocaleString',
	          'valueOf',
	          'hasOwnProperty',
	          'isPrototypeOf',
	          'propertyIsEnumerable',
	          'constructor'
	        ],
	        dontEnumsLength = dontEnums.length;

	    return function (obj) {
	      if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
	        throw new TypeError('Object.keys called on non-object');
	      }

	      var result = [], prop, i;

	      for (prop in obj) {
	        if (hasOwnProperty.call(obj, prop)) {
	          result.push(prop);
	        }
	      }

	      if (hasDontEnumBug) {
	        for (i = 0; i < dontEnumsLength; i++) {
	          if (hasOwnProperty.call(obj, dontEnums[i])) {
	            result.push(dontEnums[i]);
	          }
	        }
	      }
	      return result;
	    };
	  }());
	}

///-------------------------------------------///
/// developer: Donovan
///
/// Slice polyfill
///-------------------------------------------///

(function () {
    'use strict';
    var _slice = Array.prototype.slice;

    try {
        // Can't be used with DOM elements in IE < 9
        _slice.call(document.documentElement);
    } catch (e) { // Fails in IE < 9
        // This will work for genuine arrays, array-like objects, 
        // NamedNodeMap (attributes, entities, notations),
        // NodeList (e.g., getElementsByTagName), HTMLCollection (e.g., childNodes),
        // and will not fail on other DOM objects (as do DOM elements in IE < 9)
        Array.prototype.slice = function (begin, end) {
            // IE < 9 gets unhappy with an undefined end argument
            end = (typeof end !== 'undefined') ? end : this.length;

            // For native Array objects, we use the native slice function
            if (Object.prototype.toString.call(this) === '[object Array]'){
                return _slice.call(this, begin, end); 
            }
            
            // For array like object we handle it ourselves.
            var i, cloned = [],
                size, len = this.length;
            
            // Handle negative value for "begin"
            var start = begin || 0;
            start = (start >= 0) ? start: len + start;
            
            // Handle negative value for "end"
            var upTo = (end) ? end : len;
            if (end < 0) {
                upTo = len + end;
            }
            
            // Actual expected size of the slice
            size = upTo - start;
            
            if (size > 0) {
                cloned = new Array(size);
                if (this.charAt) {
                    for (i = 0; i < size; i++) {
                        cloned[i] = this.charAt(start + i);
                    }
                } else {
                    for (i = 0; i < size; i++) {
                        cloned[i] = this[start + i];
                    }
                }
            }
            
            return cloned;
        };
    }
}());