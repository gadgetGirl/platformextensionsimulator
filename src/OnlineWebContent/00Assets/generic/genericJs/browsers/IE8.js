///-------------------------------------------///
/// developer: Donovan
///
///window.HTMLCollection mappings///
///-------------------------------------------///
if ('HTMLCollection' in window) {
	//each
	if (!('each' in window.HTMLCollection)) {
		window.HTMLCollection.prototype.each = Array.prototype.forEach;
	}
}

if ('StaticNodeList' in window) {
	//each
	if (!('each' in window.StaticNodeList)) {
		window.StaticNodeList.prototype.each = Array.prototype.forEach;
	}
}
///-------------------------------------------///
/// developer: Donovan
///
/// Add getElementsByClassName
///-------------------------------------------///
(function() {
    if (!document.getElementsByClassName) {
        var indexOf = [].indexOf || function(prop) {
            for (var i = 0; i < this.length; i++) {
                if (this[i] === prop) return i;
            }
            return -1;
        };
        getElementsByClassName = function(className,context) {
            var elems = document.querySelectorAll ? context.querySelectorAll("." + className) : (function() {
                var all = context.getElementsByTagName("*"),
                    elements = [],
                    i = 0;
                for (; i < all.length; i++) {
                    if (all[i].className && (" " + all[i].className + " ").indexOf(" " + className + " ") > -1 && indexOf.call(elements,all[i]) === -1) elements.push(all[i]);
                }
                return elements;
            })();
            return elems;
        };
        document.getElementsByClassName = function(className) {
            return getElementsByClassName(className,document);
        };
        Element.prototype.getElementsByClassName = function(className) {
            return getElementsByClassName(className,this);
        };
    }
})();
///-------------------------------------------///
/// developer: Donovan
///
/// Add getComputedStyle
///-------------------------------------------///
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            };
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        };
        return this;
    };
}

///-------------------------------------------///
///-------------------------------------------///
/// developer: Nishal
///
/// IE8 Exit Overlay (Stage1)
/// IE8 Exit Login Overlay (Stage2)
/// IE8 Exit Redirect (Stage3)
///-------------------------------------------///

(function(){
	var stage = 1;
	var countdownTime = 10;
	var closePopup = true;
	var status = 0;
	var locationStatus = 0;
	var loginStatus = "";
	var ie8Exit = function() {};	
	ie8Exit.prototype = {
			init: function(){
				var parentObject = this;
				parentObject.stage=stage;
				parentObject.countdownTime=countdownTime;
				parentObject.loginPopup=0;
				parentObject.locationStatus=0;
				parentObject.timerVar = "";
				document.attachEvent("onclick", fnb.ie8Exit.handleClick);
				switch (stage){
				case 2: 
					parentObject.stage2();
				break;
				
				case 3:
					parentObject.stage3();
				break;
				};
				
			},	
			stage2: function(){
				this.bindStage2Events();
			},	
			stage3: function(){
				var parentObject = this;
				
					//fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen', '/banking/wrongBrowser.jsp');
				
				if(!window.location.pathname.match("ie8Exit/index.jsp"))
				{
					if(fnbContextPath != "undefined")
					{fnb.hyperion.controller.redirect(fnbContextPath+'/ie8Exit/index.jsp');}
				}
	
			},
			bindStage2Events: function() {
				var parentObject = this;
				setTimeout(function(){
				document.getElementById('ie8Stage2').removeAttribute('class');
				loginStatus = parentObject.setTimer();
			}, 100);
					
			},
			setTimer: function(){
				clearTimeout(loginStatus);
				return setTimeout(function(){
					document.getElementById('ie8loginMessage').className += ' ie8DisplayNone';
				}, 5000);
			},
			closeStage1Overlay: function(){
				var parentObject = this;
				if(parentObject.closePopup)
				{
				var input1 = document.querySelectorAll('input[name="Username"]')[0];
				document.getElementById('ie8Stage1').className += ' ie8DisplayNone';	
				input1.focus();
				}
			},
			openStage1Overlay: function(){
				var parentObject = this;
				
				if(status < 1 && parentObject.stage == 1)	
				{
				document.getElementById('ie8Stage1').removeAttribute('class');
				document.getElementById("dateCountdown").innerHTML = parentObject.initiateDateTimer();
				parentObject.initiateTimer();
				status=1;
				}
			},
			initiateTimer: function(){
				var timerElement = document.getElementById("ie8Countdown");
				var parentObject = this;
				var counter = parseInt(parentObject.countdownTime);
				var id;
				parentObject.closePopup=false;
				parentObject.timerVar = setInterval(function() {
				    counter--;
				    if(counter < 0) {
				    	parentObject.killTimer();
				    } else {
				    	timerElement.innerHTML =counter.toString();
				    }
				}, 1000);
			
			},
			killTimer: function(){
				var parentObject = this;
				parentObject.closePopup=true;
		        clearInterval(parentObject.timerVar);
		        document.getElementById("countdownText").innerHTML="";
			},
			slideMessage: function(){
				var parentObject = this;
				var msg1 = document.getElementById("message1");
				var msg2 = document.getElementById("message2");
				
				//parentObject.killTimer();
				msg1.style.display="none";
				//msg1.style.left="0";
			
			},
			initiateDateTimer: function(){
				var now = new Date();
				var end = new Date("September 14, 2014");
				var timeLeft = end - now;
				var daysLeft = Math.floor(timeLeft / (24*60*60*1000) +1);
				return daysLeft;
			
			},
			blurStage2: function(){
				var input1 = document.querySelectorAll('input[name="Username"]')[0];
				var input2 = document.querySelectorAll('input[name="Password"]')[0];
				this.addEvent(input1,'blur',function(){
					input1.value="";
					input2.value="";
				});
				this.addEvent(input2,'blur',function(){
					input1.value="";
					input2.value="";
				});

			},
			openStage2LoginMessage: function(){
				document.getElementById('ie8Stage2').removeAttribute('class');
				document.getElementById('ie8loginMessage').removeAttribute('class');
				loginStatus = fnb.ie8Exit.setTimer();
				fnb.ie8Exit.loginPopup =1;

			},
			closeStage2LoginMessage: function(){
				document.getElementById('ie8loginMessage').className += ' ie8DisplayNone';
				fnb.ie8Exit.loginPopup =0;
				clearTimeout(loginStatus);

			},
			openStage3Message: function(){			
						document.getElementById('content1').removeAttribute('class');
						document.getElementById('content2').removeAttribute('class');
						document.getElementById('content1').className += ' ie8ContentWrapper ie8DisplayNone';		
						document.getElementById('content2').className += ' ie8ContentWrapper2';
						

			},
			addEvent: function(elem, event, fn){
			
				if (elem.attachEvent)
			     {
			 		elem.attachEvent("on" + event, function() {
			 			return(fn.call(elem, window.event));   
			        });
			     }
			 },
			 handleClick: function(){
				
				   	var event = window.event;
				    
				    var target = event.srcElement;
				    
				    switch (fnb.ie8Exit.stage){
					
					case 1:
						 switch(target.name){
					        case "Username":
					        	fnb.ie8Exit.openStage1Overlay();
					            break;
					        case "ie8PopupCloseBtn":
					        	fnb.ie8Exit.closeStage1Overlay();
					            break;
					    }
					break;
					
					case 2: 
						switch(target.name){
				        case "Username":
				        	fnb.ie8Exit.blurStage2();
				            break;
				        case "Password":
				        	fnb.ie8Exit.blurStage2();
				            break;
				        case "ie8loginButton":
				        	fnb.ie8Exit.openStage2LoginMessage();
				            break;
				        case "ie8loginCloseBtn":
				        	fnb.ie8Exit.closeStage2LoginMessage();
				            break;	
				        	
				    }
					break;
					case 3:
						switch(target.name){
				        case "downloadButton3":
				        	fnb.ie8Exit.openStage3Message();
				            break;
					break;
					};

				   
			 }
		}
	};	

	window['fnb'] = window['fnb'] || {};
	fnb['ie8Exit'] = new ie8Exit();	
	
	fnb.ie8Exit.init();
	
})();
