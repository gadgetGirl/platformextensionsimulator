///-------------------------------------------///
///-------------------------------------------///
/// developer: Mike
///
/// IE9 disable ajax loading
///-------------------------------------------///

fnb.hyperion.utils.topTabs.select =  function (event) {
	//Prevent default event
	event.preventDefault();
	//If the topmenu is not moving or the user is not dragging it - execute body of function
	if(this.topScroller.moving == false){
		
		//Var for the selected tab
		var selectedTab = fnb.hyperion.$(event.currentTarget);
		//Var f or the hasSubMenu
		var hasSubMenu = selectedTab.attr("data-submenu");

		if (hasSubMenu == 'false') {
			//If selectedTab has a href load the url
    		//Get the url bound to the tab
    		var url = selectedTab.attr('data-url');
    		//Highlight selected tab
    		fnb.hyperion.utils.topTabs.highlightSelected(event);
        	//Set Scroller styles
        	this.setTobTabSlider();
        	//Notify Controller to raise loadUrl event
        	fnb.hyperion.controller.redirect(url);
		}
		else {
			//Get the ID of selected tab
    		var selectedTab =fnb.hyperion.$(event.currentTarget);
    		var selectedTabId = selectedTab.id;
    		var subMenu = fnb.hyperion.$("#subMenu_" + selectedTabId);
    		var subMenuActive = subMenu.is(':visible');
    		if (subMenuActive == 'true') {
    			//If selectedTab is currently selected hide subMenu
    		fnb.hyperion.controller.raiseEvent('hideSubMenu');
    		}
    		else {
    			//If selectedTab has a subMenu linked to it toggle the subMenu
    			fnb.hyperion.controller.raiseEvent('showSubMenu', event);
			}
		}
	}
};

fnb.hyperion.utils.hyperlink.hyperlinkToUrl = function (event) {
	//Prevent default event
	event.preventDefault();
		//Var for the selected item
		var item = fnb.hyperion.$(event.currentTarget);
		//Get the url bound to the item
		var url = item.attr('href');
		if (url != '') {
			//Notify Controller to raise loadUrl event
			fnb.hyperion.controller.redirect(url);
		}
};

fnb.hyperion.utils.pageMenu.select = function (event) {
	//Prevent default event
	event.preventDefault();
	//Var for the selected item
	var item = fnb.hyperion.$(event.currentTarget);
	//Get the url bound to the item
	var url = item.attr('href');
	if (url != '') {
		//Notify Controller to raise loadUrl event
		fnb.hyperion.controller.redirect(url);
	}
};

fnb.hyperion.utils.subMenu.selectSubMenuItem = function (event) {
	//Stop the default behaviour of the dom element from firing
	//Create a holder for a our item
	var item = fnb.hyperion.$(event.currentTarget);
	//Store the href attribute of our item.
	var url = item.attr('href');
	var thirdParty = item.attr('data-thirdParty');
	//If the url is not empty load the url
	if (url != '' && thirdParty != 'true') {
		event.preventDefault ? event.preventDefault() : event.returnValue = false;
		fnb.hyperion.controller.redirect(url);
	};
}