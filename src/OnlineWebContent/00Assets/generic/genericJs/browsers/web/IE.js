///-------------------------------------------///
/// developer: Donovan
///
/// IE get and set caret for currency inputs
///-------------------------------------------///
/*
fnb.hyperion.forms.input.currency.getCaretPosition = function (element) {
	var caretStart = 0;

    var aRange = document.selection.createRange();
    var selLength = document.selection.createRange().text.length;
    aRange.moveStart('character', -element.value.length);
    caretStart = aRange.text.length - selLength;
    caretEnd = caretStart + aRange.text.length;
   
    return (caretStart);
};

fnb.hyperion.forms.input.currency.setCaretPosition = function (element,pos) {
	 // IE Support
    // Create empty selection range
    var aRange = document.selection.createRange();
    // Move selection start and end to 0 position
    aRange.moveStart('character', -element.value.length);
    // Move selection start and end to desired position
    aRange.collapse(true);
    aRange.moveEnd('character', pos);
    aRange.moveStart('character', pos);
    aRange.select();
};
*/