(function() {

    var d = document,
        w = window,
        j = d.createElement('div'),
        s = 'position:fixed;top:0;left:0;color:#FFF;background-color:#222;padding:0.4em 1em;font:14px sans-serif;z-index:999999',
        r = function() {
    		j.setAttribute('id','debug-window-size');
            if ( w.innerWidth === undefined )
                j.innerText = d.documentElement.clientWidth + 'x' + d.documentElement.clientHeight;
            else if ( d.all )
                j.innerText = w.innerWidth + 'x' + w.innerHeight;
            else
                j.textContent = window.innerWidth + 'x' + window.innerHeight;
		};

	d.body.appendChild( j );

	if( typeof j.style.cssText !== 'undefined' )
		j.style.cssText = s;
	else
		j.setAttribute('style', s);

	r();

    if ( w.addEventListener )
	    w.addEventListener('resize', r, false);
    else if ( w.attachEvent )
        w.attachEvent('onresize', r);
    else
        w.onresize = r;

})();