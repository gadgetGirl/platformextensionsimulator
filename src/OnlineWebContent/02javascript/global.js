
// ************************************************************ dimensions
// calulate
// find position of header and footer for no position fixed hack
// used with ifNoPositionFixed
// used with headeriScroll
var windowHeight = 0;
var windowWidth = 0;
var footerHeight = 0;
var bottomOffset = 0;

var ui = function() {};

///-----------------///
/// ADDAPT ANDROID  ///
///-----------------///
/* function initAndroid() {
	$("#footer").css("position","absolute");
	$("#main").css("top", "115px");
	$("#main").css("bottom","60px");
	$("#footerMessage").hide();
	$("#clearBtn").hide();
} */
///-----------------///
///  ADDAPT IPHONE  ///
///-----------------///
/* function initIphone() {
	$("#footerMessage").hide();
	$("#clearBtn").hide();
}

function windowResizeHandler() {
	try {
		//console.log("windowResizeHandler(): window resizing");
		windowHeight = $('html').height();
		windowWidth = $('html').width();
		footerHeight = $('#footer').height();
		bottomOffset = windowHeight - footerHeight;
		subTabsUnhider();
		
	} catch (e) {}
} */

/////WINDOW
/* var queries = [
	{'target': '#pageActionBar',
		'minWidth': -1,
		'minHeight':-1,
		'maxWidth': 759,
		'maxHeight':-1,
		'actions':[
			{'action': '$(".pageActionBarWrapper .firstChild").css("width","25%")'},
			{'action': '$(".pageActionBarWrapper .firstChild").show()'},
			{'action': '$(".pageActionBarWrapper .lastChild").css("width","75%")'},
			{'action': '$(".pageActionBarWrapper .lastChild").show()'}
			]
		},
	{'target': '#pageActionBar',
		'minWidth': 759,
		'minHeight':-1,
		'maxWidth': -1,
		'maxHeight':-1,
		'actions':[
			{'action': '$(".pageActionBarWrapper .firstChild").css("width","")'},
			{'action': '$(".pageActionBarWrapper .lastChild").css("width","")'}
		]
	}
];

function mediaQueriesDetecter(windowWidth,windowHeight) {
	
	$.each(queries, function(queryIndex, queryItem) {
        if($(queryItem.target)){
        	var actionTrigger = false;
        	var minWidth = queryItem.minWidth;
        	var minHeight = queryItem.minHeight;
        	var maxWidth = queryItem.maxWidth;
        	var maxHeight = queryItem.maxHeight;
        	var actions = queryItem.actions;
        	
        	if(minWidth != -1){
        		if(windowWidth < minWidth){
        			actionTrigger = true;
        		}
        	}
        	
        	if(minHeight != -1){
        		if(windowHeight < minHeight){
        			actionTrigger = true;
        		}
        	}

        	if(maxWidth != -1){
        		if(windowWidth > maxWidth){
        			actionTrigger = true;
        		}
        	}
        	
        	if(maxHeight != -1){
        		if(windowHeight > maxHeight){
        			actionTrigger = true;
        		}
        	}
        	
        	if(actionTrigger == true){
        		$.each(actions, function(actionIndex, actionItem) {
        			try{
        				$.globalEval(actionItem.action);
        			}catch(e){
        				console.log(e);
        			}
        		});
        	}
        }
    });
	 
}

function loadContent(pageLocation) {

	// $("#content").load(pageLocation);

	if ($('body').attr('id') == 'banking') {
		// rather use ajax load if in banking - this set up is for testing
		// (i.e. refreshing ajax is a pain in the ass
		var setUpURL = pageLocation.split('.');
		var loadURL = setUpURL[0] + 'Holder.jsp';
		location.href = loadURL;
	} else {
		location.href = pageLocation;
	}

} */

// ************************************************************ get url
// parameters

function getUrlVars() {
	var vars = [], hash;
	var hashes = window.location.href.slice(
			window.location.href.indexOf('?') + 1).split('&');
	for ( var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}
/* 
// ************************************************************ subtabs

function checkSubTab() {

	// if subtab parameter exists, do to that subtab
	getUrlVars();
	var whichTab = getUrlVars()["subTab"];
	if (whichTab != null) {
		$('.subTabsContent .subTab').hide();
		$('.subTabsContent .subTab' + whichTab).show();
		$('.subTabsMenu .subTab').removeClass('active');
		$('.subTabsMenu .subTab' + whichTab).addClass('active');

		calculateDimensions()
		if (windowWidth < 759)

			$('.subTab2').hide();
		$('.subTab' + whichTab).show();
		$('.subTabsMenu .subTab' + whichTab).addClass('active');
	}

	return false;
}

// ************************************************************ subtabs

function subTabs(whichTab, me) {

	$('.subTabsContent .subTab').hide();
	$('.subTabsContent .subTab' + whichTab).show();
	$('.subTabsMenu .subTab').removeClass('active');
	$('.subTabsMenu .subTab' + whichTab).addClass('active');
	return false;

} */

switcher = function() {}
switcher.show = function(selector, me) {

	$('.switchable').hide();
	$(selector).show();

	switcher.setSelected(me);

	return false;
	
}
switcher.setSelected = function(me) {
	var switcher = $(me).parent(".tableControlsSwitcherHolder");
	$(switcher).find(".switchItem").removeClass("active");
	$(me).toggleClass("active");
	return false;
}
// ************************************************************ showtable

function showTable(whichTable, me) {

	$('.tableList').hide();
	$(whichTable).show();

	var switcher = $(me).parent(".tableControlsSwitcherHolder");
	$(switcher).find(".switchItem").removeClass("active");
	$(me).toggleClass("active");

	return false;
}

function showOptions(me) {

	$(me).toggleClass("selectOpen");

	return false;
}

function hideOptions(me) {
	$(me).toggleClass("selectOpen");

	return false;
}

function selectOption(me) {

	var parentTag = $(me).parents(".formSelect");
	parentTag.find(".formsSelectOption").removeClass("selected");
	$(me).toggleClass('selected');

	parentTag.find(".selectInput").val(me.innerHTML);
	parentTag.find(".selectValue").val($(me).attr('value'));

	return false;
}

function dropDownSelectOption(me, callback) {

	var parentTag = $(me).parents(".dropDown");
	parentTag.find(".dropDownItem").removeClass("selected");
	$(me).toggleClass('selected');

	var selectedText = parentTag.find(".selectedItem");
	var selectedValue = parentTag.find(".selectValue");

	selectedText[0].innerHTML = me.innerHTML;
	$(selectedValue[0]).val($(me).attr('value'));

	if (callback && typeof (callback) === "function") {
		// execute the callback, passing parameters as necessary
		callback();
		return true;
	} else {
		return false;
	}
}

function onRadioItemClick(radioButtonObject) {
	var parentTag = $(radioButtonObject).parents(".radioGroup");
	var radioButtons = parentTag.find(".radioItem");

	radioButtons.removeClass("selected");
	$(radioButtonObject).addClass("selected");

	parentTag.find(".radioGroupValue").val($(radioButtonObject).attr('value'));
}

function onCheckboxItemClick(checkboxObject) {
	var parentTag = $(checkboxObject).parents(".checkboxItem");
	$(checkboxObject).toggleClass('selected');

	if ($(checkboxObject).hasClass("selected")) {
		parentTag.find(".checkboxValue").val('true');
	} else {
		parentTag.find(".checkboxValue").val('false');
	}
}

function pad(n) {
	if (n < 10)
		return "0" + n;
	return n;
}

// ************************************************************ rowshow

function rowshow(me, whichTable, whichSize) {

	$(me).parents('.tabler').addClass('open');
	$(me).parents('.tableGroup').siblings('.tableGroup')
			.css('display', 'block');

	$('#' + whichTable).removeClass('one');
	$('#' + whichTable).addClass(whichSize);

	$('.rowButton').hide();

}

// ************************************************************ phone acc
// switcher hider

function closeAccSwitcher() {

	$('.tableControlsAccountPicker').removeClass('zoom');
	$('.tableControlsFilter').css('display', 'block');
	$('.tableControlsSwitcher').css('display', 'block');
	$('.tableControlsAccountPicker').css('width', '65%');
	$('.tableControlsAccountPicker').css('top', '39px');
	$('.tableControlsAccountPicker2').css('top', '0');
	$('#close').css('display', 'none');

	return false;

}

// ************************************************************ table styler

function tableStyler() {

	/* add col numbers */
	$('.tableh').each(function() {

		$(this).find('.item').each(function(howManyColumns) {

			howManyColumns++;

			$(this).addClass('col' + howManyColumns);

		});

	});

	$('.tabler').each(function() {

		$(this).find('.item').each(function(howManyColumns) {

			howManyColumns++;

			$(this).addClass('col' + howManyColumns);

		});

	});

}

// ************************************************************ unhider slider
// function

function unhider(id, what, direction, menu, old_item) {

	calculateDimensions();
	if (windowWidth < 760) {

		if (menu == 'hide') {
			$('#topBar').hide();
			$('#headerTabs').hide();
			$('#submenu').hide();
		}

	}

	if (typeof (old_item) != "undefined") {
		whichDivCurrent = $(old_item).attr('id');
		;
		console.log("old_item supplied: " + whichDivCurrent);
	} else {
		// which is currently visible content area
		whichDivCurrent = $('.pageGroup:visible').attr('id');
	}

	// which div is being unhidden
	// var whichDivShow = $(what).attr('id');

	// show the appropriate div
	$(what).show();

	// clear buttons
	// $('#footer').html('');

	// directions
	if (direction == "left") {
		// set the start position of the unhidden iSroll
		$(what).css('position', 'absolute');
		$(what).css('left', '100%');

		$('#' + whichDivCurrent).animate({
			left : '-=100%'
		}, 500, function() {
			// Animation complete.
			$('#' + whichDivCurrent).css('display', 'none');
			// $('html').css('overflow', 'auto');

		});

		$(what).animate({
			left : '-=100%'
		}, 500, function() {
			// Animation complete.
		});
	}

	else if (direction == "right") {
		// set the start position of the unhidden iSroll
		$(what).css('position', 'absolute');
		$(what).css('left', '-100%');
		$('#' + whichDivCurrent).animate({
			left : '+=100%'
		}, 500, function() {
			// Animation complete.
			$('#' + whichDivCurrent).css('display', 'none');
			// $('html').css('overflow', 'auto');

		});

		$(what).animate({
			left : '+=100%'
		}, 500, function() {
			// Animation complete.
		});

	}

	else if (direction == "down") {
		// set the start position of the unhidden iSroll
		$(what).css('position', 'absolute');
		$('#' + whichDivCurrent).css('position', 'absolute');
		$(what).css('top', '-100%');

		$('#' + whichDivCurrent).animate({
			top : '+=100%'
		}, 500, function() {
			// Animation complete.
			$('#' + whichDivCurrent).css('display', 'none');
			// $('html').css('overflow', 'auto');

		});

		$(what).animate({
			top : '+=100%'
		}, 500, function() {
			// Animation complete.
			$(what).css('height', 'auto');
			$(what).css('overflow', 'auto');
			if (windowWidth < 760) {
				$(what).css('position', 'static');

				if (menu == 'show') {
					$('#topBar').show();
					$('#headerTabs').show();
					$('#submenu').show();
				}

			}

		});
	}

	else if (direction == "up") {
		// set the start position of the unhidden iSroll
		$(what).css('position', 'absolute');
		$('#' + whichDivCurrent).css('position', 'absolute');
		$(what).css('top', '100%');

		$('#' + whichDivCurrent).animate({
			top : '-=100%'
		}, 500, function() {
			// Animation complete.
			$('#' + whichDivCurrent).css('display', 'none');
			// $('html').css('overflow', 'auto');

		});

		$(what).animate({
			top : '-=100%'
		}, 500, function() {
			// Animation complete.
			$(what).css('height', 'auto');
			$(what).css('overflow', 'auto');
			if (windowWidth < 760) {
				$(what).css('position', 'static');

				if (menu == 'show') {
					$('#topBar').show();
					$('#headerTabs').show();
					$('#submenu').show();
				}

			}
		});
	}

	// function to handle sub tabs
	// $(subtab).css('display','block');
	// $('li.'+subtab).css('background','#006666');

	$(what).show();
	return false;

}

/* dropDown */

$(document).ready(function() {

	$(".dropDown").blur(function() {
		$(this).removeClass("selectOpen");
	});

	$(".dropDown").click(function() {
		$(this).toggleClass("selectOpen");
	});

	$(".dropDownItem").click(function()

	{

		var parentTag = $(this).parents(".dropDown");

		parentTag.find(".dropDownItem").removeClass("selected");

		$(this).toggleClass('selected');

		parentTag.find(".selectedItem")[0].innerHTML = this.innerHTML;

		parentTag.find(".selectValue").val($(this).attr('value'));

	});

});

/* end of dropDown */

otp = function() {}
otp.show = function(cellNumber, email) {

	if(_ezi.eziSliderExpanded == false){
		var params = {'cellNumber':cellNumber,' email': email}
		_ezi.show('/banking/Controller?nav=navigator.otp.OtpLanding&ezi=false','0.4','eziWrapperGrey','Ezi',params);
	}else{
		$.ajaxSettings.data = [];
		$.ajaxSettings.traditional = false;
		$.ajax({
			url:'/banking/Controller?nav=navigator.otp.OtpLanding&ezi=true',
			type: "POST",
			beforeSend: function () {

			},
			success: function(data,textStatus,jqXHR){
				$('#eziWrapper').html(data);
				$('#eziWrapper').find('.cellNumber').html(cellNumber);
				$('#eziWrapper').find('.email').html(email);
			},
			statusCode: {
			    500: function() { target.innerHTML = "500 Error"; }
			}
		});
	}

};
otp.hide = function() {
	// $('html').css('overflow', 'hidden');

	$(".otpGroup").animate({
		right : '-100%'
	}, 500, function() {
		$(".eziWrapper").hide();
		$(".otpGroup").hide();
		// $('html').css('overflow', 'auto');
	});
};

otp.submit = function(formname,intoEzi) {
	var eziPanelVisible = false;
	eziPanelVisible = _ezi && _ezi.eziSliderExpanded;
	
	if(intoEzi && intoEzi == 'true'){
		eziPanelVisible = false;
	}
	
	fnb.utils.eziSlider.submit(formname,eziPanelVisible);
};

var eziSourceId = "";

ezi = function() {
}

ezi.showBody = function(url) {
	fnb.controls.controller.eventsObject.raiseEvent('eziSliderShowBody', '');
	_ajaxLoader.loadUrl(_pageContainer,url,'','',false);
}

ezi.showPannel = function(sourceId, url,width,cssClass) {
	if($(sourceId).is('*')){
		eziSourceId = $(sourceId).attr('id');
	}
	_ezi.show(url,width,cssClass,'Ezi');
}

ezi.showBlankPanel = function() {
	_ezi.openEzi();
}

ezi.back = function(url) {
	_ajaxLoader.loadUrl("#eziWrapper",url,'','',false);
}

ezi.hide = function() {
	if($('#eziPannelButtonsWrapper').is('*')) $('#eziPannelButtonsWrapper').remove();
	_progress.loaderID = 'Main';
	_progress.reset();
	fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '');
}

fnb.utils.eziSlider.submit( = function(formName, intoWorkspace,buttonTarget) {
	if (intoWorkspace == true) {
		_progress.loaderID = 'Main';
		fnb.functions.submitFormToWorkspace.submit(formName,'');
	}else {
		FormUtility.ajaxLoadForm(formName, "#eziWrapper",'',buttonTarget);
	}
}

ezi.gotoPage = function(url) {
	var me = this;
	me.div = $("#eziWrapper");

	FormUtility.ajaxLoadDiv(me.div[0], url, function(data, textStatus, jqXHR) {
		var responseHeaders = jqXHR.getAllResponseHeaders();
		var navErrorCode = jqXHR.getResponseHeader("NAV_ERROR_CODE");
		var navErrorMessage = jqXHR.getResponseHeader("NAV_ERROR_MESSAGE");
		if (navErrorCode != null && navErrorCode != 0) {
			$(me.div).html(
					"There's an error !!! Code[" + navErrorCode
							+ "] ErrorMessage[" + navErrorMessage + "]");
			return false;
		}
		return true;
	});
}

ezi.submitSelection = function(selectedValue) {
	_progress.loaderID = 'Main';
	$('#'+eziSourceId).val(selectedValue);
	fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '');
	$("body").css('overflow', 'visible');
}

// /Public Recipient Search Options

ezi.submitPublicRecipientResultSelection = function(selectedDescription,selectedRefNum,selectedCountryCode) {
	_progress.loaderID = 'Main';
	$('#' + eziSourceId).val(selectedDescription);
	$('#' + eziSourceId + "RefNum").attr('value', selectedRefNum);
	$('#' + eziSourceId + "CountryCode").attr('value', selectedCountryCode);
	fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '');
}

// The main call which groups the tabs into their own groups
function groupMenus()
{
	// Get amount of tabs to being returned to the page
	var _tabsCount =refnum $("#headCenter3 > div").size();
	// alert($("#headCenter3 > div"));
	var _itemsPerGroup = itemsToShow();
	
	// Create a div prefix name
	var divPref = '_divGroup';
	var classPref = 'groupedClass';
	
	var _groupCount = _tabsCount / _itemsPerGroup;
	
	// How many groups to use
	
	if(Math.round(_groupCount) >= _groupCount)
		{_groupCount = Math.round(_groupCount);}
	else
		{_groupCount = Math.round(_groupCount) + 1;}

// This will run when the window gets resized
/* $(window).resize(function () { 
    //waitForFinalEvent(function(){ 
    		//RecreateMenus();
     //   }, 500); 
}); */


function RecreateMenus()
{ $('#headCenter3').children('div').each(function(index) {
		$('#' + $(this).attr('id')).children(":first").unwrap(); });

	groupMenus();
	
	if(isMobile.iOS())
		{
			$('#headLeft').hide();
			$('#headRight').hide();
		}
}

// This function will be used to set the amount of tabs to show based on screen
// size
function itemsToShow()
{ 
	var returnVal = 5;
	var currentWidth = $('html').width();
		
	if(currentWidth <= 480)
	{returnVal = 3}
	else if(currentWidth >=480 && currentWidth <= 840)
	{returnVal = 6;}
	else if(currentWidth >=880)
	{returnVal = 10;}

	return returnVal;}

// Use this script when resizing the window so that the menu recreation is not
// called
// multiple times during the resize of the browser, but only when the resize is
// complete.
var waitForFinalEvent = (function () { 
	  var timers = {}; 
	  return function (callback, ms, uniqueId) { 
	    if (!uniqueId) { 
	      uniqueId = "Don't call this twice without a uniqueId"; 
	    } 
	    if (timers[uniqueId]) { 
	      clearTimeout (timers[uniqueId]); 
	    } 
	    timers[uniqueId] = setTimeout(callback, ms); 
	  }; 
	})();

	
if(_groupCount == 1)
	{

	$('#headLeft').hide();
	$('#headRight').hide();
	}
else
	{$('#headLeft').show();
	$('#headRight').show();}

	// Create an array of div names
	var _menuTabs = new Array(_tabsCount);
	$('#headCenter3').children('div').each(function(index) {
		_menuTabs[index] = $(this).attr('id'); });	
		
	// Create the divs to be used to for the groups
	var _groupId = 1;
	while(_groupCount >= _groupId)
		{
		var _groupName = divPref + _groupId;
		
		if(_groupId == 1)
			{$('<div id="' +_groupName + '"class="'+ classPref + ' activeMenu"></div>').appendTo($('#headCenter3'));}
		else
			{$('<div id="' +_groupName + '"class="'+ classPref + '"></div>').appendTo($('#headCenter3'));}
		
		_groupId++;}

		$('#headCenter2').css('width', '100%');
		$('#headCenter2').css('overflow', 'hidden');
	
		$('#headCenter3').css('width', _groupCount * 100 + '%');
		$('#headCenter3').css('float', 'left');
		
		$('.groupedClass').css('display', 'block');
		$('.groupedClass').css('float', 'left');
		$('.groupedClass').css('width', 100 / _groupCount + '%');
		
		
		var groupCount = 0;
	
	// Loop through array
	for (var i = 0; i < _menuTabs.length; i++) { 
		
		if(i % _itemsPerGroup == 0)
		{
			groupCount++;
		}
		$("div[id='"  + _menuTabs[i] + "']").appendTo($("#" + divPref + groupCount));
	}
	
	var _tabWidth = 100 / _itemsPerGroup;

	$('.tab').removeAttr('style');
	$('.tab').attr('style','');
	$('.tab').css('width', _tabWidth.toString() + '%');
	
	}

// When navigation has been clicked, the below function will call the next or
// previous
// group of tabs
function NavMenu(direction)
{
	// This variable will be the next group of tabs to show
	var divToShow;
	// This variable will be the group which will be "leaving" the page display
	var divToHide = $('.activeMenu').attr('id');

	// Which way to move
	if(direction == 'left') // Going left
		{ divToShow = $('.activeMenu').prev('div').attr('id'); }
	else // Going right
		{ divToShow = $('.activeMenu').next('div').attr('id'); }
	
	// If trying to go into a direction which does not have a group
	if(divToShow != undefined)
	{
		$('.activeMenu').removeClass('activeMenu');
		$('#' + divToShow).addClass('activeMenu');
						
		$('#' + divToHide).hide();
		$('#' + divToShow).show();
	}

	// Call to show or hide arrows when there are other groups available or not
	ShowHideArrows();
}

// Shows or hides the arrow button images if there is/is not a waiting group on
// either side.
function ShowHideArrows()
{
	// Disable or enabled the next scrolling buttons if more menu items exist
	if($('.activeMenu').next('div').attr('id') == undefined)
	{ $('#headRight').css('background', 'none'); }
	else
	{$('#headRight').css('background', 'url(/banking/03images/fnb/ui/tabBorderRight.gif) right top repeat-y, url(/banking/03images/fnb/arrows/turqRight.gif) center center no-repeat');}

	if($('.activeMenu').prev('div').attr('id') == undefined)
	{ $('#headLeft').css('background', 'none'); }
	else
	{$('#headLeft').css('background', 'url(/banking/03images/fnb/ui/tabBorderRight.gif) right top repeat-y, url(/banking/03images/fnb/arrows/turqLeft.gif) center center no-repeat');}
		
}

// Check if the os is a mobile device or not
var isMobile = {    
		Android: function() 
		
		{        
			return navigator.userAgent.match(/Android/i) ? true : false;    
			},    
			
			BlackBerry: function() 
			{        return navigator.userAgent.match(/BlackBerry/i) ? true : false;    },    
			iOS: function() {        return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;    },    
			Windows: function() {        return navigator.userAgent.match(/IEMobile/i) ? true : false;    },    
			any: function() {        
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());    }};



/** *******Gerhard Scripts********* */

var confirmPageLoaded = function(){
	try{
		$('#confirmDetails').find('.'+divShown).attr('style','display:block');
		$("." + divShown).show();
	}catch(e){
		
	}
};

var defaultSelection = function(){
	  $('#notGlobal').addClass("selected");
	  $('#isGlobal').val("notGlobal");
	};
	

/* Update Element Attributs */
/* Example */
/*
 * updateElementAttributes([{attr:
 * elementValueList[$(this).closest('#group1_column_group').parent().index('.tableRow')].attr,
 * returnVal:
 * $(this).closest('#group1_column_group').parent().find('#recipientRfn').text(),elementId:
 * elementValueList[$(this).closest('#group1_column_group').parent().index('.tableRow')].elementId}])
 */
function updateElementAttributes(elements) {

	var returnElementValueList = $.makeArray(elements);
	
	$.each(returnElementValueList, function(index, item) {
        if(item.attr == 'content'){
			$(item.elementId).html(item.returnVal);
        }else{
    		$(item.elementId).attr(item.attr,item.returnVal);
        }
    });
    
}


// number picker functions
numberInput = function() {
}

numberInput.keydown = function(me,event) {

	if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 9 || event.keyCode == 32)				
    {
		return true;
	}
	else
    if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ))
    {
    	return false;	
    }
	
}

numberInput.keyup = function(me,max) {
	
	var numberPickerVal = parseInt($(me).val());
	
	if(numberPickerVal>max){
		$(me).attr('value','');
		$(me).attr('value',max);
		alert('No more than '+max+' Quick picks can be selected.');
	}
	
}

numberInput.increase = function(max,me) {
	
	var numberPickerVal = parseInt($(me).parent().parent().find(":input").val());
	
	if(numberPickerVal<max){
		$(me).parent().parent().find(":input").attr('value',(numberPickerVal+1));
	}
	
}

numberInput.decrease = function(me) {
	
	var numberPickerVal = parseInt($(me).parent().parent().find(":input").val());
	
	if(numberPickerVal>0){
		$(me).parent().parent().find(":input").attr('value',(numberPickerVal-1));
	}
	
}
//Ezi Notifier Options OLD - Needs to be updated
var currentEziNotificationsSelections = [];

eziNotifications = function() {
}

eziNotifications.methodSelect = function(me, headerValue, inputType, displaySubject) {
	
	var parentTag = $(me).parents(".dropDown");
	parentTag.find(".dropDownItem").removeClass("selected");
	$(me).toggleClass('selected');

	parentTag.find(".selectValue").val($(me).attr('value'));
	parentTag.find(".notify_selectedItem").find('h3').replaceWith('<h3>'+$(me).find('h3').text()+'</h3>')

	if(inputType == "numeric"){
		$(me).closest('.addNotificationFull').find('.gridQuart').show();
		$(me).closest('.addNotificationFull').find('.gridTwoThird').css('width', '75%');
	}else{
		$(me).closest('.addNotificationFull').find('.gridQuart').hide();
		$(me).closest('.addNotificationFull').find('.gridTwoThird').css('width', '100%');
	}

	var displaySubjectBool = new Boolean(displaySubject.toLowerCase()=="true"?1:0);
	
	if(displaySubjectBool!=true){
		$('.addNotificationSubject').hide();
	}else{
		$('.addNotificationSubject').show();
	};

}

var paymentNotifications = [];

eziNotifications.add = function(max) {

	if($('.recipientlistItem').size() < max){

		var i = $('.recipientlistItem').size()+1;

		var contactCode = $('#codeInput').val();
		var contact = $('#contactInput').val();
		var type = $('.notify_selectedItem').text();
		var subject = $('#subjectInput').val();
		 
		var $orginalNotificationsRow = $('#recipientlistItem_template');
		var $clonedNotificationsRow = $orginalNotificationsRow.clone();
		var $headerString = contactCode+contact+'  '+type;

		$clonedNotificationsRow.find("#code").val(contactCode);
		$clonedNotificationsRow.find("#contact").val(contact);
		$clonedNotificationsRow.find("#type").val(type);
		$clonedNotificationsRow.find("#subject").val(subject);
		
		$clonedNotificationsRow.find("#code").attr("name", "code_"+$('.recipientlistItem').size());
		$clonedNotificationsRow.find("#code").attr("id", "code_"+$('.recipientlistItem').size());
		$clonedNotificationsRow.find("#contact").attr("name", "contact_"+$('.recipientlistItem').size());
		$clonedNotificationsRow.find("#contact").attr("id", "contact_"+$('.recipientlistItem').size());
		$clonedNotificationsRow.find("#type").attr("name", "type_"+$('.recipientlistItem').size());
		$clonedNotificationsRow.find("#type").attr("id", "type_"+$('.recipientlistItem').size());
		$clonedNotificationsRow.find("#subject").attr("name", "subject_"+$('.recipientlistItem').size());
		$clonedNotificationsRow.find("#subject").attr("id", "subject_"+$('.recipientlistItem').size());

		$clonedNotificationsRow.find(".recipientlistItemNumberValue").text(i);	
		$clonedNotificationsRow.find(".recipientlistItemContentHeaderValue").text($headerString);
		$clonedNotificationsRow.find(".recipientlistItemContentSubjectValue").text(subject);
		$clonedNotificationsRow.attr("style", "");
		$clonedNotificationsRow.attr("class", "recipientlistItem");

		$clonedNotificationsRow.fadeIn("slow").appendTo('#recipientlist');

		$('#paymentNotificationForm').find('.addNotificationFull').find('.gridQuart').hide();
		$('#paymentNotificationForm').find('.addNotificationFull').find('.gridTwoThird').css('width', '100%');
		
		var dropdown = $('#paymentNotificationForm').find(".dropDownBox3");
		$(dropdown).find(".dropDownItem").removeClass("selected");
		$(dropdown).find(".dropDownItem:eq(0)").toggleClass('selected');
		
		$('.addNotificationSubject').show();
		$('#codeInput').val('Code');
		$('#codeInput').attr('defaultvalue','Code');
		
		$('#contactInput').val('Contact');
		$('#contactInput').attr('defaultvalue','Contact');
		
		$('#subjectInput').val('Subject');
		$('#subjectInput').attr('defaultvalue','Subject');
		
		$('.notify_selectedItem').find('h3').replaceWith('<h3>Email</h3>')

	}else{

		alert('You can only add '+max+' notifications!');

	}
}

eziNotifications.remove = function(me) {

	$(me).parents(".recipientlistItem").fadeOut('slow', function() {

		$(me).parents(".recipientlistItem").remove();

		var i = 0;

		$(".recipientlistItem").each(function() {
	        $(this).find("#contact").attr("id", "contact_"+i);
	        $(this).find("#contact").attr("name", "contact_"+i);
	        $(this).find("#type").attr("id", "type_"+i);
	        $(this).find("#type").attr("name", "type_"+i);
	        $(this).find("#subject").attr("id", "subject_"+i);
	        $(this).find("#subject").attr("name", "subject_"+i);
	        i++;
	        $(this).find(".recipientlistItemNumberValue").text(i);
		});

	});

}
eziNotifications.keypress = function(event) {
	if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 9 || event.keyCode == 32)				
	{
		return true;
	}
	else
	if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ))
	{
		return false;	
	}
}
function tableRowExpand(me) {

	var container = $(me).parent()
	var payBtt = container.find('.payBtt')
	var moreBtt = container.find('.moreOptionsBtt')
	var expandMe = container.find('.mobiBtt')
	var moreBttReal = container.find('.rowMoreButton')

	expandMe.show()
	payBtt.show()
	moreBtt.show()
	moreBttReal.show()
	moreBttReal.css('z-index','1')
	expandMe.hide()

	container.parent().css('height','84px')
}

function tableRowMoreExpand(me) {

	var container = $(me).parent()
	var moreBtt = container.find('.rowMoreButton')
	moreBtt.trigger('onclick');
}

function tableRowPayExpand(me) {

	var container = $(me).parent()
	var payBtt = container.find('.payBtt')
	var moreBtt = container.find('.moreOptionsBtt')
	var expandMe = container.find('.mobiBtt')
	var moreBttReal = container.find('.rowMoreButton')

	container.parent().css('height','auto')
	payBtt.hide()
	moreBtt.hide()
	moreBttReal.hide()
	expandMe.hide()
	container.find('.formElement, input, .hiddenLabel').css('visibility','visible')

	var containerCol3 = container.find('.col3')
	var containerCol4 = container.find('.col4')
	var containerCol5 = container.find('.col5')
	var containerCol6 = container.find('.col6')

	containerCol3.show()
	containerCol4.show()
	containerCol5.show()
	containerCol6.show()
}
