ui.errorConsole = function() {}
ui.errorConsole.populateAndShow = function(consoleTitle,content) {
	$("#errorConsoleTitle").html(consoleTitle);
	$("#errorConsoleBody").html(content);
	ui.errorConsole.show();
};
ui.errorConsole.show = function() {
	$("#errorConsole").show();
	$("#errorConsole").animate({bottom: '60px'}, 750, function() {	
	});
};
ui.errorConsole.close = function() {
	$("#errorConsole").animate({
		bottom : '-150px'
	}, 750, function() {});
};
ui.errorConsole.hide = function() {
	$("#errorConsole").animate({
		bottom : '-270px'
	}, 750, function() {
		$("#errorConsole").hide();
	});
};