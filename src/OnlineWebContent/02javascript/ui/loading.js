var ui = ui || {};
ui.loading = ui.loading || {};

ui.loading.init = function() {	
 	if(!ui.loading.loadingBar) {
	//	ui.loading.loadingBar = document.getElementById('loadingBar');
	//	ui.loading.loadingWrapper = document.getElementById('loadingWrapper');
	//	ui.loading.loadingBar.progressBar = document.getElementById('progress');
	}
}

ui.loading.start = function(text) {	
	//ui.loading.init();
	//ui.loading.loadingBar.style.width = '10%';
	//$('#formFooterButtons').hide();
	//ui.loading.loadingWrapper.style.display = 'block';
 	
	//ui.loading.loadingBar.innerHTML = text;
	//ui.loading.setValue(70,50);
}

ui.loading.setValue = function(value,speed) {
	if (ui.loading.loadingBar.timer){
		//clearTimeout(ui.loading.loadingBar.timer);
	}
	var i = ui.loading.loadingBar.style.width.replace('%','');
	ui.loading.loadingBar.timer = 
		setInterval(function() {
			if(i > value){
				//clearTimeout(ui.loading.loadingBar.timer);
			}
			ui.loading.loadingBar.style.width = (i++)+"%";
			if(i > 90){
				clearTimeout(ui.loading.loadingBar.timer);
				//ui.loading.loadingWrapper.style.display = 'none';
				//$('#formFooterButtons').show();
			}
		}, speed);
}

ui.loading.finish = function() {
	//ui.loading.init();
	//ui.loading.setValue(90,1);
}