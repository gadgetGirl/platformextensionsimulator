ui.checkbox = function() {}
ui.checkbox.disable = function(selector) {
	var formelement = $(selector+"_checkbox").parent(".formElement");
	formelement.addClass('disabled');
}
ui.checkbox.enable = function(selector) {
	var formelement = $(selector+"_checkbox").parent(".formElement");
	formelement.removeClass('disabled');
}
ui.checkbox.getSelectedValue = function(selector) {
	var checkbox = $(selector);
	return checkbox.val();
}
ui.checkbox.setSelected = function(checkbox) {
	var checkbox = $(checkbox + "_checkbox");
	var formelement = $(checkbox).parents(".formElement");
	var parentTag = $(checkbox).parents(".checkboxItem");
	$(checkbox).addClass('selected');
	parentTag.find(".checkboxValue").val('true');
	parentTag.find(".checkboxValue").attr('checked',true);
}
ui.checkbox.setDeselected = function(checkbox) {
	var checkbox = $(checkbox + "_checkbox");
	var formelement = $(checkbox).parents(".formElement");
	var parentTag = $(checkbox).parents(".checkboxItem");
	$(checkbox).removeClass('selected');
	parentTag.find(".checkboxValue").val('false');
	parentTag.find(".checkboxValue").attr('checked',false);
}
ui.checkbox.toggleSelect = function(checkbox) {
	var checkbox = $(checkbox);
	var formelement = $(checkbox).parents(".formElement");
	if (!formelement.hasClass('disabled')) {
		var parentTag = $(checkbox).parent(".checkboxItem");
		$(parentTag).toggleClass('selected');

		if ($(parentTag).hasClass("selected")) {
			//parentTag.find(".checkboxValue").val('true');
			parentTag.find(".checkboxValue").attr('checked',true);
		} else {
			//parentTag.find(".checkboxValue").val('false');
			parentTag.find(".checkboxValue").attr('checked',false);
		}
	}
}


ui.selectButton = function() {}
ui.selectButton.disable = function(selector) {
	var formelement = $(selector).parents(".checkboxItem");
	formelement.addClass('disabled');
}
ui.selectButton.enable = function(selector) {
	var formelement = $(selector).parents(".checkboxItem");
	formelement.removeClass('disabled');
}
ui.selectButton.selectItem = function(checkboxObject) { 
	var parentTag = $(checkboxObject).parents(".checkboxItem");
	$(parentTag).toggleClass('selected');

	if($(parentTag).hasClass("selected")) {
		parentTag.find(".checkboxValue").attr('checked',true);
		
		var checkboxText = parentTag.find(".checkboxText");
		checkboxText.html(checkboxText.attr('onText'));
	}
	else {
		parentTag.find(".checkboxValue").attr('checked',false);
		
		var checkboxText = parentTag.find(".checkboxText");
		checkboxText.html(checkboxText.attr('offText'));
	}
}
ui.selectButton.toggleAll = function(checkboxObject, container, selector) {
	var parentTag = $(checkboxObject).parents(container);
	var allCheckboxItems = parentTag.find('.checkboxItem');
	
	if ($(checkboxObject).hasClass('selected')) {
		$(checkboxObject).removeClass('allSelected');
	}
	else{
		$(checkboxObject).addClass('allSelected');
	}
	for (i=0; i <= allCheckboxItems.length; i++) {
		var checkboxText = $(allCheckboxItems[i]).find(".checkboxText");
		if($(checkboxObject).hasClass('allSelected')) {
			$(allCheckboxItems[i]).find(".checkboxValue").attr('checked',true);
			checkboxText.html(checkboxText.attr('onText'));
			$(allCheckboxItems[i]).addClass('selected');
		}
		else {
			$(allCheckboxItems[i]).find(".checkboxValue").attr('checked',false);
			checkboxText.html(checkboxText.attr('offText'));
			$(allCheckboxItems[i]).removeClass('selected');
		}
	}
}