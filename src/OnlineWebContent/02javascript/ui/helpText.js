var ui = ui || {};
ui.helpText = ui.helpText || {};

ui.helpText.init = function(oTarget) {
	
	if (!ui.helpText.helptextContainer) {
		x = document.getElementById('workspace');
		ui.helpText.helptextContainer = document.createElement('div');
		ui.helpText.helptextContainer.style.display = 'none';
		ui.helpText.helptextContainer.id = 'ui-helpText';
		ui.helpText.helptextContainer.className = 'boxShadow';
		document.body.appendChild(ui.helpText.helptextContainer);

		var borderFrame = document.createElement('div');
		borderFrame.className = 'ui-helpText-frame';
		ui.helpText.helptextContainer.appendChild(borderFrame);

		ui.helpText.helptextCloseButton = document.createElement('div');
		ui.helpText.helptextCloseButton.className = 'ui-helpText-close-button';
		borderFrame.appendChild(ui.helpText.helptextCloseButton);

		ui.helpText.helptextMessage = document.createElement('div');
		ui.helpText.helptextMessage.className = 'ui-helpText-message';
		borderFrame.appendChild(ui.helpText.helptextMessage);

		var closeFunction = function() {
			ui.helpText.hideHelptext();
		};
//		ui.helpText.helptextContainer.addEventListener('click', stopBubble, false);
		console.log($(ui.helpText.helptextCloseButton));
		$(ui.helpText.helptextCloseButton).bind('click', function() {ui.helpText.hideHelptext();});
//		document.addEventListener('click', function() {ui.helpText.hideHelptext()}, false);

	}
};

ui.helpText.showHelptext = function(oTarget, event) {
	ui.helpText.init(oTarget);
	
	if (ui.helpText.helptextContainer.target != oTarget) {
		ui.helpText.helptextContainer.target = oTarget;
		ui.helpText.helptextMessage.innerHTML = oTarget.getAttribute('helptext');

		ui.helpText.helptextContainer.style.top = (getOffset(oTarget).top+5)+"px";
		ui.helpText.helptextContainer.style.left = (getOffset(oTarget).left+ui.helpText.helptextContainer.target.offsetWidth+5)+"px";

	}
	
	$(ui.helpText.helptextContainer).fadeIn();
	event.stopPropagation();
};

ui.helpText.hideHelptext = function() {
	ui.helpText.helptextContainer.target = null;
	$(ui.helpText.helptextContainer).fadeOut();
};

function getOffset( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}

function stopBubble(e){
	e.stopPropagation();
}