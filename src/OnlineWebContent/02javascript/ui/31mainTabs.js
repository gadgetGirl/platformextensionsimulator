 var paddingAmount = $('.topTabNavigation').width();
   $(document).ready(function(){
	   
	   groupMenus();
	   loadedTopNav();
	});   
   
   var myScrollNavigation;
   function loadedTopNav() {
	   myScrollNavigation = new iScroll('headCenterContainer', {
	   snap: 'div .groupedClass',
	   
	   momentum: false,	   
	   hScrollbar: false,
	   vScrollbar: false,
	   onScrollEnd: function () { 
		   
		   
		   $(this).hide();
		
		   
		   //scrollHasHappened();
		}});    		
   }
      
   document.addEventListener('DOMContentLoaded', loadedTopNav, false);
      
   function scrollHasHappened()
   {
	   //alert(myScrollNavigation.pageX);
   }
   
   //add the class for the active menu item clicked
   $(".tabInsideInner").click(function() { 
		$(".tabInsideInner").removeClass("active");
		$(this).addClass("active");
	}); 
      
   //Navigate the tabs by clicking
   $("#headLeftNav").click(function() { 
	   myScrollNavigation.scrollToPage('prev'); 
	  
	   
	   });
   
   $("#headRightNav").click(function() { 
	   myScrollNavigation.scrollToPage('next'); 
	   
	   //alert('testtest');
	   var test = $('#headCenter div').attr('id');
	   //alert(test);
	   
	   }); 
   
 	//The call which groups the tabs into their own groups
   function groupMenus()
   {
		// Create a div prefix name
	   	var divPref = '_divGroup';
	   	var classPref = 'groupedClass';
	   	
		var currentWidth = $('html').width();
		$('#headCenterContainer').css('width', (currentWidth - (paddingAmount * 2)) + 'px');
		$('#headCenterContainer').css('left', paddingAmount + 'px');
		
		var test = $('#headCenterContainer').width();
		
	   	// Get amount of tabs to being returned to the page
	   	var _tabsCount = $("#headCenter > div.tabInside").size();	   	
	   	var _itemsPerGroup = itemsToShow();	   	
	   	var _groupCount = _tabsCount / _itemsPerGroup;
     	
	   	// How many groups to use
	   	if(Math.round(_groupCount) >= _groupCount)
	   		{_groupCount = Math.round(_groupCount);}
	   	else
	   		{_groupCount = Math.round(_groupCount) + 1;}
  	   	
	   	//This code is to hide the arrow nav divs if there is only one group
	   	if(_groupCount == 1)
	   	{$('.leftTopNav').hide();
		 $('.rightTopNav').hide();}
	    else
	   	{$('.leftTopNav').show();
	   	$('.rightTopNav').show();}

	   	// Create an array of div names
	   	var _menuTabs = new Array(_tabsCount);
	   	$('#headCenter').children('div.tabInside').each(function(index) {
	   		_menuTabs[index] = $(this).attr('id'); });	
   		
	   	//Create the divs to be used to for the groups
	   	var _groupId = 1;
		var currentWidthPadded = currentWidth - (paddingAmount * 2);

	   	while(_groupCount >= _groupId)
	   		{	var _groupName = divPref + _groupId;
	   		 	$('<div id="' +_groupName + '"class="'+ classPref + '"></div>').appendTo($('#headCenter'));
	   			_groupId++;}
	
   		$('#headCenter').css('width', (currentWidthPadded * _groupCount) + 'px');
   		$('.groupedClass').css('width', (currentWidthPadded) + 'px');
   		var tabSize = 100 / _itemsPerGroup;
   		$('.tabInside').css('width', tabSize + '%');
   		
   		var groupCount = 0;
   	   	// Loop through array
	   	for (var i = 0; i < _menuTabs.length; i++) { 
	   		
	   		if(i % _itemsPerGroup == 0)
	   		{
	   			groupCount++;
	   		}
	   		
	   		$("div[id='"  + _menuTabs[i] + "']").appendTo($("#" + divPref + groupCount));
	   	}
	}
 
 
	 	//This function will be used to set the amount of tabs to show based on screen size
	   function itemsToShow()
	   { 
	   	var returnVal = 5;
	   	var currentWidth = $('html').width();
	   		
	   	if(currentWidth <= 480)
	   	{returnVal = 3}
	   	else if(currentWidth >=480 && currentWidth <= 840)
	   	{returnVal = 6;}
	   	else if(currentWidth >=880)
	   	{returnVal = 7;}
	
	   	return returnVal;
	   	
	   }
   
	 	//This will run when the window gets resized
/* 	   $(window).resize(function () { 
		   RecreateMenus();
	   }); 
	 */
	   function RecreateMenus()
	   { $('#headCenter').children('div').each(function(index) {
	   		$('#' + $(this).attr('id')).children(":first").unwrap(); });
	
	   	groupMenus();
	  
	   }