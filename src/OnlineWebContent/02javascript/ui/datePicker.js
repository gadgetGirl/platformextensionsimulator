var ui = ui || {};
ui.datePicker = ui.datePicker || {};

ui.datePicker.init = function(object) {
	ui.datePicker.object = $(object);
	ui.datePicker.taget = ui.datePicker.object.find(".formsDatePickerValue")[0].id; 
	ui.datePicker.value = ui.datePicker.object.find(".formsDatePickerValue")[0].value; 
	
	if(!ui.datePicker.today){

		cal_days_labels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		cal_months_labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		cal_days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		
		ui.datePicker.today = new Date(); 
		ui.datePicker.todayValue = ui.datePicker.today.getFullYear() + "-" + pad(ui.datePicker.today.getMonth()+1) + "-" + pad(ui.datePicker.today.getDate());
		
		ui.datePicker.holder = document.getElementById('ui-datePickerHolder');
	}
		if (ui.datePicker.value == '') {
			ui.datePicker.currentDate = new Date();
		}
		else {
			ui.datePicker.currentDate = new Date(ui.datePicker.value);
		}
		ui.datePicker.month = ui.datePicker.currentDate.getMonth();
		ui.datePicker.year  = ui.datePicker.currentDate.getFullYear();
		
		ui.datePicker.prevYear = ui.datePicker.year - 1;
		ui.datePicker.nextYear = ui.datePicker.year + 1;
};

ui.datePicker.selectDay = function(object) {
	$('.numValue').removeClass("selected");
	$(object).find('.numValue').addClass("selected");
};

ui.datePicker.done = function() {
	var selectedDate = $(".numValue.selected").attr('value');
	if(selectedDate) {
		var datePicker = $("#"+ui.datePicker.taget).parents('.formsDatePicker');
		var dispValue = $(datePicker).find(".datePickerInput");
		$(dispValue)[0].innerHTML = selectedDate;
		
		$("#"+ui.datePicker.taget).val(selectedDate);
	}
};

ui.datePicker.loadCalendar = function(object) {
	ui.datePicker.init(object);
	ui.datePicker.buildMonths();	
};

ui.datePicker.gotoPrevMonth = function(month) {
	if(month == 11){
		ui.datePicker.year = ui.datePicker.year - 1;
	}
	ui.datePicker.month = month;
	ui.datePicker.buildMonths();	
};
ui.datePicker.gotoNextMonth = function(month) {
	if(month < ui.datePicker.month){
		ui.datePicker.year = ui.datePicker.year + 1;
	}	
	ui.datePicker.month = month;
	ui.datePicker.buildMonths();	
};
ui.datePicker.gotoToday = function() {
	ui.datePicker.year = ui.datePicker.today.getFullYear();
	ui.datePicker.month = ui.datePicker.today.getMonth();
	ui.datePicker.buildMonths();	
};
ui.datePicker.changeYear = function(year) {
//	ui.datePicker.month = 0;
	ui.datePicker.year = year;
	ui.datePicker.buildMonths();	
};

ui.datePicker.buildMonths = function() {
	// calc prev and next clicks
	ui.datePicker.prevYear = ui.datePicker.year - 1;
	ui.datePicker.nextYear = ui.datePicker.year + 1;

	ui.datePicker.prevMonth = ui.datePicker.month - 1;	
	if(ui.datePicker.prevMonth < 0){
		ui.datePicker.prevMonth = 11;
	}
	ui.datePicker.nextMonth = ui.datePicker.month + 1;
	if(ui.datePicker.nextMonth > 11){
		ui.datePicker.nextMonth = ui.datePicker.nextMonth - 12;
	}
	ui.datePicker.prevMonthName = cal_months_labels[ui.datePicker.prevMonth];
	ui.datePicker.nextMonthName = cal_months_labels[ui.datePicker.nextMonth];	
	
	ui.datePicker.holder.innerHTML = "<div class='nextPrev'>";	
	ui.datePicker.holder.innerHTML += "<div class='prevBtn formButton inverted' onclick='ui.datePicker.changeYear(" + ui.datePicker.prevYear + ")'>" + ui.datePicker.prevYear + "</div>" +
	"<div class='prevBtn formButton inverted' onclick='ui.datePicker.gotoPrevMonth(" + ui.datePicker.prevMonth + ")'>Prev</div>";		
	
	ui.datePicker.holder.innerHTML += "<div class='todayBtn formButton inverted' onclick='ui.datePicker.gotoToday()'>Today</div>";
	
	ui.datePicker.holder.innerHTML += "<div class='nextBtn formButton inverted' onclick='ui.datePicker.changeYear(" + ui.datePicker.nextYear + ")'>" + ui.datePicker.nextYear + "</div>" +
	"<div class='nextBtn formButton inverted' onclick='ui.datePicker.gotoNextMonth(" + ui.datePicker.nextMonth + ")'>Next</div>";
	ui.datePicker.holder.innerHTML += "<div class='clear'></div></div>";	
	
	// build 3 months
	var iyear = ui.datePicker.year;
	var imonth = ui.datePicker.month;
	for (i=0; i < 3; i++) {	
		ui.datePicker.holder.innerHTML += ui.datePicker.buildMonth(imonth,iyear);
		imonth++;
		if(imonth > 11) {
			imonth = 0;
			iyear++;
		}
	};
};

ui.datePicker.buildMonth = function(imonth, iyear) {
	cal_current_date = new Date(); 	

//  month = (isNaN(imonth) || imonth == null) ? cal_current_date.getMonth() : imonth;
//  year  = (isNaN(iyear) || iyear == null) ? cal_current_date.getFullYear() : iyear;
  var month = imonth;
  var year  = iyear;
  this.html = '';
  
  // get first day of month
  var firstDay = new Date(year, month, 1);
  var startingDay = firstDay.getDay();
  
  // find number of days in month
  var monthLength = cal_days_in_month[month];
  
  // compensate for leap year
  if (month == 1) { // February only!
    if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
      monthLength = 29;
    }
  }
  
  // do the header
  var monthName = cal_months_labels[month];
  var html = '<div class="month">';
  var yearCCYY = new String(year);
  html +=  "<div class='header'><div class='headerMonth'>" + monthName + "</div><div class='headerYear'><div class='headerYearCC'>" + yearCCYY.substr(0,2) + "</div><div class='headerYearYY'>" + yearCCYY.substr(2,2) + "</div></div></div>";
  html += '<div class="content"><ul>';
  for(var i = 0; i <= 6; i++ ){
      switch(i){
    	case 0:
    	    html += '<li class="day sunday">';
			break;
    	default:
    	    html += '<li class="day">';
      }
    html += cal_days_labels[i];
    html += '</li>';
  }
  html += '</ul>';

  // fill in the days
  var day = 1;
  // this loop is for is weeks (rows)
  for (var i = 0; i < 6; i++) {

	html += '<ul class="row' +i+ '">';
    // this loop is for weekdays (cells)
    for (var j = 0; j <= 6; j++) { 
      if (day <= monthLength && (i > 0 || j >= startingDay)) {
    	  var dateValue = year+'-'+pad(month+1)+'-'+pad(day);
    	  var className = '';
    	  if(dateValue == ui.datePicker.todayValue) {
    		  className += ' today'
    	  }
    	  if(dateValue == ui.datePicker.value) {
    		  className += ' selected'
    	  }
          switch(j){
	        	case 0:
        			html += '<li onclick="ui.datePicker.selectDay(this)" class="num sunday" ><div class="numValue' +className+ '" value="'+dateValue+ '">'+day+'</div></li>';
	  			break;
	        	default:
        			html += '<li onclick="ui.datePicker.selectDay(this)" class="num" ><div class="numValue' +className+ '" value="'+dateValue+ '">'+day+'</div></li>';
	        }
        day++;
      }
      else {
          switch(j){
	      	case 0:
				html += '<li class="blank sunday">&nbsp;</li>';
				break;
	      	default:
	      		html += '<li class="blank">&nbsp;</li>';
	      }
      }
    }
    html += '</ul>';
  }
  html += '</div></div>';

  return html;
};