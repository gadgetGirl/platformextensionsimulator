ui.radioGroup = function() {}
ui.radioGroup.setSelectedIndex = function(radioGroup, index) {
}
ui.radioGroup.disable = function(selector) {
	var formelement = $(selector).parents(".formElement");
	formelement.addClass('disabled');
}
ui.radioGroup.enable = function(selector) {
	var formelement = $(selector).parents(".formElement");
	formelement.removeClass('disabled');
}

/* Radio Button */
ui.radioButton = function() {}
ui.radioButton.setSelected = function(radioButton, selected) {}
ui.radioButton.disable = function(selector) {
	var formelement = $(selector);
	formelement.addClass('disabled');
}
ui.radioButton.enable = function(selector) {
	var formelement = $(selector);
	formelement.removeClass('disabled');
}
ui.radioButton.onClick = function(radioButton, callbackFunc) {	
	var formelement = $(radioButton).parents(".formElement");
	if (!formelement.hasClass('disabled')) {
		var parentTag = $(radioButton).parents(".radioGroup");
		var radioButtons = parentTag.find(".radioItem");

		radioButtons.removeClass("selected");
		$(radioButton).addClass("selected");
	
		parentTag.find(".radioGroupValue").val($(radioButton).attr('value'));
		
		if(typeof(callbackFunc)=="function"){
			callbackFunc();
		}
	}
}