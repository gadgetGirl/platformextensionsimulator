ui.dropdown = function() {}
ui.dropdown.getValue = function(selector) {
	return $(selector).val();
}
ui.dropdown.disable = function(selector) {
	var formelement = $(selector+"_dropdown").parent(".formElement");
	formelement.addClass('disabled');
	$(selector + "_dropdown").removeClass("selectOpen");
}
ui.dropdown.enable = function(selector) {
	var formelement = $(selector+"_dropdown").parent(".formElement");
	formelement.removeClass('disabled');
}
ui.dropdown.showOptions = function(dropdown) {
	var dropdown = $(dropdown);
	var formelement = $(dropdown).parent(".formElement");
	if (!formelement.hasClass('disabled')) {
		dropdown.toggleClass("selectOpen");
	}
}
ui.dropdown.hideOptions = function(dropdown) {
	var parent = $(dropdown);
	var dd = $(parent).find('.dropDown');
	dd.removeClass("selectOpen");
}
ui.dropdown.setValue = function(selector, value) {
	var dropdown = $(selector);	
	var formelement = dropdown.parents(".formElement");
	var selectedItem = formelement.find(".selectedItem");

	dropdown.val(value);
	
	var items = formelement.find('.dropDownItem');
	for (i=0; i <= items.length; i++) {
		if($(items[i]).attr('value') == value) {
			$(items[i]).addClass('selected');
			selectedItem[0].innerHTML = items[i].innerHTML;
			
		}
		else{
			$(items[i]).removeClass('selected');
		}
	}
	var formelement = $(dropdown).parent(".formElement");
}
ui.dropdown.reset = function(formElement) {
	var selectValue = formElement.find(".selectValue");
	var defaultValue = selectValue.attr('dafaultvalue');
	var selectedItem = formElement.find(".selectedItem");

	selectValue.val(defaultValue);
	
	var items = formElement.find('.dropDownItem');
	for (i=0; i <= items.length; i++) {
		if($(items[i]).attr('value') == defaultValue) {
			$(items[i]).addClass('selected');
			selectedItem[0].innerHTML = items[i].innerHTML;
		}
		else{
			$(items[i]).removeClass('selected');
		}
	}
}