ui.doSomething = function() {}
ui.doSomething.setValue = function(doSomething, value) {}
ui.doSomething.disable = function(selector) {
	var formelement = $(selector);
	formelement.addClass('disabled');
}
ui.doSomething.enable = function(selector) {
	var formelement = $(selector);
	formelement.removeClass('disabled');
}
