///----------------------------------------------------------------------///
/// developer: Donovan
/// JQUERY CONTAINS EXTENTION - CASE INSENSITIVE
///----------------------------------------------------------------------///
$.extend($.expr[":"], {
	"icontains": function(elem, i, match, array) {
		return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
	}
});
///-------------------------------------------------------------///
/// developer: Donovan
/// JQUERY VERTICALLY ALIGN EXTENTION
///-------------------------------------------------------------///
(function ($) {
	$.fn.verticallyAlign = function() {
		return this.each(function(){
			var thisHeight = $(this).height();
			var parentHeight = $(this).parent().height();
			var valign = ((parentHeight/2)-(thisHeight/2));
			$(this).css("margin-top", valign + "px");
		});	
	};
})(jQuery);
///---------------------------------------------------------///
/// developer: Donovan
/// JQUERY EQUAL HEIGHTS EXTENTION
///---------------------------------------------------------///
(function ($) {
	$.fn.equalHeights = function(type,minHeight) {
		$(this).children(type).css('height','');
		var heightArray = $(this).children(type).map( function(){
			 return  $(this).height();
		}).get();
		var maxHeight = Math.max.apply( Math, heightArray);
		if(maxHeight<minHeight) maxHeight=minHeight;
		$(this).children(type).css('height',maxHeight);
		return this;
	};
})(jQuery);
///---------------------------------------------------------///
/// developer: Donovan
/// JQUERY EQUAL WIDTHS EXTENTION
///---------------------------------------------------------///
(function ($) {
	$.fn.equalWidths = function(type) {
		var widthArray = $(this).children(type).map( function(){
			 return  $(this).width();
		}).get();
		var maxWidth = Math.max.apply( Math, widthArray);
		$(this).children(type).css('min-width',maxWidth+20);
		return this;
	};
})(jQuery);
///---------------------------------------------------------///
/// developer: Donovan
/// JQUERY WAIT FOR CONTENTS TO LOAD EXTENTION
///---------------------------------------------------------///
;(function($) {
    var namespace = 'waitForContent';
    $.waitForContent = {
		//Images Types
        imageProperties: [
	        'backgroundImage',
	        'listStyleImage',
	        'borderImage',
	        'borderCornerImage',
			'background'
		//To Do: Add more data types
        ]
    };
    $.expr[':'].uncached = function(obj) {
        if ( ! $(obj).is('img[src!=""]')) {
            return false;
        }
        var img = document.createElement('img');
        img.src = obj.src;
        return ! img.complete;
    };
    $.fn.waitForContent = function(finishedCallback, eachCallback, waitAllComplete) {
		
        var allImgsLength = 0;
        var allImgsLoaded = 0;
        if ($.isPlainObject(arguments[0])) {
            eachCallback = finishedCallback.each;
            waitAllComplete = finishedCallback.waitAllComplete;
            finishedCallback = finishedCallback.finished;
        }
        finishedCallback = finishedCallback || $.noop;
        eachCallback = eachCallback || $.noop;
        waitAllComplete = !! waitAllComplete;
        if ( ! $.isFunction(finishedCallback) || ! $.isFunction(eachCallback)) {
            throw new TypeError('Invalid callback.');
        }
        return this.each(function() {
            var obj = $(this);
			var allImgs = [];
            var hasImgProperties = $.waitForContent.imageProperties || [];
            var matchUrl = /url\(\s*(['"]?)(.*?)\1\s*\)/g;
			
            if (waitAllComplete) {
                obj.find('*').andSelf().each(function() {
                    var element = $(this);
                    if (element.is('img:uncached')) {
                        allImgs.push({
                            src: element.attr('src'),
                            element: element[0]
                        });
                    }
                    $.each(hasImgProperties, function(i, property) {
					
                        var propertyValue = element.css(property);
						var match;
                        if ( ! propertyValue) {
                            return true;
                        }
                        while (match = matchUrl.exec(propertyValue)) {
                            allImgs.push({
                                src: match[2],
                                element: element[0]
                            });
                        };
                    });
                });
            } else {
                obj
                 .find('img:uncached')
                 .each(function() {
                    allImgs.push({
                        src: this.src,
                        element: this
                    });
                });
            };
            allImgsLength = allImgs.length;
            allImgsLoaded = 0;
            if (allImgsLength == 0) {
                finishedCallback.call(obj[0]);
            }
            $.each(allImgs, function(i, img) {
                var image = new Image;
                $(image).bind('load.' + namespace + ' error.' + namespace, function(event) {
                    allImgsLoaded++;
                    eachCallback.call(img.element, allImgsLoaded, allImgsLength, event.type == 'load');
                    if (allImgsLoaded == allImgsLength) {
                        finishedCallback.call(obj[0]);
                        return false;
                    };
                });
                image.src = img.src;
            });
        });
    };
})(jQuery)
///-------------------------------------------------------------------------------------///
/// developer: Donovan
/// JQUERY MASK CURRENCY EXTENTION
///-------------------------------------------------------------------------------------///
var runningTableTotal = 0;
var enableTotaling = false;

;(function($) {
	$.maskCurrency = {};

	$.maskCurrency.regions = [];

	$.maskCurrency.regions[''] = {
		symbol: '',
		positiveFormat: '%s%n',
		negativeFormat: '(%s%n)',
		decimalSymbol: '.',
		digitGroupSymbol: ' ',
		groupDigits: true
	};

	$.fn.maskCurrency = function(destination, settings) {

		if (arguments.length == 1 && typeof destination !== "string") {
			settings = destination;
			destination = false;
		}

		var defaults = {
			name: "maskCurrency",
			colorize: false,
			region: '',
			global: true,
			roundToDecimalPlace: 2,
			eventOnDecimalsEntered: false
		};

		defaults = $.extend(defaults, $.maskCurrency.regions['']);

		settings = $.extend(defaults, settings);

		if (settings.region.length > 0) {
			settings = $.extend(settings, getRegionOrCulture(settings.region));
		}
		settings.regex = generateRegex(settings);
		return this.each(function() {
			$this = $(this);
			var num = '0';
			num = $this[$this.is('input, select, textarea') ? 'val' : 'html']();
			if (num.search('\\(') >= 0) {
				num = '-' + num;
			}
			if (num === '' || (num === '-' && settings.roundToDecimalPlace === -1)) {
				return;
			}
			if (isNaN(num)) {
				num = num.replace(settings.regex, '');
				if (num === '' || (num === '-' && settings.roundToDecimalPlace === -1)) {
					return;
				}
				if (settings.decimalSymbol != '.') {
					num = num.replace(settings.decimalSymbol, '.'); 
				}
				if (isNaN(num)) {
					num = '0';
				}
			}
			var numParts = String(num).split('.');
			var isPositive = (num == Math.abs(num));
			var hasDecimals = (numParts.length > 1);
			var decimals = (hasDecimals ? numParts[1].toString() : '0');
			var originalDecimals = decimals;
			num = Math.abs(numParts[0]);
			num = isNaN(num) ? 0 : num;
			if (settings.roundToDecimalPlace >= 0) {
				decimals = parseFloat('1.' + decimals);
				decimals = decimals.toFixed(settings.roundToDecimalPlace);
				if (decimals.substring(0, 1) == '2') {
					num = Number(num) + 1;
				}
				decimals = decimals.substring(2); 
			}
			num = String(num);
			if (settings.groupDigits) {
				for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
					num = num.substring(0, num.length - (4 * i + 3)) + settings.digitGroupSymbol + num.substring(num.length - (4 * i + 3));
				}
			}
			if ((hasDecimals && settings.roundToDecimalPlace == -1) || settings.roundToDecimalPlace > 0) {
				num += settings.decimalSymbol + decimals;
			}
			var format = isPositive ? settings.positiveFormat : settings.negativeFormat;
			var money = format.replace(/%s/g, settings.symbol);
			money = money.replace(/%n/g, num);

			var $destination = $([]);
			if (!destination) {
				$destination = $this;
			} else {
				$destination = $(destination);
			}
			$destination[$destination.is('input, select, textarea') ? 'val' : 'html'](money);
			if (
				hasDecimals && 
				settings.eventOnDecimalsEntered && 
				originalDecimals.length > settings.roundToDecimalPlace
			) {
				$destination.trigger('decimalsEntered', originalDecimals);
			}
			if (settings.colorize) {
				$destination.css('color', isPositive ? 'black' : 'red');
			}
		});
	};
	$.fn.toNumber = function(settings) {
		var defaults = $.extend({
			name: "toNumber",
			region: '',
			global: true
		}, $.maskCurrency.regions['']);
		settings = jQuery.extend(defaults, settings);
		if (settings.region.length > 0) {
			settings = $.extend(settings, getRegionOrCulture(settings.region));
		}
		settings.regex = generateRegex(settings);
		return this.each(function() {
			var method = $(this).is('input, select, textarea') ? 'val' : 'html';
			$(this)[method]($(this)[method]().replace('(', '(-').replace(settings.regex, ''));
		});
	};
	$.fn.asNumber = function(settings) {
		var defaults = $.extend({
			name: "asNumber",
			region: '',
			parse: true,
			parseType: 'Float',
			global: true
		}, $.maskCurrency.regions['']);
		settings = jQuery.extend(defaults, settings);
		if (settings.region.length > 0) {
			settings = $.extend(settings, getRegionOrCulture(settings.region));
		}
		settings.regex = generateRegex(settings);
		settings.parseType = validateParseType(settings.parseType);

		var method = $(this).is('input, select, textarea') ? 'val' : 'html';
		var num = $(this)[method]();
		num = num ? num : "";
		num = num.replace('(', '(-');
		num = num.replace(settings.regex, '');
		if (!settings.parse) {
			return num;
		}
		if (num.length == 0) {
			num = '0';
		}
		if (settings.decimalSymbol != '.') {
			num = num.replace(settings.decimalSymbol, '.'); 
		}
		return window['parse' + settings.parseType](num);
	};
	function getRegionOrCulture(region) {
		var regionInfo = $.maskCurrency.regions[region];
		if (regionInfo) {
			return regionInfo;
		}
		else {
			if (/(\w+)-(\w+)/g.test(region)) {
				var culture = region.replace(/(\w+)-(\w+)/g, "$1");
				return $.maskCurrency.regions[culture];
			}
		}
		return null;
	}
	function validateParseType(parseType) {
		switch (parseType.toLowerCase()) {
			case 'int':
				return 'Int';
			case 'float':
				return 'Float';
			default:
				throw 'invalid parseType';
		}
	}
	function generateRegex(settings) {
		if (settings.symbol === '') {
			return new RegExp("[^\\d" + settings.decimalSymbol + "-]", "g");
		}
		else {
			var symbol = settings.symbol.replace('$', '\\$').replace('.', '\\.');		
			return new RegExp(symbol + "|[^\\d" + settings.decimalSymbol + "-]", "g");
		}	
	}
})(jQuery);
///-------------------------------------------------------------------------------------///
/// developer: Donovan
/// JQUERY SET INPUT CURSOR POSITION
///-------------------------------------------------------------------------------------///
;(function($) {
  $.fn.setCursorPosition = function(pos) {
    if ($(this).get(0).setSelectionRange) {
      $(this).get(0).setSelectionRange(pos, pos);
    } else if ($(this).get(0).createTextRange) {
      var range = $(this).get(0).createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  }
})(jQuery);
///-------------------------------------------------------------------------------------///
/// developer: Donovan
/// JQUERY GET INPUT CURSOR POSITION
///-------------------------------------------------------------------------------------///
(function($) {
    $.fn.getCursorPosition = function() {
        var input = this.get(0);
        if (!input) return;
        if ('selectionStart' in input) {
            return input.selectionStart;
        } else if (document.selection) {
            input.focus();
            var sel = document.selection.createRange();
            var selLen = document.selection.createRange().text.length;
            sel.moveStart('character', -input.value.length);
            return sel.text.length - selLen;
        }
    }
})(jQuery);
///-------------------------------------------------------------------------------------///
/// developer: Donovan
/// JQUERY GET OUTER HTML SELECTOR
///-------------------------------------------------------------------------------------///
(function($) {
$.fn.outerHTML = function() {
	 return (!this.length) ? this : (this[0].outerHTML || (
      function(element){
          var div = document.createElement('div');
          div.appendChild(element.cloneNode(true));
          var contents = div.innerHTML;
          div = null;
          return contents;
    })(this[0]));
}
})(jQuery);
///-------------------------------------------------------------------------------------///
/// developer: Donovan
/// JAVASCRIPT INARRAY
///-------------------------------------------------------------------------------------///
Array.prototype.inArray = function (value)
{
	 var i;
	 for (i=0; i < this.length; i++)
	 {
		 if(this[i]!=undefined&&value!=undefined){
			 if (this[i].toString() == value.toString())
			{
				return true;
			}
		 }
	 }
	 return false;
};
//---------------------------------------------------------//
//	developer: Leon
//	IE-compatible checkbox handling
//  developer: Vaughan - 14/07/2013 (added return of object on methods for chainability)
//---------------------------------------------------------//
(function ($) {
	$.fn.setCheckbox = function(checkValue) {
		var me = $(this);

		if(checkValue){
			$(me).parent().addClass('checked');
			$(me).prop('checked',true);			// For real browsers
			$(me).attr('checked','checked');	// For IE8
		} else {
			$(me).parent().removeClass('checked');
			$(me).prop('checked',false);		// For real browsers
			$(me).attr('checked','');			// Setting the 'checked' property for IE8
			$(me).removeAttr('checked');		// Also for IE8
		}
		$(me).change();
		
		return me;
	};
})(jQuery);

(function ($) {
	$.fn.disableCheckbox = function() {
		$(this).prop('disabled',true).attr('disabled','disabled').change().parent().addClass('disabled');
		
		return $(this);
	};
})(jQuery);

(function ($) {
	$.fn.enableCheckbox = function() {
		$(this).prop('disabled',false).removeAttr('disabled').change().parent().removeClass('disabled');
		
		return $(this);
	};
})(jQuery);

//--------------------------------------------------------------------//
//  developer: Vaughan
//--------------------------------------------------------------------//
(function ($) {
	$.fn.checkboxIsChecked = function() {
		if($(this).attr('checked')) 
			{return true;}
		else
			{return false;}
	};
})(jQuery);
//--------------------------------------------------------------------//
//	developer:Donovan
//	Overide Jquery Click,TouchStart,TouchEnd
//--------------------------------------------------------------------//
(function ($) {
	var original =$.fn.click;
	$.fn.click = function(){
		var f = arguments[0];
		arguments[0] = function(e) {
			e.preventDefault();
			f(e);
		}
		original.apply( this, arguments );
	}
})(jQuery);

(function ($) {
	var originaltouchstart =$.fn.touchstart;
	$.fn.touchstart = function(){
		var f = arguments[0];
		arguments[0] = function(e) {
			e.preventDefault();
			f(e);
		}
		originaltouchstart.apply( this, arguments );
	}
})(jQuery);

(function ($) {
	var originaltouchend =$.fn.touchend;
	$.fn.touchend = function(){
		var f = arguments[0];
		arguments[0] = function(e) {
			e.preventDefault();
			f(e);
		}
		originaltouchend.apply( this, arguments );
	}
})(jQuery);
//--------------------------------------------------------------------//
//	developer:Donovan
//	Juery Timer Extention
//--------------------------------------------------------------------//
(function($) {
	$.timer = function(interval, callback, options) {
		var options = $.extend({ reset: 500 }, options);
		var interval = interval || options.reset;
		if(!callback) { return false; }
		var Timer = function(interval, callback, disabled) {
			this.internalCallback = function() { callback(self);clearInterval(self.id);};
			this.stop = function() { clearInterval(self.id); };
			this.reset = function(time) {
				if(self.id) { clearInterval(self.id); }
				var time = time || options.reset;
				this.id = setInterval(this.internalCallback, time);
			};
			this.interval = interval;
			if (!disabled) {
				this.id = setInterval(this.internalCallback, this.interval);
			}
			var self = this;
		};
		return new Timer(interval, callback, options.disabled);
	};
})(jQuery);
//--------------------------------------------------------------------//
//	developer:Mike
//	Some code he stole
//--------------------------------------------------------------------//
(function($,sr){

	  // debouncing function from John Hann
	  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
	  var debounce = function (func, threshold, execAsap) {
	      var timeout;

	      return function debounced () {
	          var obj = this, args = arguments;
	          function delayed () {
	              if (!execAsap)
	                  func.apply(obj, args);
	              timeout = null;
	          };

	          if (timeout)
	              clearTimeout(timeout);
	          else if (execAsap)
	              func.apply(obj, args);

	          timeout = setTimeout(delayed, threshold || 100);
	      };
	  }
	  // smartresize 
	  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

