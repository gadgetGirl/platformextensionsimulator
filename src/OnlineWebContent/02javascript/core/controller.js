///-------------------------------------------///
/// developer: Donovan
///
/// Controller
///-------------------------------------------///
$(function(){
	function controllerObject() {
		this.eventsObject;
		this.currentEventsGroup;
		this.url;
		this.target;
		this.preLoadingCallBack;
		this.postLoadingCallBack;
		this.params;
		this.queue;
		this.expectResponse;
		this.ajaxData;
		this.xhr;
		this.targetElement;
		this.beenSubmitted;
		
		this.rawImportedData;
		this.namespaceImports;
		this.rawImportedJs;
		this.jsImportTags;
		this.cssImportTags;
		this.contentImport;
		this.scriptsImportTags;
		this.importJsCounter;
		this.pendingJsSearch;
	}
	controllerObject.prototype = {
		activeObjectsList: {},
		init : function () {
			this.pendingJsSearch = false;
			this.createControllerObjects();
			this.attachTemplates();
			this.setCurrentEventsGroup('loadSite');
			fnb.utils.eziSlider.init($(window).width());
			fnb.controls.controller.setSkin();
		},
		createTopMenuObj : function () {
			if(typeof (tabCount)!='undefined') fnb.utils.topTabs.init(tabCount);
			fnb.functions.isScrolling.init();
		},
		createPageObj : function (target) {
			fnb.controls.page.init(target);
		},
		load : function (eventsGroup,target,url) {
			if(typeof eventsGroup=='undefined') eventsGroup = '';
			this.setCurrentEventsGroup(eventsGroup+'LoadComplete');
			this.setUrl(url);
			this.setTarget(target);
			fnb.utils.load.loadUrl();
		},
		loadFormToFrame : function (eventsGroup,loadObject) {
			var form = $("form[name='"+loadObject["formName"]+"']");
			console.log("submitting form to frame...");
			form.submit();
		},
		loadUrl : function (eventsGroup,target,url,preLoadingCallBack, postLoadingCallBack,params,queue,expectResponse,preventDefaults) {
			this.setTarget(target);
			var readyState = 4;
			var doLoad = true;
			if(typeof fnb.utils.loadUrl.xhr!= 'undefined'){readyState = fnb.utils.loadUrl.xhr.readyState;};
			if(readyState==1){
				if(queue==false){
					fnb.utils.ajaxManager.stop();
					fnb.utils.loadUrl.stop();
				}else{
					doLoad = false;
					var reqObj = {eventsGroup:eventsGroup ,target:target,url:url,preLoadingCallBack:preLoadingCallBack, postLoadingCallBack:postLoadingCallBack,params:params,queue:queue,expectResponse:expectResponse,preventDefaults:preventDefaults};
					fnb.utils.ajaxManager.addRequest(reqObj);
				}
			}
			if(doLoad){
				fnb.utils.loadUrl.load(eventsGroup,url,target,preLoadingCallBack,postLoadingCallBack,params,queue,expectResponse,preventDefaults);
			}
		},
		loadUrlComplete : function (loadObj) {
			var navError = false;
			if(!fnb.utils.navError.validate(loadObj.xhr,loadObj.postLoadingCallBack,loadObj.eventsGroup)){
				if(typeof loadObj.preventDefaults=='undefined'||loadObj.preventDefaults==false||fnb.hyperion.utils.eziPanel.active==false){	
					fnb.controls.controller.setDefaults();
				}else{
					fnb.controls.controller.setEziDefaults();
				}
				this.formBeenSubmitted = false;
				navError = true;
				return;
			};

			if(fnb.utils.handleOTP.validate(loadObj.xhr)) return;

			var eventsArray = ["loadUrlToWorkspace","loadUrl","actionMenuloadResultScreen","simpleLoadUrl","loadResultScreenFromActionMenu"];
			
			var inEventsGroup = eventsArray.indexOf(loadObj.eventsGroup)

			if(inEventsGroup!=-1&&navError==false&&!loadObj.async){
				console.log('Controller: Clear footer buttons')
		   		fnb.functions.footer.clear();
				fnb.hyperion.controller.raiseEvent("hideActionMenuButtonAndClear");
			}
			
			if(typeof(loadObj.preLoadingCallBack)=="function"){
				
				loadObj.preLoadingCallBack(loadObj.data,loadObj.eventsGroup,loadObj.xhr)};
				
			try{
				if(loadObj.data!=''){
					if(loadObj.url.indexOf("bankingFrame")==-1){
						$.ajaxSettings.data = [];
						$.ajaxSettings.traditional = false;
						$(loadObj.target).html(loadObj.data);
					}else{
						fnb.controls.controller.getExternalData(loadObj)
					}
				}else{
					if(loadObj.expectResponse!=false) this.setError('No data returned from request.')
				};
			}catch(e){
				if(typeof loadObj.preventDefaults=='undefined'||loadObj.preventDefaults==false) {this.setDefaults();}else{this.setEziDefaults();}
				fnb.controls.controller.eventsObject.raiseEvent('loadError', {height:'134px',message: 'Page error.', errors:[{error: e}]});
			}

			//Init page events for new framework
			fnb.hyperion.controller.initPageEvents();
			//Try send tracking data
			try{
				fnb.hyperion.utils.tracking.checkTrackingObject(loadObj.eventsGroup,loadObj);
			}catch(e){
				console.log("Tracking Error: "+e);
			}
			this.formBeenSubmitted = false;
			this.raiseEvent(loadObj.eventsGroup+'LoadUrlComplete', loadObj,navError);
			if(typeof(loadObj.postLoadingCallBack)=="function"){loadObj.postLoadingCallBack(loadObj.data,loadObj.eventsGroup,loadObj.xhr)};

			//this.inspectDomFunctions();
			
		},
		getExternalData : function (loadObj) {

			this.rawImportedData = loadObj.data;
			this.jsImportTags = this.rawImportedData.match(/<script[^>]*?class="webJsCore"[^>]*>([\s\S]*?)<\/script>/g);
			this.cssImportTags = this.rawImportedData.match(/<link[^>]*?[^>]*>/gi);
			this.scriptsImportTags = this.rawImportedData.match(/<script[^>]*?class="initJs"[^>]*>([\s\S]*?)<\/script>/g);
			
			this.rawImportedData = this.rawImportedData.replace(/<script[^>]*?[^>]*>([\s\S]*?)<\/script>/gi,'');
			this.rawImportedData = this.rawImportedData.replace(/<link[^>]*?[^>]*>/gi,'');
			this.rawImportedData = this.rawImportedData.replace(/<meta[^>]*?[^>]*>/gi,'');

			this.setExternalData(loadObj);
		},
		setExternalData : function (loadObj) {

			var targetElement = (document.getElementById('productFinderWrapper')!= undefined) ? "productFinderWrapper" : "pageContent"
			
			this.targetSelected = document.getElementById(targetElement);
			this.targetSelected.innerHTML = '';
			
			_this = this;
			
			for (var i=0;i<this.jsImportTags.length;i++)
			{
				var url = this.jsImportTags[i].match(/src="[\s\S]*?"/g);
				var rawUrl = url[0].replace('src="','').replace('"','');
				
				var importScript = document.createElement('script');
				importScript.type ="text/javascript";
				importScript.src = rawUrl;
				this.targetSelected.appendChild(importScript);
			}
			for (var i=0;i<this.cssImportTags.length;i++)
			{ 
				var url = this.cssImportTags[i].match(/href="[\s\S]*?"/g);
				var rawUrl = url[0].replace('href="','').replace('"','');
				
				var importCss = document.createElement('link');
				importCss.type ="text/css";
				importCss.rel="stylesheet"
				importCss.href = rawUrl;
				this.targetSelected.appendChild(importCss);
			}
			
			if(targetElement=='pageContent'){
				var wrapper = document.createElement('div');
				wrapper.className = 'pageWrapper';
				$(wrapper).append(this.rawImportedData);
				$(loadObj.target).append($(wrapper));
			}else{
				$(loadObj.target).append(this.rawImportedData);
			}

			setTimeout(function() {
				_this.setExternalComplete(loadObj);
			}, 200);
		},
		setExternalComplete : function (loadObj) {
			for (var i=0;i<_this.scriptsImportTags.length;i++)
			{ 
				var scriptsImportTags = document.createElement('script');
				scriptsImportTags.type = 'text/javascript';
				var code = _this.scriptsImportTags[i].replace(/<script[^>]*?[^>]*>([\s\S]*?)/gi,'').replace(/<\/script>/gi,'');
				code = code.replace('$(document).ready(function(){','');
				code = code.replace('});','');
				scriptsImportTags.appendChild(document.createTextNode(code));
				this.targetSelected.appendChild(scriptsImportTags);
			}
			
			$(this.targetSelected).off();
			
			for (var z=0;z<cozaBankingObserver.length;z++)
			{
				var eventsString = '';
				var functionsString = '';
				var targetString = '';
				for (var x=0;x<cozaBankingObserver[z].events.length;x++)
				{ 
					targetString = cozaBankingObserver[z].events[x].target;
					for (var y=0;y<cozaBankingObserver[z].events[x].events.length;y++)
					{ 
						eventsString+=cozaBankingObserver[z].events[x].events[y];
					}
					for (var s=0;s<cozaBankingObserver[z].events[x].funtions.length;s++)
					{ 
						functionsString+=cozaBankingObserver[z].events[x].funtions[s];
					}
				}
				$(this.targetSelected).on(eventsString,targetString, function(event) {new Function(functionsString)});
			}
			$(this.targetSelected).on('click touchend', '.ui-link, .ui-linkRedirect, .subTabContent .ui-subTabLabel, .cellInner-link, .ui-phoneMenu-link, .ui-productLabelItem-link, .ui-productBenefitHeading', function(event) {fnb.functions.cozaContent.select(event,loadObj.target);}).on('click touchend','.ui-submenu-link', function(event) {fnb.functions.cozaContent.selectHref(event,loadObj.target);});
			
			$(this.targetSelected).on('click touchend', '.ui-applyNow', function(event) {pages.fnb.shares.ProductFinder.showPopup();});
		},
		initExternalFunctions : function () {
			clearTimeout(fnb.functions.timeOut.logOffTimer);
			var target="Standard";
			if($(this.getTarget()).closest('#eziWrapper').length>0){target="Ezi";$(_eziProgressWrapperContents).addClass('displayNone')};
			progressBar.init(target);
		},
		setProgressBar : function () {
			clearTimeout(fnb.functions.timeOut.logOffTimer);
			var target="Standard";
			if($(this.getTarget()).closest('#eziWrapper').length>0){target="Ezi";$(_eziProgressWrapperContents).addClass('displayNone')};
			progressBar.init(target);
		},
		clearProgressBar : function () {
			progressBar.clear();
			if($(_eziProgressWrapperContents).hasClass('displayNone'))$(_eziProgressWrapperContents).removeClass('displayNone');
			fnb.functions.timeOut.reset();
		},
		setError : function (errorObject) {
			this.formBeenSubmitted = false;
			fnb.hyperion.utils.error.show(errorObject);
		},
		showFooterButtons : function () {
			if(fnb.hyperion.utils.eziPanel.active==false) fnb.utils.frame.showFooterButtons();
		},
		clearFooterButtons : function () {
			fnb.utils.frame.clearFooterButtons();
		},
		setDefaults : function () {
			if(fnb.hyperion.utils.eziPanel.active==false){fnb.controls.controller.raiseEvent('defaultsShow', '');}
		},
		setEziDefaults : function () {
			fnb.controls.controller.raiseEvent('eziDefaultsShow', '');
		},
		setUrl : function (url) {
			this.url = url;
		},
		getUrl : function () {
			return this.url;
		},
		setTarget : function (target) {
			this.target = target;
		},
		getTarget : function () {
			return this.target;
		},
		setParams : function (params) {
			this.params = params;
		},
		setLogin: function (state) {
			var parent = this;
			parent.loggedIn = state;
		},
		getParams : function () {
			return this.params;
		},
		overrideParam: function(name,val){	
			this.params[name]=val;
		},
		setPreLoadingCallBack : function (preLoadingCallBack) {
			this.preLoadingCallBack = preLoadingCallBack;
		},
		getPreLoadingCallBack : function () {
			return this.preLoadingCallBack;
		},
		setPostLoadingCallBack : function (postLoadingCallBack) {
			this.postLoadingCallBack = postLoadingCallBack;
		},
		getPostLoadingCallBack : function () {
			return this.postLoadingCallBack;
		},
		setQueue : function (queue) {
			this.queue = queue;
		},
		getQueue : function () {
			return this.queue;
		},
		setExpectResponse : function (expectResponse) {
			this.expectResponse = expectResponse;
		},
		getExpectResponse : function () {
			return this.expectResponse;
		},
		setOverrideError: function(overrideError){
			this.overrideError = overrideError;	
		},
		getOverrideError: function() {
			return this.overrideError;	
		},
		setCurrentEventsGroup : function (eventName) {
			this.currentEventsGroup = eventName;
		},
		getCurrentEventsGroup : function () {
			return this.currentEventsGroup;
		},
		clearCurrentEventsGroup : function () {
			this.currentEventsGroup = '';
		},
		setAjaxData : function (ajaxData) {
			this.ajaxData = ajaxData;
		},
		getAjaxData : function () {
			return this.ajaxData;
		},
		setXhr : function (xhr) {
			this.xhr = xhr;
		},
		getXhr : function () {
			return this.xhr;
		},
		doDownload : function (url) {
			window.open(url);
		},
		openWindow : function (url) {
			window.open(url);
		},
		scrollToTop : function () {
			$('html,body').scrollTop(0);
		},
		setTargetElement : function (target) {
			if(document.getElementById(target)!='undefined') this.targetElement = target;
		},
		getTargetElement : function () {
			return this.targetElement;
		},
		setBodyHeight : function () {
			if(fnb.hyperion.utils.eziPanel.active==false) fnb.hyperion.controller.bodyElement.attr('data-clipoverflow', 'false');
		},
		clearBodyHeight : function () {
			fnb.hyperion.controller.bodyElement.attr('data-clipoverflow', 'true');
		},
		setOverlay : function (target) {
			fnb.utils.overlay.show();
			this.clearBodyHeight();
		},
		clearOverlay : function () {
			this.setBodyHeight();
			if(!fnb.hyperion.utils.eziPanel.active){
				fnb.utils.overlay.hide();
			}
		},
		clearActiveElements : function () {
			fnb.forms.dropdown.close();
			//New frame hide
			fnb.hyperion.utils.error.hide();
			//fnb.utils.errorPanel.hide();
			//fnb.functions.bigThree.initialized = false;
			fnb.forms.tooltip.hide();
		},
		submitForm : function (formName, targetDiv, preLoadingCallBack,postLoadingCallBack, buttonTarget, extraOptions, preventDefaults) {

			if(!this.formBeenSubmitted){
				 
				this.formBeenSubmitted = true;
				
				var keepFooter = (extraOptions) ? (extraOptions.keepFooter) ? true : false : false;
				
				fnb.utils.params.getParams(formName, targetDiv, buttonTarget, extraOptions);
				
				var loadObj = {url: fnb.controls.controller.getUrl(),target:targetDiv,preLoadingCallBack:preLoadingCallBack,postLoadingCallBack:postLoadingCallBack,params:fnb.controls.controller.getParams(),formName:formName,preventDefaults:preventDefaults,extraOptions:extraOptions};
				var formTarget = $("form[name='"+formName+"']").attr("target");
				
				if(formTarget=="frameResponse"){
					fnb.controls.controller.eventsObject.raiseEvent('loadFormToFrame', loadObj);
				}else{
					if(keepFooter==true){
						fnb.controls.controller.eventsObject.raiseEvent('simpleLoadUrlKeepFooter', loadObj)
					}else if(loadObj.preventDefaults==true){
						fnb.controls.controller.eventsObject.raiseEvent('simpleLoadUrl', loadObj);	
					}else{
						fnb.controls.controller.eventsObject.raiseEvent('loadUrl', loadObj);	
					}
				}
			}
			
		},
		submitFormToEzi : function (formName, paramName, paramValue, targetDiv, preLoadingCallBack,postLoadingCallBack, buttonTarget, extraOptions, preventDefaults) {
			if(!this.formBeenSubmitted){
				fnb.utils.params.getParams(formName, targetDiv, buttonTarget, extraOptions);
				var loadObj = {url: fnb.controls.controller.getUrl(),target:targetDiv,preLoadingCallBack:preLoadingCallBack,postLoadingCallBack:postLoadingCallBack,params:fnb.controls.controller.getParams(),formName:formName,preventDefaults:preventDefaults};
				if(typeof paramName!='undefined') this.overrideParam(paramName,paramValue);
				this.formBeenSubmitted = true;
				fnb.controls.controller.eventsObject.raiseEvent('submitFormToEzi', loadObj);	
			}
		},
		submitFormToUrl : function (formName) {
			
		},
		eziSubmitForm : function (formName, targetDiv, preLoadingCallBack,postLoadingCallBack, buttonTarget, extraOptions, preventDefaults) {
			if(!this.formBeenSubmitted){
				this.formBeenSubmitted = true;
				fnb.utils.params.getParams(formName, targetDiv, buttonTarget, extraOptions);
				var loadObj = {url: fnb.controls.controller.getUrl(),target:targetDiv,preLoadingCallBack:preLoadingCallBack,postLoadingCallBack:postLoadingCallBack,params:fnb.controls.controller.getParams(),formName:formName,preventDefaults:preventDefaults};
				fnb.controls.controller.eventsObject.raiseEvent('eziSliderPaging', loadObj);
			}
		},
		clearSubTabObject : function () {
			if(fnb.utils.mobile.subtabs.subTabTopScroller)  fnb.utils.mobile.subtabs.subTabTopScroller = undefined;
		},
		attachEvent : function (sender,eventArgs) {
			this.eventsObject.attachEvent(sender,eventArgs);
		},
		detachEvent : function (sender,eventArgs) {
			this.eventsObject.detachEvent(sender,eventArgs);
		},
		raiseEvent : function (sender,val,navError) {
			if(fnb.hyperion.utils.eziPanel.active==false&&navError!=true||fnb.hyperion.utils.eziPanel.active==true&&navError!=true) this.eventsObject.raiseEvent(sender,val);
		},
		getObject: function(objName, create){
			if (!this.activeObjectsList[objName]){
				if (!create) {
					return null;
				}
				this.activeObjectsList[objName] = [];
			}
			return this.activeObjectsList[objName];
		},
		registerObj : function (objName, object) {
			var obj = this.getObject(objName, true);
			obj.push(object);
		},
		deRegisterObj : function (objName, object) {
			var obj = this.getObject(eventName);
			if (!obj) { return; }
			var getArrayIndex = function(array, item){
				for (var i = array.length; i < array.length; i++) {
					if (array[i] && array[i] === item) {
						return i;
					}
				}
				return -1;
			};
			var index = getArrayIndex(obj, handler);
			if (index > -1) {
				obj.splice(index, 1);
			}
		},
		createControllerObjects : function () {
			namespace("fnb.utils.objectsInitializer", new fnb.utils.objectsInitializer());
			this.registerObj('fnb.utils.objectsInitializer',fnb.utils.objectsInitializer);
			fnb.utils.objectsInitializer.init();
		},
		attachTemplates : function () {
			this.eventsObject.eventList = eventTemplates;
		},
		setSkin : function () {
			_skin = $("#workspace").find(".pageWrapper").attr('data-skin');
		},
		inspectDomFunctions : function() {
			_this = this;
			if (this.pendingJsSearch == false) {
				_this.pendingJsSearch = true;
				var functionsString = "";
				setTimeout(
						function() {
							var count = 0;
							for ( var x in window) {
								try{
									if (typeof window[x] === 'function'
											&& window[x].toString().indexOf('[native code]') == -1
											&& window[x].toString().search(/fnb./i) == -1
											&& $.inArray(x.toString(),functionsArray) == -1) {
										count++;
										console.log('Non namespaced function found=>> '+x.toString())
										functionsString += '&' + x.toString();
										//delete window[x];
									}
								}catch(e){
									console.log(e)
									console.log(window[x].toString())
								}
								
							}
							if (count > 0) {
								//$.post("/banking/Controller", {
								//	"nav" : "support.security.MWD",
								//	"functions" : functionsString
								//});
							}
							_this.pendingJsSearch = false;
						}, 5000);
			}
		}
	};
	namespace("fnb.controls.controller",controllerObject);
});
