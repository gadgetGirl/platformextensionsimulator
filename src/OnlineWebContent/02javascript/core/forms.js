///-------------------------------------------///

/// developer: Donovan
///
/// Inputs
///-------------------------------------------///
$(function(){
	function input(){
		this.undeletableChars = ['(',')'];
		this.currentInputVal = '';
		this.firstFocus = false;
	};
	input.prototype = {
		init: function(){
			this.ctrlDown = false;
		},
		delegateEvents: function(event){
			_this = this;
			var eventName = event.type;
			var isNumberOnly = $(event.target).hasClass('numberOnly');
			var isNumber = $(event.target).hasClass('number');
			var isPhoneNumber = $(event.target).hasClass('phoneNumber');
			var isCurrency = $(event.target).hasClass('currencyInput');
			var isTotaller = $(event.target).parent().hasClass('tableAmountInputItem');
			if(isCurrency){
				switch(eventName)
				{
					case 'focusin':
						_this.focusEvent(event);
					break;
					case 'keypress':
						return _this.currencyKeypress(event);
					break;
					case  'keyup':
						if(isTotaller){
							var validInput = _this.currencyKeypressUp(event);
							if(validInput) _this.updateTotaller();
							return validInput;
						}else{
							return _this.currencyKeypressUp(event);
						}
					break;
					case  'focusout':
						_this.currencyBlurEvent(event);
					break;
				}
			}else if(isNumberOnly&&!isPhoneNumber){
				switch(eventName)
				{
					case 'focusin':
						_this.addFocus(event.target);
					break;
					case  'focusout':
						_this.removeFocus(event.target);
					break;
					case  'keydown':
						return _this.numberOnlyInput(event);
					break;
				}

			}else if(isNumber){
				switch(eventName)
				{
					case 'focusin':
						_this.addFocus(event.target);
					break;
					case  'focusout':
						_this.removeFocus(event.target);
					break;
					case  'keydown':
						return _this.numberInput(event);
					break;
				}

			}else if(isPhoneNumber){
				switch(eventName)
				{
					case 'focusin':
						_this.phoneFocusIn(event);
					break;
					case  'focusout':
						_this.phoneFocusOut(event);
					break;
 					case  'keyup':
						return _this.phoneKeypressUp(event);
					break;
					case  'keydown':
						return _this.phoneKeypress(event);
					break;
				}
			}else{
				switch(eventName)
				{
					case 'focusin':
						_this.addFocus(event.target);
					break;
					case  'focusout':
						_this.removeFocus(event.target);
					break;
				}
			}
		},
		addFocus: function(target){
			if(this.firstFocus==false){
				this.firstFocus == true;
				$(target).addClass('input-focus');
			}
			fnb.forms.scrollUtil.tabScroll(target);
		},
		removeFocus: function(target){
			this.firstFocus == false;
			$(target).removeClass('input-focus');
		},
		focusEvent: function(e){
			if(this.firstFocus==false){
				_this.addFocus(e.target);
				this.currentInputVal = $(e.target).val();
				this.firstFocus == true;
				if($(e.target).val() == "0.00") {
					$(e.target).val("");
				}
			}
		},
		currencyKeypress: function(e){
			var key = e.charCode||e.keyCode||e.which;
			if (key == undefined) return false; 
			if (key<48||key>57) {
				if (key==13||key==9||key==8||key==37||key==39||key==46) {
					return true;
				} else {
					return false;
				}
			}else{
				return true;
			}
		},
		currencyKeypressUp: function(e){
			var e = e;
			var keyUnicode = e.charCode || e.keyCode;
			if (e !== undefined) {
				switch (keyUnicode) {
					case 8: return true; break;
					case 46: return true; break;
					case 16: return false; break;
					case 17: return false; break;
					case 18: return false; break;
					case 27: e.target.value = ''; return false; break;
					case 35: return false; break;
					case 36: return false; break;
					case 37: return false; break;
					case 38: return false; break;
					case 39: return false; break;
					case 40: return false; break;
					case 78:  return false; break;
					case 110: return false; break;
					case 190: return false; break;
					default:

					var cursorPosition = $(e.target).getCursorPosition();
					var originalVal =  $(e.target).val();
					var currentVal = e.target.value.replace(/ /g,"");
					
					var nextChar = currentVal.charAt(cursorPosition);
					var offsetCount = currentVal.match(/ /ig) || [];

					if(!jQuery.browser.mobile)
					{
					$(e.target).maskCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1, eventOnDecimalsEntered: true });
					}

					var maskedVal = $(e.target).val();
					var offsetPos = maskedVal.indexOf(" ");
					var newOffsetCount = maskedVal.match(/ /ig) || [];
					
					
					if (maskedVal.length > originalVal.length)
					{cursorPosition++;}
					
					if (currentVal.indexOf(".") == 0)
					{cursorPosition++;}
					
					$(e.target).setCursorPosition(cursorPosition);
					return true;
				}
			}else{
				return false;
			}
		},
		currencyBlurEvent: function(e){
			this.firstFocus == false;
			_this.removeFocus(e.target);
			var currentcyDecimal = parseInt($(e.target).attr('data-decimal'));
			if (e.target.value == "") {
				$(e.target).val('0.00');
			}
			$(e.target).maskCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: currentcyDecimal });
		},
		numberInput: function(event){
			if(event.keyCode >= 48 && event.keyCode <= 57||event.keyCode >= 96 && event.keyCode <= 105||event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9|| event.keyCode == 37|| event.keyCode == 39)				
			{
				return true;
			}else{
				return false;	
			}
		},
		numberOnlyInput: function(event){
			var keyUnicode = event.which || event.keyCode;
			var isShift = window.event.shiftKey ? true : false;
			if(isShift){
				return false;
			}else{
				if(keyUnicode == 8 || keyUnicode == 9 || keyUnicode == 37 ||keyUnicode == 39 || keyUnicode == 89 || keyUnicode == 96 || keyUnicode == 97 || keyUnicode == 98 || keyUnicode == 99 || keyUnicode == 100 || keyUnicode == 101 || keyUnicode == 102 || keyUnicode == 103 || keyUnicode == 104 || keyUnicode == 105 ||  keyUnicode == 110 || keyUnicode == 111 || keyUnicode == 66 || keyUnicode == 67 || keyUnicode == 78 || keyUnicode == 79 || keyUnicode == 46 || keyUnicode == 48 || keyUnicode == 49 || keyUnicode == 50 || keyUnicode == 51 || keyUnicode == 52 || keyUnicode == 53|| keyUnicode == 54|| keyUnicode == 55|| keyUnicode == 56|| keyUnicode == 57){
					return true;
				} else {
					return false;
			    }
			}
			
		},
		phoneFocusIn: function(event){
			_this.addFocus(event.target);
			if($(event.target).hasClass('phoneCode')){
				if($(event.target).hasClass('phoneEmpty')){
					$(event.target).val('')
					$(event.target).removeClass('phoneEmpty');
				}
			}else if($(event.target).hasClass('phoneNumber')){
				if($(event.target).hasClass('phoneEmpty')){
					$(event.target).val('')
					$(event.target).removeClass('phoneEmpty');
				}
			}
		},
		phoneFocusOut: function(event){
			_this.removeFocus(event.target);
			if($(event.target).val()==''&&$(event.target).hasClass('phoneCode')){
				if(!$(event.target).hasClass('phoneEmpty')){
					$(event.target).val('Code')
					$(event.target).addClass('phoneEmpty');
				}
			}else if($(event.target).val()==''&&$(event.target).hasClass('phoneNumber')){
				if(!$(event.target).hasClass('phoneEmpty')){
					$(event.target).val('Number');
					$(event.target).addClass('phoneEmpty');
				}
			}
		},
		phoneKeypress: function(event){
			var keyUnicode = event.charCode || event.keyCode;
			if(keyUnicode==17){
				this.ctrlDown = true;
				return true;
			}else{
				if(this.ctrlDown==true&&keyUnicode==86||this.ctrlDown==true&&keyUnicode==67||this.ctrlDown==true&&keyUnicode==88){
					return true;
				}else{
					return _this.numberOnlyInput(event);
					if($(event.target).val()==''&&$(event.target).hasClass('phoneCode')){
						if(!$(event.target).hasClass('phoneEmpty')){
							$(event.target).val('Code')
							$(event.target).addClass('phoneEmpty');
						}
					}else if($(event.target).val()==''&&$(event.target).hasClass('phoneNumber')){
						if(!$(event.target).hasClass('phoneEmpty')){
							$(event.target).val('Number');
							$(event.target).addClass('phoneEmpty');
						}
					}
				}
			}

		},
		phoneKeypressUp: function(event){
			this.ctrlDown = false;
		},
		setInputCursorPosition: function(input, pos){
			if (_browserName=="MSIE"&&input.createTextRange) {
		        var range = input.createTextRange();
		        range.collapse(true);
		        range.moveEnd('character', pos);
		        range.moveStart('character', pos);
		        range.select();
		    }
			else {
				input.selectionStart = pos;
				input.selectionEnd = pos;
			}
			return true;
		},
		getInputCursorPosition: function(input){
			var inputPos = 0;
			if (document.selection) {
				input.focus ();
				var selection = document.selection.createRange ();
				selection.moveStart ('character', -input.value.length);
				inputPos = selection.text.length;
			}else if (input.selectionStart || input.selectionStart == '0')
				inputPos = input.selectionStart;
				return (inputPos);
		},
		getInputSelectedText: function(el){
			 var start = 0, end = 0, normalizedValue, range,
				textInputRange, len, endRange;

			if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
				start = el.selectionStart;
				end = el.selectionEnd;
			} else {
				range = document.selection.createRange();

				if (range && range.parentElement() == el) {
					len = el.value.length;
					normalizedValue = el.value.replace(/\r\n/g, "\n");

					textInputRange = el.createTextRange();
					textInputRange.moveToBookmark(range.getBookmark());

					endRange = el.createTextRange();
					endRange.collapse(false);

					if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
						start = end = len;
					} else {
						start = -textInputRange.moveStart("character", -len);
						start += normalizedValue.slice(0, start).split("\n").length - 1;

						if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
							end = len;
						} else {
							end = -textInputRange.moveEnd("character", -len);
							end += normalizedValue.slice(0, end).split("\n").length - 1;
						}
					}
				}
			}

			return {
				start: start,
				end: end
			};
		},
		updateTotaller: function(){
			currentPageTotal = 0;
			$.each( currentPageTotalItems, function( index, item ) {
				var currentVal = $('#'+item).val().replace(/ /g,'');
				if(currentVal=='') currentVal = 0;
				currentPageTotal += parseFloat(currentVal);
			});
			var tableTotalFloat = tableTotal+currentPageTotal;
			if(tableTotalFloat>0){
				$('#footerMessageTotal').text(tableTotalFloat.toFixed(2));
				if($('#footerMessage').hasClass('hideElement')) $('#footerMessage').removeClass('hideElement');
			}else{
				if(!$('#footerMessage').hasClass('hideElement')) $('#footerMessage').addClass('hideElement');
			}
		}
	};

	namespace("fnb.forms.input",input);

});

///----------------------------///
/// first developer: Donovan
/// second developer: Leon
/// developer: Vaughan (12/06/2013) - added enable/disable/setValue methods + onChangeCallback var
///
/// DROPDOWNS
///----------------------------///
$(function(){
	function dropdown(){
		var reverse = false;
		var dropUp = false;
		this.onclick;
		this.currentChild;
		this.scrollPos;
		this.didAdjust = false;
		this.didAdjustPos = 0;
		this.onChangeCallback;
		this.tableRow;
	};
	dropdown.prototype = {
		init: function(obj,index){
			this.reverse = false;
			this.dropUp = false;
			var contentObject = $(obj).parent().find('ul[class^="dropdown-content"]');
			index = ( (index == undefined) || (index == '') && (index != 0) ) ? 'dropdownIndexUndefined' : index;
			if ( index == 'dropdownIndexUndefined' ) {
				index = $(contentObject).index($('li.selected-item'));
			} else {
				var foundIndex = index;
				if(index != 'dropdownIndexUndefined'){
					$(contentObject).find('li').each(function(itemIndex, item) {
						if($(this).attr('data-value')  == index) foundIndex = itemIndex;
					});
				}
				
				var dropdownDownSelection = (index != 'dropdownIndexUndefined') ? $(contentObject).find('li:eq('+foundIndex+')') : [];
				if ($(dropdownDownSelection).length == 0) {
					var arrItems = $(contentObject).children();
					$.each(arrItems,function(i,n){
						var foundIndex = $("div:contains('" + foundIndex + "')",n);
						if (foundIndex.length != 0) {
							foundIndex = i-1;
						}
					});
				}
			}
			if($(contentObject).find('li').length<5) $(contentObject).find('.dropdown-content-top').remove();
			if(foundIndex=='') foundIndex = 0;
			if($(contentObject).find('li:eq('+foundIndex+')').is('*')){
				var dropdownDownSelection = $(contentObject).find('li:eq('+foundIndex+')');
				this.select($(dropdownDownSelection));
				var labelWidth = $(contentObject).attr('data-labelwidth');
				var selectWidth = $(contentObject).attr('data-selectwidth');
			}else if (foundIndex == 0){
				$(obj).find('.dropdown-selection-white').text('No Data');
			}
			else{
				$(obj).find('.dropdown-selection-white').text('Please Select');
			}
		},
		expand: function(obj,type,searchClassName,reverse,height,isAccount){
			if(!$(obj).closest('.formElementWrapper').hasClass('dropdown-disabled')){
				if(_tinyPort){
					this.reverse = false;
				}else{
					this.reverse = reverse;
				}
				var parentFunction = this;
				var enableSearch = false;
				var contentObject = $(obj).parent().parent().find('div[class*="dropdown-content-wrapper"]');
				
				$(contentObject).find('.dropdown-input').val('');
				
				$(contentObject).css({width: $(contentObject).parent().find('.dropdown-initiator').width()})
				$(contentObject).find('.dropdown-item').show();
				
				if(!$(contentObject).hasClass('dropdown-expanded')){
					if(_tinyPort){
						this.tableRow = $(obj).closest('.tableRow');
						if(this.tableRow.length>0){
							this.tableRow.addClass('addStatic');
						}
					}
					if(_tinyPort) fnb.controls.controller.clearBodyHeight();
					fnb.controls.controller.clearActiveElements();
					if(type==2){
						$(contentObject).children('ul:eq(0)').addClass('dropdown-static');
					}
					$(contentObject).addClass("dropdown-expanded");
					$(contentObject).parent().parent().addClass("dropdown-wrapper-expanded");
					parentFunction.animate($(contentObject).children('ul:eq(0)'),false,height,this.reverse);
				}else{
					if(_smallPort&&!$('#mobiPopupWrapper').is('*')){
						fnb.controls.controller.setBodyHeight();
					}
					$(contentObject).removeClass("dropdown-expanded");
					parentFunction.animate($(contentObject).children('ul:eq(0)'),true,height,this.reverse);
				}
			}
		},
		animate: function(obj,state,height,reverse){
			$(obj).show();
			
			/*if(!_isMobile)$('.pageGroup ').addClass('superPadBottom');*/
			$(obj).parent().show();
			$(obj).parent().css({'top':''});

			var parentFunction = this;
			var objHeight = $(obj).height();

			var isMobiWrapped = $(obj).closest('#mobiPopupWrapper').length;
			var isFooterWrapped = $(obj).closest('#formFooterButtons').length;
			
			height = 0;
			var fixHeight = false;
			if($(obj).find('li').length>5){
				fixHeight = true;
				height = Math.round((objHeight/$(obj).find('li').length)*6);
			}

			if(height>300||height==0) height= 300;
			if(fixHeight == true) objHeight = height;

			if(isMobiWrapped>0) reverse='false';

			if(($(_main).outerHeight(true))>$(_main).outerHeight(true)&&isMobiWrapped<1&&isFooterWrapped<1){
				reverse='true';
				if($(window).height()-179>($(_main).height()+objHeight)){
					reverse='false';
				}else{
					if($(obj).parent().position().top<objHeight){
						objHeight = ($(obj).parent().position().top)-($(obj).closest('.dropdownElementWrapper').height())-12;
					}
				}
			}
			
			if(reverse=='true'){
				var posTop = $(obj).parent().parent().position().top - objHeight;
				if($(obj).closest('.pagination').length == 0) posTop-=12;
				$(obj).parent().css({'top':posTop+'px'});
				parentFunction.dropUp = true;
			}else{
				if(!_tinyPort&&fnb.hyperion.utils.eziPanel.active == false){
					if(state==false){
						var currentScrollTop = $(document).scrollTop();
						this.didAdjustPos = currentScrollTop;
						var currentWindowHeight = $(window).height();
						var calc = ($(obj).parent().position().top-currentScrollTop)+objHeight+$(_topMenu).height()+$(_formFooterButtons).height();
						if(calc>currentWindowHeight){
							this.didAdjust = true;
							var newScrollPos = (calc-currentWindowHeight)+(currentScrollTop+48);
							fnb.functions.isScrolling.doScroll(newScrollPos);
						}
					}else if(this.didAdjust==true){
						fnb.functions.isScrolling.doScroll(this.didAdjustPos);
					}
				}else if(_tinyPort){
					objHeight = $(window).height()-30;
				}
			}
			if(state == false){
				if(_smallPort){
					if($('#main').height()<$(window).height())  $('#main').css({'min-height': $(window).height()+'px'});
					this.scrollPos = $(window).scrollTop();
				}
				if(parentFunction.dropUp == true){
					$(obj).css({'max-height':objHeight}).show();
					$(obj).parent().parent().find('.dropdown-trigger').addClass('dropup-trigger-selected');
				}else{
					$(obj).show().css({
							'max-height': 0
						}).stop().animate({
							'max-height':objHeight
						}, {
							duration: 200, 
							easing: 'swing',
							step: function( progress ){

							},
							complete: function() {
								$(obj).parent().parent().find('.dropdown-trigger').addClass('dropdown-trigger-selected');
							}
					});
				}
			}else if(state == true){
				if(_smallPort){
					window.scrollTo(0, this.scrollPos);
				}
				if(parentFunction.dropUp == true){
					$(obj).parent().parent().find('.dropdown-trigger').removeClass('dropup-trigger-selected');
					$(obj).css({'max-height':''})
					$(obj).hide();
					$(obj).parent().hide();
				}else{
					$('#main').height('');
					$(obj).animate({
						'max-height':0
					}, {
						duration: 200, 
						easing: 'swing',
						step: function( progress ){

						},
						complete: function() {
							$(obj).parent().parent().find('.dropdown-trigger').removeClass('dropdown-trigger-selected');
							$(obj).hide();
							$(obj).parent().hide();
							$(obj).css({'max-height':''})
						}
					});
				}
			}
		}, 
		mobileTriggerOnclick: function(obj){
			$(obj).trigger('onclick');
		},
		checkSelect: function(e,onclick){
			var _this = this;
			if(_isMobile){
				if(fnb.utils.mobile.utils.mobileMoved == false||typeof fnb.utils.mobile.utils.mobileMoved=='undefined'){
					var newObj = $(e.target);
					if(typeof $(newObj).attr('onclick')!='undefined'){
					}else if(typeof $(newObj).parent().attr('onclick')!='undefined'){
						newObj = $(newObj).parent();
					}else if(typeof $(newObj).parent().parent().attr('onclick')!='undefined'){
						newObj = $(newObj).parent().parent();
					}
					_this.select(newObj,onclick);
					if(_tinyPort){if(_this.tableRow.length>0) _this.tableRow.removeClass('addStatic')};
				}
			}else{
				_this.select($(e.target),onclick);
				if(_tinyPort){if(_this.tableRow.length>0) _this.tableRow.removeClass('addStatic')};
			}
		},
		checkExpand: function(e){
			if(fnb.utils.mobile.utils.mobileMoved == false||typeof fnb.utils.mobile.utils.mobileMoved=='undefined'){
				var newObj = $(e);
				if(typeof $(newObj).attr('onclick')=='undefined'){
					newObj = $(newObj).closest('.dropdown-initiator');
				}
				newObj.trigger('onclick')
			}
		},
		select: function(obj,onclick){
			if(_smallPort&&!$('#mobiPopupWrapper').is('*')) fnb.controls.controller.setBodyHeight();
			var dropDownWrapper = $(obj).closest('.dropdown-wrapper');
			if(dropDownWrapper.length==0) dropDownWrapper = $(obj).closest('.dropdownElementWrapper');
			var isAccount = dropDownWrapper.attr('data-value');
			var objValue = $(obj).attr('data-value');
			
			if(typeof(objValue) == 'undefined'){
				obj = $(obj).closest('li');
				objValue = $(obj).attr('data-value');
			}
			
			var dropdownInput = $(dropDownWrapper).find('.dropdown-hidden-input');
			if(dropdownInput.length==0) dropdownInput = $(dropDownWrapper).parent().find('.dropdown-hidden-input');

			$(dropdownInput).val(objValue);
			$(obj).parent().find('li').removeClass("selected-item");
			$(obj).addClass("selected-item");

			var selectedObjectContents = $(obj).html();
			var clonedSelection = $(selectedObjectContents).clone();
			var destination;
			
			
			var downloadValue = $(dropDownWrapper).parent().find('.formElementLabel').find('input');
		
			if(downloadValue != undefined) downloadValue.val($(clonedSelection).find('.dropdown-item-row').html());
			
			destination =  $(dropDownWrapper).find('div[class^="dropdown-selection-white"]');
			$(destination).contents().remove();
			$(clonedSelection).appendTo($(destination));
			if(typeof(isAccount)!='undefined'&&$(obj).find('input').is('*')){
				var controllerUrl = $(obj).find('input').val();
				var targetDiv =$(destination).find('div[class^="ammountsHolder"]');
				if(typeof controllerUrl!='undefined'||objValue!='-1'){
					var loadObj = {url: controllerUrl, target: targetDiv,queue:false};
					fnb.controls.controller.eventsObject.raiseEvent('dropdownLoadAmounts', loadObj)
				}
			}
			if($(obj).parent().parent().hasClass('dropdown-expanded')){
				$(obj).parent().parent().removeClass("dropdown-expanded");
				$(dropDownWrapper).removeClass("dropdown-wrapper-expanded");
				this.animate($(obj).parent(),true);
			}

			if(_isMobile&&onclick) fnb.forms.dropdown.mobileTriggerOnclick(obj);
		},
		keypress: function(obj){
			var parentObject = $(obj).parent().parent().parent().parent();
			$(parentObject).parent().css({'margin-top':''});
			var inputValue = $(obj).val().toLowerCase();
			$(parentObject).find('li[class*="dropdown"]').show();
			if(inputValue.length>0){
				$(parentObject).find('li[class*="dropdown"]').hide();
				var filter = "> li div[class*='dropdown-h']:icontains('" + inputValue + "')";
				$(filter, parentObject).parent().parent().show();
			}
			if(_tinyPort==false){
				var newParentHeight;
				if(fnb.forms.dropdown.reverse){
					newParentHeight = $(parentObject).parent().outerHeight(true);
					var dropdownNewPos = $(parentObject).parent().position().top;
					var dropdownNewMar = (Math.abs(dropdownNewPos)-newParentHeight)+4;
					$(parentObject).parent().css({'margin-top':dropdownNewMar});
				}else if(fnb.forms.dropdown.dropUp==true){
					newParentHeight = $(parentObject).parent().find('ul[class*="dropdown-content"]').height();
				}
			}
		}, 
		close: function(){
			if($('.dropdown-content-wrapper').hasClass('dropdown-expanded')){
				if(_smallPort&&!$('#mobiPopupWrapper').is('*')) fnb.controls.controller.setBodyHeight();
				this.animate($('.dropdown-expanded').find('.dropdown-content'),true);
				$('.dropdown-content-wrapper').removeClass("dropdown-expanded");
				$('.pageGroup ').removeClass('superPadBottom');
			}
		},
		focusIn: function(event){
			$(event.target).parent().find('.dropdown-selection-white').addClass('dropdown-focus');
			$(event.target).addClass('dropdown-has-focus');
			fnb.forms.scrollUtil.tabScroll(event.target);
		},
		focusOut: function(event){
			$(event.target).parent().find('.dropdown-selection-white').removeClass('dropdown-focus');
			$(event.target).removeClass('dropdown-has-focus');
		},
		focusKeyup: function(event){
			if (event.keyCode == 40) { 
				if(!$(event.target).parent().find('.dropdown-content-wrapper').hasClass('dropdown-expanded')){
					var initiator = $(event.target).parent().find('.dropdown-initiator');
					var getAttrValue = function(attributeName) {
						var a = $(initiator).attr(attributeName);
						$(initiator).removeAttr(attributeName);
						return a;
					}
					var attrOverflow = getAttrValue('data-disabled-overflow');
					var attrSearchClass = getAttrValue('data-disabled-searchclass');
					var attrReverse = getAttrValue('data-disabled-reverse');
					var attrHeight = getAttrValue('data-disabled-height');
					fnb.forms.dropdown.expand($(initiator),attrOverflow,attrSearchClass,attrReverse,attrHeight);
				}else{
					if(!this.currentChild){
						this.currentChild = $(event.target).parent().find('.selected-item').next("li");
					}else{
						this.currentChild = this.currentChild.next("li")
					}
					if(this.currentChild.length == 0) $(event.target).parent().find("li").eq(0);
					$(event.target).parent().find("li").removeClass('selected-item');
					this.currentChild = this.currentChild.addClass('selected-item')
				}
			}else if(event.keyCode == 38 ){
				if($(event.target).parent().find('.dropdown-content-wrapper').hasClass('dropdown-expanded')){
					if(!this.currentChild){
						this.currentChild = $(event.target).parent().find('.selected-item').prev("li");
					}else{
						this.currentChild = this.currentChild.prev("li")
					}
					if(this.currentChild.length == 0) $(event.target).parent().find("li").last();
					$(event.target).parent().find("li").removeClass('selected-item');
					this.currentChild = this.currentChild.addClass('selected-item')
				}
			}if(event.keyCode == 13 ){
				if($(event.target).parent().find('.dropdown-content-wrapper').hasClass('dropdown-expanded')){
					this.currentChild.click();
				}
			}
		},		
		adjust: function(){
			$('body').find('.dropdown-expanded').css({width: $('body').find('.dropdown-expanded').parent().find('.dropdown-initiator').width()})
		},
		
		setValue: function(obj, visualVal, actualVal){
			var wrapper = obj.closest('.formElementWrapper');
			
			wrapper.find('.dropdown-initiator').find('.dropdown-item-row').text(visualVal);
			
			wrapper.find('.dropdown-hidden-input').attr('value', actualVal);
			
			wrapper.find('.dropdown-item').removeClass("selected-item");
			
			wrapper.find('.dropdown-item[data-value="' + actualVal + '"]').addClass('selected-item');
		},
		enable: function(root){
			//if not root
			if(!root.hasClass('dropdownElementWrapper')) root = root.closest('.formElementWrapper');
			
			//I dont agree that the "row" wrapper should be disabled (incl. label) as only the graphic is being disabled
			root.removeClass('dropdown-disabled');
			
			var wrapper = root.find('.dropdownElementWrapper');
			
			wrapper.removeClass('disabled');
		},
		disable: function(root){
			if(!root.hasClass('dropdownElementWrapper')) root = root.closest('.formElementWrapper');
			
			//I dont agree that the "row" wrapper should be disabled (incl. label) as only the graphic is being disabled
			root.addClass('dropdown-disabled');
			
			var wrapper = root.find('.dropdownElementWrapper');
			
			wrapper.addClass('disabled');
		}
	};
	namespace("fnb.forms.dropdown",dropdown);
});
///-------------------------------///
/// developer: Mike
/// File Upload
///-------------------------------///

$(function(){
	function fileUpload(){
	
	};
	fileUpload.prototype = {
		buttonClick: function(event) {
			var id  = $(event.target).attr('id').split('-')[0];	
			$('#'+id).trigger('click');
		},
		inputClick: function(event) {
		
			var id  = $(event.target).attr('id').split('-')[0];
			$('#'+id).trigger('click');

		},
		inputChanged: function(event){
		
			var id = $(event.target).attr('id')
			$('#'+ id +'-fake-input' ).val($(event.target).val())
			
		}
	}
	namespace("fnb.forms.fileUpload",fileUpload);		
});
///-------------------------------///
/// developer: Gerhard
/// reworked: Donovan - Needs to be rebuild
/// developer: Vaughan - 25/06/2013 (added button group wrapper disabled handling)
/// Radio Buttons
///-------------------------------///
$(function(){
	function radioButtons(){
		this._radioInput;
		this._radioInputParent;
		this._radioInputValue;
	};
	radioButtons.prototype = {
		radioAction: function(radioButton){
			this._radioInput = $(radioButton).find("input");
			this._radioInputValue = $(radioButton).attr('data-value');
    		this._radioInputParent = $(radioButton).parent().parent().children();
    		
    		if (!$(radioButton).parent().hasClass("disabled") && !$(radioButton).closest('.radioButtonsGroup').hasClass("disabled")){
    			for(i = 0; i < this._radioInputParent.length; i ++){
        			$(this._radioInputParent[i]).removeClass("switcherWrapperSelected");
        		}
        		 
        		$(radioButton).parent().addClass("switcherWrapperSelected");
        		$(this._radioInputParent).attr("value",this._radioInputValue);
        		$(this._radioInputParent).trigger("change");
    		}
    		
		},
		focusIn: function(event){
			$(event.target).parent().addClass('radio-has-focus');
			$(event.target).parent().find(".switcherWrapper").eq(0).addClass('radio-focus');
			fnb.forms.scrollUtil.tabScroll(event.target);
		},
		focusOut: function(event){
			$(event.target).parent().removeClass('radio-has-focus');
			$(event.target).parent().find(".switcherWrapper").removeClass('radio-focus');
		},
		focusKeyup: function(event){
			if (event.keyCode == 39) {
				var focusedChild = $(event.target).parent().find('.radio-focus');
				$(focusedChild).removeClass('radio-focus');
				var nextChild = $(focusedChild).next("div");
				if(nextChild.length==0) nextChild = $(event.target).parent().find(".switcherWrapper").eq(0);
				this.radioAction(nextChild.find(">:first-child"));
				nextChild.find(">:first-child").click();
				$(nextChild).addClass('radio-focus');
			}else if(event.keyCode == 37){
				var focusedChild = $(event.target).parent().find('.radio-focus');
				$(focusedChild).removeClass('radio-focus');
				var prevChild = $(focusedChild).prev("div");
				if(prevChild.length==0) prevChild = $(event.target).parent().find(".switcherWrapper").last();
				this.radioAction(prevChild.find(">:first-child"));
				prevChild.find(">:first-child").click();
				$(prevChild).addClass('radio-focus');
			}
		}
	};
	namespace("fnb.forms.radioButtons",radioButtons);
});

///-------------------------------///
/// developer: Leon
/// developer: Vaughan - 24/6/2013 (modified toggleAll to fire checkbox onclick function calls)
///
/// Rebuilt/reworked Checkboxes
///-------------------------------///

$(function(){
	function checkBox(){
		this.isToggling = false;
	};
	checkBox.prototype = {
		checkboxClick: function(me){
			if(!$(me).closest('.formElementWrapper').hasClass('pending')){
				if( $(me).parent().hasClass('checked')){
					$(me).setCheckbox(false)	// UNCHECK
				}else{
					$(me).setCheckbox(true);
				}					// CHECK
			}
		},
		checkboxTouch: function(me){
			if(!$(me).closest('.formElementWrapper').hasClass('pending')||$(me).closest('.formElementWrapper')==-1){
				if($(me).parent().hasClass('checked') ){
					$(me).setCheckbox(false)	// UNCHECK
				}else{
					$(me).setCheckbox(true);	
				}// CHECK
				if($(me).attr('onclick')!=undefined&&_isMobile){
					$(me).trigger('onclick');
				}
			}
		},
		toggleAll:function(alink,event){ 
			if(typeof this.isToggling!='undefined'||this.isToggling==false){
				this.isToggling = true;
				if((alink instanceof jQuery) == false) alink = $(alink);
				
				var toggleSelected = alink.hasClass('checkall-check-link') ?  true : false;

				// on tables selectString = ".tableRowGroup${groupNumber} .col${columnNumber}"
				// which makes it possible to have ONE table with multiple select-all columns in multiple groups
				var selectString = alink.attr('data-check-group') + ' .checkbox-input:checkbox';

				$(selectString).each(function (ev,el) {
					// this check is for IE compatibility
					if (!($(el).parent().hasClass('disabled'))) {
						var checkbox = $(el).not(':disabled').not('[disabled="disabled"]');

						
						// checkbox has onclick method then call it
						if (checkbox.attr('onclick') != '')
							{checkbox.trigger('click');}
						
						checkbox.setCheckbox(toggleSelected);
		
						
		
						alink.hide();
		
					}
				});
				
				toggleSelected ? alink.parent().children('.checkall-uncheck-link').show() : alink.parent().children('.checkall-check-link').show();
				this.isToggling = false;
			}

		},
		enableElement:function(elem){		// ENABLE
			if (typeof(elem) === 'object') elem.disableCheckbox()
			else $(elem).disableCheckbox();
		},
		disableElement:function(elem){		// DISABLE
			if (typeof(elem) === 'object') elem.enableCheckbox()
			else $(elem).enableCheckbox();
		}
	};
	namespace("fnb.forms.checkBox",checkBox);
});

///-------------------------------///
/// developer: Donovan
///				Vaughan (03/06/2013)
///
/// Tooltip
///-------------------------------///

$(function(){
	function tooltip(){
		this.displaying = {};
		this.currentActiveObject;
	};
	
	tooltip.prototype = {
	
		show : function(obj) {
			this.currentActiveObject = obj;
			var data = this.data(obj);
			var tooltip = data.tooltip;
			var icon = data.icon;
			var position = data.position;
			
			if ($(obj).data('active')) {
				
				this.state('hide', obj, tooltip, icon, position);
				
				this.displaying = {};
				
			} else {
				
				this.state('show', obj, tooltip, icon, position);
				
				//hide current visible tooltip
				if(!$.isEmptyObject(this.displaying)){
					
					var data = this.data(this.displaying);
					var tooltip = data.tooltip;
					var icon = data.icon;
					var position = data.position;
					
					this.state('hide', this.displaying, tooltip, icon, position);
					
				}
				
				this.displaying = obj;

			}

		},
		
		data: function(obj){
			var data = {};
			
			var parent = this;
			data.tooltip = obj.find('.tooltip');
			data.icon = data.tooltip.parent().find('.tooltipButton');
			var pageWidth = $(window).width();
			var offset = obj.offset();
			data.position = parent.determinedPosition(pageWidth, offset.left);
			
			return data;
		},
		state: function(state, obj, tooltip, icon, position){
			if(state == 'hide'){
				if (position == 'left') {
					tooltip.removeClass('leftScreen displayBlock');
				} else {
					tooltip.removeClass('rightScreen displayBlock');
				}
				icon.removeClass ("tooltipButtonCross");
								
				$(obj).data('active', false);
			} else {
				if (position == 'left') {
					tooltip.addClass('leftScreen displayBlock');
				} else {
					tooltip.addClass('rightScreen displayBlock');
				}
				
				icon.addClass ("tooltipButtonCross");
				
				$(obj).data('active', true);
			}
		},
		determinedPosition : function(pageWidth, offset) {

			var positionOffset;

			if ((offset + 610) > pageWidth) {
				positionOffset = 'right'
			} else {
				positionOffset = 'left'
			}

			return positionOffset

		},
		hide : function() {
			if(this.currentActiveObject){
				var data = this.data(this.currentActiveObject);
				var tooltip = data.tooltip;
				var icon = data.icon;
				var position = data.position;
				
				if ($(this.currentActiveObject).data('active')) {
					
					this.state('hide', this.currentActiveObject, tooltip, icon, position);
					
					this.displaying = {};
					
				}
			}
		}
	};
	namespace("fnb.forms.tooltip",tooltip);
});

/// first developer: Mike Stott
///
/// Text Area
///-------------------------------///
$(function() {
	function textAreaHandler() {

		this._textArea;
		this._textAreaWrapper;
		this._maxChars;
		this._characterCount;
		this._scrollTop;

	}

	textAreaHandler.prototype = {
		
		init : function(target, maxChars) {
		
		},
		availChars : function(target, maxChars) {
			var parent = this;
			
			parent._textAreaWrapper = $(target)
			parent._textArea = $(target).find('textarea');
			parent._maxChars = maxChars
			parent._characterCount = 0;
			parent._characterCountDiv = parent._textAreaWrapper.find('.charCount')
		
			parent._characterCount = parent._textArea.val().length;

			if (parent._characterCount > parent._maxChars) {
			parent._characterCount = 0;	
			parent._textArea.val(parent._textArea.val().slice(0,parent._maxChars));
			parent._textArea.scrollTop(5000);
			}else {;
			
			parent._characterCountDiv.html(parent._maxChars - parent._characterCount);
			
			}
			
	
		}
	};

	namespace("fnb.forms.textAreaHandler",textAreaHandler);

});
///----------------------------///
/// developer: Leon
///
/// EXTENDED PAGE HEADING
///----------------------------///
$(function(){
	function extendedPageHeading(){
	};
	extendedPageHeading.prototype = {
		init: function(){
	
			var subTabsPresent =  ($('.subTabsMenu').length);
			if (subTabsPresent) {
				$("#extendedPageHeader").addClass('subTabsHeader').appendTo($('#subTabsPageHeader'));
			}
		}
	};
	namespace("fnb.forms.extendedPageHeading",extendedPageHeading);
});
///-------------------------------///
/// developer: Donovan
///
/// DATE PICKER
///-------------------------------///
var _datePicker=function(){
	this.selectedDatePicker;
	this.currentDatePicker;
	this.datePickerParent;
	this.datePickerExpanded = false;
	this.datePickerEziExpanded = false;
	this.datePickerYear;
	this.datePickerMonthName;
	this.datePickerMonth;
	this.datePickerDay;
	this.datePickerWeekdaySelect;
	this.datePickerWeekday;
	this.datePickerSmallPort;
	this.datePickerSelectedDate;
	this.disablePast;
	this.currentDatePickerObj;
	this.currentDatePickerTarget;
	this.checkPublicHolidays;
	this.noticeDays;
	this.alreadyCheckedPublicHolidays=false;
	this.isEziDate;
	this.scrollPos;
	this.previousDate;
	this.past;
	this.future;
	return{
		init:function(obj,selectedDate,noticeDays){
			this.noticeDays = (noticeDays!='') ? parseInt(noticeDays) : 0;
			this.disablePast = (noticeDays>0) ? true : false;
			this.checkPublicHolidays=undefined;
			this.alreadyCheckedPublicHolidays=false;
			this.previousDate=undefined;
			var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			this.datePickerWeekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
			var hours = '';
			var minutes = '';
			var second = '';
			obj = $('#'+obj);
			if(selectedDate){
				var dateArray = selectedDate.split("-");
				if(dateArray.length>1){
					this.datePickerYear  = dateArray[0];
					this.datePickerMonth = dateArray[1];
					this.datePickerDay =  dateArray[2];
					hours = '';
					minutes = '';
					second = '';
				}else if(selectedDate.split(" ").length==4){
					dateArray = selectedDate.split(" ");
					this.datePickerYear  = dateArray[3];
					var monthIndex = $.inArray(dateArray[2],  months)
					this.datePickerMonth = (monthIndex+1);
					this.datePickerDay =  dateArray[1];
					hours = '';
					minutes = '';
					second = '';
				}else if(selectedDate.match(/^[0-9]{4}[0-9]{2}[0-9]{2}$/)){
					this.datePickerYear  = selectedDate.substring(0,4);
					this.datePickerMonth = selectedDate.substring(4,6);
					this.datePickerDay =  selectedDate.substring(6,8);
					hours = '';
					minutes = '';
					second = '';
				}
			}else{
				var now = new Date();
				this.datePickerYear = now.getFullYear();
				this.datePickerMonth = now.getMonth()+1;
				this.datePickerDay = now.getDate();
				hours = now.getHours();
				minutes = now.getMinutes();
				second = now.getDate();
			}

			var currentDate = this.datePickerYear  + '/' +
				((''+this.datePickerMonth).length<2 ? '0' : '') + this.datePickerMonth + '/' +
				((''+this.datePickerDay).length<2 ? '0' : '') + this.datePickerDay + 'T' +
				((''+hours).length<2 ? '0' : '') + hours + ':' +
				((''+minutes).length<2 ? '0' : '') + minutes + ':' +
				'00Z';

			if((''+this.datePickerDay).length<2) this.datePickerDay = '0'+this.datePickerDay;
			if((''+this.datePickerMonth).length<2) this.datePickerMonth = '0'+this.datePickerMonth;
			
			this.datePickerSelectedDate = this.datePickerYear + '-' +  this.datePickerMonth + '-' + this.datePickerDay;

			$(obj).find('.formsDatePickerValue').val(this.datePickerSelectedDate);

			var prettyDate = this.createPrettyDate(currentDate);
			var prettyDay = new Date(this.datePickerSelectedDate);
			this.datePickerWeekdaySelect = this.datePickerWeekday[prettyDay.getDay()];
			if(prettyDate!='date'){
				$(obj).find('.monthName').html(prettyDate);
				$(obj).find('.datePickerDay').html('');
				$(obj).find('.datePickerYear').html('');
				$(obj).find('.datePickerWeekday').html('');
			}else{
				$(obj).find('.monthName').html(months[parseInt(this.datePickerMonth)-1]);
				$(obj).find('.datePickerDay').html(this.datePickerDay);
				$(obj).find('.datePickerYear').html(this.datePickerYear);
				$(obj).find('.datePickerWeekday').html(this.datePickerWeekdaySelect);
			}
		},
		show:function(obj){
			fnb.forms.scrollUtil.setScrollPos();
			var parmWrapper = $(obj).closest('.datePicker');
			this.past = 101;
			this.future =3;
			if(typeof parmWrapper.attr('data-past')!='undefined') this.past = parmWrapper.attr('data-past');
			if(typeof parmWrapper.attr('data-future')!='undefined') this.future = parmWrapper.attr('data-future');
						
			this.isEziDate = $(obj).closest(_eziWrapper).is('*');
			this.isMobiDate = $(obj).closest('#mobiPopupWrapper').is('*');
			if(this.isEziDate){
				_datePicker.ezishow(obj,"eziWrapper");
			}else{
				this.selectedDatePicker = this;
				this.selectedDatePicker.currentDatePickerObj = $(obj).closest('.datePicker');
				var selectedDate = $(obj).closest('.datePicker').find('input').val();
				if(selectedDate){
					var dateArray = selectedDate.split("-");
					if(dateArray.length>1){
						this.selectedDatePicker.datePickerYear  = dateArray[0];
						this.selectedDatePicker.datePickerMonth = dateArray[1];
						this.selectedDatePicker.datePickerDay =  dateArray[2];
						hours = '';
						minutes = '';
						second = '';
					}else if(selectedDate.split(" ").length==4){
						dateArray = selectedDate.split(" ");
						this.selectedDatePicker.datePickerYear  = dateArray[3];
						var monthIndex = $.inArray(dateArray[2],  months)
						this.selectedDatePicker.datePickerMonth = (monthIndex+1);
						this.selectedDatePicker.datePickerDay =  dateArray[1];
						hours = '';
						minutes = '';
						second = '';
					}else if(selectedDate.match(/^[0-9]{4}[0-9]{2}[0-9]{2}$/)){
						this.selectedDatePicker.datePickerYear  = selectedDate.substring(0,4);
						this.selectedDatePicker.datePickerMonth = selectedDate.substring(4,6);
						this.selectedDatePicker.datePickerDay =  selectedDate.substring(6,8);
						hours = '';
						minutes = '';
						second = '';
					}
				}else{
					now = new Date();
					this.selectedDatePicker.datePickerYear = now.getFullYear();
					this.selectedDatePicker.datePickerMonth = now.getMonth();
					this.selectedDatePicker.datePickerDay = now.getDate();
					hours = now.getHours();
					minutes = now.getMinutes();
					second = now.getDate();
				}
				if((''+this.selectedDatePicker.datePickerDay).length<2) this.selectedDatePicker.datePickerDay = '0'+this.selectedDatePicker.datePickerDay;
				if((''+this.selectedDatePicker.datePickerMonth).length<2) this.selectedDatePicker.datePickerMonth = '0'+this.selectedDatePicker.datePickerMonth;
				
				this.selectedDatePicker.datePickerSelectedDate = this.selectedDatePicker.datePickerYear + '-' +  this.selectedDatePicker.datePickerMonth + '-' +this.selectedDatePicker.datePickerDay;
				
				this.selectedDatePicker.currentDatePickerTarget = 'dropArrow';
				this.selectedDatePicker.datePickerParent= $(obj).closest('.datepickerWrapper');
				this.datePickerEziExpanded = false;
				this.datePickerExpanded = true;
				this.datePickerSmallPort = false;
				
				var calOffset = 1;
				var calCount = 3;
				if($(window).width()<490){
					calCount = 1;
					calOffset = 0;
					this.datePickerSmallPort = true;
				}
				
				this.currentDatePicker = new Calendar({
					element:this.selectedDatePicker.currentDatePickerTarget,
					weekNumbers: false,
					startDay: 0,
					year: this.selectedDatePicker.datePickerYear,
					month: (this.selectedDatePicker.datePickerMonth)-calOffset,
					months: calCount,
					past:this.past ,
					future:this.future,
					noticeDays:this.noticeDays,
					disablePast: this.disablePast,
					selectedDate:this.selectedDatePicker.datePickerSelectedDate,
					onSelect: function (element, selectedDate, date, cell) {			
						_datePicker.selectedDatePicker.datePickerYear = _datePicker.currentDatePicker.selectedDate.getFullYear();
						_datePicker.selectedDatePicker.datePickerMonth = _datePicker.currentDatePicker.selectedDate.getMonth()+1;
						_datePicker.selectedDatePicker.datePickerMonthName = _datePicker.currentDatePicker.opts.monthNames[(_datePicker.selectedDatePicker.datePickerMonth-1)]
						_datePicker.selectedDatePicker.datePickerDay = _datePicker.currentDatePicker.selectedDate.getDate();
						_datePicker.selectedDatePicker.datePickerWeekdaySelect = _datePicker.datePickerWeekday[_datePicker.currentDatePicker.selectedDate.getDay()];
						_datePicker.hide(true,true);
					}
				});
				
				$('div[id^="bcal-container"]').addClass('opacity100');
				
				//New frame hack
				fnb.hyperion.controller.bodyElement.attr('data-clipOverflow', 'true');
				//Set datepicker to visible
				fnb.hyperion.controller.datePickerElement.show();
			
				if(this.datePickerSmallPort == true){
					$('div[id^="bcal-container"]').addClass('mobiPopupWrapper-bcal-container');
				}
			}
			
		},
		hide:function(selectValue,valid){
			var parent = this;
			if($('div[id^="bcal-container"]').is('*')){
				if(valid==true&&this.checkPublicHolidays!=false){
					this.selectedDatePicker.datePickerSelectedDate =this.selectedDatePicker.datePickerYear + '-' +  this.selectedDatePicker.datePickerMonth + '-' + this.selectedDatePicker.datePickerDay;
					fnb.functions.datePickerVerify.verifyDateValid(this.selectedDatePicker.datePickerSelectedDate);
					var day = this.selectedDatePicker.datePickerDay;
					var month = this.selectedDatePicker.datePickerMonth;
					if((''+this.selectedDatePicker.datePickerDay).length<2) day = '0'+this.selectedDatePicker.datePickerDay;
					if((''+this.selectedDatePicker.datePickerMonth).length<2) month = '0'+this.selectedDatePicker.datePickerMonth;
					var currentDate = this.selectedDatePicker.datePickerYear + '-' +  month + '-' + day;
					if (fnb.functions.datePickerVerify.dateVerified==true&&fnb.functions.datePickerVerify.validDate==false&&this.previousDate != currentDate) {
						if (this.previousDate!='undefined') this.alreadyCheckedPublicHolidays = false;
						this.previousDate = currentDate;
					}
				}
				if(fnb.functions.datePickerVerify.dateVerified==true&&fnb.functions.datePickerVerify.validDate==true||valid==false||this.alreadyCheckedPublicHolidays==true){
					this.datePickerExpanded = false;
					this.datePickerEziExpanded = false;
					function remove(){
						$('div[id^="bcal-container"]').remove();
					}
					
					TweenMax.to($('div[id^="bcal-container"]'), 0, {css:{opacity:0},ease:Circ.easeOut,onComplete:remove});

					//New frame hack
					fnb.hyperion.controller.bodyElement.attr('data-clipOverflow', '');
					//Set datepicker to visible
					fnb.hyperion.controller.datePickerElement.hide();
				
					if(selectValue==true){
 					
						var currentDate =this.selectedDatePicker.datePickerYear + '/' +
						((''+this.selectedDatePicker.datePickerMonth ).length<2 ? '0' : '') + this.selectedDatePicker.datePickerMonth  + '/' +
						((''+this.selectedDatePicker.datePickerDay).length<2 ? '0' : '') + this.selectedDatePicker.datePickerDay + 'T' +
						((''+'00').length<2 ? '0' : '') + '00' + ':' +
						((''+'00').length<2 ? '0' : '') + '00' + ':' +
						'00Z';

						var tempDatePickerMonth =  this.selectedDatePicker.datePickerMonth;

						if((''+this.selectedDatePicker.datePickerDay).length<2) this.selectedDatePicker.datePickerDay = '0'+this.selectedDatePicker.datePickerDay
						if((''+this.selectedDatePicker.datePickerMonth).length<2) this.selectedDatePicker.datePickerMonth = '0'+this.selectedDatePicker.datePickerMonth
						if((''+tempDatePickerMonth).length<2) tempDatePickerMonth = '0'+tempDatePickerMonth

						this.selectedDatePicker.datePickerSelectedDate = this.selectedDatePicker.datePickerYear + '-' +  this.selectedDatePicker.datePickerMonth + '-' + this.selectedDatePicker.datePickerDay;
						var prettyDate = this.createPrettyDate(currentDate);

						$(this.selectedDatePicker.currentDatePickerObj).closest('div[id*="_datePicker"]').find('.formsDatePickerValue').val(this.selectedDatePicker.datePickerYear + '-' +  tempDatePickerMonth + '-' + this.selectedDatePicker.datePickerDay);

						if(prettyDate!='date'){
							this.selectedDatePicker.datePickerParent.find('.monthName').text(prettyDate);
							this.selectedDatePicker.datePickerParent.find('.datePickerDay').text('');
							this.selectedDatePicker.datePickerParent.find('.datePickerYear').text('');
							this.selectedDatePicker.datePickerParent.find('.datePickerWeekday').text('');
						}else if(prettyDate=='undefined'||prettyDate=='date'){
							this.selectedDatePicker.datePickerParent.find('.monthName').text(this.selectedDatePicker.datePickerMonthName);
							this.selectedDatePicker.datePickerParent.find('.datePickerDay').text(this.selectedDatePicker.datePickerDay);
							this.selectedDatePicker.datePickerParent.find('.datePickerYear').text(this.selectedDatePicker.datePickerYear);
							this.selectedDatePicker.datePickerParent.find('.datePickerWeekday').text(this.selectedDatePicker.datePickerWeekdaySelect);
						}
					}
					if(!this.isEziDate&&!this.isMobiDate){
						fnb.controls.controller.eventsObject.raiseEvent('closeDatePicker', this.selectedDatePicker);
					}else{
						$(_eziWrapper).find('#eziPannelButtonsWrapper').show();
						$(_eziWrapper).find('.eziPage').css({
							'margin': '0 0 75px 0'
						}).show();
						$(_eziWrapper).css({
							'height':'',
							'cursor': ''
						})
					}
				}else{
					this.notifyHolidayShow();
					this.alreadyCheckedPublicHolidays=true;
				}
			}
			fnb.forms.scrollUtil.scrollPosUpdate();
		},
		checkFooterDatePicker:function(popupUrl, submitForm, triggerButton, checkHoliday){
			var currentDate = $('#formFooterButtons').find('.formsDatePickerValue').val();
			if(this.checkPublicHolidays!=false&&checkHoliday){
				if (fnb.functions.datePickerVerify.validDate==false&&this.previousDate != currentDate) {
					if (this.previousDate!='undefined') this.alreadyCheckedPublicHolidays = false;
					this.previousDate = currentDate; 
				}
			}
			if (checkHoliday&&fnb.functions.datePickerVerify.validDate==false&&!this.alreadyCheckedPublicHolidays) {
				fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl',popupUrl);
				$('#popupWrapper').addClass('trendOverlay');
				this.notifyHolidayShow();
			}
			else {
				fnb.functions.submitFormToWorkspace.submit(submitForm,'',triggerButton, {alternateUrl: ''});
			}
		},
		setAlreadyCheckedPublicHolidays:function(checkHoliday){
			this.alreadyCheckedPublicHolidays = checkHoliday;
		},
		ezishow:function(obj,target){
			this.selectedDatePicker = this;
			this.selectedDatePicker.currentDatePickerObj = obj;
			this.selectedDatePicker.currentDatePickerTarget =target;

			var selectedDate = $(obj).find('input').val();
			if(selectedDate){
				var dateArray = selectedDate.split("-");
				if(dateArray.length>1){
					this.selectedDatePicker.datePickerYear  = dateArray[0];
					this.selectedDatePicker.datePickerMonth = dateArray[1];
					this.selectedDatePicker.datePickerDay =  dateArray[2];
					hours = '';
					minutes = '';
					second = '';
				}else if(selectedDate.split(" ").length==4){
					dateArray = selectedDate.split(" ");
					this.selectedDatePicker.datePickerYear  = dateArray[3];
					var monthIndex = $.inArray(dateArray[2],  months)
					this.selectedDatePicker.datePickerMonth = (monthIndex+1);
					this.selectedDatePicker.datePickerDay =  dateArray[1];
					hours = '';
					minutes = '';
					second = '';
				}else if(selectedDate.match(/^[0-9]{4}[0-9]{2}[0-9]{2}$/)){
					this.selectedDatePicker.datePickerYear  = selectedDate.substring(0,4);
					this.selectedDatePicker.datePickerMonth = selectedDate.substring(4,6);
					this.selectedDatePicker.datePickerDay =  selectedDate.substring(6,8);
					hours = '';
					minutes = '';
					second = '';
				}
			}else{
				now = new Date();
				this.selectedDatePicker.datePickerYear = now.getFullYear();
				this.selectedDatePicker.datePickerMonth = now.getMonth()+1;
				this.selectedDatePicker.datePickerDay = now.getDate();
				hours = now.getHours();
				minutes = now.getMinutes();
				second = now.getDate();
			}
			if((''+this.selectedDatePicker.datePickerDay).length<2) this.selectedDatePicker.datePickerDay = '0'+this.selectedDatePicker.datePickerDay;
			if((''+this.selectedDatePicker.datePickerMonth).length<2) this.selectedDatePicker.datePickerMonth = '0'+this.selectedDatePicker.datePickerMonth;
			
			this.selectedDatePicker.datePickerSelectedDate = this.selectedDatePicker.datePickerYear + '-' +  this.selectedDatePicker.datePickerMonth+ '-' +this.selectedDatePicker.datePickerDay;

			this.datePickerEziExpanded = true;
			this.datePickerExpanded = false;
			this.selectedDatePicker.datePickerParent= $(obj).closest('.datepickerWrapper');

			this.datePickerSmallPort = true;

			this.currentDatePicker = new Calendar({
				element: this.selectedDatePicker.currentDatePickerTarget,
				inline: true,
				weekNumbers: false,
				startDay: 0,
				year: this.selectedDatePicker.datePickerYear,
				month: (this.selectedDatePicker.datePickerMonth)-1,
				months: 1,
				selectedDate:this.selectedDatePicker.datePickerSelectedDate,
				onSelect: function (element, selectedDate, date, cell) {
					_datePicker.selectedDatePicker.datePickerYear = _datePicker.currentDatePicker.selectedDate.getFullYear();
					_datePicker.selectedDatePicker.datePickerMonth = _datePicker.currentDatePicker.selectedDate.getMonth()+1;
					_datePicker.selectedDatePicker.datePickerMonthName = _datePicker.currentDatePicker.opts.monthNames[(_datePicker.selectedDatePicker.datePickerMonth-1)]
					_datePicker.selectedDatePicker.datePickerDay = _datePicker.currentDatePicker.selectedDate.getDate();
					_datePicker.selectedDatePicker.datePickerWeekdaySelect = _datePicker.datePickerWeekday[_datePicker.currentDatePicker.selectedDate.getDay()];
					_datePicker.hide(true,true);
				}
			});
			/* $(_eziWrapper).css({
				'height':$(window).height()-205
			}) */
			$(_eziWrapper).find('.eziPage').css({
				'display':'none'
			})
			$(_eziWrapper).find('#eziPannelButtonsWrapper').css({
				'display':'none'
			})

			_datePicker.applyEziStyles();
			
			$(_eziWrapper).find('div[id^="bcal-container"]').find('div[id^="inner-bcal-container"]').verticallyAlign();
			$(_eziWrapper).find('div[id^="bcal-container"]').find('div[id^="backgound-bcal-container"]').verticallyAlign();
		
			$(_eziWrapper).find('.bcal-nav-left').on('click touchend', function(e) {
				$(_eziWrapper).find('.ezi-inner-bcal-container').addClass('displayNone');
				setTimeout(function(){_datePicker.applyEziStyles();
					$(_eziWrapper).find('.ezi-inner-bcal-container').removeClass('displayNone');	
				},50)
			});
			$(_eziWrapper).find('.bcal-nav-right').on('click touchend', function(e) {
				$(_eziWrapper).find('.ezi-inner-bcal-container').addClass('displayNone');
				setTimeout(function(){_datePicker.applyEziStyles();
				$(_eziWrapper).find('.ezi-inner-bcal-container').removeClass('displayNone');	
				},50)
			});
			
			$('div[id^="bcal-container"]').addClass('opacity100')

		},
		applyEziStyles:function(){
			$('div[id^="bcal-container"]').addClass('ezi-bcal-container');
			$('div[id^="inner-bcal-container"]').addClass('ezi-inner-bcal-container');
			$('div[class^="bcal-yearDiv"]').addClass('ezi-bcal-yearDiv');
			$('th[class^="bcal-nav-left"]').addClass('ezi-bcal-nav-left');
			$('.bcal-table').addClass('ezi-bcal-table');
			$('th[class^="bcal-nav-right"]').addClass('ezi-bcal-nav-right');
			$('th[class^="bcal-month"]').addClass('ezi-bcal-month');
			$('th[class^="bcal-monthName"]').addClass('ezi-bcal-monthName');
			$('div[id^="backgound-bcal-container"]').addClass('ezi-backgound-bcal-container');
		
			$(_eziWrapper).find('.bcal-nav-left').on('click touchend', function(e) {
				$(_eziWrapper).find('.ezi-inner-bcal-container').addClass('displayNone');
				setTimeout(function(){_datePicker.applyEziStyles();
					$(_eziWrapper).find('.ezi-inner-bcal-container').removeClass('displayNone');	
				},50)
			});
			$(_eziWrapper).find('.bcal-nav-right').on('click touchend', function(e) {
				$(_eziWrapper).find('.ezi-inner-bcal-container').addClass('displayNone');
				setTimeout(function(){_datePicker.applyEziStyles();
				$(_eziWrapper).find('.ezi-inner-bcal-container').removeClass('displayNone');	
				},50)
			});
		},
		createPrettyDate:function(time){
			var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
			diff = (((new Date()).getTime() - date.getTime()) / 1000),
			day_diff = Math.floor(diff / 86400);

			if ( isNaN(day_diff))
				return;
				//IF MINUTES ETC ARE REQUIRED
/* 			return day_diff < -1 && "date" ||
				day_diff == -1 && "Tommorow" ||
				day_diff == 0 && (
				diff < 60 && "Today" ||
				diff < 120 && "1 minute ago" ||
				diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
				diff < 7200 && "1 hour ago" ||
				diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
				day_diff == 1 && "Yesterday" ||
				day_diff < 7 && day_diff + " days ago" ||
				day_diff < 31 && "date"||
				day_diff > 31 && "date"; */
			return day_diff < -1 && "date" ||
				day_diff == -1 && "Tomorrow" ||
				day_diff == 0 && (
				diff < 60 && "Today" ||
				diff < 120 && "Today" ||
				diff < 3600 && "Today"  ||
				diff < 7200 && "Today"  ||
				diff < 86400 && "Today" )||
				day_diff == 1 && "Yesterday" ||
				day_diff > 1 && "date"
				;
		},
		notifyHolidayShow:function(){
			$('div[id^="inner-bcal-container"]').addClass('hideElement');
			$('#calendarCancelButton').addClass('hideElement');
			$('#calendarDoneButton').addClass('hideElement');
			$('#yearPicker_calendar').addClass('hideElement');
			$('#confirmHolidayButton').removeClass('hideElement');
			$('#cancelHolidayButton').removeClass('hideElement');
			$('#confirmHolidayTextWrapper').removeClass('hideElement');
		},
		notifyHolidayHide:function(){
			$('div[id^="inner-bcal-container"]').removeClass('hideElement');
			$('#calendarCancelButton').removeClass('hideElement');
			$('#calendarDoneButton').removeClass('hideElement');
			$('#yearPicker_calendar').removeClass('hideElement');
			$('#confirmHolidayButton').addClass('hideElement');
			$('#cancelHolidayButton').addClass('hideElement');
			$('#confirmHolidayTextWrapper').addClass('hideElement');
		},
		updateYear:function(element){
			var newDateYear = $(element).attr('data-value');
			this.currentDatePicker.detach();
			var displayMonths = 3
			var displayInline = false;
			$('div[id^="bcal-container"]').remove();
			
			if(this.datePickerSmallPort==true){
				displayMonths = 1;
				displayInline = true;
			}

			this.currentDatePicker = new Calendar({
				element: this.currentDatePickerTarget,
				inline: displayInline,
				weekNumbers: false,
				startDay: 0,
				year: newDateYear,
				month: (_datePicker.datePickerMonth)-1,
				months: displayMonths,
				past:this.past ,
				future:this.future,
				selectedDate:_datePicker.datePickerSelectedDate,
				onSelect: function (element, selectedDate, date, cell) {			
					_datePicker.datePickerYear = newDateYear;
					_datePicker.datePickerMonth = _datePicker.currentDatePicker.selectedDate.getMonth()+1;
					_datePicker.datePickerMonthName = _datePicker.currentDatePicker.opts.monthNamesFull[(_datePicker.datePickerMonth-1)]
					_datePicker.datePickerDay = _datePicker.currentDatePicker.selectedDate.getDate();
					_datePicker.datePickerWeekdaySelect = _datePicker.datePickerWeekday[_datePicker.currentDatePicker.selectedDate.getDay()];
					_datePicker.hide(true,true);
				}
			});
			
			if(this.datePickerSmallPort==true){
				$('div[id^="bcal-container"]').css({opacity:1})

				$('#yearPicker_calendar').find('li').each(function (index, item) {
						if($(item).attr('data-value')==newDateYear) fnb.forms.dropdown.select($(item),false,false);
				});
				
				$(_eziWrapper).css({
					'height':$(window).height()-205
				})
				$(_eziWrapper).find('.eziPage').css({
					'display':'none'
				})
				$(_eziWrapper).find('#eziPannelButtonsWrapper').css({
					'display':'none'
				})

				_datePicker.applyEziStyles();

				$(_eziWrapper).find('div[id^="bcal-container"]').find('div[id^="inner-bcal-container"]').verticallyAlign();
				$(_eziWrapper).find('div[id^="bcal-container"]').find('div[id^="backgound-bcal-container"]').verticallyAlign();
				
				/*$(_eziWrapper).find('.bcal-nav-left').click(function(e) {
					_datePicker.applyEziStyles();
				});
				$(_eziWrapper).find('.bcal-nav-right').click(function(e) {
					_datePicker.applyEziStyles();
				});*/
				
				$(_eziWrapper).find('.bcal-nav-left').on('click touchend', function(e) {
					_datePicker.applyEziStyles();
				});
				$(_eziWrapper).find('.bcal-nav-right').on('click touchend', function(e) {
					_datePicker.applyEziStyles();
				});
			}
			$('div[id^="bcal-container"]').css({opacity:1})

		},
		adjust: function(windowWidth){
			if(this.datePickerExpanded == true){
				if(windowWidth<805){
					if(this.datePickerSmallPort==false){
						this.currentDatePicker.detach();
						$('div[id^="bcal-container"]').remove();
						
						this.currentDatePicker = new Calendar({
							element: 'dropArrow',
							weekNumbers: false,
							startDay: 0,
							year: _datePicker.datePickerYear,
							month: _datePicker.datePickerMonth,
							months: 1,
							selectedDate:_datePicker.datePickerSelectedDate,
							onSelect: function (element, selectedDate, date, cell) {			
								_datePicker.datePickerYear = _datePicker.currentDatePicker.selectedDate.getFullYear();
								_datePicker.datePickerMonth = _datePicker.currentDatePicker.selectedDate.getMonth()+1;
								_datePicker.datePickerMonthName = _datePicker.currentDatePicker.opts.monthNamesFull[(_datePicker.datePickerMonth-1)]
								_datePicker.datePickerDay = _datePicker.currentDatePicker.selectedDate.getDate();
								_datePicker.datePickerWeekdaySelect = _datePicker.datePickerWeekday[_datePicker.currentDatePicker.selectedDate.getDay()];
								_datePicker.hide(true,true);
							}
						});
						
						$('div[id^="bcal-container"]').css({opacity:1});
					}
/* 					$('div[id^="bcal-container"]').css({
						'height':$(window).height()-205,
						'top': _topOffset+'px',
						'margin-top':'auto'
					}) */
					this.datePickerSmallPort = true;
				}else{
					if(this.datePickerSmallPort==true){
						this.currentDatePicker.detach();
						$('div[id^="bcal-container"]').remove();
						
						this.currentDatePicker = new Calendar({
							element: 'dropArrow',
							weekNumbers: false,
							startDay: 0,
							year: _datePicker.datePickerYear,
							month: _datePicker.datePickerMonth,
							months: 3,
							selectedDate:_datePicker.datePickerSelectedDate,
							onSelect: function (element, selectedDate, date, cell) {			
								_datePicker.datePickerYear = _datePicker.currentDatePicker.selectedDate.getFullYear();
								_datePicker.datePickerMonth = _datePicker.currentDatePicker.selectedDate.getMonth()+1;
								_datePicker.datePickerMonthName = _datePicker.currentDatePicker.opts.monthNamesFull[(_datePicker.datePickerMonth-1)]
								_datePicker.datePickerDay = _datePicker.currentDatePicker.selectedDate.getDate();
								_datePicker.datePickerWeekdaySelect = _datePicker.datePickerWeekday[_datePicker.currentDatePicker.selectedDate.getDay()];
								_datePicker.hide(true,true);
							}
						});
						
						$('div[id^="bcal-container"]').css({opacity:1})
					}
					this.datePickerSmallPort = false;
				}
			}else if(this.datePickerEziExpanded==true){
				/* $(_eziWrapper).css({
					'height':$(window).height()-205
				}) */
			}
		}
	};
}();

$(function() {
	function tableUtils() {
		
		this.groupOpen = false;
		this.container = "#tableActionButtons"
		this.searchOpen = false;
		
		if(!_isMobile){
			$('body').on('touchend', '.tableActionButtonContent', function(event){
				
				event.preventDefault()
				$(event.target).trigger('click');
				
			})
		};
	};
	tableUtils.prototype = {
			
		init : function(target) {
		
			var parent = this;
			parent.groupOpen = false;
			parent.searchOpen = false;
		},	
		
		switcherClick : function(event) {
					
			var parent = this;
			var target = $(event.target);
				
			if(target.attr('data-postcallback')){
				
				var container = target.parent();
				
				$(container.find('.tableSwitcherButton')).each(function(){
					if($(this).hasClass('tableSwitcherSelected')){
						$(this).removeClass('tableSwitcherSelected');
					}
				});
				target.addClass('tableSwitcherSelected');
				
				var functionRef = target.data('postcallback');
				var tempFunc = new Function(functionRef);
				
				tempFunc();
			
								
			}else{
				
				parent.switcherNavigate(target);
			}
		},
		switcherNavigate: function(target) {
			
			var url = target.data('url');
			fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen',url)	
			
		},
		actionButtonClick : function(target) {
			// type 0: action group
			// type 1: search button
			// type 2: download
			// type 3: print
			var parent = this;
			var type = target.data('type');
			switch(type){
				case 0:
					parent.actionGroupOpen(target)
				break;
				case 1:
				if(parent.groupOpen) parent.actionGroupClose(target);
				if (parent.searchOpen == false) {
					parent.actionSearch(target);
				} else {
					parent.actionSearchClose(target);
				};
				break;
				case 2:
					if(parent.groupOpen) parent.actionGroupClose(target);
					parent.actionDownload(target);
				break;
				case 3:
					if(parent.groupOpen) parent.actionGroupClose(target);	
					parent.actionPrint(target);
				break;
				case 4:
					parent.actionSearchClose(target);
				break;
				default:
				break;
				};		

		},	
		actionGroupOpen : function(target) {
			var parent = this;
			
			var groupContainer = $(target).find('.tableActionButtonContent')

			if (parent.groupOpen) {

				parent.actionGroupClose(target)
			
			}else if(parent.groupOpen == false) {

				groupContainer.addClass('buttonGroupExpanded')
				parent.groupOpen = true;

			}
			if(target.attr('onclick')!=undefined&&_isMobile){
				target.trigger('onclick');
			}
		},
		actionGroupClose : function(target){
			
			var parent = this;
			parent.groupOpen = false;
			var groupContainer = $(target).find('.tableActionButtonContent');
			groupContainer.removeClass('buttonGroupExpanded')	
			
		},
		actionDownload : function(target) {
			if(target.attr('onclick')!=undefined&&_isMobile){
				target.trigger('onclick');
			}
		},
		actionPrint : function(target) {
			if(target.attr('onclick')!=undefined&&_isMobile){
				target.trigger('onclick');
			}
		},
		actionSearch : function(target) {
			var parent = this;
			var container = $(parent.container);
			var actionButtons = container.find('.tableActionButton');
			var label = container.find('.headerControlLabel'); 
			var searchBar = container.find('.tableSearchBarWrapper'); 
			var searchInput = searchBar.find('input')
			var searchBarClose = container.find('.searchClose'); 
			var searchIcon = container.find('.searchButton');  
						
			label.addClass('displayNone');
			searchBar.addClass('displayBlock');
			searchBarClose.addClass('displayBlock');
			searchIcon.addClass('displayNone');
			
			searchInput.focus();
		
			parent.searchOpen = true;
		
		},
		actionSearchClose : function(target) {
			
			var parent = this;
			var container = $(parent.container);
			var actionButtons = container.find('.tableActionButton');
			var label = container.find('.headerControlLabel'); 
			var searchBar = container.find('.tableSearchBarWrapper');
			var searchInput = searchBar.find('input')
			var searchBarClose = container.find('.searchClose'); 
			var searchIcon = container.find('.searchButton');  
			
			searchInput.focus();
			searchInput.val(undefined);
			searchInput.trigger('keyup');
			
			label.removeClass('displayNone');
			searchBar.removeClass('displayBlock');
			searchBarClose.removeClass('displayBlock');
			searchIcon.removeClass('displayNone');
			
			
			$(actionButtons).each(function(){
				
				if($(this).hasClass('searchClose')){	
			
					$(this).removeClass('displayBlock');	
					
				}
			});
			
			parent.searchOpen = false;
			
		},
		countButtons : function() {
			var parent = this;
			var container = $(parent.container);
			var searchContainer = container.find('.tableSearchBarWrapper');
		},
		clearAmountsOnTable: function(colClass){
			var searchClass = '.'+colClass+' input.input-input';
			$('.tableContainer').find(searchClass).each(function(index, item){
				if($(item).val() != '0.00'){
					$(item).val('0.00');
					$(item).attr('data-value','0.00');
				}
			});
		}
			
	};
	namespace("fnb.forms.tableUtils", tableUtils);
});

$(function() {
	function scrollUtil() {
		this.scrollPos = 0;
		this.elementBottom;
		this.windowScroll;
		this.windowBottom;
		this.padding = 70;
		this.windowHeight = $(window).height();
	};
	scrollUtil.prototype = {
			
	setScrollPos : function() {
		var parent = this;
		parent.scrollPos = $(document).scrollTop();
	},
	
	scrollPosUpdate : function () {
		var parent = this;
		$(document).scrollTop(parent.scrollPos);
	},
	
	tabScroll : function (target) {
		var parentObject = this;
		// Constant amount of padding an element should be from the bottom of the window

		// Check their position relative to the window's scroll
		parentObject.elementBottom = $(target).offset().top + $(target).height();
		parentObject.windowScroll = $(window).scrollTop();
		parentObject.windowBottom = parentObject.windowScroll + parentObject.windowHeight;
		 
		if(parentObject.elementBottom + parentObject.padding > parentObject.windowBottom){
			$(window).scrollTop(parentObject.windowScroll + parentObject.padding);
		}
	}
	

	};
    namespace("fnb.forms.scrollUtil", scrollUtil);
});