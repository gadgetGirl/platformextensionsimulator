
///-------------------------------///
/// developer: Donovan
///
/// Progress bar
///-------------------------------///

var progressBar = function () {
	this.progressTimer;
	this.slowConnectionTimer;
	this.progressPosEnd;
	this.progressPos;
	this.progressInterval;
	this.progressTime;
	this.slowConnectionTime;
	this.progressActive=false;
	this.progressClass = "";
	this.percentVisible=false;
	this.target='';
	this.progressBar;
	this.progressBarPercent;
	this.progressWrapper;
	return  {
		init: function (id) {
			_this = this;
			this.target = id;
			this.progressPosEnd = Math.floor(Math.random() * (80-60+1)) + 60;
			this.progressInterval = 1;
			this.progressPos = 0;
			this.progressTime = 40;
			this.slowConnectionTime = 60000;
			this.progressBar = document.getElementById(this.target+'ProgressBar');
			this.progressBarPercent = document.getElementById(this.target+'ProgressBarPercent');
			this.progressWrapper = document.getElementById(this.target+'ProgressWrapper');
			setTimeout(_this.addClasses, 100);
			this.progressBar.style.width = 0;
			if(progressBar.progressActive==true){
				clearInterval(progressBar.progressTimer);
			}
			progressBar.createTimer();
		},
		createTimer: function () {
			progressBar.progressActive = true;
			showPos = 18;
			if(_smallPort) showPos = 30;
			this.progressTimer = setInterval (function ()
				{
					progressBar.progressPos = progressBar.progressPos + progressBar.progressInterval;
					if(progressBar.progressPos>96) progressBar.progressPos = 96;
					if(progressBar.progressPos>=progressBar.progressPosEnd){
						clearInterval(progressBar.progressTimer);
						if(progressBar.progressPos<96){
							progressBar.progressTime = progressBar.progressTime+800;
							progressBar.progressInterval = Math.floor(Math.random() * (((96 - progressBar.progressPos)/3)-1+1)) + 1;
							progressBar.progressPosEnd = Math.floor(Math.random() * (96-progressBar.progressPos+1)) + progressBar.progressPos;
							progressBar.createTimer()
						}else{
							progressBar.slowConnectionTimer = setTimeout(function(){fnb.functions.slowConnection.show();},progressBar.slowConnectionTime)
						}
					}
					progressBar.draw(progressBar.progressPos,showPos);
				}, 
				progressBar.progressTime);
		},
		draw: function (progressPos,showPos) {
			this.progressBar.style.width = progressPos+'%';
				if(progressPos<showPos){
				if(progressPos>(showPos-6)){
					this.progressBarPercent.className = 'progressBarPercent'+progressPos;
				}
			}else if(progressPos>(showPos-1)){
				this.progressBarPercent.innerHTML = progressPos+'%';
			}
		},
		addClasses: function () {
			if(progressBar.progressActive){
				if(typeof progressBar.progressClass=='undefined') progressBar.progressClass = '';
				progressBar.progressBarPercent.className = 'progressBarPercent progressBarHidden';
				progressBar.progressWrapper.className = ''+progressBar.progressClass;
			}
		},
		clear: function () {
			if(progressBar.progressActive==true){
				progressBar.progressActive = false;
				clearInterval(progressBar.progressTimer);
				if(typeof progressBar.slowConnectionTimer!='undefined') clearTimeout(progressBar.slowConnectionTimer)
				this.progressWrapper.className = 'progressBarHidden';
			}
		},
		destroy: function () {
			
		}
	};
}();

