///-------------------------------------------///
/// developer: Donovan
///
/// VARS
///-------------------------------------------///
var _body = "#bodyContainer";
var _header = "#header";
var _main = "#main";
var _bodyGlobalWidth = "#bodyGlobalWidth";
var _workspace = "#pageContent";
var _errorPanel = "#errorPanel";
var _errorsWrapper = "#errorsWrapper";
var _errorMessageWrapper = "#errorMessageWrapper";
var _eziWrapper = "#eziPageContent";
var _eziPannelButtons = "#eziPannelButtons";
var _eziProgressWrapperContents = 'div[data-role="eziInnerTwo"]';
var _actionMenuButton = "#actionMenuButtonWrapper";
var _actionMenuUrlWrapper = "#actionMenuUrlWrapper";
var _topNavWrapper = "#topNavWrapper";
var _topNavScrollable = "#topNavScrollable";
var _topNavContainer = "#topNavContainer";
var _topNav = '#topNav';
var _topMenu = '#topMenu';
var _topNavIndicator = "#topNavMenuSliderIndicator";
var _footerWrapper = "#footerWrapper";
var _footerMessage = "#footerMessage";
var _headerWrapper = "#headerButtonsWrapper";
var _pageContainer = "#pageContent";
var _defaultTargetContent = "#contentWrap";
var _headerButtonsWrapper = "#headerButtonsWrapper";
var _actionMenuWrapper = "#actionMenu";
var _overlay = "#overlay";
var _popupWrapper = 'div[data-role="notificationsInner"]';
var _printDiv = "#hiddenPrintDiv";
var _printDivWrapper = "#hiddenPrintWrapper";
var _hiddenDiv = "#logOff";
var _subTabsScrollable = "#subTabsScrollable";
var _formFooterButtons = "#formFooterButtons";
var _sessionTimedOutOverlay = "#sessionTimedOutOverlay";
var _slowConnectionOverlay = "#slowConnectionContainer";
var _hiddenLogOffDiv = "#logOff";
var _calendarWrapper = "#calendarWrapper";
var _smallPort = false; 
var _tinyPort = false;
var _tabMinHeight = 84;
var _topNavMinWidth = 720;
var _phoneWindowWidthMax = 782;
var _phoneWindowWidthMed = 576;
var _phoneWindowWidthMin = 430;
var _sliderOffset = 40;
var _topOffset = 124;
var _mobiTopOffset = 98;
var _topNavFreezePosition = 112;
var _device;
var _browserName;
var _browserVersion;
var _operatingSystem;
var _isMobile;
var _pageDataObject;
var _logOffTimer;
var _timeOut = 1000;
var _siteMaxWidth = 1280;
var _isIE8 = false;
var _skin;
///-------------------------------------------///
/// developer: Donovan
///
/// Document Load
///-------------------------------------------///
$(window).load(function() {


});

///-------------------------------------------///
/// developer: Donovan
///
/// Document Ready
///-------------------------------------------///
$(document).ready(function() {	

	namespace("fnb.controls.controller", new fnb.controls.controller());
	
	fnb.controls.controller.init();
	
	if ($(window).width() < 600)  _topOffset = '108';
	_device = fnb.utils.currentDevice.getDevice();
	var touch = ('ontouchstart' in window );
	var gesture = ('ongesturestart' in window);

	_isMobile = (fnbIsMobile) ? (fnbIsMobile=="false") ? false : true : false;

	_browserName = _device.browser;
	_browserVersion = _device.version;
	_operatingSystem = _device.platform;

	if(_browserName=="MSIE"&&$.support.boxModel==false||_browserName=="MSIE"&&parseInt(document.documentMode)<8){
		fnb.functions.gotoUrl.go('/banking/wrongBrowser.jsp');
	}

	if(_browserName=="MSIE"&&_browserVersion<9){
		_isIE8 = true;
	}
	fnb.controls.controller.raiseEvent(fnb.controls.controller.getCurrentEventsGroup(), 'SiteLoaded');
});
 
///-------------------------------------------///
/// developer: Donovan
///
/// Document Resize
///-------------------------------------------///
$(window).resize(function() {
 	//if(fnb.utils.topTabs) fnb.utils.topTabs.adjust($(window).width());
	//if(fnb.utils.actionMenu) fnb.utils.actionMenu.adjust($(window).width());
	//if(fnb.utils.eziSlider) fnb.utils.eziSlider.adjust();
	_datePicker.adjust($(window).width());
	if(fnb.forms.dropdown) fnb.forms.dropdown.adjust();
	fnb.utils.mobile.adjust($(window).width());
	fnb.utils.scrollingBanner.adjust($(window).width());
	if(fnb.functions.bigThree.initialized==true) fnb.functions.bigThree.adjust($(window).width());
	if(_isIE8) fnb.functions.ie8.doCheck();
	
	//fnb.hyperion.utils.footer.configFooterButtons()
});

///-------------------------------------------///
/// developer: Donovan
///
/// Document Orientation Change
///-------------------------------------------///
$( window ).on( "orientationchange", function( event ) {
	
	fnb.utils.mobile.utils.changeOrientation(event);
	fnb.utils.popup.orientationChanged();
	
});