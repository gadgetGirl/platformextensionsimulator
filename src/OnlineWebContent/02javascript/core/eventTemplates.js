 var eventTemplates ={
	'loadSite': [
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()'),
		new Function('','fnb.controls.controller.load("topMenu",_header,topMenuUrl)')
	],
	'loadSiteLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.functions.siteLoadComplete.complete()'),
		new Function('','fnb.controls.controller.createTopMenuObj()'),
		new Function('','fnb.controls.controller.createPageObj(_body)'),
		new Function('','fnb.utils.mobile.properties.init()'),
		new Function('','fnb.hyperion.utils.topTabs.init();'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('sender, loadObj','fnb.hyperion.utils.actionMenu.initNewActionMenu(loadObj)'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton();')
	],
	'navError': [
		new Function('','if(!fnb.hyperion.utils.eziPanel.active){fnb.hyperion.utils.notifications.hide()}'),
		new Function('','if(!fnb.hyperion.utils.eziPanel.active){fnb.hyperion.utils.actionMenu.showButton()}'),
		new Function('','if(!fnb.hyperion.utils.eziPanel.active){fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)}'),
		new Function('sender, errorObject','fnb.hyperion.utils.error.show(errorObject)')
	],
	'loadError': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()'),
		new Function('','fnb.hyperion.utils.notifications.hide()'),
		new Function('sender, errorObject','fnb.hyperion.utils.error.show(errorObject)')
	],
	'topMenuLoadComplete': [
		new Function('','fnb.controls.controller.loadUrl("loadSite",_workspace,defaultUrl)')
	],
	'loadUrl': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.controls.controller.clearSubTabObject()'),
		new Function('','fnb.controls.controller.scrollToTop()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.notifications.hide()'),
		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("loadUrl",loadObject.target,loadObject.url,loadObject.preLoadingCallBack,loadObject.postLoadingCallBack,loadObject.params,loadObject.queue,loadObject.expectResponse,loadObject.preventDefaults)')
	],
	'loadUrlLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
    	new Function('sender, loadObj','fnb.hyperion.utils.actionMenu.initNewActionMenu(loadObj)'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'abortLoadUrl': [
		new Function('sender, target','fnb.utils.loadUrl.stop();'),
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'simpleLoadUrl': [
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("simpleLoadUrl",loadObject.target,loadObject.url,loadObject.preLoadingCallBack,loadObject.postLoadingCallBack,loadObject.params,loadObject.queue,loadObject.expectResponse,loadObject.preventDefaults)')
	],
	'simpleLoadUrlLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
    	new Function('sender, loadObj','fnb.hyperion.utils.actionMenu.initNewActionMenu(loadObj)'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'simpleLoadUrlKeepFooter': [
	    new Function('sender, loadObject','fnb.controls.controller.loadUrl("simpleLoadUrlKeepFooter",loadObject.target,loadObject.url,loadObject.preLoadingCallBack,loadObject.postLoadingCallBack,loadObject.params,loadObject.queue,loadObject.expectResponse,loadObject.preventDefaults)')
  	],
  	'simpleLoadUrlKeepFooterLoadUrlComplete': [
  		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
  		new Function('','fnb.hyperion.progress.stop()'),
  		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
  		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
  		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
  		new Function('','fnb.functions.isScrolling.checkPos()')
  	],
	'topTabSelect': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.controls.controller.clearSubTabObject()'),
		new Function('','fnb.controls.controller.scrollToTop()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('','fnb.hyperion.utils.notifications.hide()'),
		new Function('sender, target','_datePicker.hide(false,false)'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("topTabSelect",_workspace,loadObject.url,"","","",loadObject.queue)'),
		new Function('','fnb.forms.tableUtils.init()')
	],
	'topTabSelectLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'loadResultScreen': [
 		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.controls.controller.clearSubTabObject()'),
		new Function('','fnb.controls.controller.scrollToTop()'),
		new Function('sender, errorObject','fnb.hyperion.utils.error.hide()'),
		new Function('','fnb.hyperion.utils.notifications.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.footer.clear()'),
		new Function('','fnb.hyperion.controller.raiseEvent("hideActionMenuButtonAndClear")'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()'),
		new Function('sender, url','fnb.controls.controller.loadUrl("loadResultScreen",_workspace,url,"","","",true)'),
		new Function('','fnb.forms.tableUtils.init()'),
		new Function('','fnb.hyperion.controller.clearPageObjects()')
	],
	'loadResultScreenLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()'),
		new Function('','fnb.hyperion.controller.initHtmlTemplates({})'),
		new Function('','fnb.hyperion.controller.initPageObjects()'),
		new Function('sender, loadObj','fnb.hyperion.utils.actionMenu.initNewActionMenu(loadObj)')
	],
	'loadUrlToWorkspace': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.notifications.hide()'),
		new Function('','fnb.controls.controller.clearSubTabObject()'),
		new Function('sender, errorObject','fnb.hyperion.utils.error.hide()'),
		new Function('','fnb.controls.controller.scrollToTop()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.controller.clearHtmlTemplates()'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("loadUrlToWorkspace",_workspace,loadObject.url,loadObject.preLoadingCallBack)'),
		new Function('','fnb.forms.tableUtils.init()')
	],
	'loadUrlToWorkspaceLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.controller.initHtmlTemplates({})'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()'),
		new Function('sender, loadObj','fnb.hyperion.utils.actionMenu.initNewActionMenu(loadObj)')
	] ,
	'loadUrlToTarget': [
	    new Function('sender, errorObject','fnb.hyperion.utils.error.hide()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("loadUrlToTarget",loadObject.target,loadObject.url,loadObject.preLoadingCallBack,loadObject.postLoadingCallBack,"",loadObject.queue,loadObject.expectResponse)'),
		new Function('','fnb.forms.tableUtils.init()')
	],
	'loadUrlToExpandableRow': [
		new Function('sender, loadObject','fnb.functions.loadUrlToExpandableRow.load(loadObject.target,loadObject.url,loadObject.buttonTarget)'),
		new Function('','fnb.forms.tableUtils.init()')
	],
	'topButtonsLoadUrlToWorkspace': [
	    new Function('sender, errorObject','fnb.hyperion.utils.error.hide()'),
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('','fnb.hyperion.utils.notifications.hide()'),
		new Function('','fnb.hyperion.controller.clearHtmlTemplates()'),
		new Function('','fnb.functions.footer.clear()'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()'),
		new Function('sender,url','fnb.controls.controller.loadUrl("topButtonsLoadUrlToWorkspace",_workspace,url)')
	],
	'topButtonsLoadUrlToWorkspaceLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()'),
		new Function('sender, loadObj','fnb.hyperion.utils.actionMenu.initNewActionMenu(loadObj)')
	],
	'topTabSelectLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'actionMenuloadResultScreen': [
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('sender, url','fnb.controls.controller.loadUrl("actionMenuloadResultScreen",_workspace,url)'),
		new Function('','fnb.controls.controller.setBodyHeight()'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()')
	],
	'actionMenuloadResultScreenLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('sender, loadObj','fnb.hyperion.utils.actionMenu.initNewActionMenu(loadObj)'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.footer.configFooterButtons()'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'doDownload': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('sender, url','fnb.controls.controller.doDownload(url)')
	],
	'openWindow': [
		new Function('sender, url','fnb.controls.controller.openWindow(url)')
	],
	'loadUrlSuccess': [
		new Function('','fnb.functions.slowConnection.hide()'),
		new Function('sender, eventsGroup','fnb.controls.controller.loadUrlComplete(eventsGroup)')
	],
	'actionMenuShowHide': [
		new Function('sender, target','fnb.utils.actionMenu.showHide(target)')
	],
	'actionMenuShow': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('sender, target','fnb.utils.actionMenu.show()')
	],
	'actionMenuHide': [
		new Function('sender, target','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'loadResultScreenFromActionMenu': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.controls.controller.scrollToTop()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("loadResultScreenFromActionMenu",_workspace,loadObject.url,"",loadObject.postLoadingCallBack,"",true)')
	],
	'loadResultScreenFromActionMenuLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('sender, loadObj','fnb.hyperion.utils.actionMenu.initNewActionMenu(loadObj)'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'loadToActionMenu': [
		new Function('','fnb.controls.controller.scrollToTop()'),
		new Function('sender, url','fnb.controls.controller.loadUrl("loadToActionMenu",_actionMenuUrlWrapper,url)')
	],
	'loadToActionMenuLoadUrlComplete': [
		new Function('sender, loadObj','fnb.utils.actionMenu.loadTargetToActionMenuComplete()')
	],
	'dropdownLoadAmounts': [
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("dropdownLoadAmounts",loadObject.target,loadObject.url,"","","",true)')
	],
	'tableMoreOptions': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('sender, errorObject','fnb.hyperion.utils.error.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('sender, url','fnb.controls.controller.loadUrl("tableMoreOptions",_eziProgressWrapperContents,url)'),
		new Function('','fnb.controls.controller.clearBodyHeight()'),
		new Function('','fnb.hyperion.utils.eziPanel.show()'),
		new Function('','fnb.hyperion.progress.startEziLoader()')
	],
	'tableMoreOptionsLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()')
	],
	'eziSliderShow': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('sender, errorObject','fnb.hyperion.utils.error.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('','fnb.controls.controller.clearBodyHeight()'),
		new Function('','fnb.hyperion.utils.eziPanel.show()'),
		new Function('','fnb.hyperion.progress.startEziLoader()'),
		new Function('','fnb.hyperion.utils.notifications.hide()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("eziSliderShow",_eziProgressWrapperContents,loadObject.url,loadObject.preLoadingCallBack,loadObject.postLoadingCallBack,loadObject.params,loadObject.queue,loadObject.expectResponse,loadObject.preventDefaults)')
	],
	'eziSliderShowLoadUrlComplete': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.progress.stop()')
	],
	'eziSliderPaging': [
		new Function('','fnb.hyperion.progress.startEziLoader()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("eziSliderLoad",loadObject.target,loadObject.url,loadObject.preLoadingCallBack,loadObject.postLoadingCallBack,loadObject.params,loadObject.queue,loadObject.expectResponse,loadObject.preventDefaults)')
	],
	'eziSliderLoad': [
		new Function('','fnb.hyperion.progress.start()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("eziSliderLoad",_eziProgressWrapperContents,loadObject.url,loadObject.preLoadingCallBack,loadObject.postLoadingCallBack,loadObject.params,loadObject.queue,loadObject.expectResponse,loadObject.preventDefaults)')
	],
	'eziSliderLoadLoadUrlComplete': [
		new Function('','fnb.hyperion.progress.stop()')
	],
	'eziSliderShowBody': [
		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()'),
		new Function('sender, url','fnb.controls.controller.loadUrl("eziSliderShowBody",_workspace,url)')
	],
	'eziSliderShowBodyLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.functions.footer.clear()'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'eziSliderHide': [
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.controller.raiseEvent("pageHideEzi")')
	],
	'submitFormToEzi': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('sender, errorObject','fnb.hyperion.utils.error.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("submitFormToEzi",_eziProgressWrapperContents,loadObject.url,"","",loadObject.params)'),
		new Function('','fnb.controls.controller.clearBodyHeight()'),
		new Function('','fnb.hyperion.utils.eziPanel.show()'),
		new Function('','fnb.hyperion.progress.startEziLoader()')
	],
	'submitFormToEziLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()')
	],
	'defaultsShow': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()')
	],
	'eziDefaultsShow': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()')
	],
	'reloadDropdown': [
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("reloadDropdown",loadObject.target,loadObject.url,"",loadObject.postLoadingCallBack,"",true)')
	],
	'openDatePicker': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.controls.controller.clearActiveElements()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('sender, target','_datePicker.show(target)')
	],
	'closeDatePicker': [
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('sender, target','_datePicker.hide(false,false)')
	],
	'closeEziDatePicker': [
		new Function('sender, target','_datePicker.hide(false,false)')
	],
	'actionMenuPopupLoadUrl': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('sender, url','fnb.hyperion.utils.notifications.show()'),
		new Function('sender, url','fnb.controls.controller.loadUrl("popupLoadUrl",_popupWrapper,url)')
	],
	'popupLoadUrl': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('','fnb.hyperion.utils.notifications.show()'),
		new Function('sender, url','fnb.controls.controller.loadUrl("popupLoadUrl",_popupWrapper,url)')
	],
	'popupLoadUrlFromActionMenu': [
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('sender, url','fnb.hyperion.utils.notifications.show()'),
		new Function('sender,loadObject','fnb.controls.controller.loadUrl("popupLoadUrlFromActionMenu",_popupWrapper,loadObject.url,"","","","","",loadObject.preventDefaults)')
	],
	'popupClose': [
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('sender, url','fnb.hyperion.utils.notifications.hide()')
	],
	'popupActionMenuClose': [
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('sender, url','fnb.hyperion.utils.notifications.hide()')
	],
	'subTabLoadUrl': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('sender, url','fnb.controls.controller.loadUrl("subTabLoadUrl",_workspace,url)'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()')
	],
	'subTabLoadUrlLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()')
	],
	'loadUrltoPrintDiv': [
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("loadUrltoPrintDiv",_printDiv,loadObject.url,"",loadObject.postLoadingCallBack)')
	],
	'otpShow': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('sender, errorObject','fnb.hyperion.utils.error.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("otpShow",_eziProgressWrapperContents,loadObject.url,"","",loadObject.params)'),
		new Function('','fnb.controls.controller.clearBodyHeight()'),
		new Function('','fnb.hyperion.utils.eziPanel.show()'),
		new Function('','fnb.hyperion.progress.startEziLoader()')
	],
	'otpShowLoadUrlComplete': [
		new Function('','fnb.hyperion.controller.raiseEvent("hideOverlay")'),
		new Function('','fnb.hyperion.progress.stop()'),
		new Function('sender, params','fnb.utils.otp.complete()')
	],
	'loadResultScreenToHiddenDiv': [
		new Function('sender, url','fnb.controls.controller.loadUrl("loadResultScreenToHiddenDiv",_hiddenDiv,url)')
	],
 	'loadFormToFrame': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('sender, loadObject','fnb.controls.controller.loadFormToFrame("loadUrl",loadObject)'),
		new Function('','fnb.hyperion.controller.raiseEvent("showOverlay")'),
		new Function('','fnb.hyperion.progress.start()')
	],
	'timedOut': [
		new Function('','fnb.functions.isScrolling.hide()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()'),
		new Function('','fnb.hyperion.utils.notifications.hide()')
	],
	'keepSessionAlive': [
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()'),
		new Function('','fnb.functions.isScrolling.checkPos()'),
		new Function('sender, loadObject','fnb.controls.controller.loadUrl("keepSessionAlive",loadObject.target,loadObject.url,"","","","",loadObject.expectResponse)')
	],
	'mobileShowDefaults': [
		new Function('','fnb.controls.controller.setBodyHeight()'),
		new Function('','fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.functions.isScrolling.checkPos()'),
		new Function('','fnb.hyperion.utils.actionMenu.showButton()')
	],
	'mobileHideDefaults': [
		new Function('','fnb.controls.controller.clearSubTabObject()'),
		new Function('','fnb.controls.controller.scrollToTop()'),
		new Function('','fnb.controls.controller.clearBodyHeight()'),
		new Function('','fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup)'),
		new Function('','fnb.hyperion.utils.eziPanel.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hide()'),
		new Function('','fnb.hyperion.utils.actionMenu.hideButton()')
	]
};
