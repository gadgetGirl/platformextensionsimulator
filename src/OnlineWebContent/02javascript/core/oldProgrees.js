var _progress = function () {
	var progressPosition = 0;
	var loadingTrigger;
	var perchex=255;
	var loaderEnd;
	var loaderInstance = 0;
	var loadTime;
	var loaderID;
	return  {
		init: function (target,id) {
			this.isIE8 = this.isIe8();
			if(this.progressPosition>0) this.reset();
			this.loaderID = id;
			this.loaderEnd = Math.floor(Math.random() * (95-60+1)) + 60;
			this.progressPosition = 0;
			this.loadTime=40;
			this.perchex=255;
			this.loaderInstance = 0;
			this.append(target);
			this.createTrigger();
			this.loadTime=40;
			this.loaderIncrements=1;
			this.perValueObj;
			this.progressWrapperObj;
			this.loadTextObj;
			this.progressBarObj
		},
		createTrigger: function () {
			if(this.progressPosition == 0){
				if(this.loadingTrigger!= null) clearInterval (this.loadingTrigger);
				if(document.getElementById('formFooterButtons') != null)
				{
					var footerwrapper =document.getElementById('formFooterButtons')
					footerwrapper.className += " hideElement";
					if(document.getElementById('footerMessage') != null)
					{
						document.getElementById('footerMessage').className = "footerMessage hideElement";
					}
				}
				this.loadingTrigger = setInterval (function ()
				{
					_progress.progress(_progress.progressPosition+_progress.loaderIncrements,false);
				}, _progress.loadTime);
			}
		},
		progress: function (progress, terminate) {

			_progress.perValueObj = document.getElementById('percValue'+this.loaderID);
			_progress.progressWrapperObj = document.getElementById('progressWrapper'+this.loaderID);
			_progress.loadTextObj = document.getElementById('loadText'+this.loaderID);
			_progress.progressBarObj = document.getElementById('progressBar'+this.loaderID);

			if (_progress.progressWrapperObj != null){
				if(progress==_progress.loaderIncrements){
					_progress.loaderInstance++;
					var overlayExpanded = false;
					if(typeof _overlay != 'undefined') overlayExpanded = _overlay.overlayExpanded;
					if(typeof _overlay != 'undefined'&&_progress.loaderID=='Main'&&overlayExpanded==false&&_progress.loaderInstance>1){
						_overlay.show();
						_overlay.overlayWrapper.css({'left':'40px','top':_topOffset+'px','z-index':'5'});
						_overlay.animate();
					}
					_progress.loadTime = 40;
					_progress.loaderEnd = Math.floor(Math.random() * (95-60+1)) + 60;
					if(_progress.isIE8==true){
						_progress.loaderIncrements = Math.floor(_progress.loaderEnd/3); 
						_progress.loaderEnd = _progress.loaderIncrements*3;
						_progress.loadTime = 200;
					}
					clearInterval (_progress.loadingTrigger);
					_progress.createTrigger();
					_progress.perchex = 0;

					_progress.perValueObj.style.opacity = 0;
					_progress.progressWrapperObj.style.width = 100 +'%';
					_progress.loadTextObj.innerHTML = 'Loading';
				}

				if(progress<=_progress.loaderEnd){
					_progress.progressPosition = Math.round(progress);
					_progress.perValueObj.innerHTML = _progress.progressPosition +'%';
					_progress.progressBarObj.style.width = _progress.progressPosition +'%';
					if(_progress.isIE8==true) _progress.perValueObj.style.opacity = 1;
					if(progress>12&&_progress.isIE8==false){
						_progress.perchex += 0.05;
						if (_progress.perchex <= 1) {
							_progress.perValueObj.style.opacity = _progress.perchex;
						}
					}
				}

				if(terminate==true){
					_progress.remove()
				}

			}else{
				clearInterval(_progress.loadingTrigger);
			}
		},
		append: function (target) {
			
			var progressWrapper = document.createElement("div");
			progressWrapper.setAttribute('id', 'progressWrapper'+this.loaderID);
			progressWrapper.setAttribute('class', 'progressWrapper'+this.loaderID+' progressWrapperMainInstance1');

			var progressBackGround = document.createElement("div");
			progressBackGround.setAttribute('id', 'progressBackGround'+this.loaderID);
			progressBackGround.setAttribute('class', 'progressBackGround');

			var loadTextVal = document.createTextNode("Loading");

			var loadText = document.createElement("div");
			loadText.setAttribute('id', 'loadText'+this.loaderID);
			loadText.setAttribute('class', 'loadText');
			loadText.appendChild(loadTextVal);

			var percText = document.createTextNode("0%");

			var percValue = document.createElement("div");
			percValue.setAttribute('id', 'percValue'+this.loaderID);
			percValue.setAttribute('class', 'percValue');
			percValue.appendChild(percText);

			var progressBar = document.createElement("div");
			progressBar.setAttribute('id', 'progressBar'+this.loaderID);
			progressBar.setAttribute('class', 'progressBar');

			progressBar.appendChild(loadText);
			progressBar.appendChild(percValue);

			progressWrapper.appendChild(progressBackGround);
			progressWrapper.appendChild(progressBar);
			target.appendChild(progressWrapper);
		},
		reset: function () {
			clearInterval (this.loadingTrigger);
			this.progressWrapperObj.style.width = 0;
			this.loadTextObj.innerHTML = '';
			this.progressBarObj.style.opacity = 0;
			this.progressBarObj.innerHTML = '';
			this.loadingTrigger = undefined;
			this.progressPosition = 0;
		},
		remove: function () {
			this.reset();
			if(_progress.loaderInstance>1&&_progress.loaderID=='Main'){
				
			}else{
				if(typeof(_nav)!="undefined"){
					var sliderPos = $(_nav.selectedTab).width()* (_nav.topTabSelectedIndex);
					$(_topNavIndicator).animate({left:sliderPos}, {duration: 300, 	easing: 'swing'}); 
				}
			}
			this.progress(0, false); 
		},
		isIe8: function () {
			return !!( (/msie 8./i).test(navigator.appVersion) && !(/opera/i).test(navigator.userAgent) && window.ActiveXObject && XDomainRequest && !window.msPerformance );
		}
	};
}();

