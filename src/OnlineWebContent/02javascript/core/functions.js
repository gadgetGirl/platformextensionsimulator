
///-------------------------------///
/// developer: Donovan
///
/// First page Loaded
///-------------------------------///
$(function(){
	function siteLoadComplete(){

	};
	siteLoadComplete.prototype = {
		complete: function(){
			$('html').addClass('htmlBackGround');
			$(_header).removeClass('visibilityHidden');
			$(_pageContainer).removeClass('visibilityHidden');
			$('#footerWrapperNew').removeClass('visibilityHidden');
			progressBar.progressClass = " progressWrapperStandard";
			fnb.controls.controller.setSkin();
			
		}
	};
	namespace("fnb.functions.siteLoadComplete",siteLoadComplete);
});
$(function(){
	function logonHandler(){

	};
	logonHandler.prototype = {
		loggedOn: function(){
			
			var _leftSideBar = $(_topMenu).find('.left-sidebar-top');	
			if($(_topNav).hasClass('hideTopNav')){$(_topNav).removeClass('hideTopNav')};
			if(_leftSideBar.hasClass('minimalTopMenuAdjust')){_leftSideBar.removeClass('minimalTopMenuAdjust')};
		}
	};
	namespace("fnb.functions.logonHandler",logonHandler);
});

///-------------------------------///
/// developer: Donovan
///
/// SHOW HIDE TOGGLE
///-------------------------------///
$(function(){
	function showHideToggleElements(){
		this.elements = [];
		this.parentFunction = this;
	};
	showHideToggleElements.prototype = {
			showHideToggle: function(elements){
				this.elements = $.makeArray(elements);
				var main = this.parentFunction;
				$.each(this.elements, function(elementsIndex, elementObject) {
					 switch(elementObject.show){
				         case 'true':
				        	 main.showElement(elementObject);
				        	 break;
				         case 'false':
				        	 main.hideElement(elementObject);
				        	 break;
				         case 'toggle':
				        	 main.toggleElement(elementObject);
				        	 break;	
					 }    
				 })
			},
			showElement: function(object){
				if(object.animation){
					this.animateElement('show',object);
	        	}else{
	        		$(object.element).show();
	        	}
			},
			hideElement: function(object){				
				if(object.animation){
					this.animateElement('hide',object);
		       	}else{
		       		$(object.element).hide();
		       	}
			},
			toggleElement: function(object){
				$(object.element).toggle();
			}
	};
	namespace("fnb.functions.showHideToggleElements",showHideToggleElements);
	
});
///-------------------------------///
/// developer: Donovan
///
/// BRANCH SEARCH
///-------------------------------///
$(function(){
	function branchSearch(){
		
		this.lowercodes = [];
		this.bankNames = [];
		this.nums = [];
		this.xRefCodes = [];
		
		this.bankList = [];
		this.bankOptionsList = [];
		this.bankOptionsDropdownList = [];
		
		this.parent;
		this.parentWrapper;
		
		this.focusFlag = true;
		this.error = true;
		
		this.dropDownItemSearch;
		
		this.selectedCountryCode;
		this.selectedDropdownItem;
		this.selectedDropdownValue;
		
		this.country = '';
		
	};
	branchSearch.prototype = {
			initialize: function(lowerCodes,bankNames,nums, xRefCodes, country){
				this.lowercodes = lowerCodes.substring(1, lowerCodes.length - 1).split(',');
				this.bankNames = bankNames.substring(1, bankNames.length - 1).split(',');
				this.nums = nums.substring(1, nums.length - 1).split(',');
				this.xRefCodes = xRefCodes.substring(1, xRefCodes.length - 1).split(', ');
				this.country = country;
			},
			appendBanksList: function(countryCode,returnValue,displayValue){
				this.bankList.push({countryCode:countryCode, returnValue:returnValue, displayValue:displayValue});
			},
			appendBankOptionsList: function(bankId,returnType,returnValue,countryCode){
				this.bankOptionsList.push({bankId:bankId, returnType:returnType, returnValue:returnValue, countryCode:countryCode});
			},
			appendBankOptionsDropdownsList: function(bankId,countryCode,displayValue,returnValue){
				this.bankOptionsDropdownList.push({bankId:bankId, countryCode:countryCode, displayValue:displayValue, returnValue:returnValue});
			},
			selectCountry: function(selection){

				this.dropDownItemSearch = $(selection).parent().find('.dropdown-content-top');
				this.selectedDropdownItem = selection;
				this.parent = $(this.selectedDropdownItem).parents(".dropDown");
				this.selectedDropdownValue = $(this.selectedDropdownItem).attr('data-value');
				
				$('#dynamicInputElement').parent().parent().parent().hide();
				$('#dynamicDropdownElement_parent').hide();
					
				this.clearDropdown($('#selectBankDropdown_parent').find('.dropdown-content'));

				var selectedAvailableBanks = [];

				selectedAvailableBanks = $.grep(this.bankList, function(option, index) {
			        return ( option.countryCode == fnb.functions.branchSearch.selectedDropdownValue );
			    });

				var optionsDiv = $('#selectBankDropdown_parent').find('.dropdown-content');

				$(this.dropDownItemSearch).appendTo($(optionsDiv));

				$.each(selectedAvailableBanks, function(index, item) {
					var clonedDropdownItem = $(fnb.functions.branchSearch.selectedDropdownItem).clone();
					$(clonedDropdownItem).attr('data-value',item.returnValue);
					$(clonedDropdownItem).find('.dropdown-item-row').html(item.displayValue);
					$(clonedDropdownItem).attr('onclick','fnb.functions.branchSearch.selectBank(this)').bind('click');
					$(clonedDropdownItem).appendTo($(optionsDiv));
			    }); 

			},
			selectBank: function(selection){

				this.selectedDropdownItem = selection;
				this.selectedDropdownValue = $(this.selectedDropdownItem).attr('data-value');
				this.selectedCountryCode = $('#selectCountryDropdown').attr('value');

				var selectedBranches = [];

				selectedBranches = $.grep(this.bankOptionsList, function(option, index) {
			        return ( option.countryCode == fnb.functions.branchSearch.selectedCountryCode && option.bankId == fnb.functions.branchSearch.selectedDropdownValue);
			    });
				
				if(selectedBranches.length>0){
					switch(selectedBranches[0].returnType)
			        {
			          	case '0':
			          		var filteredBranches = [];
							filteredBranches = $.grep(this.bankOptionsDropdownList, function(option, index) {
						        return ( option.countryCode == fnb.functions.branchSearch.selectedCountryCode && option.bankId == fnb.functions.branchSearch.selectedDropdownValue);
						    });

							$('#dynamicInputElement').parent().parent().parent().hide();
							$('#dynamicDropdownElement_parent').show();
				
							this.clearDropdown($('#dynamicDropdownElement_parent').find('.dropdown-content'));
							
							var optionsDiv = $('#dynamicDropdownElement_parent').find('.dropdown-content');

							$.each(filteredBranches, function(index, item) {
								var clonedDropdownItem = $(fnb.functions.branchSearch.selectedDropdownItem).clone();
								$(clonedDropdownItem).attr('data-value',item.returnValue);
								$(clonedDropdownItem).find('.dropdown-item-row').html(item.displayValue);
								$(clonedDropdownItem).attr('onclick','fnb.functions.branchSearch.returnBranchCode("'+item.returnValue+'", "'+item.displayValue+'")').bind('click');
								$(clonedDropdownItem).appendTo($(optionsDiv));
						    });
				        break;
				        case '1':
				        	fnb.functions.branchSearch.returnBranchCode(selectedBranches[0].returnValue,$(this.selectedDropdownItem).find('h3').text());
					    break;
					}	
				}else{
					if (this.selectedDropdownValue!=0) {
						$('#dynamicInputElement').parent().parent().parent().show();
						$('#dynamicDropdownElement_parent').hide();
					}
					else {
						$('#dynamicInputElement').parent().parent().parent().hide();
						$('#dynamicDropdownElement_parent').hide();
						
					}
				}
			},
			returnBranchCode: function(returnValue, displayValue){
				this.focusFlag = true;
				
				$('#branchSearchCode').val(returnValue);
				
				if(displayValue!=""){
					$('#branchSearchName').val(displayValue);
					$('#branchSearchName').parent().attr('style','');
					$('#BranchSearchNameContainer').addClass('input disabled');
					$('#BranchSearchNameContainer').children(':first-child').addClass('inputColor turqDarkBg roundedLeft');
				}else{
					fnb.functions.branchSearch.findName(returnValue);
				}
				
				$('#dynamicInputElement').parent().parent().parent().hide();
				$('#dynamicDropdownElement_parent').hide();
				
				fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '');
				
				fnb.hyperion.controller.bodyElement.attr('data-clipOverflow', 'false');
			},
			inputKeypress: function(input,event){
				
				fnb.functions.branchSearch.parentWrapper = $(event.target).parent().parent().parent().parent();
				
				var inputVal = $(input).val();
				var subInputVal = '';
				var inputCount = $(input).val().length;
				if (!isNaN(inputVal)) {
					inputVal = parseInt($(input).val());
				}
				else {
					if ((typeof inputVal != 'undefined')&&inputCount>=4) subInputVal=inputVal.substring(0,4);
				}
				
				var bankName = 'Waiting branch code';
				
					
				if(this.error = true){
					fnb.functions.branchSearch.parentWrapper.removeClass('formElement error').addClass('formElement');
					this.error = false;
				}
				if(event.keyCode == 13){
					if(inputCount<6){
						$('#branchSearchName').val('');
					}
					else if ((fnb.functions.branchSearch.country==83)&&(inputCount == 11)) {
							$.each(fnb.functions.branchSearch.xRefCodes, function(index, item) {
									if (subInputVal == fnb.functions.branchSearch.xRefCodes[index]){
										bankName = fnb.functions.branchSearch.bankNames[index];
									}
							});
							$('#branchSearchName').val(bankName);
					}else{
						$.each(fnb.functions.branchSearch.lowercodes, function(index, item) {
							var previousIndex = 0;
							if(index>0) previousIndex = parseInt(fnb.functions.branchSearch.lowercodes[index-1]);

							if(inputVal>previousIndex && inputVal<parseInt(item)){
								bankName = fnb.functions.branchSearch.bankNames[index-1];
							}
						});
						$('#branchSearchName').val(bankName);
					}
				}
				var selectionStart = $(input).get(0).selectionStart;
				var selectionEnd = $(input).get(0).selectionEnd;
				
				var c = event.keyCode;
			    var ctrlDown = event.ctrlKey||event.metaKey; // Mac support
			    // Check for Alt+Gr
			    if (ctrlDown && event.altKey) return false;
			    // Check for ctrl+c, v and x
			    else if (ctrlDown && c==67) return true; // c
			    else if (ctrlDown && c==86) return true; // v
			    else if (ctrlDown && c==88) return true; // x
			    else if (ctrlDown && c==90) return true; // z
			    else if((((fnb.functions.branchSearch.country==83)&&inputCount<=12)||((fnb.functions.branchSearch.country!=83&&inputCount<=7))) || event.keyCode == 8 || event.keyCode == 9){
					if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 9 || event.keyCode == 32)				
					{
						return true;
					}
					else
					if((fnb.functions.branchSearch.country!=83)&&(event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ))
					{
							return false;	
					}else{
						if(((fnb.functions.branchSearch.country==83)&&(inputCount<11))||((inputCount<6)&&(fnb.functions.branchSearch.country!=83))){
							return true;
						}else{
							return false;
						}
					}
				}else{
					return false;
				}
			},
			inputBlur: function(){
				if(fnb.functions.branchSearch.focusFlag){
					var inputVal = $('#branchSearchCode').val();
					fnb.functions.branchSearch.findName(inputVal);
				}
			},
			findName: function(returnValue){
				var inputVal = returnValue;
				var subInputVal = '';
				if (!isNaN(inputVal)) {
					inputVal = parseFloat(returnValue);
				}
				else {
					if ((typeof inputVal != 'undefined')&&inputVal.length>=4) subInputVal=returnValue.substring(0,4);
				}
				
				var inputCount = returnValue.length;
				var bankName = 'Waiting branch code';
				var found = false;
				var lastIndex = 0;
				var previousValue = 0;
				
				if(inputCount<6){
					this.error = true;
					$('#branchSearchName').val(bankName);
				}else{
					if (inputCount == 11) {
						$.each(fnb.functions.branchSearch.xRefCodes, function(index, item) {
								if (subInputVal == fnb.functions.branchSearch.xRefCodes[index]){
									bankName = fnb.functions.branchSearch.bankNames[index];
								}
						});
						$('#branchSearchName').val(bankName);					
					}
					else {
						$.each(fnb.functions.branchSearch.lowercodes, function(index, item) {
							var previousIndex = 0;
							if(index>0) previousIndex = parseInt(fnb.functions.branchSearch.lowercodes[index-1]);
							previousValue = previousIndex;
							lastIndex = index; 
							if(inputVal>=previousIndex && inputVal<parseFloat(item)){
								bankName = fnb.functions.branchSearch.bankNames[index-1];
								found = true;
							}
						});
						if ((!found) && (inputVal >= previousValue)) {
							bankName = fnb.functions.branchSearch.bankNames[lastIndex];
						}
						$('#branchSearchName').val(bankName);
					}

				}
			},
			clearDropdown: function(dropdownID){
				$(dropdownID).children().remove();
			}
	};

	namespace("fnb.functions.branchSearch",branchSearch);
});

///-------------------------------///
/// developer: Donovan
///
/// ADD NOTIFICATIONS
///-------------------------------///
$(function(){
	function notifications(){
		
	};
	notifications.prototype = {
		initialize: function(parent,row,max,emailLimit,smsLimit,faxLimit){
			this.parent = parent;
			this.row = row;
			this.max = max;
			this.emailLimit = emailLimit;
			this.emailIsMax = false;
			this.smsLimit = smsLimit;
			this.smsIsMax = false;
			this.faxLimit = faxLimit;
			this.faxIsMax = false;
			this.row.find('.phoneCode').attr({
				id: 'methodCodeInput0',
				name: 'methodCodeInput0'
			});
		},
		setOptions: function(row,option,valObj){
			var methodInputWrapper = row.find('.methodInputWrapper');
			var methodInputInnerContainer = methodInputWrapper.find('.input-container-inner');
			var firstAvailableDropdownItemVal = row.find('li:eq('+option+')').attr('data-value');
			row.attr('data-option',option);
			var code = valObj ? valObj.code : 'Code';
			var method =valObj ? valObj.method : 'Number';
			var subject = valObj ? valObj.subject : 'Proof of Payment';
			switch(option)
			{
				case 0:
					var updatedMethod = (method=='Number' ? '' : method);
					methodInputInnerContainer.removeClass('phoneNumberContainer').find('.phoneCodeContainer').addClass('hidden').val(code);
					methodInputInnerContainer.find('input:eq(1)').removeClass('phoneNumber').removeClass('phoneEmpty numberOnly').val(updatedMethod).removeAttr('maxlength','10');
					row.find('.subjectInputWrapper').removeClass('hidden').find('input').val(subject);
					
				break;
				case 1:
				case 2:
					methodInputInnerContainer.addClass('phoneNumberContainer').find('.phoneCodeContainer').removeClass('hidden');
					methodInputInnerContainer.find('input:eq(0)').val(code);
					methodInputInnerContainer.find('input:eq(1)').addClass('phoneNumber numberOnly').val(method).attr('maxlength','10');
					if(code == 'Code'){methodInputInnerContainer.find('input:eq(0)').addClass('phoneEmpty');}else{methodInputInnerContainer.find('input:eq(0)').removeClass('phoneEmpty');}
					if(method == 'Number') methodInputInnerContainer.find('input:eq(1)').addClass('phoneNumber').addClass('phoneEmpty');
					row.find('.subjectInputWrapper').addClass('hidden');
					row.find('.subjectInputWrapper').val();
				break;
			}

			methodInputWrapper.find('.formElementLabel').html(firstAvailableDropdownItemVal);
			row.find('.dropdown-hidden-input').val(firstAvailableDropdownItemVal);
			row.find('.dropdown-selection-white').find('.singleLine').text(firstAvailableDropdownItemVal);
		},
		dropdownSelect: function(option){
			this.setOptions($(option).closest('.addRecipientRowParent'),$(option).index());
		},
		cloneRow: function(valObj,index){
			var doClone = true;
			if(this.emailIsMax==true&&this.smsIsMax==true&&this.faxIsMax==true) doClone = false;
			if(doClone==true){
				var selectedIndex = index;
				this.clonedRow = this.row.clone();
				this.clonedRow.find('div[id^="addRecipientsRemoveButton"]').removeClass('hidden');
				this.parent.children().last().before(this.clonedRow);
				this.resetRowAttr();
				if(typeof selectedIndex=='undefined') selectedIndex = this.clonedRow.find('li:not(".hidden"):eq(0)').index();
				this.setOptions(this.clonedRow,selectedIndex,valObj);
				this.checkDropdowns();
			}
		},
		removeRow: function(obj){
			var removeRow = $(obj).closest('#addRecipientRowParent');
			removeRow.remove();
			this.resetRowAttr();
			this.checkDropdowns();
		},
		checkDropdowns: function(){
			var emailCount = this.parent.children('div[data-option="0"]').length;
			var phoneCount = this.parent.children('div[data-option="1"]').length;
			var faxCount = this.parent.children('div[data-option="2"]').length;
			if(emailCount>this.emailLimit&&this.emailIsMax==false){
				this.emailIsMax=true;
				var val = 'Email address';
				this.showHideDropDownOption(false,val)
			}else if(emailCount<=this.emailLimit&&this.emailIsMax==true){
				this.emailIsMax=false;
				var val = 'Email address';
				this.showHideDropDownOption(true,val)
			}
			if(phoneCount>this.smsLimit&&this.smsIsMax==false){
				this.smsIsMax=true;
				var val = 'Cell number';
				this.showHideDropDownOption(false,val)
			}else if(phoneCount<=this.smsLimit&&this.smsIsMax==true){
				this.smsIsMax=false;
				var val = 'Cell number';
				this.showHideDropDownOption(true,val)
			}
			if(faxCount>this.faxLimit&&this.faxIsMax==false){
				this.faxIsMax=true;
				var val = 'Fax number';
				this.showHideDropDownOption(false,val)
			}else if(faxCount<=this.faxLimit&&this.faxIsMax==true){
				this.faxIsMax=false;
				var val = 'Fax number';
				this.showHideDropDownOption(true,val)
			}
		},
		showHideDropDownOption: function(visible,label){
			this.parent.find('.addRecipientRowParent').each(function(){
				var parentObject = $(this).find('.dropdown-content');
				var filter = "> li div[class*='dropdown-h']:icontains('" + label + "')";
				if(visible==true){
					$(filter, parentObject).parent().parent().removeClass('hidden');
				}else{
					$(filter, parentObject).parent().parent().addClass('hidden');
				}
			});
		},
		resetRowAttr: function(){
			this.parent.find('.addRecipientRowParent').each(function(rowIndex,row){
				var currentRow = $(row);
				var selectedType = currentRow.find('input[id^="selectedTypeInput"]');
				var selectedCode =currentRow.find('input[id^="methodCodeInput"]');
				var selectedMethod =currentRow.find('input[id^="methodContactInput"]');
				var selectedSubject =currentRow.find('input[id^="subjectInput"]');
				selectedType.attr({
					id: 'selectedTypeInput'+rowIndex,
					name: 'selectedTypeInput'+rowIndex
				});
				selectedCode.attr({
					id: 'methodCodeInput'+rowIndex,
					name: 'methodCodeInput'+rowIndex
				});
				selectedMethod.attr({
					id: 'methodContactInput'+rowIndex,
					name: 'methodContactInput'+rowIndex
				});
				selectedSubject.attr({
					id: 'subjectInput'+rowIndex,
					name: 'subjectInput'+rowIndex
				});
			});
		}
	};

	namespace("fnb.functions.notifications",notifications);
});
///-------------------------------///
/// developer: Donovan
///
/// Unhider
///-------------------------------///
$(function(){
	function unhider(){

	};
	unhider.prototype = {
		unhide: function(id, what, direction, menu, old_item){
			calculateDimensions();
			if (windowWidth < 760) {

				if (menu == 'hide') {
					$('#topBar').hide();
					$('#headerTabs').hide();
					$('#submenu').hide();
				}

			}

			$('html').css('overflow', 'hidden');

			if (typeof (old_item) != "undefined") {
				whichDivCurrent = $(old_item).attr('id');
			} else {
				whichDivCurrent = $('.pageGroup:visible').attr('id');
			}

			$(what).show();

			if (direction == "left") {
				$(what).css('position', 'absolute');
				$(what).css('left', '100%');

				$('#' + whichDivCurrent).animate({
					left : '-=100%'
				}, 500, function() {
					$('#' + whichDivCurrent).css('display', 'none');
					$('html').css('overflow', 'auto');
				});

				$(what).animate({
					left : '-=100%'
				}, 500, function() {

				});
			}

			else if (direction == "right") {

				$(what).css('position', 'absolute');
				$(what).css('left', '-100%');
				$('#' + whichDivCurrent).animate({
					left : '+=100%'
				}, 500, function() {
					$('#' + whichDivCurrent).css('display', 'none');
					$('html').css('overflow', 'auto');
				});

				$(what).animate({
					left : '+=100%'
				}, 500, function() {
					// Animation complete.
				});

			}

			else if (direction == "down") {
				// set the start position of the unhidden iSroll
				$(what).css('position', 'absolute');
				$('#' + whichDivCurrent).css('position', 'absolute');
				$(what).css('top', '-100%');

				$('#' + whichDivCurrent).animate({
					top : '+=100%'
				}, 500, function() {
					// Animation complete.
					$('#' + whichDivCurrent).css('display', 'none');
					$('html').css('overflow', 'auto');

				});

				$(what).animate({
					top : '+=100%'
				}, 500, function() {
					// Animation complete.
					$(what).css('height', 'auto');
					$(what).css('overflow', 'auto');
					if (windowWidth < 760) {
						$(what).css('position', 'static');

						if (menu == 'show') {
							$('#topBar').show();
							$('#headerTabs').show();
							$('#submenu').show();
						}

					}

				});
			}

			else if (direction == "up") {
				// set the start position of the unhidden iSroll
				$(what).css('position', 'absolute');
				$('#' + whichDivCurrent).css('position', 'absolute');
				$(what).css('top', '100%');

				$('#' + whichDivCurrent).animate({
					top : '-=100%'
				}, 500, function() {
					// Animation complete.
					$('#' + whichDivCurrent).css('display', 'none');
					$('html').css('overflow', 'auto');

				});

				$(what).animate({
					top : '-=100%'
				}, 500, function() {
					// Animation complete.
					$(what).css('height', 'auto');
					$(what).css('overflow', 'auto');
					if (windowWidth < 760) {
						$(what).css('position', 'static');

						if (menu == 'show') {
							$('#topBar').show();
							$('#headerTabs').show();
							$('#submenu').show();
						}

					}
				});
			}

			// function to handle sub tabs
			// $(subtab).css('display','block');
			// $('li.'+subtab).css('background','#006666');

			$(what).show();
			return false;
		}
	};
	namespace("fnb.functions.unhider",unhider);
});

///-------------------------------///
/// developer: Donovan
///
/// ReloadDropdown
///-------------------------------///
$(function(){
	function reloadDropdown(){

	};
	reloadDropdown.prototype = {
		reload: function(dropdown,targetDiv,targetUrl,fieldName){
		
			preTargetUrl = targetUrl;
			preTarget = $(targetDiv);
			if(preTarget.length==0) preTarget = $('#'+targetDiv);
			var dropDownValue =$(dropdown).attr('data-value');
			
			preTargetOnClick = $(preTarget).parent().parent().find('.dropdown-item').attr('onclick');

			var paramName = "";
			if (fieldName && (fieldName != "")){
				paramName = fieldName;
			}else{
				paramName = "id";
			}
			if(preTargetUrl.indexOf("?")>0){
				preTargetUrl += "&" +paramName+ "=" + dropDownValue;
			}else{
				preTargetUrl += "?" +paramName+ "=" + dropDownValue;
			}
			var targetParent = $(preTarget).parent().parent().parent();
			var postLoadingCallBackFunction;
			if(targetParent){
				postLoadingCallBackFunction = function(){
					targetParent.find('li').attr('onclick',preTargetOnClick);
				}
			}
			var loadObj = {url: preTargetUrl,target:targetParent,postLoadingCallBack:postLoadingCallBackFunction};
			fnb.controls.controller.eventsObject.raiseEvent('reloadDropdown', loadObj);
		}
	};
	namespace("fnb.functions.reloadDropdown",reloadDropdown);
});
///-------------------------------///
/// developer: Donovan
///
/// Load Dropdown To Div
///-------------------------------///
$(function(){
	function loadDropDownDiv(){

	};
	loadDropDownDiv.prototype = {
		load: function(url, target, options){
			var postLoadCallback;
			if(options){
				postLoadCallback = options["afterCallback"];
			}
			var loadObj = {url: url,target:'#'+target,postLoadingCallBack:postLoadCallback};
			fnb.controls.controller.eventsObject.raiseEvent('reloadDropdown', loadObj);
		}
	};
	namespace("fnb.functions.loadDropDownDiv",loadDropDownDiv);
});
///-------------------------------///
/// developer: Donovan
///
/// Load URL to Print div
///-------------------------------///
$(function(){
	function loadUrlToPrintDiv(){

	};
	loadUrlToPrintDiv.prototype = {
		load: function(url){
			var postLoadCallback = function (){
				var WindowObject = window.open('/banking/printPage.jsp');
			};
					
			var loadObj = {url: url,target: _printDiv,postLoadingCallBack:postLoadCallback};
			fnb.controls.controller.eventsObject.raiseEvent('loadUrltoPrintDiv', loadObj);
		},
		processData: function(data){
			console.log(data)
		},
		getData: function(){
			return $(_printDivWrapper).html();
		}
	};
	namespace("fnb.functions.loadUrlToPrintDiv",loadUrlToPrintDiv);
});
///-------------------------------///
/// developer: Donovan
///
/// Submit Form To Workspacete
///-------------------------------///
$(function(){
	function submitFormToWorkspace(){

	};
	submitFormToWorkspace.prototype = {
		submit: function(formName, optionalFunction,buttonTarget, extraOptions,preventDefaults){
					
			$.ajaxSettings.data = [];
			fnb.controls.controller.submitForm(formName, _workspace, '', '', buttonTarget, extraOptions,preventDefaults);
		}
	};
	namespace("fnb.functions.submitFormToWorkspace",submitFormToWorkspace);
});

$(function(){
	function submitFormToDiv(){

	};
	submitFormToDiv.prototype = {
		submit: function(formName, targetDiv, optionalFunction,buttonTarget, extraOptions,preventDefaults){
			if(!extraOptions) var extraOptions = {};
			extraOptions.keepFooter = true;
			
			fnb.controls.controller.submitForm(formName, targetDiv, '', '', buttonTarget, extraOptions,preventDefaults);
		}
	};
	namespace("fnb.functions.submitFormToDiv",submitFormToDiv);
});
///-------------------------------///
/// developer: Edzard
///
/// Submit Form To EzPa
///-------------------------------///
$(function(){
	function submitFormToEziPanel(){

	};
	submitFormToEziPanel.prototype = {
		submit: function(formName, paramName, paramValue, optionalFunction,buttonTarget, extraOptions){
			fnb.controls.controller.submitFormToEzi(formName, paramName, paramValue, _eziWrapper, '', '', buttonTarget, extraOptions);
		}
	};
	namespace("fnb.functions.submitFormToEziPanel",submitFormToEziPanel);
});

$(function(){
	function submitFormToTargetDiv(){

	};
	submitFormToTargetDiv.prototype = {
		submit: function(formName, paramName, paramValue,targetDiv, optionalFunction,buttonTarget, extraOptions){
			fnb.controls.controller.submitFormToTargetDiv(formName, paramName, paramValue, targetDiv, '', '', buttonTarget, extraOptions);
		}
	};
	namespace("fnb.functions.submitFormToTargetDiv",submitFormToTargetDiv);
});

$(function(){
	function loadUrlToExpandableRow(){
	};
	loadUrlToExpandableRow.prototype = {
		load: function(targetDiv,url,caller){
						
			if($(caller).data('state') == 'open'){
			$(targetDiv).html('');
			$(caller).data('state','closed');
			$(caller).val($(caller).data('originalLabel'))
			}else{
			$(caller).data('originalLabel',$(caller).val())
			fnb.controls.controller.loadUrl("loadUrlToExpandableRow",targetDiv,url)
			$(caller).val('Close');
			$(caller).data('state','open');
			}
		}
	};
	namespace("fnb.functions.loadUrlToExpandableRow",loadUrlToExpandableRow);
});

$(function(){
	function submitFormFromEziToTarget(){

	};
	submitFormFromEziToTarget.prototype = {
		submit: function(formName, paramName, paramValue, targetDiv, optionalFunction,buttonTarget, extraOptions){
			fnb.controls.controller.submitFormFromEziToTarget(formName, paramName, paramValue, targetDiv, '', '', buttonTarget, extraOptions);
		}
	};
	namespace("fnb.functions.submitFormFromEziToTarget",submitFormFromEziToTarget);
});
///-------------------------------///
/// developer: Donovan
///
/// Account Dropdown Change Type
///-------------------------------///
$(function(){
	function accountDropdown(){

	};
	accountDropdown.prototype = {
		changeType: function(me,country){
			var selectedValue = $(me).attr('data-value');
			switch(selectedValue)
			{
				case 'W':
					$('#BranchSearchCodeContainer').addClass('input disabled');
					$('#branchSearchCode').attr('disabled','disabled');
					$('#branchSearchButton').addClass('formButtonDisabled');
					$('#branchSearchButton').attr('onclick','').unbind('click');
					$('#branchSearchCode').val('250345');
					$('#branchSearchName').val('WesBank');
					break;
				case 'F':
					$('#BranchSearchCodeContainer').addClass('input disabled');
					$('#branchSearchCode').attr('disabled','disabled');
					$('#branchSearchButton').addClass('formButtonDisabled');
					$('#branchSearchButton').attr('onclick','').unbind('click');
					var countryIndex = -1;
					var theBranchCode = '';
					if(country == 55)	{
						theBranchCode = '282767';
					} else if(country == 45){
						theBranchCode = '280179';
					} else	{
						theBranchCode = '255105';
					}
					$('#branchSearchCode').val(theBranchCode);
					$('#branchSearchName').val('First National Bank');
					
					break;
				default:
					$('#BranchSearchCodeContainer').addClass('input');
					$('#branchSearchCode').removeAttr('disabled');
					$('#BranchSearchCodeContainer').removeClass('input disabled').addClass('input');
					$('#branchSearchButton').removeClass('formButtonDisabled');
					$('#branchSearchButton').attr('onclick','fnb.controls.controller.eventsObject.raiseEvent("eziSliderShow", {url: "/banking/Controller?nav=navigator.branchsearch.simple.BranchSearchLanding"})').bind('click');
					break;
			}
		}
	};
	namespace("fnb.functions.accountDropdown",accountDropdown);
});

///-------------------------------///
/// developer: Donovan
/// developer 2: Wpienaar
/// Lotto
///-------------------------------///
$(function(){
	function lotto(){
		this.parentFunction = this;
		this.alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		this.selectedBoardCollection = [];
		this.selectedBoardNumbers = null;
		this.boardPrice;
		this.lottoType;
		this.maxBoardNumbers ;
		this.maxBoards = 20;
		this.boardIndex;
		this.lottoType;
		this.elemheight = 45;
		this.curindex = 0;
		this.elemCount = 2;
		this.selectedClass='numberSelected';
	};
	lotto.prototype = {
		init: function(type,boardPrice,maxBoards,maxBoardNumbers){
			this.boardPrice = boardPrice;
			this.lottoType = type;
			this.maxBoardNumbers = Number(maxBoardNumbers);
			this.curindex=0;
		},
		numberPickerInitialize: function(index){
			
			this.boardIndex = index;
			
			if(this.lottoType == "lotto" || this.lottoType == "powerball"){
				this.numberPickerClearSelections();
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#numberPicker'},{show:'true',element:'#numberClose'},{show:'true',element:'#numberClear'},{show:'true',element:'#numberDone'},{show:'true',element:'#numberNumberPickerHeader'},{show:'false',element:'#playLottoLanding'},{show:'false',element:'#lottoCancel'},{show:'false',element:'#lottoBuy'},{show:'false',element:'#lottoNumberPickerHeader'}]);
			}
			
			$("#numberPickerHeader_left").find(":header").html('Board '+this.alphabet[this.boardIndex]);
			$("#numberPickerHeader_left").find("p").html(this.boardPrice);

			$('#lottoBoardViewItem0').find('.lottoBoardlistItemNumbers').html('');
			$('#lottoBoardViewItem0').find('.lottoBoardViewItemName').html('Board '+this.alphabet[this.boardIndex]);
			$('#powerballSelectBall').html('');

			var viewParent = $("#lottoBoardViewItem0");
			if($('#board'+index).val() != ''){
				var numbersString = $('#board'+index).val();
				var numbersArray = numbersString.split(',');
				$.each(numbersArray, function(NumberIndex, number) {
					if(this.lottoType=="powerball"&&NumberIndex==5){
						$(viewParent).find('.lottoBoardlistItemNumbers').append('<span style="float:left;margin:4px 4px 0 0;">Powerball</span>');
					}
						$(viewParent).find('.lottoBoardlistItemNumbers').append('<div class="numberPickerNumber"><div class="numberPickerNumberText">'+number+'</div></div>');
					
					
				});
				
				var ltype = this.lottoType;
				var numberSelected=this.selectedClass;
				$('#_Grid > div').each(function() {
					var tempNumber = $(this).find('.numberPickerCellTextInner').html();
					var cell = this;
					$.each(numbersArray, function(NumberIndex, number) {
						if (number == tempNumber){
							if (!(ltype == 'powerball' && NumberIndex == 5)) $(cell).find('.numberPickerCellText').addClass(numberSelected);
						}
					});
				});
				if (this.lottoType == 'powerball'){
					$('#powerballNum_Grid > div').each(function(){
						var tempNumber = $(this).find('.numberPickerCellTextInner').html();
						if (numbersArray[5] == tempNumber){
							$(this).find('.numberPickerCellText').addClass(numberSelected);
						}
					});
				}
			}
			
		},
		numberPickerSelectNumber: function(numberObject, boardType){

			var viewParent = $("#lottoBoardViewItem0");
			var numberSelected=this.selectedClass;
			var selectedCellVal = $(numberObject).find('.numberPickerCellText').hasClass(numberSelected);
			var selectedNumber = $(numberObject).find('.numberPickerCellTextInner').html();
			var selectedNumberCount = 0;
			$('#_Grid > div').each(function () {
				if($(this).find('.numberPickerCellText').hasClass(numberSelected)){
					selectedNumberCount++;
				}
			});
			
			var maxBoardNumbers = this.maxBoardNumbers;
			if(this.lottoType=='powerball'){
				maxBoardNumbers = 5;
				var powerballNumber = 0;
				$('#powerballNum_Grid > div').each(function () {
					if($(this).find('.numberPickerCellText').hasClass(numberSelected)){
						powerballNumber = $(this).find('.numberPickerCellTextInner').html();
					}
				});
			}

			var goOn=false;
			switch(selectedCellVal)
		    {
				case false:
					if(this.lottoType=='powerball'){
						if ((powerballNumber==0 && boardType=='PB') || (selectedNumberCount<maxBoardNumbers && boardType!='PB')){
							goOn=true;
							//if (boardType!='PB' && selectedNumber==powerballNumber) goOn=false;
							/*if (boardType=='PB'){
								$(viewParent).find('.lottoBoardlistItemNumbers > div').each(function(){
									var num = $(this).find('.numberPickerNumberText').html();
									if (selectedNumber==num) goOn=false;
								});
							}*/
						}
					} else {
						if(selectedNumberCount<maxBoardNumbers) goOn=true;
					}
					if (goOn){
						var currentCount = selectedNumberCount+1;
						$(numberObject).find('.numberPickerCellText').addClass(numberSelected);
						
						
						//$(numberObject).find('.numberPickerCellText').css('background-color', '#ADDBDB');
						//$(numberObject).find('.numberPickerCellTextInner').css('color', '#FFF');
						$("#numberPickerHeaderNumber_wrapper").append('<div id="numberPickerNumber'+currentCount+'" class="numberPickerNumber" style="display:none"><div class="numberPickerNumberText">'+selectedNumber+'</div></div>');
						$("#numberPickerNumber"+currentCount).fadeIn("200");
						
						if (boardType=='PB')
							$('#powerballSelectBall').html('<span style="float:left;margin:6px 5px 0px 10px;">Powerball</span><div class="numberPickerNumber"><div class="numberPickerNumberText">'+selectedNumber+'</div></div>');
						else
							$(viewParent).find('.lottoBoardlistItemNumbers').append('<div class="numberPickerNumber"><div class="numberPickerNumberText">'+selectedNumber+'</div></div>');
						
					}
				break;
				case true:
					$(numberObject).find('.numberPickerCellText').removeClass(numberSelected);
					
					
					//$(numberObject).find('.numberPickerCellText').css('background-color', '#FFF');
					//$(numberObject).find('.numberPickerCellTextInner').css('color', '#099');
					var selectedNumber = $(numberObject).find('.numberPickerCellTextInner').html();
					if (boardType=='PB'){
						$('#powerballSelectBall').html('');
					} else {
						$('#lottoBoardViewItem0').find('.lottoBoardlistItemNumbers').find('.numberPickerNumber').each(function () {
							var tempNumber = $(this).find('.numberPickerNumberText').html();
							if(selectedNumber == tempNumber){
							  $(this).animate({
									opacity: 0
								  }, 200, function() {
										$(this).animate({
											width: 0
										}, 200, function() {
											$(this).attr('id','');
											$(this).remove();
											var l = 0;
											$("#numberPickerHeaderNumber_wrapper > div").each(function() {
												l++;
										        $(this).attr("id", "numberPickerNumber"+l);
											});
										});
								});
							}
						});
					}
			    break;
				default:
					
				break;
		    }
			
		},
		numberPickerSubmitNumbers: function(){
		
			this.selectedBoardNumbers = {};
			var numbersString = '';
			var numberSelected=this.selectedClass;
			var countNumbers = 0;
			var powerballSelected = false;

			$('#_Grid > div').each(function() {
				if($(this).find('.numberPickerCellText').hasClass(numberSelected)){
					var tempNumber = $(this).find('.numberPickerCellTextInner').html();
					countNumbers++;
					numbersString += tempNumber +',';
				}
			});
			if (this.lottoType == 'powerball'){
				$('#powerballNum_Grid > div').each(function() {
					if($(this).find('.numberPickerCellText').hasClass(numberSelected)){
						var tempNumber = $(this).find('.numberPickerCellTextInner').html();
						countNumbers++;
						numbersString += tempNumber +',';
						powerBallSelected = true;
						break;
					}else{
						
						powerBallSelected = false;
						
					}
				});
				
				
			}
			if(countNumbers<this.maxBoardNumbers){
				if (this.lottoType == 'powerball'){
					this.lottoError('01',"Please select 5 numbers per board and\none Powerball number.");
				}else{
					this.lottoError('01','Please select '+this.maxBoardNumbers+' numbers per board.');
				}
			}else{
				numbersString = numbersString.substring(0, numbersString.length - 1);
				this.selectedBoardNumbers = numbersString;
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#numberPicker'},{show:'false',element:'#numberClose'},{show:'false',element:'#numberClear'},{show:'false',element:'#numberDone'},{show:'false',element:'#numberNumberPickerHeader'},{show:'true',element:'#playLottoLanding'},{show:'true',element:'#lottoCancel'},{show:'true',element:'#lottoBuy'},{show:'true',element:'#lottoNumberPickerHeader'}]);
				
				this.lottoAppendBoard();
			}
		},
		numberPickerClearSelections: function(){
			$("#numberPickerHeaderNumber_wrapper > div").each(function() {
				$(this).animate({
				    opacity: 0
				}, 50, function() {
					$(this).remove();
				});
			});

			$('#lottoBoardViewItem0').find('.lottoBoardlistItemNumbers').html('');
			$('#powerballSelectBall').html('');

			var numberSelected=this.selectedClass;
			$('#_Grid > div').each(function () {
				if ($(this).find('.numberPickerCellText').hasClass(numberSelected)) $(this).find('.numberPickerCellText').removeClass(numberSelected);
			});
			if (this.lottoType == 'powerball'){
				$('#powerballNum_Grid > div').each(function () {
					if ($(this).find('.numberPickerCellText').hasClass(numberSelected)) $(this).find('.numberPickerCellText').removeClass(numberSelected);
				});
			}
		},
		lottoAddBoard: function(){
			if($("#lottoBoardlistRow > div").size() < this.maxBoards){
				var index = $("#lottoBoardlistRow > div").size();
				var $orginalAddLottoBoardRow = $('#lottoBoardlistItem0');
				var $clonedAddLottoBoardRow = $orginalAddLottoBoardRow.clone();
				$clonedAddLottoBoardRow.attr('id','lottoBoardlistItem'+index);
				$clonedAddLottoBoardRow.find('.lottoBoardlistItemName').html('<a onclick="fnb.functions.lotto.numberPickerInitialize('+index+');" id="boardLink" target="_self">Board '+this.alphabet[index]+'</a>');
				$clonedAddLottoBoardRow.find('.numberPickerNumber').remove();
				$clonedAddLottoBoardRow.find('#numbersLink').remove();
				$clonedAddLottoBoardRow.find(":input").attr('value','');
				$clonedAddLottoBoardRow.find(":input").attr('name','board'+index);
				$clonedAddLottoBoardRow.find(":input").attr('id','board'+index);
				$clonedAddLottoBoardRow.find('.lottoBoardlistItemNumbers').html('');
				$clonedAddLottoBoardRow.find('.lottoBoardlistItemNumbers').append('<a target="_self" id="numbersLink" onclick="fnb.functions.lotto.numberPickerInitialize('+index+')">Choose Numbers</a>');
				$clonedAddLottoBoardRow.find('.lottoBoardlistClose').find('.formButton').remove();
				$clonedAddLottoBoardRow.find('.lottoBoardlistClose').append('<a class="formButton inverted small" target="_self" onclick="fnb.functions.lotto.lottoRemoveBoard('+index+');">X</a>');
				$clonedAddLottoBoardRow.css('height', 0);
				$clonedAddLottoBoardRow.animate({height: 45}).appendTo('#lottoBoardlistRow');
				//$("#lottoBoardViewRow").append('<div id="lottoBoardViewItem'+index+'" class="lottoBoardlistItem"><div class="lottoBoardViewItemName">Board '+this.alphabet[index]+'</div><div class="lottoBoardlistItemNumbers"></div></div>');
				
				if (this.elemCount>=3){
					this.activateScrollTop(true);
					this.curindex = this.elemCount-3;
					this.scrollDown();
				}
				this.elemCount++;
				
			}else{
				//this.lottoError('01','You can only add '+this.maxBoards+' boards.');
				$('#maxMessage').animate({opacity: 100}, 100, function() { $('#maxMessage').animate({opacity: 0}, 2000)});
			}
		},
		lottoAppendBoard: function(){
			var numbersString = this.selectedBoardNumbers;
			var numbersArray = numbersString.split(',');
			var selectedParent = $('#lottoBoardlistItem'+this.boardIndex);
			var viewParent = $("#lottoBoardViewItem"+this.boardIndex);
			$(selectedParent).find('#numbersLink').remove();
			$(selectedParent).find(":input").attr('value',numbersString.toString());
			$(selectedParent).find('.lottoBoardlistItemNumbers').html('');
			$(viewParent).find('.lottoBoardlistItemNumbers').html('');
			var ltype = this.lottoType;
			$.each(numbersArray, function(NumberIndex, number) {
				if(ltype=="powerball"&&NumberIndex==5){
					$(selectedParent).find('.lottoBoardlistItemNumbers').append('<span style="float:left;margin:12px 4px 0 0;color:#000;">Powerball</span>');
					$(viewParent).find('.lottoBoardlistItemNumbers').append('<span style="float:left;margin:4px 4px 0 0;">Powerball</span>');
				}
				$(selectedParent).find('.lottoBoardlistItemNumbers').append('<div id="numberPickerNumber'+NumberIndex+'" class="numberPickerNumber"><div class="numberPickerNumberText">'+number+'</div></div>');
				$(viewParent).find('.lottoBoardlistItemNumbers').append('<div class="numberPickerNumber"><div class="numberPickerNumberText">'+number+'</div></div>');
			});
		},
		lottoRemoveBoard: function(index){
			$('#board'+index).val('');
			var parent = $("#lottoBoardlistRow").find('.lottoBoardlistItem:eq('+index+')');
			//var viewParent = $('#lottoBoardViewItem'+index);
			var alphabet = this.alphabet;
			if (this.elemCount>=3){ //if(index>1){
				$(parent).animate({
					opacity: 0
				},
				200,
				function() {
					$(parent).animate({
						height: 0
					},
					200,
					function() {
						$(parent).remove();
						var i =0;
						$("#lottoBoardlistRow > div").each(function() {
							$(this).attr('id','lottoBoardlistItem'+i);
							$(this).find("#boardLink").html('Board '+alphabet[i]);
							$(this).find("#boardLink").attr('onclick','fnb.functions.lotto.numberPickerInitialize('+i+');');
							$(this).find(":input").attr('name','board'+i);
							$(this).find(":input").attr('id','board'+i);
							$(this).find("#numbersLink").attr('onclick','fnb.functions.lotto.numberPickerInitialize('+i+');');
							$(this).find("div.lottoBoardlistClose a").attr('onclick','fnb.functions.lotto.lottoRemoveBoard('+i+');');
							i++;
						});
					});
				});
				
				this.elemCount--;
				if (this.elemCount<=3){
					this.activateScrollTop(false);
					this.activateScrollBottom(false);
				}
				if (this.curindex > 0) this.scrollUp();
			}else{
				if($(parent).find('.lottoBoardlistItemNumbers').find('div[id^="numberPickerNumber"]').is('*')){
					//$(parent).find('.lottoBoardlistItemNumbers').find('.numberPickerNumber').remove();
					$(parent).find('.lottoBoardlistItemNumbers').html('');
					$(parent).find('.lottoBoardlistItemNumbers').append('<a target="_self" id="numbersLink" onclick="fnb.functions.lotto.numberPickerInitialize('+index+')">Choose Numbers</a>');
				}
			}
		},
		lottoError: function(errorCode,message){
			//alert(errorCode + ' - ' + message);
		},
		populateConfirmNumbers: function(boardNumbers, index){
			var splitBoardNumbers = boardNumbers.split(',');
			var ltype = this.lottoType;
			$("#lottoBoardlistItem"+index).find('#boardLink').html('Board '+this.alphabet[index]);
			$.each(splitBoardNumbers, function(NumberIndex, number) {
				if(ltype=="powerball"&&NumberIndex==5){
					$('#lottoBoardlistItem'+index).find('.lottoBoardlistItemNumbers').append('<span style="float:left;margin:12px 4px 0 0;color:#000;">Powerball</span>');
				}
				$("#lottoBoardlistItem"+index).find('.lottoBoardlistItemNumbers').append('<div id="numberPickerNumber'+NumberIndex+'" class="numberPickerNumber"><div class="numberPickerNumberText">'+number+'</div></div>');
			});
		},
		scrollUp: function(){
			if ((this.curindex+3)==this.elemCount) this.activateScrollBottom(true);
			this.curindex--;
			if (this.curindex==0) this.activateScrollTop(false);
			$("div#lottoBoardlistRow").animate({top: (-this.curindex*this.elemheight)}, 400);
		},
		scrollDown: function(){
			if (this.curindex==0) this.activateScrollTop(true);
			this.curindex++;
			if ((this.curindex+3)==this.elemCount) this.activateScrollBottom(false);
			$("div#lottoBoardlistRow").animate({top: (-this.curindex*this.elemheight)}, 400);
		},
		activateScrollTop: function(active){
			if (active){
				$("#lottoScrollTop").attr('onclick', 'fnb.functions.lotto.scrollUp();return false;').animate({opacity: 100}, 100);
			} else {
				$("#lottoScrollTop").attr('onclick', 'return false;').animate({opacity: 0}, 100);
			}
		},
		activateScrollBottom: function(active){
			if (active){
				$("#lottoScrollBottom").attr('onclick', 'fnb.functions.lotto.scrollDown();return false;').animate({opacity: 100}, 100);
			} else {
				$("#lottoScrollBottom").attr('onclick', 'return false;').animate({opacity: 0}, 100);
			}
		},
		setElemCount: function(nrElems){
			this.elemCount = nrElems;
			if (this.elemCount>3){
				this.activateScrollBottom(true);
			}
		}
	};
	namespace("fnb.functions.lotto",lotto);
});
///-------------------------------///
/// developer: Donovan
///
/// SHOWDIV
///-------------------------------///
$(function(){
	function showDiv(){

	};
	showDiv.prototype = {
		show: function(targetDiv,old_item){
			fnb.functions.unhider.unhide(null,targetDiv,'left','hide',old_item);
		}
	};
	namespace("fnb.functions.showDiv",showDiv);
});
///-------------------------------///
/// developer: Donovan
///
/// NAVIGATE TO
///-------------------------------///
$(function(){
	function navigateTo(){

	};
	navigateTo.prototype = {
		navigate: function(me){
			
			$(me).find('a:eq(0)').trigger('onclick');
		}
	};
	namespace("fnb.functions.navigateTo",navigateTo);
});
///-------------------------------///
/// developer: Donovan
///
/// NAVIGATE TO
///-------------------------------///
$(function(){
	function gotoUrl(){

	};
	gotoUrl.prototype = {
		go: function(url){
			window.top.location=url;
		}
	};
	namespace("fnb.functions.gotoUrl",gotoUrl);
});
///-------------------------------///
/// developer: Donovan
///
/// Hyperlink
///-------------------------------///
$(function(){
	function hyperlink(){

	};
	hyperlink.prototype = {
		navigate: function(url,title,width,height,resizable,status,menubar){
			var popup = window.open(url,title,"width="+width+",height="+height+",resizable="+resizable+",status="+status+",menubar="+menubar);
			popup.focus();
		}
	};
	namespace("fnb.functions.hyperlink",hyperlink);
});

///-----------------------------------------------------------------///
/// developer: Vaughan
///
/// CHECKBOX HIERARCHY SWITCHER
///   manipulate checkbox column children states via data attribute,
///   for current clicked checkbox (data comes from Java)
///-----------------------------------------------------------------///
$(function(){
	function checkboxHierarchySwitcher(){
		this.childDisabled = true;
		this.childDeselected = true;
		this.dataAttrName = 'data-extended';
	}
	checkboxHierarchySwitcher.prototype = {
		checkboxClick: function(clickedCheckbox){
			var children = this.getChildrenAsIdentifierString(clickedCheckbox);
			
			if(clickedCheckbox.parent().hasClass('checked')){
				$(children).setCheckbox(true);
				if(this.childDisabled) $(children).disableCheckbox();
			} else {
				$(children).enableCheckbox();
				if(this.childDeselected) $(children).setCheckbox(false);
			}
		},
		getChildrenAsIdentifierString: function(clickedCheckbox){
			var theString = '';

			//retrieve children as string from data attribute as supplied by Java
			if(typeof clickedCheckbox.attr(this.dataAttrName) != 'undefined'){
				if(clickedCheckbox.attr(this.dataAttrName) != '') theString = '#' + (clickedCheckbox.attr(this.dataAttrName).replace(/, /g, '.checkbox-input,#')) + '.checkbox-input';
			}
			
			return theString;
		},
		getChildren: function(checkedCheckbox){
			var validItems = [];
			var itemsAsString = this.getChildrenAsIdentifierString(checkedCheckbox);
			
			if(typeof itemsAsString != 'undefined'){
				var items = itemsAsString.split(',');

		  		$.each(items, function(i, checkbox){
		  			//do not push any Java given elements that do not exist
		  			if($(checkbox).length != 0) validItems.push($(checkbox));
		  		});
			}
			
			return validItems;
		}
	};

	namespace("fnb.functions.checkboxHierarchySwitcher",checkboxHierarchySwitcher);
});

///-------------------------------------------///
/// developer: Vaughan
///
/// TABLE ROW HANDLER
///   table row and its children DOM iterator
///-------------------------------------------///
$(function(){
	function rowHandler(){}
	rowHandler.prototype = {
		getRows: function(){
			return $($('.tableContainer').find('.tableRow'));
		},
		getRowCheckboxes: function(row){
			var checkboxes = [];
			var checkboxesAsString = row.find('.checkbox-input');
			
			if(checkboxesAsString != ''){
				$.each(checkboxesAsString, function(i, checkbox){
					checkboxes.push($(checkbox));
				});
			}
			
			return checkboxes;
		},
		getRowViaCheckbox: function(checkbox){
			return checkbox.closest('.tableRow');
		},
		getLastCheckbox: function(checkboxes){
			return checkboxes[checkboxes.length - 1];
		}
	}
	namespace("fnb.functions.rowHandler",rowHandler);
});

$(function(){
	function toggleClassName(){
		
	};
	toggleClassName.prototype = {
			toggle: function(element,className){
				var parent = this;
				$(element).toggleClass(className);
				fnb.hyperion.utils.footer.configFooterButtons()
			}
	}
	namespace("fnb.functions.toggleClassName",toggleClassName);
	
});
/* Developer: Donovan
Big Three*/
$(function() {
	this.initialized = false;
	this.scrollingBannerId;
	function bigThree() {

	};
	bigThree.prototype = {
		init : function() {
			this.initialized = true;
			this.scrollingBannerId = $('#scrollingBannerContainer');
			this.adjust($(window).width());
			this.bindEvents();
		},
		onclick: function() {
			
		},
		bindEvents: function() {
			this.scrollingBannerId.on('click touchend', '.scrollingBannerItem', function(event) {fnb.functions.bigThree.select(event)});
		},
		select: function(event) {
			setTimeout(function() {
				var isMoving = false;
				$.each(fnb.utils.scrollingBanner.bannerParentItems, function(index, parentId) {
					 if(typeof fnb.utils.scrollingBanner.bannerParent[parentId].bannerScroller!="undefined"){
						if(fnb.utils.scrollingBanner.bannerParent[parentId].bannerScroller.moving == true) isMoving=true  
					 }
				});
				if( isMoving == false) {
					var url = $(event.target).find(">:first-child").attr('data-value');
					if(typeof url == 'undefined') url = $(event.target).parent().parent().attr('data-value');
					if(typeof url == 'undefined') url = $(event.target).parent().attr('data-value');
					if(typeof url == 'undefined') url = $(event.target).attr('data-value');
					if(typeof url != 'undefined') fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: url})
				}
			}, 200);
		},
		adjust: function(windowWidth) {

			if(this.scrollingBannerId.find('.scrollingBannerItem').length>0){

				var maxWidth = $(_workspace).width();
				var bigThreeWidth = this.scrollingBannerId.find('.scrollingBannerItem').first().outerWidth()*this.scrollingBannerId.find('.scrollingBannerItem').length;

				if(maxWidth>bigThreeWidth){
					this.scrollingBannerId.width('');
					this.scrollingBannerId.find('.scrollingBannerItem').removeAttr('style');
					this.scrollingBannerId.find('.scrollingBannerItem').width('');
				}else{
					if(fnb.utils.scrollingBanner.bannerEnableScrolling=="false"){
						fnb.utils.scrollingBanner.init('scrollingBannerContainer','true','true');
						fnb.utils.scrollingBanner.bannerScrollerStopWidth = windowWidth-25;
					}
				}
			}
		}
	}
	namespace("fnb.functions.bigThree",bigThree);
});

/* Developer: Donovan
Session Timeout */
$(function() {
	var parentFunction;
	var logOffTimer;
	var logOffTime = 300000;
	var logOffTimeCounter;
	var timedOutPopup;
	var timedOutStarted = false;
	function timeOut() {
		parentFunction = this;
	};
	timeOut.prototype = {
		init : function() {
			var _this = this;
			timedOutStarted = false;
			this.logOffTime = logOffTime;
			this.logOffTimeCounter = this.logOffTime;
			this.reset();
		},
		triggerTimeOut: function() {
			fnb.controls.controller.eventsObject.raiseEvent('timedOut', '');
		},
		reset: function() {
			var _this = this;
			if(loggedIn=='true'){
				_this.clearTimer();
				setTimeout(function() {
					 if(timedOutStarted == false&&progressBar.progressActive==false){
						 _this.logOffTimeCounter = _this.logOffTime;
						_this.createTimer();
					}
				}, 4000);
			}else if(typeof fnb.functions.timeOut.logOffTimer!='undefined'){
				_this.clearTimer();
			}
		},
		showPopup: function() {
			if(!_isMobile) var timedOutPopup = window.open("includes/sessionTimedOutPopup.jsp","BANKit","width=680,height=395,resizable=false,status=false,menubar=false");
			$(_overlay).addClass('timeOverlay');
			$(_sessionTimedOutOverlay).removeClass('hidden');
			fnb.functions.logOff.init();
			clearTimeout(_this.logOffTimer);
			timedOutStarted = true;
			if(!_isMobile){
				if(timedOutPopup){
					timedOutPopup.focus();
				}
			}
		},
		hideInternal: function() {
			$(_sessionTimedOutOverlay).addClass('hidden');
			fnb.functions.logOff.stopTimer();
		},
		logOff: function() {
			fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen', '/banking/Controller?nav=navigator.UserLogoff');
		},
		createTimer: function() {
			var _this = this;
			this.logOffTimer = setTimeout(function() {
			    if (_this.logOffTimeCounter < 1&&loggedIn=='true') {
					_this.triggerTimeOut();
			    }else{
					_this.clearTimer();
					_this.logOffTimeCounter= _this.logOffTimeCounter-1000;
					_this.createTimer();
				}
			}, 1000);
		},
		clearTimer: function() {
			clearTimeout(fnb.functions.timeOut.logOffTimer); 
			timedOutStarted = false;
		},
		keepSessionAlive: function() {
			$(_overlay).removeClass('timeOverlay');
			timedOutStarted = false;
			this.reset();
			this.doAliveRequest();
		},
		doAliveRequest: function() {
			var loadObj = {url: "/banking/Controller?nav=KeepSessionAlive",target: _hiddenLogOffDiv,expectResponse:false};
			fnb.controls.controller.eventsObject.raiseEvent('keepSessionAlive', loadObj);
		},
		doSimpleAliveRequest: function() {
			//var loadObj = {url: "/banking/Controller?nav=KeepSessionAlive",target: _hiddenLogOffDiv,expectResponse:false};
			//fnb.controls.controller.loadUrl("keepSessionAlive",loadObj.target,loadObj.url,"","","","",loadObj.expectResponse)
		}
	}
	namespace("fnb.functions.timeOut",timeOut);
});
/* Developer: Donovan
Internal Popup functions*/
$(function() {
	this.logOffTimer2;
	this.parentWindow;
	this.logOffTime = 120;
	function logOff() {
		parentFunction = this;
	};
	logOff.prototype = {
		init : function() {
			this.startTimer();
		},
		logOff: function() {
			clearTimeout(this.logOffTimer2);
			fnb.functions.timeOut.logOff();
		},
		exit: function() {
			clearTimeout(this.logOffTimer2);
			$(_sessionTimedOutOverlay).addClass('hidden');
			fnb.functions.timeOut.keepSessionAlive();
		},
		startTimer: function() {
			var _this = this;
			this.logOffTime = 120;
			document.getElementById("timer").innerHTML = this.logOffTime;
			this.createTimer();
		},
		createTimer: function() {
			var _this = this;
			this.logOffTimer2 = setTimeout(function() {
				_this.logOffTime--;
			    document.getElementById("timer").innerHTML = _this.logOffTime;
			    if (_this.logOffTime == 0) {
			       clearTimeout(this.logOffTimer2);
			        _this.logOff();
			    }else{
					clearTimeout(_this.ogOffTimer2);
					_this.createTimer();
				}
			}, 1000);
		},
		stopTimer: function() {
			clearTimeout(this.logOffTimer2);
		}
	}
	namespace("fnb.functions.logOff",logOff);
});
/* Developer: Donovan
More Row Select*/
$(function(){
	function tableMoreButton(){
		
	};
	tableMoreButton.prototype = {
			checkSelection: function(attr){
				if(_isMobile){
					if(fnb.utils.mobile.utils.mobileMoved == false||typeof fnb.utils.mobile.utils.mobileMoved=='undefined'){
						fnb.controls.controller.eventsObject.raiseEvent('tableMoreOptions', attr);
					}
				}else{
					fnb.controls.controller.eventsObject.raiseEvent('tableMoreOptions', attr);
				}
			}
	}
	namespace("fnb.functions.tableMoreButton",tableMoreButton);
	
});
/* Developer: Donovan
FooterButtonTransfer*/
$(function(){
	function transferFooterContent(){
		
	};
	transferFooterContent.prototype = {
			transfer: function(parent,content){
				var currentContent = content;
				parent.html('');
				$('#footerWrapper').find('.right-sidebar-bottom').html(currentContent);
			},
			clear: function(){
				$('#footerWrapper').find('.right-sidebar-bottom').html('');
			},
			reset: function(){
				$('#footerMessage').addClass('hideElement');
				this.clear();
			}
	}
	namespace("fnb.functions.transferFooterContent",transferFooterContent);
	
});
/* Developer: Donovan
Scroll Listener*/
$(function(){
	function isScrolling(){
		this.moreWrapper = $('#forMore');
	};
	isScrolling.prototype = {
			init: function(){
				if(typeof this.moreWrapper=='undefined') this.moreWrapper = $('#forMore');
			},
			checkPos: function(){
				if(fnb.utils.overlay.expanded==false){
					var myDiv = document.getElementById("pageContent");
					var height = myDiv.scrollHeight;
					var position = $(document).scrollTop();
					var scrollbar = myDiv.clientHeight;
					
					if((position+$(window).height()) < (scrollbar-50)){
						if(this.moreWrapper.hasClass('visibilityHidden')){
							this.moreWrapper.removeClass('visibilityHidden');
						}
					}else{
						if(!this.moreWrapper.hasClass('visibilityHidden')){
							this.moreWrapper.addClass('visibilityHidden');
						}
					}
				}
			},
			doScroll: function(pos){
				var scrollPos = pos;
				var position = $(document).scrollTop();
				if(typeof scrollPos=='undefined'){
					scrollPos = $(window).height();
				}else{
					position = 0;
				}
				var myDiv = document.getElementById("pageContent");
				var toPos = position+scrollPos;
				$("html, body").animate({ scrollTop:  toPos+"px" });
			},
			hide: function(){
				if(typeof this.moreWrapper=='undefined') this.moreWrapper = $('#forMore');
				if(!this.moreWrapper.hasClass('visibilityHidden')){
					this.moreWrapper.addClass('visibilityHidden');
				}
			}
	}
	namespace("fnb.functions.isScrolling",isScrolling);
});

/* Developer: Donovan
Slow Connection popup*/
$(function(){
	function slowConnection(){
		
	};
	slowConnection.prototype = {
		show: function(){
			if($(_slowConnectionOverlay).hasClass('hidden')) $(_slowConnectionOverlay).removeClass('hidden');
		},
		hide: function(){
			if(!$(_slowConnectionOverlay).hasClass('hidden')) $(_slowConnectionOverlay).addClass('hidden');
		},
		continueWaiting: function(){
			this.hide();
		},
		endNow: function(){
			this.hide();
			fnb.controls.controller.eventsObject.raiseEvent('abortLoadUrl', 'abort');
		}
	}
	namespace("fnb.functions.slowConnection",slowConnection);
});

/* Developer: Donovan
Coza Content*/
$(function(){
	function cozaContent(){
		
	};
	cozaContent.prototype = {
		select: function(event,target){
			var url = $(event.target).attr('link');
			if(typeof url == 'undefined') url = $(event.target).parent().attr('link');
			if(typeof url == 'undefined') url = $(event.target).parent().parent().attr('link');
			if(typeof url != 'undefined'){
				var loadObj = {url: url+'?bankingFrame=true',target: target};
				fnb.controls.controller.eventsObject.raiseEvent('loadUrl', loadObj);
			}
		},
		selectHref: function(event,target){
			var url = $(event.target).attr('href');
			if(typeof url == 'undefined') url = $(event.target).parent().attr('href');
			if(typeof url == 'undefined') url = $(event.target).parent().parent().attr('href');
			if(typeof url == 'undefined') url = $(event.target).parent().parent().parent().attr('href');
			if(typeof url != 'undefined'){
				var loadObj = {url: url+'?bankingFrame=true',target: target};
				fnb.controls.controller.eventsObject.raiseEvent('loadUrl', loadObj);
			}
		}
	}
	namespace("fnb.functions.cozaContent",cozaContent);
});


/* Developer: Donovan
Clone footer*/
$(function(){
	function footer(){
		
	};
	footer.prototype = {
		add: function(){

			var footerHtml = $('#formFooterButtons').children(0).html();
			
			var buttons = $('#formFooterButtons').children(0).children().get().reverse();
			
			for(i=0; i < buttons.length; i++){

				$('#footerButtonsContainer').append(buttons[i]);
			}

		},
		clear: function(){
			
			$('#footerButtonsContainer').html('');
			
		}
	}
	namespace("fnb.functions.footer",footer);
});

/* Developer: Donovan
IE8 Content*/
$(function(){
	function ie8(){
		this.isAdded = false;
	};
	ie8.prototype = {
		doCheck: function(){
			if(_browserName=="MSIE"&&_browserVersion<9){
				var windowWidth = $(window).width();
				if(windowWidth>_siteMaxWidth&&this.isAdded!=true){
					this.addClasses();
					this.isAdded = true;
				}else if(windowWidth<_siteMaxWidth&&this.isAdded==true){
					this.removeClasses();
					this.isAdded = false;
				}else if(windowWidth>_siteMaxWidth&&this.isAdded==true){
					$('#actionMenuTextWrapper').addClass('ie8-actionMenuTextWrapper');
					$('#actionMenuButtonWrapper').find('.actionMenuButtonWrapperInner').addClass('ie8-actionMenuButtonWrapperInner');
					$('#formFooterButtons').addClass('ie8-formFooterButtons');
					$('#main').addClass('ie8-main');
					$('#main').find('.pageWrapper').addClass('ie8-pageWrapper');
				}
			}
		},
		addClasses: function(){
			$('html').addClass('ie8-htmlBackGround');
			$('#main').addClass('ie8-main');
			$('#formFooterButtons').addClass('ie8-formFooterButtons');
			$('#topNavWrapper').addClass('ie8-topNavWrapper');
			$('#eziPannelButtons').addClass('ie8-eziPannelButtons');
			$('#eziWrapper').addClass('ie8-eziWrapper');
			$('#actionMenuTextWrapper').addClass('ie8-actionMenuTextWrapper');
			$('#actionMenuButtonWrapper').find('.actionMenuButtonWrapperInner').addClass('ie8-actionMenuButtonWrapperInner');
			$('#footerWrapperNew').find('.left-sidebar-bottom').addClass('ie8-left-sidebar-bottom');
			$('#fnb-logo').addClass('ie8-footerWrapperNewfnb-logo');
			
			$('#topMenu').find('.left-sidebar-top').addClass('ie8-left-sidebar-top');
			$('#footerMessage').find('.footerMessageInner').addClass('ie8-footerMessageInner');
			$('.pageWrapper').addClass('ie8-pageWrapper');
			$('#actionWrap').addClass('ie8-actionWrap');
			$('#popupWrapper').addClass('ie8-popupWrapper');
			$('#overlay').addClass('ie8-overlay');
			$('#calendarWrapper').find('.bcal-container').addClass('ie8-bcal-container');
			
			$('#footerWrapperNew').addClass('ie8-footerContent');
			
		},
		removeClasses: function(){
			$('html').removeClass('ie8-htmlBackGround');
			$('#main').removeClass('ie8-main');
			$('#formFooterButtons').removeClass('ie8-formFooterButtons');
			$('#topNavWrapper').removeClass('ie8-topNavWrapper');
			$('#eziPannelButtons').removeClass('ie8-eziPannelButtons');
			$('#eziWrapper').removeClass('ie8-eziWrapper');
			$('#actionMenuTextWrapper').removeClass('ie8-actionMenuTextWrapper');
			$('#actionMenuButtonWrapper').find('.actionMenuButtonWrapperInner').removeClass('ie8-actionMenuButtonWrapperInner');
			$('#footerWrapperNew').find('.left-sidebar-bottom').removeClass('ie8-left-sidebar-bottom');
			$('#fnb-logo').removeClass('ie8-footerWrapperNewfnb-logo');		
			
			$('#topMenu').find('.left-sidebar-top').removeClass('ie8-left-sidebar-top');
			$('#footerMessage').find('.footerMessageInner').removeClass('ie8-footerMessageInner');
			$('.pageWrapper').removeClass('ie8-pageWrapper');
			$('#actionWrap').removeClass('ie8-actionWrap');
			$('#popupWrapper').removeClass('ie8-popupWrapper');
			$('#overlay').removeClass('ie8-overlay');
			$('#calendarWrapper').find('.bcal-container').removeClass('ie8-bcal-container');
			
			$('#footerWrapperNew').removeClass('ie8-footerContent');
		}
	}
	namespace("fnb.functions.ie8",ie8);
});

var functionsArray =['Calendar','$','jQuery','_gsDefine','Ease','Power4','Strong','Quint','Power3','Quart','Power2','Cubic','Power1','Quad','Power0','Linear','TweenLite','TweenPlugin','TweenMax','TimelineLite','TimelineMax','BezierPlugin','CSSPlugin','BackOut','BackIn','BackInOut','SlowMo','SteppedEase','RoughEase','BounceOut','BounceIn','BounceInOut','CircOut','CircIn','CircInOut','ElasticOut','ElasticIn','ElasticInOut','ExpoOut','ExpoIn','ExpoInOut','SineOut','SineIn','SineInOut','horizontalScroller','namespace','ChameleonTable','onbeforeunload','changePage','verifyDateValid','verifyDateValidCustomString','confirmHoliday']