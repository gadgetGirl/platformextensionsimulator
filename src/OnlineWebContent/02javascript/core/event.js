
///-------------------------------------------///
/// developer: Donovan
///
/// Event Controller
///-------------------------------------------///
$(function(){
	function eventObject(sender) {
		this._sender = sender;
	}
	eventObject.prototype = {
		eventList: {},
		getEvent: function(eventName, create){
			if (!this.eventList[eventName]){
				if (!create) {
					return null;
				}
				this.eventList[eventName] = [];
			}
			return this.eventList[eventName];
		},
		attachEvent: function(eventName,handler,args) {
			var evt = this.getEvent(eventName, true);
			var func;
			switch(typeof(handler)) {
				case 'function':
					func = handler
				break;
				default:
					func = new Function(args,handler);
			};
			evt.push(func);
		},
		insertEvent: function(eventName, handler,args , position) {
			var evt = this.getEvent(eventName, true);
			var func;
			switch(typeof(handler)) {
				case 'function':
					func = handler
				break;
				default:
					func = new Function(args,handler);
			};
			evt.splice(position, 0, func);
		},
		detachEvent: function(eventName, handler) {
			var evt = this.getEvent(eventName);
			if (!evt) { return; }
			var getArrayIndex = function(array, item){
				for (var i = 0; i < array.length; i++) {
					if (array[i] && array[i] === item) {
						return i;
					}
				}
				return -1;
			};
			var index = getArrayIndex(evt, handler);
			if (index > -1) {
				evt.splice(index, 1);
			}
		},
		raiseEvent: function(eventName, eventArgs) {
			console.log('Events Controller -- Event Raised : '+eventName)
			
			if(eventArgs){
				if (eventArgs.url) { 
					eventArgs.url = eventArgs.url.replace(/'/gi, "'");
				}
			}
			
			if(typeof(this.getEventHandler(eventName))!='undefined'){
				if (this.getEventHandler(eventName)) {					
					this.getEventHandler(eventName)(this._sender, eventArgs);
				}
			}
		},
		getEventHandler: function(eventName) {
			var evt = this.getEvent(eventName, false);
			if (!evt || evt.length === 0) { return null; }
			var _handler = function(sender, args) {
				for (var i = 0; i < evt.length; i++) {
					evt[i](sender, args);
				}
			};
			return _handler;
		}
	};
	namespace("fnb.controls.events",eventObject);
});