///-------------------------------------------///
/// developer: Kevin
/// Namespacing object
///-------------------------------------------///
namespace = function (namespaceString,functionImplementation) {
    var parts = namespaceString.split('.'),
    parent = window,
    currentPart = '';    
            
	for(var i = 0, length = parts.length; i < length; i++) {
		currentPart = parts[i];
		parent[currentPart] = parent[currentPart] || {};
		if((i+1)==parts.length){
			parent[currentPart] = functionImplementation;
		}else{
			parent = parent[currentPart];
		}
	}
	return parent;
};

///---------------------------------///
/// developer: Donovan
///
/// AJAX MANAGER
///---------------------------------///
$(function(){
	function ajaxManager(){
		this.ajaxRequests =[];
		this.ajaxTimer;
	};
	ajaxManager.prototype = {
		addRequest: function(obj){
			this.ajaxRequests.push(obj);
		},
		removeRequest: function(obj){
			if( $.inArray(opt, fnb.utils.ajaxManager.ajaxRequests) > -1 )  fnb.utils.ajaxManager.ajaxRequests.splice($.inArray(opt,  fnb.utils.ajaxManager.ajaxRequests), 1);
		},
		checkQueue: function(){
			if(this.ajaxRequests.length) {
				this.run();
			}
		},
		run: function(){
			fnb.controls.controller.setCurrentEventsGroup(this.ajaxRequests[0].eventsGroup);
			fnb.controls.controller.setUrl(this.ajaxRequests[0].url);
			fnb.controls.controller.setTarget(this.ajaxRequests[0].target);
			fnb.controls.controller.setPreLoadingCallBack(this.ajaxRequests[0].preLoadingCallBack);
			fnb.controls.controller.setPostLoadingCallBack(this.ajaxRequests[0].postLoadingCallBack);
			fnb.controls.controller.setParams(this.ajaxRequests[0].params);
			fnb.controls.controller.setQueue(this.ajaxRequests[0].queue);
			fnb.controls.controller.setExpectResponse(this.ajaxRequests[0].expectResponse);
			this.ajaxRequests.splice(0,1);
			fnb.utils.loadUrl.load(fnb.controls.controller.getCurrentEventsGroup(),fnb.controls.controller.getUrl(),fnb.controls.controller.getTarget(),fnb.controls.controller.getPreLoadingCallBack(),fnb.controls.controller.getPostLoadingCallBack(),fnb.controls.controller.getParams(),fnb.controls.controller.getQueue(),fnb.controls.controller.getExpectResponse());
		},
        stop:  function() {
            this.ajaxRequests.requests = [];
        }
	};

	namespace("fnb.utils.ajaxManager",ajaxManager);
	
});
///-------------------------------///
/// developer: Donovan
///
/// AJAX LOAD CONTENT
///-------------------------------///
$(function(){
	function loadUrl(){
		this.xhr;
	};
	loadUrl.prototype = {
		load: function(eventsGroup,url,target,preLoadingCallBack,postLoadingCallBack,params,queue,expectResponse,preventDefaults){
			_this = this;
						
			if (typeof url  != 'undefined') {
				if(url.indexOf("?")>-1){
					url += "&targetDiv=" + $(target).attr("id");	
				}else{
					url += "?targetDiv=" + $(target).attr("id");
				}
			}else{
				url = params['alternateUrl'];
				url += "?targetDiv=" + $(target).attr("id");
			}
			
			$.ajaxSettings.data = [];
			$.ajaxSettings.traditional = false;
			$.ajaxSettings.cache = true;
			
			if(typeof params!= 'undefined'){
				$.ajaxSettings.data = params;
				$.ajaxSettings.traditional = true;
			}

			this.xhr = $.ajax({
				type: "POST",
				url: url,
				dataType:'html',
				beforeSend : function (jqXHR)
				{
					fnb.controls.controller.setXhr(jqXHR);
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlBeforeSend', 'beforeSend');
				},
				complete : function (jqXHR, textStatus)
				{
					fnb.controls.controller.setXhr(jqXHR);
					fnb.utils.ajaxManager.checkQueue();
				},
				success: function(data, textStatus, jqXHR) {
					var loadObj = {eventsGroup:eventsGroup,xhr: jqXHR,data:data,url: url,target:target,preLoadingCallBack:preLoadingCallBack,postLoadingCallBack:postLoadingCallBack,params:params,queue:queue,expectResponse:expectResponse,preventDefaults:preventDefaults};
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlSuccess', loadObj);
				},
				error: function(jqXHR, textStatus, errorThrown){
					var errorCode = 0;
					console.log('Error: '+jqXHR.status)
					switch (jqXHR.status) {
						case 0:
							if(errorThrown!="abort"){
								fnb.controls.controller.eventsObject.raiseEvent('loadError', {height:'134px',message: 'A result could not be retrieved from Online Banking. Please check your Internet connection before you continue. If you were performing a financial transaction, please check your transaction history to determine if the transaction was processed before you try again.', errors:[]});
							}
							break;
						case 500:
							errorCode = jqXHR.status*23;
							fnb.controls.controller.eventsObject.raiseEvent('loadError', {height:'134px',message: 'Internal server error.', errors:[{error: 'Error: '+errorCode}]});
							break;
						case 404:
							errorCode = jqXHR.status*23;
							fnb.controls.controller.eventsObject.raiseEvent('loadError', {height:'134px',message: 'Page not found.', errors:[{error: 'Error: '+errorCode}]});
							break;
						default:
							errorCode = jqXHR.status*23;
							fnb.controls.controller.eventsObject.raiseEvent('loadError', {height:'134px',message: 'A result could not be retrieved from Online Banking. Please check your Internet connection before you continue. If you were performing a financial transaction, please check your transaction history to determine if the transaction was processed before you try again.', errors:[{error: 'Error: '+errorCode}]});
							console.log('fnb.utils.loadUrl -- AjaxLoad Error: '+errorThrown)
					}
				}
			});
		},
		stop: function(){
			if(this.xhr){
				this.xhr.abort();
			}
		}
	};
	namespace("fnb.utils.loadUrl",loadUrl);
});

///-------------------------------///
/// developer: Donovan
///
/// Jquery LOAD CONTENT
///-------------------------------///
$(function(){
	function load(){

	};
	load.prototype = {
		loadUrl: function(){

			var currentEventsGroup = fnb.controls.controller.getCurrentEventsGroup();
			$(fnb.controls.controller.getTarget()).load(fnb.controls.controller.getUrl(), function() {
				fnb.controls.controller.eventsObject.raiseEvent(currentEventsGroup, 'success');
			});
		}
	};
	namespace("fnb.utils.load",load);
});

///-------------------------------///
/// developer: LM 
///
///	PayPal Functions
///-------------------------------///

/* This function opens a window, and allows us to close the window when moving to another state.
 * i.e. It enables us to keep track of the popup window. MG*/

$(function(){
	function paypal(){
		this.openedWindows = [];
	};
	paypal.prototype = {
		openWindow : function(url, windowName, params) {
			var me = this;
			this.openedWindows[windowName] = window.open(url, windowName, params);
		},	
		
		/* This function closes a window which was opened using the openedWindows function.*/
		closeWindow : function(windowName) {
			if (openedWindows[windowName] != null) {
				openedWindows[windowName].close();
			}
		}
	};
	namespace("fnb.utils.paypal.windows",paypal);
});

///-----------------------------------------------------------------------------------------///
/// developer: Donovan
///
/// FRAME FUNCTIONS -- TOP BUTTONS, FOOTER BUTTONS ETC
///-----------------------------------------------------------------------------------------///
$(function(){
	function frame(){

	};
	frame.prototype = {
		init: function(){	

		},
		removeHeaderButtons: function(){
			$(_headerWrapper).find('div[id^="headerButton"]').remove();
		},
		createHeaderObject: function(){
			var timeline;
			timeline = new TimelineMax();
			$('#headerButtonsWrapper').find("div[id^='headerButton']").each(function() {
				timeline.append( new TweenMax.to($(this), 0.2, {css:{top:'0'}, ease:Quart.easeOut}));
			});
		},
		clearFooterButtons: function(){
			if(!$('#formFooterButtons').hasClass('hideElement')){
				$('#formFooterButtons').addClass('hideElement');
				$('#forMore').addClass('visibilityHidden');
			}
		},
		showFooterButtons: function(){
			if($('#formFooterButtons').hasClass('hideElement')){
				$('#formFooterButtons').removeClass('hideElement');
				fnb.functions.isScrolling.checkPos();
				if(_isIE8) fnb.functions.ie8.doCheck();
			}
		},
		adjust: function(windowWidth){
			var sectionWidth;
			var windowWidthVal = windowWidth;
		},
		checkWindowsPhone: function(){
			if(_isMobile==true&&_browserName=="MSIE"){
				$(_footerWrapper).css({'position':'relative'})
			}
		}
	};
	namespace("fnb.utils.frame",frame);

});
///-------------------------------///
/// developer: Donovan
///
/// TOP TABS
///-------------------------------///
$(function(){
	function topTabs(){
		this.topScroller;
		this.topScrollerScrolling = false;
		this.topScrollerStops;
		this.topTabSelectedIndex = 0;
		this.topTabOldSelectedIndex = 0;
		this.tabsCount = 0;
		this.selectedTab;
		this.tabWidth = 0;
		this.tabDevisions = 10;
		this.sliderObject = {tabIndex: 0, scrollerIndex:0};
		this.link;
		this.label;
	};
	topTabs.prototype = {
		init: function(tabsCount){
			var parent = this;
			this.tabsCount = tabsCount;
			this.topScroller = new horizontalScroller();
			this.topScroller.scrollableParent = $(_topNavScrollable);
			this.topScroller.scrollerChildren = this.topScroller.scrollableParent.find('.gridCol');
			this.topScroller.scrollStopWidth = 200;
			this.topScroller.scrollSpeed = 500;
			this.topScroller.maxStops = 1;
			this.topScroller.moveTreshold = 0.40;
			this.topScroller.bindEvents();
			this.topScroller.enabled = false;

			this.attachEvents();
			this.createHeaderObject();
			this.adjust($(window).width());
		},
		attachEvents: function(){
			_this = this;
			$(_topNav).on('click touchstart', '.topTab', function(event) {
				fnb.utils.topTabs.checkSelectMode(event);
			});
		},
		checkSelectMode: function(event){
			if(_smallPort==true){
				setTimeout(function() {
					if(fnb.utils.topTabs.topScroller.moving == false)  fnb.utils.topTabs.delegateEvents(event);
				}, 200);
				return false;
			}else{
				fnb.utils.topTabs.delegateEvents(event);
			}
		},
		delegateEvents: function(event){
			var url;
			var label
			var tab;
			if($(event.target).hasClass('topTab')){
				tab = $(event.target);
				url = $(event.target).attr('data-value');
				label = $(event.target).find('a').html();
			}else{
				tab =  $(event.target).parent();
				url = $(event.target).parent().attr('data-value');
				label = $(event.target).html();
			}
			this.select(url,label,tab)
		},
		adjust: function(windowWidth){
		if(windowWidth>_siteMaxWidth&&_isMobile==false) windowWidth =_siteMaxWidth;
			var windowWidthVal = windowWidth-65;

			var sliderPosition;
			//for IE
			if(_isMobile==false&&_browserName=="MSIE"&&_browserVersion<='8'){
				var lastChild = $(_topNav).find($('.lastTab'));
				var firstChild = $(lastChild).parent().find(">:first-child");
				var firstChildWidth = $(firstChild).width();
				$(lastChild).width((firstChildWidth-$(lastChild).parent().children().length));
			}
			
			if (windowWidthVal <= _phoneWindowWidthMax) {
				_smallPort = true;
				_sliderOffset = 35;
				
				if(windowWidthVal <= _phoneWindowWidthMax && windowWidthVal > _phoneWindowWidthMed){
					this.tabDevisions = 7;
				}else if(windowWidthVal <= _phoneWindowWidthMed && windowWidthVal > _phoneWindowWidthMin){
					this.tabDevisions = 5;
				}else if(windowWidthVal <= _phoneWindowWidthMin){
					this.tabDevisions = 4;
				}
				
				var calcStops;
				calcStops = this.tabsCount/this.tabDevisions;
				this.topScrollerStops = Math.round(calcStops);

				var dividedTabWidth = windowWidthVal/this.tabDevisions;
				this.tabWidth = dividedTabWidth;
				$(_topNavScrollable).css({'min-width':dividedTabWidth*this.tabsCount});
				$(_topNav).find('li').width(dividedTabWidth);

				this.topScroller.maxStops = this.topScrollerStops;
				this.topScroller.scrollStopWidth = (dividedTabWidth*this.tabDevisions)-($('#leftScrollButton').width()+$('#rightScrollButton').width());
				this.topScroller.moveTo(0,1);
				this.topScroller.enabled = true;
				
				$(_topNavContainer).css({'width':windowWidthVal})
				
				if(!$('#leftScrollButton').is('*')&&!$('#rightScrollButton').is('*')){
					this.appendMenuButtons();
					$(_topNavWrapper).addClass('topNavWrapperMobi')
				}
				
				$("#topNavBottomBorderContainer").css({'width':((dividedTabWidth*10)*(this.topScrollerStops+2))});
				$("#topNavBottomBorderContainer").css({'left':-(windowWidthVal/2)});
				
			}else if(windowWidthVal > _phoneWindowWidthMax&&this.tabsCount>10){

				var calcStops;
				calcStops =10/this.tabDevisions;
				this.topScrollerStops = Math.round(calcStops);

				var dividedTabWidth = (windowWidthVal)/10;
				
				$(_topNavContainer).css({'width':windowWidthVal})
				var minWidth = (dividedTabWidth*this.tabsCount)+this.tabsCount;
				$(_topNavScrollable).css({'min-width':dividedTabWidth*minWidth});	
				
				$(_topNav).find('li').width(dividedTabWidth);
				
				this.topScroller.maxStops = this.topScrollerStops;
				this.topScroller.scrollStopWidth = dividedTabWidth*10;
				this.topScroller.moveTo(0,1);
				this.topScroller.enabled = true;

				if(!$('#leftScrollButton').is('*')&&!$('#rightScrollButton').is('*')){
					this.appendMenuButtons();
					$(_topNavWrapper).addClass('topNavWrapperMobi')
				}
				
				$("#topNavBottomBorderContainer").css({'width':(windowWidthVal*(this.topScrollerStops+5))});
				$("#topNavBottomBorderContainer").css({'left':-windowWidthVal});
				
				$(_topNavContainer).css({'width':windowWidthVal})
				
				$(_topNavWrapper).addClass('topNavWrapperOver10');
				
				this.tabWidth = dividedTabWidth;

			}else{
				if(_smallPort==true){
				
					$("#topNavMenuSlider").css({'width':''});
					$("#topNavBottomBorder").css({'width':''});

					_sliderOffset = 40;

					this.topScroller.internalEvent = true;
					this.topScroller.moveTo(0,0);
					this.topScroller.enabled = false;

					$(_topNavContainer).css({'width':''});
					$(_topNavScrollable).css({'min-width':''})
					$(_topNavScrollable).css({'-webkit-transform':''});
					$(_topNavScrollable).css({'-webkit-transition':''});

					if($('#leftScrollButton').is('*')&&$('#rightScrollButton').is('*')){
						$('#leftScrollButton').remove();
						$('#rightScrollButton').remove();
					}
					$(_topNav).find('li').width('');
					$(_topNavWrapper).removeClass('topNavWrapperMobi')
				}
				this.tabWidth = $(_topNav).find('li:eq('+this.topTabSelectedIndex+')').width();
				_smallPort = false;
			}
			
			if (windowWidthVal < 480) {
				if(_tinyPort==false){
					_tinyPort = true;
					_topOffset = '108';
				}
			}else{
				if(_tinyPort==true){
					_tinyPort =false;
					_topOffset = '134';
				}
			}
			
			sliderPosition =this.tabWidth*this.topTabSelectedIndex;
			$(_topNavIndicator).css({'width':(this.tabWidth)-2});
			$(_topNavIndicator).css({left:sliderPosition});
		},
		select: function(link,label,tab, noNav){
			if($(_topNavIndicator).hasClass('hideElement')){
				$(_topNavIndicator).removeClass('hideElement');
				$(_headerButtonsWrapper).children().find('span').removeClass('selectedButton');
			}
			fnb.utils.mobile.mobiSubtabInitialized = false;
			fnb.utils.mobile.popUpInitialized = false;

			this.topTabSelectedIndex = $(tab).index();
			this.selectedTab = tab;
			this.topTabOldSelectedIndex = this.topTabSelectedIndex;
			$(tab).parent().find('li').removeClass("selected");
			$(tab).toggleClass("selected", 1000);
			
			var sliderPos = $(tab).width()* this.topTabSelectedIndex;

			this.animateMenuSlider(link,label,sliderPos, noNav);
		},
		animateMenuSlider:function(link,label,pos, noNav){
			if(_smallPort){
				if (!noNav) fnb.utils.topTabs.load(link,label);
			}else{
				$(_topNavIndicator).animate({
					left:pos
				}, {
					duration: 300, 
					easing: 'swing',
					complete: function() {
						if (!noNav) fnb.utils.topTabs.load(link,label);
					}
				}); 
			}
		},
		load: function(link,label){
			var loadObj = {url: link,queue:false}
			fnb.controls.controller.eventsObject.raiseEvent('topTabSelect', loadObj);
		},
		appendMenuButtons: function(){
			var topLeftButton = $('<a id="leftScrollButton" class="leftScrollButton" onclick="fnb.utils.topTabs.scrollLeft(false)"></a>');
			var topRightButton = $('<a id="rightScrollButton" class="rightScrollButton" onclick="fnb.utils.topTabs.scrollRight(false)"><div class="rightScrollBottomBorder"></div></a>');
			$(topRightButton).appendTo($(_topNavWrapper));
			$(topLeftButton).prependTo($(_topNavWrapper));
		},
		scrollLeft: function(){
			this.topScroller.previous();
		},
		scrollRight: function(){
			this.topScroller.next();
		},
		createHeaderObject: function(){
			var timeline;
			timeline = new TimelineMax();
			$('#headerButtonsWrapper').find("div[id^='headerButton']").each(function() {
				timeline.append( new TweenMax.to($(this), 0.2, {css:{top:'0'}, ease:Quart.easeOut}));
			});
		},
		headerButtonSelect: function(button){
			$(_topNav).find('li').removeClass("selected");
			$(_headerButtonsWrapper).children().find('span').removeClass('selectedButton');
			$(button).find('span').addClass('selectedButton');
			if(!$(_topNavIndicator).hasClass('hideElement')) $(_topNavIndicator).addClass('hideElement');
			var url=$(button).parent().attr('data-value');
			
			if(typeof url=='undefined') url=$(button).attr('data-value');
			
			if($(button).hasClass('applyBtn')){
				
				fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', url);
				
			}else if($(button).parent().attr('id') == 'helpText'){
							
				return false;
				
			}else{
				
				fnb.controls.controller.eventsObject.raiseEvent('topButtonsLoadUrlToWorkspace', url);
				
			}
		}
	};
	namespace("fnb.utils.topTabs",topTabs);
});

///---------------------------------///
/// developer: Donovan
///
/// OVERLAYS
///---------------------------------///
$(function(){
	function overlay(){
		this.overlayWrapper;
		this.overlayPanel;
		this.overlayType;
		this.expanded;
	};
	overlay.prototype = {
		init: function(){	

		},
		show: function(){
			this.expanded = true;
			if($(_overlay).hasClass('hidden')){
				_this = this
				setTimeout(_this.display, 100);
			}
		},
		hide: function(){
			this.expanded = false;
			if(!$(_overlay).hasClass('hidden')){
				$(_overlay).addClass('hidden');
			}
		},
		display: function(){
			if(fnb.utils.overlay.expanded == true){
				$(_overlay).removeClass('hidden');
			}
		},
		adjust: function(){

		}
	};

	namespace("fnb.utils.overlay",overlay);
	
});

///----------------------------///
/// developer: Donovan
///
/// ERRORS
///----------------------------///
$(function(){
	function errorPanel(){
		this.errorMessage;
		this.errorHeight;
		this.errorObject = {};
	}
	errorPanel.prototype = {
		init: function(){	

		},
		show: function(errorObject){
			this.rawErrorObject = errorObject;
			this.errorMessage = this.rawErrorObject.message;
			this.errorHeight = this.rawErrorObject.height;
			this.errorObject = this.rawErrorObject.errors;
			this.errorExpanded = true;
			this.append();
		},
		hide: function(){
			if(!$(_errorPanel).hasClass('hidden')){
				var _this = this;
				TweenMax.to($(_errorPanel), 0.2, {css:{top:-$(_errorPanel).height()-26}, ease:Quart.easeOut,onComplete:_this.remove});
				this.errorExpanded = false;	
			}
		},
		remove: function(){
			$(_errorPanel).addClass('hidden');
		},
		setOnOpen: function(){
			$(_errorPanel).addClass('hidden');
		},
		append: function(){
			_this = this;
			var errorsString = '';
			if(typeof _this.errorObject=="object"){
				$.each(_this.errorObject, function(errorIndex, error) {
					errorsString += "<div class='errorWrapper'><div class='errorText'>"+this.error+"</div></div>";
				});
				$(_errorMessageWrapper).html(_this.errorMessage)
				$(_errorPanel).css({'min-height':_this.errorHeight});
			}else{
				errorsString += "<div class='errorWrapper'><div class='errorText'>"+this.rawErrorObject+"</div></div>";
				$(_errorPanel).css({'min-height':'134px'});
			}
			
		 	$(_errorsWrapper).html(errorsString)
			$(_errorPanel).css({'top':-$(_errorPanel).height()});
			$(_errorPanel).removeClass('hidden');
			TweenMax.to($(_errorPanel), 0.2, {css:{top:0}, ease:Quart.easeOut});
		}
	};
	namespace("fnb.utils.errorPanel",errorPanel);
});

///---------------------------------///
/// developer: Donovan
///
/// ACTION MENU
///---------------------------------///
$(function(){
	function actionMenu(){
		this.actionMenuExpanded = false;
		this.isPaging = false;
		this.actionButtonDisplaying = false;
		this.contentLabel ;
		this.isExpanding ;
	};
	actionMenu.prototype = {
		init: function(){	

		},
		set: function(){
			if($(_actionMenuWrapper).is('*')){
				fnb.utils.actionMenu.showButton();
				_actionMenu.adjust($(window).width());
			}
		},
		showHide: function(target){
			if(this.isExpanding!=true){
				if($(_actionMenuButton).hasClass('notExpanded')){
					$('#actionWrap').addClass('grid100');
					this.isExpanding=true;
					fnb.controls.controller.eventsObject.raiseEvent('actionMenuShow', target);
				}else{
					$('#actionWrap').removeClass('grid100');
					fnb.controls.controller.eventsObject.raiseEvent('actionMenuHide', target);
				}
			}
			
		},
		showButton: function(){
			if($(_actionMenuButton).is('*')){
				if($(_actionMenuButton).hasClass('hiddenContent')) $(_actionMenuButton).removeClass('hiddenContent');
				if(_isIE8) fnb.functions.ie8.doCheck(); 
			}
		},
		hideButton: function(){
			if($(_actionMenuButton).is('*')){
				if(!$(_actionMenuButton).hasClass('hiddenContent')) $(_actionMenuButton).addClass('hiddenContent');
			}
		},
		show: function(){
			var _this = this;
			if($(_actionMenuWrapper).is('*')){
				$('html').addClass('htmlForceBG');
				$(_actionMenuWrapper).parent().find('#moreOptionsDownArrow').removeClass('displayNone');
				$(_actionMenuWrapper).parent().find('#actionMenuBottomLabel').removeClass('displayNone');
				$('#actionMenu').find('.actionMenuCol').removeClass('displayNone');
				this.actionMenuExpanded = true;
				$('.pageWrapper').hide();
				$(_actionMenuWrapper).find('.moreOptionsDownArrow').addClass('visibleContent');
				var wrapperWidth;
				$('#actionMenuOrangeBanner').removeClass('offScreen');
				$(_actionMenuWrapper).removeClass('offScreen');
				$(_actionMenuButton).removeClass('notExpanded');
				$(_actionMenuButton).addClass('isExpanded');
				$(_actionMenuWrapper).addClass('actionTable');

				wrapperWidth = $(_pageContainer).width();

				function showComplete(){
					$('#actionMenuOrangeBanner').css({width:''});
					$(_actionMenuWrapper).css({width:'100%'}); 
					if($(_actionMenuWrapper).height()<$(window).height()&&_tinyPort==true) $(_actionMenuWrapper).height($(window).height()); 
					_this.isExpanding=false;
				}

				timeline = new TimelineMax({onComplete:showComplete});
				var _sliderOffset = $(_actionMenuButton).width();
				var actionButtonLeft = $(window).width()-_sliderOffset;
				if(actionButtonLeft>(_siteMaxWidth-_sliderOffset)&&_isMobile==false) actionButtonLeft = _siteMaxWidth-_sliderOffset;

				timeline.insertMultiple([new TweenMax.to($(_actionMenuWrapper), 0.3, {css:{width:'100%'}, ease:Quart.easeOut}),
														new TweenMax.to($('#actionMenuOrangeBanner'), 0.2, {css:{width:'100%'}})]);

				if(_browserName=="MSIE"&&_browserVersion<9){

				}else{
					new TweenMax.to($('#actionMenuIcon'), 0.3, {css:{rotation:180},ease:Circ.easeOut})
				}
				
				function resetActionMenuButton() {
					if($(_actionMenuButton).is('*')){
						if($(_actionMenuWrapper).find('.moreOptionsDownArrow').is('*')) new TweenMax.to($(_actionMenuWrapper).find('.moreOptionsDownArrow'), 0.5, {css:{bottom:'-38'}, ease:Bounce.easeOut, delay:0.1});
					}
				}

				TweenMax.delayedCall(0.3, resetActionMenuButton);

				$('html,body').scrollTop(0);
				
			}
		},
		hide: function(){
			if($(_actionMenuWrapper).is('*')){
				$('html').removeClass('htmlForceBG');
				this.actionMenuExpanded = false;
				if(!$(_actionMenuButton).hasClass('notExpanded')){
					$(_actionMenuWrapper).parent().find('.moreOptionsDownArrow').addClass('displayNone');
					$(_actionMenuWrapper).parent().find('.actionMenuBottomLabel').addClass('displayNone');
					$(_actionMenuButton).addClass('notExpanded')
					$(_actionMenuButton).removeClass('isExpanded');
					$('.pageWrapper').show();
					$(_actionMenuWrapper).find('.moreOptionsDownArrow').hide();
					$(_actionMenuWrapper).find('.moreOptionsDownArrow').css({'bottom':'0'})
					function hideComplete(){					
						$(_actionMenuWrapper).removeClass('actionTable');
						
						$(_actionMenuWrapper).addClass('offScreen');
						$('#actionMenuOrangeBanner').addClass('offScreen');
						///TweenMax.to( new TweenMax.to($(_actionMenuButton), 0.2, {css:{height:''}, ease:Bounce.easeOut}) );
						$(_actionMenuWrapper).css({'min-height':''}); 
					}

					$('#main').height('');
					$(_actionMenuUrlWrapper).removeClass('visibleContent');
					$(_actionMenuUrlWrapper).html('');
						
					timeline = new TimelineMax({onComplete:hideComplete});

					timeline.insertMultiple([new TweenMax.to($(_actionMenuWrapper), 0.2, {css:{width:'0%'}}),
											new TweenMax.to($('#actionMenuOrangeBanner'), 0.2, {css:{width:'0%'}})
											]);	
					
					if(_browserName=="MSIE"&&_browserVersion<9){
					
					}else{
						new TweenMax.to($('#actionMenuIcon'), 0.2, {css:{rotation:0},ease:Circ.easeOut})
					}
					$(_actionMenuUrlWrapper).height('');
				}
			}
		},
		loadTargetToActionMenu: function(url,target){
			if(fnb.hyperion.utils.actionMenu.active==true){
				this.contentLabel = $(target).find('p').text();
				fnb.controls.controller.eventsObject.raiseEvent('loadToActionMenu', url)
			}else{
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{target:_main,url:url});
			}
		},
		loadTargetToActionMenuComplete: function(){
			var headingText=$(_actionMenuUrlWrapper).find('h1');
			var headingTextValue = $(headingText).text();
			$(_actionMenuUrlWrapper).prepend('<div class="actionMenuLeftBar borderImgRightWhite"></div>')
			$(_actionMenuUrlWrapper).find('.actionMenuLeftBar').prepend('<p>'+headingTextValue+'</p>')
			$(_actionMenuUrlWrapper).find('.actionMenuLeftBar').prepend('<h2>'+this.contentLabel+'</h2>')
			$(_actionMenuUrlWrapper).prepend('<div id="actionTableClose" onclick="fnb.utils.actionMenu.clearLoadedActionBar(this)"></div>')
			$(headingText).remove();
			$(_actionMenuButton).addClass('hiddenContent');
			$(_actionMenuUrlWrapper).addClass('visibleContent');
			$('#actionMenu').find('.actionMenuCol').addClass('displayNone');
		},
		adjust: function(windowWidth){
			if(this.actionMenuExpanded == true){
				if(_smallPort==true){
					$(_actionMenuWrapper).find('.actionMenuContents').css({'height':''});
					$(_actionMenuWrapper).css({'min-height':''}); 
				}
			}
		},
		clearLoadedActionBar : function(target) {
			$(_actionMenuUrlWrapper).removeClass('visibleContent');
			$(target).parent().html('');
			$(_actionMenuWrapper).find('.actionMenuColUp').addClass('opacity100');
			$(_actionMenuButton).show();
			$('#actionMenu').find('.actionMenuCol').removeClass('displayNone');
			$(_actionMenuButton).removeClass('hiddenContent');
			$(_actionMenuWrapper).find('.actionMenuColUp').show();
			$('#main').show();
		}
	};
	namespace("fnb.utils.actionMenu",actionMenu);

});

///-------------------------------///
/// developer: Donovan
///
/// Handle Navigation Error
///-------------------------------///
$(function(){
	function navError(){

	};
	navError.prototype = {
		validate: function(xhr,postLoadingCallBack,eventsGroup){
			var responseHeaders = xhr.getAllResponseHeaders();
			var navErrorCode = xhr.getResponseHeader("NAV_ERROR_CODE");
			var navErrorMessage = xhr.getResponseHeader("NAV_ERROR_MESSAGE");
			var navDetailMessage = "";
			try{
				if(xhr.getResponseHeader("NAV_DETAIL_MESSAGE")!=null){
					navDetailMessage = xhr.getResponseHeader("NAV_DETAIL_MESSAGE");
				}
			}catch(e){
				console.log("Error while trying to read NAV_DETAIL_MESSAGE from response");
			}
			if(navErrorCode!=null && navErrorCode!=0){
				if (navErrorCode == 4 || navErrorCode == 5) {
					var errorMessage = navDetailMessage +"<br/>"+ xhr.responseText;
					if ((navDetailMessage == "null")||(navDetailMessage == "Invalid Parameter")) errorMessage = xhr.responseText; 
					if (errorMessage == "<br/>") errorMessage = "(E-" + navErrorCode +") "+  navErrorMessage +"<br/>"+ xhr.responseText;
					fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some required fields are not valid...', errors:[{error: errorMessage}]});
				}
				else if (navErrorCode == 1544) {
					var errorMessage = navDetailMessage +"<br/>"+ xhr.responseText;
					fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'OTP Sent...', errors:[{error: errorMessage}]});
				}
				else {
					var errorMessage
					if (navDetailMessage != "" && navDetailMessage != "null" && navDetailMessage != null) {
						errorMessage = "(E-" + navErrorCode +") "+  navDetailMessage +"<br/>"+ xhr.responseText;
					}
					else {
						errorMessage = "(E-" + navErrorCode +") "+ navErrorMessage +"<br/>"+ xhr.responseText;
					}
					fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
				}
				if(typeof(postLoadingCallBack)=="function"){postLoadingCallBack(eventsGroup,xhr)};
				return false;
			}
			return true;

		}
	};
	namespace("fnb.utils.navError",navError);
});

///-------------------------------///
/// developer: Donovan
///
/// Handle OTP Error
///-------------------------------///
$(function(){
	function handleOTP(){

	};
	handleOTP.prototype = {
		validate: function(xhr){
			var screenType = xhr.getResponseHeader("SCREEN_TYPE");
			if(screenType!=null && screenType=="OTP"){ 
				var r = jQuery.parseJSON(xhr.responseText);
				fnb.utils.otp.primary = r.otpPrimary;
				fnb.utils.otp.secondary = r.otpSecondary;
				var intoWorkspace;
				if (fnb.hyperion.utils.eziPanel.active == false)
					intoWorkspace = true;
				else if (fnb.hyperion.utils.eziPanel.active == true)
					intoWorkspace = false;
				fnb.controls.controller.eventsObject.raiseEvent('otpShow', {url: '/banking/Controller?nav=navigator.otp.OtpLanding&ezi='+intoWorkspace})
				return true;
			}else {
				
			}
			return false;
		}
	};
	namespace("fnb.utils.handleOTP",handleOTP);
});

///-------------------------------///
/// developer: Donovan
///
///OTP
///-------------------------------///
$(function(){
	function otp(){
		this.primary;
		this.secondary;
	};
	otp.prototype = {
		complete: function(){
			$(_errorsWrapper).find('.cellNumber').html(this.primary);
			$(_errorsWrapper).find('.email').html(this.secondary);
		},
		submit: function(formname,intoWorkspace){
			fnb.utils.eziSlider.submit(formname,intoWorkspace,'',true);
		}
	};
	namespace("fnb.utils.otp",otp);
});
///---------------------------------///
/// developer: Donovan
/// developer: Vaughan - 21/6/2013 (modified adjust method)
///
/// EZI SLIDER
///---------------------------------///
$(function(){
	var counter = 0;
	function eziSlider(){
		this.params;
		this.expanded=false;
		counter = 0;
	};
	eziSlider.prototype = {
		init: function(windowWidth){
			counter = 0;
		},
		show: function(){
			if($(_eziWrapper).parent().parent().hasClass('hidden')){
				this.expanded = true;
				var _this = this;
				$(_eziWrapper).parent().parent().removeClass('hidden');
				TweenMax.to($(_eziWrapper), 0.2, {css:{right:0}, ease:Quart.easeOut,onComplete:_this.showButtons});
			}
			counter = 0;
		},
		showButtons: function(){
			if($('#eziPannelButtonsWrapper').is('*')) $('#eziPannelButtonsWrapper').removeClass('hidden');
		},	
		hide: function(){
			var _this = this;
			if(!$(_eziWrapper).parent().parent().hasClass('hidden')){
				this.expanded = false;
				if($('#eziPannelButtonsWrapper').is('*')) $('#eziPannelButtonsWrapper').addClass('hidden');
				var wrapperWidth = 0.40*$(_pageContainer).width();
				if(_smallPort==true){
					wrapperWidth = $(_pageContainer).width()-25;
				}
				var parent = this;
				TweenMax.to($(_eziWrapper), 0.2, {css:{right:-wrapperWidth}, ease:Quart.easeOut,onComplete:_this.remove});
			}
		},
		remove: function(){
			$(_eziWrapper).parent().parent().addClass('hidden');
			$(_eziProgressWrapperContents).html('');
		},
		submit: function(formName, intoWorkspace,buttonTarget,preventDefaults){
			if(!extraOptions) var extraOptions = {};

			if(typeof intoWorkspace=='undefined') intoWorkspace=false;
			
			if (intoWorkspace == 'true'||intoWorkspace == true) {
				
				fnb.functions.submitFormToWorkspace.submit(formName,'','',extraOptions,preventDefaults);
				
			}else {
				
	            extraOptions.keepFooter = true;
	            
				fnb.controls.controller.eziSubmitForm(formName, _eziProgressWrapperContents, '', '', buttonTarget,extraOptions,preventDefaults);
			}
		},
		checkLoadError: function(){
			var _this=this;
			setTimeout(function() {
				if(progressBar.progressActive==true){
					counter++;
					if (counter < fnb.clientTimeout) _this.checkLoadError();
						else  {
							fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '');
							counter = 0;
						}
				}
				else if (fnb.utils.errorPanel.errorExpanded == true) {
					fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '');
					counter = 0;
				}
			}, 500);
		}
	};
	namespace("fnb.utils.eziSlider",eziSlider);
});

///---------------------------------///
/// developer: Donovan
///
/// POPUPS
///---------------------------------///
$(function(){
	function popup(){
		this.height;
	};
	popup.prototype = {
		init: function(){	

		},
		show: function(){
			if($(_popupWrapper).hasClass('hidden')){
				setTimeout(function() {
					fnb.utils.popup.adjust($(_popupWrapper).outerHeight(true));
				}, 500);
			}
		},
		hide: function(){
			if(!$(_popupWrapper).hasClass('hidden')){
				$(_popupWrapper).addClass('hidden');
			}
			this.clear();
			if($('#selectorDeviceHolderWrapper')){
				if(!$('#selectorDeviceHolderWrapper').hasClass('hidden')){
					$('#selectorDeviceHolderWrapper').addClass('hidden')
				}
			}
		},
		clear: function(){
			$(_popupWrapper).html("");
		},
		adjust: function(height){
			var topVal = ($(window).height()-height)/2;
			$(_popupWrapper).css({'top':topVal+'px'});
			$(_popupWrapper).removeClass('hidden');
		},
		orientationChanged: function(){
			if(!$(_popupWrapper).hasClass('hidden')){
				setTimeout(function() {
					fnb.utils.popup.adjust($(_popupWrapper).outerHeight(true));
				}, 500);
			}

		}
		
	};

	namespace("fnb.utils.popup",popup);
});

///---------------------------------///
/// developer: Donovan
///
/// HORIZONTAL SCROLLER
///---------------------------------///
$.support.touch = ('ontouchstart' in window );

var horizontalScroller = function() {
    this.startX = null;
    this.endX = null;
    this.startOffset = null;
    this.scrollableParent = null;
    this.scrollerChildren = null;
    this.x=0;
    this.currentIndex =0;
	this.scrollSpeed =0;
    this.fixedStops = true;
    this.scrollStopWidth = 0;
    this.maxStops = 0;
    this.moveTreshold = 0.3;
    this.isTouch = $.support.touch;
    this.tap_treshold = 0;
    this.tap = function(){ };
    this.afterStop =  function(index){  };
    this.doVertical = false;
    this.startElement = null;
    this.bindLive = false;
	this.enabled = false;
	this.moving = false;
	this.initialized = false;
	this.internalEvent = false;
	
    var parent = this;

    function isset(v){
        return(typeof v != 'undefined');
    }

    function mouseX(event){
        return (isset(event.originalEvent.targetTouches)) ? event.originalEvent.targetTouches[0].pageX : event.pageX;
    }

    this.bindEvents = function(){
        if(this.isTouch){
           if(_browserName!='BlackBerry'&&_browserVersion!=100){
				this.bindTouchEvents();
			}
        } else {
            this.bindClickEvents();
        }
    };

    function start(e){
        e.preventDefault();
        parent.startElement = e.target;  
        parent.startX = parent.endX = mouseX(e);
        if($.browser.webkit){
            parent.scrollableParent.css("-webkit-transition-duration", "0s");
        }

        if(parent.isTouch) {
            $(document).bind({
                'touchmove': move,
                'touchend': end,
                'touchcancel': cancel
            });
        } else {
            $(document).bind({
                'mousemove': move,
                'mouseup': end,
                'mouseleave': cancel
            });
        }
    };

    function move(e){
		if(parent.enabled){
			parent.moving = true;
			e.preventDefault();
			if(parent.startX !== null && parent.scrollableParent !== null){
				parent.endX = mouseX(e);
				var val = parent.x+(parent.endX-parent.startX);
				if($.browser.webkit){
					parent.scrollableParent.css("-webkit-transform", "translate3d("+val +"px,0px,0px)");
				} else {
					parent.scrollableParent.css({'left':val+'px'});
				}
			}
		}
    };

    function end(e){
		
        if(parent.fixedStops && parent.startX !== null && parent.endX !== null && parent.scrollableParent !== null){
            parent.moveToClosest();
            parent.startElement = e.target;  
            var moveX = Math.abs(parent.startX - parent.endX);
            if (moveX <= parent.tap_treshold * parent.scrollStopWidth) {
                if( parent.startElement != null ) { parent.tap( parent.startElement ); }
            }
        }

        parent.startX = parent.endX = parent.startElement = null;
        
        if(parent.isTouch) {
            $(document).unbind({
                'touchmove': move,
                'touchend': end,
                'touchcancel': cancel
            });
        } else {
            $(document).unbind({
                'mousemove': move,
                'mouseup': end,
                'mouseleave': cancel
            });
        }

    };

    function cancel(e){
        e.preventDefault();
        if(parent.fixedStops && parent.startX !== null && parent.endX !== null && parent.scrollableParent !== null){
            parent.moveToClosest();
        }
        parent.startX = parent.endX = parent.startElement = null;
    };

    this.moveToClosest = function (){
        var moveX = this.startX-this.endX,
            currI = Math.round((-1*this.x) / this.scrollStopWidth),
            newI = currI,
            newloc = this.scrollStopWidth*(currI);

        if(moveX > this.moveTreshold*this.scrollStopWidth && currI+1 <= (this.maxStops)){
            newI = currI+1;
        }

        if(((-1)*moveX) > this.moveTreshold*this.scrollStopWidth && currI-1 >= 0){
             newI = currI-1;
        }

        newloc = Math.round(this.scrollStopWidth*(newI));
        this.currentIndex = newI;
        this.x = -1*(newloc);
  
        if($.browser.webkit){
			this.scrollableParent.css("-webkit-transition-duration", "0."+this.scrollSpeed+"s");
			this.scrollableParent.css("-webkit-transform", "translate3d("+(-1*newloc) +"px,0px,0px)");
			setTimeout(function() {
				parent.moving = false;
			}, this.scrollSpeed); 
        } else {
			this.scrollableParent.stop().animate({'left': (-1*newloc)},{ duration: this.scrollSpeed,  complete: function() { parent.moving = false; }});
        }

        setTimeout ($.proxy( function () { this.afterStop(newI); }, this ), this.scrollSpeed);

    };

    this.moveTo = function(index, nostop){
		if(this.initialized == false||this.internalEvent == true){
			var newloc = this.scrollStopWidth*(index);
			if($.browser.webkit){
				if(_browserName!='BlackBerry'&&_browserVersion!=100){
					this.scrollableParent.css("-webkit-transition-duration", "0."+this.scrollSpeed+"s");
					this.scrollableParent.css("-webkit-transform", "translate3d("+(-1*newloc) +"px,0px,0px)");
					setTimeout(function() {
						parent.moving = false;
					}, this.scrollSpeed);
				}else{
					this.scrollableParent.css({'left':newloc});
				}
			} else {
				this.scrollableParent.stop().animate({'left': (-1*newloc)},{ duration: this.scrollSpeed,  complete: function() {   parent.moving = false; }});
			}
			this.currentIndex = index;
			this.x = -1*(newloc);
			if(typeof nostop == "undefined") { setTimeout ($.proxy( function () { this.afterStop(index); }, this ), 250); }
			this.internalEvent = false;
			this.initialized = true;
		}
    };

    this.next = function(){
        if(this.currentIndex+1 <= this.maxStops){
			this.internalEvent = true;
			this.moving = true;
            this.moveTo(this.currentIndex+1);
        }
    };

    this.previous = function(){
        if(this.currentIndex-1 >= 0){
			this.internalEvent = true;
			this.moving = true;
            this.moveTo(this.currentIndex-1);
        }
    };

    this.centerIndex = function (index){
		if (this.scrollableParent !== null){
			if(isset(index)){
				this.currentIndex = index;
			} else {
				index = this.currentIndex;
			}
			var loc = -1*((index)*this.scrollStopWidth);
			if($.browser.webkit){
				this.scrollableParent.css({
					"-webkit-transform": "translate3d("+loc+"px,0px,0px)",
					"-webkit-transition-duration": "0s"
				});
			} else {
				this.scrollableParent.stop().css('left',loc+'px');
			}
			this.x = loc;
		}
    };

    this.bindTouchEvents = function(){
        if(this.scrollerChildren !== null){
            if (this.bindLive) {
                this.scrollerChildren.live({
                    'touchstart': start
                });
            } else {
                this.scrollerChildren.bind({
                    'touchstart': start
                });
            }
        }
    };

    this.bindClickEvents = function() {
        if(this.scrollerChildren !== null){
            if (this.bindLive) {
                this.scrollerChildren.live({
                    'mousedown': start
                });
            } else {
                this.scrollerChildren.bind({
                    'mousedown': start
                }); 
            }
        }
    };
};
///-------------------------------///
/// developer: Donovan
///
/// Subtabs
///-------------------------------///
$(function(){
	function subtabs(){

	};
	subtabs.prototype = {
		select: function(target){
			var url = target.attr('data-value');
			var disabled = target.attr('data-disabled');
			
			if(typeof disabled=='undefined'){
				disabled = target.parent().attr('data-disabled')
			}
			if(!disabled) {
				if(typeof url=='undefined'){
					url = target.parent().attr('data-value');
				}if(!fnb.utils.mobile.subtabs.subTabTopScroller){
					fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen',url)
				}else{
					setTimeout(function() {
						if(fnb.utils.mobile.subtabs.subTabTopScroller.moving == false) fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen',url);
					}, 200);
				}
			}
		}
	};
	namespace("fnb.utils.subtabs",subtabs);
});
///----------------------------///
/// developer: Donovan
///
/// MOBILE EXTENTIONS
///----------------------------///
$(function(){
	function mobile(){
		var expandedTableRowButton;
		var expandedTableRow;
		var expandedTableRowId;
		var tableRowExpanded = undefined;
		
		var mobiSubtabInitialized = false;
		var mobiSubtabIndex;
		
		var switcherExpanded;
		var targetSwitcher;
		
		var targetSwitcherWrapperHeight;
		
		var popUpInitialized;
		var popUpExpanded;
		var popUpObjects = [];
		var popEventsBound = false;
		
		this.subTabTopScroller;
		
		this.headerControlsMouseDown = false;
		this.headerControlsExpanding = false;;
		this.headerControlsExpanded = false;
		
		this.mobileMoved;
		this.mobileStartX;
		this.mobileStartY;
		
		this.originalCoord = { x: 0, y: 0 };
        this.finalCoord = { x: 0, y: 0 };
		this.threshold = {x: 30, y: 10};

		this.subtabScrollerApplied = false;
	}
	mobile.prototype = {
		adjust: function(windowWidth){
			this.subtabs.adjust(windowWidth);
			this.utils.adjust();
			this.table.adjust(windowWidth);
			this.headerControls.adjust(windowWidth);
		}
	};
	mobile.prototype.table = {
		init : function() {
			
		},
		expandRow: function(row){
			if(this.tableRowExpanded==true&&$(row).parent().parent().parent().attr('id')!=this.expandedTableRowId){
				this.contractRow(this.expandedTableRow);
			}
			this.expandedTableRowButton = row;
			this.expandedTableRowId = $(this.expandedTableRowButton).parent().parent().parent().attr('id');
			this.expandedTableRow = $(this.expandedTableRowButton).parent();
			
			if(this.tableRowExpanded==false||typeof(this.tableRowExpanded)=="undefined"){
				$(this.expandedTableRow).css({height:'auto',display:'block'});
				var expandableHeight = $(this.expandedTableRow).height();
				$(this.expandedTableRow).css({height:'0'})
				$(this.expandedTableRow).find('.phoneContentButton').addClass('greyBack');
				$(this.expandedTableRow).animate({
					height:expandableHeight
				}, {
					duration: 300, 
					easing: 'swing',
					step: function( step ){

					},
					complete: function() {
					}
				}); 
				this.tableRowExpanded = true;
			}else{
				this.contractRow(this.expandedTableRow);
			}
		},
		contractRow: function(row){
			this.tableRowExpanded = false;
			$(row).find('.phoneContentButton').removeClass('greyBack');
 			$(row).animate({
				height:0
			}, {
				duration: 300, 
				easing: 'swing',
				step: function( step ){

				},
				complete: function() {

				}
			}); 
		},
		adjust: function(windowWidth){
			if(_smallPort){
				if(this.tableRowExpanded==true){
					$(this.expandedTableRow).css({'height':'auto'});		
				}
			}
		}
	};
	mobile.prototype.subtabs = {
		init : function(tabIndex,label) {
			
		},
		bind : function() {
			
		},
		destroy : function() {

		},
		select : function() {
			
		},
		scrollLeft: function(){
			this.subTabTopScroller.previous();
		},
		scrollRight: function(){
			this.subTabTopScroller.next();	
		},
		adjust: function(windowWidth){
			if (windowWidth < _phoneWindowWidthMax) {
				var scrollerWidth = Math.round($('#pageContent').width()-60);
				if($(_subTabsScrollable).children().length>=3){
					this.subTabTopScroller = new horizontalScroller();
					this.subTabTopScroller.scrollableParent = $(_subTabsScrollable);
					this.subTabTopScroller.scrollerChildren = $(_subTabsScrollable).children();
					this.subTabTopScroller.scrollStopWidth = 200;
					this.subTabTopScroller.scrollSpeed = 500;
					this.subTabTopScroller.maxStops = 1;
					this.subTabTopScroller.moveTreshold = 0.20;
					this.subTabTopScroller.bindEvents();
					this.subTabTopScroller.enabled = true;
					$(_subTabsScrollable).parent().parent().find('.subTabScrollLeft').removeClass('hidden');
					$(_subTabsScrollable).parent().parent().find('.subTabScrollRight').removeClass('hidden');
					var tabWidth = Math.round((scrollerWidth/3)*$(_subTabsScrollable).children().length)+9;
					if(this.subTabTopScroller){
						this.subTabTopScroller.maxStops = Math.round($(_subTabsScrollable).children().length/3)
						this.subTabTopScroller.scrollStopWidth = scrollerWidth;
					}
				}
				this.subtabScrollerApplied = true;
				$(_subTabsScrollable).parent().width(scrollerWidth);
				$(_subTabsScrollable).width(tabWidth);
				var childrenWidths = Math.round($('#subTabsContainer').width()/3);
				$(_subTabsScrollable).children().css({'width':childrenWidths+'px'});
				if($(_subTabsScrollable).find('div:eq(0)').outerWidth(true)>childrenWidths){
					var newWidth = $(_subTabsScrollable).find('div:eq(0)').outerWidth(true)-childrenWidths;
					newWidth = childrenWidths-newWidth;
					$(_subTabsScrollable).children().css({'width':newWidth+'px'});
				}
			}else{
					if(this.subtabScrollerApplied==true){
						this.subtabScrollerApplied = false;
						if(this.subTabTopScroller) this.subTabTopScroller = undefined;
						$('#tableHeaderUtils').height('');
						$('#tableHeaderUtils').css({'overflow':'visible'});
						$(_subTabsScrollable).parent().parent().find('.subTabScrollLeft').addClass('hidden');
						$(_subTabsScrollable).parent().parent().find('.subTabScrollRight').addClass('hidden');
						$(_subTabsScrollable).width('auto');
						$(_subTabsScrollable).parent().width('auto');
						$(_subTabsScrollable).children().css({'width':''});
					}
			}
		}
	};
	mobile.prototype.switcher = {
		init : function(switcher) {
			
		},
		expandSwitcher : function(switcher) {
			this.targetSwitcher = switcher;
			this.targetSwitcherParent = $(switcher).closest('#tableHeaderUtils');
			this.targetSwitcherWrapper = $(switcher).parent().find('.tableSwitcherItemsContainer');
			if(this.switcherExpanded==false||typeof(this.switcherExpanded)=="undefined"){
				$(this.targetSwitcher).addClass('mobi-dropdown-trigger-expanded');
				this.originalHeight = $(this.targetSwitcherParent).height();
				$(this.targetSwitcherWrapper).css({height:'auto'});
				$(this.targetSwitcherParent).css({height:'auto'});
				var expandableHeight = $(this.targetSwitcherParent).height()+39;
				$(this.targetSwitcherParent).css({height:this.originalHeight+'px'});
				$(this.targetSwitcherParent).animate({
					height:expandableHeight
				}, {
					duration: 300, 
					easing: 'swing',
					step: function( step ){
					
					}
				});
				this.switcherExpanded = true;
			}else{
				this.contractSwitcher();
			}
		},
		contractSwitcher: function(){
			_this = this;
			this.switcherExpanded = false;
			$(this.targetSwitcher).removeClass('mobi-dropdown-trigger-expanded');
 			$(this.targetSwitcherWrapper).animate({
				height:'28px'
			}, {
				duration: 300, 
				easing: 'swing',
				step: function( step ){

				},
				complete: function() {
					$(_this.targetSwitcherParent).height(_this.originalHeight);
				}
			}); 
		},
		destroy : function() {

		},
		adjust: function(windowWidth){
			
		}
	};
	mobile.prototype.headerControls = {
		mouseDown : function(event) {
			if(_smallPort == true){
				this.headerControlsMouseDown=true;
				var yPos = this.pageY(event)

				this.headerControlsStartPosition = yPos;
				this.headerControlsPrevPosition = yPos;
			}
		},
		mouseMove : function(event) {
			if(this.headerControlsMouseDown==true){
				var yPos = this.pageY(event)
				this.headerControlsPagePosition = yPos - this.headerControlsStartPosition;
				if(yPos>this.headerControlsPrevPosition){
					this.menuSlideDown();
				}else{
					this.menuSlideUp();
				}
			}

		},
		menuSlide : function(event) {
			if(this.headerControlsExpanding!=true){
				event.preventDefault();
				if(!$('.tableHeaderControls').hasClass('tableHeaderControlsExpanded')){
					this.headerControlsExpanding = true;
					$('.tableHeaderControls').addClass('tableHeaderControlsExpanded');
					this.menuSlideDown();
				}else{
					$('.tableHeaderControls').removeClass('tableHeaderControlsExpanded');
					this.menuSlideUp();
				}
			}
		},
		menuSlideDown : function() {
			var _this=this;
			$('.tableHeaderControls').height('auto');
			var menuHeight = $('.tableHeaderControls').height();
			if(menuHeight<30) menuHeight = 30;
			$('.tableHeaderControls').height(5);
			menuHeight = menuHeight+29;
			$('.tableHeaderControls').animate({
					height:menuHeight
			}, {
				duration: 200, 
				easing: 'swing',
				complete: function() {
					_this.headerControlsExpanding = false;
					_this.headerControlsExpanded = true;
				}
			});
		},
		menuSlideUp : function() {
			$('.tableHeaderControls').animate({
					height:25
			}, {
				duration: 200, 
				easing: 'swing',
				complete: function() {
					_this.headerControlsExpanded = false;
				}
			}); 
		},
		mouseUp : function(event) {
			if(this.headerControlsMouseDown==true){
				this.headerControlsMouseDown=false;
			}
		},
		pageY: function(event){
			return ( typeof (event.originalEvent.targetTouches)!= 'undefined') ? event.originalEvent.targetTouches[0].pageY : event.pageY;
		},
		scrollRight: function(){
			
		},
		adjust: function(windowWidth){
			if(windowWidth>_phoneWindowWidthMax&&this.headerControlsExpanded==true){
				$('.tableHeaderControls').height('');
				$('#subTabsContainer').width('');
			}
		}
	};
	mobile.prototype.popup = {
		init : function() {
			
		},
		show : function(elements) {
			$('#footerMessage').addClass('hideElement');
			$('#footerWrapper').addClass('hideElement');
			fnb.utils.mobile.popUpObjects = [];
			this.appendElements(elements);
			fnb.utils.mobile.popUpInitialized = true;
			fnb.utils.mobile.popUpExpanded = true;
			fnb.controls.controller.eventsObject.raiseEvent("mobileHideDefaults",'');
			fnb.hyperion.utils.footer.hide(fnb.hyperion.controller.footerButtonGroup);
		},
		hide : function(defaults) {
			if(fnb.utils.mobile.popUpExpanded==true){
				if(defaults==true){
					fnb.controls.controller.eventsObject.raiseEvent("mobileShowDefaults",'');
				}else{
					fnb.controls.controller.eventsObject.raiseEvent("mobileHideDefaults",'');
				}
				fnb.utils.mobile.popUpExpanded = false;
				if(currentPageTotal>0) $('#footerMessage').removeClass('hideElement');
				$('#footerWrapper').removeClass('hideElement');
				$('#mobiPopupWrapper').remove();
				$('body').height('');
				fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup);
			}
		},
		appendElements : function(elements) {
			var thisParent = this;
			var mobiPopupWrapper = $("<div id='mobiPopupWrapper' class='mobiPopupWrapper'><div class='mobiPopupInnerWrapper'></div><div id='eziPannelButtonsWrapper'><div id='eziPannelButtons'><div class='gridCol footerBtn mobiHiddenFooterButton'  onclick='fnb.utils.mobile.popup.hide(true)' ><div class='actionMenuButtonIcon'></div><a onclick='fnb.utils.mobile.popup.hide(true)' target='_self' id='mainBtnHref' href='#'>Close</a></div></div></div></div>").appendTo( $('#pageContent') );
			$.each(elements, function(elementIndex, elementObject) {
				fnb.utils.mobile.popUpObjects.push($(elementObject.element));
				var clonedObj = $(elementObject.element).clone();
				clonedObj.find('input').removeClass('nonSubmittable');
				var clonedObjOnclick  = clonedObj.find('a').attr('onclick');
				if(typeof clonedObjOnclick!='undefined'){
					clonedObj.find('a').attr('onclick','')
					clonedObj.attr('onclick',clonedObjOnclick)
				}; 
				var footerObj = false;
				$.each(elementObject.attributes, function(attributeIndex, attributeObject) {
					if(attributeObject.attributeName =='html'){
						footerObj = true;
						clonedObj.find(attributeObject.id).html(attributeObject.attributeValue);
					}else{
						clonedObj.find(attributeObject.id).attr(attributeObject.attributeName, attributeObject.attributeValue);
					}
				});
				if(footerObj==true){
					clonedObj.prependTo($('#mobiPopupWrapper').find('#eziPannelButtons'));
				}else{
					clonedObj.prependTo($('#mobiPopupWrapper').find('.mobiPopupInnerWrapper'));
				}
				if(elementObject.type == "footer"){
					$('<div class="actionMenuButtonIcon"></div>').prependTo($(clonedObj));
				}
				$(elementObject.element).find('input').addClass('nonSubmittable');
			 });
		},
		destroy : function() {

		},
		adjust: function(windowWidth){
			
		}
	};
	mobile.prototype.properties = {
		init : function() {			
			if(_isMobile){
				if(this.supportOverflowScrolling()==false){ this.removeFixed();}else{this.checkClasses()};
			}
		},
		removeFixed : function() {
		 	$(_formFooterButtons).addClass('positionRelative');
			$(_footerWrapper).addClass('positionRelative');
			$(_formFooterButtons).addClass('footerButtonsNoScroll');
			$(_footerWrapper).addClass('footerWrapperNoScroll');
		},
		checkClasses : function() {
			if($(_formFooterButtons).hasClass('positionRelative')){
				$(_formFooterButtons).removeClass('positionRelative');
				$(_footerWrapper).removeClass('positionRelative');
				$(_formFooterButtons).removeClass('footerButtonsNoScroll');
				$(_footerWrapper).removeClass('footerWrapperNoScroll');
			}
		},
		supportOverflowScrolling : function() {
			if (this.hasCSSProperty('overflow-scrolling') ||
				this.hasCSSProperty('-webkit-overflow-scrolling') ||
				this.hasCSSProperty('-moz-overflow-scrolling') ||
				this.hasCSSProperty('-o-overflow-scrolling')) {
				return true;
			} else {
				return false
			}
		},
		hasCSSProperty: function(prop){
			if (window.getComputedStyle) {
				return window.getComputedStyle(document.body, null)[prop];
			} else {
				return document.body.currentStyle[prop];
			}
		}
	};
	mobile.prototype.utils = {
		init : function() {

		},
		touchStart : function(e) {
			this.threshold = {x: 30, y: 10};
			this.originalCoord = { x: 0, y: 0 };
			this.finalCoord = { x: 0, y: 0 };
			
			var touch = e.originalEvent.touches[0];
            this.originalCoord.y = touch.pageY;
            this.originalCoord.x = touch.pageX;
		},
		touchMove : function(e) {
			var touch = e.originalEvent.touches[0];
			
			this.finalCoord.x = touch.pageX;
            this.finalCoord.y = touch.pageY;
			var changeY = this.originalCoord.y - this.finalCoord.y;
			
			if(changeY<0)  Math.abs(changeY);
			if(changeY > this.threshold.y) {
				this.mobileMoved = true;
            }
		},
		touchEnd : function(e) {
			_this=this;
			if(_this.mobileMoved == true){
				setTimeout(function () {
					_this.mobileMoved = false;
				}, 300);
			}
		},
		adjust : function() {
		
		},
		preventDefault : function(event) {
			if(_isMobile){
				event.preventDefault();
			}
		},
		changeOrientation : function(event) {
			if(_isMobile){

				switch ( window.orientation ) {
					case 0:
						if(!$('#mobileOrientationError').hasClass('hidden')){
							$('#mobileOrientationError').addClass('hidden');
							fnb.controls.controller.setBodyHeight();
						}
					break;
					case 90:
						if($(window).height()<420||$(window).width()<420){
							if($('#mobileOrientationError').hasClass('hidden')){
								$('#mobileOrientationError').removeClass('hidden');
								this.rotate('mobileOrientationError', -90);
								fnb.controls.controller.clearBodyHeight();
							}
						}
					break;
					case -90:
						if($(window).height()<420||$(window).width()<420){
							if($('#mobileOrientationError').hasClass('hidden')){
								$('#mobileOrientationError').removeClass('hidden');
								this.rotate('mobileOrientationError', 90);
								fnb.controls.controller.clearBodyHeight();
							}
						}
					break;
				}
			}
		},
		rotate : function(el, degs) {
			var iedegs = degs/90;
			if (iedegs < 0) iedegs += 4
				transform = 'rotate('+degs+'deg)';
				iefilter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation='+iedegs+')';
			styles = {
				transform: transform,
				'-webkit-transform': transform,
				'-moz-transform': transform,
				'-o-transform': transform,
				filter: iefilter,
				'-ms-filter': iefilter
			};
			$(el).css(styles);
		}
	}; 
	namespace("fnb.utils.mobile",mobile);
});
///-------------------------------------------///
/// developer: Donovan
///
/// DEVICE/BROWSER DETECTION
///-------------------------------------------///
$(function(){
	function currentDevice(){
		this.browser = {};
	};
	currentDevice.prototype = {
		getDevice: function(){
			var ua = navigator.userAgent;
			var isMobile = false;
			var platform=navigator.platform;
			//PH - required for BTS Event
			var width = 0;
			var height = 0;
			
			if(window.self==top){
				width = $(window).width();
				height = $(window).height();
			}
			
			var device = {
				firefox: ua.match(/Firefox/),
				chrome: ua.match(/Chrome/),
				windowsPhone: ua.match(/Windows Phone/),
				ie: ua.match(/MSIE/),
				iphone: ua.match(/iPhone/),
				ipod: ua.match(/iPod/),
				ipad: ua.match(/iPad/),
				blackberry: ua.match(/BlackBerry/),
				android: ua.match(/Android/),
				opera: ua.match(/Opera/),
				webOs: ua.match(/webOS/)
			};
			
			(function(a){jQuery.browser.mobile=/android.+mobile|ipad|avantgo|bada\/|blackberry|wp7|zunewp7|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
			if(jQuery.browser.mobile)
			{
				isMobile = true;
			}
			else
			{
			  	isMobile = false;
			}
			
			if(device.firefox){
				this.browser = {mobile: isMobile, browser: 'Firefox', version: ua.substring(ua.indexOf('Firefox/') + 8), platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.windowsPhone){
				this.browser = {mobile: isMobile, browser: 'Windows Phone', version: 0, platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.chrome){
				this.browser = {mobile: isMobile, browser: 'Chrome', version: ua.substring(ua.indexOf('Chrome/') + 7), platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.ie){
				var ieVersion = 0;
				this.compatibilityMode = false;
				
				var ieRegex = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
				if (ieRegex.exec(ua) == null)
					this.exception = "The user agent detected does not contai Internet Explorer.";
		 
				this.renderVersion = parseFloat(RegExp.$1);
				ieVersion = this.renderVersion;
		 
				if (ua.indexOf("Trident/6.0") > -1) {
					if (ua.indexOf("MSIE 7.0") > -1) {
						this.compatibilityMode = true;
						ieVersion = 10;                  // IE 10
					}
				}
				else if (ua.indexOf("Trident/5.0") > -1) {      
					if (ua.indexOf("MSIE 7.0") > -1) {
						this.compatibilityMode = true;
						ieVersion = 9;                   // IE 9
					}
				}
				else if (ua.indexOf("Trident/4.0") > -1) {
					if (ua.indexOf("MSIE 7.0") > -1) {
						this.compatibilityMode = true;
						ieVersion = 8;                   // IE 8
					}
				}
					
				this.browser = {mobile: isMobile, browser: 'MSIE', version: ua.substring(ua.indexOf('MSIE') + 5, ua.indexOf ('.', ua.indexOf('MSIE'))), platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.iphone){
				this.browser = {mobile: isMobile, browser: 'iPhone', version: 0, platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.ipod){
				this.browser = {mobile: isMobile, browser: 'iPod', version: 0, platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.ipad){
				this.browser = {mobile: isMobile, browser: 'iPad', version: 0, platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.blackberry){
				this.browser = {mobile: isMobile, browser: 'BlackBerry', version: ua.indexOf("Version/") + 8, platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.android){
				this.browser = {mobile: isMobile, browser: 'Android', version: ua.substring(ua.indexOf('Android/') + 8), platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.opera){
				this.browser = {mobile: isMobile, browser: 'Opera', version: ua.substring(ua.indexOf('Chrome/') + 8), platform: platform, width: width, height: height}
				return this.browser;
			}else if(device.webOs){
				this.browser = {mobile: isMobile, browser: 'webOS', version: 0, platform: platform, width: width, height: height}
				return this.browser;
			}else{
				this.browser = {mobile: isMobile, browser: 'Unknown Browser', version: 0, platform: platform, width: width, height: height}
				return this.browser;
			}
		},
		getIEVersion: function(ua){
			var version="NA";
			var na=ua;
			var ieDocMode="NA";
			var ie8BrowserMode="NA";
			if(/msie/i.test(na) && (!_w.opera)){
				if(_w.attachEvent && _w.ActiveXObject){		
					version = (na.match( /.+ie\s([\d.]+)/i ) || [])[1];
					if(parseInt(version)==7){				
						if(_d.documentMode){
							version = 8;
							if(/trident\/\d/i.test(na)){
								ie8BrowserMode = "Compat Mode";
							}else{
								ie8BrowserMode = "IE 7 Mode";
							}
						}
					}else if(parseInt(version)==8){
						if(_d.documentMode){ ie8BrowserMode = "IE 8 Mode";}
					}	
					ieDocMode = (_d.documentMode) ? _d.documentMode : (_d.compatMode && _d.compatMode=="CSS1Compat") ? 7 : 5;	   			
				}
			}
			return {
				"UserAgent" : na,
				"Version" : version,
				"BrowserMode" : ie8BrowserMode,
				"DocMode": ieDocMode
			}
		}
	};
	namespace("fnb.utils.currentDevice",currentDevice);
});
///---------------------------------///
/// developer: Donovan
///
/// RETURN PARAMS
///---------------------------------///
$(function(){
	function params(){

	};
	params.prototype = {
		getParams: function(formName, targetDiv, buttonTarget, extraOptions){
			var targeteUrl; 
			var alternateUrl;
			var noForm;
			if(typeof(extraOptions)  != "undefined") { 
				alternateUrl = extraOptions["alternateUrl"];
				noForm = extraOptions["noForm"];
			}
			var target = $(targetDiv);
			
			var formTarget = $("form[name='"+formName+"']");
			
			
			var parms = {};
			
			targeteUrl = formTarget.attr("action");
			
			if(typeof(alternateUrl) != "undefined" && alternateUrl){
				targeteUrl = alternateUrl;
				console.log("alternateUrl : " + alternateUrl);
			}

			parms["targetDiv"] = $(targetDiv).attr("id");
			
			// where fields with the same name exist on a page, we need to submit 
			// the values as both comma seperated values, and list type values
			//  the listTypeParameters object caters for this
			var listTypeParameters = {};
			
			if(typeof(noForm) != "undefined" && noForm){
				formTarget = target;	
			};			
			
			formTarget.find("input,textarea").each(function(element){
				if(!$(this).hasClass('nonSubmittable')){
					var inputElement = $(this);
					if(inputElement.attr("name") == undefined || inputElement.attr("name")==""){
						return;
					}
					if (inputElement.attr('type') == "checkbox") {
						if(inputElement.prop('defaultChecked')==true||typeof(inputElement.attr('data-value'))!="undefined") {
							if(parms[this.name]){
								var parmsArray = new Array();
								if(typeof(parms[this.name]) == "object"){
									parmsArray = parms[this.name];
								}else if(typeof(parms[this.name]) == "string"){
									parmsArray.push(parms[this.name]);
								}
								if(typeof(inputElement.attr('data-value'))!="undefined"&&inputElement.prop('defaultChecked')==false){
									parmsArray.push(inputElement.attr('data-value'));
								}else{
									parmsArray.push(inputElement.val());
								}
								parms[this.name] = parmsArray;
							} else {
								if(typeof(inputElement.attr('data-value'))!="undefined"&&inputElement.prop('defaultChecked')==false){
									parms[this.name] = inputElement.attr('data-value');
								}else{
									parms[this.name] = inputElement.val();
								}
							}
						}
					}else{
						var currentValue = inputElement.val();

						if(inputElement.attr('name')=='branchSearchName'){
							if(currentValue=='Waiting branch code') currentValue = "";
						}
						if(inputElement.hasClass('phoneNumber')){
							if(currentValue=='Code'||currentValue=='Number') currentValue = "";
						}
						if(typeof inputElement.attr('class')!= 'undefined'){
							if(inputElement.hasClass('currencyInput')){
								currentValue = currentValue.replace(/ /g,'');
							}
						}

						if(parms[this.name]){
							if(this.name.substring(0, 10)!="methodCode") {
								parms[this.name] = parms[this.name] + "," + currentValue;
								listTypeParameters[this.name]=true;
							}
						}else {
							parms[this.name] = currentValue;
						}
					}
				}
			});
			
			// 
			for(parmName in listTypeParameters){
				var parmValues = parms[parmName].split(",");
				try{
					for(parmIndex=0; parmIndex<parmValues.length ; parmIndex++){
						parms[parmName+parmIndex] = parmValues[parmIndex];	
					}
				}catch(e){
				
				}
			}
			
			var targetParent = $(buttonTarget).parent().parent();

			targetParent.children().each(function(element){
				if(!$(this).hasClass('nonSubmittable')){
					if($(this).attr('class') !== undefined){
						var inputElement = $(this);
						if(!$(inputElement).hasClass('footerBtn')||$(inputElement).get(0)==$(buttonTarget).parent().get(0)){
							$(inputElement).find('input').each(function(){
								if($(this).attr('id')=="action"){
									targeteUrl = $(this).val();
								}else{
									parms[this.name] = $(this).val();
								}					
							});
						}
					}
				}
			});
			//MOBI SUBMISSIONS
			if(fnb.utils.mobile.popUpInitialized){
				$('#mobiPopupWrapper').find('input').each(function(){
					if(!$(this).hasClass('nonSubmittable')){
						if($(this).attr('id')=="action"){
							targeteUrl = $(this).val();
						}else{
							parms[this.name] = $(this).val();
						}		
					}
				});
			}
			fnb.controls.controller.setUrl(targeteUrl);
			fnb.controls.controller.setParams(parms);
		}
	};
	namespace("fnb.utils.params",params);
});
///---------------------------------///
/// developer: Donovan
///
/// GRAPHS
///---------------------------------///
/// -------- USAGE
/// namespace("fnb.utils.graph", new fnb.utils.graph());
/// fnb.utils.graph.type = "stackedGroup";
/// fnb.utils.graph.target = "#Target";
/// fnb.utils.graph.init(DATA);
/// -------- TYPES : stackedGroup, pyramids, verticalBar
/// -------- DATA EXAMPLES
/// stackedGroup: [{minVal:0,maxVal:2000,val:1500,onclick:"alert('Clicked')",legend:'test1'},{minVal:0,maxVal:2000,val:800,onclick:"alert('Clicked2')",legend:'test2'}];
/// pyramids: [{val:1500,valPrefix:'eB ',onclick:"alert('Clicked Pyramid 1')",legend:'Feb'},{val:800,valPrefix:'eB ',onclick:"alert('Clicked Pyramid 2')",legend:'Mar'},{val:1000,valPrefix:'eB ',onclick:"alert('Clicked Pyramid 3')",legend:'Apr'},{val:100,valPrefix:'eB ',onclick:"alert('Clicked Pyramid 3')",legend:'May'},{val:600,valPrefix:'eB ',onclick:"alert('Clicked Pyramid 3')",legend:'Jun'},{val:1900,valPrefix:'eB ',onclick:"alert('Clicked Pyramid 3')",legend:'Jul'}];
/// verticalBar: [{val:15811,valPrefix:' pts*',label:' Your score',legend:'back in eBucks',xAxisData:[{heading:'',val:0,valPrefix:' pts'},{heading:'0.4%',val:5000,valPrefix:' pts'},{heading:'0.6%',val:10000,valPrefix:' pts'},{heading:'0.8%',val:14000,valPrefix:' pts'},{heading:'1.2%',val:16000,valPrefix:' pts'},{heading:'2.5%',val:18500,valPrefix:' pts'}]}];
$(function(){
	function graph(){
		this.type;
		this.target;
		this.overrideWidth;
		this.colors = new Array('#699ba6', '#a7cf5f', '#b56d81', '#d8a160', '#998b98','#306787','#84d001','#772c41','#ae906c');
		
	}
	graph.prototype = {
		init: function(values,target,overrideWidth){	
			
			var parentObject = this;
			parentObject.target = target;
			if(overrideWidth != undefined) {
				parentObject.overrideWidth = overrideWidth
			};
			
			switch(this.type)
			{
				case 'stackedGroup':
					this.drawStackedGroup(values);
				break;
				case 'pyramids':
					this.drawPyramids(values);
				break;
				case 'verticalBar':
					this.drawVerticalBar(values);
				break;
				case 'bar':
					this.drawBar(values);
				break;
				default:
				break;
			}
			
			
		},
		drawStackedGroup: function(values){
			var counter = 1;
			var _this = this;
			if($(this.target).find('.stackedGroup') != undefined) $(this.target).find('.stackedGroup').remove();
			var wrapper = '<div class="stackedGroup clearfix">';
			$.each(values, function(objIndex, obj) {
				var maxVal = obj.maxVal;
				var minVal = obj.minVal;
				var val = obj.val;
				var valPrefix = obj.valPrefix;
				var onclick = obj.onclick;
				var breakdownTextYouEarned = '<div class="breakdownTextYouEarned">You collected</div>';
				var breakdownTextMaxValue = '<div class="breakdownTextMaxValue">Max Points:<br />' + maxVal + ' per month</div>';
				var legend = '<div class="stackedGroupCellLegend">' + obj.legend + '</div>';
				var label = '<div class="stackedGroupCellLabel">' + breakdownTextYouEarned + val + valPrefix + '</div>' + breakdownTextMaxValue;
				var segSize = (minVal+maxVal)/25;
				var valSize = Math.ceil(val/segSize);
				var graph = '<div class="stackedGroupGraphWrapper graphNo' + counter + ' stackedGraphs" data-value="' + counter + '" onclick="'+onclick+'">';
				counter ++;
				graph += '<div class="stackedGroupGraph">';
				var cells = '';
				for (var z = 0; z < (25-valSize); z++) {
					cells += '<div class="stackedGroupEmptyCell"></div>';
				}
				for (var i = 0; i < valSize; i++) {
					cells += '<div class="stackedGroupCell"></div>';
				}
				graph+=cells;
				graph+='</div>';
				graph+=legend;
				graph+=label + '</div>';
				wrapper+=graph;
			});
			wrapper+='</div>';
			this.appendGraph(wrapper);
		},
		drawPyramids: function(values){
			var _this = this;			
			if($(_this.target).find('.pyramidGroup') != undefined) $(_this.target).find('.pyramidGroup').remove();
			var wrapper = '<div class="pyramidGroup">';
			var targetHeight = $(_this.target).height();
			var targetWidth = $(_this.target).width();
			if(_this.overrideWidth != undefined){targetWidth = _this.overrideWidth};
			var shuffledColors = this.shuffleArray(_this.colors);
			var columns = '';
			var actualWidth = Math.ceil(targetWidth/values.length);
			var percWidth = Math.ceil((actualWidth)/2);
			var percWidthLeft = (percWidth)/2
			percWidth +=percWidthLeft;
			var highestValue = this.getHighestValue(values);
			var topGap = 0.10*highestValue;
			var increments = targetHeight/(highestValue+topGap);
			var left = $(_this.target).position().left;
			var labelLeft = $(_this.target).position().left;
			var selectedColor;
			var i = 0;
					
			$.each(values, function(objIndex, obj) {
				var val = Math.round(obj.val*increments);
				if(isNaN(val)) val = 0;
				var valPrefix = obj.valPrefix;
				var columnHeight=targetHeight-val;
				var onclick = obj.onclick;
				var legend ='<div class="pyramidColumnLabel" style="left: '+labelLeft+'px;width:'+(percWidth)*2+'px;margin-top: '+(targetHeight+10)+'px;">'+obj.legend+'</div>';
				var border ='<div class="pyramidColumnBorder"  style="left: '+labelLeft+'px;width:'+(percWidth)*2+'px;margin-top: '+targetHeight+'px;"></div>';
				var dot ='<div class="pyramidDot"  style="left: '+labelLeft+'px;width:'+(percWidth)*2+'px;margin-top: '+(columnHeight-50)+'px;"></div>';
				var displayValue='<div class="pyramidValue"  style="left: '+labelLeft+'px;width:'+(percWidth)*2+'px;margin-top: '+(columnHeight-20)+'px;">'+valPrefix+obj.val+'</div>';
				if(i>(shuffledColors.length)-1) i=0; shuffledColors = _this.shuffleArray(_this.colors); 
				var randomColor = shuffledColors[i];
				if(selectedColor != randomColor){selectedColor = randomColor}else{randomColor = _this.colors[Math.floor(Math.random() * _this.colors.length)]};
				var column = '<div class="pyramidColumn" style="left: '+left+'px;height: '+columnHeight+'px;border-left: '+percWidth+'px solid transparent;border-right: '+percWidth+'px solid transparent;border-bottom: '+val+'px solid '+randomColor+';" onclick="'+onclick+'"></div>';
				columns += column;
				columns += legend;
				columns += border;
				columns += dot;
				columns += displayValue;
				left+=percWidth;
				labelLeft+=percWidth;
				i++;
			});
			wrapper+=columns;
			wrapper+='</div>';
			this.appendGraph(wrapper);
		},
		drawVerticalBar: function(values){
			var _this = this;
			if($(this.target).find('.verticalBarGroup') != undefined)$(this.target).find('.verticalBarGroup').remove();
			var wrapper = '<div class="verticalBarGroup">';
			var targetHeight = $(this.target).height();
			var targetWidth = $(this.target).width();
			var barOffset = 18;
			var nextIsFull = false;
			$.each(values, function(objIndex, obj) {
				var columnHeight =  (targetHeight/values[objIndex].xAxisData.length)-(barOffset/values[objIndex].xAxisData.length);
				var val = obj.val;
				var valPrefix = obj.valPrefix;
				var level = obj.level;
				if(typeof level =='undefined') level = 2; 
				var label = obj.label;
				var legend = obj.legend;
				var prevVal = 0;
				var bumpVal = obj.bumpVal ? parseFloat(obj.bumpVal) : 0;
				var bumpLabel = obj.bumpValLabel;
				var bumpValPrefix = obj.bumpValPrefix;
				var lowestVal = _this.getLowestValue(values[objIndex].xAxisData);
				var highestVal = _this.getHighestValue(values[objIndex].xAxisData);
				var newHighestVal = (highestVal/5)+highestVal;
				var barHeight;
				var totalVal = parseFloat(val)+parseFloat(bumpVal);
				$.each(values[objIndex].xAxisData.reverse(), function(itemIndex, Item) {
					var positionClass = 'verticalBarColumnNegAxisIndicator';
					var labelPositionClass = '';
					var columnClass = 'verticalBarColumn';
					if(level<2) columnClass="";
					var xAxisInicator = '';
					var bumpDevided = '';
					var barLength = values[objIndex].xAxisData.length-itemIndex;
					if(Item.val<totalVal&&level!=0){var positionClass = 'verticalBarColumnPosAxisIndicator';};
						if(nextIsFull){
							nextIsFull = false;
							positionClass = 'verticalBarColumnPosFullAxisIndicator';
						}
						if(totalVal<newHighestVal){
							if(Item.val<totalVal&&totalVal<prevVal&&level!=0){
								positionClass = 'verticalBarColumnPosFullAxisIndicator';
								barHeight = (columnHeight*(barLength))-columnHeight;
								barDevided = (columnHeight/(prevVal-Item.val))*(totalVal-Item.val);
								barHeight=barHeight+barDevided;
								labelPositionClass = ' fullLabel';
							}else if(Item.val==totalVal&&level!=0&&itemIndex!=0){
								barHeight = (columnHeight*(barLength))-columnHeight;
								barDevided = (columnHeight/(prevVal-Item.val))*(totalVal-Item.val);
								barHeight=barHeight+barDevided; 
								nextIsFull = true;
							}else{
								if(totalVal==Item.val){
									barHeight =columnHeight*(barLength-1);
								}else if(totalVal>highestVal){
								
									barHeight = targetHeight-(0.13*targetHeight);
								}
							}
						}else{
							barHeight = targetHeight-(0.1*targetHeight);
						}
						
						if(level==1&&itemIndex<(values[objIndex].xAxisData.length-1)){
							positionClass = 'verticalBarColumnNegAxisIndicator';
						}else if(level==1&&itemIndex==(values[objIndex].xAxisData.length-1)){
							positionClass = 'verticalBarColumnPosFullAxisIndicator';
						}
						if(level<2) labelPositionClass=" displayNone";
						if(itemIndex==0&&totalVal>Item.val) positionClass = 'verticalBarColumnPosFullAxisIndicator';
						if(itemIndex==0&&totalVal==Item.val) nextIsFull=true;
					
						if(Item.heading!=''){
							xAxisInicator = '<div class="verticalBarXaxisIndicator '+positionClass+'" style="left: -'+(columnHeight/2)+'px;height: '+columnHeight+'px;width: '+columnHeight+'px"><div class="verticalBarXaxisIndicatorLabel">'+Item.heading+'</div></div>';
						}else{
							columnClass +=' noXAxisIndicator';
						}

						if(Item.val<totalVal&&totalVal<prevVal&&level>1||prevVal==totalVal&&level>1||itemIndex==0&&totalVal>Item.val&&level>1){
							xAxisInicator += '<div class="verticalBarXaxisLabel">'+legend+'</div>';
						}
						if(Item.val==0){
							bumpHtml = '';
							if(bumpVal>0){
								bumpDevided = bumpVal*columnHeight;
								bumpHtml = '<div class="bumpVerticalBar bumpVerticalBarVisible" style="height: '+bumpDevided+'px"><div class="bumpVerticalBarIndicator"></div><div class="verticalBarLabel">'+label+' '+val+' '+valPrefix+'</div><div class="verticalBarBumpLabel">'+bumpLabel+'</div></div>';
							}else{
								bumpHtml = '<div class="bumpVerticalBar bumpVerticalBarNotVisible"><div class="verticalBarLabel">'+label+' '+val+' '+valPrefix+'</div></div>';
							}
							if(level>1) xAxisInicator+= '<div class="verticalWrapper" style="bottom: '+barOffset+'px"><div class="verticalBar" style="height: '+barHeight+'px">'+bumpHtml+'</div></div>';
						}	
						var column = '<div class="'+columnClass+'"  style="height: '+columnHeight+'px">'+xAxisInicator+'<div id="verticalBarColumnLabel'+itemIndex+'" class="verticalBarColumnLabel'+labelPositionClass+'">'+Item.val+Item.valPrefix+'</div></div>';
						wrapper+=column;
						prevVal = Item.val;
					
				})
			});
			wrapper+='</div>';
			this.appendGraph(wrapper);
		},
		drawBar: function(values){

			var _this = this;
			var targetHeight = $(this.target).height();
			var targetWidth = $(this.target).width();
			
			var allGroupsHighestValues = [];
			$.each(values, function(objIndex, obj) {
				allGroupsHighestValues.push(_this.getHighestValue(values[objIndex].xAxisData));
			});
			var highestValue = Math.max.apply( null, allGroupsHighestValues);

			var roundToVal = Math.pow( 1, Math.floor( Math.round( Math.log(highestValue) / Math.LN10 * 1e10 ) / 1e10 ) );
			var graphTopValue = _this.roundTo((highestValue+(0.10*highestValue)),roundToVal);
			var yIndexCount = 6;
			var subtractVal = graphTopValue/5;
			graphTopValue = Math.round(graphTopValue+subtractVal);
			
			var pixelHeights = (targetHeight-(0.20*targetHeight))/graphTopValue;

			$.each(values, function(objIndex, obj) {
				var displayXaxis = obj.displayXaxisVals;
				var displayYaxis = obj.displayYaxisVals;
				var yAxisWidth = obj.yAxisWidth;
				var xValPrefix =obj.xValPrefix;
				var xValSuffix =obj.xValSuffix;
				var legend = obj.legend;
				var percWidth = Math.round(100/values.length);
				var wrapper = '<div class="barGroup" style="width:'+percWidth+'%;height:100%">';
				var barsCount = values[objIndex].xAxisData.length;
				var yAxisPercWidth = 0;
				if(displayYaxis==true){
					yAxisPercWidth = yAxisWidth/targetWidth*100;
					var barYaxisWrapperHeight = 0.80*targetHeight
					var barYaxisWrapper = '<div class="barYAxisWrapper" style="height:'+barYaxisWrapperHeight+'px;width:'+yAxisPercWidth+'%">';
					if(graphTopValue<10) yIndexCount = graphTopValue; subtractVal=1;
					var currentXvalue = yIndexCount;
					var yAxisHeight = 100/(graphTopValue/subtractVal);
					for (var n = 0; n < yIndexCount; ++ n){						
						currentXvalue = currentXvalue-subtractVal;
						barYaxisWrapper+= '<div class="yAxisLabelContainer yAxisLabelContainer'+n+'" style="height:'+yAxisHeight+'%"><div class="yAxisLabel yAxisLabel'+n+'">'+xValPrefix+currentXvalue+xValSuffix+'</div></div>';
					}
					barYaxisWrapper+='</div>';
					wrapper+=barYaxisWrapper;
				}
				$.each(values[objIndex].xAxisData.reverse(), function(itemIndex, Item) {
					var yAxisOffset = 0;
					if(displayYaxis==true){
						yAxisOffset = yAxisWidth/barsCount;
					}
					var colPxWidth = targetWidth/barsCount;
					colPxWidth = colPxWidth-yAxisOffset;
					var colPercWidth = colPxWidth/targetWidth*100;
					var currentColLegend = Item.legend;
					var currentColVal = Item.val;
					var currentColOnClick =Item.onclick;
					var currentColHeight = pixelHeights*currentColVal;
					var currentColPosClass = "";
					if(itemIndex==0){
						currentColPosClass = ";border-left: 4px solid #dddddd;";
					}else if(itemIndex==(barsCount-1)){
						currentColPosClass = ";border-right: 4px solid #dddddd;";
					}
					var currentColMarginTop = Math.round((0.80*targetHeight)-currentColHeight)+2;
					var columnWrapper = '<div class="barColumnWrapper barColumnWrapper'+itemIndex+'" style="width:'+colPercWidth+'%;height:90%'+currentColPosClass+'">';
					var column = '<div class="barColumn barColumn'+itemIndex+'" style="height:'+(currentColHeight+2)+'px;margin-top:'+currentColMarginTop+'px"></div>';
					columnWrapper+=column;
					if(displayXaxis==true){
						if(currentColLegend!=''||typeof currentColLegend!='undefined'){
							var columnLegend = '<div class="barColumnLegend barColumnLegend'+itemIndex+'">'+currentColLegend+'</div>';
							columnWrapper+=columnLegend;
						}
					}
					columnWrapper+='</div>';
					wrapper+=columnWrapper;
				});
				if(legend!=''||typeof legend!='undefined'){
					var barLegend = '<div class="barLegend barColumnLegend'+objIndex+'" style="width:'+(100-yAxisPercWidth)+'%">'+legend+'</div>';
					wrapper+=barLegend;
				}
				wrapper+='</div>';
				_this.appendGraph(wrapper);
			});
		},
		getHighestValue: function(collection){
			var valuesArray = [];
			$.each(collection, function(objIndex, obj) {
				valuesArray.push(obj.val);
			});
			return Math.max.apply( null, valuesArray);
		},
		getLowestValue: function(collection){
			var valuesArray = [];
			$.each(collection, function(objIndex, obj) {
				valuesArray.push(obj.val);
			});
			return Math.min.apply( null, valuesArray);
		},
		shuffleArray: function(collection){
			for(var j, x, i = collection.length; i; j = parseInt(Math.random() * i), x = collection[--i], collection[i] = collection[j], collection[j] = x);
			return collection;
		},
		roundTo: function(number, to) {
			return ((number%to) > 0)?number-(number%to) + to:number;
		},
		appendGraph: function(html){
			$(this.target).append(html);
		},
		removeGraph: function(){
		  
			var parentObject = this;

			//$(this.target).find('.pyramidGroup').remove();
		},
		reset: function() {
		
		this.removeGraph();
					
		}
	};

	namespace("fnb.utils.graph",graph);
});
///---------------------------------///
/// developer: Donovan
///
/// SCROLLING TOUCH BANNER
///---------------------------------///
$(function(){
	function scrollingBanner(){
		this.bannerScroller = [];
		this.bannerParentItems = [];
		this.bannerScrollerScrolling = false;
		this.bannerScrollerStops;
		this.bannerScrollerStopWidth;
		this.bannerWidth;
		this.bannerParent;
		this.bannerScrollerParent;
		this.bannerScrollerWidth;
		this.bannerScrollingBannerContainer;
		this.bannerItems;
		this.bannerEnableScrolling = true;
		this.childCount;
		this.firstChild;
		this.bannerItemHeight;
		this.bannerItemWidth;
		this.bannerItemSideMargins;
		this.bannerItemsOnScreenCount;
		this.bannerScrollStopWidth;
		this.bannerItemsOnScreenGap;
		this.bannerItemsOnScreenFill;
		this.doSpacing = 'false';
	};
	scrollingBanner.prototype = {
		init: function(parent,visible,doSpacing){

			if(typeof this.bannerParent=='undefined') this.bannerParent = [];
			this.bannerParentItems.push(parent);
			this.bannerParent[parent] = $('#'+parent);
			this.bannerParent[parent].parentId = parent;
			this.bannerParent[parent].doSpacing = (doSpacing) ? true : false;
			this.bannerParent[parent].bannerEnableScrolling = false;
			this.bannerParent[parent].bannerScrollerParent = this.bannerParent[parent].find('#scrollableBanner');
			this.bannerParent[parent].bannerScrollingBannerContainer =  this.bannerParent[parent].find('#scrollingBannerContainer');
			this.bannerParent[parent].bannerScrollingBannerWrapper =  this.bannerParent[parent].find('#scrollWrapper');
			this.bannerParent[parent].bannerItems = this.bannerParent[parent].bannerScrollingBannerWrapper.find('.scrollingBannerItem');
			this.bannerParent[parent].childCount = this.bannerParent[parent].bannerItems.length;
			var _current = this;
			this.show(parent);
			setTimeout(function(){
				_current.doCalculations(parent);
			},200); 
		},
		doCalculations: function(parent){
			
			var maxWidth = $(_workspace).width();
			var itemsWidth = this.bannerParent[parent].find('.scrollingBannerItem:eq(0)').outerWidth()*this.bannerParent[parent].find('.scrollingBannerItem').length;
			
			if(itemsWidth>maxWidth){

				this.firstChild = this.bannerParent[parent].find('.scrollingBannerItem:eq(0)');
				var bannerItemWidth = Math.floor($(this.firstChild).outerWidth(true))-1;
				this.bannerParent[parent].bannerItemHeight = $(this.firstChild).outerHeight(true);
				
				var bannerWidth =  Math.round(this.bannerParent[parent].width());
				var bannerItemsOnScreenCount = Math.floor(bannerWidth/bannerItemWidth);
				
				this.bannerParent[parent].scrollerWidth = "";
				
				if(bannerItemsOnScreenCount<this.bannerParent[parent].childCount){
					this.bannerParent[parent].bannerEnableScrolling = true;
					if(this.bannerParent[parent].doSpacing==false){
						var newBannerWidth = Math.round(bannerWidth/bannerItemsOnScreenCount);
						this.bannerParent[parent].bannerItems.css({'width':newBannerWidth});
						this.bannerParent[parent].scrollerWidth =this.bannerParent[parent].childCount*newBannerWidth;
					}else{
						this.bannerParent[parent].bannerItemSideMargins = this.firstChild.outerWidth(true) - this.firstChild.outerWidth();
						var bannerItemsOnScreenGap = (bannerWidth - (bannerItemsOnScreenCount*bannerItemWidth))-bannerItemsOnScreenCount;
						this.bannerParent[parent].bannerItemsOnScreenFill = Math.round((bannerItemsOnScreenGap/bannerItemsOnScreenCount)/2);
						this.bannerParent[parent].scrollerWidth =this.bannerParent[parent].childCount*bannerWidth;
					}
					this.bannerParent[parent].bannerScrollerStopWidth =bannerWidth;
					var scrollerStops = Math.round(this.bannerParent[parent].scrollerWidth/(bannerWidth/bannerItemsOnScreenCount))-1;
					this.bannerParent[parent].bannerScrollerStops = scrollerStops;
					this.setupBannerItems(parent);
					this.setupBanner(parent);
					this.initHorizontalScroller(parent);
				}else{
					if(typeof this.bannerParent[parent].bannerScroller !='undefined') this.bannerParent[parent].bannerScroller.enabled = false;
				}

			}
		},
		initHorizontalScroller: function(parent){
			this.bannerParent[parent].bannerScroller = new horizontalScroller();
			this.bannerParent[parent].bannerScroller.scrollableParent = this.bannerParent[parent].bannerScrollerParent;
			this.bannerParent[parent].bannerScroller.scrollerChildren = this.bannerParent[parent].find('.scrollingBannerItem');
			this.bannerParent[parent].bannerScroller.scrollStopWidth = this.bannerParent[parent].bannerScrollerStopWidth;
			this.bannerParent[parent].bannerScroller.scrollSpeed = 500;
			this.bannerParent[parent].bannerScroller.maxStops = this.bannerParent[parent].bannerScrollerStops;
			this.bannerParent[parent].bannerScroller.moveTreshold = 0.20;
			this.bannerParent[parent].bannerScroller.bindEvents();
			this.bannerParent[parent].bannerScroller.enabled = this.bannerParent[parent].bannerEnableScrolling;
		},
		resetBannerItems: function(parent){
			_this=this;
			_this.bannerParent[parent].bannerScrollerParent.css({'width':''});
			_this.bannerParent[parent].bannerItems.css({'margin':'0','width':''});
			
		},
		setupBannerItems: function(parent){
			_this=this;
			var onScreenMargin = Math.floor((this.bannerParent[parent].bannerItemSideMargins/2)+this.bannerParent[parent].bannerItemsOnScreenFill);
			var onScreenMarginString = '0 '+onScreenMargin+'px 0 '+onScreenMargin+'px';
			$.each(_this.bannerParent[parent].bannerItems, function(bannerItemIndex, bannerItem) {
				$(bannerItem).css({'margin':onScreenMarginString});
			});
		},
		setupBanner: function(parent){
			this.bannerParent[parent].height(this.bannerParent[parent].bannerItemHeight);
			this.bannerParent[parent].bannerScrollerParent.width(this.bannerParent[parent].scrollerWidth);
		},
		show: function(parent){
			this.bannerParent[parent].scrollingBannerVisible = true;
			this.bannerParent[parent].removeClass('hidden');
		},
		hide: function(parent){
			this.bannerParent[parent].scrollingBannerVisible = false;
			this.bannerParent[parent].addClass('hidden');
		},
		scrollLeft: function(parent){
			this.bannerParent[parent].bannerScroller.previous();
		},
		scrollRight: function(parent){
			this.bannerParent[parent].bannerScroller.next();
		},
		adjust: function(windowWidth,specificParent){

			_this=this;
			if(typeof specificParent=='undefined'){
				if(this.bannerParentItems.length>0){
					$.each(_this.bannerParentItems, function(index, parentId) {
						_this.resetBannerItems(parentId);
						_this.doCalculations(parentId);
					});
				}
			}else{
				_this.resetBannerItems(specificParent);
				_this.doCalculations(specificParent);
			}
		}
	};
	namespace("fnb.utils.scrollingBanner",scrollingBanner);
});
///---------------------------------///
/// developer: Donovan
///
/// CREATE SITE OBJECTS
///---------------------------------///
$(function(){
	function objectsInitializer(){
		
	};
	objectsInitializer.prototype = {
		init: function(){
			namespace("fnb.utils.frame", new fnb.utils.frame());
			fnb.controls.controller.registerObj('frame',fnb.utils.frame);
			namespace("fnb.utils.topTabs", new fnb.utils.topTabs());
			fnb.controls.controller.registerObj('topTabs',fnb.utils.topTabs);
			fnb.controls.controller.eventsObject = new fnb.controls.events(this);
			fnb.controls.controller.registerObj('eventsObject',this.eventsObject);
			namespace("fnb.controls.page", new fnb.controls.page());
			fnb.controls.controller.registerObj('pageObject',fnb.controls.page);
			namespace("fnb.utils.load", new fnb.utils.load());
			fnb.controls.controller.registerObj('load',fnb.utils.load);
			namespace("fnb.utils.ajaxManager", new fnb.utils.ajaxManager());
			fnb.controls.controller.registerObj('fnb.utils.ajaxManager',fnb.utils.ajaxManager);
			namespace("fnb.utils.loadUrl", new fnb.utils.loadUrl());
			fnb.controls.controller.registerObj('loadUrl',fnb.utils.loadUrl);
			namespace("fnb.forms.input", new fnb.forms.input());
			fnb.controls.controller.registerObj('input',fnb.forms.input);
			namespace("fnb.utils.navError", new fnb.utils.navError());
			fnb.controls.controller.registerObj('error',fnb.utils.navError);
			namespace("fnb.utils.handleOTP", new fnb.utils.handleOTP());
			fnb.controls.controller.registerObj('otp',fnb.utils.handleOTP);
			namespace("fnb.utils.overlay", new fnb.utils.overlay());
			fnb.controls.controller.registerObj('overlay',fnb.utils.overlay);
			namespace("fnb.utils.errorPanel", new fnb.utils.errorPanel());
			fnb.controls.controller.registerObj('errorPanel',fnb.utils.errorPanel);
			namespace("fnb.utils.actionMenu", new fnb.utils.actionMenu());
			fnb.controls.controller.registerObj('fnb.utils.actionMenu',fnb.utils.actionMenu);
			namespace("fnb.utils.mobile",new fnb.utils.mobile());
			fnb.controls.controller.registerObj('fnb.utils.mobile',fnb.utils.mobile);
			namespace("fnb.utils.currentDevice",new fnb.utils.currentDevice());
			fnb.controls.controller.registerObj('fnb.utils.currentDevice',fnb.utils.currentDevice);
			namespace("fnb.forms.extendedPageHeading",new fnb.forms.extendedPageHeading());
			fnb.controls.controller.registerObj('fnb.utils.currentDevice',fnb.forms.extendedPageHeading);
			namespace("fnb.forms.dropdown",new fnb.forms.dropdown());
			fnb.controls.controller.registerObj('fnb.forms.dropdown',fnb.forms.dropdown);
			namespace("fnb.forms.radioButtons",new fnb.forms.radioButtons());
			fnb.controls.controller.registerObj('fnb.forms.radioButtons',fnb.forms.radioButtons);
			namespace("fnb.utils.eziSlider",new fnb.utils.eziSlider());
			fnb.controls.controller.registerObj('fnb.utils.eziSlider',fnb.utils.eziSlider);
			namespace("fnb.functions.branchSearch",new fnb.functions.branchSearch());
			fnb.controls.controller.registerObj('fnb.functions.branchSearch',fnb.functions.branchSearch);
			namespace("fnb.forms.checkBox",new fnb.forms.checkBox());
			fnb.controls.controller.registerObj('fnb.forms.checkBox',fnb.forms.checkBox);
			namespace("fnb.utils.params",new fnb.utils.params());
			fnb.controls.controller.registerObj('fnb.utils.params',fnb.utils.params);
			namespace("fnb.functions.reloadDropdown",new fnb.functions.reloadDropdown());
			fnb.controls.controller.registerObj('fnb.functions.reloadDropdown',fnb.functions.reloadDropdown);
			namespace("fnb.functions.loadDropDownDiv",new fnb.functions.loadDropDownDiv());
			fnb.controls.controller.registerObj('fnb.functions.loadDropDownDiv',fnb.functions.loadDropDownDiv);
			namespace("fnb.utils.popup",new fnb.utils.popup());
			fnb.controls.controller.registerObj('fnb.utils.popup',fnb.utils.popup);
			namespace("fnb.functions.notifications",new fnb.functions.notifications());
			fnb.controls.controller.registerObj('fnb.functions.notifications',fnb.functions.notifications);
			namespace("fnb.functions.showHideToggleElements",new fnb.functions.showHideToggleElements());
			fnb.controls.controller.registerObj('fnb.functions.showHideToggleElements',fnb.functions.showHideToggleElements);
			fnb.controls.controller.registerObj('fnb.functions.toggleClassName',fnb.functions.toggleClassName);
			namespace("fnb.functions.toggleClassName",new fnb.functions.toggleClassName());
			fnb.controls.controller.registerObj('fnb.functions.toggleClassName',fnb.functions.toggleClassName);
			namespace("fnb.functions.loadUrlToPrintDiv",new fnb.functions.loadUrlToPrintDiv());
			fnb.controls.controller.registerObj('fnb.functions.loadUrlToPrintDiv',fnb.functions.loadUrlToPrintDiv);
			namespace("fnb.functions.submitFormToWorkspace",new fnb.functions.submitFormToWorkspace());
			fnb.controls.controller.registerObj('fnb.functions.submitFormToWorkspace',fnb.functions.submitFormToWorkspace);
			namespace("fnb.utils.otp",new fnb.utils.otp());
			fnb.controls.controller.registerObj('fnb.utils.otp',fnb.utils.otp);
			namespace("fnb.utils.subtabs",new fnb.utils.subtabs());
			fnb.controls.controller.registerObj('fnb.utils.subtabs',fnb.utils.subtabs);
			namespace("fnb.functions.unhider",new fnb.functions.unhider());
			fnb.controls.controller.registerObj('fnb.functions.unhider',fnb.functions.unhider);
			namespace("fnb.forms.tooltip",new fnb.forms.tooltip());
			fnb.controls.controller.registerObj('fnb.forms.tooltip',fnb.forms.tooltip);
			namespace("fnb.functions.accountDropdown",new fnb.functions.accountDropdown());
			fnb.controls.controller.registerObj('fnb.functions.accountDropdown',fnb.functions.accountDropdown);
			namespace("fnb.functions.lotto",new fnb.functions.lotto());
			fnb.controls.controller.registerObj('fnb.functions.lotto',fnb.functions.lotto);
			namespace("fnb.forms.textAreaHandler",new fnb.forms.textAreaHandler());
			fnb.controls.controller.registerObj('fnb.forms.textAreaHandler',fnb.forms.textAreaHandler);	
			namespace("fnb.functions.navigateTo",new fnb.functions.navigateTo());
			fnb.controls.controller.registerObj('fnb.functions.navigateTo',fnb.functions.navigateTo);	
			namespace("fnb.functions.gotoUrl",new fnb.functions.gotoUrl());
			fnb.controls.controller.registerObj('fnb.functions.gotoUrl',fnb.functions.gotoUrl);
			namespace("fnb.functions.siteLoadComplete", new fnb.functions.siteLoadComplete());
			namespace("fnb.functions.checkboxHierarchySwitcher",new fnb.functions.checkboxHierarchySwitcher());
			fnb.controls.controller.registerObj('fnb.functions.checkboxHierarchySwitcher',fnb.functions.checkboxHierarchySwitcher);
			namespace("fnb.functions.rowHandler",new fnb.functions.rowHandler());
			fnb.controls.controller.registerObj('fnb.functions.rowHandler',fnb.functions.rowHandler);
			namespace("fnb.forms.tableUtils", new fnb.forms.tableUtils());
			fnb.controls.controller.registerObj('fnb.forms.tableUtils',fnb.forms.tableUtils);
			namespace("fnb.utils.paypal.windows",new fnb.utils.paypal.windows());
			fnb.controls.controller.registerObj('fnb.utils.paypal.windows',fnb.utils.params);
			namespace("fnb.forms.scrollUtil",new fnb.forms.scrollUtil());
			fnb.controls.controller.registerObj('fnb.forms.scrollUtil', fnb.forms.scrollUtil);
			namespace("fnb.functions.submitFormToEziPanel",new fnb.functions.submitFormToEziPanel());
			fnb.controls.controller.registerObj('fnb.functions.submitFormToEziPanel', fnb.functions.submitFormToEziPanel);
			namespace("fnb.functions.submitFormToDiv",new fnb.functions.submitFormToDiv());
			fnb.controls.controller.registerObj('fnb.functions.submitFormToDiv', fnb.functions.submitFormToDiv);
			namespace("fnb.functions.timeOut",new fnb.functions.timeOut());
			fnb.controls.controller.registerObj('fnb.functions.timeOut', fnb.functions.timeOut);
			namespace("fnb.functions.logOff",new fnb.functions.logOff());
			fnb.controls.controller.registerObj('fnb.functions.logOff', fnb.functions.logOff);
			namespace("fnb.utils.graph",new fnb.utils.graph());
			fnb.controls.controller.registerObj('fnb.utils.graph', fnb.utils.graph);
			namespace("fnb.utils.scrollingBanner",new fnb.utils.scrollingBanner());
			fnb.controls.controller.registerObj('fnb.utils.scrollingBanner', fnb.utils.scrollingBanner);
			namespace("fnb.functions.bigThree",new fnb.functions.bigThree());
			fnb.controls.controller.registerObj('fnb.functions.bigThree',fnb.functions.bigThree);
			namespace("fnb.functions.logonHandler",new fnb.functions.logonHandler());
			fnb.controls.controller.registerObj('fnb.functions.logonHandler',fnb.functions.logonHandler);
			namespace("fnb.forms.fileUpload",new fnb.forms.fileUpload());
			fnb.controls.controller.registerObj('fnb.forms.fileUpload',fnb.forms.fileUpload);
			namespace("fnb.functions.tableMoreButton",new fnb.functions.tableMoreButton());
			fnb.controls.controller.registerObj('fnb.functions.tableMoreButton',fnb.functions.tableMoreButton);	
			namespace("fnb.functions.hyperlink",new fnb.functions.hyperlink());
			fnb.controls.controller.registerObj('fnb.functions.hyperlink',fnb.functions.hyperlink);				
			namespace("fnb.functions.isScrolling",new fnb.functions.isScrolling());
			fnb.controls.controller.registerObj('fnb.functions.isScrolling',fnb.functions.isScrolling);
			namespace("fnb.functions.loadUrlToExpandableRow",new fnb.functions.loadUrlToExpandableRow());
			fnb.controls.controller.registerObj('fnb.functions.loadUrlToExpandableRow',fnb.functions.loadUrlToExpandableRow);
			namespace("fnb.functions.cozaContent",new fnb.functions.cozaContent());
			fnb.controls.controller.registerObj('fnb.functions.cozaContent',fnb.functions.cozaContent);
			namespace("fnb.functions.ie8",new fnb.functions.ie8());
			fnb.controls.controller.registerObj('fnb.functions.ie8',fnb.functions.ie8);
			namespace("fnb.functions.slowConnection",new fnb.functions.slowConnection());
			fnb.controls.controller.registerObj('fnb.functions.slowConnection',fnb.functions.slowConnection);
			namespace("fnb.functions.footer",new fnb.functions.footer());
			fnb.controls.controller.registerObj('fnb.functions.footer',fnb.functions.footer);
		}
	};
	
	namespace("fnb.utils.objectsInitializer",objectsInitializer);

});