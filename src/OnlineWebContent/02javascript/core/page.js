///-------------------------------------------///
/// developer: Donovan
///
/// Page Controller
///-------------------------------------------///
$(function(){
	function pageObject() {
		this.activeObject;
		this.observerObject;
	}
	pageObject.prototype = {
		init : function (parent) {
			this.attachListeners(parent)   
		},
		attachListeners: function(observer) {
			_this = this;
			this.setObserverObject(observer);
			//TABBING
			/* GLOBAL IPHONE FIX */
			//---->> DROPDOWNS
			$(this.observerObject).on('focus', '.dropdown-keyboard-tab', function(event) {fnb.forms.dropdown.focusIn(event);});
			$(this.observerObject).on('blur', '.dropdown-keyboard-tab', function(event) {fnb.forms.dropdown.focusOut(event);});
			$(this.observerObject).on('keyup', '.dropdown-has-focus', function(event) {fnb.forms.dropdown.focusKeyup(event);});
			$(this.observerObject).on('keypress keydown', '.dropdown-has-focus', function(event) {if(event.keyCode == 40 ) fnb.utils.mobile.utils.preventDefault(event);});
			//FORMS
			$(this.observerObject).on('focus keypress keyup keydown blur', '.input-input', function(event) {fnb.controls.controller.clearActiveElements();fnb.controls.page.setActiveObject(event.target);return fnb.forms.input.delegateEvents(event);});
			$(this.observerObject).on('click touchend', '.radioButton', function(event) {fnb.controls.controller.clearActiveElements();fnb.forms.radioButtons.radioAction(event.target);fnb.controls.page.setActiveObject(event.target);});
			//---Checkbox
			if(!_isMobile){
				$(this.observerObject).on('click', '.checkbox-input', function(event) {fnb.utils.mobile.utils.preventDefault(event);fnb.controls.controller.clearActiveElements(event);fnb.forms.checkBox.checkboxClick(event.target);fnb.controls.page.setActiveObject(event.target);});
			}else{
				$(this.observerObject).on('touchend', '.checkbox-graphic-wrapper', function(event) {fnb.utils.mobile.utils.preventDefault(event);fnb.controls.controller.clearActiveElements(event);fnb.forms.checkBox.checkboxTouch(event.target);fnb.controls.page.setActiveObject(event.target);});
			}
			//---Checkbox END
			//---Dropdown

			if(!_isMobile){
				$(this.observerObject).on('click', '.dropdown-item', function(event) {fnb.forms.dropdown.checkSelect(event,true);fnb.controls.page.setActiveObject(event.target);});
				$(this.observerObject).on('click', '.dropdown-initiator', function(event) {event.stopPropagation()});
				$(this.observerObject).on('click', '.dropdown-input', function(event) {event.stopPropagation();});
			}else{
				$(this.observerObject).on('touchend', '.dropdown-initiator', function(event) {event.stopPropagation();fnb.utils.mobile.utils.preventDefault(event);fnb.forms.dropdown.checkExpand(event.target)});
				$(this.observerObject).on('touchend', '.dropdown-item', function(event) {fnb.utils.mobile.utils.preventDefault(event);fnb.forms.dropdown.checkSelect(event,true);fnb.controls.page.setActiveObject(event.target);});
				$(this.observerObject).on('touchend', '.dropdown-wrapper', function(event) {event.stopPropagation();});
				$(this.observerObject).on('touchend', '.dropdown-content-wrapper', function(event) {event.stopPropagation();});
				$(this.observerObject).on('touchend', '.dropdown-item-row-wrapper', function(event) {fnb.utils.mobile.utils.preventDefault(event);fnb.utils.mobile.utils.touchEnd(event);});
				$(this.observerObject).on('touchend', '.dropdown-item-row', function(event) {fnb.utils.mobile.utils.preventDefault(event);fnb.utils.mobile.utils.touchEnd(event);});
				$(this.observerObject).on('touchend', '.dropdown-input', function(event) {event.stopPropagation();});
			}
			
			$(this.observerObject).on('keyup', '.dropdown-input', function(event) {fnb.forms.dropdown.keypress(event.target);fnb.controls.page.setActiveObject(event.target);});
			
			//---Dropdown END			
			//---Datepicker
			if(!_isMobile){
				$(this.observerObject).on('click', '.datepickerWrapper', function(event) {fnb.controls.controller.clearActiveElements();fnb.controls.controller.eventsObject.raiseEvent('openDatePicker', event.target);fnb.controls.page.setActiveObject(event.target);});
			}else{
				$(this.observerObject).on('touchend', '.datepickerWrapper', function(event) {fnb.utils.mobile.utils.preventDefault(event);fnb.controls.controller.clearActiveElements();fnb.controls.controller.eventsObject.raiseEvent('openDatePicker', event.target);fnb.controls.page.setActiveObject(event.target);});
			}
			//---Datepicker END	
			$(this.observerObject).on('click touchend', '#calendarCancelButton', function(event) {fnb.utils.mobile.utils.preventDefault(event);_datePicker.hide(false,false);fnb.controls.page.setActiveObject(event.target);});
			$(this.observerObject).on('click touchend', '#confirmHolidayButton', function(event) {fnb.utils.mobile.utils.preventDefault(event);_datePicker.hide(true,false);fnb.controls.page.setActiveObject(event.target);});
			$(this.observerObject).on('click touchend', '#cancelHolidayButton', function(event) {fnb.utils.mobile.utils.preventDefault(event);_datePicker.notifyHolidayHide();fnb.controls.page.setActiveObject(event.target);});
			$(this.observerObject).on('focus', '.radioGroupValue', function(event) {fnb.forms.radioButtons.focusIn(event);});
			$(this.observerObject).on('blur', '.radioGroupValue', function(event) {fnb.forms.radioButtons.focusOut(event);});
			$(this.observerObject).on('keyup', '.radio-has-focus', function(event) {fnb.forms.radioButtons.focusKeyup(event);});
			//File upload widget
		//	$(this.observerObject).on('click touchend', '.file-button-fake', function(event) {fnb.forms.fileUpload.buttonClick(event);});
		//	$(this.observerObject).on('click touchend', '.file-input-fake', function(event) {fnb.forms.fileUpload.inputClick(event);});
		//	$(this.observerObject).on('change', '.input-file', function(event) {fnb.forms.fileUpload.inputChanged(event);});
			//File upload widget END
			$(this.observerObject).on('keyup', '.radio-has-focus', function(event) {fnb.forms.radioButtons.focusKeyup(event);});
			
			//TOOLTIPS
			if(!_isMobile){
				$(this.observerObject).on('click', '.tooltipParent', function(event) {event.stopPropagation();fnb.forms.tooltip.show($(event.target).parent());});
			}else{
				//$(this.observerObject).on('touchend', '.tooltipParent', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.forms.tooltip.show($(event.target).parent());});
				$(this.observerObject).on('touchend', '.tooltipButton', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.forms.tooltip.show($(event.target).parent().parent());});
			}
			
			//OTHER
			$(this.observerObject).on('click touchend', '.headerButton', function(event) {event.stopPropagation();fnb.utils.topTabs.headerButtonSelect($(event.target).parent());});
			//TABLES
			if(!_isMobile){
				$(this.observerObject).on('click', '.rowMoreButton', function(event) {fnb.controls.controller.clearActiveElements();fnb.functions.tableMoreButton.checkSelection($(event.target).parent().attr('data-value'))});
			}else{
				$(this.observerObject).on('touchend', '.rowMoreButton', function(event) {fnb.controls.controller.clearActiveElements();fnb.functions.tableMoreButton.checkSelection($(event.target).parent().attr('data-value'))});
			}
			
			$(this.observerObject).on('click touchend', '.tableSwitcherButton', function(event) {fnb.forms.tableUtils.switcherClick(event)});
			//---Table Action Button
			//---Table Action Button END
			if(!_isMobile){
				$(this.observerObject).on('click', '.tableActionButton', function(event) {fnb.forms.tableUtils.actionButtonClick($(event.currentTarget))});
			}else{
				$(this.observerObject).on('touchend', '.tableActionButtonLabel', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.forms.tableUtils.actionButtonClick($(event.target).parent())});
				$(this.observerObject).on('touchend', '.tableActionButton', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.forms.tableUtils.actionButtonClick($(event.target))});
			}
			//---Select All
			if(!_isMobile){
				$(this.observerObject).on('click', '.checkall-link', function(event) {fnb.utils.mobile.utils.preventDefault(event);fnb.forms.checkBox.toggleAll($(event.target),event)});
			}else{
				$(this.observerObject).on('touchend', '.checkall-link', function(event) {fnb.utils.mobile.utils.preventDefault(event);fnb.forms.checkBox.toggleAll($(event.target),event)});
				$(this.observerObject).on('touchstart', '.checkall-link', function(event) {fnb.utils.mobile.utils.preventDefault(event);});
			}
			//---Select All END
			//SUBTABS
			$(this.observerObject).on('click touchend', '.subTabButton', function(event) {fnb.controls.controller.clearActiveElements(event);fnb.utils.subtabs.select($(event.target))});
			//OVERLAY
			$(this.observerObject).on('click touchend', '.overlay', function(event) {event.stopPropagation();});
			//MOBILE
			$(this.observerObject).on('click touchend', '.mobi-header-trigger', function(event) {fnb.utils.mobile.headerControls.menuSlide(event)});

			$(this.observerObject).on('touchend', '.errorButtonWrapper', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.utils.errorPanel.hide()});
			//ACTION MENU
			if(!_isMobile){
				$(this.observerObject).on('click', '.actionMenuButtonWrapper', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.controls.controller.clearActiveElements();fnb.controls.controller.eventsObject.raiseEvent('actionMenuShowHide', $(event.target))});
			}else{
				$(this.observerObject).on('touchend', '.actionMenuButtonWrapper', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.controls.controller.clearActiveElements();fnb.controls.controller.eventsObject.raiseEvent('actionMenuShowHide', $(event.target))});
				$(this.observerObject).on('touchend', '.actionMenuTextWrapper', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.controls.controller.clearActiveElements();fnb.controls.controller.eventsObject.raiseEvent('actionMenuShowHide', $(event.target).closest('.actionMenuButtonWrapper'))});
				$(this.observerObject).on('touchend', '.actionMenuIcon', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.controls.controller.clearActiveElements();fnb.controls.controller.eventsObject.raiseEvent('actionMenuShowHide', $(event.target).closest('.actionMenuButtonWrapper'))});
				$(this.observerObject).on('touchend', '.moreoptionsActionMenuTextSwap', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.controls.controller.clearActiveElements();fnb.controls.controller.eventsObject.raiseEvent('actionMenuShowHide', $(event.target).closest('.actionMenuButtonWrapper'))});
				$(this.observerObject).on('touchend', '.actionMenuText', function(event) {fnb.utils.mobile.utils.preventDefault(event);event.stopPropagation();fnb.controls.controller.clearActiveElements();fnb.controls.controller.eventsObject.raiseEvent('actionMenuShowHide', $(event.target).closest('.actionMenuButtonWrapper'))});

				$(this.observerObject).on('touchstart', '.actionMenuButtonWrapper', function(event) {fnb.utils.mobile.utils.preventDefault(event);});
				$(this.observerObject).on('touchstart', '.actionMenuTextWrapper', function(event) {fnb.utils.mobile.utils.preventDefault(event);});
				$(this.observerObject).on('touchstart', '.actionMenuIcon', function(event) {fnb.utils.mobile.utils.preventDefault(event);});
				$(this.observerObject).on('touchstart', '.moreoptionsActionMenuTextSwap', function(event) {fnb.utils.mobile.utils.preventDefault(event);});
				$(this.observerObject).on('touchstart', '.actionMenuText', function(event) {fnb.utils.mobile.utils.preventDefault(event);});
			}

			//ACTION MENU END
			$(this.observerObject).on('click touchend', '.footerBtn', function(event) {event.stopPropagation();});
			$(this.observerObject).on('click touchend', '.datePickerDate', function(event) {event.stopPropagation();});
			$(this.observerObject).on('click touchend', '.formFooterButtons', function(event) {event.stopPropagation();});
			
			if(_isMobile){
				$(this.observerObject).on('touchstart', '.tableRow', function(event) {fnb.utils.mobile.utils.touchStart(event);});
				$(this.observerObject).on('touchmove', '.tableRow', function(event) {fnb.utils.mobile.utils.touchMove(event);});
				$(this.observerObject).on('touchend', '.tableRow', function(event) {fnb.utils.mobile.utils.touchEnd(event);});
				
				$(this.observerObject).on('touchstart', '.dropdown-content', function(event) {fnb.utils.mobile.utils.touchStart(event);});
				$(this.observerObject).on('touchmove', '.dropdown-content', function(event) {fnb.utils.mobile.utils.touchMove(event)});
				$(this.observerObject).on('touchend', '.dropdown-content', function(event) {fnb.utils.mobile.utils.touchEnd(event)});
	
				$(this.observerObject).on('touchstart', '.dropdown-initiator', function(event) {fnb.utils.mobile.utils.touchStart(event);});
				$(this.observerObject).on('touchmove', '.dropdown-initiator', function(event) {fnb.utils.mobile.utils.touchMove(event)});
				$(this.observerObject).on('touchend', '.dropdown-initiator', function(event) {fnb.utils.mobile.utils.touchEnd(event)});
			}
			
			$(this.observerObject).on('touchstart', '.mobiDropdownClose', function(event) {event.stopPropagation();fnb.utils.mobile.utils.preventDefault(event);});
			$(this.observerObject).on('click touchend', '.mobiDropdownClose', function(event) {event.stopPropagation();fnb.utils.mobile.utils.preventDefault(event);fnb.forms.dropdown.close()});
			$(this.observerObject).on('click touchend', '.footerWrapper', function(event) {event.stopPropagation();fnb.utils.mobile.utils.preventDefault(event);});
			
			/* TIMEOUT */
			if(!_isMobile){
				$('html').on('click', 'body', function(event) {fnb.controls.controller.clearActiveElements();fnb.hyperion.utils.timeOut.reset()});
			}else{
				$('html').on('touchend', 'body', function(event) {fnb.controls.controller.clearActiveElements();fnb.hyperion.utils.timeOut.reset();});
			}
			
			//Mobile Error
			$(this.observerObject).on('click touchend', '.mobileOrientationError', function(event) {event.stopPropagation();});
			/* MORE DATA LISTENER */
			$(window).scroll(function () {fnb.functions.isScrolling.checkPos();});
			$(this.observerObject).on('click touchend', '#forMore', function(event) {event.stopPropagation();fnb.functions.isScrolling.doScroll()});
		},
		setObserverObject: function(target) {
			this.observerObject = target;
		},
		getObserverObject: function() {
			return this.observerObject;
		},
		setActiveObject: function(target) {
			this.activeObject = target;
		},
		getActiveObject: function() {
			return this.activeObject;
		},
		detachListeners: function() {
			$(this.observerObject).off('focus keypress keyup keydown blur', 'input');
		},
		destroy : function () {
			
		}
	};
	namespace("fnb.controls.page",pageObject);
});
