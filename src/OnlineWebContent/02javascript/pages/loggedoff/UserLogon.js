{
	
	function genericPageObject() {
	}
	genericPageObject.prototype = {

		validateError : function(navErrorMessage) {
			if (navErrorMessage != 'null') {
				alert(navErrorMessage);
				$('#HomePageForm').submit();
				return false;
			}else{
				$('#LoggedInForm').submit();
				return true;
			}
		}
	}

	namespace("fnb.user.login.UserLogon",genericPageObject);	
}
