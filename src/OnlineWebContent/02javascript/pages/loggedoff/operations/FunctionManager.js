$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
						
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
		},
		
		pageLoaded : function() {
			var parent = this;
			var functionAction = $('#factAction .radioGroupValue').val();
						
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showUrlDropdown'},{show:'false',element:'#updateFunction'},
			                                                     {show:'false',element:'#currentParentDiv'},{show:'false',element:'#factRecordDiv'}] );
			
			if(functionAction == 'UPDATE') {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showUpdateURLCheckBox'}, {show:'false',element:'#showAddURLCheckBox'}] );
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showUpdateURLCheckBox'}, {show:'true',element:'#showAddURLCheckBox'}] );				
				$("#url").removeAttr('disabled');
				$("#url").removeAttr('readonly');
			}
			
			this.clearCookies();
			
		},
		
		toggleFunctionDiv : function(value) {
			
			if(value == 'add') {
				 fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#updateFunction'}] );
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#updateFunction'}] );
			}
			
			fnb.forms.dropdown.select($('#childReference_parent li:eq(0)'));
			$('#functionDescription').val("");
			
			
			
		},
		updateURL : function(obj){
			
			var isChecked = $(obj).attr('checked')=='checked';
			
			if(!isChecked) {
				$("#url").attr('disabled','true');
			}else{
				$("#url").removeAttr('disabled');
				$("#url").removeAttr('readonly');
			}
			
		},
		
		addNewURL : function(obj){
			
			var isChecked = $(obj).attr('checked')=='checked';
			
			if(isChecked) {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showUrlDropdown'}] );
				$("#url").attr('disabled','true');
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showUrlDropdown'}] );
				$('#url').val("/banking/Controller?nav=");
				$("#url").removeAttr('disabled');
				$("#url").removeAttr('readonly');
			}
			
			fnb.forms.dropdown.select($('#urlAdd_parent li:eq(0)'));
			
		},
		toggleFactDiv : function(value) {
					
			if(value == 'add') {
				 fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#factRecordDiv'},{show:'false',element:'#showUpdateURLCheckBox'},{show:'true',element:'#showAddURLCheckBox'}] );
				 $("#url").removeAttr('disabled');
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showUpdateURLCheckBox'},{show:'false',element:'#showAddURLCheckBox'}] );
				$("#url").attr('disabled','true');
			}
			
			this.clearFields();
			
		},
		
		clearFields : function() {
			
			fnb.forms.dropdown.select($('#farfn_parent li:eq(0)'));
			fnb.forms.dropdown.select($('#fawxchild_parent li:eq(0)'));
			fnb.forms.dropdown.select($('#fawxparent_parent li:eq(0)'));
			fnb.forms.dropdown.select($('#target_parent li:eq(0)'));
			fnb.forms.dropdown.select($('#type_parent li:eq(0)'));
			fnb.forms.dropdown.select($('#order_parent li:eq(0)'));
			$('#label').val("");
			$('#heading').val("");
			$('#url').val("/banking/Controller?nav=");
		
		},
		
		toggleParentRefDiv : function(value) {
			
			if(value == 'add') {
				 fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#currentParentDiv'}] );
			}
			
			fnb.forms.dropdown.select($('#childReference1_parent li:eq(0)'));
			fnb.forms.dropdown.select($('#currentParent_parent li:eq(0)'));
			fnb.forms.dropdown.select($('#parentReference_parent li:eq(0)'));
			
		},
		
		retriveParentRef : function (obj, div) {
			
			var div = div;
			var functionAction = $('#relationshipAction .radioGroupValue').val();
			var val=$(obj).attr('data-value');
		
			
			if(functionAction == 'UPDATE' && div == 'wx') {
				var url = '/banking/Controller?nav=operations.actions.navigator.ReloadFunctionDropDowns&dropdownId=currentParent&dropdownLabel=Current%20Parent&action=2&childRef='+val;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#currentParentDiv'}); 
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#currentParentDiv'}] );
			}

		},
		
		retrieveUrls : function () {
			
			var functionAction = $('#factAction .radioGroupValue').val();
			
			if(functionAction == 'ADD') {			
				var url = '/banking/Controller?nav=operations.actions.navigator.ReloadFunctionDropDowns&dropdownId=farfn&dropdownLabel=Fact%20Reference%20(FARFN)&action=4';
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#factRecordDiv'});
			}
			
		},
		
		retrieveFACTParentRef : function (obj, div) {
			
			var div = div;
			var functionAction = $('#factAction .radioGroupValue').val();
			var val=$(obj).attr('data-value');

			if(functionAction == 'UPDATE') {
			
				var url = '/banking/Controller?nav=operations.actions.navigator.ReloadFunctionDropDowns&dropdownId=farfn&dropdownLabel=Fact%20Reference%20(FARFN)&action=3&childRef='+val;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#factRecordDiv'});
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#factRecordDiv'}] );
				
			}else{
				var url = '/banking/Controller?nav=operations.actions.navigator.ReloadFunctionDropDowns&dropdownId=fawxparent&dropdownLabel=Single%20Parent%20(FASGLPARENT)&action=2&childRef='+val;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#currentFAParentDiv'});
			}
		},
		
		retriveFACTDetails : function (obj) {
			
			var val=$(obj).attr('data-value');
			var functionAction = $('#relationshipAction .radioGroupValue').val();
			
			var url = '/banking/Controller?nav=operations.actions.navigator.FunctionRetrieveFact&dropdownId=farfn&action=getFactRecord&dropdownLabel=Fact%20Reference%20(FARFN)&reference='+val;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#factDetailsDiv'});
			
		},
		retriveURLDetails : function (obj) {
			
			
			var val =$(obj).attr('data-value');
				
			var url = '/banking/Controller?nav=operations.actions.navigator.FunctionRetrieveUrl&dropdownId=urrfn&&dropdownLabel=URL&childRef='+val;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#showEditUrl'});
			
		},
		
		
		
		processFunctionRef : function(){
			
			var functionAction = $('#functionAction .radioGroupValue').val();
			var childRef ='';
			var childDescription = $('#functionDescription').val();
						
			if(functionAction == 'UPDATE') {
				childRef = $('#childReference').val();	
			}
			
			var url = '/banking/Controller?nav=operations.actions.navigator.FunctionReferenceProcess&action='+functionAction+'&reference='+childRef+'&description='+childDescription;
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#functionResults'});
		},
		
		processFunctionParentRef : function(){
			
			var functionAction = $('#relationshipAction .radioGroupValue').val();
			var childRef ='';
			var currentParentRef ='';
			var newParentRef ='';
						
			if(functionAction == 'UPDATE') {
				currentParentRef = $('#currentParent').val();	
			}
			
			childRef = $('#childReference1').val();	
			newParentRef = $('#parentReference').val();	
	
			
			var url = '/banking/Controller?nav=operations.actions.navigator.FunctionParentProcess&action='+functionAction+'&childRef='+childRef+'&currentParentRef='+currentParentRef+'&newParentRef='+newParentRef;
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#functionParentResults'});
			
			
		},
		
		processFactRef : function(){
			
			var isChecked = $('#urlUpdate').attr('checked')=='checked';
			
			var functionAction = $('#factAction .radioGroupValue').val();
			var farfn = $('#farfn').val();
			var heading = $('#heading').val();
			var label = $('#label').val();
			var target = $('#target').val();
			var type = $('#type').val();
			var url = $('#url').val().replace(new RegExp('&', 'g'),'|');
			var faparent = $('#fawxparent').val();
			var fawxchild = $('#fawxchild').val();
			var urrfn = $('#urrfn').val();
			var version = $('#version').val();
			var urlAdd = $('#urlAdd').attr('checked')=='checked';
			var order = $('#order').val();
			var column = $('#column').val();
			
			var url = '/banking/Controller?nav=operations.actions.navigator.FunctionFactProcess&action='+functionAction+'&version='+version+'&urlAdd='+urlAdd
						+'&fawxchild='+fawxchild+'&urrfn='+urrfn+'&urlUpdate='+isChecked+'&farfn='+farfn+'&heading='+heading+'&label='+label+'&target='+target
						+'&type='+type+'&url='+url+'&functionParent='+faparent+'&order='+order + '&column=' + column ;
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#functionFACTResults'});
			
		},
		
		clearCookies : function(){
			
			var cookie_date = new Date ( );  // now
			cookie_date.setTime ( cookie_date.getTime() - 1 ); // one second before now.
			// empty cookie's value and set the expiry date to a time in the past.
			document.cookie = "logged_in=; expires=" + cookie_date.toGMTString();
		}
		
		
	}
	
	namespace("fnb.operations.FunctionManager",genericPageObject); 
});