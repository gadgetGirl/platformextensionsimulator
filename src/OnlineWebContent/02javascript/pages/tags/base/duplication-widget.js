$(function(){
	var methods = {
		init : function(options) {

		//default options
			var defaults = {
				wrapper			:	'#dwWrapper',
				templateParent	:	'dwParent',
				max				:	5,
				count			:	0,
				template		:	{},
				addButton		:	'dwAddButton',
				removeButton	:	'dwRemoveButton',
				inputIds		:	[],
				multiple        :   false
			
				
			};
			var options = $.extend(defaults, options);

			return this.each(function() {
				
				var rmvBtt = $('.dwRemoveButton');
				
				$('#'+options.addButton).bind('click', function(){
					methods.addRow(options);
				});
			
				$(options.wrapper).on('click','.dwRemoveButton',function(event){
					var parentDiv = $(event.currentTarget).parent().parent().parent().parent();
					methods.removeRow(options,parentDiv)
				});

				options.inputIds.push(options.removeButton);
				if(options.multiple == true){
					options.template = $('#templateContainer').find('.dwParent');
					$(options.wrapper).data('template',options.template);
					var templateClone = $(options.wrapper).data('template').clone();
					$('#templateContainer').remove();
					var rows = $('.'+options.templateParent);
					//$(options.wrapper).empty();
					rows.each(function(index,el){
						if(noBranchCodeAndModel){
							fnb.hyperion.pages.duplicationWidget.updateModel(options.count,'add',templateClone);
						}
						options.count++;
						if(defaults.count == 5){
							$("#dwAddButtonWrapper").hide();
						}
					})
					
				}else{
					options.template = $('#'+options.templateParent);
					$(options.wrapper).data('template',options.template);
					$(options.wrapper).empty();
					methods.addRow(options);	
				}
	
			});	//END init -> return this.each

		},	//END init Method

		addRow : function(options) {

			var templateClone = $(options.wrapper).data('template').clone();
			
			var rmvBtt = $(templateClone).find('.dwRemoveButton');
			
			$.each(options.inputIds,function(i,n){
				$(templateClone).find('input[id^="'+n+'"]').attr({'name':n+options.count,'id':n+options.count});
				if (i == (options.inputIds.length-1) ) {
					if (options.count == 0) {
						rmvBtt.hide();
					} else if (options.count == 1) {
						$('#'+options.templateParent+'0').find('.dwRemoveButton').show();
					}					
				}
			});
				
		
			$(templateClone).attr('id',options.templateParent+options.count).appendTo(options.wrapper);
				
			if(noBranchCodeAndModel){
				fnb.hyperion.pages.duplicationWidget.updateModel(options.count,'add',templateClone);
			}
			options.count++;

			if (options.count == options.max) {
				$('#'+options.addButton+'Wrapper').hide();
			}
		
		},	//END addRow Method */

		removeRow : function(options,elToRemove) {
			
			var modelIndex = $(elToRemove).attr('id');
			modelIndex = modelIndex.match(/\d+/g).toString();
			
			$(elToRemove).remove();
			options.count--;
			methods.shuffle(options);
			if (options.count == (options.max-1)){
				$('#'+options.addButton+'Wrapper').show();
			};
			if(noBranchCodeAndModel){
				fnb.hyperion.pages.duplicationWidget.updateModel(modelIndex,'remove');
			}
		},	//END removeRow Method */

		shuffle : function(options) {
			options.count = 0;
			var rmvBtt = $(options.wrapper).find('.'+options.templateParent).find('.dwRemoveButton');
			$(options.wrapper).find('.'+options.templateParent).each(function(index,el){
				var shufflerRow = $(this);
				var rmvBtt = $(el).find('.dwRemoveButton');
				$.each(options.inputIds,function(i,n){
					$(shufflerRow).find('input[id^="'+n+'"]').attr({'name':n+options.count,'id':n+options.count});
					if (i == (options.inputIds.length-1) ) {
						if (options.count == 0) {
							rmvBtt.hide();
						} else if (options.count == 1) {
							$('#'+options.templateParent+'0').find('.dwRemoveButton').show();
						}
					}
				});

				$(shufflerRow).attr('id',options.templateParent+options.count);
				
				options.count++;
			});
			
		}

	};	//END Methods

	$.fn.duplicationWidget = function(method) {
		// Method calling logic
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.duplicationWidget' );
		}

	};	// END fn.duplicationWidget
});
/*--HYPERION CODE--*/

(function() {
	branchCodeUtils = function() {

		this.branchCodeObject = {}
		console.log(this.branchCodeObject)
	};
	branchCodeUtils.prototype = {
			autoInit:false,
			showField: false,
			matchResult:'',
			init : function(lowerCodes, nums) {

			var parentObject = this;

			parentObject.branchCodeObject['lowercodes'] = lowerCodes.substring(1, lowerCodes.length - 1).split(',');
			parentObject.branchCodeObject['nums'] = nums.substring(1,nums.length - 1).split(',');
			if(noBranchCodeAndModel){
				parentObject.bindEvents();
			}
			console.log(parentObject.branchCodeObject);
		},
		matchBranchCode : function(returnValue) {

			var parentObject = this;
			
			var inputVal = returnValue;
			var subInputVal = '';
			if (!isNaN(inputVal)) {
				inputVal = parseFloat(returnValue);
			}
			else {
				if ((typeof inputVal != 'undefined')&&inputVal.length>=4) subInputVal=returnValue.substring(0,4);
			}

			var inputCount = returnValue.length;
			var branchCode = '';
			var found = false;
			var lastIndex = 0;
			var previousValue = 0;

			if (returnValue == '') {
				return 'emptyField';
			}

			$.each(parentObject.branchCodeObject['lowercodes'], function(index, item) {
				
				var previousIndex = 0;
				if(index>0) previousIndex = parseInt(parentObject.branchCodeObject['lowercodes'][index-1]);
				previousValue = previousIndex;
				lastIndex = index; 
				
				if(inputVal>=previousIndex && inputVal<parseFloat(item)){
					
					branchCode = parentObject.branchCodeObject['nums'][index-1];
					
					console.log('branchCode: '+branchCode)
					if (branchCode == 54 || branchCode == 8) {
						
						parentObject.matchResult = 'isFNB';
						
						return false;
					}else{

						parentObject.matchResult = 'noMatch';
						
						return false;
						
					}
				}
			});
		
			return parentObject.matchResult;
		},
		bindEvents : function() {
			
			var parentObject = this;
			$('#dwWrapper').on('blur','.branchCodeCheck',function(event) {

				
					fnb.hyperion.pages.branchCodeUtils.checkInput($(event.currentTarget))
				
			});
			
			

		},
		checkInput: function(element,isInput){
			console.log('checkInput')
			console.log(element)
			
			var parentObject = this;
			
			parentObject.matchResult = '';
			
			var inputVal;
			
			if(isInput){
				inputVal = fnb.hyperion.pages.branchCodeUtils.matchBranchCode($(element).val());
			}else{
				inputVal = fnb.hyperion.pages.branchCodeUtils.matchBranchCode($(element).find('input').val());	
			}
			console.log('inputVal: '+inputVal)
			if (inputVal == 'noMatch') {
				fnb.hyperion.pages.duplicationWidget.showEmailField();
				parentObject.showField = true;
				
				
			} else if (inputVal == 'emptyField') {
				fnb.hyperion.pages.duplicationWidget.hideEmailField();
				parentObject.showField = false;
				
			} else if (inputVal == 'isFNB') {
				fnb.hyperion.pages.duplicationWidget.hideEmailField();
				parentObject.showField = false;
			}
			
		}
	};

	fnb.namespace('pages.branchCodeUtils', branchCodeUtils, true);

})();

(function() {
	duplicationWidget = function() {
		this.emailField = $('#emailBlock');
		if($('#inboxBlock')){
			this.inboxField = $('#inboxBlock');
		}
		if($('#emailBlockZBI')){
			this.emailFieldZBI = $('#emailBlockZBI');
		}
		if($('#deliveryOptionsBlock')){
			this.deliveryOptionsField = $('#deliveryOptionsBlock');
		}
	};
	duplicationWidget.prototype = {
		autoInit:false,
		model : [],
		showEmailField : function() {
			var parentObject = this;
			parentObject.emailField.removeClass('displayNone')
			parentObject.inboxField.removeClass('displayNone')
			parentObject.deliveryOptionsField.removeClass('displayNone')
			parentObject.emailFieldZBI.addClass('displayNone') // We never show the email block for ZBI in here
		},
		hideEmailField : function() {
			var parentObject = this;
			parentObject.emailField.addClass('displayNone')
			parentObject.inboxField.addClass('displayNone')
			parentObject.deliveryOptionsField.addClass('displayNone')
			parentObject.emailFieldZBI.addClass('displayNone') // We never show the email block for ZBI in here
		},
		updateModel : function(index, action, item) {
			var parentObject = this;
			var model = fnb.hyperion.pages.duplicationWidget.model
			var executeAction = action
			if (executeAction == 'remove') {

				model.splice(index, 1);

			} else if (executeAction == 'add') {

				var modelItem = {
					isFNB : '',
					hasValue : ''
				}
				modelItem['item'] = '#branchCode'+index;
				model.push(modelItem);
			}
			parentObject.scanModel();
		},
		scanModel : function() {
			var parentObject = this;
			var model = fnb.hyperion.pages.duplicationWidget.model
			for ( var i = 0; i < model.length; i++) {

				
				fnb.hyperion.pages.branchCodeUtils.checkInput(model[i]['item'],true);	
			}
			
			if(fnb.hyperion.pages.branchCodeUtils.showField == true){
				fnb.hyperion.pages.duplicationWidget.showEmailField();
			}
			if(fnb.hyperion.pages.branchCodeUtils.showField == false){
				fnb.hyperion.pages.duplicationWidget.hideEmailField();
			}
		}
	};

	fnb.namespace('pages.duplicationWidget', duplicationWidget, true);

})();