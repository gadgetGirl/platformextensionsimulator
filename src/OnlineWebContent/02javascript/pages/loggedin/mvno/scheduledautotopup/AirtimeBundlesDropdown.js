fnb.hyperion.controller.extendPage({

	//Function reference for this page object
	functionReference: "AirtimeBundleDropdown",

	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.', 
	amountDiv : '',
	endDate : '',
	remainingPurchases : '',
	scheduleExpiryOptionGroup : '',
	flexiAmount : '',
	targetElement : '',
	reInit : true,
	init : function(){
		console.log("AirtimeBundleDropdown");
		//Bind Events
		this.bindEvents();    	
		//Do global selection for this page
		this.doGlobalSelections();
	},
	bindEvents : function(){
		//List of events for this module                                                                                  
    	var events = [{events:'click', selector: '#formEl_voucherType .dropdownItem', handler: this.context+'AirtimeBundleDropdown.retriveAmountForVoucherType(event)'},
    	{events:'click', selector: '#specifyDateOption', handler: this.context+'AirtimeBundleDropdown.showHideToggle(event)'},
    	{events:'click', selector: '#remainingPurchasesOption', handler: this.context+'AirtimeBundleDropdown.showHideToggle(event)'},
    	{events:'click', selector: '#neverExpireOption', handler: this.context+'AirtimeBundleDropdown.showHideToggle(event)'},
    	{events:'click', selector: '#amount .dropdownItem', handler: this.context+'AirtimeBundleDropdown.showHideToggle(event)'},
    	{events:'click', selector: '#formEl_frequency .dropdownItem', handler: this.context+'AirtimeBundleDropdown.showHideToggle(event)'}];
    	//Append events to actions module
    	
    	fnb.hyperion.actions.addEvents(events);
	},
	doGlobalSelections: function(){
	
		// select amount Div. Dropdown will be loaded into this div.
		this.amountDiv = fnb.hyperion.$('#amountDiv'); 
		// preselect form elements to show and hide 
		this.endDate = fnb.hyperion.$('#endDate_datePicker');
		this.remainingPurchases = fnb.hyperion.$('#formEl_remainingPurchases');
		this.scheduleExpiryOptionGroup = fnb.hyperion.$('#formEl_scheduleExpiryOption');
		this.flexiAmount = fnb.hyperion.$('#formEl_flexiAmount');

		var voucherType = fnb.hyperion.$('#voucherType').val();
		if(voucherType != 'please select' && (voucherType == 'Data Bundles' || voucherType =='Blackberry Bundles' || voucherType == 'SMS Bundles' || voucherType == 'Airtime')){
				var amountSelected = fnb.hyperion.$('#amountSelected').val();
				this.laodVoucherType(voucherType,amountSelected);
			}	

	    this.remainingPurchases.hide();
	    this.flexiAmount.hide();
	    
	    var radioSelectedArr = fnb.hyperion.$('#formEl_scheduleExpiryOption').find('.radioButton');

	      radioSelectedArr.each(function(element, index){

	             if(element.attr('data-state') == 'checked'){
	            	   targetId = element.attr('id');
	            	   fnb.hyperion.controller.page.AirtimeBundleDropdown.doShowHide(targetId);
	             }
              });
	},
	showHideToggle : function(event){
			_this = fnb.hyperion.controller.page.AirtimeBundleDropdown;
		    _this.targetElement = fnb.hyperion.$(event.currentTarget);
			var	targetElementParent = _this.targetElement.parent();
			//var	targetElementParentParent = _this.targetElement.parent().parent();
			var	targetId = (targetElementParent.attr('id') == undefined || targetElementParent.attr('id') == null )? _this.targetElement.attr('id') : targetElementParent.attr('id') ;
			_this.doShowHide(targetId);
			
	},
	//hide add show elements depending on id given.
	doShowHide : function(targetId){
		_this = fnb.hyperion.controller.page.AirtimeBundleDropdown;
		if(targetId == 'specifyDateOption'){
			_this.remainingPurchases.hide();	
			_this.endDate.show();
		}else if(targetId == 'remainingPurchasesOption'){
			_this.remainingPurchases.show();	
			_this.endDate.hide();
		}else if(targetId == 'neverExpireOption'){
			_this.remainingPurchases.hide();	
			_this.endDate.hide();
		}else if(targetId == 'frequency' && _this.targetElement.html() == 'Once off'){
			_this.scheduleExpiryOptionGroup.hide();
			_this.remainingPurchases.hide();	
			_this.endDate.hide();
		}else if(targetId == 'frequency' && _this.targetElement.val() != 'Once off'){
			_this.scheduleExpiryOptionGroup.show();
			/*_this.remainingPurchases.show();
			_this.endDate.show();*/
		}else if(targetId == 'amount' && _this.targetElement.html() == 'Flexi Recharge'){
			_this.flexiAmount.show();
		}else if(targetId == 'amount' && _this.targetElement.html() != 'Flexi Recharge'){
			_this.flexiAmount.hide();
		}
	},
	retriveAmountForVoucherType : function (event) {
		_this = fnb.hyperion.controller.page.AirtimeBundleDropdown;
		var element = fnb.hyperion.$(event.currentTarget);

		// change dropdown text to loading
		var val = element.attr('data-value');
		var url = '/banking/Controller?nav=mvno.topup.navigator.AirtimeBundleDropDown&dropdownId=amount&dropdownLabel=Amount&valueSelected='+val;
			fnb.hyperion.controller.raiseEvent('asyncLoadContent',{url:url, target: fnb.hyperion.$('#amountDiv'), postLoadingCallback : fnb.hyperion.controller.page.AirtimeBundleDropdown.initDropDown})
			fnb.hyperion.$('#amountDiv').show();
	},
	laodVoucherType : function(voucherType,amountSelected){
			var url = '/banking/Controller?nav=mvno.topup.navigator.AirtimeBundleDropDown&dropdownId=amount&dropdownLabel=Amount&valueSelected='+voucherType+'&amountSelected='+amountSelected;
			fnb.hyperion.controller.raiseEvent('asyncLoadContent',{url:url, target: fnb.hyperion.$('#amountDiv'), postLoadingCallback : fnb.hyperion.controller.page.AirtimeBundleDropdown.initDropDown})
			fnb.hyperion.$('#amountDiv').show();
	},
	//initDropDown 
	initDropDown: function(){
		//initialize PageEvents				
		fnb.hyperion.controller.initPageEvents();
		var amountType = fnb.hyperion.$('#formEl_amount').find('.dropdownSelected').html();
		var frequency = fnb.hyperion.$('#formEl_frequency').find('.dropdownSelected').html();
		if(amountType == 'Flexi Recharge'){
			_this.flexiAmount.show();
		}
		if(frequency == 'Once off'){
			_this.scheduleExpiryOptionGroup.hide();
			_this.remainingPurchases.hide();	
			_this.endDate.hide();
		}
	}
});