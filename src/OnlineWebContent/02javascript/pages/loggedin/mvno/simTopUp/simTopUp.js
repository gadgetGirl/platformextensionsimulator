fnb.hyperion.controller.extendPage({

	//Function reference for this page object
	functionReference: "simTopUp",

	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.', 
	configObject : '',
	flexiDiv : '',
	flexiStatus : '',
	airtimeLi : '',
	reInit : true,
	init : function(){
    	console.log("Init simTopUp");
    	
   		this.configObject = fnb.hyperion.load.includesData.simTopUp;
  
		//Bind Events
		this.bindEvents();

		var isPostPaid = this.configObject.isPostpaid;

		if(isPostPaid == "false") {
			this.doGlobalSelections();
			
			if(this.flexiDiv.length()>0) this.flexiDiv.hide();
			
			this.checkFlexi(this.airtimeLi);
		}
	},
	bindEvents : function(){
		//List of events for this module
    	var events = [{events:'click', selector: '#formEl_airtime .dropdownItem', handler: this.context+'simTopUp.showHideFlexi(event)'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
	},
	doGlobalSelections:function(){
		// select and hide footer buttons.
		this.flexiDiv = fnb.hyperion.$('#flexiRechargeDiv');
		this.flexiStatus = fnb.hyperion.$('#flexiStatus');
		this.airtimeLi = fnb.hyperion.$('#formEl_airtime').find('.dropdownSelected');
	},
	checkFlexi: function(element){
		_this = fnb.hyperion.controller.page.simTopUp;
		var targetVal = element.html();

		if( targetVal == 'Flexi Recharge'){
			if(this.flexiDiv.length()>0) _this.flexiDiv.show();
			_this.flexiStatus.val('enabled');
		}else if( targetVal != 'Flexi Recharge'){
			if(this.flexiDiv.length()>0) _this.flexiDiv.hide();
			_this.flexiStatus.val('disabled');
		}

	},
	showHideFlexi: function(event){
			_this = fnb.hyperion.controller.page.simTopUp;
			var targetElement = fnb.hyperion.$(event.currentTarget),
				targetVal = targetElement.html();

			if( targetVal == 'Flexi Recharge'){
				if(this.flexiDiv.length()>0) _this.flexiDiv.show();
				_this.flexiStatus.val('enabled');
			}else if( targetVal != 'Flexi Recharge'){
				if(this.flexiDiv.length()>0) _this.flexiDiv.hide();
				_this.flexiStatus.val('disabled');
			}

	}
});