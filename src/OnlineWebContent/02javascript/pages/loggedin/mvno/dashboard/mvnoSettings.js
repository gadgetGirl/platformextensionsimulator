///-------------------------------------------///
/// developer: 
///
/// MVNODashboard.jsp javascript
///-------------------------------------------///

fnb.hyperion.controller.extendPage({
	//Function reference for this page object
	functionReference: "mvnodashboard",
	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.',
	otherNumber : '',
	radioButtonother : '',
	reInit : true,
	//init method
	init : function(){
		//Bind Events
		this.bindEvents();
		//Do global selection for this page
		this.doGlobalSelections();
	},
	//Bind page events
	bindEvents : function(){
		//List of events for this module
    	var events = [{events:'click', selector: '#simpleFormEl_simAccounts .dropdownItem', handler: this.context+'mvnodashboard.changeSimCard(event)'},
    	              {events:'click', selector: '#voicemail', handler: this.context+'mvnodashboard.setCallManageOption(event)'},
    	              {events:'click', selector: '#other', handler: this.context+'mvnodashboard.setCallManageOption(event)'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
	},
	doGlobalSelections: function(){
		//select otherNumber textbox
		this.otherNumber = fnb.hyperion.$('#MVNO_CM2widgetRowItemotherNumber');
		// default is voice -> hide otherNumber
		if(this.otherNumber.length() > 0){
			this.otherNumber.hide();
		}
		
		this.radioButtonother = fnb.hyperion.$('#other');
		if(this.radioButtonother.length() > 0){
			this.checkOther();
		}
		
	},
	checkOther :function(){
		_this = fnb.hyperion.controller.page.mvnodashboard;
		if(_this.radioButtonother.attr('data-state') == 'checked'){
			this.otherNumber.show();
		}else{
			this.otherNumber.hide();
		}
	},
	
	//Load account settings on dropdown change
	changeSimCard : function(event){
		//Get selcted target
		var target = fnb.hyperion.$(event.currentTarget);
		// get value of simCard
		var val= target.attr('data-value');
		//Raise loadPage event
		fnb.hyperion.controller.raiseEvent('loadPage', {url: "/banking/Controller?nav=mvno.dashboard.navigator.MVNODashboard&index="+val}); 
	},
	setCallManageOption: function(event){
		_this = fnb.hyperion.controller.page.mvnodashboard;
		//get element selected value and show and hide textbox.
		var element = fnb.hyperion.$(event.currentTarget);
		 	elemVal = element.find('=input').val(); 
		 	_this.action = elemVal;
		 //show and hide buttons
		 if(elemVal == 'voicemail'){
			 _this.otherNumber.hide();
		 }else if(elemVal == 'other'){
			 _this.otherNumber.show();
		 }
	}
});


