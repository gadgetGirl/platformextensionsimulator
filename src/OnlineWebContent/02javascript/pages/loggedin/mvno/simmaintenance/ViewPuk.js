fnb.hyperion.controller.extendPage({

	//Function reference for this page object
	functionReference: "ViewPuk",

	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.', 
	configObject : '',
	url : '',
	reInit : true,

	init : function(){
		//Bind Events
		this.bindEvents();
    	console.log("ViewPuk");

    	this.configObject = fnb.hyperion.load.includesData.ViewPuk;
		//Bind Events
		this.bindEvents();
		//Do global selection for this page
		this.doGlobalSelections();
	},
	bindEvents : function(){
		//List of events for this module
    	var events = [{events:'click', selector: '#PrintPUK', handler: this.context+'ViewPuk.printPuk()'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
	},
	doGlobalSelections: function(){
	},
	printPuk : function(){
		
		var loadObj = {url: '/banking/Controller?nav=mvno.simmaintenance.navigator.PrintPuk', target: fnb.hyperion.controller.printDiv};

		fnb.hyperion.controller.raiseEvent('eziLoadPrintDiv', loadObj);
	}
});

