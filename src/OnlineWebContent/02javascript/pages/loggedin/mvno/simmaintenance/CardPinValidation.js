fnb.hyperion.controller.extendPage({
	//Function reference for this page object
	functionReference: "CardPinValidation",
	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.',
	pageData : '',
	configObject : '',
	cryptoURL : '',
	cardNumber : '',
	cardlength : '',
	pnLength : '',
	cntrCode : '',
	ticketDiv : '',

	init : function(){
		//Set configObject equal to page object
		this.configObject = fnb.hyperion.load.includesData.CardPinValidation;

		//Bind Events
		this.bindEvents();

		//this.doGlobalSelections();
	},
	bindEvents : function(){
		
		var _this = this;
		//List of events for this module
    	var events = [{events:'click', selector: _this.configObject.actionButton, handler: this.context+'CardPinValidation.validateCardAndPinCrypto(event)'},];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
	},
	//validate Card and Pin
	validateCardAndPinCrypto : function(event){
	
			if (this.validateCardAndPin()) {
				this.submitCardAndPin();
			}
			return false;
		},
	//validate Card and Pin
	validateCardAndPin : function (){
				// set variables 
			    this.cardNumber = fnb.hyperion.$("#cardNumber").val();
				this.cardlength = this.cardNumber.length;
				this.pnLength	= fnb.hyperion.$("#pin").val().length;
				this.cntrCode   = fnb.hyperion.$('#countryCode').val();
				this.ticketDiv	= fnb.hyperion.$("#ticketDiv");

			//validate Card length	
			if (this.cardlength != 16) {
		
				this.errorMessage("Please enter the 16 digit card number as it appears on the card without spaces");
				return false;
			}
			//validate pin length
			if (this.pnLength != 4) {
	
				this.errorMessage("Please enter a valid 4 digit PIN");
				return false;
			}
  			return true;
		},
		// return errorMessage
		errorMessage : function(errorMessage){
			//Request error from controller
			fnb.hyperion.controller.error({message: 'Some errors have occurred...', errors:[{error: errorMessage}]},'navError');
		},
		//after pin validation submit form and return status message.
		checkResponse : function(){
			//get config object
			_this = fnb.hyperion.controller.page.CardPinValidation;
			//get respone url from pageObject
			var reponseUrl = _this.configObject.postUrl,
			formName  = _this.configObject.formName; 
			
			//buils dataSettings object for form submit.
			var dataSettings = {"event": "submitFromEziToEzi","url": reponseUrl,"target": "", "urlTarget": "","dataTarget": formName, "clearHtmlTemplates": false, "clearPageModuleObject": false, "clearPageEventsArray": false, "clearPageTemplatesArray": false};
			//submit form
			fnb.hyperion.controller.raiseEvent( "submitFromEziToEzi" ,dataSettings);
			
		},
		// form validation Card and min.
		submitCardAndPin : function(formName){
			//get config object
			_this = fnb.hyperion.controller.page.CardPinValidation;
			var cardNumLength = 0, errorMessage = "", success = true; 
			var isNumPattern =/^[0-9]+$/;

			//check if cardlength is 16 not smaller or larger and is numeric
			if ( ( _this.cardlength > 16) || ( _this.cardlength < 16 ) || ( _this.cardlength == 0) || !isNumPattern.test( _this.cardNumber)  ){
				errorMessage = "Please enter the 16 digit card number as it appears on the card without spaces.";
				this.errorMessage(errorMessage);

				success = false;
			}
 			//check if Pin value is numeric
            if (!isNumPattern.test( fnb.hyperion.$("#pin").val() ) ){ 
               	errorMessage = "Please enter a valid 4 digit PIN.";
				//alertO(errorMessage);
				_this.errorMessage(errorMessage);

				success = false;
            //check if Pin length is 4 not smaller or larger  	
			}else if (this.pnLength > 4 || this.pnLength < 4) {
				errorMessage = "Please enter a valid 4 digit PIN";
				//alertO(errorMessage);
				_this.errorMessage(errorMessage);

				success = false;
			}
			
			//if  success equal false return false
			if (!success) {
				//resetMultiple();
				return false;
			}
			
			// call  getTicket
			_this.getTicket();
			
			return false;
		},
		// get Ticket
		getTicket : function (){
				_this = fnb.hyperion.controller.page.CardPinValidation;
				_this.ticketDiv.html('');
			var	formName = _this.configObject.formName.replace('#','');

				// build ajax url
				var urlTick="/banking/Controller?nav=cardpin.crypto.navigator.CardPinCryptoRegistrationTickectNoOTP&formname="+formName+"&action=getticket&countryID="+_this.cntrCode+"&cardNumber="+_this.cardNumber;
				// do ajax call asyncLoadContent 
				fnb.hyperion.controller.raiseEvent('asyncLoadContent',{url:urlTick, target: _this.ticketDiv,postLoadingCallback:_this.executeGetTicket})
		},
		//validate ticket and call checkResponse
		executeGetTicket : function(loadObj){
			//set variables 
			  _this = fnb.hyperion.controller.page.CardPinValidation;
        	var p = fnb.hyperion.$("#pin").val(),
			   card = fnb.hyperion.$("#cardNumber").val(),
		  secureUrl = _this.configObject.cryptoURL,
		  
		       	link = secureUrl+"/authenticatepin?ticket=" + loadObj.data + "&cardno=" + card.substring(3, 15) + "&pin=" + p; 
				//set ifrom src value
				fnb.hyperion.$("#res").prop('src', link );
				//call checkResone Function
				fnb.hyperion.controller.page.CardPinValidation.checkResponse();
		}
});