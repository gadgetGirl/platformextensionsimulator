fnb.hyperion.controller.extendPage({

	//Function reference for this page object
	functionReference: "OrderSIM",

	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.', 
	configObject : '',
	url : '',

	init : function(){
		//Bind Events
		this.bindEvents();
    	console.log("PrintSIM");

	},
	bindEvents : function(){
		//List of events for this module
    	var events = [{events:'click', selector: '#PrintSIM', handler: this.context+'OrderSIM.printSIM()'},
    	              {events:'click', selector: '#DownloadSIM', handler: this.context+'OrderSIM.downloadSIM()'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
	},
	printSIM : function(){
		var loadObj = {url: '/banking/Controller?nav=mvno.simmaintenance.navigator.OrderNewSimResult', target: fnb.hyperion.controller.printDiv};

		fnb.hyperion.controller.raiseEvent('eziLoadPrintDiv', loadObj);
	},
	downloadSIM : function(){

		//Hyperion Raise download event
		var loadObj = {url: '/banking/Controller?nav=mvno.simmaintenance.navigator.OrderNewSimResultsEziDownload'};
		fnb.hyperion.controller.raiseEvent('download', loadObj);
	}
});

