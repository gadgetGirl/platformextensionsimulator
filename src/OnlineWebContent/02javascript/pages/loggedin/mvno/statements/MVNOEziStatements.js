fnb.hyperion.controller.extendPage({

	//Function reference for this page object
	functionReference: "MVNOEziStatements",

	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.', 
	configObject : '',
	url : '',
	downloadBtn : '',
	continueBtn : '',
	action : '',
	radioOptEmail : '',
	radioptDownlaod : '',
	reInit : true,
	statementReturned : false,
	init : function(){
		//Bind Events
		this.bindEvents();
    	console.log("MVNOEziStatements");

    	this.configObject = fnb.hyperion.load.includesData.MVNOEziStatements;
		//Bind Events
		this.bindEvents();
		//Do global selection for this page
		this.doGlobalSelections();
	},
	bindEvents : function(){
		//List of events for this module
    	var events = [{events:'click', selector: '#MVNOEziStatementDownload', handler: this.context+'MVNOEziStatements.doDownload()'},
			    	  {events:'click', selector: '#radioButtonactionEmail', handler: this.context+'MVNOEziStatements.setAction(event)'},
			    	  {events:'click', selector: '#radioButtonactionDownload', handler: this.context+'MVNOEziStatements.setAction(event)'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
	},
	doGlobalSelections: function(){
		// select and hide footer buttons.
		this.downloadBtn = fnb.hyperion.$('#MVNOEziStatementDownload');
		this.downloadBtn.hide();
		this.continueBtn = fnb.hyperion.$('#MVNOEziStatementContinue');
		this.continueBtn.hide();
		// set action
	    this.action = "email";

	    //email radio is the default selected and downlaod unchecked.
		this.radioOptEmail = fnb.hyperion.$('#radioButtonactionEmail');
		this.radioptDownlaod = fnb.hyperion.$('#radioButtonactionDownload');

		// set radio Email as checked
		this.radioOptEmail.attr('data-state', 'checked');
        //Change email radio form element value
        this.radioOptEmail.find('=input').attr('checked',true);
      	
      	// set radio radio data attribute of checkbox as unChecked
    	this.radioptDownlaod.attr('data-state', 'unChecked');
    	//set radio radio form element value
    	this.radioptDownlaod.find('=input').attr('checked','');

    	//retrieveSingleStatements
    	this.retrieveSingleStatements();

	},
	setAction : function(event){
		_this = fnb.hyperion.controller.page.MVNOEziStatements;
		var element = fnb.hyperion.$(event.currentTarget);
		 	elemVal = element.find('=input').val(); 
		 	_this.action = elemVal;
		 //show and hide buttons
		 if(elemVal == 'download'){
			_this.continueBtn.hide();
			if(fnb.hyperion.controller.page.MVNOEziStatements.statementReturned) _this.downloadBtn.show();
		 }else if(elemVal == 'email'){
			_this.downloadBtn.hide();
			if(fnb.hyperion.controller.page.MVNOEziStatements.statementReturned) _this.continueBtn.show();
		 }
	},
	doDownload : function(){
		      _this = fnb.hyperion.controller.page.MVNOEziStatements;
		//set varaibles
		var  action = _this.action,		 			 	
			   value = fnb.hyperion.$('#statementReference').val();
		//   _this.url = '/banking/Controller?nav=mvno.statements.navigator.MVNOEziStatementDownloadConfirm&statementReference='+value+'&action=download';
		   //get file to download.	
		   //var dataSettings = {"url": _this.url,"target": "", "urlTarget": "","dataTarget": formName, "clearHtmlTemplates": false, "clearPageModuleObject": false, "clearPageEventsArray": false, "clearPageTemplatesArray": false};
	
		   fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '');
		   window.open('/banking/Controller?nav=mvno.statements.navigator.MVNOEziStatementDownloadConfirm&statementReference='+value+'&action=download');
		   
		   
		   
		//   fnb.controls.controller.eventsObject.raiseEvent('doDownload', _this.url);
		   //fnb.hyperion.controller.raiseEvent("download", _this.url );
	},
	//retrieve Statement Ref 
	retrieveSingleStatements : function () {
		_this = fnb.hyperion.controller.page.MVNOEziStatements;
		
		// change dropdown text to loading
		//fnb.hyperion.$('#showStatementsList').find('![data-role="dropdownSelected"]').html('loading...');
 		//find('[data-role="dropdownSelected"]').html('loading...');
		//set varaibles
		var  action = _this.action,
			  anrfn =  _this.configObject.statementsAvailable,
		        url = '/banking/Controller?nav=mvno.statements.navigator.MVNORetrieveStatements&anrfn='+anrfn+'&action='+action;
		
		//do ajax call load statementRef

		//fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#showStatementsList'}); 
		// do ajax call asyncLoadContent 
		fnb.hyperion.controller.raiseEvent('asyncLoadContent',{url:url, target: fnb.hyperion.$('#showStatementsList'), postLoadingCallback : fnb.hyperion.controller.page.MVNOEziStatements.initDropDown})



		//show and hinde div #showStatementsList
		//fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showStatementsList'}] );
		 fnb.hyperion.$('#showStatementsList').show();
		 //show and hide buttons
		 if(action == 'download'){
			fnb.hyperion.controller.page.MVNOEziStatements.continueBtn.hide();
			if(fnb.hyperion.controller.page.MVNOEziStatements.statementReturned) fnb.hyperion.controller.page.MVNOEziStatements.downloadBtn.show();
		 }else if(action == 'email'){
			fnb.hyperion.controller.page.MVNOEziStatements.downloadBtn.hide();
			if(fnb.hyperion.controller.page.MVNOEziStatements.statementReturned) fnb.hyperion.controller.page.MVNOEziStatements.continueBtn.show();
		 }
	},
	//initDropDown 
	initDropDown: function(){
		fnb.hyperion.controller.page.MVNOEziStatements.statementReturned = true;
		console.log('Init initDropDown')
		//initialize PageEvents				
		fnb.hyperion.controller.initPageEvents();
		
		if(fnb.hyperion.controller.page.MVNOEziStatements.action == 'download') {
			fnb.hyperion.controller.page.MVNOEziStatements.downloadBtn.show();
		}else if(fnb.hyperion.controller.page.MVNOEziStatements.action == 'email'){
			fnb.hyperion.controller.page.MVNOEziStatements.continueBtn.show();
		}
	}
});

