///-------------------------------------------///
/// developer: 
///
/// MVNOTransactionHistory.jsp javascript
///-------------------------------------------///

fnb.hyperion.controller.extendPage({
	//Function reference for this page object
	functionReference: "transactionHistory",
	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.',
	otherNumber : '',
	//init method
	init : function(){
		//Bind Events
		this.bindEvents();
	},
	//Bind page events
	bindEvents : function(){
		//List of events for this module
    	var events = [{events:'click', selector: '#simpleFormEl_simAccountsTransactionHistory .dropdownItem', handler: this.context+'transactionHistory.changeSimCard(event)'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
	},
	
	//Load account settings on dropdown change
	changeSimCard : function(event){
		//Get selcted target
		var target = fnb.hyperion.$(event.currentTarget);
		// get value of simCard
		var index = target.attr('data-value');
		//Raise loadPage event
		fnb.hyperion.controller.raiseEvent('loadPage', {url: "/banking/Controller?nav=mvno.transactionhistory.navigator.MVNOTransactionHistory&index="+index}); 
		
	}
});