$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
			
			inputVals: {},
			paymentRDL : 0,
			init: function(){

				var parentObject = this;
				
				var inputs = $("#secondaryPayBlock").find('.col2 input');
				var tableId = $("#secondaryPayBlock");
				parentObject.paymentRDL = parseInt($("#primaryPayBlock").find(".col3 .tableCellItem").html());
				
				tableId.on('blur','.col2 input',function(event){
					var currentinput = $(event.target);
					parentObject.checkDailyLimits(currentinput);
				});				
				
				$("#currentPaymentLimit").on('blur',function(event){
					inputs.each(function(index, item){
						parentObject.checkDailyLimits($(item));	
					});
				});	
				
				inputs.each(function(index, item){
					var inputItem = $(item);
					var key = inputItem.attr('id');
					parentObject.inputVals[key] = {"isOver":false};
					parentObject.checkLimits($(item));
					parentObject.checkDailyLimits($(item));	
					
				});
			},
			
			updateLimits: function(){
				var parentObject = this;
				var showPopUp = false;
				
					for(key in parentObject.inputVals) {
						if(parentObject.inputVals[key].isOver)
							{showPopUp = true;break;}
					}
				
					if(showPopUp)
						{fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '/banking/Controller?nav=settings/simple/DailyProfileLimitsCapturePopUp');}
					else
						{fnb.functions.submitFormToWorkspace.submit('dailyLimitsCaptureForm','',this);}
					return false;
							
			},
			checkLimits: function(obj){
				var parentObject = this;
				var parent = obj;
				var dailyLimit = parseInt(parent.val());
				var remainingDailyLimit = parentObject.paymentRDL;
				var note = parent.closest(".tableRowInner").find(".col4").find(".tableCellItem");
				if(dailyLimit > remainingDailyLimit)
					{
						parent.closest(".col2").next(".tableCell").find(".tableCellItem").addClass("orangeNote");
						parent.closest(".tableRowInner").find(".col4").addClass("displayNote");
					}
				else
					{
						parent.closest(".col2").next(".tableCell").find(".tableCellItem").removeClass("orangeNote");
						parent.closest(".tableRowInner").find(".col4").removeClass("displayNote");
					}		
			},
			checkDailyLimits: function(obj){
				var parentObject = this;
				var parent = obj;
				var paymentDailyLimit = parseInt($("#currentPaymentLimit").val());console.log("parent.val()  "+parent.val())
				var thisDailyLimit = parseInt(parent.val());
				if(thisDailyLimit > paymentDailyLimit)
					{
					parentObject.inputVals[obj.attr('id')].isOver = true;
					}
				else
					{
					parentObject.inputVals[obj.attr('id')].isOver = false;
					}		
			}
	}
			namespace("profileLimitSetting", genericPageObject);
	});