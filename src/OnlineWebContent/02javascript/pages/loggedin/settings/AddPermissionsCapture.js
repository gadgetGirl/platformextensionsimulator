$(function() {
	function genericPageObject() {
		this.configObject = {};
		
	}
	genericPageObject.prototype = {
		init : function(dataSource1, dataSource2) {

			

		},
		cardDelivery : function(me) {
			setTimeout(function(){	
				var accountRole = $("#accountRole").val();
				var signingOfCheque = $("#signingOfCheque").val();
				var chequeCard = $("#cardTypeCardHolder").val();
				var cardColor = $("#cardColor").find('input').val();
				var petrolCard = $("#petrolCardRequired").find('input').val();
				var prevChequeCard = $("#hadChequeCard").val();
				var prevPetrolCard = $("#hadPetrolCard").val();
				var prevCardColor = $("#oldColor").val();
				var isIslamicAccount = $("#isIslamicAccount").val();
				var hadFullFunctionChequeCard = $("#hadFullFunctionChequeCard").val();
				var cardType = $("#cardType_dropdownPaired").val();
				var fullFunctionChequeCard = '';
					
				if (accountRole == '3' || accountRole == '1') {
					console.log("For cardholder option we determine cheque card required flag from cardTypeCardholder variable. cardTypeCardholder = "+ chequeCard);
					if (chequeCard == '1') {
						chequeCard = 'y';
						fullFunctionChequeCard = 'false';
					} else if (chequeCard == '2') {
						chequeCard = 'y';
						fullFunctionChequeCard = 'true';
					}
			
					
				} else if (accountRole == '4') {
					console.log("For Customise option we determine cheque card required flag from cardType variable. CardType = "+ cardType);
					if (cardType == '1' || cardType == '2') {
						chequeCard = 'y';	
					} else {
						chequeCard = 'n';
					}

					if (signingOfCheque == '1' || signingOfCheque == '3') {
						fullFunctionChequeCard = 'true';
						
					} else if (signingOfCheque == '2') {
						fullFunctionChequeCard = 'false';
						
					}
				}
				
				console.log("isIslamicAccount = "+ isIslamicAccount);
				console.log("accountRole = "+ accountRole);
				console.log("signingOfCheque = "+ signingOfCheque);
				console.log("---------- previous Cards info started ------------");
				console.log("prevChequeCard = "+ prevChequeCard);
				console.log("hadFullFunctionChequeCard = "+ hadFullFunctionChequeCard);
				console.log("prevCardColor = "+ prevCardColor);
				console.log("prevPetrolCard = "+ prevPetrolCard);
				console.log("---------- current Cards info started ------------");
				console.log("chequeCard = "+ chequeCard);
				console.log("fullFunctionChequeCard = "+ fullFunctionChequeCard);
				console.log("cardColor = "+ cardColor);
				console.log("petrolCard = "+ petrolCard);
				console.log("---------- Cards info debug ended ------------");
				console.log("me = "+ me);

				if ((chequeCard == 'y' && prevChequeCard == 'false') || (petrolCard == 'y' && prevPetrolCard == 'false') || (chequeCard == 'y' && prevChequeCard == 'true' && hadFullFunctionChequeCard != fullFunctionChequeCard) || (chequeCard == 'y' && prevChequeCard == 'true' && isIslamicAccount == 'false' && cardColor != prevCardColor) || me == 'y') {
					fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:'#deliveryDetailsContainer'});
					console.log("doshow");
				} else {
					fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#deliveryDetailsContainer'});
					console.log("dontshow");
				}
			},2)
			
		},
		cardTypeChosen : function(me) {
			var cardChosen = $(me).attr("data-value");
			
			if (cardChosen == '3' || cardChosen == '0'){
				$("#cardColorContainer").hide();
				$("#chequeLimitsContainer").hide();				
			}else if (cardChosen == '1'){
				$("#cardColorContainer").hide();
				$("#chequeLimitsContainer").show();
				$("#setLimits").show();
			}else if (cardChosen == '2'){
				$("#cardColorContainer").hide();
				$("#chequeLimitsContainer").show();
				$("#setLimits").show();
			}
			fnb.pages.settings.cuac.AddPermissionsCapture.cardDelivery();

		},
		signingOfChequeChosen : function(me) {
			var signingOfCheque = $(me).attr("data-value");
			console.log(signingOfCheque);
			if (signingOfCheque == '2'){
				$("#cardTypeContainer").hide(); 
				$("#chequeCardContainer").hide();
				$("#signingOfChequeJoint").show();
			}else if (signingOfCheque == '1'){
				$("#cardTypeContainer").hide(); 
				$("#chequeCardContainer").hide();
				$("#signingOfChequeJoint").hide();
			}else if (signingOfCheque == '3'){
				$("#cardTypeContainer").show();
				$("#chequeCardContainer").hide();
				$("#signingOfChequeJoint").hide();
			} else if (signingOfCheque == '0'){
				$("#cardTypeContainer").hide();
				$("#chequeCardContainer").hide();
				$("#signingOfChequeJoint").hide();
			}
			fnb.pages.settings.cuac.AddPermissionsCapture.cardDelivery();

		},
		accountRoleClick: function(me){
			var roleChosen = $(me).attr("data-value");
			var chequeCard = $("#cardTypeCardHolder").val();
			var petrolCard = $("#petrolCardRequired").find('input').val();
			
			if (chequeCard == '1') {
				chequeCard = 'y';
				fullFunctionChequeCard = 'false';
			} else if (chequeCard == '2') {
				chequeCard = 'y';
				fullFunctionChequeCard = 'true';
			}
			
			if (roleChosen == '1'){
//				$("#chequeCardContainer .switcherLabel").html('Full Function Cheque Card');
				$("#cardTypeContainer").show();
				$("#deliveryDetailsContainer").hide();
				$("#chequeLimitsContainer").hide();
				$("#cardColorContainer").hide();
				$("#setLimits2").hide();
				if (chequeCard == 'y' || petrolCard == 'y') {
					fnb.pages.settings.cuac.AddPermissionsCapture.cardDelivery();
					if (chequeCard == 'y') {
						$("#cardColorContainer").hide();
						$("#chequeLimitsContainer").show();
					} 
					if (petrolCard == 'y') {
						$("#setLimits2").show();
					} 
					
				} else {
					$("#deliveryDetailsContainer").hide();
				}
				$("#signingOfChequeContainer").hide(); 
				$("#cardTypeContainer").show();
				$("#petrolCardContainer").show();
				fnb.pages.settings.cuac.AddPermissionsCapture.cardDelivery();
			}else if (roleChosen == '2' || roleChosen == '0'){
				console.log('Yes');
				$("#chequeCardContainer").hide();
				$("#cardColorContainer").hide();
				$("#deliveryDetailsContainer").hide();
				$("#chequeLimitsContainer").hide();
				$("#setLimits2").hide();
				$("#signingOfChequeContainer").hide();
				$("#cardTypeContainer").hide(); 
				$("#petrolCardContainer").hide(); 
			}else if (roleChosen == '3'){
//				$("#chequeCardContainer .switcherLabel").html('Limited Function Cheque Card');
				$("#chequeCardContainer").hide();
				$("#cardTypeContainer").show();
				$("#chequeLimitsContainer").hide();
				$("#cardColorContainer").hide();
				$("#setLimits2").hide();
				if (chequeCard == 'y' || petrolCard == 'y') {
					fnb.pages.settings.cuac.AddPermissionsCapture.cardDelivery();
					if (chequeCard == 'y') {
						$("#cardColorContainer").hide();
						$("#chequeLimitsContainer").show();
					} 
					if (petrolCard == 'y') {
						$("#setLimits2").show();
					} 
				} else {
					$("#deliveryDetailsContainer").hide();
				}
				$("#signingOfChequeContainer").hide();
				$("#petrolCardContainer").show(); 
				fnb.pages.settings.cuac.AddPermissionsCapture.cardDelivery();
			}else if (roleChosen == '4'){
				$("#chequeCardContainer").hide();
//				$("#deliveryDetailsContainer").hide();
				$("#chequeLimitsContainer").hide();
				$("#setLimits2").hide();
				$("#signingOfChequeContainer").show();
				$("#cardTypeContainer").hide();
				$("#petrolCardContainer").show(); 
				fnb.pages.settings.cuac.AddPermissionsCapture.cardDelivery();
			}
		}
	}
	
	namespace("fnb.pages.settings.cuac.AddPermissionsCapture",genericPageObject);
});


