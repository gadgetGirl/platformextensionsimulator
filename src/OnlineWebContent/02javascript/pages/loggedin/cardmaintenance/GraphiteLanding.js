$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
						
		}, 
		pageLoaded : function() {

			var parent = this;
		},
		adjustTotalLimits :function(idVal) {
			
			var parent = this;
			
			var straightLimit=$('#straightLimitVal_'+idVal).val();
			var budgetLimit=$('#budgetLimitVal_'+idVal).val();


			if(straightLimit.toString().trim()== 0){
				$('#straightLimitVal_'+idVal).val("0");
				straightLimit = '0';
			}
			if(budgetLimit.toString().trim()== 0){
				$('#budgetLimitVal_'+idVal).val("0");
				budgetLimit = '0';
			}
			
			
			
			var totalLimit = parseFloat(straightLimit) + parseFloat(budgetLimit);
			var totalLimitString = totalLimit+'';
			var totalLimitValue = totalLimitString;
			
			if(totalLimitString.length > 3) {
				
				var part1 = totalLimitString.substring(0,totalLimitString.length-3);
				var part2 = totalLimitString.substring(totalLimitString.length-3,totalLimitString.length);
				totalLimitValue = part1 +','+part2;
			}
				
			$('#totalLimit_'+idVal).html(totalLimitValue); 
			parent.unallocatedLimits();
		},
		
		unallocatedLimits :function() {
			
			var totalLimit = parseFloat($('#totalLimit').val().replace(',',''));
			
			var rowSize = parseFloat($('#rowSize').val()+'')+parseFloat('1');
			var usedlimits = 0;
			
			for(var x =1 ; x < rowSize; x++) {
				
				var straightLimit=$('#straightLimitVal_'+x).val();
				var budgetLimit=$('#budgetLimitVal_'+x).val();
			
				usedlimits = usedlimits + (parseFloat(straightLimit) + parseFloat(budgetLimit));
			}
						
			var grandTotal = totalLimit - usedlimits;
					
			var unallocatedLimitsString = grandTotal+'';
			var unallocatedLimitsValue = unallocatedLimitsString;
			
			if(unallocatedLimitsString.length > 3) {
				
				var part1 = unallocatedLimitsString.substring(0,unallocatedLimitsString.length-3);
				var part2 = unallocatedLimitsString.substring(unallocatedLimitsString.length-3,unallocatedLimitsString.length);
				unallocatedLimitsValue = part1 +','+part2;
			}
			$('#unallocatedLimit').val(unallocatedLimitsValue);			
			$('#unallocatedLimitVal').html(unallocatedLimitsValue); 
			
		}
	}
	
	namespace("cardmaintenance.GraphiteLanding",genericPageObject);
		
});