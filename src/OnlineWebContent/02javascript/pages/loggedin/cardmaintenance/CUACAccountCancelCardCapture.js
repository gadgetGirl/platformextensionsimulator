$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();			
		}, 

		pageLoaded : function() {
			var parent = this;
			
			parent.hideFields();
			
			
		},
		
		
		/**
		 *
		 */
		showReplaceNowSection : function(me){
			var val = $(me).attr('data-value');
			var parent = this;
			
		  	if (parent.configObject.isIndividual == 'true')
	    	{
	    		if((val == '1' || val == '2' || val == '3') && ((parent.configObject.accountRelation == 'SAR') || (parent.configObject.accountRelation == 'SST')))
	    		{
	    			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.replaceNow'}]);
	    		}else{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.replaceNow'}]);
	    		}
	    	}
	    	else 
	    	{
				if((val == '1' || val == '3') && ((parent.configObject.accountRelation == 'SAR') || (parent.configObject.accountRelation == 'SST')))
	    		{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.replaceNow'}]);
	
				}else{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.replaceNow'}]);
				}
	    	}			
			
		},

		/**
		 * 
		 */
		
		hideFields : function(){
			var parent = this;

			if(parent.configObject.cancelReason == '1' || parent.configObject.cancelReason == '2' || parent.configObject.cancelReason == '3'
				&& ((parent.configObject.accountRelation == 'SAR') || (parent.configObject.accountRelation == 'SST')))
			{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.replaceNow'}]);
					if(parent.configObject.replace == 'Yes')
					{
						var radiobtn = document.getElementById("Yes");
						radiobtn.selected = 'true';
					}
					else if(parent.configObject.replace == 'No')
						{
							var radiobtn = document.getElementById("No");
							radiobtn.selected = 'true';
						}
						else
						{
							var radiobtn = document.getElementById("Yes");
							radiobtn.selected = 'false';
							var radiobtn = document.getElementById("No");
							radiobtn.selected = 'false';
						}
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.replaceNow'}]);
				var radiobtnYes = document.getElementById("Yes");
				radiobtnYes.selected = 'false';
				var radiobtnNo = document.getElementById("No");
				radiobtnNo.selected = 'false';
			}

		}	
		

	}

	namespace("fnb.cardmaintenance.CUACAccountCancelCardCapture",genericPageObject); 
	
});