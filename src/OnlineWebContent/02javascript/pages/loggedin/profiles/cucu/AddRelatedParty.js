$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {
		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
		}, 
		pageLoaded : function() {
			var parent = this;
		},
		submitForm : function() {
			fnb.functions.submitFormToWorkspace.submit('recipientFoldersCapture');
		}
	}
	namespace("fnb.profiles.cucu.AddRelatedParty",genericPageObject); 
});


