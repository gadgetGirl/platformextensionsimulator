$(function(){
	function genericPageObject (){
		this.configObject = {};
	};
	genericPageObject.prototype = {
		init: function(dataSource){
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
		}, 
		
		pageLoaded : function() {
			var parent = this;
		},
				
		validateForm: function(){
			var path = document.getElementById("theFile").value;
			
			if (path == ""){
				var errorMessage = "Please enter a file Path";
				fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
				return false;
			} 
			else {
				return true;
			}
						
		},
		submitForm : function() {
			var result = this.validateForm();
			if (result) {
				fnb.functions.submitFormToWorkspace.submit('RELATED_PARTY_UPLOAD_DOCS');
			}
			return result;
		}
		
	}
	
	namespace("fnb.profiles.cucu.RelatedPartiesFileUpload", genericPageObject);

});

