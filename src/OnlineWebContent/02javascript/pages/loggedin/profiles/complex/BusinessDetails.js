$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		init: function(){
			this.postalSameAs($('#postalYes'));
		},
		
		postalSameAs: function(item){
			if((item instanceof jQuery) == false) item = $(item);
			
			if(item.attr('data-value') == 'y'){
				$('#postalCOBuilding, #postalStreetAddress, #postalSuburb, #postalCity, #postalProvince, #postalCountry, #postalCode').attr('disabled', 'disabled');
				
				this.updateInputValue($('#postalCOBuilding'), $('#businessCOBuilding').val());
				this.updateInputValue($('#postalStreetAddress'), $('#businessStreetAddress').val());
				this.updateInputValue($('#postalSuburb'), $('#businessSuburb').val());
				this.updateInputValue($('#postalCity'), $('#businessCityName').val());
				this.updateInputValue($('#postalCode'), $('#businessPostalCode').val());
				this.updateInputValue($('#postalProvince'), $('#businessProvince').val());
				this.updateInputValue($('#postalCountry'), $('#businessCountry').val());
				
				fnb.forms.dropdown.disable($('#postalProvince'));
				fnb.forms.dropdown.disable($('#postalCountry'));
				
				fnb.forms.dropdown.onChangeCallback = this.dropdownChanged;
				
				this.dropdownChanged($('#businessProvince').closest('.formElementWrapper').find('.dropdownElementWrapper'));
				this.dropdownChanged($('#businessCountry').closest('.formElementWrapper').find('.dropdownElementWrapper'));
				
				this.createInputListenerEvents();
			} else {
				$('#postalCOBuilding, #postalStreetAddress, #postalSuburb, #postalCity, #postalProvince, #postalCountry, #postalCode').removeAttr('disabled');
				
				fnb.forms.dropdown.enable($('#postalProvince'));
				fnb.forms.dropdown.enable($('#postalCountry'));
				
				fnb.forms.dropdown.onChangeCallback = undefined;
				
				this.destroyInputListenerEvents();
			}
		},
		
		createInputListenerEvents: function(){
			var scope = this;
			
			$('#businessDetails').on('input', '#businessCOBuilding', function(){
				scope.updateInputValue($('#postalCOBuilding'), $('#businessCOBuilding').val());
			});
			
			$('#businessDetails').on('input', '#businessStreetAddress', function(){
				scope.updateInputValue($('#postalStreetAddress'), $('#businessStreetAddress').val());
			});
			
			$('#businessDetails').on('input', '#businessSuburb', function(){
				scope.updateInputValue($('#postalSuburb'), $('#businessSuburb').val());
			});
			
			$('#businessDetails').on('input', '#businessCityName', function(){
				scope.updateInputValue($('#postalCity'), $('#businessCityName').val());
			});
			
			$('#businessDetails').on('input', '#businessPostalCode', function(){
				scope.updateInputValue($('#postalCode'), $('#businessPostalCode').val());
			});
		},
		
		destroyInputListenerEvents: function(){
			$('#businessDetails').off();
		},
		
		updateInputValue: function(updatedObj, val){
			updatedObj.val(val);
		},
		
		dropdownChanged: function(obj){console.log(obj);
			var input = obj.closest('.formElementWrapper').find('.dropdown-hidden-input');
			var actualVal = input.val();
			var visualVal = obj.closest('.dropdownElementWrapper').find('.dropdown-initiator').find('.dropdown-item-row').text();
			
			var id = input.attr('id');
			var objToUpdate = '';
			
			switch(id){
				case 'businessProvince':
					objToUpdate = $('#postalProvince');
					break;
				
				case 'businessCountry':
					objToUpdate = $('#postalCountry');
			}
			
			if(objToUpdate.length > 0) fnb.forms.dropdown.setValue(objToUpdate, visualVal, actualVal);
		},
		
		getPaypalRegDropdownValue:function(dropDownType, me, divName){
			console.log("firing getPaypalRegDropdownValue for : " + divName);
			var val=$(me).attr('data-value');

			if (divName == 'sector'){
				
				if (val == 'Please Select'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#sectorDiv'} ]);
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSector'} ]);
					return false;
				}
				
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#sectorDiv'} ]);
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSector'} ]);
				fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=settings.simple.navigator.BusinessDetailsDropdownBuilder&dropDownType='+dropDownType+'&industry='+val,divName);
			}
			
			if (divName == 'subSectorDropDown'){
				
				if (val == 'Please Select'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSector'} ]);		
					return false;
				}
					
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSector'} ]);	
				fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=settings.simple.navigator.BusinessDetailsDropdownBuilder&dropDownType='+dropDownType+'&sector='+val,divName);
			}
			
		},
		
		isEditing: function(url,target){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSector'},	
			                                                      {show : 'true', element : '#subSectorDetails'} ]);	
			fnb.functions.loadDropDownDiv.load(url,target);
		},
		isEditing2: function(url,target){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSector'},
			                                                      {show : 'true', element : '#subSectorDetails'} ]);	
			fnb.functions.loadDropDownDiv.load(url,target);
		},
		isEditingPaypal: function(url,target){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#sectorDiv'},	
			                                                      {show : 'false', element : '#subSector'} ]);	
			fnb.functions.loadDropDownDiv.load(url,target);
		},
		isEditing2Paypal: function(url,target){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSector'} ]);	
			fnb.functions.loadDropDownDiv.load(url,target);
		},
		showHideSecurityPaypal: function(val){
			if (val == "1") {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#pSmsBlock'},
				                                                      {show : 'false', element : '#pEmailBlock'} ]);	
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#pSmsBlock'},
				                                                      {show : 'true', element : '#pEmailBlock'} ]);
			}			
		},
		showHideSecurity2Paypal: function(val){
			if (val == "1") {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#sEmailBlock'},
				                                                      {show : 'true', element : '#sSmsBlock'} ]);	
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#sEmailBlock'},
				                                                      {show : 'false', element : '#sSmsBlock'} ]);
			}			
		},
		destroy: function(){
			var self = this;
			self = null;
		}
	}
	
	namespace("fnb.profiles.complex.businessDetails", genericPageObject);
});