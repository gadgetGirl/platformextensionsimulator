$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		init: function(){
			this.checkStates();
			this.delayLimit();
		},
		
		limit: function(){
			var scope = this;
	   		
	   		//hack to handle new observer
        	//without a delay the firing order of script's gets messed up
        	//chatted with Don about a proper fix
        	setTimeout(function(){
				scope.delayLimit();
			}, 50);
		},
		
		delayLimit: function(){
	   		if($('#limit').find('.dropdown-initiator').find('.dropdown-item-row.dropdown-h4').text() == 'Unlimited' || $('#limit').find('.dropdownElementWrapper').hasClass('disabled')){
	   			$('#limit').find('input').attr('disabled', 'disabled').val('99999999999.99');
	   		} else {
	   			$('#limit').find('input').removeAttr('disabled');
	   		}
		},
		
		loadContent: function(element){
			namespace("pages.fnb.complex.common", new fnb.profiles.complex.common());
			fnb.controls.controller.registerObj('pages.fnb.complex.common', pages.fnb.complex.common);
			
			pages.fnb.complex.common.loadContent(element);
		},
		
		checkStates: function(){
			var obj = this;
			
			var checkboxes = $('.tableContainer').find('.checkbox-input');

			$.each(checkboxes, function(i, checkbox){
				//filter: if current checkbox has an onclick event then it must be part of a checkbox/dropdown/input group
				if($(checkbox).attr('onclick') != undefined && $(checkbox).attr('onclick') != '') obj.checkboxRules($(checkbox));
			});
		},
		
		checkboxClick: function(checkbox){
			var scope = this;
			
			//hack to handle new observer
        	//without a delay the firing order of script's gets messed up
        	//chatted with Don about a proper fix
        	setTimeout(function(){
				scope.checkboxRules($(checkbox));
			}, 50);
		},
		
		checkboxRules: function(checkbox){
			var row = checkbox.closest('.groupColCell');
			var dropdown = row.find('.dropdownElementWrapper');
			var input = row.find('.input-input');

			if(input.val() < 1) fnb.forms.dropdown.setValue(dropdown.find('.dropdown-item-row.dropdown-h4'), 'Unlimited', 'unlimited');

	   		if(checkbox.parent().hasClass('checked')){
	   			dropdown.removeClass('disabled');
	   		} else {
	   			dropdown.addClass('disabled');
	   		}

	   		//dropdown initiator sent to rule
	   		this.dropdownRules(dropdown.find('.dropdown-initiator'));
		},
		
		dropdownOnChange: function(dropdownItem){
			var scope = this;
			
			//hack to handle new observer
        	//without a delay the firing order of script's gets messed up
        	//chatted with Don about a proper fix
        	setTimeout(function(){
				scope.dropdownRules($(dropdownItem));
			}, 50);
		},
		
		//dropdownObj could be a dropdown item or the dropdown initiator but this is filtered via the jQuery selectors
		dropdownRules: function(dropdownObj){
			var column = dropdownObj.closest('.groupColCell');
			
	   		if(dropdownObj.find('.dropdown-item-row.dropdown-h4').text() == 'Unlimited' || column.find('.dropdownElementWrapper').hasClass('disabled')){
	   			column.find('.input-input').attr('disabled', 'disabled');
	   			if(dropdownObj.find('.dropdown-item-row.dropdown-h4').text() == 'Unlimited') column.find('.input-input').val('99999999999.99');
	   		} else {
	   			column.find('.input-input').removeAttr('disabled');
	   		}
		},
		
		destroy: function(){
			var self = this;
			self = null;
		}
	}
	
	namespace("fnb.profiles.complex.accountPermissions", genericPageObject);
});