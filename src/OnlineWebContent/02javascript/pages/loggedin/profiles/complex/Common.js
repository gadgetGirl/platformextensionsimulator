$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		loadContent: function(obj){
			var urlNew = $(obj).attr('data-value');
			fnb.controls.controller.raiseEvent('loadUrlToWorkspace', {url: urlNew});
		},
		
		destroy: function(){
			var self = this;
			self = null;
		}
	}
	
	namespace("fnb.profiles.complex.common", genericPageObject);
});