$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		init: function(){
			this.styleTable();
		},
		
		styleTable: function(){
			var images = this.tableImages();
			
			$.each(images, function(i, image){
				//styling added through JS as I cant manipulate the tag
				if($(image).is(':visible')) $(image).closest('table').addClass('borderTop');
			});
		},
		
		tableImages: function(){
			return $('#mainTable_').find('img');
		},
		
		destroy: function(){
			var self = this;
			self = null;
		}
	}
	
	namespace("fnb.profiles.complex.hierarchy", genericPageObject);
});