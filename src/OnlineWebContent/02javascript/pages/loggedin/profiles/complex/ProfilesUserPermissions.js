$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		init: function(){
			this.setupDefaults();
		},
		
		destroy: function(){
			var self = this;
			self = null;
		},
		
		setupDefaults: function(){
			var checkboxes = $('#userPermissions').find('.checkbox-input');
			
         	$.each(checkboxes, function(i, checkbox){
         		fnb.functions.checkboxHierarchySwitcher.childDeselected = false;
         		fnb.functions.checkboxHierarchySwitcher.checkboxClick($(checkbox));
         	});
		},
		
		checkboxClick: function(clickedCheckboxName){
			var scope = this;
        	
        	//hack to handle new observer
        	//without a delay the firing order of script's gets messed up
        	//chatted with Don about a proper fix
        	setTimeout(function(){
				scope.pauseCheckboxClick(clickedCheckboxName);
			}, 50);
		},
		
		pauseCheckboxClick: function(clickedCheckboxName){
			fnb.functions.checkboxHierarchySwitcher.childDeselected = false;
			fnb.functions.checkboxHierarchySwitcher.checkboxClick($(clickedCheckboxName));
		}
	}
	
	namespace("fnb.profiles.complex.profilesUserPermissions", genericPageObject);
});