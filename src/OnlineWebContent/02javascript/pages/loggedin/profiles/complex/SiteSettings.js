$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		init: function(){
			this.hash($('#duplicateHashCheckBox'));
		},
		
		hash: function(checkbox){
			if((checkbox instanceof jQuery) == false) checkbox = $(checkbox);
			
			checkbox.checkboxIsChecked() ? fnb.forms.dropdown.enable($('#duplicateHashCheck').siblings('.dropdownElementWrapper')) : fnb.forms.dropdown.disable($('#duplicateHashCheck').siblings('.dropdownElementWrapper'));
		},
		
		destroy: function(){
			var self = this;
			self = null;
		}
	}
	
	namespace("fnb.profiles.complex.siteSettings", genericPageObject);
});