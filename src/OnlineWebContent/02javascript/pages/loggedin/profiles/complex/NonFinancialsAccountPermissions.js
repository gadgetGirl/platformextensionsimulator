$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		init: function(){
			this.matrixIterate();
		},
		
		matrixIterate: function(){
			var scope = this;
			var checkboxes = $('.groupedCol2').find('.checkbox-input');
			
			$.each(checkboxes, function(i, checkbox){
				scope.delayRule($(checkbox));
			});
		},

		rule: function(checkbox){
			var scope = this;
			
			//hack to handle new observer
        	//without a delay the firing order of script's gets messed up
        	//chatted with Don about a proper fix
        	setTimeout(function(){
				scope.delayRule($(checkbox));
			}, 50);
		},
		
		delayRule: function(checkbox){
			var group = checkbox.closest('.groupColCell');
			var tableRow = checkbox.closest('.tableRow');
			var tpfmCheckbox = tableRow.find('.3pfm').find('.checkbox-input');
			var checkboxes = tableRow.find('.checkbox-input:checkbox');

			if($(tpfmCheckbox).checkboxIsChecked())
				{
				$(checkboxes[0]).disableCheckbox().setCheckbox(true);
				$(checkboxes[1]).disableCheckbox().setCheckbox(true);
				}
			else
				{
				$(checkboxes[0]).enableCheckbox() ;
				$(checkboxes[1]).enableCheckbox() ;
				}
			
			//	$(checkboxes[0]).checkboxIsChecked() ? $(checkboxes[1]).disableCheckbox().setCheckbox(true) : $(checkboxes[1]).enableCheckbox() ;

		},

		loadContent: function(element){
			namespace("pages.fnb.complex.common", new fnb.profiles.complex.common());
			fnb.controls.controller.registerObj('pages.fnb.complex.common', pages.fnb.complex.common);
			
			pages.fnb.complex.common.loadContent(element);
		},
		
		matrix: function(){
		},
		
		destroy: function(){
			var self = this;
			self = null;
		}
	}
	
	namespace("fnb.profiles.complex.nonFinancialsAccountPermissions", genericPageObject);
});