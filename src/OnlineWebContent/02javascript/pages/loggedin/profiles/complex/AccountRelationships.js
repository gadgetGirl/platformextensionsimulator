$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		init: function(){
		},
		
		dropdownChanged: function(obj,functionRef){
			var val=$(obj).attr('data-value');
			fnb.controls.controller.eventsObject.raiseEvent
			('loadUrlToWorkspace', {url: "/banking/Controller?nav=profiles.cuac.navigator.UserMaintenanceLandingZBI&functionRef="+functionRef+"&accountRefNum="+val});
		},
		
		dropdownChangedOnReceivedRequestsSwitcherPage: function(obj,functionRef){
			var val=$(obj).attr('data-value');
			fnb.controls.controller.eventsObject.raiseEvent
			('loadUrlToWorkspace', {url: "/banking/Controller?nav=profiles.cuac.navigator.B2bReceivedRequestsLandingZbi&functionRef="+functionRef+"&accountRefNum="+val});
		},
		
		dropdownChangedOnSentRequestsSwitcherPage: function(obj,functionRef){
			var val=$(obj).attr('data-value');
			fnb.controls.controller.eventsObject.raiseEvent
			('loadUrlToWorkspace', {url: "/banking/Controller?nav=profiles.cuac.navigator.B2bSentRequestsLandingZbi&functionRef="+functionRef+"&accountRefNum="+val});
		},
		
		destroy: function(){
			var self = this;
			self = null;
		}
	}
	namespace("fnb.profiles.accountRelationships", genericPageObject);
});