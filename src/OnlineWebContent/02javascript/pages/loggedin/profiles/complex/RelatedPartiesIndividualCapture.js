$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		init: function(){
		},
		
		destroy: function(){
			var self = this;
			self = null;
		},
		
		maritalDropdownChange: function(dropdown){
			if($(dropdown).attr('data-value') != 'M' && $(dropdown).attr('data-value') != 'S'){
				$('#maritalContracts_parent').addClass('hidden');
			} else {
				$('#maritalContracts_parent').removeClass('hidden');
			}
		}
	}
	
	namespace("fnb.profiles.complex.relatedPartiesIndividualCapture", genericPageObject);
});