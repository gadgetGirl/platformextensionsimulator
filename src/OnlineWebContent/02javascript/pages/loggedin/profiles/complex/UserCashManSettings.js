(function(){
    function TableHandler(){
    	this.clicked = {};
    }
    TableHandler.prototype = {
        init: function(){
			this.matrixIterate();
        },
        
        matrixIterate: function(){
            var rows = fnb.functions.rowHandler.getRows();
            
            //for every row with checkboxes
			$.each(rows, function(){
				//reset counter to zero
				var counter = 0;
				//get all checkboxes for current row
				var checkboxes = fnb.functions.rowHandler.getRowCheckboxes($(this));
				//store last checkbox
                var lastCheckbox = fnb.functions.rowHandler.getLastCheckbox(checkboxes);
				
                //for each checkbox of current row
				$.each(checkboxes, function(i, checkbox){
					//is current checkbox checked
					if(checkbox.parent().hasClass('checked')){
						//current checkbox is not last column
						if(i != checkboxes.length - 1) counter++;
					}
					
					//for the current checkbox set all its column children to disabled
					if(checkbox.parent().hasClass('checked')) $(fnb.functions.checkboxHierarchySwitcher.getChildrenAsIdentifierString(checkbox)).disableCheckbox();
				});
				
				//if there are more checkboxes selected than just the last
				if(counter > 0) lastCheckbox.disableCheckbox();
				
				//update last checkbox counter data to total checked value
				lastCheckbox.data('counter', counter);
			});
        },
        
        checkboxClick: function(clickedCheckboxName){
        	var scope = this;
        	
        	//hack to handle new observer
        	//without a delay the firing order of script's gets messed up
        	//chatted with Don about a proper fix
        	setTimeout(function(){
				scope.pauseCheckboxClick(clickedCheckboxName);
			}, 50);
        },
        
        pauseCheckboxClick: function(clickedCheckboxName){
			this.clicked.obj = $(clickedCheckboxName);
			this.clicked.row = fnb.functions.rowHandler.getRowViaCheckbox(this.clicked.obj);
			this.clicked.checkboxes = fnb.functions.rowHandler.getRowCheckboxes(this.clicked.row);
			this.clicked.lastCheckbox = fnb.functions.rowHandler.getLastCheckbox(this.clicked.checkboxes);
			
			//update last checkbox counter data based on clicked checkbox state
			this.updateCounter();
			
			//update row state
			this.updateRowState();
			
			//update clicked checkbox children state
			//added function call delay due to possible firing order issues
			fnb.functions.checkboxHierarchySwitcher.checkboxClick(this.clicked.obj);
			
			//loop on last checkbox children and add/remove checked state depending on sibling states
			this.checkLastCheckboxChildren();
			
			var rows = fnb.functions.rowHandler.getRows();
			
            //depending on the situation the last checkbox can be enabled even though it should be disabled
            //this loop resets to disabled if it needs to be disabled
			$.each(rows, function(){
				var checkboxes = fnb.functions.rowHandler.getRowCheckboxes($(this));
				
				$.each(checkboxes, function(i, checkbox){
					if(checkbox.parent().hasClass('checked')) fnb.functions.checkboxHierarchySwitcher.checkboxClick(checkbox);
				});
			});
        },
        
        updateCounter: function(){
        	var counter = 0;
        	var cBoxes = this.clicked.checkboxes;

        	$.each(cBoxes, function(i, checkbox){
        		//checkbox is checked then increment counter
        		if(checkbox.parent().hasClass('checked') && i != cBoxes.length - 1) counter++;
        	});
        	
        	//store counter total in last checkbox
        	this.clicked.lastCheckbox.data('counter', counter);
        },
        
        updateRowState: function(){
        	//there are more checkboxes selected than just the last
			if(this.clicked.lastCheckbox.data('counter') > 0){
				//select last
				this.clicked.lastCheckbox.setCheckbox(true);
				//disable last
				this.clicked.lastCheckbox.disableCheckbox();
				//set last's children state
				fnb.functions.checkboxHierarchySwitcher.checkboxClick(this.clicked.lastCheckbox);
			}
			
			//no checkboxes besides the last are checked then remove possible disabled state of last
			if(this.clicked.lastCheckbox.data('counter') == 0) this.clicked.lastCheckbox.enableCheckbox();
        },
        
        checkLastCheckboxChildren: function(){
        	//get last checkbox's children
        	var children = fnb.functions.checkboxHierarchySwitcher.getChildren(this.clicked.lastCheckbox);
        	
        	$.each(children, function(a, child){
        		var row = fnb.functions.rowHandler.getRowViaCheckbox(child);
        		var siblings = fnb.functions.rowHandler.getRowCheckboxes(row);
        		
        		$.each(siblings, function(b, sibling){
        			if(sibling.parent().hasClass('checked') && b != siblings.length){
        				var lastCheckbox = fnb.functions.rowHandler.getLastCheckbox(siblings);
        				lastCheckbox.setCheckbox(true);
        				lastCheckbox.disableCheckbox();
        			}
        		});
        	});
        }
    };

    window.TableHandler = TableHandler;
})(jQuery);

$(function(){
	function genericPageObject(){
		this.tableHandler;
	}
	genericPageObject.prototype = {
		init: function(){
			this.tableHandler = new TableHandler();
			this.tableHandler.init();
		},
		destroy: function(){
			delete this.tableHandler;
			delete window.TableHandler;
			var self = this;
			self = null;
		},
		changeDropDown: function(obj){
			var ccn = $(obj).attr('data-value');
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: "/banking/Controller?nav=profiles.complex.navigator.UserCashManSettings&ccn="+ccn});
		},
		changeViewDropDown: function(obj){
			var data = $(obj).attr('data-value').split(',');
			var ccn = data[0];
			var view = data[1];
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: "/banking/Controller?nav=profiles.complex.navigator.UserCashManSettingsView&ccn="+ccn+"&view="+view});
		}
	}
	namespace("fnb.profiles.complex.userCashManSettings", genericPageObject);
});