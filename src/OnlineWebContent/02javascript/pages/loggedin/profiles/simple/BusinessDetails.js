fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSector'} ]);		
fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetails'} ]);		
fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorInputDiv'} ]);		
fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetailsInputDiv'} ]);		

function getDropdownValue(dropDownType, me, divName){
		var val=$(me).attr('data-value');
		
		if (divName == 'subSectorDropDown'){
			
			if (val == 0){
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSector'} ]);	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetails'} ]);	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorInputDiv'} ]);		
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetailsInputDiv'} ]);		
				return false;
			}
			else if (val == 'Other'){
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSector'} ]);	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetails'} ]);	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSectorInputDiv'} ]);		
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetailsInputDiv'} ]);		
				return false;
			}
			
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorInputDiv'} ]);		
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetailsInputDiv'} ]);		
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSector'} ]);	
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetails'} ]);	
			fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=settings.simple.navigator.BusinessDetailsDropdownBuilder&dropDownType='+dropDownType+'&sector='+val,divName);
		}
		
		if (divName == 'subSectorDetailsDropDown'){
			if (val == 0){
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetails'} ]);	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorInputDiv'} ]);		
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetailsInputDiv'} ]);		
				return false;
			}
			else if (val == 'Other'){
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSector'} ]);	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetails'} ]);	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorInputDiv'} ]);		
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSectorDetailsInputDiv'} ]);	
				return false;
			}
			
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSectorDetails'} ]);	
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorInputDiv'} ]);		
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetailsInputDiv'} ]);		
			fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=settings.simple.navigator.BusinessDetailsDropdownBuilder&dropDownType='+dropDownType+'&sector='+val,divName);
		}
		
	}

function getPaypalRegDropdownValue(dropDownType, me, divName){
	var val=$(me).attr('data-value');

	if (divName == 'sector'){
		
		if (val == 'Please Select'){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#sectorDiv'} ]);
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSector'} ]);
			return false;
		}
		
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#sectorDiv'} ]);			
		fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=settings.simple.navigator.BusinessDetailsDropdownBuilder&dropDownType='+dropDownType+'&industry='+val,divName);
	}
	
	if (divName == 'subSectorDropDown'){
		
		if (val == 'Please Select'){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSector'} ]);		
			return false;
		}
			
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSector'} ]);	
		fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=settings.simple.navigator.BusinessDetailsDropdownBuilder&dropDownType='+dropDownType+'&sector='+val,divName);
	}
	
}

function showInput(me){
	var val=$(me).attr('data-value');
	if (val == 'Other'){
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetails'} ]);		
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSectorDetailsInputDiv'} ]);	
		return false;
	}
	else{
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetails'} ]);		
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetailsInputDiv'} ]);
	}
	return false;
	
}

function showDetailsInput(me){
	var val=$(me).attr('data-value');
	if (val == 'Other'){
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#subSectorDetails'} ]);		
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#subSectorDetailsInputDiv'} ]);	
		return false;
	}
	return false;
	
}

