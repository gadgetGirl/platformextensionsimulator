	function validateIncludeData() {
		var cardNumber = document.getElementById("cardNumber").value;
		var cardNumLength = cardNumber.length;
		
		if ((cardNumLength > 16) || (cardNumLength < 16) || (cardNumLength == 0) || (!isNumber(cardNumber))) {
			alert("Validation error - Please enter the 16 digit card number as it appears on the card without spaces.");
			return false;
		}
		
		if (!isNumber(document.getElementById("pin").value)) {
			alert("Validation error - Please enter a valid 4 digit PIN.");
			return false;
		} else if (document.getElementById("pin").value.length > 4 || document.getElementById("pin").value.length < 4) {
			alert("Validation error - Please enter a valid 4 digit PIN.");
			return false;
		}
		return true;
	}
	
	function executeGetTicket(requestForm) {
		var cardNumber = document.getElementById("cardNumber").value;
		var cardNumLength = cardNumber.length;

		var url = "/Controller?nav=cardpin.crypto.navigator.CardPinCryptoGetTicketNoOTP&cardNumber="+cardNumber+"&countryID="+${countryID};
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		if (xmlhttp) {
			xmlhttp.open("GET", url, true); // true for async, false for sync
		    xmlhttp.onreadystatechange=function() {     
		        if (xmlhttp.readyState==4) {
		            if (xmlhttp.status == 200) {
		                // pass the response to the callback function
		                sendCardAndPinForValidation(xmlhttp.responseText);
		            } else {
		                // pass the error to the callback function
		                sendCardAndPinForValidation(xmlhttp.responseText);
		            }
		        }
		    }
		    xmlhttp.send(null);
		    return false;
		} else {
			return false;
		}
	}

	function sendCardAndPinForValidation(ajaxResponse) {
		
		if (ajaxResponse.length != 64) {
			alert(ajaxResponse);
			return false;
		} else {
			document.getElementById("tick").value = ajaxResponse;
		}
		
		var p = document.getElementById("pin").value;
		var card = document.getElementById("cardNumber").value;
		var tic = ajaxResponse;
		if (tic.length === 64) {
			var cryptoURI = "${cryptoURL}";
			var url = "${cryptoURL}/authenticatepin?ticket=" + tic + "&cardno=" + card.substring(3, 15) + "&pin=" + p;
			document.getElementById("res").src = url;
			document.getElementById("pin").value = "";
			document.getElementById("cardNumber").value = "";
			window.setTimeout("doSubmit()", 6500);
			return false;
		}
	}
	
	function doSubmit() {
		fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: "/Controller?nav=settings.simple.navigator.DailyProfileLimitsResult"})
	}
	
	function validate() {
		if (validateIncludeData()){
			return executeGetTicket("DAILY_PROFILE_LIMITS_CONFIRM");
		}
		else{
			return false;
		}
	}