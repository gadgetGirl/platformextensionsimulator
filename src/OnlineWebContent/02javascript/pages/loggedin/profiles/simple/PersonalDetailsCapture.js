fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#maritalContractDiv'} ]);		
fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#residentialDiv'} ]);	
fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#employDiv'} ]);	
fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#educationDiv'} ]);	

	function hideMaritalContract(me){
		var val=$(me).attr('data-value');
	
		if (val == 'M' || val == 'S'){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#maritalContract_parent'} ]);	
		}
		else{
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#maritalContract_parent'} ]);	
		}
		
	}
	
	function hideResidential(me){
		var val=$(me).attr('data-value');
		if (val == 'O'){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#residentialDiv'} ]);	
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#residentialDivToolTip'} ]);
		}
		else{
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#residentialDiv'} ]);	
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#residentialDivToolTip'} ]);	
		}
		
	}
	
	function hideEmploy(me){
		var val=$(me).attr('data-value');
		if (val == 'ZZ' || val == 'PB'){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#employDiv'} ]);	
		}
		else{
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#employDiv'} ]);	
		}
		
	}
	
	function hideEducation(me){
		var val=$(me).attr('data-value');
		if (val == 'G10' || val == 'G12' || val == 'L10'){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#educationDiv'} ]);	
		}
		else{
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#educationDiv'} ]);	
		}
		
	}