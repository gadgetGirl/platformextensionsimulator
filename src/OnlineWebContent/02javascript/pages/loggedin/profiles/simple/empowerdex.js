$(function() {
	function genericPageObject() {
		
	}
	genericPageObject.prototype = {
			
			shareholders : function(me) {
				shareholdersNumber = parseInt($(me).val());
				var shareholderAmmount = $("#shareholders").children();
				if(shareholderAmmount.length != shareholdersNumber){
					setTimeout(function(){
						if(shareholdersNumber <= 10 && shareholdersNumber > 0) {
							fnb.hyperion.$('#addShareHolder').show();
						} else {
							fnb.hyperion.$('#addShareHolder').hide();
							
						}
					}, 1000);
				} else {
					fnb.hyperion.$('#addShareHolder').hide();
				}
			},
			// when clicking edit from confirm page, clicking add shareholder from ezi, toggling renew and register option-> ensures that fields that were disabled or expanded etc remain that way when we return to pafe
			toggleMenu : function() {
				pages.fnb.simple.Empowerdex.shareholders(document.getElementById("numberOfShareholders"));
				
				//we use temp variable so we dont interview with viewbean values
				var formType_tmp =  document.getElementById("formType_tmp").value;
				var companyTurnover_tmp = document.getElementById("companyTurnover_tmp").value;
				var directorsChange_tmp = document.getElementById("directorsChange_tmp").value;

				
				if (formType_tmp != null && formType_tmp == 'Renew'){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#isRenew'}]);
					
					if (companyTurnover_tmp == 'Yes'){
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#annualTurnoverDetiails_parent'}]);	
					}else{
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#annualTurnoverDetiails_parent'}]);	
					}
					
					if (directorsChange_tmp == 'Yes'){
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#shareholderContainer'}]);										
					}else{
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#shareholderContainer'}]);										
					}
					
				}
					
				if (formType_tmp != null && formType_tmp == 'Register'){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#isRenew'}]);		
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#annualTurnoverDetiails_parent'}]);		
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#shareholders'}]);		
				}
				
				pages.fnb.simple.Empowerdex.setFormType(formType_tmp);
				pages.fnb.simple.Empowerdex.setCompanyTurnover(companyTurnover_tmp);
				pages.fnb.simple.Empowerdex.setDirectorsChangeYN(directorsChange_tmp);
			},
			// landing does not submit page data to shareholder so we need to explicity make calls to set values of fields. works with returnUrl and toggleMenu
			setFormType : function(myValue) {
				document.getElementById("formType").value = myValue;
			},
			setDirectorsChangeYN : function(myValue) {
				document.getElementById("directorsChange").value = myValue;
			},
			setCompanyTurnover : function(myValue) {
				document.getElementById("companyTurnover").value = myValue;
			},
			//Preserves data captured on the landing page before we load the addshsareholder page. landing does not submit page data to addshareholder ezi
			returnUrl : function(cType) {
				
				var urls = "";
				
				if (cType == "zob"){
					urls = '/banking/Controller?nav=profiles.simple.navigator.AddShareholder' + pages.fnb.simple.Empowerdex.returnUrlData();
				}else{
					urls = '/banking/Controller?nav=profiles.complex.navigator.AddShareholder' + pages.fnb.simple.Empowerdex.returnUrlData();	
				}
				return fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: urls});
			},
			// works in conjuction with returnUrl above
			returnUrlData : function() {
				var urlData = '&contactPersonName=' + document.getElementById("contactPersonName").value;
				urlData += '&contactPersonSurname=' + document.getElementById("contactPersonSurname").value;
				urlData += '&formType=' + document.getElementById("formType").value;
				urlData += '&cellphoneNumber=' + document.getElementById("cellphoneNumber").value;
				urlData += '&workTelephoneNumber=' + document.getElementById("workTelephoneNumber").value;
				urlData += '&alternativeNumber=' + document.getElementById("alternativeNumber").value;
				urlData += '&emailAddress=' + document.getElementById("emailAddress").value;
				urlData += '&companyTurnover=' + document.getElementById("companyTurnover").value;
				urlData += '&directorsChange=' + document.getElementById("directorsChange").value;
				urlData += '&annualTurnoverDetiails=' + document.getElementById("annualTurnoverDetiails").value;
				urlData += '&numberOfShareholders=' + document.getElementById("numberOfShareholders").value;
				urlData += '&index=&action='
				return urlData;
			},
			// to automatically show the fields for the preselected shareholder type
			toggleShareholderMenu : function() {
				var shareholderType =  document.getElementById("shareHolder_tmp").value;
				
				if (shareholderType == 'Individual'){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#individualContainer'},{show:'false',element:'#businessContainer'}]);
				}
				
				if (shareholderType == 'Business'){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#individualContainer'},{show:'true',element:'#businessContainer'}]);
				}
			}
	
	};

	namespace("fnb.profiles.simple.Empowerdex",genericPageObject);
});
