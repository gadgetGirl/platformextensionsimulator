$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.reloadPage();
						
		}, 
		
		reloadPage : function(){
			var parent = this;
			var isAuthorised = parent.configObject.authorised;
			var waitTime = parent.configObject.waitTime;
			var url = parent.configObject.pageUrl;
			
			if(isAuthorised == 'true'){
				window.setTimeout(function(){
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:'/banking/Controller?nav=fnbconnect.tabletapp.navigator.TabletAppAuthResults',target:'#'});
				}, waitTime);
				window.setTimeout(function(){
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:'/banking/Controller?nav=fnbconnect.tabletapp.navigator.TabletAppAuthResults',target:'#'});
				}, waitTime);
				window.setTimeout(function(){
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:'/banking/Controller?nav=fnbconnect.tabletapp.navigator.TabletAppAuthResults',target:'#'});
				    isAuthorised='true';
				}, waitTime);
				window.setTimeout(function(){
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:'/banking/Controller?nav=fnbconnect.tabletapp.navigator.TabletAppAuthResults',target:'#'});
				}, waitTime);
				window.setTimeout(function(){
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:'/banking/Controller?nav=fnbconnect.tabletapp.navigator.TabletAppAuthResults',target:'#'});
				    isAuthorised='true';
				}, waitTime);
			}
		}
	}
	namespace("fnb.fnbconnect.TabletAppAuthResults",genericPageObject); 	
});