$(function(){
	function genericPageObject(){
		this.graphUtils_isAnimating = false;
	}
	genericPageObject.prototype = {
		init: function(){
			$("#chequeGraphs").css("display","none");
			setTimeout(function() {
				$.each($("#groupedGraph1 .graphs.right"), function() {
					pages.fnb.complexreports.CashAndChequeLanding.goTo(this, "right");
				}); 
			}, 500);
		},
		goTo: function(me, direction){
			$("#tooltip").remove();
			var parent = $(me).closest(".barGraphWrapper");
			
			var graphs = parent.find(".graphs");
			var graph = parent.find(".graphs .barGraphHolder");
			var leftArrow = parent.find(".leftArrow");
			var rightArrow = parent.find(".rightArrow");
			
			var shift = $(graph)[0].offsetWidth;
			var rightStop = ($(parent)[0].offsetWidth - (shift * graph.length));
			var move = 0;
			
			if (direction == "left") {
				if ($(graphs)[0].offsetLeft != 0) {
					move = 0;
					this.shiftGraph(graphs, move, 0);
				}
				$(leftArrow).addClass("disabled");
				$(rightArrow).removeClass("disabled");
			}
			else if (direction == "right") {
				if ($(graphs)[0].offsetLeft > rightStop) {
					move = 0 - (shift * (graph.length - 3));
					this.shiftGraph(graphs, move, 0);
				}
				$(leftArrow).removeClass("disabled");
				$(rightArrow).addClass("disabled");
			}

		},
		move: function(me, direction){
			if($(me).hasClass("disabled")) {return false;}
			
			$("#tooltip").remove();
			var parent = $(me).closest(".barGraphWrapper");
			
			var graphs = parent.find(".graphs");
			var graph = parent.find(".graphs .barGraphHolder");
			var leftArrow = parent.find(".leftArrow");
			var rightArrow = parent.find(".rightArrow");
			
			var shift = $(graph)[0].offsetWidth;
			var rightStop = ((shift * graph.length) - $(parent)[0].offsetWidth) * -1;
			var move = 0;
			if (direction == "left") {
				if ($(graphs)[0].offsetLeft != 0) {
					move = $(graphs)[0].offsetLeft + shift;
					this.shiftGraph(graphs, move, 0.25);
				}
			}
			else if (direction == "right") {
				if ($(graphs)[0].offsetLeft > rightStop) {
					move = $(graphs)[0].offsetLeft - shift;
					this.shiftGraph(graphs, move, 0.25);
				}
			}

			if (move == 0) {
				$(leftArrow).addClass("disabled");
			}
			else {
				$(leftArrow).removeClass("disabled");
			}
			if (move < rightStop) {
				$(rightArrow).addClass("disabled");
			}
			else {
				$(rightArrow).removeClass("disabled");
			}
		},
		shiftGraph: function(graphs, move, speed){
			if (this.graphUtils_isAnimating == false||typeof this.graphUtils_isAnimating == 'undefined') {
				this.animatingOn();
				TweenMax.to($(graphs),speed,{css:{left:move},onComplete:pages.fnb.complexreports.CashAndChequeLanding.animatingOff});
			}
		},
		animatingOn: function(){
			pages.fnb.complexreports.CashAndChequeLanding.graphUtils_isAnimating = true;
		},
		animatingOff: function(){
			pages.fnb.complexreports.CashAndChequeLanding.graphUtils_isAnimating = false;
		},
		showTooltip: function(x, y, contents){
			 $('<div id="tooltip">' + contents + '</div>').css( {
		            top: y - 25,
		            left: x - 150
		        }).appendTo("body").show();
		},
		setSelected: function(me){
			var switcher = $(me).parent(".switcher");
			$(switcher).find(".switch").removeClass("active");
			$(me).toggleClass("active");
			return false;
		},
		showCashGraphs: function(){
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#cashGraphs'}, {show : 'false', element : '#chequeGraphs'}])
			$.each($("#groupedGraph1 .graphs.right"), function() {
				pages.fnb.complexreports.CashAndChequeLanding.goTo(this, "right");
			});
		},
		showChequesGraphs: function(){
			$('#chequeGraphs').css('left','0px');
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#cashGraphs'}, {show : 'true', element : '#chequeGraphs'}]);
			$.each($("#groupedGraph2 .graphs.right"), function() {
				pages.fnb.complexreports.CashAndChequeLanding.goTo(this, "right");
			});
		}
	}
	
	namespace("fnb.payments.complexreports.CashAndChequeLanding",genericPageObject);
});