$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(){	

		},
		returnValues: function(selectedDescription,selectedRefNum,selectedCountryCode) {
			$("#publicRecipientSearch").val(selectedDescription);
			$("#publicRecipientSearchRefNum").attr('value', selectedRefNum);
			$("#publicRecipientSearchCountryCode").attr('value', selectedCountryCode);
			fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '');
		}
	}

	namespace("fnb.publicrecipientsearch.simple.PublicRecipientSearchFinish",genericPageObject);
});