$(function(){
	function genericPageObject(){
 
	}
	genericPageObject.prototype = {
		init: function(datasource){	
			var parent = this;
			parent.acceptEnabled = true;
			this.hideContent();
			//Adjust footer buttons
			fnb.hyperion.utils.footer.configFooterButtons();
		},
		loadApplet: function(url){	
			var parent = this;
			this.hideContent();
			if(url != undefined){
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#hiddenAppletDiv"});
			}
		},
		authorise : function(){
			var parent = this;
			$("#action").attr("value", "Authorise");
			fnb.functions.submitFormToWorkspace.submit("AuthorisationsLanding");
		},
		displayContent : function(args){
			var parent = this;
			$("#certificatePath").attr("value", args[0]);
			if(args[1] == 'true' && $("#rememberPath").attr("checked") != 'checked'){
				$("#rememberPath").trigger("click");
				 $("#rememberPath").attr("checked","checked"); 
			}
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#appletDiv'}])
			var changePin = ($("#changePin").attr("checked") == 'checked')?'true':'false';
			fnb.functions.showHideToggleElements.showHideToggle([{show:changePin,element:'#appletDivChangePin'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDivLoading'}])
			$('.appletButtons').removeClass('displayNone')
			fnb.controls.controller.showFooterButtons();
			parent.acceptEnabled = true;
		},
		changePin : function(){
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#appletDiv'}])
			var changePin = ($("#changePin").attr("checked") == 'checked')?'true':'false';
			fnb.functions.showHideToggleElements.showHideToggle([{show:changePin,element:'#appletDivChangePin'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDivLoading'}])
			fnb.controls.controller.showFooterButtons();
		},
		hideContent : function(args){
			var parent = this;
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDiv'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDivChangePin'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#appletDivLoading'}])
			$('.appletButtons').addClass('displayNone')
			fnb.controls.controller.clearFooterButtons();
		},
		browse : function(){
			var parent = this;
			this.sendMessage(["Browse"]);
		},
		accept : function(isHardCert){
			
			var parent = this;
			if(parent.acceptEnabled){
				
				parent.acceptEnabled = false;
				
				var certLogin = $("#certLogin").attr("value");
				var disclaimer = ($("#disclaimer").attr("checked") == 'checked');
				
				
				if(isHardCert == 'true'){
					var certificatePIN = $("#certificatePIN").attr("value");
					var newPIN = $("#newPIN").attr("value");
					var confirmPIN = $("#confirmPIN").attr("value");
					var changePin = ($("#changePin").attr("checked") == 'checked');
					
					this.sendMessage(["Accept", certificatePIN,newPIN, confirmPIN,changePin,disclaimer,certLogin ]);
				}else{
					var password = $("#certificatePassword").attr("value");
					var path = $("#certificatePath").attr("value");
					var rememberPath = ($("#rememberPath").attr("checked") == 'checked');
					
					this.sendMessage(["Accept", path,rememberPath, password,disclaimer,certLogin ]);
			}
			}
		},
		
		cancel : function(){
			var parent = this;
			this.sendMessage(["Cancel"]);
		},
		reject : function(){
			var parent = this;
			$("#action").attr("value", "Reject");
			fnb.functions.submitFormToWorkspace.submit("AuthorisationsLanding");
		},
		sendMessage : function(args){
			var parent = this;
			try{
				$('#chameleonApplet')[0].sendMessage(args);
			}catch(e){
				document.applets[0].sendMessage(args);
			}
		}
		
	};
	namespace("fnb.authorisations.AuthorisationList",genericPageObject);
});