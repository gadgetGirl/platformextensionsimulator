$(function(){

	function genericPageObject(){
	}

	genericPageObject.prototype = {
			init: function(config){
				var me = this;
				me.config = config;
				me.start();
				return me;
			}
			,destroy: function(){
				var me = this;
			}
			,start: function(){
				var me = this;
				
			}
			,deleteSelected: function(){
				
				var tableContainer = $('.tableList');
				var boxes = tableContainer.find($("input[id^='ANRFN']"));
				var kbrfns = new Array();
				var NumItemsToDelete = 0;
				  
				  $(boxes).each(function(){
					   if($(this).attr('checked')){
							kbrfns.push($(this).val());
							NumItemsToDelete++;
						}
				   })
				   
				   if(NumItemsToDelete > 0)
					   {fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '/banking/Controller?nav=scheduledexports.navigator.ScheduledExportsDeleteWarning&anrfn='+kbrfns);}
				   else
					   {fnb.controls.controller.eventsObject.raiseEvent('loadError', {height:'134px',message: 'No items have been selected for deletion', errors:[]});}
				   
				    
					
				
			}
	}

	namespace("fnb.scheduledexports.landing",genericPageObject);

});