$(function() {
	function genericPageObject() {

	}

	genericPageObject.prototype = {
			init: function(config){
				var me = this;
				me.config = config;
				me.start();
				return me;
			}
			,destroy: function(){
				var me = this;
			}
			,start: function(){
				var me = this;
				
			}
			,checkClick : function() {
				
				if($('#emailAddress').attr('disabled') == 'disabled'){
					$('#emailAddress').prop('disabled', false);
					$('#emailSubject').prop('disabled', false);
					$('#email').prop('value', 'on');
				} else {
					$('#emailAddress').prop('disabled', true);
					$('#emailSubject').prop('disabled', true);
					$('#email').prop('value', '');
				}
				
				
			}
	}
	
	namespace("fnb.scheduledexports.capture",genericPageObject);
	
});


