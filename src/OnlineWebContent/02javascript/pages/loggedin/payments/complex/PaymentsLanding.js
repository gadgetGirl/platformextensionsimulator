(function($){
	
//	try{
//		// try catch is here since the hierarchy may not exist yet
//		if(pages.fnb.payments.complex.PaymentsLanding){
//			// if we've already defined this object, return
//			return;
//		}	
//	}catch(e){
//		
//	}
		
	function genericPageObject(){
		var me = this;
		me.buttonToNavMap = {
				"Authorise":"payments.complex.navigator.PaymentBatchRequestAuth"
				, "Delete":"transfers.complex.navigator.TransfersDeleteResults"
					}
	}
		
	genericPageObject.prototype = {
			
			init: function(config){
				
				var me = this;
				me.config = config;
				me.start();
				return me;
				
			}
			, destroy: function(){
				
				var me = this;
				
			}
			, start: function(){
				var me = this;
				
			}
			, deletePayments : function(){
								    
					    var rfns = new Array();

					    var items = $('.col6').find('.checkbox-graphic-wrapper');
					    $.each(items, function(index, item) {
					    	if($(this).hasClass('checked')){
					    		rfns.push($(this).find('input').attr('value'));
					    	}
						});
					    
					    if(rfns.length == 0){
						    var msg = "Please select one or more batches to delete";
						    parentObject.showError(msg);						    
						  	
					    	return false;
					    }
					    
					    var rfnsUrl="";
					    
					    for(i=0;i<rfns.length;i++){
					    	
					    	rfnsUrl = rfnsUrl + "&TBRFN"+i+"="+rfns[i];
					    }
					    
					    fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '/banking/Controller?nav=payments.complex.PaymentBatchDeleteConfirm'+rfnsUrl)

			}
			, showError: function(errorMessage){
				fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
			}
			
			
	}
	
	
	namespace("pages.fnb.payments.complex.PaymentsLanding",new genericPageObject());
	
	
	
	
})(jQuery);
