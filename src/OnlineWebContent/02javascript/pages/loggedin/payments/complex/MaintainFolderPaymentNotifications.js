$(function() {

	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
			return parent;

		}

		,pageLoaded : function() {
			var parent = this;
			
			// set form submission target
			//var theForm = $("form[name='MAINTAIN_PAYMENT_NOTIFICATIONS']");
			//theForm.attr("target",theForm.closest(".expandableTableRow"));

		}
	};

});