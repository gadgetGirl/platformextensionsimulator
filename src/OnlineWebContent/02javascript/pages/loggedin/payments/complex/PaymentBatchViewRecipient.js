$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){
			var parent = this;
		    parent.configObject = dataSource;
		},
		destroy: function(){
			var parent = this;
			parent = null;
		},
		goNext: function(){
			var parent = this;
			parent.configObject.itemIndex++;
			parent.configObject.itemRFN++;
			parent.configObject.templateItemRFN++;
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url:'/banking/Controller?nav=payments.complex.navigator.PaymentBatchViewRecipient&itemIndex='+parent.configObject.itemIndex+'&itemRFN='+parent.configObject.itemRFN+'&templateItemRFN='+parent.configObject.templateItemRFN+'&batchRFN='+parent.configObject.batchRFN});
		},
		goPrev: function(){
			var parent = this;
			parent.configObject.itemIndex--;
			parent.configObject.itemRFN--;
			parent.configObject.templateItemRFN--;
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url:'/banking/Controller?nav=payments.complex.navigator.PaymentBatchViewRecipient&itemIndex='+parent.configObject.itemIndex+'&itemRFN='+parent.configObject.itemRFN+'&templateItemRFN='+parent.configObject.templateItemRFN+'&batchRFN='+parent.configObject.batchRFN});
		}
	}
	
	namespace("fnb.payments.complex.PaymentBatchViewRecipient",genericPageObject);
});