$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
			return parent;
		},
		
		pageLoaded : function() {
			var parent = this;
			
			parent.hideOrShowFields();
		}
		
		,folderChanged: function(aDropDown){
			var parent = this;
			var selectedFolder = $(aDropDown).attr("data-value"); 
			var defaults = parent.configObject.accountDefaults;
			for(index in defaults){
				if(defaults[index]["templateReference"] == selectedFolder){
					var accountDropDown = $("#account_parent").find('li[data-value="'+defaults[index]["defaultAccount"]+'"]');
					var accountDropDownText = accountDropDown.text();
					fnb.forms.dropdown.setValue($(accountDropDown),accountDropDownText,defaults[index]["defaultAccount"]);
					
					if(defaults[index]["overrideFromAccountAllowed"]=="false"){
						fnb.forms.dropdown.disable($(accountDropDown));
					}else{
						fnb.forms.dropdown.enable($(accountDropDown));
					}
					
					if(defaults[index]["isConsolidated"]=="false"){
//						var radioButton = $("#radioitemised");
						var radioButton = $("#consolidated").find(".radioButton").last();
						radioButton.click();
						$("#headerFromRef").val("");
					}else{
						//var radioButton = $("#radioConsolidated");
						var radioButton = $("#consolidated").find(".radioButton").first();
						radioButton.click();
						$("#headerFromRef").val(defaults[index]["fromReference"]);
					}
					
					break;
				}
			}
			
		}
		
		,checkService: function (n) {
			
			var parent = this;

			if (n == "4") {
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'true',
					element : '#recurringPayment'
				} ]);
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'false',
					element : '#payAndClearNow'
				} ]);
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'false',
					element : '#execDate'
				} ]);

			} else if (n == "3") {
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'true',
					element : '#payAndClearNow'
				} ]);
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'false',
					element : '#recurringPayment'
				} ]);
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'true',
					element : '#execDate'
				} ]);

			} else if (n == "1") {

				fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '/banking/Controller?nav=payments.complex.FolderNamePopup');
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'false',
					element : '#payAndClearNow'
				} ]);
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'false',
					element : '#recurringPayment'
				} ]);
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'true',
					element : '#execDate'
				} ]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'false',
					element : '#payAndClearNow'
				} ]);
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'false',
					element : '#recurringPayment'
				} ]);
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'true',
					element : '#execDate'
				} ]);
			}
		}
		
		,hideOrShowFields : function(){
			var parent = this;

			var consolidatedVal = $("input#consolidated").val();
			if(consolidatedVal == "1")
			{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#consolFee'}]);
			}
		}

	}

	namespace("fnb.payments.complex.PaymentCapture",genericPageObject);
});


