$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
			return parent;
		},
		
		pageLoaded : function() {
			var parent = this;
			
			parent.hideOrShowFields();
		}
		
		,hideOrShowFields : function(){
			var parent = this;

			var consolidatedVal = $("input#consolidated").val();
			
			if(consolidatedVal == "2")
			{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#consolFee'}]);
			}
		}

	}

	namespace("fnb.payments.complex.OnceOffPaymentCapture",genericPageObject);
});


