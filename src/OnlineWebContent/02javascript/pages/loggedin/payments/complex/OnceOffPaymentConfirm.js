$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
			return parent;
		},
		
		pageLoaded : function() {
			var parent = this;
			if(parent.configObject["recipientType"]=="publicRecipient"){
				$("#publicRecipient").click();
			}else if(parent.configObject["recipientType"]=="notGlobal"){
				$("#notGlobal").click();
			}else if(parent.configObject["recipientType"]=="eWallet"){
				$("#eWallet").click();
			}
			
		},
		
		submitUrlWithMultipleParams: function(url,keyValuePairsObject){
			
			var href = url
			var params =[]
			var paramString = ''			
			for (var key in keyValuePairsObject) {
				inputValue = $(keyValuePairsObject[key]).attr('value')==''? null :  $(keyValuePairsObject[key]).attr('value') 
				params.push(key+'='+inputValue);
			}
			if(params.length > 0){
				paramString=params.toString().replace(',','&');
			}else{
				paramString = '';
			}
		
			fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow',{url:href,params:paramString});
		}
	};
	namespace("fnb.payments.complex.OnceOffPaymentConfirm",genericPageObject);
});


