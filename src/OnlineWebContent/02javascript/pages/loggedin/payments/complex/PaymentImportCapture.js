$(function() {
	function genericPageObject() {
		this.configObject = {};
		var once = 0;
	}
	genericPageObject.prototype = {

			init : function(dataSource) {

				var parent = this;
				once = 0;
				parent.configObject = dataSource;
				/*
				 * parent.configObject.defaults is an array of objects with default values for the folders
				 * it looks something like:
				 * {
				 * "templateReference":"nnnnn"
				 * ,"defaultAccount":"nnnnn"
				 * ,"isConsolidated":"true"
				 * ,"fromReference":"Example Reference"
				 * ,"overrideFromAccountAllowed":"true"
				 * }
				 * 
				 */
				parent.pageLoaded();
				return parent;

			}

	, pageLoaded : function() {

		var parent = this;
		once = 0;

	}

	,folderChanged: function(dropDownItem){

		var parent = this;
		var folderReference = $(dropDownItem).attr("data-value");
		var folderName = $(dropDownItem).text().trim();
		console.log("folderChanged fired: " + folderReference);
		console.log("folderChanged fired: " + folderName);
		var checkBoxOBJ = $("#onceOff");

		if (once%2 != 0){
			checkBoxOBJ.trigger('click');
		}

		for(index in parent.configObject.defaults){
			var templateDefault = parent.configObject.defaults[index];
			if(templateDefault["templateReference"]==folderReference){
				console.log("match found: " + templateDefault["fromReference"])
				console.log("match found: " + templateDefault["defaultAccount"])

				$("#headerFromRef").val(templateDefault["fromReference"].trim());//remove spaces
				$("#accountRfn").val(templateDefault["defaultAccount"]);
				$("#folderName").val(folderName);
				$("#type").val("TB"); // Folder payment
				break;
			}else{
				$("#type").val("BA"); // Once-off payment
			}
		}



		// SCRITP NEEDS TO BE REWORKED TO WORK WITH 9+ ELEMENSTS

		//alert($("#folder").val());
		//ui.inputbox.setValue("#folderName",$("#folder").val());

		//getFormElementById(formName,"folderName").value = ;
		//if (getFormElementById(formName,"onceOff") != null){
		//getFormElementById(formName,"onceOff").checked = false;
		//ui.checkbox.setDeselected('#onceOff')
		//}
		//ui.inputbox.setValue("#type","TB");

		//document.getElementById("type").value="TB";
//		var gotOne = false;
//		for (var k=0; k<folders.length; k++){
//		if ($("#folder").val() == folders[k]){
//		ui.inputbox.setValue("#headerFromRef",fromRef[k]);
//		ui.inputbox.setValue("#accountRfn",accounts[k]);
//		gotOne = true;
//		break;
//		}
//		}
//		if (!gotOne){
//		ui.inputbox.setValue("#headerFromRef","");

//		//getFormElementById(formName,"headerFromRef").value = "";
//		}
		//salert(document.getElementById("type").value);
	}

	,checkService: function(service) {
		if ($(service).attr("data-value") == 1) {

			fnb.controls.controller.eventsObject
			.raiseEvent(
					'eziSliderShow',
					{
						url : '/banking/Controller?nav=payments.complex.FolderNameEziSlide'
					});
			fnb.functions.showHideToggleElements.showHideToggle([ {
				show : 'false',
				element : '#payAndClearNow'
			} ]);
		}else if ($(service).attr("data-value") == 3) {
			fnb.functions.showHideToggleElements.showHideToggle([ {
				show : 'true',
				element : '#payAndClearNow'
			} ]);
		}
		else {
			fnb.functions.showHideToggleElements.showHideToggle([ {
				show : 'false',
				element : '#payAndClearNow'
			} ]);
		}
	}

	,onceOffChecked: function(checkBoxItem){
		var me = $(checkBoxItem);

		if (once==0){
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#onceOffContainer'}]);
		}
		else if (once%2 == 0){
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#onceOffContainer'}]);
		}
		else{
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#onceOffContainer'}]);
		}

		once++;
	}

	}

	namespace("fnb.payments.complex.PaymentImportCapture",genericPageObject);
});


