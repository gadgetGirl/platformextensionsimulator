$(function() {
	function genericPageObject() {

	}
	genericPageObject.prototype = {
			
		clearFormVerifyLater: function(){
			for(i=0; i<5; i++){ // clear all duplication widget fields
				$('#accountNumber'+i).val('');
				$('#branchCode'+i).val('');
				$('#idRegNumber'+i).val('');
				$('#initials'+i).val('');
				$('#surnameBusName'+i).val('');
			}
			// We clear all these variables irrespective
			$('#deliveryEmail').val('');
			$('#notificationEmail').val('');
			$('#notificationCellNumber').val('');
		},
		clearFormVerifyNow: function(){
				$('#accountNumber').val('');
				$('#branchCode').val('');
				$('#idRegNumber').val('');
				$('#initials').val('');
				$('#surnameBusName').val('');
		}
	};

	namespace("pages.fnb.payments.simple.verifyaccountowner.VerifyAccountOwner",genericPageObject);
});