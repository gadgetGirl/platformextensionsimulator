$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {
		loadCategories : function(selection) {
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:"/banking/Controller?nav=navigator.payments.simple.PaymentHistorySearchDropdownType&type="+$(selection).attr('data-value'), target:"#categoryAndType"});
		},
	
		loadRecipients : function(selection) {
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:"/banking/Controller?nav=navigator.payments.simple.RecipientCategoryFilter&recipientCategory="+$(selection).attr('data-value'), target:"#recipientContainer"});
		}
	}

	namespace("fnb.payments.simple.PaymentHistorySearch",genericPageObject);
});


