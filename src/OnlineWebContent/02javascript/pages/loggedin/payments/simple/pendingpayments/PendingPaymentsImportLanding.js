function clearEmails(){
	document.getElementById('primaryEmail').value = '';
	document.getElementById('CCaddress1').value = '';
	document.getElementById('CCaddress2').value = '';
}

function validateform(){
	var path = document.getElementById('theFile').value;
	var msg = "";
	var err = 0;
	
	var rt = $(".radioGroupValue").attr("value"); 
	if(rt == ""){
		msg += "Please select a result destination <br>"
		err++;
	}
	
	if(rt == "0"){
		if(document.getElementById('primaryEmail').value == ""){
			msg += "Please enter a primary email address <br>";
			err++;
		}
	}	
	if(document.getElementById('fileTypeReq').value == '0'){
		msg += "Please select a format <br>";
		err++;
	}
	
	if(document.getElementById('name').value == ''){
		msg += "Please enter a name <br>"
		err++;
	}
	if(document.getElementById('fileType').value == '0'){
		msg += "Please select a file type <br>";
		err++;
	}
	if (path == ""){
		msg +="Please enter a file Path <br>";
		err++;
	} 
	
	if(err > 0){
		window.setTimeout('showError("' + msg + '");',50); 				
		return false;
	}
	
	fnb.functions.submitFormToWorkspace.submit("paymentImport");
}

function showError(errorMessage){
	fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
}