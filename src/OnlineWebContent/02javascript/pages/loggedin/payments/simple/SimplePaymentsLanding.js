$(function(){
	function genericPageObject(){

	}
	genericPageObject.prototype = {
		init: function(observer,target){	
			//this.obeserver.init(observer,target)
		},
		checkDateAndSubmitForm: function(checkHoliday) {
			_datePicker.checkFooterDatePicker('/banking/Controller?nav=navigator.payments.simple.HolidayCalenderPopupPaymentsLanding', 'payments_landing', $('#mainBtnHref'), checkHoliday);
		}
	
	};
	genericPageObject.prototype.obeserver = {
		init: function(observer,target){
			var _this = this;
			$(observer).on('click touchend', target, function(event) {_this.cellSelected($(event.target))});
			$(observer).on('blur', target, function(event) {_this.cellBlur($(event.target))});
		},
		cellSelected: function(cell) {
			cell.closest('.tableRow').find('.browserHidden').each(function(cellIndex, cellItem) {
				$(this).addClass('browserDisplay');
			});
			var currentHeaderGroup = cell.closest('.tableCell').attr('data-value');
			$('#'+currentHeaderGroup).find('.browserHidden').each(function(cellIndex, cellItem) {
				$(this).addClass('browserDisplay');
			});
		},
		cellBlur: function(cell) {
			if($(cell).val()==""||$(cell).val()=="0.00"){
				cell.closest('.tableRow').find('.browserHidden').each(function(cellIndex, cellItem) {
					$(this).removeClass('browserDisplay');
				});
			}
		},
		formObserver: function(observer) {
		
		}
	};
	namespace("fnb.payments.simple.SimplePaymentsLanding",genericPageObject);
});