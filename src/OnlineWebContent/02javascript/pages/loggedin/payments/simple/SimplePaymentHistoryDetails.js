$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			this.configObject = dataSource;
			this.pageLoaded();

		},
		
		pageLoaded : function() {
			if (this.configObject.printNow == "true") {
				window.print();
				return false;
			}
		}

	}

	namespace("fnb.payments.simple.SimplePaymentHistoryDetails",genericPageObject);
});


