function validate() {
		if (document.getElementById('expressClearing').value == "") {

			ui.errorConsole
					.populateAndShow(
							'Please Note',
							'You have selected the option of Pay and Clear Now payment. Please select Yes or No as confirmation')

			return false;
		} else
			return true;
	}

	proofOfPaymentsExpander();
	var exanded = false;

	function proofOfPaymentsExpander() {
		$('#proofOfPayment').attr('onclick', '').unbind('click');
		$('#proofOfPayment').attr('onclick',
				'proofOfPaymentsExpanderClick(this);').bind('onclick');
	}

	function proofOfPaymentsExpanderClick(me) {

		if (exanded == true) {
			$('#expand_paymentTable_row_1').hide();
		} else {
			console.log('Not Expanded')
			$('#expand_paymentTable_row_1').show();
		}

		if (exanded == true) {
			exanded = false;
			$(me)
					.find('a')
					.css(
							{
								'background' : 'url(/banking/03images/fnb/icons/addSmall.png) no-repeat center'
							})

		} else if (exanded == false) {
			exanded = true;
			$(me)
					.find('a')
					.css(
							{
								'background' : 'url(/banking/03images/fnb/icons/removeSmall.png) no-repeat center'
							})
		}

		$(me).attr('onclick', '').unbind('click');
		$(me).attr('onclick', 'proofOfPaymentsExpanderClick(this);').bind(
				'onclick');
	}