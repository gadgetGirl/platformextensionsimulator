$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();

		},
		checkDateAndSubmitForm: function(checkHoliday) {
			_datePicker.checkFooterDatePicker('/banking/Controller?nav=navigator.payments.simple.HolidayCalenderPopup', 'OnceOffPayments', $('#mainBtnHref'), checkHoliday);
		},
		
		pageLoaded : function() {

			var parent = this;
					
			if (parent.configObject.normalPayment) {
				fnb.functions.showHideToggleElements.showHideToggle([
				                   {show:'true',element:'.notGlobal'},
				                   {show:'false',element:'.publicRecipient'},
				                   {show:'false',element:'.eWallet'},
				                   {show:'true',element:'#recipientForm'},
				                   {show:'true',element:'.notify'}]);
			} else if(parent.configObject.publicRecipient) {
				 fnb.functions.showHideToggleElements.showHideToggle([
				                    {show:'false',element:'.notGlobal'},
				                    {show:'true',element:'.publicRecipient'},
				                    {show:'true',element:'#recipientForm'},
				                    {show:'false',element:'.eWallet'},
				                    {show:'true',element:'.notify'}]);
			} else if (parent.configObject.ewallet) {
				fnb.functions.showHideToggleElements.showHideToggle([
									{show:'false',element:'.notGlobal'},
									{show:'false',element:'.publicRecipient'},
									{show:'false',element:'#recipientForm'},
									{show:'true',element:'.eWallet'},
									{show:'false',element:'.notify'}]);
			}

		},
		submitUrlWithMultipleParams: function(url,keyValuePairsObject){
		
			var href = url
			var params =[]
			var paramString = ''			
			for (var key in keyValuePairsObject) {
				inputValue = $(keyValuePairsObject[key]).attr('value')==''? null :  $(keyValuePairsObject[key]).attr('value') 
				params.push(key+'='+inputValue);
			}
			if(params.length > 0){
				paramString=params.toString().replace(',','&');
			}else{
				paramString = '';
			}
		
			fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow',{url:href,params:paramString});
		}
	};

	namespace("fnb.payments.simple.OnceOffPayments",genericPageObject);
});


