$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){	
			
			var parent = this; 	
			parent.configObject = dataSource;
			parent.pageLoaded();
			
		},
		
		pageLoaded: function() {
		
			var parent = this;
			
			if (parent.configObject.type == 1){
				
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'true',
					element : '#bank_recipient_wrapper'
				}, {
					show : 'true',
					element : '#account_wrapper'
				}, {
					show : 'false',
					element : '#recipient_wrapper'
				} ])
				
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show : 'false',
					element : '#bank_recipient_wrapper'
				}, {
					show : 'true',
					element : '#recipient_wrapper'
				}, {
					show : 'false',
					element : '#account_wrapper'
				} ])
			}
		}
		
	}
	
	namespace("fnb.payments.simple.MaintainRecipientLanding",genericPageObject);
});


