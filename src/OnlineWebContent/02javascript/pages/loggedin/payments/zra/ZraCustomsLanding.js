$(function(){

	function genericPageObject(){
	}

	genericPageObject.prototype = {
			init: function(config){
				var me = this;
				me.config = config;
				me.start();
				return me;
			}
			,destroy: function(){
				var me = this;
			}
			,start: function(){
				var me = this;
				
			}
			,doSearch: function (){

				var searchType = $('#searchType').find('input').attr('value');
				var zraNumber = $("#zraNumber").val();
					
				url = "/banking/Controller?nav=payments.zra.navigator.ZraAssessmentSearch&reference=2492&zraNumber="+zraNumber+"&searchType="+searchType;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget', {url : url,target : "#assessmentTableContainer"});				

			}
			,doMiscSearch: function (){
				var zraNumber = $("#zraNumber").val();
				
				var url = "/banking/Controller?nav=payments.zra.navigator.ZraAssessmentSearch&reference=2493&zraNumber="+zraNumber;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget', {url : url,target : "#assessmentTableContainer"});
			}
					
	}

	namespace("fnb.payments.zra.ZraCustomsLanding",genericPageObject);

});