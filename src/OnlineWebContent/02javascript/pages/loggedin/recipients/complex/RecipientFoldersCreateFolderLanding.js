$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
			init: function(dataSource){
				
			},
			clear: function(){
				$('#consolidated').trigger('click');
				$('#override-graphic-wrapper').removeClass('checked');
				$('#override').val(false);
				$('#override').attr('data-value','false');
				$('#override').removeAttr("checked");
				$('#folderType_parent').find('li:eq(0)').trigger('click');
				$('#recipientFoldersCapture').find('.input-input').each(function(inputIndex, input) {
					$(this).val('');
				});
			},
			showHide: function(me){
				var val = $(me).attr('data-value');
				if ("2" == val){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#paymentAccountsBlock'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionAccountsBlock'}]);
				} else if ("3" == val){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentAccountsBlock'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#collectionAccountsBlock'}]);
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentAccountsBlock'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionAccountsBlock'}]);
				}
			}
	}

	namespace("fnb.payments.complex.RecipientFoldersCreateFolderLanding",genericPageObject);
});
