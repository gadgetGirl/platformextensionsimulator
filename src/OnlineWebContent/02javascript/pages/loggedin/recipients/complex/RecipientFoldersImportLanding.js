$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
			init: function(dataSource){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'},{show:'false',element:'#account'}]);
			},
			clear: function(){
				$('#type_parent').find('li:eq(0)').trigger('click');
				$('#switcherWrappernew').find('div:eq(0)').trigger('click');
				$('#fromaccoverride').find('div:eq(2)').trigger('click');
				$('#statementEntry').find('div:eq(1)').trigger('click');
				
				$('#recipientFoldersAddImportLanding').find('.input-input').each(function(inputIndex, input) {
					$(this).val('');
				});
			},
			showHideDivs: function(me){
				var type =$("#type").val();
				var format =$(me).attr('data-value');
				if (type == "2"){
					if (format == "CAMS (Import),*.*" || format == "CAMS (Export),*.*"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#account'}]);
					}
					else{
							fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'}]);
					}
				}
				else if (type == "3"){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'}]);
				}
			},
			showHide: function(me){
					var type =$(me).attr('data-value');
					var option=$(".radioGroupValue").attr("value");
					var format =$("#paymentFileType").val();
					if (type == "2"){
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#paymentFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFolders'}]);
						if (option=="0"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFolders'}]);
						}
						else if (option=="1"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#paymentFolders'}]);
						}
						if (format == "CAMS (Import),*.*" || format == "CAMS (Export),*.*"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#account'}]);
						}
						else{
							fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'}]);
						}
					}
					else if (type == "3"){
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFolders'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#collectionFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#collectionFolders'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'}]);
						if (option=="0"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFolders'}]);
						}
						else if (option=="1"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#collectionFolders'}]);
						}
					}
					else{
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFolders'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFolders'}]);
					}
					
				},
			showHideBlock: function(me){
					var type =$("#type").val();
					var option=$(me).attr("data-value");
					var format =$("#paymentFileType").val();
					if (option=="0"){
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#NewFolderBlock'}]);
					}
					else if (option=="1"){
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#NewFolderBlock'}]);
					}
					if (type == "2"){
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#paymentFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFolders'}]);
						if (option=="0"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFolders'}]);
							fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#NewFolderBlock'}]);
						}
						else if (option=="1"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#paymentFolders'}]);
							fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#NewFolderBlock'}]);
						}
						if (format == "CAMS (Import),*.*" || format == "CAMS (Export),*.*"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#account'}]);
						}
						else{
							fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'}]);
						}
					}
					else if (type == "3"){
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFolders'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#collectionFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#collectionFolders'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'}]);
						if (option=="0"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFolders'}]);
							fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#NewFolderBlock'}]);
						}
						else if (option=="1"){
							fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#collectionFolders'}]);
							fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#NewFolderBlock'}]);
						}
					}
					else{
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFolders'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFormat'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#collectionFolders'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#account'}]);
					}
				},
				
				showReference: function(){
					$('#headerFromRef').parent().parent().parent().show();
				},
				
				hideReference: function(){
					$('#headerFromRef').parent().parent().parent().hide();
					
				}
			}

	namespace("fnb.payments.complex.RecipientFoldersImportLanding",genericPageObject);
});
