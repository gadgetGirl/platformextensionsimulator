$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
			init: function(dataSource){
				
			},
			clear: function(){
				$('#RecipientTypeSelected').trigger('click');
				$('#UnlimitedLimitSelected').trigger('click');
				$('#UnlimitedPaymentsSelected').trigger('click');
				$('#RecipientFoldersAddLanding').find('.input-input').each(function(inputIndex, input) {
					$(this).val('');
				});
			},
			redirect: function(){
				document.getElementById("addMore").value="yes";
				fnb.functions.submitFormToWorkspace.submit("RecipientFoldersAddLanding");
			},
			submit: function(){
				document.getElementById("addMore").value="";
				fnb.functions.submitFormToWorkspace.submit("RecipientFoldersAddLanding");
			},
			submitUrlWithMultipleParams: function(url,keyValuePairsObject){
				
				var href = url
				var params =[]
				var paramString = ''			
				for (var key in keyValuePairsObject) {
					inputValue = $(keyValuePairsObject[key]).attr('value')==''? null :  $(keyValuePairsObject[key]).attr('value') 
					params.push(key+'='+inputValue);
				}
				if(params.length > 0){
					paramString=params.toString().replace(',','&');
				}else{
					paramString = '';
				}
			
				fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow',{url:href,params:paramString});
			}
	};

	namespace("fnb.payments.complex.RecipientFoldersAddLanding",genericPageObject);
});
