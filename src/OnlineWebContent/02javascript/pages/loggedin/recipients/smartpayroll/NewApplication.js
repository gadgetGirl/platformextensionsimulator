$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {

			init: function(dataSource){
				var parent = this;
				parent.configObject = dataSource;
			
			},
			destroy: function(){
				var parent = this;
				parent = null;
			},

			displayDivs: function(){
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#mainDiv'}]);	
			},
			
			displayOtherDivs: function(val){
				
				if (val == 'existing'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#existingFolderDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#folderDiv'}]);
				}
				else{
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#folderDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#existingFolderDiv'}]);
					
				}
			},
			isEditing: function(folderName){
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#mainDiv'}]);
				if (folderName == ''){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#existingFolderDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#folderDiv'}]);
				}
				else{
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#folderDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#existingFolderDiv'}]);
					
				}
			}
	}

	namespace("fnb.recipients.smartpayroll.NewApplication",genericPageObject);
});