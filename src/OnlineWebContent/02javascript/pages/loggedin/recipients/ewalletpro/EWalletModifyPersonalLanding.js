
function ProofOfIDPassportResidence(){
	fnb.functions.showHideToggleElements.showHideToggle([
		{show:'true', element:'#blockOne'},
		{show:'true', element:'#blockTwo'},
		{show:'true', element:'#blockThree'},
		{show:'true', element:'#blockOneTwoThree'}
	]);
};
function IDPassportShowCopyTaken(){
	fnb.functions.showHideToggleElements.showHideToggle([
		{show:'true', element:'#blockOne'},
		{show:'false',element:'#blockTwo'},
		{show:'true', element:'#blockThree'},
		{show:'true', element:'#blockOneTwoThree'}
	]);
};
function NoIDPassportShow(){
	fnb.functions.showHideToggleElements.showHideToggle([
		{show:'false',element:'#blockOne'},
		{show:'false',element:'#blockTwo'},
		{show:'false',element:'#blockThree'},
		{show:'false',element:'#blockOneTwoThree'}
	]);
};

//Need to Speak to mike to see how I need to invoke this...will move it once clarified
function toggleOption(){
	var docVal = '${docStatus}'
	if(docVal == 'Y'){
		ProofOfIDPassportResidence();
	}else if(docVal == 'X'){
		IDPassportShowCopyTaken();
	}else if(docVal == 'N'){
		NoIDPassportShow();
	}
 }