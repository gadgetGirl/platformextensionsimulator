$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();			
		}, 

		pageLoaded : function() {
			var parent = this;
		},
		
		searchForFines : function(by, targetDiv){
			var parent = this;
			var url = '/banking/Controller?nav=paytrafficfines.navigator.TrafficFinesSearch&searchByAction='+by;
			
			if(by == 'notice'){
				var noticeNumber = $("#noticeNumber").val();
				var payFromAccount = $("#payFromAccount").val();
				url += "&noticeNumber=" + noticeNumber + "&payFromAccount=" + payFromAccount;
			}else{
				this.clearTable();
			}
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace',{url:url});			
		},
		
		clearTable : function(){
			$("#noticeNumber").val("");
			$("#finesDIV").html("");
		}
		

	}
	namespace("fnb.paytrafficfines.TrafficFinesLanding",genericPageObject); 
	
});