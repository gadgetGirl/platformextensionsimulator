$(function() {
	function genericPageObject() {
		this.configObject = {};
		this.data;
	}
	genericPageObject.prototype = {

		init : function(dataSource1) {
			this.data =	dataSource1;	
			fnb.utils.graph.type = "pyramids";
			fnb.utils.graph.target = "#Target2";
			fnb.utils.graph.init(dataSource1,'#Target2',$('#popupWrapper').width());

		}
	}
	namespace("fnb.rewards.simple.Rewards",genericPageObject);
});