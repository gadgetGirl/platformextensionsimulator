$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {
		init : function(dataSource1) {
			fnb.utils.graph.type = "stackedGroup";
			fnb.utils.graph.init(dataSource1, "#stackedBarGraph");
			fnb.utils.graph.type = "stackedGroup";
			fnb.utils.graph.init(dataSource1, "#stackedBarGraphHidden");
		},
		stackClick: function(graphClicked){
			var graphNumber = graphClicked;
			var showHideSelected = $(graphNumber).attr("data-value");
			$("#stackedBarGraphHiddenContainer .stackedGraphs").hide();
			$("#stackedBarGraphHiddenContainer .graphNo" + showHideSelected).show();
			$(".itemBreakdowns").hide();
			$("#itemBreakdownsContainer" + showHideSelected).show();
		},
		getAxisData: function(num){
			var axisData = [
		//("Everyday Shopping")		                
							[{heading:'0.6%',val:-4000,valPrefix:''},{heading:'0.9%',val:0,valPrefix:' pts'},{heading:'1.2%',val:12000,valPrefix:' pts'},{heading:'1.5%',val:16000,valPrefix:' pts'},{heading:'3%',val:18000,valPrefix:' pts'}],
		//("Grocery Spend")
							[{heading:'0.6%',val:-4000,valPrefix:' pts'},{heading:'0.9%',val:0,valPrefix:' pts'},{heading:'1.6%',val:12000,valPrefix:' pts'},{heading:'2.5%',val:16000,valPrefix:' pts'},{heading:'5%',val:18000,valPrefix:' pts'}],			                
		//("Checkers") 
							[{heading:'1%',val:-4000,valPrefix:' pts'},{heading:'2.5%',val:0,valPrefix:' pts'},{heading:'5%',val:12000,valPrefix:' pts'},{heading:'7.5%',val:16000,valPrefix:' pts'},{heading:'15%',val:18000,valPrefix:' pts'}],
		//("Fuel And Gautrain Tickets")
							[{heading:'0.6%',val:-4000,valPrefix:' pts'},{heading:'2.5%',val:0,valPrefix:' pts'},{heading:'5%',val:12000,valPrefix:' pts'},{heading:'7.5%',val:16000,valPrefix:' pts'},{heading:'15%',val:18000,valPrefix:' pts'}],		
		//("Online Prepaid Airtime Purchases")
							[{heading:'0.6%',val:-4000,valPrefix:' pts'},{heading:'2.5%',val:0,valPrefix:' pts'},{heading:'5%',val:12000,valPrefix:' pts'},{heading:'7.5%',val:16000,valPrefix:' pts'},{heading:'15%',val:18000,valPrefix:' pts'}],		
		//("Online Prepaid Electricity Purchases")
							[{heading:'0.6%',val:-4000,valPrefix:' pts'},{heading:'2.5%',val:0,valPrefix:' pts'},{heading:'5%',val:12000,valPrefix:' pts'},{heading:'7.5%',val:16000,valPrefix:' pts'},{heading:'15%',val:18000,valPrefix:' pts'}],
		//("Smart Device Contract Spend") 
							[{heading:'0%',val:-4000,valPrefix:' pts'},{heading:'10%',val:0,valPrefix:' pts'},{heading:'25%',val:12000,valPrefix:' pts'},{heading:'50%',val:16000,valPrefix:' pts'},{heading:'100%',val:18000,valPrefix:' pts'}],
		//("Priority Pass")
							[{heading:'0',val:-4000,valPrefix:' pts'},{heading:'1',val:0,valPrefix:' pts'},{heading:'2',val:12000,valPrefix:' pts'},{heading:'4',val:16000,valPrefix:' pts'},{heading:'10',val:18000,valPrefix:' pts'}],		  
		//("Avis Point 2 Point trips")  
							[{heading:'0',val:-4000,valPrefix:' pts'},{heading:'1',val:0,valPrefix:' pts'},{heading:'2',val:12000,valPrefix:' pts'},{heading:'4',val:16000,valPrefix:' pts'},{heading:'5',val:18000,valPrefix:' pts'}],		  
		//("SLOW + Bidvest Airport Lounge Access")  
							[{heading:'8',val:-4000,valPrefix:' pts'},{heading:'&#8734;',val:0,valPrefix:' pts'},{heading:'&#8734;',val:12000,valPrefix:' pts'},{heading:'&#8734;',val:16000,valPrefix:' pts'},{heading:'&#8734;',val:18000,valPrefix:' pts'}],		  
		//("SLOW in the City Access")    
							[{heading:'0',val:-4000,valPrefix:' pts'},{heading:'2',val:0,valPrefix:' pts'},{heading:'2',val:12000,valPrefix:' pts'},{heading:'2',val:16000,valPrefix:' pts'},{heading:'2',val:18000,valPrefix:' pts'}],
		//("Uncapped ADSL Data Discount") 
							[{heading:'10%',val:-4000,valPrefix:' pts'},{heading:'20%',val:0,valPrefix:' pts'},{heading:'45%',val:12000,valPrefix:' pts'},{heading:'85%',val:16000,valPrefix:' pts'},{heading:'100%',val:18000,valPrefix:' pts'}],		
		//("3G Data") 
							[{heading:'100<br/>MB',val:-4000,valPrefix:' pts'},{heading:'200<br/>MB',val:0,valPrefix:' pts'},{heading:'300<br/>MB',val:12000,valPrefix:' pts'},{heading:'400<br/>MB',val:16000,valPrefix:' pts'},{heading:'500<br/>MB',val:18000,valPrefix:' pts'}]
							
						];
			
			return axisData[num];
		},
		newGraph: function(str){
			
			var karr = str.split('_');
			var num = karr[0];
			var value = karr[1];
			var level = parseInt(karr[2]);
			var currentXAxisData = this.getAxisData(num);

			$("#specificDisclaimers h3").addClass("displayNone");
			switch(num) {
				case '0':
					$("#specificDisclaimers  #shopping").removeClass("displayNone");
					break;
				case '1':
					$("#specificDisclaimers  #grocery").removeClass("displayNone");
					break;
				case '2':
					$("#specificDisclaimers  #checkersShopright").removeClass("displayNone");
					break;
				case '3':
					$("#specificDisclaimers  #fuelAndGautrain").removeClass("displayNone");
					break;
				case '4':
					$("#specificDisclaimers  #prepaid").removeClass("displayNone");
					break;
				case '5':
					$("#specificDisclaimers  #prepaidElectricity").removeClass("displayNone");
					break;
				case '6':
					$("#specificDisclaimers  #smartDeviceContract").removeClass("displayNone");
					break;
				case '7':
					$("#specificDisclaimers  #priorityPass").removeClass("displayNone");
					break;
				case '8':
					break;
				case '9':
					if (level < 1){
						$("#specificDisclaimers  #slowLounge").removeClass("displayNone");
					}else{
						$("#specificDisclaimers  #slowLounge2").removeClass("displayNone");
					}
					break;
				case '10':
					$("#specificDisclaimers  #slowInTheCity").removeClass("displayNone");
					break;
				case '11':
					$("#specificDisclaimers  #uncappedADSLDiscounts").removeClass("displayNone");
					break;
				case '12':
					if(level > 0){
						$("#specificDisclaimers  #data3G").removeClass("displayNone");
					}
					break;
				default:
					break;
			}


			var mess1;
		
			switch(num) {
			case '11':
				mess1 = 'Discount **';
			break;
			case '7':
				mess1 = 'Free<br/>visits p/a **';
			break;
			case '8':
				mess1 = 'Free<br/>trips p/a';
			break;
			case '9':
				if (value > 0 ){
					mess1 = 'Unlimited free<br/>visits **'
				}else{
					mess1 = 'Free<br/>visits **';
				}
			break;
			case '10':
				mess1 = 'Free<br/>visits **';
			break;
			case '12':
				mess1 = 'Free 3G<br/>data **';
			break;
			
			default:
				mess1 = 'back in eBucks **';
			};

			if(level > 0){
				$("#specificDisclaimers  #level5").removeClass("displayNone");
			};
			if(level == 0){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#level0WealthContainer'},{show:'false',element:'#level1WealthContainer'}]);
			} else if (level == 1) {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#level0WealthContainer'},{show:'true',element:'#level1WealthContainer'}]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#level0WealthContainer'},{show:'false',element:'#level1WealthContainer'}]);
			}
			
			var dataSource = [{level:level,val:value,valPrefix:' pts*',label:' Your score',legend:mess1,bumpVal:'',bumpValLabel:'',xAxisData:currentXAxisData}];
			
			setTimeout(function() {
				fnb.utils.graph.type = "verticalBar";
				fnb.utils.graph.init(dataSource, '#verticalBarGraph');
			}, 500);
			
		}
	}

	namespace("fnb.rewards.simple.Rewards",genericPageObject);
});
		