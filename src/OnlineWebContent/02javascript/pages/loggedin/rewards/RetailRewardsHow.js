$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {
		init : function(dataSource1, dataSource2) {
			setTimeout(function() {
				fnb.utils.graph.type = "verticalBar";
				fnb.utils.graph.init(dataSource2, '#verticalBarGraph');
				fnb.utils.graph.type = "stackedGroup";
				fnb.utils.graph.init(dataSource1, "#stackedBarGraph");
				fnb.utils.graph.type = "stackedGroup";
				fnb.utils.graph.init(dataSource1, "#stackedBarGraphHidden");
			}, 500);
		},
		stackClick: function(graphClicked){
			var graphNumber = graphClicked;
			var showHideSelected = $(graphNumber).attr("data-value");
			$("#stackedBarGraphHiddenContainer .stackedGraphs").hide();
			$("#stackedBarGraphHiddenContainer .graphNo" + showHideSelected).show();
			$(".itemBreakdowns").hide();
			$("#itemBreakdownsContainer" + showHideSelected).show();
		}
	}

	namespace("fnb.rewards.simple.Rewards",genericPageObject);
});


