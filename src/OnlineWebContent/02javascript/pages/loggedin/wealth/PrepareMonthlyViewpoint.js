$(function(){
	function genericPageObject(){
		this.configObject = {};
		this.skin = '';
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){
			var parent = this;
		    parent.configObject = dataSource;
		},
		destroy: function(){
			var parent = this;
			parent = null;
		},
		
		openViewPoint: function(){
			var viewpoint = $("#monthEndDropDown").val();
			var path = "https://www.fnb.co.za/downloads/RMBPrivateBank/"+this.configObject.skin+"/MonthlyInvestmentViewpoint_"
					+ viewpoint + ".pdf";
			if (!window.open(path, 'moreInfo', 'width=800,height=600,left='
					+ (screen.width / 2 - 400) + ',top='
					+ (screen.height / 2 - 300)
					+ ',scrollbars=yes,status=yes,resizable=yes')){
				//alertO('Monthly Viewpoint Was not found.');
			}
		},
	}
	
	namespace("fnb.wealth.PrepareMonthlyViewpoint",genericPageObject);
});