(function($){

	function genericPageObject(){
	}

	genericPageObject.prototype = {
			init: function(config){
				var me = this;
				me.config = config;
				me.start();
				return me;
			}
			,destroy: function(){
				var me = this;
			}
			,start: function(){
				var me = this;
				
			}
			,validateForm: function(){
				var company = document.getElementById("compName").value;
				var share	= document.getElementById("shareCode").value;
				var msg = "";
				var err = 0;
				
				if (company == "" && share == ""){
					msg +="Please enter a value to search by Company or Share";
					err++;
				} 
				
				if(err > 0){
					window.setTimeout('showError("' + msg + '");',50); 				
					return false;
				}else{				
					fnb.functions.submitFormToWorkspace.submit("portfolioManagementShareSearchForm");
				}	
			}
	}

	namespace("fnb.wealth.PortfolioManagementSharesSearch",genericPageObject);

})(jQuery);

function showError(errorMessage){
	fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
}