(function($){
	
	function genericPageObject(){
		var me = this;
	}
	
	
	genericPageObject.prototype = {
			
			init: function(config){
				
				var me = this;
				me.config = config;
				me.start();
				return me;
				
			}
			, destroy: function(){
				
				var me = this;
				
			}
			, start: function(){
				var me = this;
				console.log("starting PortfolioLanding");
				
			}

	}
	
	
	namespace("pages.fnb.wealth.simple.PortfolioLanding",new genericPageObject());
	
	
	
})(jQuery);
