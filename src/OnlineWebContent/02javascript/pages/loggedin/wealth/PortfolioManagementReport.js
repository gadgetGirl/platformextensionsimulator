(function($){

	function genericPageObject(){
	}

	genericPageObject.prototype = {
			init: function(config){
				var me = this;
				me.config = config;
				me.start();
				return me;
			}
			,destroy: function(){
				var me = this;
			}
			,start: function(){
				var me = this;
				
			}
			,reportsDropDownChanged: function(type){
				if(type==10||type==11){
					fnb.functions.showHideToggleElements.showHideToggle([
					                                                     {element:'#fromToDate',show : 'true'}
					                                                     ,{element:'#dateBox',show : 'false'}
					                                                     ,{element:'#monthEnd',show : 'false'}
					                                                     ,{element:'#monthEnd2',show : 'false'}
					                                                     ]);
				}else if(type==1||type==12){
					fnb.functions.showHideToggleElements.showHideToggle([
					                                                     {element:'#dateBox',show : 'true'}
					                                                     ,{element:'#fromToDate',show : 'false'}
					                                                     ,{element:'#monthEnd',show : 'false'}
					                                                     ,{element:'#monthEnd2',show : 'false'}
					                                                     ]);
				}else if((type==3||type==4||type==5||type==6||type==7||type==9||type==13||type==14)&&$("#frequency").val()=='Monthly'){
					fnb.functions.showHideToggleElements.showHideToggle([
					                                                     {element:'#monthEnd',show : 'true'}
					                                                     ,{element:'#monthEnd2',show : 'false'}
					                                                     ,{element:'#fromToDate',show : 'false'}
					                                                     ,{element:'#dateBox',show : 'false'}
					                                                     ]);
				}else if(type==2){
					if ($("#frequency").val()=='Monthly') {
						fnb.functions.showHideToggleElements.showHideToggle([
						                                                     {element:'#monthEnd2',show : 'true'}
						                                                     ,{element:'#monthEnd',show : 'false'}
						                                                     ,{element:'#fromToDate',show : 'false'}
						                                                     ,{element:'#dateBox',show : 'false'}
						                                                     ]);
					}
					else {
						fnb.functions.showHideToggleElements.showHideToggle([
						                                                     {element:'#monthEnd2',show : 'false'}
						                                                     ,{element:'#monthEnd',show : 'false'}
						                                                     ,{element:'#fromToDate',show : 'false'}
						                                                     ,{element:'#dateBox',show : 'false'}
						                                                     ]);
					}
				}else if(type==8){
					fnb.functions.showHideToggleElements.showHideToggle([
					                                                     {element:'#monthEnd2',show : 'true'}
					                                                     ,{element:'#monthEnd',show : 'false'}
					                                                     ,{element:'#fromToDate',show : 'false'}
					                                                     ,{element:'#dateBox',show : 'false'}
					                                                     ]);
				}else{
					fnb.functions.showHideToggleElements.showHideToggle([
					                                                     {element:'#monthEnd2',show : 'false'}
					                                                     ,{element:'#monthEnd',show : 'false'}
					                                                     ,{element:'#fromToDate',show : 'false'}
					                                                     ,{element:'#dateBox',show : 'false'}
					                                                     ]);
				}
			}
			
			,dropDownChanged: function(item,type){
				var me = this;
				// item will be an li with data-value of the account rfn
				var selectedAccount="0";
				var frequency="0";
				if(type=="portfolio"){
					selectedAccount=$(item).data("value");
					frequency=$("#frequency").val();
				}
				if(type=="frequency"){
					frequency=$(item).data("value");
					console.log(frequency);
					selectedAccount=$("#portfolio").val();
				}
				
				var targetDiv = $("#reportType_parent").parent();
				var dropdownURL="/banking/Controller?nav=wealth.navigator.PortfolioManagementReportsDropdown&frequency="+frequency+"&portfolio="+selectedAccount;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url: dropdownURL,target: targetDiv,
					postLoadingCallBack: function(){me.reportsDropDownChanged(0);}
					});	
				
				
			},
			reportsDropDownChangedBefore: function(me){
				
				var parentObject = this;
				
				var dropdownValue = $(me).data('value');
				parentObject.reportsDropDownChanged(dropdownValue);
					
				
				
			}
			
			,emailReport: function(){
				document.getElementById("nav").value="wealth.navigator.PortfolioManagementReportEmail";
				fnb.functions.submitFormToWorkspace.submit("portfofolioViewForm");
			}
			
			,viewReport: function(){
				document.getElementById("nav").value="wealth.navigator.PortfolioManagementReportView";
				fnb.functions.submitFormToWorkspace.submit("portfofolioViewForm");
			}
	}

	namespace("fnb.wealth.PortfolioManagementReport",genericPageObject);

})(jQuery);