$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
						
		}, 

		pageLoaded : function() {
			var parent = this;
			var initialDescription = parent.configObject.initialDescription;
			var transactionReasonSelected = parent.configObject.transactionReasonSelected;
			var transactionDescriptionSelected = parent.configObject.transactionDescriptionSelected;
			var segmentInIntFormat = parent.configObject.segmentInIntFormat;
			var reasonWithProperties = parent.configObject.reason;
			var showDescription = parent.configObject.showDescription;
			
			parent.hideFields(initialDescription, transactionReasonSelected, transactionDescriptionSelected);
			
            if (transactionDescriptionSelected != null && transactionDescriptionSelected != "" && transactionDescriptionSelected != "0")
            {
                parent.setExtraFieldsOnLoad(segmentInIntFormat, reasonWithProperties);
            }
            else
            {
                parent.showExtraFields(reasonWithProperties);
            }
            if (showDescription=="false") {
            	parent.onSearchWizzardDropdownChangeToOther();
            }
			
		},
		
		hideFields : function(initialDescription, transactionReasonSelected, transactionDescriptionSelected) {
			
			//split bopcode
			
			//set to true/false;
			var searchWizzard1 = "";
			var searchWizzard2 = "";
			var searchWizzard3 = "";
			
			var searchWizzard4d = "";
			var searchWizzard4e = "";
			var searchWizzard4f = "";
			var searchWizzard4g = "";
			
			var searchWizzard4 = "";
			var searchWizzard5a = "";
			var searchWizzard5b = "";
			var searchWizzard5c = "";
			var searchWizzard6 = "";
			var searchWizzard7 = "";
			var searchWizzard8 = "";
			var searchWizzard9 = "";
			var searchWizzard10 = "";
			var helperVariables = "false";
			
			var parent = this;
			
			if((initialDescription == null && transactionDescriptionSelected == null) 
			|| (initialDescription != null && transactionDescriptionSelected != null && initialDescription == "" && transactionDescriptionSelected == ""))
			{
				searchWizzard1 = "true";
				searchWizzard2 = "false";
				searchWizzard3 = "false";
				
				
				searchWizzard4d = "false";
				searchWizzard4e = "false";
				searchWizzard4f = "false";
				searchWizzard4g = "false";
				
				searchWizzard4 = "true";
				searchWizzard5a = "false";
				searchWizzard5b = "false";
				searchWizzard5c = "false";
				searchWizzard6 = "false";
				searchWizzard7 = "false";
				searchWizzard8 = "false";
				searchWizzard9 = "false";
				searchWizzard10 = "false";				
			}
			else if ((initialDescription != null && transactionDescriptionSelected != null)
			&& (initialDescription != "" && initialDescription != "-1" && transactionDescriptionSelected == ""))
			{
				searchWizzard1 = "true";
				searchWizzard2 = "false";
				searchWizzard3 = "false";
				
				searchWizzard4d = "false";
				searchWizzard4e = "false";
				searchWizzard4f = "false";
				searchWizzard4g = "false";
				
				searchWizzard4 = "true";
				searchWizzard5a = "false";
				searchWizzard5b = "false";
				searchWizzard5c = "false";
				searchWizzard6 = "false";
				searchWizzard7 = "false";
				searchWizzard8 = "false";
				searchWizzard9 = "false";
				searchWizzard10 = "false";
				
			}
			/*Below 'else if' not needed. Done via method setExtraFieldsOnLoad*/
			/*else if ((initialDescription != null && transactionDescriptionSelected != null)
			&& (initialDescription != "" && initialDescription == "-1" && transactionDescriptionSelected != ""))
			{
				searchWizzard1 = "true";
				searchWizzard2 = "true";
				searchWizzard3 = "false";				
				searchWizzard4d = "false";				
				searchWizzard4e = "false";
				searchWizzard4f = "false";
				searchWizzard4g = "false";
				searchWizzard4 = "true";
				searchWizzard5a = "false";
				searchWizzard5b = "false";
				searchWizzard5c = "false";
				searchWizzard6 = "false";
				searchWizzard7 = "false";
				searchWizzard8 = "false";
				searchWizzard9 = "false";
				searchWizzard10 = "false";

			}*/		
			else
			{
				searchWizzard1 = "true";
				searchWizzard2 = "false";
				searchWizzard3 = "false";
				
				searchWizzard4d = "false";
				searchWizzard4e = "false";
				searchWizzard4f = "false";
				searchWizzard4g = "false";
				
				searchWizzard4 = "true";
				searchWizzard5a = "false";
				searchWizzard5b = "false";
				searchWizzard5c = "false";
				searchWizzard6 = "false";
				searchWizzard7 = "false";
				searchWizzard8 = "false";
				searchWizzard9 = "false";
				searchWizzard10 = "false";
				
			}
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:searchWizzard1,element:'#searchWizzard1'},
				                              	                   {show:searchWizzard2,element:'#searchWizzard2'},
				                              	                   {show:searchWizzard3,element:'#searchWizzard3'},
				                              	                  
				                              	                   {show:searchWizzard4d,element:'#searchWizzard4d'},
				                              	                   {show:searchWizzard4e,element:'#searchWizzard4e'},
				                              	                   {show:searchWizzard4f,element:'#searchWizzard4f'},
				                              	                   {show:searchWizzard4g,element:'#searchWizzard4f'},
				                              	                   
				                              	                   {show:searchWizzard4,element:'#searchWizzard4'},
				                              	                   {show:searchWizzard5a,element:'#searchWizzard5a'},
				                              	                   {show:searchWizzard5b,element:'#searchWizzard5b'},				                   
				                              	                   {show:searchWizzard5c,element:'#searchWizzard5c'},
				                              	                   {show:searchWizzard6,element:'#searchWizzard6'},
				                              	                   {show:searchWizzard7,element:'#searchWizzard7'},
				                              	                   {show:searchWizzard8,element:'#searchWizzard8'},
				                              	                   {show:searchWizzard9,element:'#searchWizzard9'},
				                              	                   {show:searchWizzard10,element:'#searchWizzard10'},
				                              	                   {show:helperVariables,element:'#helperVariables'}]);
			

		},
		hideReasonsFields : function(bopCode) {
			
			//split bopcode
			
			//set to true/false;
			var searchWizzard1 = "";
			var searchWizzard2 = "";
			var searchWizzard3 = "";

			var searchWizzard4d = "";
			var searchWizzard4e = "";
			var searchWizzard4f = "";
			var searchWizzard4g = "";
			
			var searchWizzard4 = "";
			var searchWizzard5a = "";
			var searchWizzard5b = "";
			var searchWizzard5c = "";
			var searchWizzard6 = "";
			var searchWizzard7 = "";
			var searchWizzard8 = "";
			var searchWizzard9 = "";
			var searchWizzard10 = "";

			if (bopCode != null && bopCode != "" && bopCode == "-1")
			{
				searchWizzard1 = "true";
				searchWizzard2 = "true";
				searchWizzard3 = "false";
				
				searchWizzard4d = "false";
				searchWizzard4e = "false";
				searchWizzard4f = "false";
				searchWizzard4g = "false";
				
				searchWizzard4 = "true";
				searchWizzard5a = "false";
				searchWizzard5b = "false";
				searchWizzard5c = "false";
				searchWizzard6 = "false";
				searchWizzard7 = "false";
				searchWizzard8 = "false";
				searchWizzard9 = "false";
				searchWizzard10 = "false";
				
			}
			else
			{
				searchWizzard1 = "true";
				searchWizzard2 = "false";
				searchWizzard3 = "false";
				
				searchWizzard4d = "false";
				searchWizzard4e = "false";
				searchWizzard4f = "false";
				searchWizzard4g = "false";
				
				searchWizzard4 = "true";
				searchWizzard5a = "false";
				searchWizzard5b = "false";
				searchWizzard5c = "false";
				searchWizzard6 = "false";
				searchWizzard7 = "false";
				searchWizzard8 = "false";
				searchWizzard9 = "false";
				searchWizzard10 = "false";
			}

			
			fnb.functions.showHideToggleElements.showHideToggle([{show:searchWizzard1,element:'#searchWizzard1'},
				                              	                   {show:searchWizzard2,element:'#searchWizzard2'},
				                              	                   {show:searchWizzard3,element:'#searchWizzard3'},
				                              	                  
				                              	                   {show:searchWizzard4d,element:'#searchWizzard4d'},
				                              	                   {show:searchWizzard4d,element:'#searchWizzard4e'},
				                              	                   {show:searchWizzard4d,element:'#searchWizzard4f'},
				                              	                   {show:searchWizzard4d,element:'#searchWizzard4g'},
				                              	                   
				                              	                   {show:searchWizzard4,element:'#searchWizzard4'},
				                              	                   {show:searchWizzard5a,element:'#searchWizzard5a'},
				                              	                   {show:searchWizzard5b,element:'#searchWizzard5b'},				                   
				                              	                   {show:searchWizzard5c,element:'#searchWizzard5c'},
				                              	                   {show:searchWizzard6,element:'#searchWizzard6'},
				                              	                   {show:searchWizzard7,element:'#searchWizzard7'},
				                              	                   {show:searchWizzard8,element:'#searchWizzard8'},
				                              	                   {show:searchWizzard9,element:'#searchWizzard9'},
				                              	                   {show:searchWizzard10,element:'#searchWizzard10'}]);
		},
		
		hideAlternateDescFields : function(reason) {
			
			//split bopcode
			
			//set to true/false;
			var searchWizzard1 = "";
			var searchWizzard2 = "";
			var searchWizzard3 = "";
			
			var searchWizzard4d = "";
			var searchWizzard4e = "";
			var searchWizzard4f = "";
			var searchWizzard4g = "";
			
			var searchWizzard4 = "";
			var searchWizzard5a = "";
			var searchWizzard5b = "";
			var searchWizzard5c = "";
			var searchWizzard6 = "";
			var searchWizzard7 = "";
			var searchWizzard8 = "";
			var searchWizzard9 = "";
			var searchWizzard10 = "";
			
			if (reason != null && reason != "" && reason != "0")
			{
				searchWizzard1 = "true";
				searchWizzard2 = "true";
				searchWizzard3 = "true";
				
				searchWizzard4d = "false";
				searchWizzard4e = "false";
				searchWizzard4f = "false";
				searchWizzard4g = "false";
				
				searchWizzard4 = "true";
				searchWizzard5a = "false";
				searchWizzard5b = "false";
				searchWizzard5c = "false";
				searchWizzard6 = "false";
				searchWizzard7 = "false";
				searchWizzard8 = "false";
				searchWizzard9 = "false";
				searchWizzard10 = "false";
				
			}
			else
			{
				searchWizzard1 = "true";
				searchWizzard2 = "true";
				searchWizzard3 = "false";
				
				searchWizzard4d = "false";
				searchWizzard4e = "false";
				searchWizzard4f = "false";
				searchWizzard4g = "false";
				
				searchWizzard4 = "true";
				searchWizzard5a = "false";
				searchWizzard5b = "false";
				searchWizzard5c = "false";
				searchWizzard6 = "false";
				searchWizzard7 = "false";
				searchWizzard8 = "false";
				searchWizzard9 = "false";
				searchWizzard10 = "false";
			}

			
			fnb.functions.showHideToggleElements.showHideToggle([{show:searchWizzard1,element:'#searchWizzard1'},
				                              	                   {show:searchWizzard2,element:'#searchWizzard2'},
				                              	                   {show:searchWizzard3,element:'#searchWizzard3'},
				                              	                  
				                              	                   {show:searchWizzard4d,element:'#searchWizzard4d'},
				                              	                   {show:searchWizzard4d,element:'#searchWizzard4e'},
				                              	                   {show:searchWizzard4d,element:'#searchWizzard4f'},
				                              	                   {show:searchWizzard4d,element:'#searchWizzard4g'},

				                              	                   {show:searchWizzard4,element:'#searchWizzard4'},
				                              	                   {show:searchWizzard5a,element:'#searchWizzard5a'},
				                              	                   {show:searchWizzard5b,element:'#searchWizzard5b'},				                   
				                              	                   {show:searchWizzard5c,element:'#searchWizzard5c'},
				                              	                   {show:searchWizzard6,element:'#searchWizzard6'},
				                              	                   {show:searchWizzard7,element:'#searchWizzard7'},
				                              	                   {show:searchWizzard8,element:'#searchWizzard8'},
				                              	                   {show:searchWizzard9,element:'#searchWizzard9'},
				                              	                   {show:searchWizzard10,element:'#searchWizzard10'}]);
		},
		
		onSearchWizzardDropdownChange : function(obj) {
			var parent = this;
			var val=$(obj).attr('data-value');			
			
			parent.resetExtraFields();
			
			parent.hideReasonsFields(val);
			
			if(val != null && val != "" && val != "0" && val != "-1"){
				parent.showExtraFields(val);			

			}	
		},
		
		onSearchWizzardDropdownChangeToOther : function() {
			var parent = this;
			//Set value of val to 'Other'			
			var val=-1;			
			parent.resetExtraFields();
			
			parent.hideReasonsFields(val);
			
		},
		
		/**
		 * Retrieves the alternative descriptions for a Reason of transaction.
		 */		
		getAlternativeDropdownValues : function(obj, divName, ddValue, segment) {
			var parent = this;
			var reason=$(obj).attr('data-value');
			
			parent.resetExtraFields();
			
			parent.hideAlternateDescFields(reason);
			
			parent.dropDownAjaxCall(reason, divName, ddValue, segment);
		},
		
		/**
		 * Makes a ajax call to populate dropdown
		 */
		dropDownAjaxCall : function(reason, divName, ddValue, segment) {
			if(reason != 0 && reason != "")
			{//Please Select
				var functionName = 'pages.fnb.forex.simple.ApplicationForGlobalPaymentAddReason.showAdditionalFields(this)';
				var url = '/banking/Controller?nav=forex.simple.navigator.ForexPaymentReceivedDropDownBuilder&value=' + ddValue + '&transactionReasonSelected=' + reason + '&type=outwardsAlternativeDescriptions&dropdownDiv='+divName + '&functionName='+functionName + '&segment='+segment;
				fnb.functions.loadDropDownDiv.load(url,divName,null);
			}
		},
		
		/**
		 * Makes an ajax call to populate dropdown
		 * reasonWithProperties in Format "101 -01:N;N;N;N;N;N;N;N;N;N;N;N;N;N;N"
		 */
		dropDownAjaxCallOnLoad : function(reason, divName, ddValue, segment, reasonWithProperties) {
			if(reason != 0 && reason != "")
			{
				var parent = this;
				parent.showExtraFields(reasonWithProperties);
				
				parent.dropDownAjaxCall(reason, divName, ddValue, segment);
		
				
			}
		},	
	
		resetExtraFields : function() {
			
			/*document.getElementById("loanReferenceNumber").value = "";
			document.getElementById("loanInterestRate").value = "";
			document.getElementById("sarbAuthReferenceNumber").value = ""; 
			document.getElementById("sarbAuthApplicationNumber").value = ""; 
			document.getElementById("loanTenor").value = "";
			document.getElementById("invoiceNumber").value = "";
			document.getElementById("importControlNumber").value = "";
			document.getElementById("transportDocumentNumber").value = "";
			document.getElementById("thirdPartyLegalEntityTaxClearanceCertificateRef").value = "";
			document.getElementById("thirdPartyLegalEntityCustomsClientNumber").value = "";
			document.getElementById("thirdPartyLegalEntityTaxNumber").value = "";
			document.getElementById("thirdPartyLegalEntityVatNumber").value = "";*/
			
			document.getElementById("transactionDescriptionSelected").value = "";

		},
		
		showExtraFields : function(singlDesc){
			
			singlDesc = singlDesc.substring(singlDesc.indexOf(':')+1, singlDesc.length);
			
			options = singlDesc.split(";");
						
			if(options!=null && options.length==15){
				
				/**
				 * Caters for both BOPCUS 2 and BOPCUS 3
				 * 'Y' & 'N' - BOPCUS 2
				 * 'M', 'O', & 'N' - BOPCUS 3
				 */
				var FRCCN = options[0].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[0].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[0].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRMRN = options[1].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[1].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[1].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRTAX = options[2].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[2].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[2].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var  FRVAT = options[3].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[3].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[3].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRLOANRFN = options[4].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[4].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[4].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRSARB = options[5].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[5].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[5].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FREXPCNO = options[6].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[6].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[6].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRINVNO = options[7].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[7].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[7].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRTAXCRTIND = options[8].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[8].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[8].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRTAXCRTREFNO = options[9].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[9].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[9].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRLOANTENOR = options[10].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[10].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[10].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRLOANINT = options[11].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[11].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[11].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRSARBREF = options[12].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[12].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[12].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRTRANDOCNO = options[13].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[13].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[13].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
				var FRTRDPART = options[14].replace(/^\s+|\s+$/g, '') == "Y" ? "true" : (options[14].replace(/^\s+|\s+$/g, '') == "M" ? "true" : (options[14].replace(/^\s+|\s+$/g, '') == "O" ? "true" : "false"));
								
				fnb.functions.showHideToggleElements.showHideToggle([{show:FRLOANRFN,element:'#searchWizzard5a'},
													                   {show:FRLOANINT,element:'#searchWizzard5b'},	
													                   {show:FRMRN,element:'#searchWizzard5c'},	
													                   {show:FRINVNO,element:'#searchWizzard4g'},
													                   {show:FRLOANTENOR,element:'#searchWizzard4d'},
													                   {show:FRTAXCRTREFNO,element:'#searchWizzard4e'},
													                   {show:FRTRANDOCNO,element:'#searchWizzard4f'},
													                   {show:FRSARB,element:'#searchWizzard6'},
													                   {show:FRSARBREF,element:'#searchWizzard7'},													                   
													                   {show:FRCCN,element:'#searchWizzard8'},
														               {show:FRTAX,element:'#searchWizzard9'},
														               {show:FRVAT,element:'#searchWizzard10'}]);
				
			}else{ //No Selection default all to false.
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#searchWizzard5a'},
													                   {show:'false',element:'#searchWizzard5b'},	
													                   {show:'false',element:'#searchWizzard5c'},	
													                   {show:'false',element:'#searchWizzard4d'},
													                   {show:'false',element:'#searchWizzard4e'},
													                   {show:'false',element:'#searchWizzard4f'},
													                   {show:'false',element:'#searchWizzard4g'},
													                   {show:'false',element:'#searchWizzard6'},
													                   {show:'false',element:'#searchWizzard7'},
													                   {show:'false',element:'#searchWizzard8'},
													                   {show:'false',element:'#searchWizzard9'},
					                              	                   {show:'false',element:'#searchWizzard10'}]);
			}
			
		},
		setExtraFieldsOnLoad : function(segment, reasonWithProperties){

			var singlDesc = $("#initialDescription").val().replace(/^\s+|\s+$/g, '');
			if(singlDesc != null && singlDesc == "-1"){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#searchWizzard2'}]);	
				var parent = this;
				var singlReason = $("#transactionReasonSelected").val().replace(/^\s+|\s+$/g, ''); 
				var divName = "singlAltDescDIV";
				if(singlReason != null && singlDesc != "" && singlDesc != "0"){
					var altDescSelected = $("#transactionDescriptionSelected").val().replace(/^\s+|\s+$/g, '');
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#searchWizzard3'}]);	
					parent.dropDownAjaxCallOnLoad(singlReason, divName, altDescSelected, segment, reasonWithProperties);
				}
			}

		},

		/**
		 * Show extra fields for a bopcode
		 * Invoked by method call dropDownAjaxCall ==> ForexPaymentReceivedDropDownBuilderNavigator.java 
		 */	
		 showAdditionalFields : function(obj) {
			var parent = this;
			
			parent.resetExtraFields();
			
			var val=$(obj).attr('data-value');
			
			if(val != null && val != "0" && val != ""){
				
				document.getElementById("transactionDescriptionSelected").value = val;
				
				parent.showExtraFields(val);

			}
		}

	}
	namespace("fnb.forex.simple.ApplicationForGlobalPaymentAddReason",genericPageObject); 
	
});


