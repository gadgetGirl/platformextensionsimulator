$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
						
		}, 

		pageLoaded : function() {
			var parent = this;
			
			var thirdPartyIndividualIdType = parent.configObject.thirdPartyIndividualIdType;
			var personSelected = parent.configObject.personSelected;
			var thirdPartySelected = parent.configObject.thirdPartySelected;
			var postalSameAsResidential = parent.configObject.postalSameAsResidential;
			var thirdPartyResidentialCountryCode = parent.configObject.thirdPartyResidentialCountryCode
			var thirdPartyPostalCountryCode = parent.configObject.thirdPartyPostalCountryCode
			
			parent.displayExtraFields(thirdPartyIndividualIdType, personSelected, thirdPartySelected, postalSameAsResidential, thirdPartyResidentialCountryCode, thirdPartyPostalCountryCode);

			
		},
		
		displayExtraFields : function(thirdPartyIndividualIdType, personSelected, thirdPartySelected, postalSameAsResidential, thirdPartyResidentialCountryCode, thirdPartyPostalCountryCode) {
			
			var rsaID = "false";
			var passportSelect = "false";
			var tempResPermit = "false";
			
			var thirdPartyContainer = "false";
			var individualContainer2 = "false";
			var individualDetails = "false";
			var companyDetails = "false";
			var radioClicked = "false";
			var postalAddressContainer = "false";
			var thirdPartyResidentialProvinceDropDownContainer = "false";
			var thirdPartyResidentialProvinceInputContainer = "false";
			var thirdPartyPostalProvinceDropDownContainer = "false";
			var thirdPartyPostalProvinceInputContainer = "false";			
			var helperVariables = "false";
			
			if (thirdPartyIndividualIdType != null && thirdPartyIndividualIdType != "" && thirdPartyIndividualIdType != "0")
			{
				if (thirdPartyIndividualIdType == "RSAID")
				{
					rsaID = "true";
					passportSelect = "false";
					tempResPermit = "false";
				}	
				else if (thirdPartyIndividualIdType == "PASSPT")
				{
					rsaID = "false";
					passportSelect = "true";
					tempResPermit = "false";
				}	
				else if (thirdPartyIndividualIdType == "TEMPR")
				{
					rsaID = "false";
					passportSelect = "false";
					tempResPermit = "true";

				}
			}	
			
			if (personSelected != null)
			{
				if (personSelected == "Myself")
				{
					thirdPartyContainer = "false";
				}	
				else if (personSelected == "Third Party")
				{
					thirdPartyContainer = "true";
				}	
			}	
				
			if (thirdPartySelected != null)
			{
				if (thirdPartySelected == "Individual")
				{
					individualContainer2 = "true";
					individualDetails = "true";
					companyDetails = "false";
					radioClicked = "true";
				}	
				else if (thirdPartySelected == "Company")
				{
					individualContainer2 = "false";
					individualDetails = "false";
					companyDetails = "true";
					radioClicked = "true";
				}				
			}
			
			if (thirdPartyResidentialCountryCode != null && thirdPartyResidentialCountryCode != "" && thirdPartyResidentialCountryCode != "0")
			{
				if (thirdPartyResidentialCountryCode == 'ZA')
				{
					thirdPartyResidentialProvinceDropDownContainer = "true";
					thirdPartyResidentialProvinceInputContainer = "false";
					document.getElementById("thirdPartyResidentialProvinceInput").value = "";
				}
				else
				{
					thirdPartyResidentialProvinceDropDownContainer = "false";
					thirdPartyResidentialProvinceInputContainer = "true";
					document.getElementById("thirdPartyResidentialProvinceDropDown").value = "0";
				}	
			}	
			
			if (thirdPartyPostalCountryCode != null && thirdPartyPostalCountryCode != "" && thirdPartyPostalCountryCode != "0")
			{
				if (thirdPartyPostalCountryCode == 'ZA')
				{
					thirdPartyPostalProvinceDropDownContainer = "true";
					thirdPartyPostalProvinceInputContainer = "false";
					document.getElementById("thirdPartyPostalProvinceInput").value = "";
				}
				else
				{
					thirdPartyPostalProvinceDropDownContainer = "false";
					thirdPartyPostalProvinceInputContainer = "true";
					document.getElementById("thirdPartyPostalProvinceDropDown").value = "0";
				}	
			}			
			
			if (postalSameAsResidential != null && (postalSameAsResidential == 'false' || postalSameAsResidential == "false" || postalSameAsResidential == false))
			{
				postalAddressContainer = "true";
			}	
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:rsaID,element:'#rsaID'},
				                              	                   {show:passportSelect,element:'#passportSelect'},
				                              	                   {show:tempResPermit,element:'#tempResPermit'},
			                                                       {show:thirdPartyContainer,element:'#thirdPartyContainer'},
				                              	                   {show:individualContainer2,element:'#individualContainer2'},
				                              	                   {show:individualDetails,element:'#individualDetails'},
				                              	                   {show:companyDetails,element:'#companyDetails'},
				                              	                   {show:radioClicked,element:'#radioClicked'},
				                              	                   {show:thirdPartyResidentialProvinceDropDownContainer,element:'#thirdPartyResidentialProvinceDropDownContainer'},
				                              	                   {show:thirdPartyResidentialProvinceInputContainer,element:'#thirdPartyResidentialProvinceInputContainer'},
				                              	                   {show:thirdPartyPostalProvinceDropDownContainer,element:'#thirdPartyPostalProvinceDropDownContainer'},
				                              	                   {show:thirdPartyPostalProvinceInputContainer,element:'#thirdPartyPostalProvinceInputContainer'},				                              	                   
				                              	                   {show:postalAddressContainer,element:'#postalAddressContainer'},
				                              	                   {show:helperVariables,element:'#helperVariables'}]);
			
		},
		
		showIDOrPassportOrTempResNo : function(obj) {
			
			var val=$(obj).attr('data-value');
			
			if (val != null && val != "" && val != "0")
			{
				if (val == "RSAID")
				{
					document.getElementById("thirdPartyIndividualPassportNumber").value = "";
					document.getElementById("thirdPartyIndividualPassportCountry").value = "0";
					document.getElementById("thirdPartyIndividualTemporaryResidencePermitNumber").value = "";
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#rsaID'},
						                              	                   {show:'false',element:'#passportSelect'},
						                              	                   {show:'false',element:'#tempResPermit'}]);
				}	
				else if (val == "PASSPT")
				{
					document.getElementById("thirdPartyIndividualRsaIdNumber").value = "";
					document.getElementById("thirdPartyIndividualTemporaryResidencePermitNumber").value = "";
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#rsaID'},
						                              	                   {show:'true',element:'#passportSelect'},
						                              	                   {show:'false',element:'#tempResPermit'}]);
				}	
				else if (val == "TEMPR")
				{
					document.getElementById("thirdPartyIndividualRsaIdNumber").value = "";
					document.getElementById("thirdPartyIndividualPassportNumber").value = "";
					document.getElementById("thirdPartyIndividualPassportCountry").value = "0";
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#rsaID'},
						                              	                   {show:'false',element:'#passportSelect'},
						                              	                   {show:'true',element:'#tempResPermit'}]);
				}					
					
			}	
			
		},
		
		resetPostalFields : function(postalSameAsResidential) {
			
			
			if (postalSameAsResidential != null)
			{	
				if (postalSameAsResidential == 'true' || postalSameAsResidential == true)
				{
					/*document.getElementById("thirdPartyPostalAddressLine1").value = document.getElementById("thirdPartyResidentialAddressLine1").value;
					document.getElementById("thirdPartyPostalAddressLine2").value = document.getElementById("thirdPartyResidentialAddressLine2").value;
					document.getElementById("thirdPartyPostalSuburb").value = document.getElementById("thirdPartyResidentialSuburb").value;
					document.getElementById("thirdPartyPostalCity").value = document.getElementById("thirdPartyResidentialCity").value; 
					document.getElementById("thirdPartyPostalProvince").value = document.getElementById("thirdPartyResidentialProvince").value;
					document.getElementById("thirdPartyPostalPostalCode").value = document.getElementById("thirdPartyResidentialPostalCode").value; 
					document.getElementById("thirdPartyPostalCountryCode").value = document.getElementById("thirdPartyResidentialCountryCode").value;*/
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#postalAddressContainer'}]);
	
				}
				else if (postalSameAsResidential == 'false' || postalSameAsResidential == false)
				{
					/*document.getElementById("thirdPartyPostalAddressLine1").value = "";
					document.getElementById("thirdPartyPostalAddressLine2").value = "";
					document.getElementById("thirdPartyPostalSuburb").value = "";
					document.getElementById("thirdPartyPostalCity").value = ""; 
					document.getElementById("thirdPartyPostalProvince").value = "";
					document.getElementById("thirdPartyPostalPostalCode").value = ""; 
					document.getElementById("thirdPartyPostalCountryCode").value = "";*/
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#postalAddressContainer'}]);
	
				}
			}	
			
		},
		
		resetThirdPartyFields : function(thirdPartySelected) {
			
			
			if (thirdPartySelected == "Individual")
			{
				document.getElementById("thirdPartyLegalEntityName").value = "";
				document.getElementById("thirdPartyLegalEntityRegistrationNumber").value = "";
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#individualContainer2'},
				                                                     {show:'true',element:'#individualDetails'},
				                                                     {show:'false',element:'#companyDetails'},
				                                                     {show:'true',element:'#radioClicked'}]);
			}	
			else if (thirdPartySelected == "Company")
			{
				document.getElementById("thirdPartyIndividualSurname").value = "";
				document.getElementById("thirdPartyIndividualFirstName").value = "";
				document.getElementById("thirdPartyIndividualGender").value = "0";
				document.getElementById("thirdPartyIndividualDateOfBirth").value = ""; 
				document.getElementById("thirdPartyIndividualRsaIdNumber").value = "";
				document.getElementById("thirdPartyIndividualIdType").value = "";
				document.getElementById("thirdPartyIndividualTemporaryResidencePermitNumber").value = "";
				document.getElementById("thirdPartyIndividualPassportNumber").value = ""; 
				document.getElementById("thirdPartyIndividualPassportCountry").value = "0";
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#individualContainer2'},
				                                                     {show:'false',element:'#individualDetails'},
				                                                     {show:'true',element:'#companyDetails'},
				                                                     {show:'true',element:'#radioClicked'}]);

			}
			
		},
		
		onThirdPartyResidentialCountryChange : function(obj) {
			
			var country=$(obj).attr('data-value');
			
			if (country != null && country != "" && country != "0")
			{	
				if (country == 'ZA')
				{
					document.getElementById("thirdPartyResidentialProvinceInput").value = "";
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#thirdPartyResidentialProvinceDropDownContainer'},
					                                                     {show:'false',element:'#thirdPartyResidentialProvinceInputContainer'}]);
				}
				else
				{
					document.getElementById("thirdPartyResidentialProvinceDropDown").value = "0";
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#thirdPartyResidentialProvinceDropDownContainer'},
					                                                     {show:'true',element:'#thirdPartyResidentialProvinceInputContainer'}]);
				}	
			}
			
		},
		
		onThirdPartyPostalCountryChange : function(obj) {
			
			var country=$(obj).attr('data-value');
			
			if (country != null && country != "" && country != "0")
			{	
				if (country == 'ZA')
				{
					document.getElementById("thirdPartyPostalProvinceInput").value = "";
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#thirdPartyPostalProvinceDropDownContainer'},
					                                                     {show:'false',element:'#thirdPartyPostalProvinceInputContainer'}]);
				}
				else
				{
					document.getElementById("thirdPartyPostalProvinceDropDown").value = "0";
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#thirdPartyPostalProvinceDropDownContainer'},
					                                                     {show:'true',element:'#thirdPartyPostalProvinceInputContainer'}]);
				}	
			}
			
		}		
	}
	namespace("fnb.forex.simple.ForexPaymentReceivedAddPerson",genericPageObject); 
	
});


