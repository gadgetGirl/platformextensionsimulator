
$(function() {
	function genericPageObject() {
		this.configObject = {};
		
		this.thirdPartyFullNameIdentifier = ".col3 .tableCellItem";
		this.thirdPartyUniqueIdentifierIdentifier = ".col4 .tableCellItem";
		this.thirdPartyCreationTimeStampIdentifier = ".col5 .tableCellItem";
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
			parent.bindEvents();
						
		},
		payerCountrySelectPhysical : function(me) {
			
			var countryValue = $(me).attr('data-value');
			if(countryValue == 'ZA'){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#payerProvinceDropdownPhysical'},{show:'false',element:'#payerProvinceInputPhysical'}]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#payerProvinceDropdownPhysical'},{show:'true',element:'#payerProvinceInputPhysical'}]);
			}
			
		},
		payerCountrySelectPostal : function(me) {
			
			var countryValue = $(me).attr('data-value');
			if(countryValue == 'ZA'){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#payerProvinceDropdownPostal'},{show:'false',element:'#payerProvinceInputPostal'}]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#payerProvinceDropdownPostal'},{show:'true',element:'#payerProvinceInputPostal'}]);
			}
			
		},
		recipientCountrySelectPhysical : function(me) {
			
			var countryValue = $(me).attr('data-value');
			if(countryValue == 'ZA'){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#recipientProvinceDropdownPhysical'},{show:'false',element:'#recipientProvinceInputPhysical'}]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#recipientProvinceDropdownPhysical'},{show:'true',element:'#recipientProvinceInputPhysical'}]);
			}
			
		},

		pageLoaded : function() {

			var parent = this;
		
			if(parent.configObject.companyOrIndividual != "") {
							
				if(parent.configObject.companyOrIndividual == 'I' || parent.configObject.companyOrIndividual == 'i'){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#companyTitle'},{show:'true',element:'#individualTitle'}]);	
				}
				else{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#companyTitle'},{show:'false',element:'#individualTitle'}]);
				}
			}
			
			var val = parent.configObject.iban;
			if(val != "") {
				if(val == "I" || val == "i" || val == "iban"){
					$("#accNumberContainer .formElementLabel").html("IBAN Number");	
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#branchCodeContainer'}]);	
				}
				else if(val == "A" || val == "a" || val == "acc"){
					$("#accNumberContainer .formElementLabel").html("Recipient Account");
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#branchCodeContainer'}]);
				}
				else if(val == "N" || val == "n"){
					$("#accNumberContainer .formElementLabel").html("NIB Number");
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#branchCodeContainer'}]);
				}
			}
			
			if(parent.configObject.preBookedDeal == 'y'){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#dealNumberDiv'}]);
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#dealNumberDiv'}]);
			}
			
			if(parent.configObject.dealtCurrency != '' && parent.configObject.dealtCurrency != '0'){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#currencyDiv'}]);
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#currencyDiv'}]);
			}
					
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#bicCodeSearch'}]);
		
		},
		
		showAccountType : function(obj){
			if (!$(obj).closest('#isAccountNumberIbanNib').hasClass('disabled')){
				var parent = this;
				var val=$(obj).attr('data-value');
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#accountIbanNibContainer'}]);
				
				if(val == "I"){
					console.log(val);
					$("#accountNumberIbanNibContainer .formElementLabel").html("IBAN Number");	
					$("#isAccountNumberIbanNib").attr("name", val);	
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#recipientBranchCodeContainer'}]);
				}
				else if(val == "A"){
					console.log(val);
					$("#accountNumberIbanNibContainer .formElementLabel").html("Recipient Account");
					$("#isAccountNumberIbanNib").attr("name", val);	
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#recipientBranchCodeContainer'}]);
				}
				else if(val == "N"){
					console.log(val);
					$("#accountNumberIbanNibContainer .formElementLabel").html("NIB Number");
					$("#isAccountNumberIbanNib").attr("name", val);	
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#recipientBranchCodeContainer'}]);
				}
			}
			
		},
		
		individualClick : function(obj){
			if (!$(obj).closest('#customerType').hasClass('disabled')){
				console.log('individualClick');
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#companyTitle'},{show:'true',element:'#individualTitle'}])
			}
		},
		
		companyClick : function(obj){
			if (!$(obj).closest('#customerType').hasClass('disabled')){
				console.log('companyClick');
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#companyTitle'},{show:'false',element:'#individualTitle'}])
			}
		},
		
		accountSelect : function(test, chargeCodeDropdown1){
			var accountLabel = $(test).find(".dropdown-h4").html();
			var msg = "";
			if ($(test).attr("data-value") < 0) {				
				alertO(msg);
				return false;
			} else {				
				$("#accountNameContainer").show();
				$("#accountDropdownContainer").hide();
				$("#infoContainer").show();
				$("#accountNameContainer .formElementContainer").html(accountLabel);
				$('#origNote').hide();
				var dropId1 = chargeCodeDropdown1;
				fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=forex.simple.navigator.ApplicationForGlobalPaymentGetCISInfo&forexAccountSelected='  +$(test).attr("data-value"),'test');
				fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=forex.simple.navigator.ApplicationForGlobalPaymentChargeCodes&forexAccountSelected='+$(test).attr("data-value") + '&dropId1=' + dropId1,'chargeCodesContainer1');
			}
		},
		
		currencySelect : function(me){
			var currency = $(me).attr("data-value");
			if(currency != "0"){
				$("#currencyChooseContainer").show();
				var radioItem = $("#dealtCurrencyRadio")
				radioItem.html(currency);
				radioItem.attr("data-value", currency);
				radioItem.trigger('click')
				var radioItemParent = radioItem.parent().parent();
				radioItemParent.find('input').attr('value',currency);
				fnb.forms.radioButtons.radioAction(radioItem);
				this.currencyRadioSelect(me);
				
			}
//			$("#specifiedAmount").attr("name", currency); we dont want to change the name, because the name of the html element is mapped to the validation viewbean by name
//			$("#GRBContainer .formElementLabel").html("Amount in " + currency); #GRBContainer??? The "Amount in currency" label is catered for in the currencyRadioSelect() function below
		},
		
		currencyRadioSelect : function(me){
			var currency = $(me).attr("data-value");
			$("#specifiedAmountContainer").show();
			$("#specifiedAmountContainer .formElementLabel").html("Specified amount ("+currency+")");
//			$("#specifiedAmount").attr("name", currency); we dont want to change the name, because the name of the html element is mapped to the bean by name
		},
		
		setSelectedAccount :function(obj) {
			var parent = this;
			var val=$(obj).attr('data-value');
			var reference = parent.configObject.linkSequenceNumber;
			var action= 'changeAccount';
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: '/banking/Controller?nav=forex.simple.navigator.PayGlobalRecipientCapture&anrfn='+val+'&action=changeAccount&reference='+reference})
		},
		
		getDropdownValue : function(obj, dropDownType, divName, target){
			var parent = this;
			var result = target + "";
			var val=$(obj).attr('data-value');
			var name_space = parent.configObject.nameSpace;
	
			fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=forex.simple.navigator.ForexBICSearchDropdown&dropDownType=' + dropDownType + '&nameSpace=' + name_space + '&dropDownValue='+val + '&resultDiv=' + result,divName);

		},

		 changeValue : function(obj, target){
			
			var value = $(obj).attr('data-value');
			document.getElementById(target).value = value;
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#bicCodeSearch'}]);
		},

		 forexUpdateCurSelector : function(obj, target){
			 fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#currencyDiv'}]);
			var value = $(obj).attr('data-value');
			document.getElementById(target).innerHTML = value;
			
			//$('#' + target).attr('data-value',value); Turns out I didn't need it...but usefull for future reference
		},
		
		/**
		 * Makes a ajax call to populate CIS Info
		 */
		populateCISInfoAjaxCall : function(obj){
			var val=$(obj).attr('data-value');
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: '/banking/Controller?nav=forex.simple.navigator.ApplicationForGlobalPaymentGetCISInfoNavigator&forexAccountSelected=' + val})			
		},
		
		addBOPMonetaryDetailsToThirdParty : function(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp) {
			
			document.getElementById("thirdPartyToAddToEditOrDeleteFullName").value = thirdPartyFullName;
			document.getElementById("thirdPartyToAddToEditOrDeleteUniqueIdentifier").value = thirdPartyUniqueIdentifier;
			document.getElementById("thirdPartyToAddToEditOrDeleteCreationTimeStamp").value = thirdPartyCreationTimeStamp;
			document.getElementById("actionToBePerformed").value = "ADD";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			
			fnb.functions.submitFormToEziPanel.submit('applicationForGlobalPayment', 'nav','forex.simple.navigator.ApplicationForGlobalPaymentAddReason');
			
		},
		
		addThirdParty : function(myselfOrMyCompanyAdded) {
			
			document.getElementById("actionToBePerformed").value = "ADD";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			
			if (myselfOrMyCompanyAdded != null && (myselfOrMyCompanyAdded == 'true' || myselfOrMyCompanyAdded == true))
			{
				document.getElementById("defaultPersonSelection").value = "Third Parties";
			}
			else
			{	
				document.getElementById("defaultPersonSelection").value = "";
			}
			
			fnb.functions.submitFormToEziPanel.submit('applicationForGlobalPayment', 'nav', 'forex.simple.navigator.ApplicationForGlobalPaymentAddPerson');
			
		},
		
		addPaymentSentOnBehalfThirdParty : function(who) {
			if (who != null)
			{	
				if(who == "Myself" || who == "My Company")
				{
					document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "ADD";
					document.getElementById("reasonEziSliderLastActionPerformed").value = "";
					document.getElementById("actionToBePerformed").value = "";
					
					document.getElementById("personSelected").value = who;
					
					document.getElementById("paymentSentBehalfOf").value = who;
					
					document.getElementById("nav").value = "forex.simple.navigator.ApplicationForGlobalPaymentCapture";
					
					fnb.functions.submitFormToWorkspace.submit('applicationForGlobalPayment');
				}	
				else	
				{	
				
					document.getElementById("actionToBePerformed").value = "ADD";
					document.getElementById("reasonEziSliderLastActionPerformed").value = "";
					document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
					
					document.getElementById("paymentSentBehalfOf").value = who;
					

					document.getElementById("defaultPersonSelection").value = who;
					
					fnb.functions.submitFormToEziPanel.submit('applicationForGlobalPayment', 'nav', 'forex.simple.navigator.ApplicationForGlobalPaymentAddPerson');
				}	
			}	
		},
		
		editBOPMonetaryDetailsForAThirdParty : function(obj) {
			
			document.getElementById("actionToBePerformed").value = "EDIT";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			
			fnb.functions.submitFormToEziPanel.submit('applicationForGlobalPayment', 'nav','forex.simple.navigator.ApplicationForGlobalPaymentAddReason');
			
		},
		
		deleteBOPMonetaryDetailsForAThirdParty : function() {
			
			document.getElementById("actionToBePerformed").value = "DELETE_REASON";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			
			document.getElementById("nav").value = "forex.simple.navigator.ApplicationForGlobalPaymentCapture";
			
			fnb.functions.submitFormToWorkspace.submit('applicationForGlobalPayment');
			
		},
		
		editThirdParty : function(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp) {
			
			document.getElementById("thirdPartyToAddToEditOrDeleteFullName").value = thirdPartyFullName;
			document.getElementById("thirdPartyToAddToEditOrDeleteUniqueIdentifier").value = thirdPartyUniqueIdentifier;
			document.getElementById("thirdPartyToAddToEditOrDeleteCreationTimeStamp").value = thirdPartyCreationTimeStamp;
			
			document.getElementById("actionToBePerformed").value = "EDIT";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			
			fnb.functions.submitFormToEziPanel.submit('applicationForGlobalPayment', 'nav','forex.simple.navigator.ApplicationForGlobalPaymentAddPerson');
			
		},
		
		removeThirdParty : function(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp) {
			
			document.getElementById("thirdPartyToAddToEditOrDeleteFullName").value = thirdPartyFullName;
			document.getElementById("thirdPartyToAddToEditOrDeleteUniqueIdentifier").value = thirdPartyUniqueIdentifier;
			document.getElementById("thirdPartyToAddToEditOrDeleteCreationTimeStamp").value = thirdPartyCreationTimeStamp;
			
			document.getElementById("actionToBePerformed").value = "DELETE_THIRD_PARTY";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			
			document.getElementById("nav").value = "forex.simple.navigator.ApplicationForGlobalPaymentCapture";
			
			fnb.functions.submitFormToWorkspace.submit('applicationForGlobalPayment');
			
		},
		
		/**
		 * Binds the onclick events to the buttons for the custom built table - look at ForexPaymentReceivedCaptureForexBopMonetaryDetailsResolver.java
		 */
		bindEvents: function() {
			var parent = this;
			var thirdPartiesWithReasonsTable = $(".tableContainer");
			thirdPartiesWithReasonsTable.on('click','.eziLink',
					function(eventObject)
					{
						var o = $(eventObject.target);
												
						var thirdPartyFullName = o.closest(".tableRow").find(parent.thirdPartyFullNameIdentifier).text().replace(/^\s+|\s+$/g, ''); 
						var thirdPartyUniqueIdentifier = o.closest(".tableRow").find(parent.thirdPartyUniqueIdentifierIdentifier).text().replace(/^\s+|\s+$/g, ''); 
						var thirdPartyCreationTimeStamp = o.closest(".tableRow").find(parent.thirdPartyCreationTimeStampIdentifier).text().replace(/^\s+|\s+$/g, '');
						
						$("#thirdPartyToAddToEditOrDeleteFullName").val(thirdPartyFullName);
						$("#thirdPartyToAddToEditOrDeleteUniqueIdentifier").val(thirdPartyUniqueIdentifier);
						$("#thirdPartyToAddToEditOrDeleteCreationTimeStamp").val(thirdPartyCreationTimeStamp);
						
						if (o.text().replace(/^\s+|\s+$/g, '') == "Edit")
						{
							parent.editBOPMonetaryDetailsForAThirdParty();
						}							
						else if (o.text().replace(/^\s+|\s+$/g, '') == "Remove")
						{
							parent.deleteBOPMonetaryDetailsForAThirdParty();
						}	
					});		
	
		},
		swiftSearch: function() {
			fnb.functions.submitFormToEziPanel.submit('applicationForGlobalPayment', 'nav', 'forex.simple.navigator.ForexSwiftSearch');
		},
		swiftSearch: function() {
			fnb.functions.submitFormToEziPanel.submit('applicationForGlobalPayment', 'nav', 'forex.simple.navigator.ForexSwiftSearch');
		},
		showDealNumber : function(showDeal) {
			if (showDeal == true) {
				$("#dealNumber").val("");
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#prebookedDealContainer'}]);
			}
			else {
				$("#dealNumber").val("");
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#prebookedDealContainer'}]);
			}
		},
	}
	namespace("fnb.forex.simple.ApplicationForGlobalCapture",genericPageObject); 
	
});


