/**
 * Change navigation and force form submit
 */
var setNavigation = function(navigateTo){
	if(navigateTo == 'ORDER'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexRatesQuoteOrder";
	}else if(navigateTo == 'CALCULATE'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexRatesQuoteCalculator";
	}else if(navigateTo == 'CANCEL'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexRatesQuoteDecline";
	}
	
}