/**
 * Change navigation and force form submit
 */
var setNavigation = function(navigateTo){
	if(navigateTo == 'CONTINUE'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexRatesQuoteAllocation";
		document.getElementById("action").value = "continue";
	}else if(navigateTo == 'BACK'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexRatesQuoteOrder";
		document.getElementById("action").value = "back";
	}else if(navigateTo == 'CANCEL'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexRatesQuoteDecline";
		document.getElementById("action").value = "cancel";
	}
}
