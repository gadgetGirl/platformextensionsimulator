
var bopCode304 = "304";
var bopCode101 = "101";
var bopCode501 = "501"

var submitEditForm = function(){
	$("#nav").val("forex.simple.navigator.ApplicationForGlobalPaymentCapture");
	$("#action").val("back");
}	
	
/**
 * Hide All fields onLoad
 */
var hideFields = function(){
	
	if(multipleReasons != true){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#singleReasonBlock'}, 
		                   {show:'false',element:'#singleExtraFieldsBlock'}]);
	}else{
		for(var pos = 1; pos < 6; pos++){
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#reasonsContainer'+pos}]);
		}
	}
	
	fnb.functions.showHideToggleElements.showHideToggle([{show:TAX,element:'#taxDIV'},
	                   {show:VAT,element:'#vatDIV'},
	                   {show:'false',element:'#passportDIV'}]);
} 

/**
 * Reset extra fields when the bopcode changes
 */
var resetExtraFields = function(pos, bopCode){
	
	if(!multipleReasons){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#singleExtraFieldsBlock'}]);	
	}	
	
	$("#transactionRef"+pos).val("");
	$("#sarbAuthNum"+pos).val("");
	$("#exportControlNum"+pos).val("");
	$("#customsClientNum"+pos).val("");
	$("#taxNum"+pos).val("");
	$("#vatNum"+pos).val("");
	$("#locationCountry"+pos).val("0");
	$("#originalCurrency"+pos).val("");
}

/**
 * Hide the single reason block
 */
var singleReasonShow = function(obj){
	var singlDescr=$(obj).attr('data-value');
	var bopCode = singlDescr.substring(0, singlDescr.indexOf(":"));
	resetExtraFields(1, bopCode);
	showDivForSingleDescription(singlDescr);
}

/**
 * Hide the single reason block
 */
var showDivForSingleDescription = function(singlDescr){
	if(singlDescr == "-1"){ //Other
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#singleReasonBlock'}]);
	}else{
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#singleReasonBlock'}]);
		showSingleExtraFields(singlDescr);
	}
}

/**
 * Retrieves the alternative descriptions for a Reason of transaction.
 */
var getAlternativeDropdownValues = function(obj, divName, ddValue){
	resetExtraFields(1,'');
	var singlReason=$(obj).attr('data-value');
	dropDownAjaxCall(singlReason, divName, ddValue);
}

/**
 * Makes a ajax call to populate dropdown
 */
var dropDownAjaxCall = function(singlReason, divName, ddValue){
	if(singlReason != 0 && singlReason != ""){//Please Select
		var url = '/banking/Controller?nav=forex.simple.navigator.ForexPaymentReceivedDropDownBuilder&transactionReasonSelected=' + singlReason + '&type=outwardsAlternativeDescriptions&dropdownDiv='+divName;
		fnb.functions.loadDropDownDiv.load(url,divName);
	}
}

/**
 * Show extra fields for a single bopcode
 */
var showAdditionalFields = function(obj){
	var singlAltDesc=$(obj).attr('data-value');
	var bopCode = singlAltDesc.substring(0, singlAltDesc.indexOf(":"));
	resetExtraFields(1, bopCode);
	if((singlAltDesc != "0" && singlAltDesc != "")){ //Please Select
		if(bopCode == bopCode304){
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#passportDIV'}]);			
		}else{
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#passportDIV'}]);
		} 
		showSingleExtraFields(singlAltDesc);
	}
}

/**
 * Show extra fields for a single bopcode
 */
var showSingleExtraFields = function(singlAltDesc){
	var bopCode = singlAltDesc.substring(0, singlAltDesc.indexOf(":"));
	singlAltDesc = singlAltDesc.substring(singlAltDesc.indexOf(':')+1, singlAltDesc.length);
	options = singlAltDesc.split(";");
	
	//show gift terms and conditions
	var isBopCode501 = bopCode == bopCode501 ? "true" : "false";
	fnb.functions.showHideToggleElements.showHideToggle([{show:isBopCode501,element:'#giftConfirmDiV'}]);	
	
	if(options!=null && options.length==7){
		var FRCCN = options[0].trim() == "Y" ? "true" : "false" ;
		var FRMRN = options[1].trim() == "Y" ? "true" : "false" ;
		var FRVAT = options[2].trim() == "Y" ? "true" : "false" ;
		var FRTAX = options[3].trim() == "Y" ? "true" : "false" ;
		var FRLOANRFN = options[4].trim() == "Y" ? "true" : "false" ;
		var FRSARB = options[5].trim() == "Y" ? "true" : "false" ;
		var FREXPCNO = options[6].trim() == "Y" ? "true" : "false" ;
		
		//For bopCode = 101 the description must change from "Movement Reference Number" to "Invoice Number" and the input field must contain INV
		if(bopCode==bopCode101){
			$("#transactionRefDiv1 .input-label-inner").html("Invoice Number");
			$("#transactionRef1").val("INV");
		}else{
			$("#transactionRefDiv1 .input-label-inner").html("Movement Reference Number");
		}
		
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#singleExtraFieldsBlock'},
		                   {show:'true',element:'#reasonsContainer1'},
		                   {show:FRCCN,element:'#FRCCN_1'},
		                   {show:FRMRN,element:'#FRMRN_1'},
		                   {show:FRVAT,element:'#FRVAT_1'},
		                   {show:FRTAX,element:'#FRTAX_1'},
		                   {show:FRLOANRFN,element:'#FRLOANRFN_1'},
		                   {show:FRSARB,element:'#FRSARB_1'},
		                   {show:FREXPCNO,element:'#FREXPCNO_1'}]);
		
	}else{ //No Selection default all to false.
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#singleExtraFieldsBlock'},
		                   {show:'false',element:'#reasonsContainer1'},
		                   {show:'false',element:'#FRCCN_1'},
		                   {show:'false',element:'#FRMRN_1'},
		                   {show:'false',element:'#FRVAT_1'},
		                   {show:'false',element:'#FRTAX_1'},
		                   {show:'false',element:'#FRLOANRFN_1'},
		                   {show:'false',element:'#FRSARB_1'},
		                   {show:'false',element:'#FREXPCNO_1'}]);	
	}
}

/**
 * Show extra fields for a split/multiple bopcode
 */
var showMultipleExtraFields = function(obj,pos){
	
	var selectedReason=$(obj).attr('data-value');
	var bopCode = selectedReason.substring(0, selectedReason.indexOf(":"));
	resetExtraFields(pos, bopCode);
	
	if(bopCode == bopCode304){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#passportDIV'}]);			
	}else{
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#passportDIV'}]);
	}  
	
	if((selectedReason != "0" && selectedReason != "")){ //Please Select
		displayExtraFields(selectedReason, pos);
	}
}

/**
 * Show extra fields for a split/multiple bopcode
 */
var displayExtraFields = function(selectedReason, pos){
	var bopCode = selectedReason.substring(0, selectedReason.indexOf(":"));
	selectedReason = selectedReason.substring(selectedReason.indexOf(':')+1, selectedReason.length);
	options = selectedReason.split(";");
	
	var FRCCN = options[0].trim() == "Y" ? "true" : "false" ;
	var FRMRN = options[1].trim() == "Y" ? "true" : "false" ;
	var FRVAT = options[2].trim() == "Y" ? "true" : "false" ;
	var FRTAX = options[3].trim() == "Y" ? "true" : "false" ;
	var FRLOANRFN = options[4].trim() == "Y" ? "true" : "false" ;
	var FRSARB = options[5].trim() == "Y" ? "true" : "false" ;
	var FREXPCNO = options[6].trim() == "Y" ? "true" : "false" ;
	
	//For bopCode = 101 the description must change from "Movement Reference Number" to "Invoice Number" and the input field must contain INV
	if(bopCode==bopCode101){
		$("#transactionRefDiv"+pos+" .input-label-inner").html("Invoice Number");
		$("#transactionRef"+pos).val("INV");
	}else{
		$("#transactionRefDiv"+pos+" .input-label-inner").html("Movement Reference Number");
	}
	
	fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#reasonsContainer'+pos},
	                   {show:FRCCN,element:'#FRCCN_'+pos},
	                   {show:FRMRN,element:'#FRMRN_'+pos},
	                   {show:FRVAT,element:'#FRVAT_'+pos},
	                   {show:FRTAX,element:'#FRTAX_'+pos},
	                   {show:FRLOANRFN,element:'#FRLOANRFN_'+pos},
	                   {show:FRSARB,element:'#FRSARB_'+pos},
	                   {show:FREXPCNO,element:'#FREXPCNO_'+pos}]);
}
