$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
						
		},
		addPersonCountrySelectPhysical : function(me) {
			
			var countryValue = $(me).attr('data-value');
			console.log(countryValue);
			if(countryValue == 'ZA'){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#dropdownPhysical'},{show:'false',element:'#inputPhysical'}]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#dropdownPhysical'},{show:'true',element:'#inputPhysical'}]);
			}
			
		},
		addPersonCountrySelectPostal : function(me) {
			
			var countryValue = $(me).attr('data-value');
			console.log(countryValue);
			if(countryValue == 'ZA'){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#dropdownPostal'},{show:'false',element:'#inputPostal'}]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#dropdownPostal'},{show:'true',element:'#inputPostal'}]);
			}
			
		},

		pageLoaded : function() {
			var parent = this;
			
			var personSelected = parent.configObject.personSelected;
			var thirdPartySelected = parent.configObject.thirdPartySelected;
			var postalSameAsResidential = parent.configObject.postalSameAsResidential;

			parent.displayExtraFields(personSelected, thirdPartySelected, postalSameAsResidential);

			
		},
		
		displayExtraFields : function(personSelected, thirdPartySelected, postalSameAsResidential) {
			
			var thirdPartyContainer = "false";
			var individualContainer2 = "false";
			var individualDetails = "false";
			var companyDetails = "false";
			var radioClicked = "false";
			var postalAddressContainer = "false";
			var helperVariables = "false";
			
			if (personSelected != null)
			{
				if (personSelected == "Myself")
				{
					thirdPartyContainer = "false";
				}	
				else if (personSelected == "Third Party")
				{
					thirdPartyContainer = "true";
				}	
			}	
				
			if (thirdPartySelected != null)
			{
				if (thirdPartySelected == "Individual")
				{
					individualContainer2 = "true";
					individualDetails = "true";
					companyDetails = "false";
					radioClicked = "true";
				}	
				else if (thirdPartySelected == "Company")
				{
					individualContainer2 = "false";
					individualDetails = "false";
					companyDetails = "true";
					radioClicked = "true";
				}				
			}
			
			if (postalSameAsResidential != null && (postalSameAsResidential == 'false' || postalSameAsResidential == false))
			{
				postalAddressContainer = "true";
			}	
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:thirdPartyContainer,element:'#thirdPartyContainer'},
				                              	                   {show:individualContainer2,element:'#individualContainer2'},
				                              	                   {show:individualDetails,element:'#individualDetails'},
				                              	                   {show:companyDetails,element:'#companyDetails'},
				                              	                   {show:radioClicked,element:'#radioClicked'},
				                              	                   {show:postalAddressContainer,element:'#postalAddressContainer'},
				                              	                   {show:helperVariables,element:'#helperVariables'}]);
			
		},
		
		resetPostalFields : function(postalSameAsResidential) {
			
			
			if (postalSameAsResidential != null)
			{	
				if (postalSameAsResidential == 'true' || postalSameAsResidential == true)
				{
					document.getElementById("thirdPartyPostalAddressLine1").value = document.getElementById("thirdPartyResidentialAddressLine1").value;
					document.getElementById("thirdPartyPostalAddressLine2").value = document.getElementById("thirdPartyResidentialAddressLine2").value;
					document.getElementById("thirdPartyPostalSuburb").value = document.getElementById("thirdPartyResidentialSuburb").value;
					document.getElementById("thirdPartyPostalCity").value = document.getElementById("thirdPartyResidentialCity").value; 
					document.getElementById("thirdPartyPostalProvince").value = document.getElementById("thirdPartyResidentialProvince").value;
					document.getElementById("thirdPartyPostalProvinceInput").value = document.getElementById("thirdPartyResidentialProvinceInput").value;
					document.getElementById("thirdPartyPostalPostalCode").value = document.getElementById("thirdPartyResidentialPostalCode").value; 
					document.getElementById("thirdPartyPostalCountryCode").value = document.getElementById("thirdPartyResidentialCountryCode").value;
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#postalAddressContainer'}]);
	
				}
				else if (postalSameAsResidential == 'false' || postalSameAsResidential == false)
				{
					document.getElementById("thirdPartyPostalAddressLine1").value = "";
					document.getElementById("thirdPartyPostalAddressLine2").value = "";
					document.getElementById("thirdPartyPostalSuburb").value = "";
					document.getElementById("thirdPartyPostalCity").value = ""; 
					document.getElementById("thirdPartyPostalProvince").value = "";
					document.getElementById("thirdPartyPostalProvinceInput").value = "";
					document.getElementById("thirdPartyPostalPostalCode").value = ""; 
					document.getElementById("thirdPartyPostalCountryCode").value = "";
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#postalAddressContainer'}]);
	
				}
			}	
			
		},
		
		resetThirdPartyFields : function(thirdPartySelected) {
			
			
			if (thirdPartySelected == "Individual")
			{
				document.getElementById("thirdPartyLegalEntityName").value = "";
				document.getElementById("thirdPartyLegalEntityRegistrationNumber").value = "";
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#individualContainer2'},
				                                                     {show:'true',element:'#individualDetails'},
				                                                     {show:'false',element:'#companyDetails'},
				                                                     {show:'true',element:'#radioClicked'}]);
			}	
			else if (thirdPartySelected == "Company")
			{
				document.getElementById("thirdPartyIndividualSurname").value = "";
				document.getElementById("thirdPartyIndividualFirstName").value = "";
				document.getElementById("thirdPartyIndividualGender").value = "0";
				document.getElementById("thirdPartyIndividualDateOfBirth").value = ""; 
				document.getElementById("thirdPartyIndividualRsaIdNumber").value = "";
				document.getElementById("thirdPartyIndividualIdType").value = "";
				document.getElementById("thirdPartyIndividualTemporaryResidencePermitNumber").value = "";
				document.getElementById("thirdPartyIndividualPassportNumber").value = ""; 
				document.getElementById("thirdPartyIndividualPassportCountry").value = "0";
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#individualContainer2'},
				                                                     {show:'false',element:'#individualDetails'},
				                                                     {show:'true',element:'#companyDetails'},
				                                                     {show:'true',element:'#radioClicked'}]);

			}
			
		},
		passportChange : function(me) {
			var passportType = $(me).attr("data-value");
			if(passportType == "RSAID"){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#rsaID'},{show:'false',element:'#passportSelect'},{show:'false',element:'#tempResPermit'}]);
			} else if (passportType == "PASSPT") {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#rsaID'},{show:'true',element:'#passportSelect'},{show:'false',element:'#tempResPermit'}]);
			} else if (passportType == "TEMP") {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#rsaID'},{show:'false',element:'#passportSelect'},{show:'true',element:'#tempResPermit'}]);
			}
		}
	}
	namespace("fnb.forex.simple.ApplicationForGlobalPaymentAddPerson",genericPageObject); 
	
});


