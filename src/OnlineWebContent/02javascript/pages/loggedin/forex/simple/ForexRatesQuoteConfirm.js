/**
 * Change navigation and force form submit
 */
var setNavigation = function(navigateTo){
	if(navigateTo == 'CONTINUE'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexRatesQuoteResults";
	}else if(navigateTo == 'BACK'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexRatesQuoteCalculator";
		document.getElementById("action").value = "back";
	}else if(navigateTo == 'CANCEL'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexRatesQuoteDecline";
	}
}
