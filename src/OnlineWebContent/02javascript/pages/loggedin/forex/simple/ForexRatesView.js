/**
 *
 */
function currencyChanged(obj){
	var value = $(obj).attr('data-value');
	if(value=="USD" || value=="GBP" || value=="EUR"){
		alert("Please select another currency not already showing");
	}else{
		var navigateTo ="/banking/Controller?nav=forex.simple.navigator.ForexRatesView&selectedValue="+value;
		fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url:navigateTo});
	}
}

$(document).ready(function(){
	$("#currencyCode .dropdown-item").attr("onClick", "currencyChanged(this);");
});