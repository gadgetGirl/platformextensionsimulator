/**
 * Show extra fields for a split/multiple bopcode
 * This function is used by confirm and result pages
 * JSP[ForexPaymentReceivedConfirm, 
 *     ForexPaymentReceivedResult, 
 *     ForexPaymentReceivedHistoryDetail] 	
 * 
 */
function displayExtraFields(selectedReason, pos){
	var bopCode = selectedReason.substring(0, selectedReason.indexOf(":"));
	selectedReason = selectedReason.substring(selectedReason.indexOf(':')+1, selectedReason.length);
	options = selectedReason.split(";");
	
	if(options!=null && options.length == 7){
		var FRCCN = options[0].trim() == "Y" ? "true" : "false" ;
		var FRMRN = options[1].trim() == "Y" ? "true" : "false" ;
		var FRVAT = options[2].trim() == "Y" ? "true" : "false" ;
		var FRTAX = options[3].trim() == "Y" ? "true" : "false" ;
		var FRLOANRFN = options[4].trim() == "Y" ? "true" : "false" ;
		var FRSARB = options[5].trim() == "Y" ? "true" : "false" ;
		var FREXPCNO = options[6].trim() == "Y" ? "true" : "false" ;
		
		fnb.functions.showHideToggleElements.showHideToggle([{show:FRCCN,element:'#FRCCN_'+pos},
		                   {show:FRMRN,element:'#FRMRN_'+pos},
		                   {show:FRVAT,element:'#FRVAT_'+pos},
		                   {show:FRTAX,element:'#FRTAX_'+pos},
		                   {show:FRLOANRFN,element:'#FRLOANRFN_'+pos},
		                   {show:FRSARB,element:'#FRSARB_'+pos},
		                   {show:FREXPCNO,element:'#FREXPCNO_'+pos}]);
	}else{ //No Selection default all to false.
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#FRCCN_'+pos},
		                   {show:'false',element:'#FRMRN_'+pos},
		                   {show:'false',element:'#FRVAT_'+pos},
		                   {show:'false',element:'#FRTAX_'+pos},
		                   {show:'false',element:'#FRLOANRFN_'+pos},
		                   {show:'false',element:'#FRSARB_'+pos},
		                   {show:'false',element:'#FREXPCNO_'+pos}]);
	}
}
