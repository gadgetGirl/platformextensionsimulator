
var getDropdownValue = function(obj, dropDownType, divName, target){
	var result = target + "";
	var val=$(obj).attr('data-value');
	fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=forex.simple.navigator.ForexBICSearchDropdown&dropDownType=' + dropDownType + '&dropDownValue='+val + '&resultDiv=' + result,divName);

}

var changeValue = function(obj, target){
	
	var value = $(obj).attr('data-value');
	document.getElementById(target).value = value;
	fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#bicCodeSearch'}]);
}

var forexUpdateCurSelector = function(obj, target){
	var value = $(obj).attr('data-value');
	document.getElementById(target).innerHTML = value;
	//$('#' + target).attr('data-value',value); Turns out I didn't need it...but usefull for future reference
}

/**
 * Hide All fields onLoad
 */
var hideFieldsOnLoad = function(){
	var iban = $("#acountOrIBANValue").val();
	var companyOrIndividual =  $("#recipientEntityTypeValue").val();
	if(iban == "A"){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#BranchCodeBlock'}]);
	}else{
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#BranchCodeBlock'}]);
	}
	if(companyOrIndividual == "I"){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#companyTitle'},{show:'true',element:'#individualTitle'}]);	
	}else if(companyOrIndividual == "C"){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#companyTitle'},{show:'false',element:'#individualTitle'}])
	}
	
	fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#bicCodeSearc'}]);
}

$(document).ready(function(){
	hideFieldsOnLoad();
});