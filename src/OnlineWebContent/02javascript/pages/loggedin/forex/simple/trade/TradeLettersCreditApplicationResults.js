function validateForm() {
	
	
}

$(function(){
	function genericPageObject(){
		
	}
	genericPageObject.prototype = {
		validateForm: function(){
			var path = document.getElementById('theFile').value;
			var msg = "";
			var err = 0;	
			
			if(document.getElementById('docType').value == '0'){
				msg += "Please select a Document to Upload <br/>"
				err++;
			}
			if(document.getElementById('fileType').value == '0'){
				msg += "Please select a File Format <br/>";
				err++;
			}
			if (path == ""){
				msg +="Please enter a File Location <br/>";
				err++;
			} 
			
			if(err > 0){
				errorMessage = msg;
				fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
				return false;
		
			} else {
				document.getElementById('fileName').value = path;
			}
			
			fnb.functions.submitFormToWorkspace.submit("UPLOAD_TRADE_DOCUMENTS");

			return true;
		}
	}

	namespace("fnb.forex.simple.trade.TradeLettersCreditApplicationResults",genericPageObject);
});