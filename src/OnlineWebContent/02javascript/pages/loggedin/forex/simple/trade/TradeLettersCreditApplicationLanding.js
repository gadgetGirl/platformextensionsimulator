$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){
			var parent = this;
		    parent.configObject = dataSource;
		},
		destroy: function(){
			var parent = this;
			parent = null;
		},
		
		mimicRadio: function(){
			var container = '#checkGroup .checkbox-input:checkbox';
			$(container).each(function(){
				$(this).removeAttr('checked');
				var itsParent = $(this).parent();
				if(itsParent.hasClass('checked')){
					itsParent.removeClass('checked');
				}
			});
		}
	}
	
	namespace("fnb.forex.simple.trade.TradeLettersCreditApplicationLanding",genericPageObject);
});