
var bopCode304 = "304";

/**
 * Hide All fields onLoad
 */
var hideFields = function(){
	
	if(multiple != true){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#singleReasonBlock'}, 
		                   {show:'false',element:'#singleExtraFieldsBlock'}]);
	}else{
		for(var pos = 1; pos < 6; pos++){
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#reasonsContainer'+pos}]);
		}
	}
	
	var TAX = showTax ? "true" : "false";
	var VAT = showVat ? "true" : "false";	
	
	fnb.functions.showHideToggleElements.showHideToggle([{show:TAX,element:'#taxDIV'},
	                   {show:VAT,element:'#vatDIV'},
	                   {show:'false',element:'#passportDIV'}]);
} 

/**
 * pre-populates values and hides/shows fields
 */
var setExtraFieldsOnLoad = function(){
	if(multiple != true){
		var singlDescr = $("#singlDescr").val().trim();
		if(singlDescr != null && singlDescr == "-1"){
			showDivForSingleDescription(singlDescr);
			var singlReason = $("#singlReason").val().trim();
			var divName = "singlAltDescDIV";
			if(singlReason != null && singlDescr != "0"){
				var altDescSelected = $("#altDescSelected").val().trim();
				dropDownAjaxCall(singlReason, divName, altDescSelected);
			}
		}
	}else{
		for(var i = 1; i <= bopItemCount; i++){
			var selectedReason = $("#transactionReason"+i).val();
			if(selectedReason != null){
				displayExtraFields(selectedReason, i);
			}
		}
	}
}

/**
 * Reset extra fields when the bopcode changes
 */
var resetExtraFields = function(pos){
	
	if(multiple != true){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#singleExtraFieldsBlock'}]);	
	}	
	
	document.getElementById("transactionRef"+pos).value = "";
	document.getElementById("sarbAuthNum"+pos).value = ""; 
	document.getElementById("exportControlNum"+pos).value = "";
	document.getElementById("customsClientNum"+pos).value = "";
	document.getElementById("taxNum"+pos).value = "";
	document.getElementById("vatNum"+pos).value = "";
	document.getElementById("locationCountry"+pos).value = "0";
	document.getElementById("originalCurrency"+pos).value = "";
}

/**
 * Hide the single reason block
 */
var singleReasonShow = function(obj){
	resetExtraFields(1);
	var singlDescr=$(obj).attr('data-value');
	showDivForSingleDescription(singlDescr);
}

/**
 * Hide the single reason block
 */
var showDivForSingleDescription = function(singlDescr){
	if(singlDescr == "-1"){ //Other
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#singleReasonBlock'}]);		
	}else{
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#singleReasonBlock'}]);
		singlDescr = singlDescr.substring(singlDescr.indexOf(':')+1, singlDescr.length);
		showSingleExtraFields(singlDescr);
	}
}

/**
 * Retrieves the alternative descriptions for a Reason of transaction.
 */
var getAlternativeDropdownValues = function(obj, divName, ddValue){
	resetExtraFields(1);
	var singlReason=$(obj).attr('data-value');
	dropDownAjaxCall(singlReason, divName, ddValue);
}

/**
 * Makes a ajax call to populate dropdown
 */
var dropDownAjaxCall = function(singlReason, divName, ddValue){
	if(singlReason != 0 && singlReason != ""){//Please Select
		var url = '/banking/Controller?nav=forex.simple.navigator.ForexPaymentReceivedDropDownBuilder&value=' + ddValue + '&transactionReasonSelected=' + singlReason + '&type=inwardsAlternativeDescriptions&dropdownDiv='+divName;
		fnb.functions.loadDropDownDiv.load(url,divName,null);
	}
}

/**
 * Show extra fields for a single bopcode
 */
var showAdditionalFields = function(obj){
	resetExtraFields(1);
	var singlAltDesc=$(obj).attr('data-value');
	if((singlAltDesc != "0" && singlAltDesc != "")){ //Please Select
		var bopCode = singlAltDesc.substring(0, singlAltDesc.indexOf(":"));
		
		if(bopCode == bopCode304){
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#passportDIV'}]);			
		}else{
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#passportDIV'}]);
		} 
	
		singlAltDesc = singlAltDesc.substring(singlAltDesc.indexOf(':')+1, singlAltDesc.length);
		showSingleExtraFields(singlAltDesc);
	}
}

/**
 * Show extra fields for a single bopcode
 */
var showSingleExtraFields = function(singlAltDesc){

	options = singlAltDesc.split(";");
	
	if(options!=null && options.length==7){
		var FRCCN = options[0].trim() == "Y" ? "true" : "false" ;
		var FRMRN = options[1].trim() == "Y" ? "true" : "false" ;
		var FRVAT = options[2].trim() == "Y" ? "true" : "false" ;
		var FRTAX = options[3].trim() == "Y" ? "true" : "false" ;
		var FRLOANRFN = options[4].trim() == "Y" ? "true" : "false" ;
		var FRSARB = options[5].trim() == "Y" ? "true" : "false" ;
		var FREXPCNO = options[6].trim() == "Y" ? "true" : "false" ;
		
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#reasonsContainer1'},
					       {show:'true',element:'#singleExtraFieldsBlock'},
		                   {show:FRCCN,element:'#FRCCN_1'},
		                   {show:FRMRN,element:'#FRMRN_1'},
		                   {show:FRVAT,element:'#FRVAT_1'},
		                   {show:FRTAX,element:'#FRTAX_1'},
		                   {show:FRLOANRFN,element:'#FRLOANRFN_1'},
		                   {show:FRSARB,element:'#FRSARB_1'},
		                   {show:FREXPCNO,element:'#FREXPCNO_1'}]);
	}else{ //No Selection default all to false.
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#reasonsContainer1'},
						   {show:'false',element:'#singleExtraFieldsBlock'},
		                   {show:'false',element:'#FRCCN_1'},
		                   {show:'false',element:'#FRMRN_1'},
		                   {show:'false',element:'#FRVAT_1'},
		                   {show:'false',element:'#FRTAX_1'},
		                   {show:'false',element:'#FRLOANRFN_1'},
		                   {show:'false',element:'#FRSARB_1'},
		                   {show:'false',element:'#FREXPCNO_1'}]);	
	}
}

/**
 * Show extra fields for a split/multiple bopcode
 */
var showMultipleExtraFields = function(obj, pos){
	resetExtraFields(pos);
	var selectedReason=$(obj).attr('data-value');
	var bopCode = selectedReason.substring(0, selectedReason.indexOf(":"));
	
	if(bopCode == bopCode304){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#passportDIV'}]);			
	}else{
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#passportDIV'}]);
	}  
	
	selectedReason = selectedReason.substring(selectedReason.indexOf(':')+1, selectedReason.length);
	
	if((selectedReason != "0" && selectedReason != "")){ //Please Select
		displayExtraFields(selectedReason, pos);
	}
}

/**
 * Show extra fields for a split/multiple bopcode
 */
var displayExtraFields = function(selectedReason, pos){
	var bopCode = selectedReason.substring(0, selectedReason.indexOf(":"));
	selectedReason = selectedReason.substring(selectedReason.indexOf(':')+1, selectedReason.length);
	options = selectedReason.split(";");
	
	if(options!=null && options.length == 7){
		var FRCCN = options[0].trim() == "Y" ? "true" : "false" ;
		var FRMRN = options[1].trim() == "Y" ? "true" : "false" ;
		var FRVAT = options[2].trim() == "Y" ? "true" : "false" ;
		var FRTAX = options[3].trim() == "Y" ? "true" : "false" ;
		var FRLOANRFN = options[4].trim() == "Y" ? "true" : "false" ;
		var FRSARB = options[5].trim() == "Y" ? "true" : "false" ;
		var FREXPCNO = options[6].trim() == "Y" ? "true" : "false" ;
		
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#reasonsContainer'+pos},
		                   {show:FRCCN,element:'#FRCCN_'+pos},
		                   {show:FRMRN,element:'#FRMRN_'+pos},
		                   {show:FRVAT,element:'#FRVAT_'+pos},
		                   {show:FRTAX,element:'#FRTAX_'+pos},
		                   {show:FRLOANRFN,element:'#FRLOANRFN_'+pos},
		                   {show:FRSARB,element:'#FRSARB_'+pos},
		                   {show:FREXPCNO,element:'#FREXPCNO_'+pos}]);
	}else{ //No Selection default all to false.
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#reasonsContainer'+pos},
		                   {show:'false',element:'#FRCCN_'+pos},
		                   {show:'false',element:'#FRMRN_'+pos},
		                   {show:'false',element:'#FRVAT_'+pos},
		                   {show:'false',element:'#FRTAX_'+pos},
		                   {show:'false',element:'#FRLOANRFN_'+pos},
		                   {show:'false',element:'#FRSARB_'+pos},
		                   {show:'false',element:'#FREXPCNO_'+pos}]);
	}
}

/**
 * Change navigation and force form submit
 */
var setNavigation = function(navigateTo){
	if(navigateTo == 'BACK'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexPaymentReceivedCapture";
	}else if(navigateTo == 'QUOTELATER'){
		document.getElementById("nav").value = "forex.simple.navigator.ForexPaymentReceivedQuoteLaterConfirm";
	}
	
	document.getElementById("move").value = navigateTo;
}