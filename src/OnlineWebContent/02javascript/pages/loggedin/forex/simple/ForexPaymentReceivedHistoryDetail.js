
/**
 * Hide All fields onLoad
 */
var hideFields = function(){
	var TAX = showTax ? "true" : "false";
	var VAT = showVat ? "true" : "false";	
	
	fnb.functions.showHideToggleElements.showHideToggle([{show:TAX,element:'#taxDIV'},
	                   {show:VAT,element:'#vatDIV'},
	                   {show:'false', element:'#passportDIV'}]);
} 
