$(function(){
	function genericPageObject (){

	};
	genericPageObject.prototype = {
		init: function(){
			console.log('init function in forexDocumentUpload');
		}, 
		bindEvents: function(){
			
		}, 
		validateForm: function(bopCode){
			var bopCodeReportType = 'reportType_' + bopCode;
			var bopCodeFileType = 'fileType_' + bopCode;
			var bopCodeFile = 'theFile_' + bopCode;
			var fileType = $("#"+bopCodeReportType).val();
			var fileFormat = $("#"+bopCodeFileType).val();
			console.log('bopCode');
			console.log(bopCode);
			if(fileType == "0"){
				var errorMessage = 'Please select a document to upload.';
					fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
				return false;
			}
			if(fileFormat == "0"){
				var errorMessage = 'Please select a file format.';
					fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
				return false;
			}
		
			var path = document.getElementById(bopCodeFile).value;

			if (path == ""){
				var errorMessage = 'Please enter a file Path';
					fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
				return false;
			} 
			
			if (document.getElementById("multipleSubmit").value == 0)
			{
				document.getElementById("multipleSubmit").value = 1;
				return true;
			}
			else
			{			
				return false;
			}			
		},
		submitForm: function(bopCode){
			console.log('submitForm');
			var result = this.validateForm(bopCode);
			if(result){				
				console.log('submitForm function in forexDocumentUpload');
				//$('#forexDocumentUploadForm').submit();
				$('#forexDocumentUploadForm').trigger('submit');
			}
			return false;
		},
		toggle: function(id){
			var e = document.getElementById('content' + id);
			var plusSign = document.getElementById('plus_sign' + id);
			var minusSign = document.getElementById('minus_sign' + id);
			var otherElements;
			var otherPlusSigns;
			var otherMinusSigns;
			var otherBopCodes;
			var otherBopCodeReportType;
			var otherBopCodeFileType;
			var otherBopCodeFile;
			var otherBopCodeHashMapKey;
			
			if (e.style.display == 'block') { // If this div is opened close it
				e.style.display = 'none';			
				plusSign.style.display = 'block';		
				minusSign.style.display = 'none';
				
			}
			else { // If this div is closed open it & close all other divs 
				e.style.display = 'block';
				plusSign.style.display = 'none';		
				minusSign.style.display = 'block';
				
				for (var index=1; index<= 10 ; index++){ 
					if ( (index != id) && (document.getElementById('content' + index))) {
						otherElements = document.getElementById('content' + index);
						otherPlusSigns = document.getElementById('plus_sign' + index);
						otherMinusSigns = document.getElementById('minus_sign' + index);
						otherBopCodeHashMapKey =document.getElementById('bopCode'+index).value;
						
						otherElements.style.display = 'none';	
						otherPlusSigns.style.display = 'block';
						otherMinusSigns.style.display = 'none';
						
						otherBopCodeReportType = 'reportType_' + otherBopCodeHashMapKey;
						otherBopCodeFileType = 'fileType_' + otherBopCodeHashMapKey;
						otherBopCodeFile = 'theFile_' + otherBopCodeHashMapKey;
						
						document.getElementById(otherBopCodeReportType).value = '';
						document.getElementById(otherBopCodeFileType).value = '';
						document.getElementById(otherBopCodeFile).value = '';
						
						fnb.forms.dropdown.setValue($('#'+otherBopCodeReportType+'_parent'),'Please Select','');
						fnb.forms.dropdown.setValue($('#'+otherBopCodeFileType+'_parent'),'Please Select','');
					}
				} 
			}
		}
	}
	
	namespace("fnb.forex.DocumentUpload", genericPageObject);

});

