$(function() {
	function genericPageObject() {
		this.configObject = {};
		
		this.thirdPartyFullNameIdentifier = ".col3 .tableCellItem";
		this.thirdPartyUniqueIdentifierIdentifier = ".col4 .tableCellItem";
		this.thirdPartyCreationTimeStampIdentifier = ".col5 .tableCellItem";
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			
			parent.configObject = dataSource;
			parent.pageLoaded();
			parent.bindEvents();
						
		}, 

		pageLoaded : function() {
			var parent = this;
			var reccuringInstruction = parent.configObject.reccuringInstruction;
			var hasDealorFecNumber = parent.configObject.hasDealorFecNumber;
			var senderTypeSelected = parent.configObject.senderTypeSelected;
			var residentialAddressOnly = parent.configObject.residentialAddressOnly;
			var residentialCountryCode = parent.configObject.residentialCountryCode
			var postalCountryCode = parent.configObject.postalCountryCode

			parent.displayExtraFields(reccuringInstruction, hasDealorFecNumber, senderTypeSelected, residentialAddressOnly, residentialCountryCode, postalCountryCode);

			
		},
		
		displayExtraFields : function(reccuringInstruction, hasDealorFecNumber, senderTypeSelected, residentialAddressOnly, residentialCountryCode, postalCountryCode) {
			
			var automatedContainer = "false";
			var prebookedContainer = "false";
			var individualContainer = "false";
			var companyContainer = "false";
			var postalAddres = "true";
			var residentialProvinceDropDownContainer = "false";
			var residentialProvinceInputContainer = "false";
			var postalProvinceDropDownContainer = "false";
			var postalProvinceInputContainer = "false";			
			var helperVariables = "false";
			
			if (reccuringInstruction != null && (reccuringInstruction == 'true' || reccuringInstruction == true))
			{
				automatedContainer = "true";
			}
			
			if (hasDealorFecNumber != null && (hasDealorFecNumber == 'true' || hasDealorFecNumber == true))
			{
				prebookedContainer = "true";
			}
			
			if (senderTypeSelected != null)
			{
				if (senderTypeSelected == "Individual")
				{
					individualContainer = "true";
				}
				else if (senderTypeSelected == "Company")
				{
					companyContainer = "true";
				}	
			}
			
			if (residentialCountryCode != null && residentialCountryCode != "" && residentialCountryCode != "0")
			{	
				if (residentialCountryCode == 'ZA')
				{
					residentialProvinceDropDownContainer = "true";
					residentialProvinceInputContainer = "false";
					document.getElementById("residentialProvinceInput").value = "";
				}
				else
				{
					residentialProvinceDropDownContainer = "false";
					residentialProvinceInputContainer = "true";
					document.getElementById("residentialProvinceDropDown").value = "0";
				}
			}
			
			if (postalCountryCode != null && postalCountryCode != "" && postalCountryCode != "0")
			{	
				if (postalCountryCode == 'ZA')
				{
					postalProvinceDropDownContainer = "true";
					postalProvinceInputContainer = "false";
					document.getElementById("postalProvinceInput").value = "";
				}
				else
				{
					postalProvinceDropDownContainer = "false";
					postalProvinceInputContainer = "true";
					document.getElementById("postalProvinceDropDown").value = "0";
				}
			}			
			
			if (residentialAddressOnly != null && (residentialAddressOnly == 'true' || residentialAddressOnly == "true" || residentialAddressOnly == true))
			{
				postalAddres = "false";
			}
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:individualContainer,element:'#individualContainer'},
				                              	                   	{show:companyContainer,element:'#companyContainer'},
				                              	                   	{show:automatedContainer,element:'#automatedContainer'},
																	{show:prebookedContainer,element:'#prebookedContainer'},
																	{show:residentialProvinceDropDownContainer,element:'#residentialProvinceDropDownContainer'},
																	{show:residentialProvinceInputContainer,element:'#residentialProvinceInputContainer'},
																	{show:postalProvinceDropDownContainer,element:'#postalProvinceDropDownContainer'},
																	{show:postalProvinceInputContainer,element:'#postalProvinceInputContainer'},																	
																	{show:postalAddres,element:'#postalAddres'},
																	{show:helperVariables,element:'#helperVariables'}]);
			
		},
		
		setNavigation : function() {
			
			document.getElementById("nav").value = "forex.simple.navigator.ForexPaymentReceivedQuoteLaterConfirm";
			
			document.getElementById("quoteLater").value = true;
			
			fnb.functions.submitFormToWorkspace.submit('forexPaymentReceivedPaymentDetails');

		},
		
		clearAndOrShowAutomateTransactionIndicator : function(reccuringInstruction) {
			
			if (reccuringInstruction != null)
			{
				if (reccuringInstruction == 'true' || reccuringInstruction == true)
				{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#automatedContainer'}]);
				}
				else if (reccuringInstruction == 'false' || reccuringInstruction == false)
				{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#automatedContainer'}]);
					
					document.getElementById("automated").value = false;
				}				
			}
			
		},
		
		clearAndOrShowDealNo : function(hasDealorFecNumber) {
			
			if (hasDealorFecNumber != null)
			{
				if (hasDealorFecNumber == 'true' || hasDealorFecNumber == true)
				{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#prebookedContainer'}]);
				}
				else if (hasDealorFecNumber == 'false' || hasDealorFecNumber == false)
				{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#prebookedContainer'}]);
					
					document.getElementById("dealOrFECNumber").value = "";
				}				
			}
			
		},
		
		clearAndOrShowYourPostalAddress : function(postalAddressSameAsResidential) {
			
			if (postalAddressSameAsResidential != null)
			{
				if (postalAddressSameAsResidential == 'true' || postalAddressSameAsResidential == true)
				{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#postalAddres'}]);					

				}
				else if (postalAddressSameAsResidential == 'false' || postalAddressSameAsResidential == false)
				{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#postalAddres'}]);
				}
			}
		},
		
		addBOPMonetaryDetailsToThirdParty : function(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp, paymentIsToAnMCAAccount) {
			
			if (paymentIsToAnMCAAccount != null)
			{	
				if (paymentIsToAnMCAAccount == 'true' || paymentIsToAnMCAAccount == true)
				{
					document.getElementById("actionToBePerformed").value = "OFFSHORE_PAYMENT";
					document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
					document.getElementById("reasonEziSliderLastActionPerformed").value = "";
					
					document.getElementById("nav").value = "forex.simple.navigator.ForexPaymentReceivedCapture";
					
					fnb.functions.submitFormToWorkspace.submit('forexPaymentReceivedPaymentDetails');
				}
				else
				{
					document.getElementById("thirdPartyToAddToEditOrDeleteFullName").value = thirdPartyFullName;
					document.getElementById("thirdPartyToAddToEditOrDeleteUniqueIdentifier").value = thirdPartyUniqueIdentifier;
					document.getElementById("thirdPartyToAddToEditOrDeleteCreationTimeStamp").value = thirdPartyCreationTimeStamp;
					document.getElementById("actionToBePerformed").value = "ADD";
					document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
					document.getElementById("reasonEziSliderLastActionPerformed").value = "";
					
					fnb.functions.submitFormToEziPanel.submit('forexPaymentReceivedPaymentDetails', 'nav','forex.simple.navigator.ForexPaymentReceivedAddReason');
				}	
			}
			
		},
		
		addThirdParty : function(myselfOrMyCompanyAdded) {
			
			document.getElementById("actionToBePerformed").value = "ADD";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			
			if (myselfOrMyCompanyAdded != null && (myselfOrMyCompanyAdded == 'true' || myselfOrMyCompanyAdded == true))
			{
				document.getElementById("defaultPersonSelection").value = "Third Parties";
			}
			else
			{	
				document.getElementById("defaultPersonSelection").value = "";
			}	
			
			fnb.functions.submitFormToEziPanel.submit('forexPaymentReceivedPaymentDetails', 'nav', 'forex.simple.navigator.ForexPaymentReceivedAddPerson');
			
		},
		
		addPaymentReceivedOnBehalfThirdParty : function(who) {
			
			if (who != null)
			{	
				if(who == "Myself" || who == "My Company")
				{
					document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "ADD";
					document.getElementById("reasonEziSliderLastActionPerformed").value = "";
					document.getElementById("actionToBePerformed").value = "";
					
					document.getElementById("personSelected").value = who;
					
					document.getElementById("paymentReceivedOnBehalf").value = who;
					
					document.getElementById("nav").value = "forex.simple.navigator.ForexPaymentReceivedCapture";
					
					fnb.functions.submitFormToWorkspace.submit('forexPaymentReceivedPaymentDetails');
				}	
				else	
				{	
				
					document.getElementById("actionToBePerformed").value = "ADD";
					document.getElementById("reasonEziSliderLastActionPerformed").value = "";
					document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
					
					document.getElementById("paymentReceivedOnBehalf").value = who;					

					document.getElementById("defaultPersonSelection").value = who;
					
					fnb.functions.submitFormToEziPanel.submit('forexPaymentReceivedPaymentDetails', 'nav', 'forex.simple.navigator.ForexPaymentReceivedAddPerson');
				}	
			}	
			
		},
		
		editBOPMonetaryDetailsForAThirdParty : function(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp) {
			
			document.getElementById("thirdPartyToAddToEditOrDeleteFullName").value = thirdPartyFullName;
			document.getElementById("thirdPartyToAddToEditOrDeleteUniqueIdentifier").value = thirdPartyUniqueIdentifier;
			document.getElementById("thirdPartyToAddToEditOrDeleteCreationTimeStamp").value = thirdPartyCreationTimeStamp;
			
			document.getElementById("actionToBePerformed").value = "EDIT";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";			
			
			fnb.functions.submitFormToEziPanel.submit('forexPaymentReceivedPaymentDetails', 'nav','forex.simple.navigator.ForexPaymentReceivedAddReason');
			
		},
		
		removeBOPMonetaryDetailsForAThirdParty : function(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp) {
			
			document.getElementById("thirdPartyToAddToEditOrDeleteFullName").value = thirdPartyFullName;
			document.getElementById("thirdPartyToAddToEditOrDeleteUniqueIdentifier").value = thirdPartyUniqueIdentifier;
			document.getElementById("thirdPartyToAddToEditOrDeleteCreationTimeStamp").value = thirdPartyCreationTimeStamp;
			
			document.getElementById("actionToBePerformed").value = "DELETE_REASON";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			
			document.getElementById("nav").value = "forex.simple.navigator.ForexPaymentReceivedCapture";
			
			fnb.functions.submitFormToWorkspace.submit('forexPaymentReceivedPaymentDetails');
			
		},
		
		editThirdParty : function(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp) {
			
			document.getElementById("thirdPartyToAddToEditOrDeleteFullName").value = thirdPartyFullName;
			document.getElementById("thirdPartyToAddToEditOrDeleteUniqueIdentifier").value = thirdPartyUniqueIdentifier;
			document.getElementById("thirdPartyToAddToEditOrDeleteCreationTimeStamp").value = thirdPartyCreationTimeStamp;
			
			document.getElementById("actionToBePerformed").value = "EDIT";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			
			fnb.functions.submitFormToEziPanel.submit('forexPaymentReceivedPaymentDetails', 'nav','forex.simple.navigator.ForexPaymentReceivedAddPerson');
			
		},
		
		removeThirdParty : function(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp) {
			
			document.getElementById("thirdPartyToAddToEditOrDeleteFullName").value = thirdPartyFullName;
			document.getElementById("thirdPartyToAddToEditOrDeleteUniqueIdentifier").value = thirdPartyUniqueIdentifier;
			document.getElementById("thirdPartyToAddToEditOrDeleteCreationTimeStamp").value = thirdPartyCreationTimeStamp;
			
			document.getElementById("actionToBePerformed").value = "DELETE_THIRD_PARTY";
			document.getElementById("reasonEziSliderLastActionPerformed").value = "";
			document.getElementById("thirdPartyEziSliderLastActionPerformed").value = "";
			
			document.getElementById("nav").value = "forex.simple.navigator.ForexPaymentReceivedCapture";
			
			fnb.functions.submitFormToWorkspace.submit('forexPaymentReceivedPaymentDetails');
			
		},
		
		onResidentialCountryChange : function(obj) {
			
			var country=$(obj).attr('data-value');
			
			if (country != null && country != "" && country != "0")
			{	
				if (country == 'ZA')
				{
					document.getElementById("residentialProvinceInput").value = "";
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#residentialProvinceDropDownContainer'},
					                                                     {show:'false',element:'#residentialProvinceInputContainer'}]);
				}
				else
				{
					document.getElementById("residentialProvinceDropDown").value = "0";
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#residentialProvinceDropDownContainer'},
					                                                     {show:'true',element:'#residentialProvinceInputContainer'}]);
				}	
			}
			
		},
		
		onPostalCountryChange : function(obj) {
			
			var country=$(obj).attr('data-value');
			
			if (country != null && country != "" && country != "0")
			{	
				if (country == 'ZA')
				{
					document.getElementById("postalProvinceInput").value = "";
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#postalProvinceDropDownContainer'},
					                                                     {show:'false',element:'#postalProvinceInputContainer'}]);
				}
				else
				{
					document.getElementById("postalProvinceDropDown").value = "0";
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#postalProvinceDropDownContainer'},
					                                                     {show:'true',element:'#postalProvinceInputContainer'}]);
				}	
			}
			
		},
		
		/**
		 * Binds the onclick events to the buttons for the custom built table - look at ForexPaymentReceivedCaptureForexBopMonetaryDetailsResolver.java
		 */
		bindEvents: function() {
			var parent = this;
			var thirdPartiesWithReasonsTable = $(".tableContainer");
			thirdPartiesWithReasonsTable.on('click','.eziLink',
					function(eventObject)
					{
						var o = $(eventObject.target);
												
						var thirdPartyFullName = o.closest(".tableRow").find(parent.thirdPartyFullNameIdentifier).text().replace(/^\s+|\s+$/g, ''); 
						var thirdPartyUniqueIdentifier = o.closest(".tableRow").find(parent.thirdPartyUniqueIdentifierIdentifier).text().replace(/^\s+|\s+$/g, ''); 
						var thirdPartyCreationTimeStamp = o.closest(".tableRow").find(parent.thirdPartyCreationTimeStampIdentifier).text().replace(/^\s+|\s+$/g, '');
						
						if (o.text().replace(/^\s+|\s+$/g, '') == "Edit")
						{
							parent.editBOPMonetaryDetailsForAThirdParty(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp);
						}							
						else if (o.text().replace(/^\s+|\s+$/g, '') == "Remove")
						{
							parent.removeBOPMonetaryDetailsForAThirdParty(thirdPartyFullName, thirdPartyUniqueIdentifier, thirdPartyCreationTimeStamp);
						}	
					});		
	
		}		
		
		

	}
	namespace("fnb.forex.simple.ForexPaymentReceivedCapture",genericPageObject); 
	
});


