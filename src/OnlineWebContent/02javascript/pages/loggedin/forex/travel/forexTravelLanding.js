$(function() {
	function genericPageObject() {
		this.configObject = {};
		
		this.currencyIdentifier = ".col1 .tableCellItem";
	}

	genericPageObject.prototype = {
			
		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
		},	
		loadTravelCISDetail : function(me) {
			var parent = this;
			$("#anrfn").attr('value', $(me).attr("data-value"));
			$("#action").attr("value", "retrieveCISDetails");
			fnb.functions.submitFormToWorkspace.submit("ForexTravelLanding");

		},
		showAllocationDetail: function(me){
			var parent = this;
			var val=$(me).attr('data-value');
			var rowId=$(me).closest('.tableRow').attr('id').split("_")[1];
			parent.loadTable(rowId,'/banking/Controller?nav=forex.travel.navigator.ForexTravelAddTraveller&targetRowId='+rowId +'&tableId=ForexTravellingTable' +'&forexIsFor=' + val);
			if(val == "MYSELF"){
				fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#expandableRow_ForexTravellingTable_row_'+ rowId});
			}
		},
		loadTable: function(rowId, targetUrl){
			var parent = this;
			var targetDivId = '#expandableRow_ForexTravellingTable_row_'+ rowId;
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:targetUrl,target:targetDivId});
			fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:targetDivId});
		}, 
		expandTable: function(rows){
			var parent = this;
			for(i=0;i<rows.length;i++){
				var itemConfig = rows[i];
				parent.loadTable(itemConfig["rowId"],itemConfig["targetUrl"]);
			}
		},
		removeRow: function(rowId) {
			rowId = rowId.substring(rowId.indexOf("_") + 1);
			fnb.functions.submitFormToDiv.submit(
				'','#ForexTravellingTable_tableContent',this,'',
				{alternateUrl: '/banking/Controller?nav=forex.travel.navigator.ForexTravelRemoveRow&rowId=' + rowId,noForm:true}
			);
		},
		removeTraveller: function(rowId, fieldId) {
			fnb.functions.submitFormToDiv.submit(
				'','#ForexTravellingTable_tableContent',this,'',
				{alternateUrl: '/banking/Controller?nav=forex.travel.navigator.ForexTravelRemoveTraveller&rowId=' + rowId + '&fieldId=' + fieldId,noForm:true}
			);
		},
		editTraveller: function(rowId, fieldId, tableId) {
			var urlValue =
				'/banking/Controller?nav=forex.travel.navigator.ForexTravelAddAllocation&targetRowId=' +
				 rowId + '&rowId=' + rowId + '&fieldId=' + fieldId + '&tableId=' + tableId + '&editTraveller=true';

			// Add allocation amount params
			$("input[id^='allocationAmount_']").each(function(index,value){
				urlValue += '&' + $(this).attr("id") + "=" + $(this).val();
			});

			fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', { url: urlValue });
		},
		scrollToTable : function() {
			$('html, body').animate({
			    scrollTop: $("#ForexTravellingTable").position().top
			 }, 10);
		},
		selectAmount: function(rowId) {
			rowId = rowId.substring(rowId.indexOf("_") + 1);

			var urlValue = '/banking/Controller?nav=forex.travel.navigator.ForexTravelSelectAmountLanding&rowId=' + rowId;
			// Add allocation amount params
			$("input[id^='allocationAmount_']").each(function(index,value){
				urlValue += '&' + $(this).attr("id") + "=" + $(this).val();
			});

			fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', { url: urlValue });
		}
		
	}

	namespace("fnb.forex.simple.forexTravelLanding",genericPageObject);
});
