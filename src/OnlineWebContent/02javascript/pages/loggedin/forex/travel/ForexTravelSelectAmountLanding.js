$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {
		initSelectAmount : function(currency) {
			$("#specifyAmountInContainer .formElementLabel").html("Amount in " + currency);
			$(".radioGroupValue").filter("#specifiedCurrencyCode").attr("value", currency);
		},
		denominationChange : function(dataSource) {
			if($(dataSource).attr('data-value') == 'NOTES'){
				$("#deliveryNote").hide();
				$("#denominationContainer").show();
			} else {
				$("#deliveryNote").show();
				$("#denominationContainer").hide();
			}
		},
		currencySelect : function(me){
			console.log(me);
			var currency = $(me).attr("data-value");
			$("#currencyChooseContainer").show();
			$("#dealtCurrencyRadio").html(currency);
			$("#dealtCurrencyRadio").attr("data-value", currency);

			// Fix to keep radio selected value on label change, may be better ways of doing this?
			if ($("#counterCurrencyRadio").parent().filter(".switcherWrapperSelected").attr("value") != null) {
				currency = $("#counterCurrencyRadio").attr("data-value");
			} else {
				// Sets radio group value, otherwise inconsistencies when submit after just drop down change
				$(".radioGroupValue").filter("#specifiedCurrencyCode").attr("value", currency);
			}

			$("#specifyAmountInContainer .formElementLabel").html("Amount in " + currency);
		},
		radioSelect : function(me){
			console.log(me);
			var currency = $(me).html();
			$(me).attr("data-value", currency);
			$("#specifyAmountInContainer .formElementLabel").html("Amount in " + currency);
		}
	
	}
	namespace("fnb.forex.simple.Forex",genericPageObject); 
	
});