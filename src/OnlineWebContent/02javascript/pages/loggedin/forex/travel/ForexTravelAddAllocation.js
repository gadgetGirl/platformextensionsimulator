$(function() {

	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {
		changeIDTypeDropDown : function(dropDown) {
			pages.fnb.forex.travel.ForexTravelAddAllocation.changeIDType($(dropDown).attr("data-value"));
		},
		changeIDType : function(value) {
			if (value != null && value != "") {
				$("#idNumDiv .formElementLabel").html(value == "RSAID" ? "RSA ID Number" : "Temporary Residence Number");			
				$("#idNumDiv").removeClass("hidden");
			}
		}
	}
	namespace("pages.fnb.forex.travel.ForexTravelAddAllocation", genericPageObject); 
	
});