$(function() {
	
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		enterAmount : function(me) {
			pages.fnb.forex.travel.ForexTravelAddTraveller.calculate($(me).attr('id').substring(17,18));
		},
		calculate : function(parentRowNumber) {
			var allocatedAmount = 0;
			var toBeAllocatedAmount = 0;
			var parentRow = $('#expandableRow_ForexTravellingTable_row_' + parentRowNumber);
			var parentTableRow = $('#tabelRow_' + parentRowNumber);
			var allInputs = $(parentRow).find(".currencyInput");

			var allocatedAmountContainer = $(parentRow).find("#alreadyAllocated .allocationAmount");
			var toBeAllocatedAmountContainer = $(parentRow).find("#toBeAllocated .allocationAmount");
			
			var amountAvailable = parseFloat($(parentTableRow).find("#amount").html());
			
			$(allInputs).each(function(index,value){
				var currentValue = parseFloat($(this).val().replace(/ /g,""));
				
				if(isNaN(currentValue)){
					currentValue = 0;
				}
				allocatedAmount += currentValue;
			});
			var toBeAllocatedAmount = amountAvailable - allocatedAmount;
			$(allocatedAmountContainer).html(allocatedAmount);
			$(toBeAllocatedAmountContainer).html(amountAvailable - allocatedAmount);

			if(toBeAllocatedAmount < 0){
				setTimeout(function(){
					fnb.controls.controller.eventsObject.raiseEvent('loadError', {height:'134px',message: 'Page error.', errors:[{error: 'Amount negative'}]});
					$("#isNotNegative").val("false");
				}, 20);
			} else {
				$("#isNotNegative").val("true");
			}
			
		},
		addTraveller : function(targetRowId, tableId, forexIsFor) {
			var travellerCount = 0;
			// Iterate traveller count values to get largest, can't trust the value set on a specific div as they aren't updated as a whole
			$("input[name^='travellerCount']").each(function(index,value){
				if ($(this).val() > travellerCount) {
					travellerCount = $(this).val();
				}
			});

			if (travellerCount >= 25) {
				window.setTimeout('showError("(E-100) A maximum of 25 travellers are allowed");', 50);
			} else {
				var urlValue =
					'/banking/Controller?nav=forex.travel.navigator.ForexTravelAddAllocation&targetRowId=' +
					targetRowId + '&tableId=' + tableId + '&forexIsFor=' + forexIsFor + "&addTraveller=true";

				// Add allocation amount params
				$("input[id^='allocationAmount_']").each(function(index,value){
					urlValue += '&' + $(this).attr("id") + "=" + $(this).val();
				});

				fnb.controls.controller.eventsObject.raiseEvent( 'eziSliderShow', {url: urlValue } );
			}
		}
	}

	namespace("pages.fnb.forex.travel.ForexTravelAddTraveller",genericPageObject);	
});

function showError(errorMessage){
	fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
}
