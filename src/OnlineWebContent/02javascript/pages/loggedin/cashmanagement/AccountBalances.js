	var calStartDate = new CalendarPopup();
	calStartDate.setReturnFunction("showStartDate");

	function showStartDate(y,m,d){
		if (m < 10){
		   m = "0" + m;
		}
		if (d < 10){
		   d = "0" + d;
		}
		getFormElementById("ACCOUNT_BALANCES_SEARCH","fromDate").value = y + "-" + m + "-" + d;
	}
	
	var calEndDate = new CalendarPopup();
	calEndDate.setReturnFunction("showEndDate");
	function showEndDate(y,m,d){
		if (m < 10)	{
		   m = "0" + m;
		}
		if (d < 10)	{
		   d = "0" + d;
		}
		getFormElementById("ACCOUNT_BALANCES_SEARCH","toDate").value = y + "-" + m + "-" + d;
	}
	
	function expandSearch() {
		document.getElementById("searchLink").style.display="none"
		document.getElementById("search").style.display="block"
	}
	
	function exportButton(overlay){
		
		overlay.hide();

	var formname    = "CASH_MAN_LIST_VIEW";
	var action="accountBalancesView";
	var exportButton="Export";
	var ccn=document.getElementById("ccn").value;
	var accounts=document.getElementById("accounts").value;
	var fromdate=document.getElementById("fromDate").value
	var todate=document.getElementById("toDate").value

parent.result.location="/banking/Controller?action="+action+"&formname="+formname+"&exportButton="+exportButton+"&ccn="+ccn+"&accounts="+accounts+"&fromDate="+fromdate+"&toDate="+todate;
		
	}
	
	function searchButton(overlay){
		overlay.hide();
		var formname    = "CASH_MAN_LIST_VIEW";
		var action="accountBalancesView";
		var searchButton="Search";
		var ccn=document.getElementById("ccn").value;
		var accounts=document.getElementById("accounts").value;
		var fromdate=document.getElementById("fromDate").value
		var todate=document.getElementById("toDate").value

	parent.result.location="/banking/Controller?action="+action+"&formname="+formname+"&searchButton="+searchButton+"&ccn="+ccn+"&accounts="+accounts+"&fromDate="+fromdate+"&toDate="+todate;
			
	
	}	
	function checkForm(form){
		var success = true;
		
		getFormElementById("ACCOUNT_BALANCES_SEARCH","ccn").niceName = "ccn";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","ccn").validate = "true";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","ccn").fieldType = "dropdown";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","ccn").invalidValue = "";
		
		getFormElementById("ACCOUNT_BALANCES_SEARCH","accounts").niceName = "account number";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","accounts").validate = "true";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","accounts").fieldType = "dropdown";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","accounts").invalidValue = "";
		
		getFormElementById("ACCOUNT_BALANCES_SEARCH","fromDate").niceName = "start date";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","fromDate").validate = "true";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","fromDate").fieldType = "date";
		
		getFormElementById("ACCOUNT_BALANCES_SEARCH","toDate").niceName = "end date";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","toDate").validate = "true";
		getFormElementById("ACCOUNT_BALANCES_SEARCH","toDate").fieldType = "date";
		
		success = parent.menu.verifyForm(form);
		
		return success;
	}
	
	function executeAccountLink(){
	
		if (!getFormElementById("ACCOUNT_BALANCES_SEARCH","ccn").value == "") {
		 	parent.result.location = "/banking/Controller?formname=CASH_MAN_LIST_VIEW&action=accountBalancesView&ccn=" + getFormElementById("ACCOUNT_BALANCES_SEARCH","ccn").value;		
		}
	}
	