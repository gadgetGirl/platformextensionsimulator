$(function(){
	function genericPageObject(){
 
	}
	genericPageObject.prototype = {
		init: function(datasource){	
			var parent = this;
			this.hideContent();
			if(datasource != undefined){
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:datasource, target:"#hiddenAppletDiv"});
			}
			
			fnb.hyperion.utils.footer.configFooterButtons();
		},
		displayContent : function(args){
			var parent = this;
			$("#certificatePath").attr("value", args[0]);

			$("#certificatePassword").attr("value","");
			$("#certificateConfirm").attr("value","");
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#appletDiv'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#detectionDiv'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDivLoading'}])

			$('#appletButtonsNext').removeClass('displayNone')
			$('#appletButtonsCancel').removeClass('displayNone')
			
			$('#appletDetectionButtonsNext').addClass('displayNone')
			$('#appletDetectionButtonsCancel').addClass('displayNone')
			
			var changePin = ($("#changePin").attr("checked") == 'checked')?'true':'false';
			fnb.functions.showHideToggleElements.showHideToggle([{show:changePin,element:'#appletDivChangePin'}])
			
			fnb.controls.controller.showFooterButtons();
			//Developer: Donovan
			fnb.hyperion.utils.footer.configFooterButtons();
		},
		displayDetectionContent : function(args){
			var parent = this;
			$("#detectionPath").attr("value", args[0]);
			$("#detectionPassword").attr("value","");
						
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDiv'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#detectionDiv'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDivLoading'}])
			
			$('#appletButtonsNext').addClass('displayNone')
			$('#appletButtonsCancel').addClass('displayNone')
			
			$('#appletDetectionButtonsNext').removeClass('displayNone')
			$('#appletDetectionButtonsCancel').removeClass('displayNone')

			fnb.controls.controller.showFooterButtons();
			//Developer: Donovan
			fnb.hyperion.utils.footer.configFooterButtons();
		},
		changePin : function(){
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#appletDiv'}])
			var changePin = ($("#changePin").attr("checked") == 'checked')?'true':'false';
			fnb.functions.showHideToggleElements.showHideToggle([{show:changePin,element:'#appletDivChangePin'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDivLoading'}])
			fnb.controls.controller.showFooterButtons();
			//Developer: Donovan
			fnb.hyperion.utils.footer.configFooterButtons();
		},
		hideContent : function(){
			var parent = this;
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDiv'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#detectionDiv'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#appletDivChangePin'}])
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#appletDivLoading'}])
			
			$('#appletButtonsNext').addClass('displayNone')
			$('#appletButtonsCancel').addClass('displayNone')
			
			$('#appletDetectionButtonsNext').addClass('displayNone')
			$('#appletDetectionButtonsCancel').addClass('displayNone')
			
			fnb.controls.controller.clearFooterButtons();
		},
		browse : function(){
			var parent = this;
			this.sendMessage(["Browse"]);
		},
		accept : function(isHardCert){
			var parent = this;
			var emailPassCode = $("#emailPassCode").attr("value");
			var cellPassCode = $("#cellPassCode").attr("value");
			if(isHardCert == 'true'){
				var certificatePIN = $("#certificatePIN").attr("value");
				var newPIN = $("#newPIN").attr("value");
				var confirmPIN = $("#confirmPIN").attr("value");
				var changePin = ($("#changePin").attr("checked") == 'checked');
				this.sendMessage(["Next", certificatePIN,newPIN, confirmPIN, changePin,emailPassCode, cellPassCode]);
			}else{
				var password = $("#certificatePassword").attr("value");
				var confirm = $("#certificateConfirm").attr("value");
				var path = $("#certificatePath").attr("value");
				var rememberPath = $("#rememberPath").attr("value");
			
				this.sendMessage(["Next", path,rememberPath, password, confirm,emailPassCode, cellPassCode]);
			}
			
			
		},
		acceptDetection : function(){
			var parent = this;
			var password = $("#detectionPassword").attr("value");
			var path = $("#detectionPath").attr("value");
			
			this.sendMessage(["Next", path, password]);
		},
		cancel : function(){
			var parent = this;
			this.sendMessage(["Cancel"]);
		},
		resend : function(){
			var parent = this;
			this.sendMessage(["Resend"]);
		},
		sendMessage : function(args){
			var parent = this;
			try{
				$('#chameleonApplet')[0].sendMessage(args);
			}catch(e){
				document.applets[0].sendMessage(args);
			}
		}
		
	};
	namespace("fnb.certificate.CertificateIssueValidation",genericPageObject);
});