$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
		init: function(){
		},
		
		dropdownChanged: function(obj,merNum,merName){
			var val=$(obj).attr('data-value');
			fnb.controls.controller.eventsObject.raiseEvent
			('loadUrlToWorkspace', {url: "/banking/Controller?nav=merchantservices.transactionhistory.navigator.MerchantServicesTransactionHistory&filterOption="+val+"&merchantNumber="+merNum+"&merchantName="+merName});
		},
		
		dropdownChangedForTransactionHistory: function(obj,merNum,merName){
			var val=$(obj).attr('data-value');
			fnb.controls.controller.eventsObject.raiseEvent
			('loadUrlToWorkspace', {url: "/banking/Controller?nav=merchantservices.transactionhistory.navigator.MerchantServicesTerminalID&filterOption="+val+"&merchantNumber="+merNum+"&merchantName="+merName});
		},
		
		destroy: function(){
			var self = this;
			self = null;
		}
	}
	namespace("fnb.merchantservices.transactionhistory", genericPageObject);
});