$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){
			var parent = this;
		    parent.configObject = dataSource;
		},
		destroy: function(){
			var parent = this;
			parent = null;
		},
		
		applyToAll: function(){
			var length = $("#statementSize").val();
			if($("#applyAll").is(':checked')){

				var firstEmail = $("#primaryEmailAdress0").val();
				var cc1 = $("#CC1Address0").val();
				var cc2 = $("#CC2Address0").val();
				var cycle = $("#statementCycle0").val();
				
				for(var i=1; i<length; i++){
					$("#primaryEmailAdress"+i).val(firstEmail);
					$("#CC1Address"+i).val(cc1);
					$("#CC2Address"+i).val(cc2);
					$("#statementCycle"+i).val(cycle);

					if($("#format0 .radioGroupValue").val()=='pdf'){
						$("#format"+i+" .radioGroupValue").val('pdf');
						var container = $("#format"+i);
						$(container.find('.mobi-dropdown-trigger')).attr('value', 'pdf');
						
						$(container.find('.radioButton')).each(function(){
							var itsParent = $(this).parent();
							if(itsParent.hasClass('switcherWrapperSelected')) itsParent.removeClass('switcherWrapperSelected');
							if($(this).attr('data-value')=='pdf'){
								itsParent.addClass('switcherWrapperSelected');
							}
							itsParent.attr('value', 'pdf');
						});
					} else {
						$("#format"+i+" .radioGroupValue").val('pdfcsv');
						var container = $("#format"+i);
						$(container.find('.mobi-dropdown-trigger')).attr('value', 'pdfcsv');
						
						$(container.find('.radioButton')).each(function(){
							var itsParent = $(this).parent();
							if(itsParent.hasClass('switcherWrapperSelected')) itsParent.removeClass('switcherWrapperSelected');
							if($(this).attr('data-value')=='pdfcsv'){
								itsParent.addClass('switcherWrapperSelected');
							}
							itsParent.attr('value', 'pdfcsv');
						});
					}
				}
			} else {
				for(var i=1; i<length; i++){
					$("#primaryEmailAdress"+i).val('');
					$("#CC1Address"+i).val('');
					$("#CC2Address"+i).val('');
					$("#statementCycle"+i).val('');

					$("#format"+i+" .radioGroupValue").val('');
					var container = $("#format"+i);
					$(container.find('.mobi-dropdown-trigger')).attr('value', '');
					
					$(container.find('.radioButton')).each(function(){
						var itsParent = $(this).parent();
						if(itsParent.hasClass('switcherWrapperSelected')) itsParent.removeClass('switcherWrapperSelected');
						itsParent.attr('value', '');
					});
				}
			}
		}
	}
	
	namespace("fnb.statementdelivery.complex.StatementDeliveryEmail",genericPageObject);
});