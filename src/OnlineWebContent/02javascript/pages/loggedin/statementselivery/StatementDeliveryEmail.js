

	function validateForm() {

		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var address1 = getFormElementById('STATEMENT_DELIVERY_FORM',
				'emailPrimary').value;
		var address2 = getFormElementById('STATEMENT_DELIVERY_FORM', 'emailCC1').value;
		var address3 = getFormElementById('STATEMENT_DELIVERY_FORM', 'emailCC2').value;

		if (reg.test(address1) == false) {
			alertO("Invalid primary email address");
			return false;
		}

		if (reg.test(address2) == false && address2 != "") {
			alertO("Invalid CC1 email address");
			return false;
		}

		if (reg.test(address3) == false && address3 != "") {
			alertO("Invalid CC2 email address");
			return false;
		}

		var fieldsEntered = true;
		var email_primary = getFormElementById('STATEMENT_DELIVERY_FORM',
				'emailPrimary').value;

		if (!getFormElementById('STATEMENT_DELIVERY_FORM', 'acknowledge').checked) {
			alertO("Please tick the check box in order to acknowledge that you accept the disclaimer.");
			fieldsEntered = false;
		}

		if ((email_primary == null) || (email_primary == "")) {
			alertO("Please enter a primary email address");
			fieldsEntered = false;
		} else {
			var email_format = getFormElementById('STATEMENT_DELIVERY_FORM',
					'format').value;
			if (!getFormElementById('STATEMENT_DELIVERY_FORM', 'format')[0].checked
					&& !getFormElementById('STATEMENT_DELIVERY_FORM', 'format')[1].checked) {
				alertO("Please select a valid format");
				fieldsEntered = false;
			}
		}

		if (fieldsEntered) {
			document.STATEMENT_DELIVERY_FORM.submit();
		} else {
			return fieldsEntered;
		}
	}

	function validateTC() {

		if (!getFormElementById('STATEMENT_DELIVERY_FORM',
				'homeloanTCConfirmed').checked) {
			alertO("Please read and accept the Terms and Conditions before continuing.");
		} else {
			validateForm();
		}

	}
