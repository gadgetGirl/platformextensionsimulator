
$(function() {
	function genericPageObject() {

	}
	genericPageObject.prototype = {
		checkClick : function() {
			
			if($('#emailAddress').attr('disabled') == 'disabled'){
				$('#emailAddress').prop('disabled', false);
				$('#emailSubject').prop('disabled', false);
			} else {
				$('#emailAddress').prop('disabled', true);
				$('#emailSubject').prop('disabled', true);
			}
			
			
		}
	}
	namespace("fnb.interimstatements.InterimStatementsExport",genericPageObject); 
	
});


