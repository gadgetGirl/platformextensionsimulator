
{
	function genericPageObject (){
		
		this.buyButtonIdentifier = ".col5 .eziLink";
		this.instrumentNameFieldIdentifier = ".col1 .tableCellItem.doubleItemBottom";
		this.instrumentCodeFieldIdentifier = ".col1 .tableCellItem.doubleItemTop";
		
	};
	
	genericPageObject.prototype = {
			init: function(configObject){
				var me = this;
				me.config = configObject;
				me.bindEvents();
				
				
				// hide on init - should ideally be done on the page itself
				// front-end to fix
				$("#resetButton").hide(); //LM - Removed the reset button from the screen js not needed anymore for now
				$("#buyButton").hide();
				
				// check if we need to select a share by default
				// there's only one for now, so we just click the only item there is
				// This also needs to be hidden 
				if(me.config["selectInstrument"]){
					$("#goldPrepareTable").find(".eziLink").each(function(){
						$(this).hide();
						$(this).trigger('click');
					});
				}
				
				
				return me;
			}
			, bindEvents: function(){
				var me = this;
				var sharesTable = $("#goldPrepareTable");
				sharesTable.on('click','.eziLink',
						function(eventObject){
							var o = $(eventObject.target);
							// if this is a disabled button, ignore
							if(o.hasClass("disabled")){
								return;
							}
							var itemRow = o.closest(".tableRow");
							var instrumentName = itemRow.find(me.instrumentNameFieldIdentifier).text()
							var instrumentCode = itemRow.find(me.instrumentCodeFieldIdentifier).text()
							$("#instrumentDescription").html(instrumentName);
							$("#instrumentCode").val(instrumentCode);
							sharesTable.find(me.buyButtonIdentifier).addClass("disabled");
							$("#sharesFooterMessage").removeClass("hideElement");
							$("#resetButton").show();//LM - Removed the reset button from the screen js not needed anymore for now
							$("#buyButton").show();
							
							fnb.hyperion.utils.footer.configFooterButtons();
					});
			}
			,resetFields: function(){
				var me = this;
				var sharesTable = $("#goldPrepareTable");
				// reset amounts to the default
				$("#numberOfShares").val("");
				$("#quantity").val("");
	
				// show buttons
				sharesTable.find(me.buyButtonIdentifier).not(".unavailable").removeClass("disabled");
				// hide input box
				$("#sharesFooterMessage").addClass("hideElement");
				// remove reset button
				$("#resetButton").hide();//LM - Removed the reset button from the screen js not needed anymore for now
				$("#buyButton").hide();
			}
	}
	
	namespace("fnb.shares.BuySellGold", genericPageObject);

}