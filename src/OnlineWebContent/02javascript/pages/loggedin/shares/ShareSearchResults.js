$(function(){
	function genericPageObject (){
		
		this.buyButtonIdentifier = ".col5 .eziLink";
		this.shareIdFieldIdentifier = ".col6 .tableCellItem";
		this.shareNameFieldIdentifier = ".col1 .tableCellItem.doubleItemBottom";
		this.shareCodeFieldIdentifier = ".col1 .tableCellItem.doubleItemBottom";
		
	};
	
	genericPageObject.prototype = {
			init: function(configObject){
				var me = this;
				me.config = configObject;
				me.bindEvents();
				
				$("#buyButton").hide();
				
				if(me.config["selectShare"]){
					$("#shareResultsTable").find(".eziLink").trigger('click');
				}
				
			
			}, 
			bindEvents: function(){
				var me = this;
				var sharesTable = $("#shareResultsTable");
				sharesTable.on('click','.eziLink', function(eventObject){
							var o = $(eventObject.target);						
							if(o.hasClass("disabled")){
								return;
							}
							var shareName = o.closest(".tableRow").find(me.shareNameFieldIdentifier).text(); 
							var shareId = o.closest(".tableRow").find(me.shareIdFieldIdentifier).text(); 
							$("#shareSearchShareId").html(shareName);
							$("#shareId").val(shareId);
							sharesTable.find(me.buyButtonIdentifier).addClass("disabled");
							$("#sharesFooterMessage").removeClass("hideElement");
							$('#formFooterButtons').removeClass('displayNone')
							$("#buyButton").show();
							fnb.hyperion.utils.footer.configFooterButtons();
					});
			}
			,resetFields: function(){
				var me = this;
				var sharesTable = $("#shareResultsTable");
				// reset amounts to the default
				sharesTable.find(".currencyInput").val(0);
				// show buttons
				sharesTable.find(me.buyButtonIdentifier).removeClass("disabled");
				// hide input box
				var footerMessage = $('.footerMessage')
				footerMessage.find(".currencyInput").val(0);
				$("#sharesFooterMessage").addClass("hideElement");
				$("#buyButton").hide();
				$("#error").hide();
				fnb.hyperion.utils.footer.configFooterButtons();
			}
			
	}
	
	namespace("fnb.shares.ShareSearchResults", genericPageObject);

});