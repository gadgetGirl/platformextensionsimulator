$(function() {
	function genericPageObject() {
		this.defaults = {};
	}
	genericPageObject.prototype = {

		init : function(defaults) {
			var me = this;
			me.defaults  = defaults;
			if(defaults["selectedMethod"]=="N"){
				$("#methodNow").click();
			}else if(defaults["selectedMethod"]=="D"){
				$("#methodBest").click();
			}
			
		},
		disable:function() {
			$('#bestSelection').click();
			$('#switcherWrapperlimitSelection').addClass('disabled');
		},
		enable:function() {
			$('#switcherWrapperlimitSelection').removeClass('disabled');
		}
		, limitSelectionClick: function(radio){
			if($(radio).closest('.switcherWrapper').hasClass('disabled')){
				return false; 
			}
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#priceLimit'}]);
			fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '/banking/mammoth/loggedin/invest/shares/investor/ShareInvestorSellCapturePopUp.jsp');
		}
		
	}

	namespace("fnb.shares.simple.investor.ShareInvestorSellCapture",genericPageObject);
});


