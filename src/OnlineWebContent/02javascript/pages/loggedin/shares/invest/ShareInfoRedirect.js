/*
 * The functions below are needed on the shareinfo pages, hence they cannot follow the chameleon 
 * naming convention. This needs to be changed once we fix ShareInfo
 * Non-standard ajax loads are use for the same reason.
 */

function viewDiv(contentType, target){
	if($('#'+target).css("display")=="block"){
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#'+target} ]);
	}else{
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#'+target} ]);	
	}
	
	return false;
}

function showSens(target){
	if($('#'+target).css("display")!="none"){
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#'+target} ]);
	}else{
		fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#'+target} ]);	
	}
	
	return false;
}


function emailSharesHistory() {
	
	
	var formTarget = $("form[name='mail_form']");
	
	pages.fnb.shares.invest.ShareInfoRedirect.emailHistory(
						formTarget.find("#shareCodeSearchVal").val()
						,formTarget.find("#level").val()
						,formTarget.find("#email").val()
						);
	
	return false;

}



$(function() {
	function genericPageObject() {
		this.configObject = {};
		this.someVarName = true;
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();

		},

		destroy : function() {

			var parent = this;
			parent = null;

		},

		pageLoaded : function() {

			var parent = this;
			// preload all the share information into defined divs
			//_ajaxLoader.loadUrl(targetDiv,tUrl,preLoadingCallBack, postLoadingCallBack,false);
			// load sens
			$("#sens").load(parent.configObject["sharesPrefix"] 
							+ "/Controller?action=sens"
							+ "&shareCode="+parent.configObject["shareCode"]
							+"&hide=true");
			// load directors dealings
			$("#dealings").load(parent.configObject["sharesPrefix"] 
							+ "/Controller?action=dealings"
							+ "&shareCode="+parent.configObject["shareCode"]
							+"&hide=true");
			// load dividends
			$("#dividend").load(parent.configObject["sharesPrefix"] 
							+ "/Controller?action=dividends"
							+ "&shareCode="+parent.configObject["shareCode"]
							+"&hide=true");
			// load company information
			$("#companyinfo").load(parent.configObject["sharesPrefix"] 
							+ "/Controller?action=companyinfo"
							+ "&shareCode="+parent.configObject["shareCode"]
							+"&hide=false");
			// load share ratios
			$("#ratios").load(parent.configObject["sharesPrefix"] 
							+ "/Controller?action=ratios"
							+ "&shareCode="+parent.configObject["shareCode"]
							+"&hide=true");
			// load financials
			$("#financial").load(parent.configObject["sharesPrefix"] 
							+ "/Controller?action=financial"
							+ "&shareCode="+parent.configObject["shareCode"]
							+"&hide=true");
			// load events calendar
			$("#eventscalendar").load(parent.configObject["sharesPrefix"] 
							+ "/Controller?action=eventscalendar"
							+ "&shareCode="+parent.configObject["shareCode"]
							+"&hide=true");

		},

		emailHistory : function(shareCode, level, email) {
			
			var parent = this;
			
			console.log("emailing shrare history");
			$.ajax({
				url : parent.configObject["sharesPrefix"] + "/Controller?"
								+ "shareCodeSearchVal="+shareCode
								+ "&level="+level
								+ "&email="+email
				,
				beforeSend : function(xhr) {
					//xhr.overrideMimeType("text/plain; charset=x-user-defined");
				}
			}).done(function(data) {
				if (console && console.log) {
					console.log("Sample of data:", data.slice(0, 100));
				}
			});

		},

		newMethod2 : function() {
			//method code here

		}

	}

	namespace("fnb.shares.invest.ShareInfoRedirect", genericPageObject);
});


