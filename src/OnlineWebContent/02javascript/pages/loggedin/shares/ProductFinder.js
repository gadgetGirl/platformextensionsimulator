$(function(){
	function genericPageObject (){
			this.footerButtons;
			this.footerButtonsContent;
			this.loadObject;
			this.hasFooterButtons = false;
	};
	genericPageObject.prototype = {
		init: function(noShares){
			 
			var parentObject = this;
			parentObject.footerButtons = $('#footerButtonsContainer');
			 parentObject.footerButtonsContent = parentObject.footerButtons.children();

			if(parentObject.footerButtonsContent != undefined && parentObject.footerButtonsContent.length > 0) {
			   parentObject.footerButtonsContent = parentObject.footerButtons.children();
			   parentObject.hasFooterButtons = true;
			} 
			$('#productFinderWrapper').on('click touchend', '.ui-link', function(event) {pages.fnb.shares.ProductFinder.navigateToURL(event)});
			this.load();
				
		}, 
		load: function(){
			var parentObject = this;
			var loadObj = {url: '/share-investing/overview.html?bankingFrame=true',target: '#productFinderWrapper'};
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget', loadObj);
			
		}, 
		showPopup: function(type){
			
			if (type == '' || type == 'undefined') {
				fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl','/banking/Controller?nav=invest.shares.navigator.ProductFinderPopUp&prodCode=');
			}else{

				fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl','/banking/Controller?nav=invest.shares.navigator.ProductFinderPopUp&prodCode='+type);
			}
		},
		hijackFooterButtons: function() {
		
			var parentObject = this;			
			var cozaCancelBtt = $('#productApplyNow').find('.ui-link') ;
					
			var cozaApplyBtt = $('#productApplyNow').find('.ui-applyNow');
					
			var prodCode = "" + cozaApplyBtt.attr('productcode') + "";
			var applyBttTemplate;
			var cancelBttTemplate = '';
						
			if(cozaCancelBtt.length > 0 && parentObject.hasFooterButtons == false) {
				
				parentObject.loadObj = {url:'/share-investing/overview.html' + '?bankingFrame=true',target: '#productFinderWrapper'};
				cancelBttTemplate = '<div id="cancelBtn" class="gridCol grid10 footerBtn"><a id="cancelBtnHref" href="#" target="_self" onclick="pages.fnb.shares.ProductFinder.navigateWithinCoZa()">Cancel</a></div>' 
				
			}else if(cozaCancelBtt.length > 0 && parentObject.footerButtonsContent.prop('outerHTML').length > 0){
				
				cancelBttTemplate = parentObject.footerButtonsContent.prop('outerHTML');	
				
			}else if(cozaCancelBtt.length == 0 && parentObject.hasFooterButtons == false){
			
			    cancelBttTemplate = '';
			  				
			};
			
			if(cozaApplyBtt.length > 0){
				
				var productCode = prodCode;
				
				var onclick = "fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url:'/banking/Controller?nav=offer.navigator.InstantSales&shares=true&product="+productCode+"'})"
				//change the below line to redirect to instant sales-> find out how to bring out a slider. setNextStateNewNavigation("offer.navigator.InstantSales");
				"fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url:'/banking/Controller?nav=offer.navigator.InstantSales'})"
				applyBttTemplate = '<div id="applyBtn" class="gridCol grid10 footerBtn"><a id="applyBtnHref" href="#" target="_self" onclick="'+onclick+'">Apply</a></div>'+ cancelBttTemplate
				//applyBttTemplate = '<div id="applyBtn" class="gridCol grid10 footerBtn"><a id="applyBtnHref" href="#" target="_self" onclick="pages.fnb.shares.ProductFinder.showPopup('+prodCode+')">Apply</a></div>'+ cancelBttTemplate
			
			}else{ 
			
				applyBttTemplate = cancelBttTemplate
			}
			console.log(parentObject.footerButtonsContent.prop('outerHTML'));
			parentObject.footerButtons.html('');
			parentObject.footerButtons.prepend(applyBttTemplate);
			fnb.hyperion.utils.footer.show(fnb.hyperion.controller.footerButtonGroup);
			
		
		},
		
		navigateWithinCoZa: function() {
	
			
			var parentObject = this;
			fnb.controls.controller.eventsObject.raiseEvent('loadUrl',parentObject.loadObj)
		},
		navigateToURL: function(event) {
			
			var href = $(event.currentTarget).attr('link');
			var loadObj = {url:href+'?bankingFrame=true',target: '#productFinderWrapper'};
			fnb.controls.controller.eventsObject.raiseEvent('loadUrl',loadObj)
		}
		
	}
	
	namespace("fnb.shares.ProductFinder", genericPageObject);

});



