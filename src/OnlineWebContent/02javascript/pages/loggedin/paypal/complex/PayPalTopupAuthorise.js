$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoad();			
		}, 
		
		pageLoad : function(){
			
		},
					
		confirmDetailsAJAX : function() {
			var parent = this;
							
			var termsAndConditions = $("#termsAndConditions").attr('checked') == 'checked';
			var detailsConfirmed = $("#detailsConfirmed").attr('checked') == 'checked';
			var documentsConfirmed = $("#documentsConfirmed").attr('checked') == 'checked';
			
			var url = '/banking/Controller?nav=paypal.complex.navigator.PayPalSingleAuthConfirmCheckBox&termsAndConditions='+termsAndConditions+'&detailsConfirmed='+detailsConfirmed+'&documentsConfirmed='+documentsConfirmed;
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url});
		
			
		}
		
		
	}
	namespace("fnb.paypal.complex.PayPalTopupAuthorise",genericPageObject);	
});