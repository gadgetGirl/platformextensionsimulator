$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoad();			
		}, 
		
		pageLoad :function(){
			var parent = this;
			var currenciesDropdownSize = parent.configObject.currenciesDropdownSize;
			var showTransactionReasonDIV = currenciesDropdownSize <= 2 ? 'true' : 'false';
			fnb.functions.showHideToggleElements.showHideToggle([{show:showTransactionReasonDIV,element:'#notes'},
			                                                     {show:showTransactionReasonDIV,element:'#transactionReasonDIV'},
			                                                     {show:showTransactionReasonDIV,element:'#topupAmountDIV'}]);
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#displayCCN'}, 
			                                    				  {show:'false',element:'#displayTax'}]);			
		},
		
		currencyFields : function(obj, targetDiv){
			var parent = this;
			
			var currency=$(obj).attr('data-value');
			
			if(currency != 0 && currency != ""){
				var url = '/banking/Controller?nav=paypal.complex.navigator.PayPalGetSelectedCurrency&currency=' + currency;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#"+targetDiv});
				
				if (currency == 'USD') {			 
					 
					 $("#topupAmountDIV .formElementLabel").html("Top Up Amount in (USD)");
					 $('#transactionReason').val('0');
					 $('#topupNoteCurrency').html(' and the Reason for Transaction');
					 
					 fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#notes'}, 
					                                    				  {show:'true',element:'#transactionReasonDIV'},
					                                    				  {show:'true',element:'#topupAmountDIV'},
					                                    				  {show:'false',element:'#zarNote'}]);
					 
				 }  else if (currency == 'ZAR') {
					 
					 $("#topupAmountDIV .formElementLabel").html("Top Up Amount in (ZAR)");
					 $('#transactionReason').val('0');
					 $('#topupNoteCurrency').html('');
					 
					 fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#notes'}, 
					                                    				  {show:'false',element:'#transactionReasonDIV'},
					                                    				  {show:'true',element:'#topupAmountDIV'},
					                                    				  {show:'true',element:'#zarNote'}]);
					 
				 }
			}else{//Please Select
				parent.pageLoad();
			}
		},
		
		clearAmountField : function(id){
			
		},
		
		showExtraFields : function(obj, targetDiv){
			var parent = this;
			var transactionReason=$(obj).attr('data-value');
			
			if(transactionReason != 0 && transactionReason != ""){
				var url = '/banking/Controller?nav=paypal.complex.navigator.PayPalGetSelectedTransactionReason&transactionReason=' + transactionReason;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#"+targetDiv});
			}
		},
		
		openTermsAndConditions : function(){
			var parent = this;
			var skinCode = parent.configObject.skinCode;
			window.open("/mammoth/staticPdfs/"+skinCode+"PayPalTermsAndConditions.pdf", "", "width=800, height=600,left=350,top=250,scrollbars=yes,status=yes");
		}
	}
	namespace("fnb.paypal.complex.PayPalTopupCapture",genericPageObject); 	
});