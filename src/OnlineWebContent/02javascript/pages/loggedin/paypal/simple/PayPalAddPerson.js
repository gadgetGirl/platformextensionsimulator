$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
		}, 

		pageLoaded : function() {
			var parent = this;
			
			var personSelected = parent.configObject.personSelected;
			var thirdPartySelected = parent.configObject.thirdPartySelected;
			var postalSameAsResidential = parent.configObject.postalSameAsResidential;

			parent.displayExtraFields(personSelected, thirdPartySelected, postalSameAsResidential);

			
		},
		checkIdType : function(me) {
			if($(me).attr('data-value') == 1 || $(me).attr('data-value') == 2){
				$('#passportCountry_parent').removeClass('displayNone');
				if($(me).attr('data-value') == 2) $('#passportCountry_parent').addClass('displayNone');
			} else {
				$('#passportCountry_parent').addClass('displayNone');
			}
			
			if($(me).attr('data-value') == 0){
				$("#dateofBirth_datePicker").hide();
			} else {
				$("#dateofBirth_datePicker").show();
			}
			
		},
		
		displayExtraFields : function(personSelected, thirdPartySelected, postalSameAsResidential) {
			
			var thirdPartyContainer = "false";
			var individualContainer2 = "false";
			var individualDetails = "false";
			var companyDetails = "false";
			var radioClicked = "false";
			var postalAddressContainer = "false";
			var helperVariables = "false";
			
			if (personSelected != null)
			{
				if (personSelected == "Myself")
				{
					thirdPartyContainer = "false";
				}	
				else if (personSelected == "Third Party")
				{
					thirdPartyContainer = "true";
				}	
			}	
				
			if (thirdPartySelected != null)
			{
				if (thirdPartySelected == "Individual")
				{
					individualContainer2 = "true";
					individualDetails = "true";
					companyDetails = "false";
					radioClicked = "true";
				}	
				else if (thirdPartySelected == "Company")
				{
					individualContainer2 = "false";
					individualDetails = "false";
					companyDetails = "true";
					radioClicked = "true";
				}				
			}
			
			if (postalSameAsResidential != null && (postalSameAsResidential == 'false' || postalSameAsResidential == false))
			{
				postalAddressContainer = "true";
			}	
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:thirdPartyContainer,element:'#thirdPartyContainer'},
				                              	                   {show:individualContainer2,element:'#individualContainer2'},
				                              	                   {show:individualDetails,element:'#individualDetails'},
				                              	                   {show:companyDetails,element:'#companyDetails'},
				                              	                   {show:radioClicked,element:'#radioClicked'},
				                              	                   {show:postalAddressContainer,element:'#postalAddressContainer'},
				                              	                   {show:helperVariables,element:'#helperVariables'}]);
			
		},
		
		resetPostalFields : function(postalSameAsResidential) {
			
			
			if (postalSameAsResidential != null)
			{	
				if (postalSameAsResidential == 'true' || postalSameAsResidential == true)
				{
					document.getElementById("thirdPartyPostalAddressLine1").value = "";
					document.getElementById("thirdPartyPostalAddressLine2").value = "";
					document.getElementById("thirdPartyPostalSuburb").value = "";
					document.getElementById("thirdPartyPostalCity").value = ""; 
					document.getElementById("thirdPartyPostalProvince").value = "";
					document.getElementById("thirdPartyPostalPostalCode").value = ""; 
					document.getElementById("thirdPartyPostalCountryCode").value = "";
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#postalAddressContainer'}]);
	
				}
				else if (postalSameAsResidential == 'false' || postalSameAsResidential == false)
				{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#postalAddressContainer'}]);
	
				}
			}	
			
		},
		
		resetThirdPartyFields : function(thirdPartySelected) {
			
			
			if (thirdPartySelected == "Company")
			{
				document.getElementById("thirdPartyLegalEntityName").value = "";
				document.getElementById("thirdPartyLegalEntityRegistrationNumber").value = "";
				document.getElementById("thirdPartyLegalEntityCustomsClientNumber").value = "";
				document.getElementById("thirdPartyLegalEntityTaxNumber").value = ""; 
				document.getElementById("thirdPartyLegalEntityVatNumber").value = "";
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#individualContainer2'},
				                                                     {show:'true',element:'#individualDetails'},
				                                                     {show:'false',element:'#companyDetails'},
				                                                     {show:'true',element:'#radioClicked'}]);
			}	
			else if (thirdPartySelected == "Individual")
			{
				document.getElementById("thirdPartyIndividualSurname").value = "";
				document.getElementById("thirdPartyIndividualFirstName").value = "";
				document.getElementById("thirdPartyIndividualGender").value = "";
				document.getElementById("thirdPartyIndividualDateOfBirth").value = ""; 
				document.getElementById("thirdPartyIndividualRsaIdNumber").value = "";
				document.getElementById("thirdPartyIndividualIdType").value = "";
				document.getElementById("thirdPartyIndividualTemporaryResidencePermitNumber").value = "";
				document.getElementById("thirdPartyIndividualPassportNumber").value = ""; 
				document.getElementById("thirdPartyIndividualPassportCountry").value = "0";
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#individualContainer2'},
				                                                     {show:'false',element:'#individualDetails'},
				                                                     {show:'true',element:'#companyDetails'},
				                                                     {show:'true',element:'#radioClicked'}]);

			}
			
		},
		submitForm : function() {
			if ($("#amount").val() == "") {
				$("#amount").val("0");
			}
			if ($("#amountBusiness").val() == "") {
				$("#amountBusiness").val("0");
			}

			
			fnb.functions.submitFormToDiv.submit('PayPalAddPerson','#allocationTableContainer',this, {alternateUrl: ''}, '',true);
		}
	}
	namespace("fnb.paypal.simple.PayPalAddPerson",genericPageObject); 
	
});


