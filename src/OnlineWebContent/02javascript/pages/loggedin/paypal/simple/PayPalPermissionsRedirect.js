$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.openPayPalWindow();
						
		}, 
		
		openPayPalWindow : function(){
			var parent = this;
			var paypalRedirectURL = parent.configObject.paypalRedirectURL;
			fnb.utils.paypal.windows.openWindow(paypalRedirectURL,"payPalWindow", "location=1,menubar=1,resizable=1,scrollbars=1,status=1,titlebar=1,toolbar=1,height=768,width=1024");
		},
		
		cancelPayPalLogin : function(){
			var parent = this;
			var sessionId = parent.configObject.sessionId;
			var url = "/banking/Controller?nav=paypal.simple.navigator.PayPalPostRedirect&fnbsessionid="+sessionId+"&process=cancelRedirect";
			fnb.utils.paypal.windows.openedWindows["payPalWindow"].close();
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: url});
		}
	}
	namespace("fnb.paypal.simple.PayPalPermissionsRedirect",genericPageObject); 	
});