$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.reloadPage();
						
		}, 
		
		reloadPage : function(){
			var parent = this;
			var isPendingStatus = parent.configObject.pendingStatus;
			var waitTime = parent.configObject.waitTime;
			var url = parent.configObject.pageUrl;
			
			if(isPendingStatus == 'true'){
				window.setTimeout(function(){
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: url});	
				}, waitTime);
			}
		}
	}
	namespace("fnb.paypal.simple.PaypalWithdrawalRequestResult",genericPageObject); 	
});