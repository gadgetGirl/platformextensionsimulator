$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoad();			
		}, 
		
		pageLoad :function(){
			var parent = this;
			var currenciesDropdownSize = parent.configObject.currenciesDropdownSize;
			var showTransactionReasonDIV = currenciesDropdownSize <= 2 ? 'true' : 'false';
			fnb.functions.showHideToggleElements.showHideToggle([{show:showTransactionReasonDIV,element:'#notes'},
			                                                     {show:showTransactionReasonDIV,element:'#transactionReasonDIV'},			                                                     
			                                                     {show:showTransactionReasonDIV,element:'#topupAmountDIV'}]);
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#displayCCN'}, 
			                                                     {show:'false',element:'#paymentOfMoreThanOneInvoiceContainer'},
			                                                     {show:'false',element:'#invoiceTable'},
			                                                     {show:'false',element:'#allocationTableContainer'},
			                                    				  {show:'false',element:'#displayTax'}]);			
		},
		
		currencyFields : function(obj, targetDiv){
			var parent = this;
			
			var currency=$(obj).attr('data-value');
			
			if(currency != 0 && currency != ""){
				var url = '/banking/Controller?nav=paypal.simple.navigator.PayPalGetSelectedCurrency&currency=' + currency;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#"+targetDiv});
				
				if (currency == 'USD') {			 
					 
					 $("#topupAmountDIV .formElementLabel").html("Top Up Amount in (USD)");
					 fnb.forms.dropdown.select($('#transactionReason_parent li:eq(0)'));
					 $('#topupNoteCurrency').html(' and the Reason for Transaction');
					 
					 fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#notes'}, 
					                                    				  {show:'true',element:'#transactionReasonDIV'},
					                                    				  {show:'true',element:'#topupAmountDIV'},
					                                    				  {show:'false',element:'#zarNote'},					                                    				  
					                                    				  {show:'false',element:'#thirdPartyIndividualDetails'},
					                                    				  {show:'false',element:'#thirdPartyBusinessDetails'}]);
					 
				 }  else if (currency == 'ZAR') {
					 
					 $("#topupAmountDIV .formElementLabel").html("Top Up Amount in (ZAR)");
					 fnb.forms.dropdown.select($('#transactionReason_parent li:eq(0)'));
					 $('#topupNoteCurrency').html('');
					 
					 fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#notes'}, 
					                                    				  {show:'false',element:'#transactionReasonDIV'},
					                                    				  {show:'true',element:'#topupAmountDIV'},
					                                    				  {show:'true',element:'#zarNote'}]);
					 
				 }
			}else{//Please Select
				parent.pageLoad();
			}
		},
		
		clearAmountField : function(id){
			
		},
		
		showExtraFields : function(obj, targetDiv){
			var parent = this;
			var transactionReason=$(obj).attr('data-value');
			
			if(transactionReason != 0 && transactionReason != ""){
				var url = '/banking/Controller?nav=paypal.simple.navigator.PayPalGetSelectedTransactionReason&transactionReason=' + transactionReason;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#"+targetDiv});
			}
			var reason=transactionReason.substring(0,3);
			var subCode = "";
			if(transactionReason.indexOf("-") > 0){
				var start = transactionReason.indexOf("-")+1;
				var end = transactionReason.indexOf(":");
				subCode = transactionReason.substring(start, end);
				
			}
			if('260' == reason || '261' == reason){
                    $("#travelAgent").show();
                    $("#paymentOfMoreThanOneInvoiceContainer").hide();
                    $("#invoiceTableHidden").hide();   
                    $("#invoiceTable").hide();   
			    }
			else{
				$("#travelAgent").hide();
				
			}
			if('101' == reason ){
				$("#travelAgent").hide();
				$("#paymentOfMoreThanOneInvoiceContainer").show();
				$("#invoiceTable").hide();
				if(subCode != '11'){
					$("#invoiceTableHidden").show();
				}else{
					$("#invoiceTableHidden").hide();
				}
				$("#displayCCNDiv").show(); 
			}
			else{
				$("#paymentOfMoreThanOneInvoiceContainer").hide();
				$("#invoiceTable").hide();   
				$("#invoiceTableHidden").hide();
				$("#displayCCNDiv").hide();
				
			}
			if('401' == reason){
				$("#displayCCNDiv").hide(); 
			}
			
		},
		
		openTermsAndConditions : function(){
			var parent = this;
			var skinCode = parent.configObject.skinCode;
			window.open("/mammoth/staticPdfs/"+skinCode+"PayPalTermsAndConditions.pdf", "", "width=800, height=600,left=350,top=250,scrollbars=yes,status=yes");
		},
		
		openThirdPartyDetails : function(obj, targetDiv){
			var parent = this;
			
			var thirdPartyType = $(obj).attr('data-value');
			alert()
			var url = '/banking/Controller?nav=paypal.simple.navigator.PayPalGetSelectedTransactionReason&thirdPartyType=' + thirdPartyType;
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#"+targetDiv});
		}
	}
	namespace("fnb.paypal.simple.PayPalTopupCapture",genericPageObject); 	
});