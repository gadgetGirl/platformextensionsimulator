$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoad();			
		}, 
		
		pageLoad :function(){
			var parent = this;
			var transactionReason=parent.configObject.transactionReason;
			var targetDiv=parent.configObject.displayCCNDiv;
	
			if(transactionReason != 0 && transactionReason != ""){
				var url = '/banking/Controller?nav=paypal.simple.navigator.PayPalGetSelectedTransactionReason&transactionReason=' + transactionReason;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#"+targetDiv});
			}						
		},		
		
		clearAmountField : function(id){
			
		},
		
		showExtraFields : function(obj, targetDiv){
			var parent = this;
			var transactionReason=$(obj).attr('data-value');
			
			if(transactionReason != 0 && transactionReason != ""){
				var url = '/banking/Controller?nav=paypal.simple.navigator.PayPalGetSelectedTransactionReason&transactionReason=' + transactionReason;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#"+targetDiv});
			}
		},
		
		openTermsAndConditions : function(){
			var parent = this;
			var skinCode = parent.configObject.skinCode;
			window.open("/mammoth/staticPdfs/"+skinCode+"PayPalTermsAndConditions.pdf", "", "width=800, height=600,left=350,top=250,scrollbars=yes,status=yes");
		}
	}
	namespace("fnb.paypal.simple.PayPalTopupQuoteConfirm",genericPageObject);	
});