$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoad();	
			//alert("int");
		}, 
		
		pageLoad :function(){
			var parent = this;
		},
		
		exportedFields : function(me){
			var selected=$(me).attr('data-value');
			
			if (selected == 'YES') {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#transactionReasonDIV'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#goodsAmountDIV'}]);
			
			} else if (selected == 'NO') {		
				 $('#transactionReason').val('0');
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#transactionReasonDIV'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#goodsAmountDIV'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#taxNumberRow'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#customsNumberRow'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#UniqueConNum'}]);
				
			
			}
		},
		
		currencyFields : function(me){
			var selectedCurrency = $(me).attr('data-value');
			
			//alert('Hello '+selectedCurrency);
			 if (selectedCurrency == 'USD') {
		
				 $('#totalWithdrawalCurrency').html('(USD)');
				 $('#goodsExportedCurrency').html('(USD)');
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#transactionReasonDIV'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#goodsPaymentIncludedDIV'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#goodsAmountDIV'}]);
					 
					 $('#goodsPaymentIncluded').val('0');
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#taxNumberRow'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#customsNumberRow'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#UniqueConNum'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#usdToolTipDIV'}]);
						
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#zarToolTipDIV'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#zarNote'}]);		
			 } else if (selectedCurrency == 'ZAR') {
		
		
				 $('#totalWithdrawalCurrency').html('(ZAR)');
				 $('#goodsExportedCurrency').html('(ZAR)');
				 $('#transactionReason').val('0');
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#transactionReasonDIV'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#goodsPaymentIncludedDIV'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#goodsAmountDIV'}])
				$('#goodsPaymentIncluded').val('0');
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#taxNumberRow'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#customsNumberRow'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#UniqueConNum'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#usdToolTipDIV'}]);
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#zarToolTipDIV'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#zarNote'}]);	
			 } else {
		
				 $('#transactionReason').val('0');
				 
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#transactionReasonDIV'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#goodsPaymentIncludedDIV'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#goodsAmountDIV'}]);
					 $('#goodsPaymentIncluded').val('0');
		
					 
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#taxNumberRow'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#customsNumberRow'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#UniqueConNum'}]);
					 		
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#usdToolTipDIV'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#zarToolTipDIV'}]);
			}
		},
	
		extraFields : function (me){

			var parent = this;
		    var customerTypeIndicator=parent.configObject.customerTypeIndicator;		    
		    var transactionReason = $(me).attr('data-value');
		    var category = transactionReason.substring(0,3);
		    var ivsAllowed = parent.configObject.ivsAllowed;
	                    			//If not allowed,(ie has a ssite entry) we dont' allow IVS BoP codes
		    var subCode = "";
			if(transactionReason.indexOf("-") > 0){
				var start = transactionReason.indexOf("-")+1;
				var end = transactionReason.indexOf(":");
				subCode = transactionReason.substring(start, end);
				
			}
		    
		    if(!ivsAllowed){
				
				var part1 = "Due to scheduled maintenance this Balance of Payment category is currently unavailable.  All functionality will be available at 20 August 2011.\n"
				var part2 = "In the interim please contact us on 0861 PAYPAL (729725) for assistance, we appreciate your patience and apologize for the inconvenience.";
	
				if(category == '101'){
					   alert(part1 + part2);
				}
			}
			
			if(category != 0 && category != ""){
				var url = '/banking/Controller?nav=paypal.simple.navigator.PayPalWithdrawalTransactionReason&transactionReason=' + transactionReason;
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#customsNumberRow'}]);
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#customsNumberRow"});
				
			}
			
			
			
			var reason=$(me).attr('data-value').substring(0,3);

			
			//Unique Consignemnt Number row
			if (reason == '101' && (subCode == '01' || subCode == '02')){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#paymentOfMoreThanOneInvoiceContainer'}]);
				
				var isUniqueConsignment = $("#paymentOfMoreThanOneInvoice").val() == "true";
				
				if(!isUniqueConsignment){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#invoiceTable'}])
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#invoiceTableHidden'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#UniqueConNum'}]);
				}else{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#invoiceTable'}])
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#invoiceTableHidden'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#UniqueConNum'}]);
					
				}
				
			}
			else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#invoiceTableHidden'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#UniqueConNum'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#paymentOfMoreThanOneInvoiceContainer'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#invoiceTable'}]);
			}
			
			
			
		},
			
		exportedFields : function(me){
			var selected=$(me).attr('data-value');
			
			if (selected == 'YES') {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#transactionReasonDIV'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#goodsAmountDIV'}]);
			
			} else if (selected == 'NO') {		
				 $('#transactionReason').val('0');
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#transactionReasonDIV'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#goodsAmountDIV'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#taxNumberRow'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#customsNumberRow'}]);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#UniqueConNum'}]);
			}
		},
		
		openTermsAndConditions : function(){
			var parent = this;
			var skinCode = parent.configObject.skinCode;
			window.open("/mammoth/staticPdfs/"+skinCode+"PayPalTermsAndConditions.pdf", "", "width=800, height=600,left=350,top=250,scrollbars=yes,status=yes");
		}
	}
	namespace("fnb.paypal.simple.PayPalWithdrawal",genericPageObject); 	
});