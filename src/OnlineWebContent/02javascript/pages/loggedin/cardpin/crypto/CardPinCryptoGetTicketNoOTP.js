$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoad();	
			//alert("int");
		}, 
		pageLoad :function(){
			var parent = this;
		},
		errorMessage : function(errorMessage){
			fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
		},
		checkResponse : function(){
			var parent = this;
			clearTimeout(timer);
			fnb.utils.eziSlider.submit('cardCreatePIN');
			//Main.loadUrlToWorkspace("/banking/Controller?formname=VIEW_PIN_IBM&action=changePinConfirm", 10);
		}
		submitCardAndPin : function(me){
		var cardNumLength = 0;
		var errorMessage = "";
		var success = true;
		formName = form;
		if ($('#card').val()) {
			cardNumLength = $('#card'),val().length();
		}
		
		if ((cardNumLength > 16) || (cardNumLength < 16) || (cardNumLength == 0) || !isNumeric($('#card').val())) {
			errorMessage = "Please enter the 16 digit card number as it appears on the card without spaces.";
			parent.errorMessage(errorMessage);

			success = false;
		}
		
		if (!isNumeric($('#pin').val())) {
			errorMessage = "Please enter a valid 4 digit PIN.";
			//alertO(errorMessage);
			parent.errorMessage(errorMessage);

			success = false;
		} else if ($('#pin').val().length() > 4 || $('#pin').val().length() < 4) {
			errorMessage = "Please enter a valid 4 digit PIN";
			//alertO(errorMessage);
			parent.errorMessage(errorMessage);

			success = false;
		}
		
		if (!success) {
			//resetMultiple();
			return false;
		}
		
		executeGetTicket();
		
		return false;
		},
		executeGetTicket : function(me){
			
			
			$("#ticketDiv").html("");
			var urlTick="/banking/Controller?nav=cardpin.crypto.navigator.CardPinCryptoGetTicketNoOTP&formname=CRYPTO_FORM&action=getticket&countryID="$('#country')+"&cardNumber="+$('#card')"
             $.ajax({
			  url: urlTick,
			  cache: false
			}).done(function( html ) {
			  $("#tick").val(html);
			})
		
			var p = $("#pin").val();
			var card = $("#card").val();
			var tic = $("#tick");
			if (tic.length === 64) {
				var cryptoURI = "${cryptoURL}";
				var urlValidation = "${cryptoURL}/authenticatepin?ticket=" + tic + "&cardno=" + card.substring(3, 15) + "&pin=" + p;
				document.getElementById("res").src = urlValidation;
				$("#pin").val("");
				$("#card").val("") ;
				timer=setTimeout(parent.checkResponse,300);
				return true;
			}
		}
	}
	namespace("fnb.paypal.simple.PayPalWithdrawal",genericPageObject); 	
});