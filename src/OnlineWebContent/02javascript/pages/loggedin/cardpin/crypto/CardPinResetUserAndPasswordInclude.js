$(function() {
	function genericPageObject() {
		this.configObject = {};
		this.cryptoURL={};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			//parent.pageLoad();	
		}, 
		pageLoad :function(){
			var parent = this;
		},
		validateCardAndPinCrypto : function(){
			if (this.validateCardAndPin()) {
				this.submitCardAndPin("FORGOTTEN_USER_AND_PASSWORD");
			}
			
			return false;
		},
		validateCardAndPin : function (){
			
			var crdLength=$("#cardNumber").val().length;
			var pnLength=$("#cardPin").val().length;
			if (crdLength != 16) {
				this.errorMessage("Please enter the 16 digit card number as it appears on the card without spaces");
					return false;
				}
				if (pnLength!= 4) {
	
	            this.errorMessage("Please enter a valid 4 digit PIN");
					return false;
				}
//				if ($.trim($("#capValue").val()) || 
//						$("#capValue").val().length == 0) {
//					this.errorMessage("Please enter your captcha value ");
//
//						return false;
//					}
				//$("#simple").val("true");
				$("#formname").val("FORGOTTEN_USER_AND_PASSWORD");
				return true;
			
			
		},
		errorMessage : function(errorMessage){
			fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
		},
		checkResponse : function(){
			var parent = this;
			clearTimeout(timer);
			fnb.functions.submitFormToWorkspace.submit('FORGOTTEN_USER_AND_PASSWORD','',this, {alternateUrl: ''});
		},
		submitCardAndPin : function(me){
			var cardNumLength = 0;
			var errorMessage = "";
			var success = true;
			formName = me;
			if ($('#cardNumber').val()) {
				cardNumLength = $('#cardNumber').val().length;
			}
			
			if ((cardNumLength > 16) || (cardNumLength < 16) || (cardNumLength == 0) || !$.isNumeric($('#cardNumber').val())) {
				errorMessage = "Please enter the 16 digit card number as it appears on the card without spaces.";
				this.errorMessage(errorMessage);

				success = false;
			}
			
			if (!$.isNumeric($('#cardPin').val())) {
				errorMessage = "Please enter a valid 4 digit PIN.";
				//alertO(errorMessage);
				this.errorMessage(errorMessage);

				success = false;
			} else if ($('#cardPin').val().length > 4 || $('#cardPin').val().length < 4) {
				errorMessage = "Please enter a valid 4 digit PIN";
				//alertO(errorMessage);
				this.errorMessage(errorMessage);

				success = false;
			}
			
			if (!success) {
				//resetMultiple();
				return false;
			}
			this.getTicket();
			
			return false;
			},
			getTicket : function (me){
				var me=this;
				var cntr=$('#countryID').val();
				var crd=$('#cardNumber').val();
				$("#ticketDiv").html("");
				var urlTick="/banking/Controller?nav=cardpin.crypto.navigator.CardPinCryptoRegistrationTickectNoOTP&formname=CRYPTO_FORM&action=getticket&countryID="+cntr+"&cardNumber="+crd;
	                fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:urlTick, target:"#ticketDiv",postLoadingCallback:pages.fnb.card.crypto.CardPinResetUserAndPasswordInclude.checkTicket()});
			},
			checkTicket : function (){
				_this=this;
				setTimeout(function(){
					var isLoaded = pages.fnb.card.crypto.CardPinResetUserAndPasswordInclude.executeGetTicket()
					if(isLoaded==false) pages.fnb.card.crypto.CardPinResetUserAndPasswordInclude.checkTicket();
				},300)
			},
			executeGetTicket : function(){
	        	var tic = $("#ticketDiv").html();
	        	var p = $("#cardPin").val();
				var card = $("#cardNumber").val();
				var secureUrl = this.configObject.cryptoUrl;
				if (tic.length === 64) {
					$('#tick').val(tic);
					document.getElementById("res").src=secureUrl+"/authenticatepin?ticket=" + tic + "&cardno=" + card.substring(3, 15) + "&pin=" + p;
					timer=setTimeout(this.checkResponse,300);
					$("#pin").val("");
					return true
				}
				return false;
		}   
	}
	namespace("fnb.card.crypto.CardPinResetUserAndPasswordInclude",genericPageObject); 	
});