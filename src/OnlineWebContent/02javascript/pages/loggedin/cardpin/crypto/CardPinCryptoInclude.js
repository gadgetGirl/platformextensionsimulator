$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoad();	
			//alert("int");
		}, 
		pageLoad :function(){
			var parent = this;
		},
		errorMessage : function(errorMessage){
			fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
		},
		checkResponse : function(){
			var parent = this;
			clearTimeout(timer);
			fnb.functions.submitFormToWorkspace.submit('FORGOTTEN_USER_AND_PASSWORD','',this, {alternateUrl: ''});
			//Main.loadUrlToWorkspace("/banking/Controller?formname=VIEW_PIN_IBM&action=changePinConfirm", 10);
		},
		submitCardAndPin : function(me){
			var cardNumLength = 0;
			var errorMessage = "";
			var success = true;
			var errorsFound="";
			formName = me;
			if ($('#cardNumber').val()) {
				cardNumLength = $('#cardNumber').val().length;
			}
			
			//alert($('#cardNumber').val().length);
			if ((cardNumLength > 16) || (cardNumLength < 16) || (cardNumLength == 0) || !$.isNumeric($('#cardNumber').val())) {
				errorMessage = "Please enter the 16 digit card number as it appears on the card without spaces.";
				errorsFound=errorsFound+"<br/>"+errorMessage+"<br/>";

				success = false;
			}
			
			if ($('#countryID').val()=='Please Select') {
				errorMessage = "Please select country";
				errorsFound=errorsFound+"<br/>"+errorMessage+"<br/>";
				//alertO(errorMessage);

				success = false;
			}
			
			if ($('#idNumber').val().length==0) {
				errorMessage = "Please enter id number";
				errorsFound=errorsFound+"<br/>"+errorMessage+"<br/>";
				//alertO(errorMessage);

				success = false;
			}
			if (!$.isNumeric($('#cardPin').val())) {
				errorMessage = "Please enter a valid 4 digit PIN.";
				errorsFound=errorsFound+"<br/>"+errorMessage+"<br/>";
				//alertO(errorMessage);

				success = false;
			} else if ($('#cardPin').val().length > 4 || $('#cardPin').val().length < 4) {
				errorMessage = "Please enter a valid 4 digit PIN";
				//alertO(errorMessage);
				errorsFound=errorsFound+"<br/>"+errorMessage+"<br/>";
				success = false;
			}
			
			if (!success) {
				this.errorMessage(errorsFound);
				return false;
			}
			
			this.executeGetTicket();
			
			return false;
			},
		executeGetTicket : function(me){
            var me=this;
			var cntr=$('#countryID').val();
			var crd=$('#cardNumber').val();
			$("#ticketDiv").html("");
			var urlTick="/banking/Controller?nav=cardpin.crypto.navigator.CardPinCryptoRegistrationTickectNoOTP&formname=CRYPTO_FORM&action=getticket&countryID="+cntr+"&cardNumber="+crd;
        	fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:urlTick, target:"#ticketDiv"});

			var p = $("#cardPin").val();
			var card = $("#cardNumber").val();
			var tic = $("#tick");
		 	var tic = $("#ticketDiv").html();
	    
			var secureUrl = me.configObject.cryptoUrl;
			if (tic.length === 64) {
				$('#tick').val(tic);
				document.getElementById("res").src=secureUrl+"/authenticatepin?ticket=" + tic + "&cardno=" + card.substring(3, 15) + "&pin=" + p;
				timer=setTimeout(this.checkResponse,300);
				$("#pin").val("");
			}
		}
	}
	namespace("fnb.card.crypto.CardPinCryptoInclude",genericPageObject); 	
});