fnb.hyperion.controller.extendPage({
	//Function reference for this page object
	functionReference: "commsController",
	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.',
	
	scriptData : {},
	newsPage : {},
	rewardsContainer : {},
	bannerContainer : {},
	significantEventsContainer : {},

	bannerLocation : "/04banners/banking",
	bannerName : "/banner.html",
	
	skinBanner : "",
		
	namedBanners : 
		['/04banners/banking/banner01/banner.html',
		 '/04banners/banking/banner02/banner.html',
		 '/04banners/banking/banner03/banner.html',
		 '/04banners/banking/banner04/banner.html'],

	//Exposed functions
	init : function(banners) {
		//Get parent object
		var _this = this;
		//Do global selections
		_this.doGlobalSelections();
		
		//Fetch the banner
		_this.loadBanner();
		
		//Fetch the significant events
		if(_this.scriptData.seAllowed) {
			_this.loadeSignificantEvents();
		}
		
		//If the client has rewards fetch them
		if(_this.scriptData.hasRewards) {
			_this.loadRewards();
		};
		
		//Bind page events
		_this.bindEvents();

		//Register page
		_this.RegisterEvent();
	},
	
	//Bind page events
	bindEvents : function(){
		//List of events for this module
    	var events = [{events:'click', selector: '*#significantEventsContainer .dismissSigEvent', handler: this.context+'commsController.dismissSignificantEvents(event)'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
	},
	
	//Do global seletions
	doGlobalSelections: function () {
		//Get parent object
		var _this = this;
		
		//Get data from script tag
		_this.scriptData = fnb.hyperion.load.includesData.commsController;
		//Get the new page container
		_this.newsPage = fnb.hyperion.$('#newsLanding');
		//Get banner container
		_this.bannerContainer = fnb.hyperion.$("#bannerContainer");
		//Get rewards container
		_this.rewardsContainer = fnb.hyperion.$("#rewardsContainer");
		//Get significant events container
		_this.significantEventsContainer = fnb.hyperion.$("#significantEventsContainer");
		
		_this.skinsBanner = _this.bannerLocation + "/skins/" + _this.scriptData.skin + _this.bannerName;
		
	},
	
	loadBanner : function() {
		//Get parent object
		var _this = this;
		
		//A function to return a random number
		function getRandomInt(min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}
		
		//If S.A. but not ZBI show random banner otherwise show banner for skin 
		var bannerURL = _this.skinsBanner;
		if((_this.scriptData.isSub == "false") && (_this.scriptData.isZBI == "false") && _this.scriptData.skin == 0) {
			bannerURL  = _this.namedBanners[getRandomInt(0, 3)];
		}
		
		fnb.hyperion.controller.raiseEvent(
			'asyncLoadContent', {
				url : bannerURL,
				target : fnb.hyperion.$("#bannerContainer"),
				showErrors : false
			});
	},
	
	//A function to load the rewards content
	loadRewards : function() {
		//Get parent object
		var _this = this;
		//Check if has rewards
		if (_this.scriptData.skin) {
			
			//Setup the rewards navigator url
			var url = "/banking/Controller?nav=rewards.EbucksNewsInfo";
			
			//Request that the rewards url be loaded into the rewards placeholder 
			fnb.hyperion.controller.raiseEvent(
					'asyncLoadContent', {
						url : url,
						target : fnb.hyperion.$("#rewardsContainer"),
						showErrors : false
					});
		}
	},
	
	//A function to load the significant events content
	loadeSignificantEvents : function() {
		//Get parent object
		var _this = this;
		//Setup the significant events url
		var url = "/banking/Controller?nav=significantevents.navigator.SignificantEventsLanding";

		//
		fnb.hyperion.controller.raiseEvent(
				'asyncLoadContent', {
					url : url,
					target : fnb.hyperion.$("#significantEventsWrapper"),
					showErrors : false
				});
	},
	
	//A function to load the significant events content
	dismissSignificantEvents : function(event) {
		//Prevent default event
		event.stopImmediatePropagation();
		event.preventDefault();
		event.stopPropagation();
		
		//Get parent object
		var _this = this;
		
		//Get the identifier of the significant event to dismiss
		var offerType = fnb.hyperion.$(event.currentTarget).attr("offerType"); 
		var counter = fnb.hyperion.$(event.currentTarget).attr("counter"); 
		
		//Setup the significant events url
		var url = "/banking/Controller?nav=significantevents.navigator.SignificantEventsLanding&counter=" + counter;
		
		//
		fnb.hyperion.controller.raiseEvent(
				'asyncLoadContent', {
					url : url,
					target : fnb.hyperion.$("#significantEventsWrapper"),
					showErrors : false
				});
	},
	
	
	RegisterEvent : function() {
		//Get parent object
		var _this = this;
		//Get openUrl
		var urla = _this.scriptData.openUrl;
		if (urla != "") {
			window.open(urla);
		}
		
		var regUrl = "/banking/Controller?nav=navigator.RegisterEvent&event=UICommsPage"
				+ "&browser=" + fnb.utils.currentDevice.getDevice().browser
				+ "&version=" + fnb.utils.currentDevice.getDevice().version
				+ "&platform=" + fnb.utils.currentDevice.getDevice().platform
				+ "&height=" + fnb.utils.currentDevice.getDevice().height
				+ "&width=" + fnb.utils.currentDevice.getDevice().width
				+ "&mobile=" + fnb.utils.currentDevice.getDevice().mobile;
		
		fnb.hyperion.controller.raiseEvent(
				'post', {
				url : regUrl,
				showErrors : false
		});
	}
});
