(function($){

	function genericPageObject(){
	}

	genericPageObject.prototype = {
			init: function(config){
				var me = this;
				me.config = config;
				me.start();
				return me;
			},
			destroy: function(){
				var me = this;
			},
			start: function(){
				var me = this;
				$("#footerButtonsContainer").find(".footerBtn > a").each(function(index, item){
					var labelToActionMap = {
						"Save & Exit": "save"
						,"Submit":"submit"
						,"Authorise":"auth"
					};
					var buttonLabel = $(item).text();
					if(buttonLabel=="Save & Exit" 
						||buttonLabel=="Submit" 
					    ||buttonLabel=="Authorise" 
						)
					$(item).click(function(){
						document.getElementById("buttonClicked").value = labelToActionMap[buttonLabel];
						$("#action").val(buttonLabel);
						fnb.functions.submitFormToWorkspace.submit("transfers_Form");
						
					});
				});
				
			},
			submitTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TPFMTransferMaintainContinue";
				document.getElementById("buttonClicked").value="submit";
				fnb.functions.submitFormToWorkspace.submit("transfersAddAcrossEntityLanding");
			},
			authTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TPFMTransferMaintainContinue";
				document.getElementById("buttonClicked").value="auth";
				fnb.functions.submitFormToWorkspace.submit("transfersAddAcrossEntityLanding");
			},
			submitWithinTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TPFMTransferMaintainContinue";
				document.getElementById("buttonClicked").value="submit";
				fnb.functions.submitFormToWorkspace.submit("transfers_Form");
			},
			authWithinTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TPFMTransferMaintainContinue";
				document.getElementById("buttonClicked").value="auth";
				fnb.functions.submitFormToWorkspace.submit("transfers_Form");
			},
			saveAndExitTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TPFMTransferMaintainContinue";
				document.getElementById("buttonClicked").value="save";
				fnb.functions.submitFormToWorkspace.submit("transfersAddAcrossEntityLanding");
			},
			addMore: function(){
				document.getElementById("addMore").value="true";
				document.getElementById("nav").value="transfers.complex.navigator.TransfersCopyLanding";
				fnb.functions.submitFormToWorkspace.submit("transfers_Form");
			},
			cloneRow: function(parent, cloneTarget){
				var count = parent.find('.tableRow').length;
				var cloneItem = cloneTarget.clone();
				cloneItem.attr('id','tabelRow_'+(count+1));
				cloneItem.find('#fromAcc .dropdown-hidden-input').attr({'id': 'fromAcc'+count,'name': 'fromAcc'+count}).val('');
				var fromParentID = 'fromAcc'+count+'_dropId';
				cloneItem.find('#fromAcc .dropdown-initiator').attr('id',fromParentID)
				var fromDropID = 'fromAcc'+count+'_parent';
				cloneItem.find('#fromAcc .singleTierDropDown').attr('id',fromDropID)
				cloneItem.find('#fromAcc .dropdown-selection-white').text('Please Select');

				cloneItem.find('#toAcc .dropdown-hidden-input').attr({'id': 'toAcc'+count,'name': 'toAcc'+count}).val('');
				fromParentID = 'toAcc'+count+'_dropId';
				cloneItem.find('#toAcc .dropdown-initiator').attr('id',fromParentID)
				fromDropID = 'toAcc'+count+'_parent';
				cloneItem.find('#toAcc .singleTierDropDown').attr('id',fromDropID)
				cloneItem.find('#toAcc .dropdown-selection-white').text('Please Select');

				cloneItem.find('.col3 input:eq(0)').attr({'id': 'transferReferenceFrom'+count,'name': 'transferReferenceFrom'+count}).val('');
				cloneItem.find('.col4 input:eq(0)').attr({'id': 'transferReferenceTo'+count,'name': 'transferReferenceTo'+count}).val('');
				cloneItem.find('.col5 input:eq(0)').attr({'id': 'transferAmount'+count,'name': 'transferAmount'+count}).val('0.00');
				cloneItem.find('.col6 input:eq(0)').attr({'id': 'itemRFN'+count,'name': 'itemRFN'+count,'data-value': 0}).val(0);
				cloneItem.find('.col7 input:eq(0)').attr({'id': 'index'+count,'name': 'index'+count,'data-value': count}).val(count);

				cloneItem.appendTo(parent);
				
				setTimeout(function(){
					fnb.forms.dropdown.init($('#fromAcc'+count+'_dropId'),0);
					fnb.forms.dropdown.init($('#toAcc'+count+'_dropId'),0);	
				},200)
				
			}
			
	}

	namespace("fnb.tpfm.transfers.TPFMTransferMaintain",genericPageObject);

	/*
	function getDropdownValue(me,index, divName){
		var val=$(me).attr('value');
		dropdownUtility.loadDropDownDiv('/banking/Controller?nav=transfers.complex.navigator.TransfersToAccountDropDownBuilder&index='+index+'&fromAccDropDownValue='+val,divName);
	}
	*/
})(jQuery);