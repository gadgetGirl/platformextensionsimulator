$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showUrlDropdown'},{show:'true',element:'#showEmailAddress'}]);
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showUrlDropdown'},{show:'true',element:'#showEmailAddress'}]);
			
		},
		
		pageLoaded : function() {
			var parent = this;
			var accountNumber = parent.configObject.statementsAvailable;
			if(accountNumber != 0 && accountNumber != "") {
				parent.retrieveSingleStatements(accountNumber);
			}
			
		},
		
		retrieveStatements : function (obj) {
		
			var accountNumber =	$(obj).attr('data-value');
						
			var radio = $('#action .radioGroupValue').val();
								
			var url = '/banking/Controller?nav=tpfm.reportsandstatements.navigator.TPFMStatementRetrieve&accountNumber='+accountNumber+'&action='+radio;
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#showStatementsList'}); 
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showStatementsList'}] );
			
			$('#statementReference_dropId').find('.dropdown-selection-white').find('.dropdown-item-row').text('loading...');

			
		},
		
		retrieveSingleStatements : function (accountNumber) {
							
			var radio = $('#action .radioGroupValue').val();
								
			var url = '/banking/Controller?nav=tpfm.reportsandstatements.navigator.TPFMStatementRetrieve&accountNumber='+accountNumber+'&action='+radio;
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#showStatementsList'}); 
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showStatementsList'}] );
			
			$('#statementReference_dropId').find('.dropdown-selection-white').find('.dropdown-item-row').text('loading...');

			
		},		
			
		downloadFile : function(){
			
			var value = $('#statementReference').val();
			var action = $('#action .radioGroupValue').val();
			
			if(value == "freeEziStatementItemList") {
			var errorMessage = "You can only download 1 statement at the time";

				setTimeout(function(){
					fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
				}, 20);
				
				return false;
			}
									
			var url = '/banking/Controller?nav=tpfm.reportsandstatements.navigator.TPFMAccountStatementConfirm&action='+action+'&statementReference='+value;

			
			fnb.controls.controller.eventsObject.raiseEvent('doDownload', url);
			
			
		}
	}
	
	namespace("tpfm.reportsandstatements.TPFMStatements",genericPageObject); 
	
})