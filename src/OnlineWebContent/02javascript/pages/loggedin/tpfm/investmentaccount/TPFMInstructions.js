$(function(){
	function genericPageObject(){}
	genericPageObject.prototype = {
			init: function(){
			},

			showAmountFields: function(me, structureCode){			
				var val=$(me).attr('data-value');

				if (val == "MC") {	

						fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#amountDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#amountCloseDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#dateTermDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#transferDiv'}]);

				
				} else if (val == "MW") {

					if (structureCode == "3I") {
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#amountDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#amountCloseDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#dateTermDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#transferDiv'}]);

					} else {
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#amountDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#amountCloseDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#dateTermDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#transferDiv'}]);

					}

				} else if (val == "RINV") {
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#amountDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#amountCloseDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#dateTermDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#transferDiv'}]);

				}		

				else if (val == "NC") {	

						fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#amountDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#amountCloseDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#dateTermDiv'}]);
						fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#transferDiv'}]);


				} else if (val == "NW") {
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#amountDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#amountCloseDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#dateTermDiv'}]);
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#transferDiv'}]);

				}

			},
			
			showDateField: function(me){			
				var val=$(me).attr('data-value');
				if (val != "IM" && val != "EM") {		
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#dayInMonthDate'}]);
				}else{
					fnb.functions.showHideToggleElements.showHideToggle([{show : 'false', element : '#dayInMonthDate'}]);
				}

			},
			showInvestTermType: function(me){
				var val=$(me).attr('data-value');
				
				if (val == "date") {	
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#DateWrapper'}, 
					                                                      {show : 'false', element : '#termWrapper'}]);
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#DateWrapper'}, 
					                                                      {show : 'true', element : '#termWrapper'}]);
				}			
			},
			
			showInvestTermIn: function(me){
				var val=$(me).attr('data-value');
				
				if (val == "Y") {	
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberYearsWrapper'},
					                                                      {show : 'false', element : '#numberDaysWrapper'},
					                                                      {show : 'false', element : '#numberMonthsWrapper'} ]);
				} else if (val == "M") {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberMonthsWrapper'},
					                                                      {show : 'false', element : '#numberDaysWrapper'},
					                                                      {show : 'false', element : '#numberYearsWrapper'} ]);
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberDaysWrapper'},
					                                                      {show : 'false', element : '#numberMonthsWrapper'},
					                                                      {show : 'false', element : '#numberYearsWrapper'} ]);
				}			
			},
	}
			namespace("TPFMInstructions", genericPageObject);
	});