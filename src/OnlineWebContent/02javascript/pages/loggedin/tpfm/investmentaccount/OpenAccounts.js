$(function(){
	function genericPageObject(){
		this.earnRate;		
	}
	genericPageObject.prototype = {
		init: function(earnRate){
			
			$('#tpfmFeeCall').on('blur',function(event){
				fnb.tpfm.investmentaccount.openAccounts.calculateNetClientRate($('#tpfmFeeCall').val(),earnRate);	
			})
		},

		loadInvestmentAccountType: function(me, clientName){
			var val=$(me).attr('data-value');
			if(val == "Please select")
				{fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#addButtonWrapper'},
				                                                       {show : 'false', element : '#accountDiv'},
				                                                       {show : 'false', element : '#minInvestmentAmount'}
																	]);
				return;}
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget', {url:"/banking/Controller?nav=tpfm.investmentaccount.navigator.TPFMOpenAccountRetrieveAccountType&selectedInvestmentCode="+val+"&selectedClientUCN="+clientName+"&fromEdit=false", target:"#accountDiv"});
			
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#addButtonWrapper'},
			                                                      {show : 'true', element : '#accountDiv'},
			                                                      {show : 'true', element : '#minInvestmentAmount'}
			                                                   ]);
		},
		
		loadInvestmentAccountMinimumBalance: function(me){
			var val=$(me).attr('data-value');
			if(val == "Please select")
				{return;}
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget', {url:"/banking/Controller?nav=tpfm.investmentaccount.TPFMOpenAccountMinimumAmount", target:"#minInvestmentAmount"});
			
		},
		
		calculateNetClientRate: function(fee, earnRate) {			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget', {url:"/banking/Controller?nav=tpfm.investmentaccount.navigator.TPFMOpenAccountNetClientRate&fee="+fee+"&earnRate="+earnRate, target:"#netClientRate"});
		},
		
		showDayInMonthField: function(me){			
			var val=$(me).attr('data-value');
			
			if (val == "00") {	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#dayInMonthWrapper'} ]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#dayInMonthWrapper'} ]);
			}
			
		},
		
		showInvestTermType: function(me){
			var val=$(me).attr('data-value');
			
			if (val == "date") {	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#DateWrapper'}, 
				                                                      {show : 'false', element : '#termWrapper'}]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#DateWrapper'}, 
				                                                      {show : 'true', element : '#termWrapper'}]);
			}			
		},
		
		showInvestTermIn: function(me){
			var val=$(me).attr('data-value');
			
			if (val == "Y") {	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberYearsWrapper'},
				                                                      {show : 'false', element : '#numberDaysWrapper'},
				                                                      {show : 'false', element : '#numberMonthsWrapper'} ]);
			} else if (val == "M") {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberMonthsWrapper'},
				                                                      {show : 'false', element : '#numberDaysWrapper'},
				                                                      {show : 'false', element : '#numberYearsWrapper'} ]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberDaysWrapper'},
				                                                      {show : 'false', element : '#numberMonthsWrapper'},
				                                                      {show : 'false', element : '#numberYearsWrapper'} ]);
			}			
		},
		
		showInvestTermInMaturity: function(me){
			var val=$(me).attr('data-value');
			
			if (val == "Y") {	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberYearsMaturityWrapper'},
				                                                      {show : 'false', element : '#numberDaysMaturityWrapper'},
				                                                      {show : 'false', element : '#numberMonthsMaturityWrapper'} ]);
			} else if (val == "M") {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberMonthsMaturityWrapper'},
				                                                      {show : 'false', element : '#numberDaysMaturityWrapper'},
				                                                      {show : 'false', element : '#numberYearsMaturityWrapper'} ]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberDaysMaturityWrapper'},
				                                                      {show : 'false', element : '#numberMonthsMaturityWrapper'},
				                                                      {show : 'false', element : '#numberYearsMaturityWrapper'} ]);
			}			
		},
		
		showMaturityInstructionFields: function(me, structureCode, maturityTermType){			
			var val=$(me).attr('data-value');
			var radioItem = $("#linkedAccount");
			radioItem.trigger('click');
			if (val == "MC") {	
				
				if (structureCode == "3I") {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#linkedAccountWrapper'},
					                                                      
					                                                      {show : 'false', element : '#amountMaturityDiv'},					                                                      		                                                      
					                                                      {show : 'false', element : '#investTermTypeMaturityDiv'},
					                                                      {show : 'false', element : '#termMaturityWrapper'}					                                                      
					                                                    ]);
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#transferToDiv'},
					                                                      {show : 'true', element : '#linkedAccountWrapper'},
					                                                      
					                                                      {show : 'false', element : '#amountMaturityDiv'},					                                                      		                                                      
					                                                      {show : 'false', element : '#investTermTypeMaturityDiv'},
					                                                      {show : 'false', element : '#termMaturityWrapper'}
					                                                    ]);
				}
				
				
			} else if (val == "MW") {
				
				if (structureCode == "3I") {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#linkedAccountWrapper'},
					                                                      {show : 'true', element : '#amountMaturityDiv'},
					                                                      
					                                                      {show : 'false', element : '#investTermTypeMaturityDiv'},
					                                                      {show : 'false', element : '#termMaturityWrapper'}
					                                                    ]);
					
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#transferToDiv'},
					                                                      {show : 'true', element : '#linkedAccountWrapper'},				                                                  
					                                                      {show : 'true', element : '#amountMaturityDiv'},
					                                                      
					                                                      {show : 'false', element : '#investTermTypeMaturityDiv'},
					                                                      {show : 'false', element : '#termMaturityWrapper'}
				                                                    ]);

				}
			
			} else if (val == "RINV") {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#investTermTypeMaturityDiv'},
				                                                      {show : 'true', element : '#termMaturityWrapper'},
				                                                				                                                      
				                                                      {show : 'false', element : '#transferToDiv'},
				                                                      {show : 'false', element : '#linkedAccountWrapper'},
				                                                      {show : 'false', element : '#otherAccountContentWrapper'},
				                                                      {show : 'false', element : '#amountMaturityDiv'}
				                                                    ]);
				
			} else {
				
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#investTermTypeMaturityDiv'},
				                                                      {show : 'false', element : '#termMaturityWrapper'},				                                                				                                                      
				                                                      {show : 'false', element : '#transferToDiv'},
				                                                      {show : 'false', element : '#linkedAccountWrapper'},
				                                                      {show : 'false', element : '#otherAccountContentWrapper'},
				                                                      {show : 'false', element : '#amountMaturityDiv'}
				                                                    ]);
				
			}	
			
		},
		
		showHideCapitalisationDate: function(investmentCycleCode){
			
			if (investmentCycleCode == "00") {	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#dayInMonthWrapper'} ]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#dayInMonthWrapper'} ]);
			}
			
		},
		
		doEdit: function(invAccType, clientUCN){
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget', {url:"/banking/Controller?nav=tpfm.investmentaccount.navigator.TPFMOpenAccountRetrieveAccountType&selectedInvestmentCode="+invAccType+"&selectedClientUCN="+clientUCN+"&fromEdit=true", target:"#accountDiv"});
			
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#addButtonWrapper'} ]);
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget', {url:"/banking/Controller?nav=tpfm.investmentaccount.TPFMOpenAccountMinimumAmount", target:"#minInvestmentAmount"});
		
		},
		
		doEditTerms: function(investTermType, investTermIn){
			
			//Investment Term Type
			if (investTermType == "date") {	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#DateWrapper'}, 
				                                                      {show : 'false', element : '#termWrapper'}]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#DateWrapper'}, 
				                                                      {show : 'true', element : '#termWrapper'}]);
			}	
			
			//Investment Term In
			if (investTermIn == "Y") {	
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberYearsWrapper'},
				                                                      {show : 'false', element : '#numberDaysWrapper'},
				                                                      {show : 'false', element : '#numberMonthsWrapper'} ]);
			} else if (investTermIn == "M") {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberMonthsWrapper'},
				                                                      {show : 'false', element : '#numberDaysWrapper'},
				                                                      {show : 'false', element : '#numberYearsWrapper'} ]);
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberDaysWrapper'},
				                                                      {show : 'false', element : '#numberMonthsWrapper'},
				                                                      {show : 'false', element : '#numberYearsWrapper'} ]);
			}
			
		},
		
		doEditMaturity: function(maturityTermType, maturityTermIn, investInstruction, accountLinkedOrOtherMaturity, structureCode){				
			
			//Maturity Instruction Type
			if (investInstruction == "MC") {	
				
				if (structureCode == "3I") {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#linkedAccountWrapper'},
					                                                      
					                                                      {show : 'false', element : '#amountMaturityDiv'},					                                                      		                                                      
					                                                      {show : 'false', element : '#investTermTypeMaturityDiv'},
					                                                      {show : 'false', element : '#termMaturityWrapper'}					                                                      
					                                                    ]);
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#transferToDiv'},
					                                                      {show : 'true', element : '#linkedAccountWrapper'},
					                                                      
					                                                      {show : 'false', element : '#amountMaturityDiv'},					                                                      		                                                      
					                                                      {show : 'false', element : '#investTermTypeMaturityDiv'},
					                                                      {show : 'false', element : '#termMaturityWrapper'}
					                                                    ]);
				}
				
				//Maturity Linked or Other Account
				if (accountLinkedOrOtherMaturity == "other") {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#otherAccountContentWrapper'},
					                                                      {show : 'false', element : '#linkedAccountWrapper'} ]);
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#linkedAccountWrapper'},
					                                                      {show : 'false', element : '#otherAccountContentWrapper'} ]);
				}
				
				
			} else if (investInstruction == "MW") {
				
				if (structureCode == "3I") {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#linkedAccountWrapper'},
					                                                      {show : 'true', element : '#amountMaturityDiv'},	
					
																		  {show : 'false', element : '#investTermTypeMaturityDiv'},
														                  {show : 'false', element : '#termMaturityWrapper'}
					                                                    ]);
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#transferToDiv'},
					                                                      {show : 'true', element : '#linkedAccountWrapper'},				                                                  
					                                                      {show : 'true', element : '#amountMaturityDiv'},
					                                                      
					                                                      {show : 'false', element : '#investTermTypeMaturityDiv'},
					                                                      {show : 'false', element : '#termMaturityWrapper'}
				                                                    ]);
				}
				
				//Maturity Linked or Other Account
				if (accountLinkedOrOtherMaturity == "other") {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#otherAccountContentWrapper'},
					                                                      {show : 'false', element : '#linkedAccountWrapper'} ]);
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#linkedAccountWrapper'},
					                                                      {show : 'false', element : '#otherAccountContentWrapper'} ]);
				}
			
			} else if (investInstruction == "RINV") {
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#investTermTypeMaturityDiv'},
				                                                      {show : 'true', element : '#termMaturityWrapper'},
				                                                      				                                                      
				                                                      {show : 'false', element : '#transferToDiv'},
				                                                      {show : 'false', element : '#linkedAccountWrapper'},
				                                                      {show : 'false', element : '#otherAccountContentWrapper'},
				                                                      {show : 'false', element : '#amountMaturityDiv'}
				                                                    ]);
				
				//Maturity Term In
				if (maturityTermIn == "Y") {	
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberYearsMaturityWrapper'},
					                                                      {show : 'false', element : '#numberDaysMaturityWrapper'},
					                                                      {show : 'false', element : '#numberMonthsMaturityWrapper'} ]);
				} else if (maturityTermIn == "M") {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberMonthsMaturityWrapper'},
					                                                      {show : 'false', element : '#numberDaysMaturityWrapper'},
					                                                      {show : 'false', element : '#numberYearsMaturityWrapper'} ]);
				} else {
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#numberDaysMaturityWrapper'},
					                                                      {show : 'false', element : '#numberMonthsMaturityWrapper'},
					                                                      {show : 'false', element : '#numberYearsMaturityWrapper'} ]);
				}
			}	
		}
		
	}
		
	namespace("fnb.tpfm.investmentaccount.openAccounts", genericPageObject);
});