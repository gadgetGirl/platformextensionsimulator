$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
			return parent;
		},
		
		pageLoaded : function() {
			var parent = this;
			if(parent.configObject["recipientType"]=="publicRecipient"){
				$("#publicRecipient").click();
			}else if(parent.configObject["recipientType"]=="notGlobal"){
				$("#notGlobal").click();
			}/*else if(parent.configObject["recipientType"]=="eWallet"){
				$("#eWallet").click();
			}*/
			
		}
		
		

	}

	namespace("fnb.tpfm.payments.complex.TPFMOnceOffPaymentConfirm",genericPageObject);
});


