$(function() {
	function genericPageObject() {
		var fnbSelectionPicker = function(){};
		var lastScroll = 0;
		var scrollCount=1;
		var parentID;
	}
	genericPageObject.prototype = {
		init : function(MainHolderName,ScrollingWrapperID) {
			this.parentID = ScrollingWrapperID;
			this.MainHolder = $(MainHolderName);
			fnb.utils.scrollingBanner.show(this.parentID);
		},
		show : function(brand) {
			fnb.utils.scrollingBanner.adjust($(window).width(),this.parentID)
			if (brand != "" && typeof brand != "undefined") {
            	brand = 'cardImageHolder' + brand;
            	fnb.utils.scrollingBanner.bannerParent[this.parentID].find('.scrollingBannerItem').attr("class", 'scrollingBannerItem cardImageHolder hidden');
            	
            	fnb.utils.scrollingBanner.bannerParent[this.parentID].find("div[id^='" + brand + "']").attr("class", 'scrollingBannerItem cardImageHolder');
            	fnb.utils.scrollingBanner.bannerParent[this.parentID].bannerItems = fnb.utils.scrollingBanner.bannerParent[this.parentID].bannerScrollingBannerWrapper.children(":not(.hidden)");
            	fnb.utils.scrollingBanner.bannerParent[this.parentID].childCount = fnb.utils.scrollingBanner.bannerParent[this.parentID].bannerItems.length+1;
            } else {
            	var items = fnb.utils.scrollingBanner.bannerParent[this.parentID].find('.scrollingBannerItem');
            	items.attr("class", 'scrollingBannerItem cardImageHolder');
            	fnb.utils.scrollingBanner.bannerParent[this.parentID].bannerItems = items;
            	fnb.utils.scrollingBanner.bannerParent[this.parentID].childCount = fnb.utils.scrollingBanner.bannerParent[this.parentID].bannerItems.length;
            }
        	
        	this.MainHolder.removeClass('visibilityHidden');
        	
        	fnb.utils.scrollingBanner.bannerParent[this.parentID].bannerScroller.internalEvent = true;
        	fnb.utils.scrollingBanner.bannerParent[this.parentID].bannerScroller.moving = true;
        	fnb.utils.scrollingBanner.bannerParent[this.parentID].bannerScroller.x = 0;
        	fnb.utils.scrollingBanner.bannerParent[this.parentID].bannerScrollingBannerWrapper.parent().css({'-webkit-transition': '','transition': '','-webkit-transform': ''})
        	
		},
         
        hide: function(){
        	this.MainHolder.addClass('visibilityHidden');
        },
        destroy: function(){        
            var parent = this;              
            parent = null;
        },
        selectCard: function(objectID) {
            var selectedImageName = $("#selectorDeviceHolderBodyProductImage" + objectID).attr("src");
            var selectedProductName = $(".selectorDeviceHolderBodyProductName" + objectID).text();
            var selectedProductPrice = $(".selectorDeviceHolderBodyProductPrice" + objectID).text();
            var selectedProductType = $("#deviceDescription" + objectID).val();
            
            this.hide();      
        },
        showHideDeals: function(hide) {
        	if (hide) {
        		$("[id^='deal']").hide();
        	} else {
        		$("[id^='deal']").show();
        	}
        },
        scrollLeft: function() {
        	fnb.utils.scrollingBanner.scrollLeft(this.parentID)
        },
        scrollRight: function() {
        	fnb.utils.scrollingBanner.scrollRight(this.parentID)
        }
        
	}

	namespace("fnb.mobiledeviceoffer.simple.MobileDeviceOffer",genericPageObject);
});

