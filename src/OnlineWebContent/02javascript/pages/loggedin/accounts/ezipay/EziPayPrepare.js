/**
 * Makes a ajax call to populate dropdown
 */
var dropDownAjaxCall = function(obj, divName){
	var ddValue=$(obj).attr('data-value');
	if(ddValue != ""){//Please Select
		var url = '/banking/Controller?nav=navigator.payments.simple.RecipientCategoryFilter&recipientCategory=' + ddValue +'&dropdownDiv='+divName;
		fnb.functions.loadDropDownDiv.load(url,divName,null);
	}
}