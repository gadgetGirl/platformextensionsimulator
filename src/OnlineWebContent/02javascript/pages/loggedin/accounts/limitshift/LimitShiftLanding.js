$(function(){
	function genericPageObject(){
		this.configObject = {};
		this.limitTotal = 0;
	}
	genericPageObject.prototype = {
		init: function(dataSource){
			var me = this; 
		    me.configObject = dataSource;
		},
		destroy: function(){
			var me = this; 
			me = null;
		},
		updateStraightLimit: function() {
			var me = this;
			document.getElementById("straightLimit").value = ( me.configObject.limitTotal - me.parseNum(document.getElementById("budgetLimit").value) );
		},
		updateBudgetLimit: function() {
			var me = this;
			document.getElementById("budgetLimit").value = ( me.configObject.limitTotal - me.parseNum(document.getElementById("straightLimit").value) );
		},
		parseNum: function(num) {
			var val = parseFloat( num.replace(/ /g,"") );
			return (isNaN(val) ? 0 : val);
		}
	}
	namespace("fnb.accounts.limitshift.LimitShiftLanding",genericPageObject);
});