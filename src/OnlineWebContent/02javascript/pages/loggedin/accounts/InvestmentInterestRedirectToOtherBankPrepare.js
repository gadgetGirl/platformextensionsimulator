var refreshable = true;
function refresh() {
	if (getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","accountType").value == 'W' || getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","accountType").value == 'F') {
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","bank").disabled = 'disabled';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","branchCode").disabled='disabled';
		tdBank.className='normalGreyFont';
		tdBranch.className='normalGreyFont';
		restrictedBranch = true;
	}
	getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","branchCode").click();	
}
function changeRemittanceType(radio){
	if (radio.value == "0"){
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","subject").disabled=true;
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","contact").disabled=true;
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","subject").value="";
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","contact").value="";
	}else{
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","subject").disabled=false;
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","contact").disabled=false;
	}
}
function blankFields() {
	if (getFormElementsByName("FORM_INTEREST_REDIRECT_TO_OTHER","isGlobal")[0].checked) {
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","bank").disabled = 'disabled';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","branchCode").disabled='disabled';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","accountNumber").disabled='disabled';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","accountType").disabled = 'disabled';
		tdBank.className='normalGreyFont';
		tdBranch.className='normalGreyFont';
		tdAccType.className='normalGreyFont';
		tdAccNum.className='normalGreyFont';
		tdPublic.className='normalBlackFont';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","isGlobal").disabled = '';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","bank").value='0';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","branchCode").value='';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","accountNumber").value='';
		getFormElemantById("FORM_INTEREST_REDIRECT_TO_OTHER","accountType").value='0';
	} else {
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","bank").disabled = '';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","branchCode").disabled='';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","accountNumber").disabled='';
		getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","accountType").disabled = '';
	}		
}

function publicCollapse() {
	expandcontent('addDiv');
}
function expandBranch() {
	expandcontent('branchCodeSearchDiv');
}
function clearPublicList() {
	getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","publicName").value = "";
	getFormElementById("FORM_INTEREST_REDIRECT_TO_OTHER","publicRFN").value = "0";
}
function doChanges(){
	parent.menu.setBankName('FORM_INTEREST_REDIRECT_TO_OTHER');
}
function applyPublicRecipient() {
	if (getFormElementsByName("FORM_INTEREST_REDIRECT_TO_OTHER","isGlobal")[0].checked) {
		blankFields();
		expandcontent('addDiv');
	}
}

function checkAccountAndSubmit(){
	var selectedAccount = getFormElementById("FORM_INTEREST_INSTRUCTION","selectedAccount").value;
	if (selectedAccount != "0") {
		 submitForm("FORM_INTEREST_INSTRUCTION");
	} else {
		alertO("Please select Account Number");
		getFormElementsByName("FORM_INTEREST_INSTRUCTION","option")[0].checked = false;
		getFormElementsByName("FORM_INTEREST_INSTRUCTION","option")[1].checked = false;
	}
}
function doChanges(){
	parent.menu.setBankName('FORM_INTEREST_REDIRECT_TO_OTHER');
}
function setBankName(which, branchCode) {
	var bank = parent.document.forms[0].bank;
	bank = which;
	parent.document.forms[0].branchCode.value = branchCode;
//	parent.document.forms[0].branchCode.click();
	parent.doChanges();
	parent.restrictedBranch.value=false;
	parent.expandBranch();
	parent.document.forms[0].accountType.disabled='';
	parent.tdAccType.className='normalBlackFont';
}