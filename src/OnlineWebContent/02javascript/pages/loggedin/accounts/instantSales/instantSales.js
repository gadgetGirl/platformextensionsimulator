// developer: Mike
///
/// Page object
///-------------------------------------------///
fnb.hyperion.controller.extendPage({
	//Function reference for this page object
	functionReference: "instantSales",
	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.',
	reInit:true,
	///////////////////////////
    //Page object methods
    ///////////////////////////
	//Init method -- Do not add this if you dont want your object to be initialized
	init : function(){
		
		var url = fnb.hyperion.$('#urlContainer').attr('data-url');
		
		fnb.hyperion.controller.raiseEvent('loadEzi',{url:url})
		//var dataObject =  fnb.hyperion.load.includesData.instantSales;
		//console.log(fnb.hyperion.load.includesData.instantSales)
	}
});