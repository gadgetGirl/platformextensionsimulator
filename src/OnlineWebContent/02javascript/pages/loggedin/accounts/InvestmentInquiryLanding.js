function checkForm(){
	var selectedAccount = getFormElementById("FORM_INTEREST_INSTRUCTION","selectedAccount");
	var result = true;
	if (selectedAccount.value == '0') { 
		alertO('Please select Account'); 
		result = false;
	} else {
		var options = getFormElementsByName("FORM_INTEREST_INSTRUCTION","option");
		if (!(options[0].checked || options[1].checked)) { 
			alertO('Please select Instruction option'); 
			result = false;
		}
	}
	return result;	
}