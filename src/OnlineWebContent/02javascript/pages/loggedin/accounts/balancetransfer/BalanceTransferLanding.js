$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {
			
		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();			
		}, 
			
		pageLoaded : function() {
			var parent = this;
		},
		
		resetClick: function(){
			$("#reset").removeClass("footerBtn").addClass("displayNone");
			$("#calculate").removeClass("footerBtn").addClass("displayNone");
			$("#acceptOffer").addClass("footerBtn").removeClass("displayNone");

			fnb.hyperion.utils.footer.configFooterButtons();
		},
		customiseClick: function(){
			$("#reset").addClass("footerBtn").removeClass("displayNone");
			$("#acceptOffer").removeClass("footerBtn").addClass("displayNone");
			$("#calculate").addClass("footerBtn").removeClass("displayNone");
			fnb.hyperion.utils.footer.configFooterButtons();
		},
		
		setNavigation : function(btn){					
			if(btn == 'accept'){
				document.getElementById("nav").value = 'accounts.balancetransfer.navigator.BalanceTransferConfirm';
				document.getElementById("action").value = 'balancetransferaccept';
			}else{
				document.getElementById("nav").value = 'accounts.balancetransfer.navigator.BalanceTransferLanding';
				document.getElementById("action").value = 'balancetransferrecalculate';
			}
		}, 
		
		openLink : function(name){
			if(name == "fnb"){
				window.open("https://www.fnb.co.za/credit-card/balance-transfer.html","","toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable,alwaysRaised,dependent,titlebar=yes,width=800,height=600,");  
			}

			if(name == "rmb"){
				window.open("https://www.rmbprivatebank.com/our-service-approach/transactional/balance-transfer.html","","toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable,alwaysRaised,dependent,titlebar=yes,width=800,height=600,");  
			}

			if(name == "kuluka"){
				window.open("https://www.fnb.co.za/downloads/credit-cards/Balance_transfer_kulula.pdf","","toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable,alwaysRaised,dependent,titlebar=yes,width=800,height=600,");  
			}

			if(name == "clicks"){
				window.open("https://www.fnb.co.za/downloads/credit-cards/Balance_transfer_clicks.pdf","","toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable,alwaysRaised,dependent,titlebar=yes,width=800,height=600,");  
			}
			
			/*if(name == "discovery"){
				window.open("https://www.fnb.co.za/credit-card/balance-transfer.html","","toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable,alwaysRaised,dependent,titlebar=yes,width=800,height=600,");  
			}*/

		}
	}
	namespace("fnb.accounts.balancetransfer.BalanceTransferLanding",genericPageObject); 		
});
	