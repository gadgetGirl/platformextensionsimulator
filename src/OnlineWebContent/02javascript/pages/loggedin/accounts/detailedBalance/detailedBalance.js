function loadAccountSettings(me){
	var val=$(me).attr('data-value');
	fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen',"/banking/Controller?nav=accounts.detailedbalance.navigator.DetailedAccountBalanceRedirect&anrfn="+val);
}

$(function(){
	function genericPageObject(){
	
	}
	genericPageObject.prototype = {
		
		init: function(){	
			
			var parent = this; 	
		   		
		},

	      destroy: function(){	
			
			var parent = this; 	
			parent = null;
			
	      },
		
	      loadAccountSettings: function(me){
			var val=$(me).attr('data-value');
			fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen',"/banking/Controller?nav=accounts.detailedbalance.navigator.DetailedAccountBalanceRedirect&anrfn="+val);
	    
	      }
	}
	
	namespace("fnb.balances.SharedDropDownObject",genericPageObject);
});
