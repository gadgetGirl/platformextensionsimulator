$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
		},
		
		setSelectedAccount :function(obj) {
			var parent = this;
			var val=$(obj).attr('data-value');
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: '/banking/Controller?nav=accounts.orderchequebook.navigator.OrderChequeBookLanding&action=reload&anrfn='+val});
		}
	}
	
	
	namespace("fnb.accounts.orderchequebook.OrderChequeBookLanding",genericPageObject); 
})