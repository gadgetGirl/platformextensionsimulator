
$(function() {
	function genericPageObject() {

	}
	genericPageObject.prototype = {
		init : function() {

		},
		updateDownloadUrl : function(selection) {
			var value = $(selection).attr('data-value');
			
			$('#mainDownloadBtn').attr('onclick',"fnb.controls.controller.eventsObject.raiseEvent('doDownload','/banking/Controller?nav=transactionhistory.advancedsearch.navigator.TransactionHistoryAdvancedSearchDownload&downloadFormat="+value+"'); fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', ''); return false;");
		}
	}
	namespace("fnb.transactionhistory.TransactionHistoryAdvancedSearchDownloadEzi",genericPageObject); 
	
});


