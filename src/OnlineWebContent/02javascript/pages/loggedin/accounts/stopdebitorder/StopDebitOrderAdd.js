var doCheck = true;
var goSelected = false;

//ive stopped trying to understand the workings of the javascript on this page - its "special" to say the least - 
//but ive added this cancel function that simply takes the selected accounts rfn and redirects back to the settings
//subtab... MG  
function cancelStop(){
	var dropdown = document.getElementById("account");
	parent.result.location = "/banking/Controller?formname=ACCOUNTS_TAB_FORM&action=prepareMaintainAccount&ANRFN="+dropdown.options[dropdown.selectedIndex].value;
}

function checkForm() {	
	if (!doCheck) {
		return true;
	}
	
	if (!checkMultiple()) {
		return false;
	}

	var dayOfMonth = getFormElementById("STOP_DEBIT_ORDER_ADD","dayofMonth").value;

	if (goSelected) {
		if (dayOfMonth == 0) {
			alertO("Select a day of the month to get a list of debit orders deducted on that day in the previous month.");
			resetMultiple();
			return false;
		}

	} else {
		if (debitOrderInstructions_getDebitOrders_size == 0 && dayOfMonth == 0) {
			alertO("Select a day of the month to get a list of debit orders deducted on that day in the previous month.");
			resetMultiple();
			return false;
		} 
		
		var currentRowSelection;
		var currentRowAmountFrom;
		var currentRowAmountTo;
		var debitOrderSelected = false;
		var debitOrderIndex = 0;
		var msg = "";

		for (var i=0; i < debitOrderInstructions_getDebitOrders_size; i++){
				debitOrderSelected = getFormElementsByName("STOP_DEBIT_ORDER_ADD","stopDebitOrder")[i].checked;
				debitOrderIndex = getFormElementsByName("STOP_DEBIT_ORDER_ADD","stopDebitOrder")[i].id;
				if (debitOrderSelected) {
					currentRowAmountFrom = document.getElementById('amountfrom_' + debitOrderIndex).value;
					currentRowAmountTo = document.getElementById('amountto_' + debitOrderIndex).value;

					if (currentRowAmountFrom == '' || currentRowAmountTo == '' || parseInt(currentRowAmountFrom) > parseInt(currentRowAmountTo)) {
						alertO("Please enter an amount from and an amount to. The amount to must be the same or greater than the amount from.");
						resetMultiple();
						return false;
					}


					if (currentRowAmountFrom == 0 || currentRowAmountFrom > 999999999.99 || currentRowAmountTo == 0 || currentRowAmountTo > 999999999.99 ) {
						alertO("Please enter an amount between 0 and 999999999.99");
						resetMultiple();
						return false;
					}

					if (!parent.menu.isAmount(currentRowAmountFrom)) {
						msg += "Amount from is not a valid number \n";
					}

					if (!parent.menu.isAmount(currentRowAmountTo)) {
						msg += "Amount to is not a valid number";
					}

					if (msg.length > 0 ) {
						alertO(msg);
						resetMultiple();
						return false;
					}

					break;
				}
		}

		if (!debitOrderSelected) {
			alertO("Please select a debit Order");
			resetMultiple();
			return false;
		}
		
	}
	return true;
}

function checkSelection2(lastDebitAmount, row) {
	if (getFormElementsByName('STOP_DEBIT_ORDER_ADD','stopDebitOrder')[row].value == StopDebitOrderViewBean_STOP_NOT_REQUIRED) {
		document.getElementById('amountfrom_' + row).style.visibility='hidden';
		document.getElementById('amountfrom_' + row).style.display = 'none';
		document.getElementById('amountfrom_' + row).disabled = '';

		document.getElementById('amountto_' + row).style.visibility='hidden';
		document.getElementById('amountto_' + row).style.display = 'none';
	} else if (getFormElementsByName('STOP_DEBIT_ORDER_ADD','stopDebitOrder')[row].value == StopDebitOrderViewBean_STOP_SPECIFIC_AMOUNT) { 
		alertO(lastDebitAmount);
		document.getElementById('amountfrom_' + row).value = lastDebitAmount;

		document.getElementById('amountfrom_' + row).style.visibility='visible';
		document.getElementById('amountfrom_' + row).style.display = '';
		document.getElementById('amountfrom_' + row).disabled = 'disabled';
		
		document.getElementById('amountto_' + row).style.visibility='hidden';
		document.getElementById('amountto_' + row).style.display = 'none';
	} else {
		document.getElementById('amountfrom_' + row).style.visibility='visible';
		document.getElementById('amountfrom_' + row).style.display = '';
		document.getElementById('amountfrom_' + row).disabled = '';

		document.getElementById('amountto_' + row).style.visibility='visible'
		document.getElementById('amountto_' + row).style.display = '';
	}
		
}


function checkSelection(lastDebitAmount, row) {
	if (getFormElementsByName('STOP_DEBIT_ORDER_ADD','stopDebitOrder')[row].value == StopDebitOrderViewBean_STOP_NOT_REQUIRED) {
		document.getElementById('amountfrom_' + row).value = '';
		document.getElementById('amountto_' + row).value = '';
		
		document.getElementById('amountfrom_' + row).disabled = 'disabled';
		document.getElementById('amountto_' + row).disabled = 'disabled';
	} else if (getFormElementsByName('STOP_DEBIT_ORDER_ADD','stopDebitOrder')[row].value == StopDebitOrderViewBean_STOP_SPECIFIC_AMOUNT) { 
		document.getElementById('amountfrom_' + row).value = lastDebitAmount;

		document.getElementById('amountfrom_' + row).disabled = 'disabled';
		document.getElementById('amountto_' + row).disabled = 'disabled';
		
	} else {
		document.getElementById('amountfrom_' + row).disabled = '';
		document.getElementById('amountto_' + row).disabled = '';
	}
		
}

function populateAmounts(counter, lastDebitOrderAmount) {
	document.getElementById('amountfrom_' + counter).value = lastDebitOrderAmount;
	document.getElementById('amountto_' + counter).value = lastDebitOrderAmount;
	document.getElementById('stopOrderCounter').value = counter 
	
	for (var i=0; i < debitOrderInstructions_getDebitOrders_size; i++ ) {
		if (i != counter) {
			document.getElementById('amountfrom_' + i).value = '';
			document.getElementById('amountto_' + i).value = '';
		}
	}

	return true;
	
}

function executeSelection() {
	parent.result.location = "/banking/Controller?formname=STOP_DEBIT_ORDER_ADD&action=accountChanged&ANRFN=" + document.getElementById('account').value;
	return false;
}