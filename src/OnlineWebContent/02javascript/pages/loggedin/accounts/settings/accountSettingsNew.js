///-------------------------------------------///
/// developer: Donovan
///
/// AccountMaintenanceChequeDashboard.jsp javascript
///-------------------------------------------///

fnb.hyperion.controller.extendPage({
	//Function reference for this page object
	functionReference: "accountSettings",
	//Shortcut for namespacing
	context: 'fnb.hyperion.controller.page.',
	//init method
	init : function(){
		//Bind Events
		this.bindEvents();
	},
	//Bind page events
	bindEvents : function(){
		//List of events for this module
    	var events = [{events:'click', selector: '[data-role="dropdownItem"]', handler: this.context+'accountSettings.loadAccountSettings(event)'}];
    	//Append events to actions module
    	fnb.hyperion.actions.addEvents(events);
	},
	//Load account settings on dropdown change
	loadAccountSettings : function(event){
		//Get selcted target
		var target = fnb.hyperion.$(event.currentTarget);
		//var rfn=$('#fromAccount').val();
		var val= target.attr('data-value');
		//Raise loadPage event
		fnb.hyperion.controller.raiseEvent('loadPage', {url: "/banking/Controller?nav=navigator.AccountView&anrfn="+val}); 
	}
});