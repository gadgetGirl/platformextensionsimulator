$(function() {

	function DeviceArrearsGraphController() {

		this.colors;
		this.total;
		this.arrears;
		this.totalMinusArrears;
		this.data;
		this.labels;
		this.dataPercents;
		this.skin;

	}
	;

	DeviceArrearsGraphController.prototype = {

		init : function(configObject) {

			var parentObject = this;
			
			if(configObject.skin == '1') {
				parentObject.colors = configObject.rmbColors;
			}else if(configObject.skin == '2'){
				parentObject.colors = configObject.discoveryColors;
			}else {
				parentObject.colors = configObject.colors;	
			}
			
			parentObject.labels = configObject.labels;
			parentObject.total = configObject.total;
			parentObject.arrears = configObject.arrears;
			parentObject.totalMinusArrears = parentObject.total - parentObject.arrears;
			parentObject.dataPercents = [parentObject.arrears / parentObject.total * 100,parentObject.totalMinusArrears / parentObject.total * 100 ]
			parentObject.data = [ parentObject.arrears,	parentObject.totalMinusArrears ];
			parentObject.drawPieChart();

		},
		positionLabels : function() {
				
			var parentObject = this;
			var lastend = 0;
			var laststart = 0;
			var angle = 0;

			for ( var i = 0; i < parentObject.data.length; i++) {
				parentObject.labels[i].find('.value').append(Math.round(parentObject.dataPercents[i]) + '%')
			}
		},
		drawPieChart : function() {

			var parentObject = this;
			$('#graphContainer').find('.dynamicsparkline').sparkline(parentObject.data, {type : 'pie',sliceColors : parentObject.colors,width : '150px',height : '150px',disableInteraction : true});
			parentObject.positionLabels();
		}
	};
	namespace("fnb.pages.DeviceArrearsGraphController",	DeviceArrearsGraphController);
});