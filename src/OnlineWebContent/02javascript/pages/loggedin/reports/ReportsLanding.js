$(function() {
	function genericPageObject() {
		this.graphDataSets = [];
	}
	genericPageObject.prototype = {
		init : function() {
			var parent = this;
			//parent.configObject = dataSource;
		},
		createDataSet : function(object) {
			this.graphDataSets.push(object);
		},
		createLandingGraph : function() {
			var date = new Date();
			var month = date.getMonth();
			this.createGraph(pages.fnb.complex.ReportsLanding.graphDataSets[month]);	
		},
		createGraph : function(dataSet) {
			fnb.utils.graph.type = "bar";
			fnb.utils.graph.init(dataSet,"#currentMonthWrapper");
		},
		loadBillinData : function() {
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:'/banking/Controller?nav=reports.onlineBilling.BillingLanding&action=onebilling_presentment', target:"#onlineBilling"});
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#onlineBilling'}, {show:'false',element:'#cashAnalytics'}, {show:'false',element:'#chequeAnalytics'}]);
		},
		allClicked : function(box) {
			var isChecked = $('#allUsers:checked').val()?true:false;
			if(isChecked){
				fnb.forms.dropdown.disable($('#inUserActive'));		
				
				/*if(deletedSize > 1){	*/		
					fnb.forms.dropdown.disable($('#inUserDeleted'));		
				/*}*/
			}else {
				fnb.forms.dropdown.enable($('#inUserActive'));		
				
				/*if(deletedSize > 1){*/
					fnb.forms.dropdown.enable($('#inUserDeleted'));		
				/*}*/
			}
		}

	}

	namespace("fnb.reports.complex.ReportsLanding",genericPageObject);
});


