$(function() {
	
	function toggleResultFormats(importType) {
		switch (importType) {
			case "CSV": 
				$("#resultsFormat_parent [data-value='ACB']").addClass("hidden");
				$("#resultsFormat_parent [data-value='CSV']").removeClass("hidden");
				fnb.forms.dropdown.setValue($("#resultsFormat_parent"), "CSV", "CSV");
				break;
			case "ACB": 
				$("#resultsFormat_parent [data-value='CSV']").addClass("hidden");
				$("#resultsFormat_parent [data-value='ACB']").removeClass("hidden");
				fnb.forms.dropdown.setValue($("#resultsFormat_parent"), "ACB", "ACB");
				break;
			default:
				console.info("importType: " + importType)
				break;
		}
	}
	function genericPageObject() {
	}
	genericPageObject.prototype = {
		pageLoaded : function() {
			var parent = this;
			var itemValue = $("#importFormat")[0].value;
			toggleResultFormats(itemValue);
		},
		setDropdownValues: function(event) {
			var item = event.currentTarget;
			toggleResultFormats(item.getAttribute("data-value"));
		}
	}
	namespace("fnb.reports.VerifyAccountOwnerImport",genericPageObject); 
});

