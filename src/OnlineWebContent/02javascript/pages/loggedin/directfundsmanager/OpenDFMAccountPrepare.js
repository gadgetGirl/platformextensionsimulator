$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){
			var parent = this;
		    parent.configObject = dataSource;
		    this.changeAddToNewGroup(!parent.configObject.addToNewGroup);
		},
		destroy: function(){
			var parent = this;
			parent = null;
		},
		
		changeAddToNewGroup: function(isTrue){
			if (isTrue) {
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'true',
					element : '#existingGroup'
				}, {
					show : 'false',
					element : '#newGroup'
				} ])
			} else {
				fnb.functions.showHideToggleElements.showHideToggle([ {
					show : 'true',
					element : '#newGroup'
				}, {
					show : 'false',
					element : '#existingGroup'
				} ])
			}
		}
	}
	
	namespace("fnb.directfundsmanager.OpenDFMAccountPrepare",genericPageObject);
});