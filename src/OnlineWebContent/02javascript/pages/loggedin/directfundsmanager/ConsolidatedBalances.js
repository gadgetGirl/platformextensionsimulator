$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){
			var parent = this;
		    parent.configObject = dataSource;
		},
		destroy: function(){
			var parent = this;
			parent = null;
		},
		
		changeTrustDropdown: function(selectElement){
			parent.result.location = "/banking/Controller?formname=DFM_LANDING_FORM&action=closed_account_history"
					+ "&ANRFN=" + selectElement;
			return true;
		}
	}
	
	namespace("fnb.directfundsmanager.ConsolidatedBalances",genericPageObject);
});