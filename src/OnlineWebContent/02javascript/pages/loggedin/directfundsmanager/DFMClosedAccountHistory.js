$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){
			var parent = this;
		    parent.configObject = dataSource;
		},
		destroy: function(){
			var parent = this;
			parent = null;
		},
		
		changeTrustDropdown: function(selectElement){
			fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','/banking/Controller?nav=directfundsmanager.DFMClosedAccountHistory&action=closed_account_history&anrfn=' + selectElement);
			return true;
		}
	}
	
	namespace("fnb.directFundManager.DFMClosedAccountHistory",genericPageObject);
});