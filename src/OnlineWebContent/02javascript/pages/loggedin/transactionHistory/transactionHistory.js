$(function() {
	function genericPageObject(){
		this.configObject = {};
		
	}
	genericPageObject.prototype = {
		quickSearch : function() {
			var checkValue=$('#isSimple').val();
			$('input[name^="stdQuickCustom"]').val("quick");
			
			$('#dateSelectQuick').removeClass('displayNone');
			$('#dateSelect').addClass('displayNone');
			$('#dateSearchContainer').addClass('displayNone');
			$('#advFormSubmitBtn').addClass('displayNone');
			$('#stdFormSubmitBtn').removeClass('displayNone');
			$('#standardSearchBtn').addClass('displayNone');
			$('#advancedSearchBtn').removeClass('displayNone');
			$('#standardSearch').removeClass('displayNone');
			$('#advancedSearch').addClass('displayNone');
			$('#stdExportFormSubmitBtn').addClass('displayNone');
			$('#advExportFormSubmitBtn').addClass('displayNone');
		},
		customSearch : function() {
			var checkValue=$('#isSimple').val();
			$('input[name^="stdQuickCustom"]').val("custom");
			
			$('#dateSelectQuick').addClass('displayNone');
			$('#dateSelect').removeClass('displayNone');
			$('#dateSearchContainer').removeClass('displayNone');
			$('#advFormSubmitBtn').addClass('displayNone');
			$('#stdFormSubmitBtn').removeClass('displayNone');
			$('#standardSearchBtn').addClass('displayNone');
			$('#advancedSearchBtn').removeClass('displayNone');
			$('#standardSearch').removeClass('displayNone');
			$('#advancedSearch').addClass('displayNone');
			$('#stdExportFormSubmitBtn').removeClass('displayNone');
			$('#advExportFormSubmitBtn').addClass('displayNone');
			
			if(checkValue=="true"){
				//console.log("Event Controller hide()--?"+checkValue);
				$('#stdExportFormSubmitBtn').remove();//hide export button for zob,export button is intended for zbi
			}
		},
		advancedSearch : function() {
			var checkValue=$('#isSimple').val();
			$('#advFormSubmitBtn').removeClass('displayNone');
			$('#stdFormSubmitBtn').addClass('displayNone');
			$('#standardSearchBtn').removeClass('displayNone');
			$('#advancedSearchBtn').addClass('displayNone');
			$('#standardSearch').addClass('displayNone');
			$('#advancedSearch').removeClass('displayNone');
			$('#stdExportFormSubmitBtn').addClass('displayNone');
			$('#advFormSubmitBtn').removeClass('displayNone');
			$('#advExportFormSubmitBtn').removeClass('displayNone');
			
			if(checkValue=="true"){
				//console.log("Event Controller hide()--?"+checkValue);
				$('#advExportFormSubmitBtn').remove();//hide export button for zob,export button is intended for zbi
			}
		},
		exportSubmit : function(type) {
			if(type=='std'){
				$("form[name=transactionHistorySearchForm] input[id=export]").val("true");
				$("form[name=transactionHistorySearchForm] input[id=nav]").val("transactionhistory.export.navigator.ImmediateExportConfirmation");
				fnb.utils.eziSlider.submit('transactionHistorySearchForm');
			}else{
				$("form[name=advTransactionHistorySearchForm] input[id=export]").val("true");
				fnb.utils.eziSlider.submit('advTransactionHistorySearchForm');
			}
		}
	}
	namespace("fnb.transactionhistory.TransactionHistorySearchLanding",genericPageObject);
				
});