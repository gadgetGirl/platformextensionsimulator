$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){
			var parent = this;
		    parent.configObject = dataSource;
		},
		destroy: function(){
			var parent = this;
			parent = null;
		},
		
		getDropdownValue: function(dropDownType, me, divName){
			var val=$(me).attr('data-value');
			
			if (val == 0 || val == "Please select"){
				
				if (divName == 'voucherTypeDiv'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#rechargeDiv'} ]);		
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#vouchersContainer'} ]);
				}
				
				if (divName == 'vouchersDiv'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#rechargeDiv'} ]);		
				}
					
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#flexiDiv'} ]);	
				return false;
			}
			
			
			if (val.indexOf("lexi") !== -1){
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#flexiDiv'} ]);	
				$(flexiAmount).attr("value","");
			}
			else{
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#flexiDiv'} ]);	
			}
			
			if (divName !== 'rechargeDiv'){
				fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=buy.airtimeandbundles.navigator.AirtimeBundlesDropdownBuilder&airtimeDropdownType=' + dropDownType + '&airtimeDropDownValue='+val+'&airtimeDropdownDiv='+divName,divName);
				if (divName == 'vouchersDiv'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#rechargeDiv'} ]);
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#vouchersContainer'} ]);	
				}
				else{
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#vouchersContainer'} ]);	
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#rechargeDiv'} ]);			
				}
			}
		},
		toggleStopOption: function(val){
			if(val == 'S'){
				fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#stopDateContainer'});
			}else{
				fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:'#stopDateContainer'});
			}
		},
		isEditingScheduled: function(value1,url1,url2){

			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#rechargeDiv'} ]);		
			fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#vouchersContainer'} ]);	

			if (value1.indexOf("lexi") !== -1){
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#flexiDiv'} ]);	
			}

			fnb.functions.loadDropDownDiv.load(url1,'voucherTypeDiv');
			fnb.functions.loadDropDownDiv.load(url2,'vouchersDiv');


		}
	}
	
	namespace("fnb.buy.airtimeandbundles.AirtimeBundlesCapture",genericPageObject);
});