$(function(){
	function genericPageObject(){
		this.configObject = {};
		this.flexi = '';
		this.val = '';
	}
	genericPageObject.prototype = {

			init: function(dataSource){
				var parent = this;
				parent.configObject = dataSource;
				this.displayDivs();
			},
			destroy: function(){
				var parent = this;
				parent = null;
			},

			displayDivs: function(){
				if (this.configObject.val != 'Telkom Landline'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#showCell'} ]);
				}
				if (this.configObject.flexi == 'true'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#flexiDiv'} ]);
				}
			},
			getDropdownValue: function(dropDownType, me, divName){
				var val=$(me).attr('data-value');

				if (val == 0 || val == "Please select"){

					if (divName == 'voucherTypeDiv'){
						fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#rechargeDiv'},{show : 'false', element : '#vouchersContainer'}]);		
					}

					if (divName == 'vouchersDiv'){
						fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#rechargeDiv'} ]);		
					}

					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#flexiDiv'} ]);	
					return false;
				}

				if (val.indexOf("lexi") !== -1){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#flexiDiv'} ]);	
				} else{
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#flexiDiv'} ]);	
				}

				if (divName == 'voucherTypeDiv'){
					if (val == 'Telkom Landline')
						fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#showCell'} ]);
					else
						fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#showCell'} ]);
				}

				if (divName !== 'rechargeDiv'){
					fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=buy.airtimeandbundles.navigator.AirtimeBundlesDropdownBuilder&airtimeDropdownType=' + dropDownType + '&airtimeDropDownValue='+val+'&airtimeDropdownDiv='+divName,divName);

					if (divName == 'vouchersDiv'){
						fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#rechargeDiv'},{show : 'true', element : '#vouchersContainer'}]);
					} else{
						fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#vouchersContainer'},{show : 'false', element : '#rechargeDiv'}]);			
					}
				}
			},
			shLabel: function(obj){
				var v = $(obj).attr('data-value');
				if (v != 0)
					fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:'#voucherLabel'});
				else
					fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#voucherLabel'});
			},
			isEditing: function(value1,value2,url1,url2){

				fnb.functions.showHideToggleElements.showHideToggle([{show : 'true', element : '#rechargeDiv'},
				                                                     {show : 'true', element : '#vouchersContainer'},
				                                                     {show : 'false', element : '#flexiDiv'}]);		

				if (value2 != 'Telkom Landline'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#showCell'} ]);
				}

				if (value1 == 'true'){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#flexiDiv'} ]);
				}
				else{
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#flexiDiv'} ]);
				}

				fnb.functions.loadDropDownDiv.load(url1,'voucherTypeDiv');
				fnb.functions.loadDropDownDiv.load(url2,'vouchersDiv');


			},
			toggleStopOption: function(val){
				if(val == 'S'){
					fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#stopDateContainer'});
				}else{
					fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:'#stopDateContainer'});
				}
			},
			isEditingScheduled: function(value1,url1,url2){

				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#rechargeDiv'},
				                                                      {show : 'true', element : '#vouchersContainer'}]);

				if (value1.indexOf("lexi") !== -1){
					fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#flexiDiv'} ]);	
				}

				fnb.functions.loadDropDownDiv.load(url1,'voucherTypeDiv');
				fnb.functions.loadDropDownDiv.load(url2,'vouchersDiv');


			}
	}

	namespace("fnb.buy.airtimeandbundles.AirtimeBundlesCapture",genericPageObject);
});