$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(dataSource){
			var parent = this;
		    parent.configObject = dataSource;
		},
		destroy: function(){
			var parent = this;
			parent = null;
		},
		
		shAmount: function(val){
			if (val.indexOf("lexi") !== -1){
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'true', element : '#flexiDiv'} ]);
				$('#amount').attr("value","0.00");
			}
			else{
				fnb.functions.showHideToggleElements.showHideToggle([ {show : 'false', element : '#flexiDiv'} ]);	
				$('#amount').attr("value","0.00");
			}
		}
	}
	
	namespace("fnb.buy.electricity.BuyElectricityLanding",genericPageObject);
});