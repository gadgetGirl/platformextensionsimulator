$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.reloadPage();
						
		}, 
		
		reloadPage : function(){
			var parent = this;
			var isPendingStatus = parent.configObject.pendingStatus;
			var waitTime = parent.configObject.waitTime
			var newString = waitTime.substr(1,waitTime.length-2)			
			var url = parent.configObject.pageUrl;
			var timeArray = newString.split(',');
			
			if(isPendingStatus == 'true'){
				setTimeout(function(){
				
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#multichannelPrepaidFinishTable_tableContent"});
				}, timeArray[0]);
				setTimeout(function(){
					
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#multichannelPrepaidFinishTable_tableContent"});
				}, timeArray[1]);
				setTimeout(function(){
					
					fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:"#multichannelPrepaidFinishTable_tableContent"});
				}, timeArray[2]);
			}
		}
	}
	namespace("pages.fnb.buy.multichannelprepaid.MultiChannelPrepaidResults",genericPageObject); 	
});