$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();			
		}, 

		pageLoaded : function() {
			var parent = this;
		},
		
		loadAccountCards : function(obj){
			var anrfn=$(obj).attr('data-value');
			fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen',"/banking/Controller?nav=cards.complex.navigator.AccountLinkedCards&anrfn="+anrfn);
		}
	}
	namespace("fnb.cards.complex.AccountLinkedCards",genericPageObject); 
	
});