$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();			
		}, 

		pageLoaded : function() {
			var parent = this;
			
			if(parent.configObject.cardType != ""){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.contactInfo'}]);
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.contactInfo'}]);
			}
				
			switch(parent.configObject.deliveryMethod){
				case "B"://show branch 
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
					                   {show:'true',element:'.collectFromAnFNBBranch'},
					                   {show:'false',element:'.postItToMe'}]);
					break;
				case "C"://show courier
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.deliveredToMe'},
					                   {show:'false',element:'.collectFromAnFNBBranch'},
					                   {show:'false',element:'.postItToMe'}]);
					break;
				case "P": //show post
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
					                   {show:'false',element:'.collectFromAnFNBBranch'},
					                   {show:'true',element:'.postItToMe'}]);
					break;
				default: //hide all
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
					                   {show:'false',element:'.collectFromAnFNBBranch'},
					                   {show:'false',element:'.postItToMe'}]);
					break;
			}
		},

		loadCardTypesAndBranches : function(me, divName){
			var val = $(me).attr('data-value');
			if(val != null && val != "" && val != "0"){
				fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=cards.navigator.CardReplacementCardType&anrfn=' + val + '&divName='+divName,divName);
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},{show:'false',element:'.collectFromAnFNBBranch'},
				                   {show:'false',element:'.postItToMe'},{show:'false',element:'.contactInfo'}]);
			}
		},

		searchBranch : function(){
			var val = $("#branchSearchName").val();
			if(val != null && val != "" && val != "0"){
				var url = '/banking/Controller?nav=cards.navigator.FNBBranchNameSearch&search=Search&keyword=' + val;
				fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: url});
			}else{
				var errorMessage = "Please enter a branch name keyword to search";
				fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
			}
		},

		getRadioButtons : function(me,divName){
			var cardType = $(me).attr('data-value');
			if(cardType != null && cardType != "" && cardType != "0"){
				fnb.functions.loadDropDownDiv.load('/banking/Controller?nav=cards.navigator.CardRadioButtonsBuilder&cardType='+cardType+'&divName='+divName,divName);
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.contactInfo'}]);
			}
		}
	}
	namespace("fnb.cards.complex.CardReplacementCapture",genericPageObject); 
	
});


