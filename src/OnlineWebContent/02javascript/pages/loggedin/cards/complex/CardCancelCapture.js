$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();			
		}, 

		pageLoaded : function() {
			var parent = this;
			parent.hideFields();
		},
		

		/**
		 * If the card is a Credit Card and It is Lost/Stolen an incident report is generated.
		 */
		showCancelInfo : function(me){
			var parent = this;
			var val = $(me).attr('data-value').trim();
			
			if(parent.configObject.isCreditCard == 'true'){
				//LSO = Lost; LS1 = Stolen - Commercial
				//Lost; Stolen - Consumer
				if(val == 'LS0' || val == 'LS1' || val == 'Lost' || val == 'Stolen'){
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.cancelInfo'}]);
				}else{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.cancelInfo'}]);
				}
			}
		},

		/**
		 * 
		 */
		hideFields : function(){
			var parent = this;
			//alert("Is Credit Card = "+isCreditCard+", Cancel Reason = " + cancelReason);
			if(parent.configObject.isCreditCard == 'true'){
				//LSO = Lost; LS1 = Stolen - Commercial
				//Lost; Stolen - Consumer
				if(parent.configObject.cancelReason == 'LS0' || 
						parent.configObject.cancelReason == 'LS1' || 
						parent.configObject.cancelReason == 'Lost' || 
						parent.configObject.cancelReason == 'Stolen'){
					
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.cancelInfo'}]);
				}else{
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.cancelInfo'}]);
				}
			}
		}

	}
	namespace("fnb.cards.complex.CardCancelCapture",genericPageObject); 
	
});