$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {
			
		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();			
		}, 
			
		pageLoaded : function() {
			var parent = this;
		},
		
		replace : function(){
			var parent = this;
			
			var isCreditCard = parent.configObject.isCreditCard;
			var cardNumber = parent.configObject.cardNumber;
			var cardType = parent.configObject.cardType;
			var maskedCardNumberKey = parent.configObject.maskedCardNumberKey;
			var isLimitUpdate = parent.configObject.isLimitUpdate;
			var cardStatus = parent.configObject.cardStatus;
			var accountNumber = parent.configObject.accountNumber;
			var cardHolder = parent.configObject.cardHolder;
			var anrfn = parent.configObject.anrfn;
			
			var pUrl = "";
			if(isCreditCard == 'true'){
				pUrl = "/banking/Controller?nav=cards.complex.navigator.CreditCardReplacementLanding&cardNumber="+cardNumber+"&cardNumberSelected="+maskedCardNumberKey+
					"&cardTypeSelected="+cardType+"&crdCard="+isCreditCard+"&crdLimit="+isLimitUpdate+"&cardStatus=Cancelled&accountNumber="+accountNumber+
					"&cardHolder="+cardHolder;
			}else{
				pUrl = "/banking/Controller?nav=cards.complex.navigator.CardReplacementCapture&maskedCardNumberKey="+maskedCardNumberKey+"&anrfn="+anrfn;
			}
						
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: pUrl, preLoadingCallBack:fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '')});
		}	
	}
	namespace("fnb.cards.complex.CardCancelResult",genericPageObject); 		
});
	