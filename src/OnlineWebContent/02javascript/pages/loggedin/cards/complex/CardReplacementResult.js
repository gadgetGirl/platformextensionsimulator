$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();			
		}, 

		pageLoaded : function() {
			var parent = this;
			
			if(parent.configObject.cardType != ""){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.contactInfo'}]);
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.contactInfo'}]);
			}
				
			switch(parent.configObject.deliveryMethod){
				case "B"://show branch 
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
					                   {show:'true',element:'.collectFromAnFNBBranch'},
					                   {show:'false',element:'.postItToMe'}]);
					break;
				case "C"://show courier
					fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.deliveredToMe'},
					                   {show:'false',element:'.collectFromAnFNBBranch'},
					                   {show:'false',element:'.postItToMe'}]);
					break;
				case "P": //show post
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
					                   {show:'false',element:'.collectFromAnFNBBranch'},
					                   {show:'true',element:'.postItToMe'}]);
					break;
				default: //hide all
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
					                   {show:'false',element:'.collectFromAnFNBBranch'},
					                   {show:'false',element:'.postItToMe'}]);
					break;
			}
		}		
	}
	namespace("fnb.cards.complex.CardReplacementResult",genericPageObject); 
	
});