var hideFields=function(){
	
	if(cardType != ""){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.contactInfo'}]);
	}else{
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.contactInfo'}]);
	}
		
	switch(deliveryMethod){
		case "B"://show branch 
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
			                   {show:'true',element:'.collectFromAnFNBBranch'},
			                   {show:'false',element:'.postItToMe'}]);
			break;
		case "C"://show courier
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.deliveredToMe'},
			                   {show:'false',element:'.collectFromAnFNBBranch'},
			                   {show:'false',element:'.postItToMe'}]);
			break;
		case "P": //show post
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
			                   {show:'false',element:'.collectFromAnFNBBranch'},
			                   {show:'true',element:'.postItToMe'}]);
			break;
		default: //hide all
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
			                   {show:'false',element:'.collectFromAnFNBBranch'},
			                   {show:'false',element:'.postItToMe'}]);
			break;
	}
}