
$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
		}, 

		pageLoaded : function() {
			var parent = this;
		},
		
		/**
		 * 
		 */
		errorMessage : function(errorMessage){
			setTimeout(function(){
				fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
			}, 20);
		},
		
		/**
		 * reference: change_card_pin_ibm.jsp
		 */
		doAjax : function(){
			
			var parent = this;
			if ($('#terms').parent().hasClass('checked')){
				var cardNumber = parent.configObject.cardNumber;
				var ticket = parent.configObject.ticket;
				var pin = $("#pin").val();
				var confirmPin = $("#pinConfirm").val();
				var serverUrl = parent.configObject.url;
				var url = serverUrl + "/changepin?ticket="+ticket+"&PIN=" +pin;
				
				var errorMessage = "";
				if(pin != confirmPin){
					errorMessage = 'PIN number must be the same.';
					parent.errorMessage(errorMessage);
					return false;
				}
				
				if(pin.length != 4){
					errorMessage = 'PIN number must be 4 digits long.';
					parent.errorMessage(errorMessage);
					return false;
				}
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#eziPannelButtons'}]);
				document.getElementById("resIFrame").src=url;
				timer=setTimeout(parent.checkResponse,1500);
				return true;
			} else {
				parent.errorMessage('Please accept the Terms and Conditions.')
				return false;
			}
		},
		
		checkResponse : function(){
			var parent = this;
			clearTimeout(timer);
			fnb.utils.eziSlider.submit('cardCreatePIN');
			//Main.loadUrlToWorkspace("/banking/Controller?formname=VIEW_PIN_IBM&action=changePinConfirm", 10);
		}
	}
	namespace("fnb.forex.simple.CardCreatePINCapture",genericPageObject); 
	
});