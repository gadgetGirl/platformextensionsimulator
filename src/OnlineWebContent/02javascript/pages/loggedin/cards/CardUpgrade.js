
$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {

			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
			
			var value = $('#deliveryOption22').val();
			
			
			
			if(value == 'B') {
				
				$('#deliveryAddress1').val("");
				$('#deliveryAddress2').val("");
				$('#suburb').val("");
				$('#city').val("");
				$('#postalCode').val("");
				
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#deliveredToMe'},{show:'true',element:'#collectFromAnFNBBranch'}]);	
			}else if(value == 'C'){
				
				$('#branchSearchCode').val("");
				$('#branchSearchName').val("");
								
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#deliveredToMe'},{show:'false',element:'#collectFromAnFNBBranch'}]);
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#deliveredToMe'},{show:'false',element:'#collectFromAnFNBBranch'}]);	
			}
			
			 
			
						
		}, 

		pageLoaded : function() {
			var parent = this;
		},
		
		searchBranch : function(){
			var val = $("#branchSearchName").val();
			if(val != null && val != "" && val != "0"){
				var url = '/banking/Controller?nav=cards.navigator.FNBBranchNameSearch&search=Search&keyword=' + val;
				fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: url});
			}else{
				var errorMessage = "Please enter a branch name keyword to search";
				fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
			}
		}

	}
	namespace("fnb.cards.CardUpgrade",genericPageObject); 
	
});


