var hideFields=function(){
	
	if(cardType != ""){
		fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.contactInfo'}]);
	}else{
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.contactInfo'}]);
	}
		
	switch(deliveryMethod){
		case "B"://show branch 
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
			                   {show:'true',element:'.collectFromAnFNBBranch'},
			                   {show:'false',element:'.postItToMe'},
			                   {show:'true',element:'.collectFromAnFnbSuite'},
			                   {show:'true',element:'.collectFromAnRmbSuite'}]);
			
			break;
		case "C"://show courier
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.deliveredToMe'},
			                   {show:'false',element:'.collectFromAnFNBBranch'},
			                   {show:'false',element:'.postItToMe'},
			                   {show:'false',element:'.collectFromAnFnbSuite'},
			                   {show:'false',element:'.collectFromAnRmbSuite'}]);
			break;
		case "P": //show post
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
			                   {show:'false',element:'.collectFromAnFNBBranch'},
			                   {show:'true',element:'.postItToMe'},
			                   {show:'false',element:'.collectFromAnFnbSuite'},
			                   {show:'false',element:'.collectFromAnRmbSuite'}]);
			break;
		case "PFNB"://show branch 
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
			                   {show:'true',element:'.collectFromAnFNBBranch'},
			                   {show:'false',element:'.postItToMe'},
			                   {show:'true',element:'.collectFromAnFnbSuite'},
			                   {show:'true',element:'.collectFromAnRmbSuite'}]);
			
			break;
		case "PRMB"://show branch 
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
			                   {show:'true',element:'.collectFromAnFNBBranch'},
			                   {show:'false',element:'.postItToMe'},
			                   {show:'true',element:'.collectFromAnFnbSuite'},
			                   {show:'true',element:'.collectFromAnRmbSuite'}]);
			
			break;
		default: //hide all
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},
			                   {show:'false',element:'.collectFromAnFNBBranch'},
			                   {show:'false',element:'.postItToMe'},
			                   {show:'false',element:'.collectFromAnFnbSuite'},
			                   {show:'false',element:'.collectFromAnRmbSuite'}]);
			break;
	}
}