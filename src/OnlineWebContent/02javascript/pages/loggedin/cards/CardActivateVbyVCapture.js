
$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();			
		}, 

		pageLoaded : function() {
			var parent = this;
			parent.hideFields();
		},
		
		hideFields : function(){
			var parent = this;
			var otpType = parent.configObject.otpType;
			
			if(otpType == 'S'){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.cellphoneNumberBlock'},{show :'false',element:'.emailAddressBlock'}]);
			}else if(otpType == 'E'){
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.cellphoneNumberBlock'},{show :'true',element:'.emailAddressBlock'}]);
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.cellphoneNumberBlock'},{show :'false',element:'.emailAddressBlock'}]);
			}
		}
	}
	namespace("fnb.cards.CardActivateVbyVCapture",genericPageObject); 
});