var searchBranch=function(){
	var val = $("#branchSearchName").val();
	if(val != null && val != "" && val != "0"){
		var url = '/banking/Controller?nav=cards.navigator.FNBBranchNameSearch&search=Search&keyword=' + val;
		fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: url});
	}else{
		var errorMessage = "Please enter a branch name keyword to search";
		fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
	}
}

