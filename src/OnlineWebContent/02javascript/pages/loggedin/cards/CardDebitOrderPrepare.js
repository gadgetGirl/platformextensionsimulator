$(function() {
	
	function genericPageObject() {
		this.configObject = {};
	}
	
	genericPageObject.prototype = {
	
		init : function(dataSource) {
						
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
			var type = $('#autoPayIndicator').val();
			
			this.toggleShowNoteDiv(type);
			
			parent.togglePaymentNote(type);
			
			if(type == '3') { //percentage payment
				 fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showFixedAmountDiv'},{show:'false',element:'#showPercentageDiv'}]);
			}else if(type == '6') {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showPercentageDiv'},{show:'false',element:'#showFixedAmountDiv'}]);
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showFixedAmountDiv'},{show:'false',element:'#showPercentageDiv'}]);
			}
		},
		
		pageLoaded : function() {
		
		},
		
		togglePaymentOption : function(obj) {
						
			var type=$(obj).attr('data-value');
					
											
			if(type == 3) { 
				 fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showFixedAmountDiv'},{show:'false',element:'#showPercentageDiv'}]);
			}else if(type == 6) {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showPercentageDiv'},{show:'false',element:'#showFixedAmountDiv'}]);
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showFixedAmountDiv'},{show:'false',element:'#showPercentageDiv'}]);
			}
			
			this.togglePaymentNote(type);					
		},
		
		togglePaymentNote : function(type) {
					
			this.toggleShowNoteDiv(type);		
		},
		
		
		toggleShowNoteDiv : function(type) {
			var fullAmount = '1';
			var minimumPayment = '2';
			var fixedAmount = '3';
			var actualStraightBalance = '4';
			var monthlyStatement = '5';
			var percentagePayment = '6';
			
			switch(type) {
				case fullAmount : 
						fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#fullAmountNoteDiv'},
						                                                     {show:'false',element:'#minimumPaymentNoteDiv'},
						                                                     {show:'false',element:'#fixedAmountNoteDiv'},
						                                                     {show:'false',element:'#actualStatementBalanceNoteDiv'},
						                                                     {show:'false',element:'#monthlyStatementNoteDiv'},
						                                                     {show:'false',element:'#percentagePaymentNoteDiv'}]);
															break;
				case minimumPayment :
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#fullAmountNoteDiv'},
						                                                     {show:'true',element:'#minimumPaymentNoteDiv'},
						                                                     {show:'false',element:'#fixedAmountNoteDiv'},
						                                                     {show:'false',element:'#actualStatementBalanceNoteDiv'},
						                                                     {show:'false',element:'#monthlyStatementNoteDiv'},
						                                                     {show:'false',element:'#percentagePaymentNoteDiv'}]);
															break;
				case fixedAmount :
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#fullAmountNoteDiv'},
						                                                     {show:'false',element:'#minimumPaymentNoteDiv'},
						                                                     {show:'true',element:'#fixedAmountNoteDiv'},
						                                                     {show:'false',element:'#actualStatementBalanceNoteDiv'},
						                                                     {show:'false',element:'#monthlyStatementNoteDiv'},
						                                                     {show:'false',element:'#percentagePaymentNoteDiv'}]);
															break;
				case actualStraightBalance : 
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#fullAmountNoteDiv'},
						                                                     {show:'false',element:'#minimumPaymentNoteDiv'},
						                                                     {show:'false',element:'#fixedAmountNoteDiv'},
						                                                     {show:'true',element:'#actualStatementBalanceNoteDiv'},
						                                                     {show:'false',element:'#monthlyStatementNoteDiv'},
						                                                     {show:'false',element:'#percentagePaymentNoteDiv'}]);
															break;
				case monthlyStatement : 
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#fullAmountNoteDiv'},
						                                                     {show:'false',element:'#minimumPaymentNoteDiv'},
						                                                     {show:'false',element:'#fixedAmountNoteDiv'},
						                                                     {show:'false',element:'#actualStatementBalanceNoteDiv'},
						                                                     {show:'true',element:'#monthlyStatementNoteDiv'},
						                                                     {show:'false',element:'#percentagePaymentNoteDiv'}]);
															break;						
				case percentagePayment : 
						fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#fullAmountNoteDiv'},
						                                                     {show:'false',element:'#minimumPaymentNoteDiv'},
						                                                     {show:'false',element:'#fixedAmountNoteDiv'},
						                                                     {show:'false',element:'#actualStatementBalanceNoteDiv'},
						                                                     {show:'false',element:'#monthlyStatementNoteDiv'},
						                                                     {show:'true',element:'#percentagePaymentNoteDiv'}]);
															break;
															
				default	:	
					fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#fullAmountNoteDiv'},
					                                                     {show:'false',element:'#minimumPaymentNoteDiv'},
					                                                     {show:'false',element:'#fixedAmountNoteDiv'},
					                                                     {show:'false',element:'#actualStatementBalanceNoteDiv'},
					                                                     {show:'false',element:'#monthlyStatementNoteDiv'},
					                                                     {show:'false',element:'#percentagePaymentNoteDiv'}]);
														break;
								
			} 		
		}
		
	}
	
	namespace("fnb.cards.CardDebitOrderPrepare",genericPageObject); 
})

