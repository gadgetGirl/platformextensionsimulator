var searchBranch=function(){
	var val = $("#branchSearchName").val();
	if(val != null && val != "" && val != "0"){
		var url = '/banking/Controller?nav=cards.navigator.FNBBranchNameSearch&search=Search&keyword=' + val;
		fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: url});
	}else{
		var errorMessage = "Please enter a branch name keyword to search";
		fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
	}
}


///-----------------------------------------------------------------------------------------///
///
/// Card Selection Picker FUNCTIONS 
///-----------------------------------------------------------------------------------------///
var fnbSelectionPicker = function(){};
var lastScroll = 0;
var scrollCount=1;
	
fnbSelectionPicker.prototype = {
		
	init: function(MainHolderName){
		var parent = this;
		this.MainHolder = MainHolderName;
	},
	
	destroy: function(){	
		var parent = this; 	
		parent = null;
	},
  
	show: function() {
		var windowHeight = $(window).height();
		var objectHeight = $(this.MainHolder).height();
		var topdisplayPosition = (windowHeight / 2) - (objectHeight / 2);
		var highlightClassName = ".cardImageHolder" + $('.selectorFirstField').val();
		
		lastScroll = 0;
		scrollCount = 1;
		
		$(this.MainHolder).css({ top: topdisplayPosition });
		$("#selectorHolderBody").animate({scrollLeft: 0});
		
		fnb.controls.controller.setOverlay();
		
		$(this.MainHolder).show()
	},
	
	hide: function() {
		fnb.controls.controller.clearOverlay();
		$(this.MainHolder).hide();
		fnbSelectionPickerObject.destroy();
	},
	
	scrollLeft: function(objectCount) {
		var scrollStep = $('#cardImageHolder').outerWidth(true);
		var bodySizer =$('#selectorHolderBody').outerWidth(true) / scrollStep;
		
		if (scrollCount < (objectCount - bodySizer) + 1) {
			scrollCount ++;
			lastScroll = lastScroll + scrollStep;
			$("#selectorHolderBody").animate({scrollLeft: lastScroll});
		}
	},
	
	scrollRight: function() {
		var scrollStep = $('#cardImageHolder').outerWidth(true);
		var bodySizer = Math.round($('#selectorHolderBody').outerWidth(true) / scrollStep);
		
		if (scrollCount > 1) {
			scrollCount --;
			lastScroll = lastScroll - scrollStep;
			$("#selectorHolderBody").animate({scrollLeft: lastScroll});
		}
	},
	
	pickCard: function(pickedCardValue) {
		var selectedInputName = "#img" + pickedCardValue;
		var selectedImageName = $(selectedInputName).attr("src");
		
		$('.selectorFirstField').val(pickedCardValue);
		$("#selectorFirstImage").attr("src", selectedImageName);
		
		fnbSelectionPickerObject.hide();
	},
};

var fnbSelectionPickerObject = new fnbSelectionPicker();
fnbSelectionPickerObject.init("#selectorHolder");