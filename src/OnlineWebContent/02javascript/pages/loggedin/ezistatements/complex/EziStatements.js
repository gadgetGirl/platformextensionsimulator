$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {

		init : function(dataSource) {
						
			var parent = this;
			parent.configObject = dataSource;
			parent.pageLoaded();
				
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showUrlDropdown'},{show:'true',element:'#showEmailAddress'}]);
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showEmailButton'},{show:'false',element:'#showDownloadButton'}]);
			fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showActioRadioButton'}]);
			
						
		},
		
		pageLoaded : function() {
			var parent = this;
			var anrfn = parent.configObject.statementsAvailable;
					
			if(anrfn != 0 && anrfn != "") {
				parent.retrieveSingleStatements(anrfn);
			}
			
		},
		
		retrieveStatements : function (obj) {
		
			var anrfn =	$(obj).attr('data-value');
						
			var radio = $('#action .radioGroupValue').val();
								
			var url = '/banking/Controller?nav=ezistatements.complex.navigator.EziStatementRetrieve&anrfn='+anrfn+'&action='+radio;
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#showStatementsList'}); 
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showStatementsList'}] );
					
		},
		
		showHButtons: function(obj) {
			if(obj == 'download') {
				fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showDownloadButton'},{show:'false',element:'#showEmailButton'},{show:'false',element:'#showEmailAddress'}]);
			}else{
				fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#showDownloadButton'},{show:'true',element:'#showEmailButton'},{show:'true',element:'#showEmailAddress'}]);
			}
		},
		
		retrieveSingleStatements : function (anrfn) {
							
			var radio = $('#action .radioGroupValue').val();
								
			var url = '/banking/Controller?nav=ezistatements.complex.navigator.EziStatementRetrieve&anrfn='+anrfn+'&action='+radio;
			
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:url, target:'#showStatementsList'}); 
			
			fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'#showStatementsList'}] );
			
						
	/*		$('#statementReference_dropId').find('.dropdown-selection-white').find('.dropdown-item-row').text('loading...');*/

			
		},		
			
		downloadFile : function(){
			
			var value = $('#statementReference').val();
			var action = $('#action .radioGroupValue').val();
				
			if(value == "freeEziStatementItemList") {
	
			var errorMessage = "You can only download 1 statement at the time";

				setTimeout(function(){
					fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
				}, 20);
				
				return false;
			}
									
			var url = '/banking/Controller?nav=ezistatements.complex.navigator.EziStatementConfirm&action='+action+'&statementReference='+value;

			
			fnb.controls.controller.eventsObject.raiseEvent('doDownload', url);
			
			
		}
	}
	
	namespace("ezistatements.complex.EziStatements",genericPageObject); 
	
})