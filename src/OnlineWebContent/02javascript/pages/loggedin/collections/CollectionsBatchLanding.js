$(function(){

	function genericPageObject(){
	}

	genericPageObject.prototype = {
			init: function(config){
				var me = this;
				me.config = config;
				me.start();
				return me;
			}
			,destroy: function(){
				var me = this;
			}
			,start: function(){
				var me = this;
				
			}
			,deleteSelected: function(){

				  var kbrfns = new Array();

				    var items = $('.col6').find('.checkbox-graphic-wrapper');
				    $.each(items, function(index, item) {
				    	if($(this).hasClass('checked')){
				    		kbrfns.push($(this).find('input').attr('value'));
				    	}
					});

					fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '/banking/Controller?nav=collections.CollectionDeleteWarning&kbrfn='+kbrfns);
			}
			,authSelected: function(){
		 		fnb.functions.submitFormToWorkspace.submit("CollectionBatchLanding");
			}
	}

	namespace("fnb.collections.landing",genericPageObject);

});