$(function() {
	function genericPageObject() {
		this.configObject = {};
	}
	genericPageObject.prototype = {
		submitUrlWithMultipleParams: function(url,keyValuePairsObject){
		
			var href = url
			var params =[]
			var paramString = ''			
			for (var key in keyValuePairsObject) {
				inputValue = $(keyValuePairsObject[key]).attr('value')==''? null :  $(keyValuePairsObject[key]).attr('value') 
				params.push(key+'='+inputValue);
			}
			if(params.length > 0){
				paramString=params.toString().replace(',','&');
			}else{
				paramString = '';
			}
		
			fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow',{url:href,params:paramString});
		}
	};
	namespace("fnb.collections.CollectionsOnceOffRecipientCapture",genericPageObject);
});