(function($){
	function genericPageObject(){
	
	}
	genericPageObject.prototype = {
			init: function(){
				
			},
			destroy: function(){
				var me = this;
			},
			show: function(item){
				$(item).removeClass('displayNone');
				return this;
			},
			hide: function(item){
				$(item).addClass('displayNone');
				return this;
			}
	}
	namespace("fnb.cashman.complex.CollectionsMarketinglanding",genericPageObject);
})(jQuery);	