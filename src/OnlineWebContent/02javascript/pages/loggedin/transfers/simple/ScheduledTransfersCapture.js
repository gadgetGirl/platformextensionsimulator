function toggleStopOption(val){
	if(val == 'S'){
		fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:'#stopDateContainer'});
		fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#specifyDate'},{show:'true',element:'#numberOfTransfers'}])
		$('#transferAmount').val('1');
		$('#transferAmount').attr("disabled", true);
		$('#type').children().removeClass('switcherWrapperSelected');
		$('#type').children('div:eq(2)').addClass('switcherWrapperSelected');
		$('#addScheduled').find('input[name^="expiryType"]').val('2')
		fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#type'});
		
	}else{
		fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:'#stopDateContainer'});
		fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:'#type'});
		$('#type').children().removeClass('switcherWrapperSelected');
		$('#transferAmount').attr("disabled", false);
		$('#transferAmount').val('');
		$('#addScheduled').find('input[name^="expiryType"]').val('0')
		fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#numberOfTransfers'});
		fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#specifyDate'});
	}
}