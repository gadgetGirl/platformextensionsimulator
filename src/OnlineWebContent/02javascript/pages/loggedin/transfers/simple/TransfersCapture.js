$(function() {
	function genericPageObject() {

	}
	genericPageObject.prototype = {
		init : function() {
			this.bindEvents();
			this.fromRefInput = $('#fromAccountReference');
			this.toRefInput = $('#toAccountReference');
		},
		createDataSet : function(object) {

		},
		bindEvents : function() {
			var _this = this;
			$('#refParent').on('keydown keyup', '#fromAccountReference', function(event) {_this.copyValues();});
		},
		copyValues : function() {
			var triggerUpdate = false;
			var toRefVal = this.toRefInput.val();
			var fromRefSub = this.fromRefInput.val().substr(0,(this.fromRefInput.val().length-1))
			if(toRefVal==fromRefSub||toRefVal==this.fromRefInput.val()||this.fromRefInput.val()==this.toRefInput.val().substr(0,(this.toRefInput.val().length-1))){
				triggerUpdate = true;
			}
			if(toRefVal==''||triggerUpdate == true){
				this.toRefInput.val(this.fromRefInput.val())
			}
		}

	}

	namespace("fnb.transfers.simple.TransfersCapture",genericPageObject);
});



