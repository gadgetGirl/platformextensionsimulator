$(function() {
	function genericPageObject(){
		/**
		* Currency radio button 1
		*/
		this.currencyRadio1;
		
		/**
		* Currency radio button 2
		*/
		this.currencyRadio2;
		
		/**
		 * The current transfer type
		 */
		this.transferType = this.NOT_SET;
				
		/**
		 * The transfer types
		 */
		this.NOT_SET = -1;
		this.ZAR_TO_ZAR = 0;  
		this.ZAR_TO_FCA = 1;  
		this.FCA_TO_ZAR = 2;
		this.FCA_TO_FCA = 3; /*NEW_TYPE_GOES_HERE : 4*/
		
		/**
		 * The various elements associated with each type of transfer - depending on the selected transfer type,
		 * we display one of the groups of elements in this array.
		 */
		this.pageElements = [];
		
		this.validationFunctions = [null, null, null, null];
		
		this.formActions = ["transfers.simple.navigator.TransferConfirm", 
   		               "transfers.simple.multicurrency.navigator.MultiCurrencyTransferZARtoFCAGetQuote", 
   		               "transfers.simple.multicurrency.navigator.MultiCurrencyTransferFCAtoOtherGetQuote", 
   		               "transfers.simple.multicurrency.navigator.MultiCurrencyTransferFCAtoOtherGetQuote"];		//maps to ZAR_TO_ZAR, ZAR_TO_FCA, etc, etc														//
   		
   		/**
		 * The hidden "nav" input on the form
		 */
		this.formActionElement;
		
		/**
		 * Is the otp required before we display the BOP details?
		 */
   		        // var bopOtpRequired : <%=isOtpRequired.booleanValue()%>;
	
		/**
		 * The validation function to call when the user submits the form
		 */
		this.validationFunction;
		
		/**
		 * The from account currency - we need to store this in a separate value as the front-devs advise that the only
		 * reliable way to get the selected/return value of a dropdown is from the parameter that the dropdown passes to
		 * its onchange event handler(the currency is in the dropdown's return value along with the anrfn), so we grab
		 * the value in the fromAccountChanged method and store it here. Not the best way of doing it, but...
		 */
		this.fromAccountCurrency;
		
		/**
		 * The to account currency - we need to store this in a separate value as the front-devs advise that the only
		 * reliable way to get the selected/return value of a dropdown is from the parameter that the dropdown passes to
		 * its onchange event handler(the currency is in the dropdown's return value along with the anrfn), so we grab
		 * the value in the toAccountChanged method and store it here. Not the best way of doing it, but...
		 */
		this.toAccountCurrency;
		
		
		/**
		 * Used for MCA to MCA to check if bop reporting needed, will treat as MCA to ZAR if MCA acc is onShore
		 */
		this.toAccOffShore;
		
		this.userBaseCurrencyCode;
		this.fromAccCurrencyCode;
		this.toAccCurrencyCode;
	}
	genericPageObject.prototype = {
		el : function(elementId){
			return document.getElementById(elementId);
		},
		init : function(userBaseCurrencyCode,fromAccCurrencyCode,toAccCurrencyCode) {
			this.userBaseCurrencyCode = userBaseCurrencyCode;
			this.fromAccCurrencyCode = fromAccCurrencyCode;
			this.toAccCurrencyCode = toAccCurrencyCode;
			this.currencyRadio1 = this.el("currencyRadio1");
			this.currencyRadio2 = this.el("currencyRadio2");
			this.currencyRadioHiddenInput = this.el("transferCurrencyCode");

			this.pageElements[this.ZAR_TO_ZAR] = [this.el("dateRow"), this.el("zarTransferNoticeBox")];
			this.pageElements[this.ZAR_TO_FCA] = [this.el("fcaAmountRow"), this.el("bopZarMca"), this.el("BOPReportingPaneZarMca"), this.el("BOPReportingPane"), this.el("BOPReportingPaneTwo"), this.el("fcaTransferNoticeBox")];
			this.pageElements[this.FCA_TO_ZAR] = [this.el("fcaAmountRow"), this.el("bopMcaZar"), this.el("chargeAccountRow"), this.el("BOPReportingPane"), this.el("BOPReportingPaneTwo"), this.el("fcaTransferNoticeBox")];
			this.pageElements[this.FCA_TO_FCA] = [this.el("fcaAmountRow"), this.el("chargeAccountRow"), this.el("fcaFcaTransferNoticeBox")];
			
			var formChildren = this.el("transfer_capture").childNodes;	
			
			for(var i = 0; i < formChildren.length; i++){																//previously we had a hidden input that I could just reference directly, but now I need to drill into the form
				if(formChildren.item(i).id == "nav"){
					this.formActionElement = formChildren.item(i); 														//was previously "action"
					break;
				}
			}
			
			if(! this.formActionElement){
				//console.log("form action element is null");
			}
			
			var thisObj = this;																							//because 'this' remains literal if called asyncronously in another thread of execution
					
			this.hideAllElements();	
			
			if(this.isEditing()){
				this.resetAccountCurrencies();
				this.getBopCodeAttr();
			} 
			
			var initType = (this.isEditing() ? this.getTransferTypeToEdit() : this.ZAR_TO_ZAR);						//if we're editing then work console.log which controls to display else default to ZAR to ZAR
			
			this.changeControls(this.pageElements[initType], this.validationFunctions[initType], this.formActions[initType]);
			
			this.fromRefInput = $('#fromAccountReference');
			this.toRefInput = $('#toAccountReference');
			this.bindEvents();
		},
		bindEvents : function() {
			var _this = this;
			$('#refParent').on('keydown keyup', '#fromAccountReference', function(event) {_this.copyValues();});
		},
		copyValues : function() {
			var triggerUpdate = false;
			var toRefVal = this.toRefInput.val();
			var fromRefSub = this.fromRefInput.val().substr(0,(this.fromRefInput.val().length-1))
			if(toRefVal==fromRefSub||toRefVal==this.fromRefInput.val()||this.fromRefInput.val()==this.toRefInput.val().substr(0,(this.toRefInput.val().length-1))){
				triggerUpdate = true;
			}
			if(toRefVal==''||triggerUpdate == true){
				this.toRefInput.val(this.fromRefInput.val())
			}
		},
		getBopCodeAttr :function(){
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:"/banking/Controller?nav=transfers.simple.multicurrency.navigator.MultiCurrencyTransferCaptureQuoteBopCodes&bopCodeSelectedZarMca=isEdit", target:"#bopCodeSelectionContainer"});
		},
		resetAccountCurrencies : function(){
			this.fromAccountCurrency = this.fromAccCurrencyCode;
			this.toAccountCurrency = this.toAccCurrencyCode;
		},
		isEditing : function(){
			return pages.fnb.simple.MultiCurrencyTransferCaptureQuotePage.isEditing();
		},
		getTransferTypeToEdit : function(){
			var fromAccIsFca = pages.fnb.simple.MultiCurrencyTransferCaptureQuotePage.getTransferTypeToEditfromAccIsFca();
			var toAccIsFca = pages.fnb.simple.MultiCurrencyTransferCaptureQuotePage.getTransferTypeToEdittoAccIsFca();
			var toAccOnShore = pages.fnb.simple.MultiCurrencyTransferCaptureQuotePage.getTransferTypeToEdittoAccOnShore();
			
			return this.determineTransferType(fromAccIsFca, toAccIsFca , toAccOnShore);
		},
		fromAccountChanged : function(selectedItem){
			var returnVal = $(selectedItem).attr('data-value');
			
			console.log("from account changed : "+returnVal);
			
			if(returnVal != -1){			
				this.fromAccountCurrency = returnVal.substring((returnVal.length-4), returnVal.length-1);
			}
			$("#currencyRadio1").attr("data-value", this.fromAccountCurrency);

			this.clearCurrencyRadioValues();				
			this.resetZARtoZARControls();
		},
		clearCurrencyRadioValues : function(){
			this.currencyRadio1.innerHTML = "-";																		//clear the radio values
			this.currencyRadio2.innerHTML = "-";
		},
		toAccountChanged : function(selectedItem){	
			//we need to refresh the object reference if the from account changes as the to account dropdown is redrawn - can do it here tho as the user will have to reselect the to account
			var returnVal = $(selectedItem).attr('data-value');
			
			console.log("to account changed : "+returnVal);
			
			if(returnVal == -1){
				this.resetZARtoZARControls();
			}else{
			
				this.toAccountCurrency = returnVal.substring((returnVal.length-4), returnVal.length-1);
				this.toAccOffShore = returnVal.substring((returnVal.length-1), returnVal.length);
				$("#currencyRadio2").attr("data-value", this.toAccountCurrency);
				
				var tranType = this.determineTransferType(this.isFromAccountFCA(), this.isToAccountFCA(), this.isToAccountOnShore());				//determine the new transfer type
				console.log("tranType : "+tranType);
				if(this.isEditing()){
					$('#transferCurrencyCode.radioGroupValue').attr('value', this.toAccountCurrency);
				}
				if(tranType == this.NOT_SET){
					showError("Unable to determine transfer type");
				}else{// if(transfer != this.transferType){																	//if the new type differs from the last, then do the work
				
					this.hideAllElements();																					//hide all the controls
					
					if(tranType == this.ZAR_TO_FCA){																		//if zar to fca
						//console.log("determined zar to fca");
						/*if(this.bopOtpRequired){																			//and an otp is required
						//console.log("showing otp overlay");
						OverlayManager.showOverlay("mutlicurrency_bop_otp");											//show the otp overlay which calls the showZARtoFCAControls func when a good (otp) response is received
						}else{*/
						//console.log("showing zar to fca controls");
						this.showZARtoFCAControls();																	//else show the controls
						/*}*/
					}else{
						this.changeControls(this.pageElements[tranType], this.validationFunctions[tranType],				//display the controls
						this.formActions[tranType]);
					}
					
					this.transferType = tranType;																			//set the current transfer type to the new type
				}
			}
		},
		showZARtoFCAControls : function(){
			//this.bopOtpRequired = false;																				//if we got this far then the user has provided the otp
			
			this.changeControls(this.pageElements[this.ZAR_TO_FCA], 													//display these elements,
								this.validationFunctions[this.ZAR_TO_FCA],												//use this validation function 
								this.formActions[this.ZAR_TO_FCA]);														//and this form action when the user submits
			
			/*if(! BOPDetailsAJAXParser.isFieldsPopulated()){
				this.getBOPDetails();
			}*/
		},
		resetZARtoZARControls : function(){
			this.changeControls(this.pageElements[this.ZAR_TO_ZAR], 													//display these elements,
								this.validationFunctions[this.ZAR_TO_ZAR],												//use this validation function 
								this.formActions[this.ZAR_TO_ZAR]);													//and this form action when the user submits
			
			//this.toAccDropDown.selectedIndex = 0; TODO THE CHAMELEON WAY!!!!
		},
		changeControls : function(elementsToDisplay, validationFunction, formActionValue){
			this.hideAllElements();
			
			for(var i=0; i<elementsToDisplay.length; i++){
				this.showElement(elementsToDisplay[i]);
			}			
			
			this.validationFunction = validationFunction;
			this.formActionElement.value = formActionValue;
			
			this.changeCurrencyRadios();
		},
		changeCurrencyRadios : function(){
			this.currencyRadio1.innerHTML = this.getFromAccountCurrency(); 
			this.currencyRadio2.innerHTML = this.getToAccountCurrency();
		},
		currencyRadio1Changed : function(){
			$("#transferCurrencyCode").find("input").attr("value", this.getFromAccountCurrency());						 //using jquery (sis!) to find the input because some bright spark on the front-end gave multiple elements the same id, so document.getElementById doesnt return the input element
			console.log("currency 1 changing to "+this.getFromAccountCurrency());
			
			 
			//console.log("el len: "+document.getElementsByName("transferCurrencyCode").length);
			/*var input = document.getElementsByName("transferCurrencyCode")[0];
			console.log("the input element is a: "+input + "id: "+input.id);
			input.value = this.getFromAccountCurrency();			
			console.log("the input value is now "+input.value);*/
		},
		currencyRadio2Changed : function(){
			$("#transferCurrencyCode").find("input").attr("value", this.getToAccountCurrency()); 				 		//using jquery (sis!) to find the input because some bright spark on the front-end gave multiple elements the same id, so document.getElementById doesnt return the input element
			console.log("currency 2 changing to "+this.getToAccountCurrency());
			
			/*var input = document.getElementsByName("transferCurrencyCode")[0];
			console.log("the input element is a: "+input + "id: "+input.id);
			input.value = this.getToAccountCurrency();
			console.log("the input value is now "+input.value);*/
		
		},
		hideAllElements : function(){
			this.hideElements(this.pageElements[this.ZAR_TO_ZAR]);
			this.hideElements(this.pageElements[this.ZAR_TO_FCA]);
			this.hideElements(this.pageElements[this.FCA_TO_ZAR]);
			this.hideElements(this.pageElements[this.FCA_TO_FCA]);
		},
		hideElements : function(elements){
			for(var i=0; i<elements.length; i++){
				this.hideElement(elements[i]);
			}
		},
		hideElement : function(e){
			e.style.display = "none";
		},
		showElement : function(e){
			e.style.display = "";																						//forces the default style...
		},
		isFromAccountFCA : function(){
			
			return (this.fromAccountCurrency.indexOf(this.userBaseCurrencyCode ) == -1);	
		},
		isToAccountFCA : function(){	
			return (this.toAccountCurrency.indexOf(this.userBaseCurrencyCode ) == -1);
		},
		isToAccountOnShore : function(){	
			return (this.toAccOffShore.indexOf("1") == -1);
		},
		getFromAccountCurrency : function(){
			return this.fromAccountCurrency;
		},
		getToAccountCurrency : function(){			
			return this.toAccountCurrency;
		},
		determineTransferType : function(isFromAccFCA, isToAccFCA, isToAccOnShore){
			if(! (isFromAccFCA || isToAccFCA))	return this.ZAR_TO_ZAR;
			if(isToAccFCA && ! isFromAccFCA)	return this.ZAR_TO_FCA;
			if(isFromAccFCA && ! isToAccFCA)	return this.FCA_TO_ZAR;
			if(isFromAccFCA && isToAccFCA)		return this.FCA_TO_FCA;
			if(isFromAccFCA && isToAccFCA && isToAccOnShore) return this.FCA_TO_ZAR;

			return this.NOT_SET;	
		},
		submitForm : function(){
			var fromAnrfn = this.el("fromAnrfn");
			
			if(fromAnrfn.value.indexOf(this.getFromAccountCurrency()) > -1){												//if the from anrfn has the currency code still appended, then we need to trim it off
				fromAnrfn.value = fromAnrfn.value.substring(0, (fromAnrfn.value.length-5));
			}
			
			var toAnrfn = document.getElementsByName("toAnrfn")[0];
			
			if(toAnrfn.value.indexOf(this.getToAccountCurrency()) > -1){													//if the to anrfn has the currency code still appended, then we need to trim it off
				toAnrfn.value = toAnrfn.value.substring(0, (toAnrfn.value.length-5));
			}
			
			console.log("submitting tccode: "+(document.getElementsByName("transferCurrencyCode")[0]).value);
	
			fnb.functions.submitFormToWorkspace.submit("transfer_capture");
		},
		bopCodeMcaZarDropdownChange : function(selection){
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:"/banking/Controller?nav=transfers.simple.multicurrency.navigator.MultiCurrencyTransferCaptureQuoteBopCodes&bopCodeSelectedMcaZar="+$(selection).attr('data-value'), target:"#bopCodeSelectionContainer"});
		},
		bopCodeZarMcaDropdownChange : function(selection){
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:"/banking/Controller?nav=transfers.simple.multicurrency.navigator.MultiCurrencyTransferCaptureQuoteBopCodes&bopCodeSelectedZarMca="+$(selection).attr('data-value'), target:"#bopCodeSelectionContainer"});
		}
	}
	namespace("fnb.transfers.simple.MultiCurrencyTransferCaptureQuote",genericPageObject);
});