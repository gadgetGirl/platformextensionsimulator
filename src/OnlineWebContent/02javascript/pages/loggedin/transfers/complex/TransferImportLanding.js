fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#resultSettingsBlock'}])

function changeFileName() {
	var path = document.getElementById("IMPORT_TRANSFER_DETAILS").theFile.value;

	if (path == ""){
		alert("Please enter a file Path");
		return false;
	} 
	else{
		document.getElementById("IMPORT_TRANSFER_DETAILS").fakefile.value = path;
		return true;
	}
}

function checkAllDetails() {
	var path = document.getElementById("IMPORT_TRANSFER_DETAILS").theFile.value;
	var emailValid=checkEmail();
	var result=true;
	var result1=true;
	var result2=true;
	var result3=true;
	var result4=true;
	var result5=true;
    var errors  ="";
    if(document.getElementById('transferName').value != ""){
		result5 = true;
		}else{
			errors = errors+"\nPlease enter transfer name";
			result5 = false;
		}

    if (path == ""){
		errors = errors+"\nPlease enter a file location";
		result2 = false;
	}

    if(document.getElementById('importType').value !='0'){
		result4 = true;
		}else{
			errors = errors+"\nPlease select a file type";
			result4 = false;
		}

	if (emailValid=="true"){
		result1 = true;
		}
		else {
			errors = errors+"\nPlease enter a valid "+emailValid+" email address";
			result1 = false;
		}

	if (document.getElementById('fileTypeReq').value !='0'){
		result3 = true;
		}
		else{
			errors = errors+"\nPlease select a file format";
			result3 = false;
		}

	if ((result1)&&(result2)&&(result3)&&(result4)&&(result5)){
		 result=true;
		}
		else{
			alert(errors);
			result=false;
		}
	return result;
}
	
function emailSelect(result){
	var x="${userEmail}";
	if (result==true){
        document.getElementById('email').style.display = 'inline';
        document.getElementById('resultSelect').value = 'Email';
        document.getElementById('primaryEmail').value = x;
        
	}
	else{
        document.getElementById('email').style.display = 'none';
        document.getElementById('resultSelect').value = 'Inbox';
        document.getElementById('primaryEmail').value = '';
        document.getElementById('CCaddress1').value = '';
        document.getElementById('CCaddress2').value = '';
	}
}

function checkEmail(){
	if(document.getElementById('resultSelect').value=='Email'){
		var primary=document.getElementById('primaryEmail').value;
		var cc1=document.getElementById('CCaddress1').value;
		var cc2=document.getElementById('CCaddress2').value;
		
		var result=validateEmail(primary.trim());
		var ccresult1=true;
		var ccresult2=true;

		if ((cc1.trim()!="") || (cc2.trim()!="")){
			 ccresult1=validateEmail(cc1);
			 ccresult2=validateEmail(cc2);
		
			 if (cc1.trim()==""){
				ccresult1=true;
		 	}
			 if (cc2.trim()==""){
				ccresult2=true;
			 }
		}

	 	if ((result)&&(ccresult1)&&(ccresult2))
		 	return "true";
	 	else{
			if (!result) 
				return "primary";
			if (!ccresult1) 
				return "CC1";
			if (!ccresult2) 
				return "CC2";
		}
	}
	return "true";
}

function setOutput(){
	if(document.getElementById('importType').value == '.CSV,*.csv'){
		document.getElementById('fileTypeReq').value ='CSV';
		document.getElementById('fileTypeReq').text ='CSV';
	}else if(document.getElementById('importType').value == 'BankServ (ACB),*.*'){
		document.getElementById('fileTypeReq').value ='ACB';
		document.getElementById('fileTypeReq').text ='ACB';
	}
}