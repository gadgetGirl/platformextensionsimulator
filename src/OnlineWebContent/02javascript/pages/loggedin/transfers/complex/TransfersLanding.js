(function($){
	
	try{
		// try catch is here since the hierarchy may not exist yet
		if(pages.fnb.transfers.complex.TransfersLanding){
			// if we've already defined this object, return
			return;
		}	
	}catch(e){
		
	}
			
	function genericPageObject(){
		var me = this;
		me.buttonToNavMap = {
				"Authorise":"transfers.complex.navigator.TransferRequestAuth"
				, "Delete":"transfers.complex.navigator.TransfersDeleteResults"
				, "Submit":"transfers.complex.navigator.TransferRequestAuth"
					}
	}
	
	
	genericPageObject.prototype = {
			
			init: function(config){
				
				var me = this;
				me.config = config;
				me.start();
				return me;
				
			}
			, destroy: function(){
				
				var me = this;
				
			}
			, onFormSubmit: function(sender,parms){
				pages.fnb.transfers.complex.TransfersLanding.changeFormSubmit(sender,parms)
				
			}
			, changeFormSubmit: function(sender,parms){
				
				try{
					if(parms["formName"]=="transfersLandingForm"){
						var me = this;
						$("form[name='transfersLandingForm']").find("#action").val(
								$(parms["buttonTarget"]).text()
								);
						$("form[name='transfersLandingForm']").find("#nav").val(
								me.buttonToNavMap[$(parms["buttonTarget"]).text()]
						);
					}	
				}catch(e){
					console.log("error: " + e)
				}
				
				
				
			}
			, start: function(){
				var me = this;
				console.log("starting TransfersLanding ssss");
				fnb.controls.controller.detachEvent("onSubmitForm", me.onFormSubmit);
				fnb.controls.controller.attachEvent("onSubmitForm", me.onFormSubmit);
				
			}
			, deleteTransfers : function(){
				
				 var xfrfns = new Array();

				    var items = $('.col6').find('.checkbox-graphic-wrapper');
				    $.each(items, function(index, item) {
				    	if($(this).hasClass('checked')){
				    		xfrfns.push($(this).find('input').attr('value'));
				    	}
					});
				    
				    if(xfrfns.length == 0){
					    var msg = "Please select one or more batches to delete";
					    parentObject.showError(msg);						    
					    //window.setTimeout('me.showError("' + msg + '");',50); 				
				    	return false;
				    }
				    
				    fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '/banking/Controller?nav=transfers.complex.navigator.TransferDeleteWarning&XFRFN='+xfrfns);

			}
			, showError: function(errorMessage){
				fnb.controls.controller.eventsObject.raiseEvent('navError',{height:'134px',message: 'Some errors have occurred...', errors:[{error: errorMessage}]});
			}
			
			
	}
	
	
	namespace("pages.fnb.transfers.complex.TransfersLanding",new genericPageObject());
	
	
	
	
})(jQuery);
