(function($){

	function genericPageObject(){
	}

	genericPageObject.prototype = {
			init: function(config){
				var me = this;
				me.config = config;
				//alert("BATCH SIZE: "+config.batchSize);
				me.start();
				return me;
			},
			destroy: function(){
				var me = this;
			},
			start: function(){
				var me = this;
				$("#footerButtonsContainer").find(".footerBtn > a").each(function(index, item){
					var labelToActionMap = {
						"Save & Exit": "save"
						,"Submit":"submit"
						,"Authorise":"auth"
					};
					var buttonLabel = $(item).text();
					if(buttonLabel=="Save & Exit" 
						||buttonLabel=="Submit" 
					    ||buttonLabel=="Authorise" 
						)
					$(item).click(function(){
						document.getElementById("buttonClicked").value = labelToActionMap[buttonLabel];
						$("#action").val(buttonLabel);
						fnb.functions.submitFormToWorkspace.submit("transfersAddWithinEntityLanding");
						
					});
				});
				
			},
			dropDowChanged: function(item){
				// item will be an li with data-value of the account rfn
				
				var parentObject = this;
				var selectedAccount=$(item).data("value");
				var rowDiv = $(item).closest('.tableDataRow');
				var rowIndex = 0;

				// rowIds are obtained by parsing the row id ex: a row Id may be: "tabelRow_1"
				// rowIds are not 0 indexed, hence we subtract 1 to get the index
				rowIndex = rowDiv.attr("id").split("_")[1] - 1;
				var targetDiv = rowDiv.find("div[name='toAccountRFN']");
				var dropdownURL="/banking/Controller?nav=transfers.complex.navigator.TransfersMaintenanceToAccountDropDownBuilder&index="+rowIndex+"&fromAccDropDownValue="+selectedAccount;
				fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',
					{
						url: dropdownURL,
						target: targetDiv,
						//postLoadingCallback: parentObject.changeDropId(rowIndex)
					});
				// _ajaxLoader.loadUrl(targetDiv,dropdownURL,'','',false);
			},
			/*changeDropId: function(index){
				console.log('Index:' + index);
				document.getElementById("toAcc"+index).id = "toAccountRFN"+index;			//need to change the dropdowns id and name to the correct value
			},*/
			submitTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TransfersAddAcrossEntityContinue";
				document.getElementById("buttonClicked").value="submit";
				fnb.functions.submitFormToWorkspace.submit("transfersAddAcrossEntityLanding");
			},
			authTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TransfersAddAcrossEntityContinue";
				document.getElementById("buttonClicked").value="auth";
				fnb.functions.submitFormToWorkspace.submit("transfersAddAcrossEntityLanding");
			},
			submitWithinTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TransfersAddWithinEntityContinue";
				document.getElementById("buttonClicked").value="submit";
				fnb.functions.submitFormToWorkspace.submit("transfersAddWithinEntityLanding");
			},
			authWithinTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TransfersAddWithinEntityContinue";
				document.getElementById("buttonClicked").value="auth";
				fnb.functions.submitFormToWorkspace.submit("transfersAddWithinEntityLanding");
			},
			saveAndExitTransfer: function(){
				document.getElementById("nav").value="transfers.complex.navigator.TransfersAddAcrossEntityContinue";
				document.getElementById("buttonClicked").value="save";
				fnb.functions.submitFormToWorkspace.submit("transfersAddAcrossEntityLanding");
			},
			addMore: function(){
				document.getElementById("addMore").value="true";
				document.getElementById("nav").value="transfers.complex.navigator.TransfersCopyLanding";
				fnb.functions.submitFormToWorkspace.submit("transfersAddWithinEntityLanding");
			},

			cloneRow: function(parent, cloneTarget){
				var count = this.config.batchSize;//parent.find('.tableRow').length;
		
				var cloneItem = cloneTarget.clone();
				cloneItem.attr('id','tabelRow_'+(parseInt(count)+1));
				cloneItem.find('#fromAccountRFN .dropdown-hidden-input').attr({'id': 'fromAccountRFN'+count,'name': 'fromAccountRFN'+count}).val('');
				var fromParentID = 'fromAccountRFN'+count+'_dropId';
				cloneItem.find('#fromAccountRFN .dropdown-initiator').attr('id',fromParentID)
				var fromDropID = 'fromAccountRFN'+count+'_parent';
				cloneItem.find('#fromAccountRFN .singleTierDropDown').attr('id',fromDropID)
				cloneItem.find('#fromAccountRFN .dropdown-selection-white').text('Please Select');

				cloneItem.find('#toAccountRFN .dropdown-hidden-input').attr({'id': 'toAccountRFN'+count,'name': 'toAccountRFN'+count}).val('');
				fromParentID = 'toAccountRFN'+count+'_dropId';
				cloneItem.find('#toAccountRFN .dropdown-initiator').attr('id',fromParentID)
				fromDropID = 'toAccountRFN'+count+'_parent';
				cloneItem.find('#toAccountRFN .singleTierDropDown').attr('id',fromDropID)
				cloneItem.find('#toAccountRFN .dropdown-selection-white').text('Please Select');

				cloneItem.find('.col3 input:eq(0)').attr({'id': 'fromAccountReference'+count,'name': 'fromAccountReference'+count}).val('');
				cloneItem.find('.col4 input:eq(0)').attr({'id': 'toAccountReference'+count,'name': 'toAccountReference'+count}).val('');
				cloneItem.find('.col5 input:eq(0)').attr({'id': 'amount'+count,'name': 'amount'+count}).val('0.00');
				cloneItem.find('.col6 input:eq(0)').attr({'id': 'itemRFN'+count,'name': 'itemRFN'+count,'data-value': 0}).val(0);
				cloneItem.find('.col7 input:eq(0)').attr({'id': 'index'+count,'name': 'index'+count,'data-value': count}).val(count);

				cloneItem.appendTo(parent);
				
				this.config.batchSize++; 							//increment the index so that the user adds yet another row, the index is valid
				
				/*setTimeout(function(){
					//fnb.forms.dropdown.init($('#fromAccountRFN'+count+'_dropId'),0);
					//fnb.forms.dropdown.init($('#toAccountRFN'+count+'_dropId'),0);	
				},200)*/
				
			}
			
	}

	namespace("fnb.transfers.complex.TransferMaintenanceLanding",genericPageObject);

	/*
	function getDropdownValue(me,index, divName){
		var val=$(me).attr('value');
		dropdownUtility.loadDropDownDiv('/banking/Controller?nav=transfers.complex.navigator.TransfersToAccountDropDownBuilder&index='+index+'&fromAccDropDownValue='+val,divName);
	}
	*/
})(jQuery);