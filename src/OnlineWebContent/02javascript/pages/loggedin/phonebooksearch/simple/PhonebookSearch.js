$(function(){
	function genericPageObject(){
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(me){
			if ((14 - me) < 0) {
				alert("Too many entries");
			} else {
				fnb.controls.controller.eventsObject.raiseEvent('eziSliderLoad', {url: '/banking/Controller?nav=navigator.phonebookSearch.simple.PhonebookAdd'});
			}

		},
		returnValue: function(selectedValue) {
			$("#phoneBookSearch").val(selectedValue);
			fnb.controls.controller.eventsObject.raiseEvent('eziSliderHide', '');
		}
	}

	namespace("fnb.phonebookSearch.simple.PhonebookSearch",genericPageObject);
});