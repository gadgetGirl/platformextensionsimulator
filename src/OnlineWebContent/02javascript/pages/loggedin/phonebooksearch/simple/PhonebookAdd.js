$(function(){
	function genericPageObject(){
		var numberEntry;
		var nameEntry;
		var removeEntry;
		var currentSize;
		this.configObject = {};
	}
	genericPageObject.prototype = {
		
		init: function(me){
			this.currentSize = me;
		
			$('#phoneBookContainer').on('click','.remove',function(event){
				
				$(event.target).parent().remove();

			})
		
		},
		
		//removeEntry: function(me){
		//	var entry = me;
		//	var rowDelete = $(entry).parent().attr('id').substring(6,7);
		//	var parentRow = $('#phoneBookEntries' + rowDelete);
			
		//},
			
		addPhoneEntry: function(){
			if(((15 - 1) - this.currentSize) > 0){

				var clonedItem = $('#phoneBookEntries0').clone(true, true);
				$(clonedItem).attr('id','phoneBookEntries' + this.currentSize);
				$('#phoneBookContainer').append(clonedItem);
				
				this.nameEntry = $(clonedItem).find("[id^='name_']");
				this.numberEntry = $(clonedItem).find("[id^='phonebookNumber_']");
				this.removeEntry = $(clonedItem).find("[id^='delete_']");
				
				this.nameEntry.attr('id','name_' + (this.currentSize));
				this.nameEntry.attr('name_','name_' + (this.currentSize));
				this.numberEntry.attr('id','phonebookNumber_' + (this.currentSize));
				this.numberEntry.attr('name_','phonebookNumber_' + (this.currentSize));
				this.removeEntry.attr('id','delete_' + (this.currentSize));
				this.nameEntry.attr('value','');
				this.numberEntry.attr('value','');
				
				this.currentSize ++;
			} else {
				$('#phoneErrorMessage').show();
				$('#addPhoneEntryCol').hide();
			}
		}
	}

	namespace("fnb.phonebookAdd.simple.PhonebookAdd",genericPageObject);
});