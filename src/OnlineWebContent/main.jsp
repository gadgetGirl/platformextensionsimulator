
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@page import="mammoth.utility.HyphenString"%>

<c:set var="session_contextPath" scope="session">/BiFrostServer</c:set>

<%@ taglib prefix="fnb.hyperion.banking.frame" tagdir="/WEB-INF/tags/banking/frame"%>

<%@ taglib prefix="fnb.hyperion.banking.mainMenu" tagdir="/WEB-INF/tags/banking/frame/mainMenu"%>

<c:set var="theUser" value="${sessionScope.SessionData['HyphenUser']}"></c:set>

<c:url value="/TopMenu.jsp" var="site_topmenu"/>
<c:url value="/Controller?config=true" var="site_login" scope="session"/>

<c:set value="${param['country']}" 	var="country" 	scope="session"/>
<c:set value="${param['countryCode']}" 	var="countryCode" 	scope="session"/>
<c:set value="${param['language']}" var="language" 	scope="session"/>
	
	<c:choose>
		<c:when test="${!empty param['url']}">
			<c:set value="${param['url']}"	var="skin"	scope="session"/>
		</c:when>
		<c:otherwise>
			<c:set value="${param['skin']}" 		var="skin" 		scope="session"/>
		</c:otherwise>
	</c:choose>

	<c:set var="skin" scope="session" >
	        <fnb.hyperion.banking.frame:skin/>
	</c:set>

<c:set var="systemMetaTags">

	<meta charset="utf-8">
	
	<meta http-equiv="expires" content="0"></meta>
	
	<base target="result"></base>

 	<% String responseHeader = response.getHeader("User-Agent"); %>
 
	<c:choose>
		<c:when test="${responseHeader == 'BlackBerry'}">
			<meta name="viewport" content="width=device-width,target-densitydpi=device-dpi,user-scalable=no,initial-scale=1.0">
		</c:when>
		<c:otherwise>
			<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		</c:otherwise>
	</c:choose>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="format-detection" content="telephone=no" />
	<meta name="HandheldFriendly" content="true" />

	<meta name="apple-touch-fullscreen" content="yes" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<link rel="apple-touch-icon" href="03images/base/mobi/fnbicon.jpg"/>
	<link rel="apple-touch-startup-image" href="03images/base/mobi/fnbicon.jpg">
	
</c:set>

<c:set var="systemJs">

	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/extentions/xml/xml.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/extentions/cookies/cookies.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/utils/timeOut/banking/timeOut.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/utils/actionMenu/actionMenu.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/ajax/banking/loadEzi.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/ajax/banking/loadPage.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/ajax/banking/loadPopup.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/ajax/banking/loadIntoPage.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/ajax/banking/submitFromPage.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/ajax/banking/submitFromEziToEzi.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/ajax/banking/asyncLoadContent.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/ajax/banking/post.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/validateXHR/validateXHR.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/functions/print/print.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/templates/banking/eventTemplates.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/forms/button/banking/button.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/utils/tracking/banking/tracking.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/utils/ajax/validResponse/validResponse.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/00Assets/generic/genericJs/utils/ajax/otp/otp.js" />

	<%-- Developer: Donovan 
		Old frame includes
	--%>
	
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/02javascript/libs/calendar-1.5.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/02javascript/libs/jquery-1.8.2.min.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/02javascript/libs/respond.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/02javascript/core/combinedJs.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/02javascript/libs/TweenMax.min.js" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="js" link="${session_contextPath}/02javascript/libs/jquery.sparkline.min.js" />

</c:set>

<c:set var="systemCss">

	<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/dashBoardLayout.css" />
	<!--CSS-->
	
	<%-- Developer: Donovan 
		Old frame includes
	--%>
	<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/01css_new/global/reset.css" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/01css_new/global/calendar.css" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/01css_new/global/topMenu.css" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/01css_new/global/core.css" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/01css_new/global/nonGlobal.css" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/01css_new/global/print.css" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/01css_new/global/mobile.css" />
	<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/01css_new/fonts/stylesheet.css" />
	
	<c:if test="${skin == 1 || skin == 2 || skin == 6 || skin == 11 || skin == 12 || skin == 13}">
		<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/04skins/0/skinReset.css" />
		<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/04skins/${skin == 13 ? 1 : skin}/css/global.css" />

		<c:if test="${skin == 13}">
			<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/04skins/13/css/raslogos.css" />
		</c:if>
		
		<fnb.hyperion.banking.frame:import coreAsset="true" type="css" link="${session_contextPath}/04skins/0/skinMobile.css" />
	</c:if>
	
</c:set>

<c:choose>
	<c:when test="${skin == 11}"><fnb.hyperion.banking.frame:import type="image/x-icon" link="03images/favicon/clicks/favicon.png"/></c:when>
	<c:when test="${skin == 2}"><fnb.hyperion.banking.frame:import type="image/x-icon" link="03images/favicon/discovery/favicons.ico"/></c:when>
	<c:when test="${skin == 12}"><fnb.hyperion.banking.frame:import type="image/x-icon" link="03images/favicon/india/favicon.ico"/></c:when>
	<c:when test="${skin == 6}"><fnb.hyperion.banking.frame:import type="image/x-icon" link="03images/favicon/kulula/favicon.ico"/></c:when>
	<c:when test="${skin == 1}"><fnb.hyperion.banking.frame:import type="image/x-icon" link="03images/favicon/rmb/favicon.ico"/></c:when>
</c:choose>

<jsp:useBean id="topMenuViewBean" class="mammoth.utility.HyphenSortedVector" scope="session" />
<jsp:useBean id="topMenuOptionsViewBean" class="mammoth.utility.HyphenSortedVector" scope="session" />

<% response.setHeader("X-UA-Compatible", "IE=EDGE"); %>

<fnb.hyperion.banking.frame:coreContainer 
	systemMetaTags="${systemMetaTags}"
	systemJs="${systemJs}"
	systemCss="${systemCss}"
	mainMenu="${mainMenu}"
	subMenu="${subMenu}"
	pageTitle="Online Banking">
	
</fnb.hyperion.banking.frame:coreContainer>
 
<div id="hiddenPrintWrapper" style="display:none" >
<div id="hiddenPrintDiv">

</div>
</div>


	
<script type="text/javascript">
	var sessionTimeoutHandle = -1;
	namespace("fnb.clientTimeout",120);
	var timeToWait = 600000;
	var topMenuUrl = '${site_topmenu}';
	var defaultUrl = '${site_login}';
	var loggedIn = ${theUser.loggedOn eq 'true'}
	
</script>