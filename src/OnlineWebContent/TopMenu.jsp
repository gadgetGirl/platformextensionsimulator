<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<jsp:useBean id="muaUniqueIDRenderApplet" class="java.lang.String" scope="session" />
<jsp:useBean id="topMenuViewBean" class="mammoth.utility.HyphenSortedVector" scope="session" />
<jsp:useBean id="topMenuOptionsViewBean" class="mammoth.utility.HyphenSortedVector" scope="session" />
<base:bnkTabs menuViewBean="${topMenuViewBean}" topMenuOptionsViewBean="${topMenuOptionsViewBean}"/>