<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="width"%>
<%@ attribute required="false" name="span"%>
<%@ attribute required="false" name="className"%>
<c:if test="${not empty span}"><c:set var="span" value="colspan='${span}'"/></c:if>
<c:if test="${not empty className}"><c:set var="className" value="class='${className}'"/></c:if>
<td data-role="gridCol" ${not empty className?className:''}data-width="${empty width?'100':width}" ${not empty span?span:''}><jsp:doBody /></td>