<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="width"%>
<%@ attribute required="false" name="span"%>
<c:if test="${not empty span}"><c:set var="span" value="colspan='${span}'"/></c:if>
<th data-role="gridColHeading" data-width="${empty width?'100':width}" ${not empty span?span:''}><jsp:doBody /></th>