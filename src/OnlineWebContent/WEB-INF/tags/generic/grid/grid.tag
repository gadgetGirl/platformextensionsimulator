<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="width"%>
<table data-role="grid" data-width="${empty width?'100':width}"><jsp:doBody /></table>