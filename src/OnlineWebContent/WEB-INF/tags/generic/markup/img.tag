<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="id" required="false" %>
<%@ attribute name="src" required="true" %>
<%@ attribute name="altSrc" required="false" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="onclick" required="false" %>
<%@ attribute name="alt" required="false" %>

<div<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty className}"> class="${className}"</c:if>>
	<img src="${session_contextPath}${src}"<c:if test="${not empty onclick}"> onclick="${onclick}"</c:if><c:if test="${not empty alt}"> alt="${alt}"</c:if> <c:if test="${not empty altSrc}"> altSrc="${altSrc}"</c:if>/>
</div>