<%@ attribute name="id" required="false" rtexprvalue="true" description="id of list" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>

<ol id="${id}" class="${className}" data-role="orderedList" ><jsp:doBody /></ol>