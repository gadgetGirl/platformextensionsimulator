<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="width"%>
<div class="tableWrapper">
	<table data-role="table" width="${empty width?'100%':width}"><jsp:doBody /></table>
</div>