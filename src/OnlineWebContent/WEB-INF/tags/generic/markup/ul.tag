<%@ attribute name="id" required="false" rtexprvalue="true" description="id of list" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>

<ul id="${id}" class="${className}" data-role="unorderedList" ><jsp:doBody /></ul>