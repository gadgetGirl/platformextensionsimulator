<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="id" required="false" rtexprvalue="true" description="id of listitem" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="dataAttributeLabel" required="false" rtexprvalue="true" %>
<%@ attribute name="dataAttributeValue" required="false" rtexprvalue="true" %>

<div<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty className}"> class="${className}"</c:if><c:if test="${not empty dataAttributeLabel && dataAttributeValue }"> data-${dataAttributeLabel}="${dataAttributeValue}"</c:if>><jsp:doBody /></div>