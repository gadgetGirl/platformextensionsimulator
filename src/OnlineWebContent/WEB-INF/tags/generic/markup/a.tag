<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/fn.tld" prefix="fn"%>
<%@ attribute name="id" required="false" rtexprvalue="true" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="href" required="true" rtexprvalue="true" description="page to link to" %>
<%@ attribute name="newTab" required="false" rtexprvalue="true" description="open page in new tab" %>
<%@ attribute name="ajax" required="false" rtexprvalue="true" description="turn ajax on or off" %>
<c:choose>
	<c:when test="${not empty href}">
		<c:choose>
			<c:when test="${fn:contains(href,'Controller') and (newTab eq 'true')}">
				<a id="${id}" class="${className}" href="${href}" target="_blank"><jsp:doBody /></a>
			</c:when>
			<c:when test="${fn:contains(href,'Controller')}">
				<a id="${id}" class="${className}" href="${href}" ><jsp:doBody /></a>
			</c:when>
			<c:when test="${fn:contains(href,'://') and (newTab eq 'false')}">
				<a id="${id}" class="${className}" href="${href}" ><jsp:doBody /></a>
			</c:when>
			<c:when test="${(fn:contains(href,'://')) or (fn:contains(href,'/downloads/'))}">
				<a id="${id}" class="${className}" href="${href}" target="_blank"><jsp:doBody /></a>
			</c:when>
			<c:when test="${fn:contains(href,'mailto:')}">
				<a id="${id}" class="${className}" href="${href}"><jsp:doBody /></a>
			</c:when>
			<c:when test="${(ajax eq 'true') and (newTab eq 'true')}">
				<a id="${id}" class="${className}" href="${href}" ><jsp:doBody /></a>
			</c:when>
			<c:otherwise>
				<a id="${id}" class="${className}" ${ajax ne 'false'?'data-role="hyperlink"':''} href="${href}"><jsp:doBody /></a>
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise><jsp:doBody /></c:otherwise>
</c:choose>