<%@ attribute name="id" required="false" rtexprvalue="true" description="id of listitem" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>

<li id="${id}" class="${className}"><jsp:doBody /></li>