<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="id" required="false" rtexprvalue="true" description="id of paragraph" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>

<p<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty className}"> class="${className}"</c:if>><jsp:doBody /></p>