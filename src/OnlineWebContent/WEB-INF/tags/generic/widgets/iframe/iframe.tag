<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="id" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="src" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="parameters" required="false" rtexprvalue="true" type="java.lang.String"%>

<div class="iFrame-widget ${className}">
	<iframe id="${id}" src="${src}${parameters}" height="100%" width="100%" ></iframe>
</div>