<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame" %>
<%@ attribute name="id" required="true" rtexprvalue="true"%>
<%@ attribute name="loadCSS" required="false" rtexprvalue="true"%>
<%@ attribute name="loadJS" required="false" rtexprvalue="true"%>
<c:set var="bannerID">${id}</c:set>

<div id="${bannerID}-banner" class="banner bannerObject">
	<c:if test="${not empty loadCSS && loadCSS == true}">
	<%-- loads custom css if true --%>
		<fnb.generic.frame:import type="css" link="${session_contextPath}/04Banners/${bannerID}/style.css" />
	</c:if>
	<a id="${bannerID}" data-role="bannerLink" href="${session_contextPath}${param.url}">	 
		<jsp:doBody/>
	</a>
	<c:if test="${not empty loadJS && loadJS == true}">
	<%-- loads custom javascript if true --%>
		<fnb.generic.frame:import type="js" link="${session_contextPath}/04Banners/${bannerID}/script.js"/>
	</c:if>
</div>
