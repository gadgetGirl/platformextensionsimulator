<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="group" required="true" rtexprvalue="true"%>
<%@ attribute name="interval" required="false" rtexprvalue="true"%>
<%@ attribute name="random" required="false" rtexprvalue="true"%>
<%@ attribute name="bannerList" required="false" rtexprvalue="true"%>
<c:set var="bannerDivId">
bnr<%=Math.round((Math.random() * 1000))%>
</c:set>
<c:choose>
	<c:when test="${not empty bannerList}">
		<c:set var='banners' value="${bannerList}" />
	</c:when>
	<c:otherwise>
		<c:set var='banners' value="empty" />
	</c:otherwise>
</c:choose>
<div class="bannerContainer" id="${bannerDivId}" data-contextPath="${session_contextPath}" data-settings='{"rotationInterval":"${interval}","random":"${random}","type":"${group}"}' data-banners='${banners}'>
<jsp:doBody/>
</div>