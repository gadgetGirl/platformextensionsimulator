<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute name="id" required="false" rtexprvalue="true" description="id of paragraph" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="accountName" required="false" rtexprvalue="true" %>
<%@ attribute name="onclick" required="false" rtexprvalue="true" %>
<%@ attribute name="rate" required="false" rtexprvalue="true" %>
<%@ attribute name="img" required="false" rtexprvalue="true" %>
<div class="selectedCard">
	<img src="${img}" width="50%" />
	<div>${accountName}</div> 
	<fnb.generic.forms:dataButton label="Choose" url="${onclick}" event="loadIntoEzi"/>
</div>

