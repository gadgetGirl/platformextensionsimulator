<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute name="id" required="false" rtexprvalue="true" description="id of paragraph" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="label" required="true" rtexprvalue="true" %>
<%@ attribute name="content" required="true" rtexprvalue="true" %>
<%@ attribute name="width" required="false" rtexprvalue="true" %>
<%@ attribute name="icon" required="true" rtexprvalue="true" description="Cellphone, Ebucks, Screen" %>


<div id="${id}" class="iconContainer ${icon} ${className} width${width}">
	${label}
	<div class="iconContent displayNone">${content}</div>	
</div>
