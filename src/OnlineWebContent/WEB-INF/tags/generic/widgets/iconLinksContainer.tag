<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="id" required="false" rtexprvalue="true" description="id of paragraph" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ taglib prefix="fnb.generic.widgets" tagdir="/WEB-INF/tags/generic/widgets"%>
<%@ attribute required="true"  rtexprvalue="true" name="services" type="java.util.Map" description="A list of all the forced services"%>
<div class="iconLinksWidget">

	<c:forEach var="service" items="${services}" varStatus="status">
		<c:set var="isLeft" value="${(status.index+1)%2}"/>
			<c:set var="idItemAttr">
			<c:choose>
				<c:when test="${service.key == 'eBucks'}">Ebucks</c:when>
				<c:when test="${service.key == 'Cellphone Banking'}">Cellphone</c:when>
				<c:when test="${service.key == 'eMail Statement'}">Screen</c:when>
				<c:when test="${service.key == 'Online Banking'}">Screen</c:when>
				<c:when test="${service.key == 'inContact'}">Cellphone</c:when>
				<c:when test="${service.key == 'FNB Connect'}">Cellphone</c:when>
				<c:otherwise></c:otherwise>
			</c:choose>
		</c:set>
		<c:if test='${isLeft == 0}'><c:set var="tooltipPos" value="rightToolTip"/></c:if>
		<fnb.generic.widgets:iconLinks label="${service.key}" content="${service.value}" icon="${idItemAttr}" width="50 ${tooltipPos}" />
	</c:forEach>
	
</div>
<%-- 
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="true"  rtexprvalue="true" name="services" type="java.util.Map" description="A list of all the forced services"%>
<c:if test="${not empty services}">
<h3 id="ssaYouAlsoGetTitle">You also get ... </h3>
<div id="ssaYouAlsoGetWrapper" class="clearfix">
	<c:set var="count" value="0" scope="page"/>
	<c:forEach var="service" items="${services}">
		<c:set var="count" value="${count + 1}" scope="page"/>
		<c:set var="idItemAttr">
			<c:choose>
				<c:when test="${service.key == 'eBucks'}">id="youAlsoGetEbucks"</c:when>
				<c:when test="${service.key == 'Cellphone Banking'}">id="youAlsoGetCellphoneBanking"</c:when>
				<c:when test="${service.key == 'eMail Statement'}">id="youAlsoGetEmailStatement"</c:when>
				<c:when test="${service.key == 'Online Banking'}">id="youAlsoGetOnlineBanking"</c:when>
				<c:when test="${service.key == 'inContact'}">id="youAlsoGetInContact"</c:when>
				<c:when test="${service.key == 'FNB Connect'}">id="youAlsoGetFnbConnect"</c:when>
				<c:otherwise></c:otherwise>
			</c:choose>
		</c:set>
		<div class="ssaYouAlsoGetCol${count} ssaYouAlsoGetItem td ${className}">
			<div ${idItemAttr} class="ssaYouAlsoGetItemInner" onclick="fnb.forms.tooltip.show(this);"><span class="ssaYouAlsoGetText">${service.key}</span></div>
			<div class="tooltip" onclick="fnb.forms.tooltip.close(this);">${service.value}<span class="tooltip-pointer">&nbsp;</span></div>
		</div>
	</c:forEach>
</div>
</c:if>
<jsp:doBody />
--%>
