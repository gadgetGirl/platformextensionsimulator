<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="value" required="false" %>

<div id="" class="slideHeader">
	<div class="slideHeaderRow">
		<div class="slideHeaderLeftImage"></div>
		<div class="slideHeaderInner"><p>${value}</p></div>
		<div class="slideHeaderRightImage"></div>
	</div>
</div>
