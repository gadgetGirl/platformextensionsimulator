<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="href" required="false" %>
<%@ attribute name="label" required="false" %>
<c:set var="body"><jsp:doBody /></c:set>
<c:set var="label">${empty label?body:label}</c:set>

<c:choose>
	<c:when test="${frameId ne 'bankingFrame'}">
		<a class="ui-branchLocator-link" href="/locators/branch-locator.html">${label}</a>
	</c:when>
	<c:otherwise>${label}</c:otherwise>
</c:choose>