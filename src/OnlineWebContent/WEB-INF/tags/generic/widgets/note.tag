<%@ attribute name="id" required="false" rtexprvalue="true" description="id of paragraph" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>


<p id="${id}" class="${className}"><jsp:doBody /></p>