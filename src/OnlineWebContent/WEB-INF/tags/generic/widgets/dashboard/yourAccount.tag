<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ taglib prefix="fnb.generic.layout" tagdir="/WEB-INF/tags/generic/layout"%>
<%@ taglib prefix="fnb.generic.widgets.dashboard" tagdir="/WEB-INF/tags/generic/widgets/dashboard"%>


<c:set var="sideBarContent">
${browserName}
</c:set>

		<fnb.generic.widgets.dashboard:dashboardHeader title="Your Account"/>
		
		<fnb.generic.forms:formElementWrapper className=" " id="one" label="Account Owner Name" labelValue="Lee-Anne van Zyl">
		</fnb.generic.forms:formElementWrapper>
		
		<!-- Input without note -->
		<fnb.generic.forms:input.text inlineError="Error text" id="input1" label="Account Owner Nickname" value="LvZ"/>
		
		<!-- Input without note -->
		<fnb.generic.forms:input.text inlineError="Error text" id="input1" label="Name on proof of payment" value="LvZ">
			<fnb.generic.forms:inlineLink href="#" label="Update" id="updateLink1"  />
		</fnb.generic.forms:input.text>
		
		<fnb.generic.forms:formElementWrapper className=" " id="one" label=" ">
			<div data-role="formElementLabel">
				
			</div>
		</fnb.generic.forms:formElementWrapper>
				
