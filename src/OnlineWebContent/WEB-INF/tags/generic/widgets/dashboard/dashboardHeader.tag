<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>
<%@ attribute required="true" name="title"%>

<fnb.generic.markup:h level="3" className="dashboardTitle" >${title}</fnb.generic.markup:h>