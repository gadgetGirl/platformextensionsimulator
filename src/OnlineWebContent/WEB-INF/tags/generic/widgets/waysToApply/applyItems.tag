<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.widgets.waysToApply" tagdir="/WEB-INF/tags/generic/widgets/waysToApply"%>
<%@ taglib prefix="fnb.generic.widgets.locators" tagdir="/WEB-INF/tags/generic/widgets/locators"%>

<%@ attribute name="apply" required="false" rtexprvalue="true"%>
<%@ attribute name="call" required="false" rtexprvalue="true"%>
<%@ attribute name="branches" required="false" rtexprvalue="true"%>
<%@ attribute name="atm" required="false" rtexprvalue="true"%>
<%@ attribute name="sms" required="false" rtexprvalue="true"%>

<c:set var="body">
	<c:if test="${(apply ne 'false') and (not empty productBoxUrl)}">
		<fnb.generic.widgets.waysToApply:applyItem><a href="${productBoxUrl}">Apply Online</a></fnb.generic.widgets.waysToApply:applyItem>
	</c:if>
	<c:if test="${(callMe ne 'false') and (not empty leadFormUrl)}">
		<fnb.generic.widgets.waysToApply:applyItem><a href="${leadFormUrl}">Call me back</a></fnb.generic.widgets.waysToApply:applyItem>
	</c:if>
	<c:if test="${call ne 'false'}">
		<%-- repeated in another section 
			<fnb.generic.widgets.waysToApply:applyItem>Call <b>${contactNumber}</b></fnb.generic.widgets.waysToApply:applyItem> --%>
	</c:if>
	<c:if test="${branches eq 'true'}">
		<fnb.generic.widgets.waysToApply:applyItem>
			Your nearest <fnb.generic.widgets.locators:branchLocator>FNB branch</fnb.generic.widgets.locators:branchLocator>
		</fnb.generic.widgets.waysToApply:applyItem>
	</c:if>	
	<c:if test="${atm eq 'true'}">
		<fnb.generic.widgets.waysToApply:applyItem>
			<fnb.generic.widgets.locators:atmLocator>FNB ATM</fnb.generic.widgets.locators:atmLocator>
		</fnb.generic.widgets.waysToApply:applyItem>
	</c:if>	
	<jsp:doBody />
</c:set>

<c:if test="${not empty body}">
	<ul data-role="applyItems">
		${body}		
	</ul>
</c:if>