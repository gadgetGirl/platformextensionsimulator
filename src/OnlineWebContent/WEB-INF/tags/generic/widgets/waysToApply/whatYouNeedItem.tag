<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="label" required="false" %>
<c:set var="body"><jsp:doBody /></c:set>
<c:set var="label">${empty label?body:label}</c:set>
<li data-role="whatYouNeedItem">${label}</li>