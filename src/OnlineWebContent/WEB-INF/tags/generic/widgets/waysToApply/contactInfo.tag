<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="additionalInfo" description="additional contact information"%>
<%@ attribute required="false" name="number" description="contact's number"%>
<%@ attribute required="false" name="numberLabel" description="label for the contact's number"%>
<%@ attribute required="false" name="fax" description="contact's fax number"%>
<%@ attribute required="false" name="faxLabel" description="label for the contact's fax number"%>
<%@ attribute required="false" name="email" description="contact's email address"%>
<%@ attribute required="false" name="emailLabel" description="label for the contact's email address"%>
<%@ attribute required="false" name="url" description="contact's web address"%>
<%@ attribute required="false" name="urlLabel" description="label for the contact's web address"%>
<%@ attribute required="false" name="thumbnailImage" description="thumbnail for block"%>

<c:if test="${empty additionalInfo}"><c:set var="additionalInfo"><jsp:doBody /></c:set></c:if>
<c:set var="contactInfo">
	<c:if test="${not empty number} and ${not empty fax} and ${not empty email} and ${not empty url}">
		<p>
			<c:if test="${not empty number}"><b>${numberLabel}</b><br />${number}<br /></c:if>
			<c:if test="${not empty fax}"><b>${faxLabel}</b><br />${fax}<br /></c:if>
			<c:if test="${not empty email}"><a mailto="${email}">${not empty emailLabel?emailLabel:email}</a><br /></c:if>
			<c:if test="${not empty url}"><a href="${url}">${not empty urlLabel?urlLabel:url}</a><br /></c:if>
		</p>
	</c:if>
	${additionalInfo}
</c:set>

<c:if test="${not empty contactInfo}">
	<div data-role="layoutOneContactInfo">
		<div data-role="layoutOneContactInfoThumbnail">
			<div data-role="layoutOneContactInfoThumbnailWrapper">
				<img src="${thumbnailImage}" />
			</div>
		</div>
		<div data-role="layoutOneContactInfoTitle">${label}</div>
		<div data-role="layoutOneContactInfoContent">
			${contactInfo}
		</div>
	</div>
</c:if>