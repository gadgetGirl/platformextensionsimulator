<%@ taglib prefix="fnb.generic.widgets.waysToApply" tagdir="/WEB-INF/tags/generic/widgets/waysToApply"%>

<%@ attribute required="false" name="additionalApplyInfo" description="what you can do to apply"%>
<%@ attribute required="false" name="additionalWhatYouNeedInfo"%>
<%@ attribute required="false" name="additionalContactInfo" description="contact information related to applying"%>
<div class="waysToApply">
<img class="info" src="${session_contextPath}/00Assets/skins/00/images/waysToApply/bubble-info.png" />
<fnb.generic.widgets.waysToApply:apply
	label="Apply"
	additionalInfo="${additionalApplyInfo}"
	thumbnailImage="${session_contextPath}/00Assets/skins/00/images/waysToApply/APPLY.jpg" />
<fnb.generic.widgets.waysToApply:whatYouNeed
	label="Need"
	additionalInfo="${additionalWhatYouNeedInfo}"
	thumbnailImage="${session_contextPath}/00Assets/skins/00/images/waysToApply/NEED.jpg" />
<fnb.generic.widgets.waysToApply:contactInfo
	label="Contact"
	additionalInfo="${additionalContactInfo}"
	thumbnailImage="${session_contextPath}/00Assets/skins/00/images/waysToApply/CONTACT.jpg" />

</div>