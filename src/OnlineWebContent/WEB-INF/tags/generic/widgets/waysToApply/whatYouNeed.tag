<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="additionalInfo"%>
<%@ attribute required="false" name="thumbnailImage" description="thumbnail for block"%>

<c:if test="${empty additionalInfo}"><c:set var="additionalInfo"><jsp:doBody /></c:set></c:if>
<c:if test="${not empty additionalInfo}">
	<div data-role="layoutOneWhatYouNeed">
		<div data-role="layoutOneWhatYouNeedThumbnail">
			<div data-role="layoutOneWhatYouNeedThumbnailWrapper">
				<img src="${thumbnailImage}" />
			</div>
		</div>
		<div data-role="layoutOneWhatYouNeedTitle">${label}</div>
		<div data-role="layoutOneWhatYouNeedContent">
			${additionalInfo}
		</div>
	</div>
</c:if>