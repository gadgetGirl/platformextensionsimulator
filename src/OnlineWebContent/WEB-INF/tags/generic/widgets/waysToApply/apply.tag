<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="additionalInfo" description="additional infomation about the apply process"%>
<%@ attribute required="false" name="productBoxUrl" description="url to the product box"%>
<%@ attribute required="false" name="productBoxLabel" description="label of the url to the product box"%>
<%@ attribute required="false" name="leadFormUrl" description="url to the lead form"%>
<%@ attribute required="false" name="leadFormUrlLabel" description="label to the url to the lead form"%>
<%@ attribute required="false" name="thumbnailImage" description="thumbnail for block"%>

<c:if test="${empty additionalInfo}"><c:set var="additionalInfo"><jsp:doBody /></c:set></c:if>
<c:set var="body">
	<c:choose>
		<c:when test="${empty additionalInfo}">
			<c:if test="${not empty productBoxUrl} and ${not empty leadFormUrl}">
				<p>
					<c:if test="${not empty productBoxUrl}"><a href="${productBoxUrl}" data-role="salesLink">${productBoxLabel}</a><br /></c:if>
					<c:if test="${not empty leadFormUrl}"><a href="${leadFormUrl}" data-role="leadFormLink">${leadFormUrlLabel}</a></c:if>
				</p>
			</c:if>
		</c:when>
		<c:otherwise>
			${additionalInfo}
		</c:otherwise>
	</c:choose>
</c:set>

<c:if test="${not empty body}">
	<div data-role="layoutOneApply">
		<div data-role="layoutOneApplyThumbnail">
			<div data-role="layoutOneApplyThumbnailWrapper">
				<img src="${thumbnailImage}" />
			</div>
		</div>
		<div data-role="layoutOneApplyTitle">${label}</div>
		<div data-role="layoutOneApplyContent">
			${body}
		</div>
	</div>
</c:if>