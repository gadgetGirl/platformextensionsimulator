<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="label" required="false" %>
<%@ attribute name="number" required="false" %>
<li data-role="contactItem"><b>${label}</b><br />${number}</li>