<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="href" required="false" %>
<%@ attribute name="label" required="false" %>
<c:set var="body"><jsp:doBody /></c:set>
<c:set var="label">${empty label?body:label}</c:set>
<a href="${href}" data-role="pdfDownload" target="_blank">${label}</a>
