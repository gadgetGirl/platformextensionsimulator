<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute name="id" required="false" rtexprvalue="true" description="id of paragraph" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>

<%@ attribute name="step" required="true" rtexprvalue="true" %>
<%@ attribute name="totalSteps" required="true" rtexprvalue="true" %>

<style>
	.stepsContainer {display: block; float: left; width: 100%; clear: both;}
	.stepsInner {float: right; width: auto; clear: both;}
	.stepBox {float: left;}
	
	.stepsContainer .stepBox {background: #D8D8D8; color: #fff; padding: 2px 5px; margin: 0 5px 5px 5px;}
	.stepsContainer .stepBox.highlighted {background: #009999; color: #fff;}
	
</style>

<div class="stepsContainer">
	<div class="stepsInner">
		<c:forEach var="i" begin="1" end="${totalSteps}">
		   <div id="${id}${i}" class="${className} stepBox <c:if test='${i == step}'>highlighted</c:if>">
				${i}
			</div>
		</c:forEach>
	</div>
</div>
