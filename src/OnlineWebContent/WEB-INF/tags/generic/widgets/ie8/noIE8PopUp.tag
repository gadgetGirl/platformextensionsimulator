<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="false" name="skin"%>
<%@ attribute required="false" name="path"%>

<c:set  var="path" value="${session_contextPath}"></c:set>

<c:set  var="closeBtn" value="url('${path}/00Assets/skins/00/images/ie8Exit/close.png') no-repeat"></c:set>

<c:if test="${skin == '0' || skin == '1'}">
<c:choose>
	<c:when test="${session_contextPath == '/banking'}">
		<c:set  var="findOutMoreLink" ></c:set>
		<c:set  var="gaChrome" ></c:set>
		<c:set  var="gaIE" ></c:set>
	</c:when>
	<c:otherwise>
		<c:set  var="findOutMoreLink"><a class="ie8FindOutMoreLink reactNormal" href="${path}/promotions/PullingThePlugIE8/index.html" onclick="_gaq.push(['_trackEvent', 'IE8 Exit Stage 1', 'IE8 Exit Overlay', 'Find out more']);">Find out more</a></c:set>
		<c:set  var="gaChrome" >onclick="_gaq.push(['_trackEvent', 'IE8 Exit Stage 1', 'IE8 Exit Overlay', 'Download Google Chrome']);"</c:set>
		<c:set  var="gaIE" >onclick="_gaq.push(['_trackEvent', 'IE8 Exit Stage 1', 'IE8 Exit Overlay', 'Internet Explorer 9 and above']);"</c:set>
	</c:otherwise>
</c:choose>

</c:if>

<c:choose>
	<c:when test="${session_contextPath == '/banking'}">
		<c:set  var="downloadButton"><a class="ie8Btn ie8DownloadBtn formButton" href="#" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: '/banking/Controller?nav=softwaredownloads.SoftwareDownloads'}); return false;">Download newer browser</a></c:set>
		<c:set  var="overlayZIndex" >z-index:49;</c:set>
	</c:when>
	<c:otherwise>
		<c:set  var="downloadButton"><a class="ie8Btn ie8DownloadBtn formButton" href="${session_contextPath}/tools/softwareDownloads.jsp" >Download newer browser</a></c:set>
		<c:set  var="overlayZIndex" >z-index:61;</c:set>
	</c:otherwise>
</c:choose>

<c:set var="alternativeStyle" value=""/>



<c:choose>
	<c:when test="${skin == 6}">
		<c:set  var="primaryColour" value="#89c13c"></c:set>
		<c:set  var="secondaryColour" value="#89c13c"></c:set>
		<c:set  var="background" value="#e7e7e7"></c:set>
		<c:set  var="primaryFontColour" value="#89c13c"></c:set>
		<c:set  var="secondaryFontColour" value="#333333"></c:set>
		<c:set  var="button" value="padding: 9px 15px 9px 20px"></c:set>
		<c:set  var="buttonColour" value="color:#ffffff"></c:set>
		<c:set  var="plugImage" value="url('${path}/00Assets/skins/06/images/ie8Exit/plug.png') no-repeat"></c:set>
	</c:when>
	<c:when test="${skin == 1}">
		<c:set  var="primaryColour" value="#b1ae9d"></c:set>
		<c:set  var="secondaryColour" value="#277093  url('${path}/00Assets/skins/01/images/ie8Exit/whiteArrow.png') no-repeat"></c:set>
		<c:set  var="background" value="#b1ae9d"></c:set>
		<c:set  var="primaryFontColour" value="#277093"></c:set>
		<c:set  var="secondaryFontColour" value="#000000"></c:set>
		<c:set  var="button" value="padding: 9px 15px 9px 30px"></c:set>
		<c:set  var="buttonColour" value="color:#ffffff"></c:set>
		<c:set  var="plugImage" value="none"></c:set>
		<c:set var="alternativeStyle">
		.IE8 #ie8Stage1 .ie8Btn {background:#277093  url('${path}/00Assets/skins/01/images/ie8Exit/whiteArrow.png') no-repeat 5px center;color:#ffffff;}
		.IE8 #ie8Stage1 h1{background:none;font-family:Helvetica, FoundationRoman, Sans-serif;}
		.IE8 #ie8Stage1 p{margin-left:0;}
		.IE8 #ie8Stage1 {z-index:61;}
		.IE8 #ie8Stage1 .ie8ContentWrapper{padding-left:0;width:60%;}
		.IE8 #ie8Stage1 .ie8p2{margin-bottom:0;}
		</c:set>
	</c:when>	
	<c:when test="${skin == 12}">
		<c:set  var="primaryColour" value="none"></c:set>
		<c:set  var="secondaryColour" value="#FFD200 url('${path}/00Assets/skins/12/images/ie8Exit/blueArrow.png') no-repeat"></c:set>
		<c:set  var="background" value="url('${path}/00Assets/skins/12/images/ie8Exit/indiaTextures.jpg') repeat"></c:set>
		<c:set  var="primaryFontColour" value="#094F97"></c:set>
		<c:set  var="secondaryFontColour" value="#000000"></c:set>
		<c:set  var="button" value="padding: 9px 15px 9px 30px"></c:set>
		<c:set  var="buttonColour" value="color:#094F97"></c:set>
		<c:set  var="plugImage" value="url('${path}/00Assets/skins/12/images/ie8Exit/plug.png') no-repeat"></c:set>
	</c:when>
	<c:when test="${skin == 11}">
		<c:set  var="primaryColour" value="#ce0683"></c:set>
		<c:set  var="secondaryColour" value="#435e93"></c:set>
		<c:set  var="background" value="#e7e7e7"></c:set>
		<c:set  var="primaryFontColour" value="#435e93"></c:set>
		<c:set  var="secondaryFontColour" value="#000000"></c:set>
		<c:set  var="button" value="padding: 9px 15px 9px 20px"></c:set>
		<c:set  var="buttonColour" value="color:#ffffff"></c:set>
		<c:set  var="plugImage" value="url('${path}/00Assets/skins/11/images/ie8Exit/plug.png') no-repeat"></c:set>
	</c:when>
	<c:when test="${skin == 2}">
		<c:set  var="primaryColour" value="#5c447e"></c:set>
		<c:set  var="secondaryColour" value="#e7e7e7"></c:set>
		<c:set  var="background" value="url('${path}/00Assets/skins/02/images/ie8Exit/dicscoveryTextures.jpg') repeat"></c:set>
		<c:set  var="primaryFontColour" value="#5c447e"></c:set>
		<c:set  var="secondaryFontColour" value="#333333"></c:set>
		<c:set  var="button" value="padding: 9px 15px 9px 20px"></c:set>
		<c:set  var="buttonColour" value="height:32px;border:1px solid #cccccc;color:#333333;background: url('${path}/00Assets/skins/02/images/ie8Exit/gradient.png') repeat-x"></c:set>
		<c:set  var="plugImage" value="url('${path}/00Assets/skins/02/images/ie8Exit/plug.png') no-repeat"></c:set>
	</c:when>
	<c:otherwise>
		<c:set  var="primaryColour" value="none"></c:set>
		<c:set  var="secondaryColour" value="#ff9900 url('${path}/00Assets/skins/00/images/ie8Exit/whiteArrow.png') no-repeat"></c:set>
		<c:set  var="background" value="url('${path}/00Assets/skins/00/images/ie8Exit/fnbTexture.jpg') repeat"></c:set>
		<c:set  var="primaryFontColour" value="#ff9900"></c:set>
		<c:set  var="secondaryFontColour" value="#023433"></c:set>
		<c:set  var="button" value="padding: 9px 15px 9px 30px"></c:set>
		<c:set  var="buttonColour" value="color:#ffffff"></c:set>
		<c:set  var="plugImage" value="url('${path}/00Assets/skins/00/images/ie8Exit/plug.png') no-repeat"></c:set>
	</c:otherwise>
</c:choose>

 <!--[if lt IE 9]>	
<style>
#ie8Stage1{
    background: url("${path}/00Assets/skins/00/images/ie8Exit/popupoverlay.png") repeat;
    height: 100%;
    left: 0px;
    position: absolute;
    top: 0px;
    width:100%;
    ${overlayZIndex}
    bottom:0px;
    min-width:500px;
}

#ie8Stage1 .ie8popupOverlay{
    display: block;
    height: 100%;
    left: 0px;
    position: absolute;
    top: 0;
    width: 100%;
}

#ie8Stage1 .ie8popupBar{
    background: #ffffff;
    border-right: none;
    left: 0px;
    width:100%;
    min-height: 250px;
    position: relative;
    right: 0;
    top: 150px;
    z-index: 8010;
    -moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;	
	border-top:none;
}


#ie8Stage1 .ie8ContentWrapper{
	background:none;
	background-position: 0px bottom; 
	padding-top:35px;
	padding-bottom:3%;
    width: 50%;
	float:none;
	margin:auto;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;	
	overflow:hidden;
	min-width:500px;
}

#ie8Stage1 #messages{
	width:200%;
	position:relative;
}

#ie8Stage1 #message1{
	width:50%;
	float:left;
	position:relative;
}


#ie8Stage1 #message2{
	width:50%;
	float:left;
	position:relative;
}

#ie8Stage1 .ie8header1{
	color:${primaryFontColour};
	font-family:"FNBSansBold";
	border-bottom:5px solid #000000;
	font-size:36px!important;
	padding-left:0px;
	padding-bottom:8px;
}

#ie8Stage1 .ie8header2{
	color:${secondaryFontColour};
	font-family:"FNBSansRegular";
	margin-bottom:3%;
	font-size:48px;
	padding-top:10px;
	padding-left:0px;
}

#ie8Stage1 .ie8p1{
	color:#000000;
	font-size:18px;
	margin-bottom:15px;
	padding:0;
	line-height: 20px;
	font-family:"FNBSansLight";
}

#ie8Stage1 .ie8p1b{
	color:#000000;
	font-size:18px;
	margin-bottom:15px;
	line-height:20px;
	font-family:"FNBSansLight";
	padding:0;
}

#ie8Stage1 .ie8p1b b{
	font-family:"FNBSansRegular";
}

#ie8Stage1 .ie8p1b a{
	color:#000000;
	margin:0px;
	font-size:18px;
	text-decoration:underline;
}

#ie8Stage1 .ie8p2{
	color:${secondaryFontColour};
	font-size:18px;
	padding:0;
}

#ie8Stage1 .ie8p3{
	color:#000000;
	font-size:18px;
	font-family:"FNBSansLight";
	display:block;
	float:left;
	line-height:30px;
	margin-right:30px;
	padding:0;
}

#ie8Stage1 .ie8p3 b{
	font-family:"FNBSansBold";
}

#ie8Stage1 .bottomLine{
	background:none!important;
	width:100%;
	border-bottom:2px solid #000000;
	padding-top:30px;
	margin-bottom:45px;
}


#ie8Stage1 .ie8p4{
	color:#000000;
	font-size:12px;
	font-family:"FNBSansRegular";
	font-weight:bold;
	margin-bottom:10px;
}

#ie8Stage1 .ie8p4 a{
	text-decoration:underline;
	color:#000000;
	font-size:12px;
	font-family:"FNBSansRegular";
	font-weight:bold;
}


#ie8Stage1 .ie8p5{
	color:#999999;
	font-size:12px;
	font-family:"FNBSansLight";
	font-weight:bold;
}



#ie8Stage1  #downloadChromeBtn{
	float:left;
	display:block;
	margin:0;
}

#ie8Stage1 .ie8BtnWrapper{
	width:100%;
	margin:auto;
	text-align:center;
	float:none;
	margin-top:3%;
}

#ie8Stage1 .ie8Btn{
	border:none;
	${button};
	${buttonColour};
	text-decoration:none;
	width:210px;
	text-align:left;
}

#ie8Stage1 .ie8DownloadBtn{
	background: ${secondaryColour};
	background-position: 5px center;
	${buttonColour};
	float:left;
	margin-top:15px;
	font-family:"FNBSansRegular";
}

#ie8Stage1 .ie8FindOutMoreLink{
	text-decoration:underline;
	display:block;
	margin-top:15px;
	color:#000000;
	text-align:left;
	color:#000000!important;
	padding-left:0px;
	display:block;
	font-size:14px;
	font-family:"FNBSansRegular";
}

#ie8Stage1 .ie8FindOutMoreBtn{
	background: #ffffff url("${path}/00Assets/skins/00/images/ie8Exit/Arrow.png") no-repeat;
	background-position: 5px center;
	float:right;
	color:#000000;
}

.clearBoth{
	clear:both;
}


.ie8DisplayNone{
	display:none;
}


#ie8Stage1 #ie8PopupCloseBtn{
    background: ${closeBtn};
    cursor: pointer;
    height: 23px;
    position: absolute;
    right: 7%;
    top: 18px;
    width: 23px;
}

#ie8Stage1 .countdownText {
	font-size:14px;
    position: absolute;
    right: 7%;
    top: 50px;
    z-index:999999;
    font-family:"FNBSansRegular";
}

${alternativeStyle}


@media only screen and (max-width: 1100px)
{
#ie8Stage1 .ie8ContentWrapper{
	padding: 2% 40px 90px 210px;
}



}
</style>

<div id="ie8Stage1" class="ie8DisplayNone ">
	<div class="ie8popupOverlay " id="ie8Overlay">
		<div class="ie8popupBar">
			<div class="ie8ContentWrapper">
				<h1 class="ie8header1">
					<span id="dateCountdown"></span> days left
				</h1>
				<div id="messages">
					<div id="message1">
						<h1 class="ie8header2">Your access will be blocked...</h1>
						<p class="ie8p1">From 14 September 2014 you will no longer be able to do your banking online if you are still using <b>Internet Explorer 8</b>.</p>
						<p class="ie8p1b">If you are already using <b>Internet Explorer 9 or above</b> and still see this message, <a class=" reactNormal" target="_blank" href="/downloads/support/${skin}/browser_compatibility_settings.pdf" onclick="_gaq.push(['_trackEvent', 'IE8 Exit Stage 1', 'IE8 Exit Overlay', 'Browser Compatibility Document']);"> please check your compatibility mode.</a> </p>
						<p class="ie8p2">Upgrading to a newer browser is FREE and simple.</p>
						${findOutMoreLink} 
						<a class="ie8Btn ie8DownloadBtn reactNormal" href="#" onclick=";fnb.ie8Exit.slideMessage();return false;">Free browser download</a>
					</div>
					<div id="message2">
						<h1 class="ie8header2">Downloads</h1>
						<p class="ie8p3">
							Doing your banking online works best on <b>Google Chrome</b>
						</p>
						<a class="ie8Btn ie8DownloadBtn  reactNormal" id="downloadChromeBtn" href="https://www.google.com/chrome/browser/" target="_blank" ${gaChrome}>Download Google Chrome</a>
						<div class="clearBoth bottomLine"></div>
						<p class="ie8p4">
							We also support <a href="http://www.microsoft.com/en-za/download/internet-explorer-9-details.aspx" class="reactNormal" target="_blank" ${gaIE}>Internet Explorer 9 and above</a>
						</p>
						<p class="ie8p5">Please note that this link automatically redirects to Internet Explorer 11.</p>
						<p class="ie8p5">Once on the page scroll down to select Internet Explorer 9.</p>
					</div>
				</div>
			</div>
			<div class="clearBoth"></div>
			<div id="ie8PopupCloseBtn" class="ie8PopupCloseBtn" onclick="fnb.ie8Exit.closeStage1Overlay();return false;"></div>
			<p class="countdownText" id="countdownText">
				You can close this in <span id="ie8Countdown">10</span> seconds...
			</p>
		</div>
	</div>
</div>
<![endif]-->	

