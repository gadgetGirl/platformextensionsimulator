<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="false" name="skin"%>
<%@ attribute required="false" name="path"%>

		<c:choose>
			<c:when test="${skin == 6}">
				<c:set  var="borderColour" value="#89c13c"></c:set>
				<c:set  var="secondaryColour" value="#747474"></c:set>
				<c:set  var="skin" value="06"></c:set>
				<c:set  var="background" value="#e7e7e7"></c:set>
				<c:set  var="buttonColour" value="#89c13c;color:#ffffff!important"></c:set>				
				<c:set  var="fontColour" value="color:#333333!important;"></c:set>
			</c:when>
			<c:when test="${skin == 12}">
				<c:set  var="borderColour" value="none"></c:set>
				<c:set  var="primaryColour" value="#FFD200"></c:set>
				<c:set  var="secondaryColour" value="#094F97"></c:set>
				<c:set  var="buttonColour" value="#FFD200;color:#094F97!important"></c:set>	
				<c:set  var="background" value="url('${path}/00Assets/skins/12/images/ie8Exit/indiaTextures.jpg') repeat"></c:set>
				<c:set  var="fontColour" value="color:#ffffff!important;"></c:set>
				<c:set  var="skin" value="12"></c:set>
			</c:when>
			<c:when test="${skin == 11}">
			<c:set  var="borderColour" value="#ce0683"></c:set>
				<c:set  var="primaryColour" value="#435e93"></c:set>
				<c:set  var="secondaryColour" value="#ce0683"></c:set>
				<c:set  var="background" value="#f1f1f1"></c:set>
				<c:set  var="buttonColour" value="#435e93;color:#ffffff!important"></c:set>	
				<c:set  var="fontColour" value="color:#435e93!important;"></c:set>
				<c:set  var="skin" value="11"></c:set>
			</c:when>
			<c:when test="${skin == 1}">
				<c:set  var="primaryColour" value="#0f66ad"></c:set>
				<c:set  var="secondaryColour" value="#747474"></c:set>
				<c:set  var="skin" value="01"></c:set>
			</c:when>			
			<c:when test="${skin == 2}">
				<c:set  var="borderColour" value="#5c447e"></c:set>
				<c:set  var="secondaryColour" value="#5c447e"></c:set>
				<c:set  var="buttonColour" value="url('${path}/00Assets/skins/02/images/ie8Exit/gradient.png') repeat-x;color:#000000!important"></c:set>
				<c:set  var="background" value="url('${path}/00Assets/skins/02/images/ie8Exit/dicscoveryTextures.jpg') repeat"></c:set>
				<c:set  var="fontColour" value="color:#333333!important;"></c:set>
				<c:set  var="skin" value="02"></c:set>
			</c:when>
			<c:when test="${skin == 13}">
				<c:set  var="borderColour" value="#01509d"></c:set>
				<c:set  var="secondaryColour" value="#5c447e"></c:set>
				<c:set  var="buttonColour" value="#01509d;color:#ffffff!important"></c:set>
				<c:set  var="background" value="#f1f1f1"></c:set>
				<c:set  var="fontColour" value="color:#333333!important;"></c:set>
				<c:set  var="skin" value="13"></c:set>
			</c:when>
			<c:otherwise>
				<c:set  var="borderColour" value="none"></c:set>
				<c:set  var="secondaryColour" value="none"></c:set>
				<c:set  var="background" value="url('${path}/00Assets/skins/00/images/ie8Exit/fnbTexture.jpg') repeat"></c:set>
				<c:set  var="buttonColour" value="#ff9900"></c:set>				
				<c:set  var="skin" value="00"></c:set>
			</c:otherwise>
		</c:choose>

<style>
#ie8Stage3{
    background: url("${path}/00Assets/skins/${skin}/images/ie8Exit/popupoverlay.png") repeat;
    height: 100%;
    left: 0px;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 9999;
    min-height:700px;
}

#ie8Stage3 .ie8popupOverlay {
    display: block;
    height: 100%;
    position: absolute;
    top: 0;
    width: 100%;
}

#ie8Stage3  .ie8popupBar {
    background: ${background};
    border-right: none;
    left: 0px;
    width:100%;
    min-height: 250px;
    position: relative;
    right: 0;
    top: 150px;
    z-index: 8010;
    -moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;	
	border-top:10px solid ${borderColour};
	text-align:center;
}

#ie8Stage3  .ie8ContentWrapper{
	background:url("${path}/00Assets/skins/${skin}/images/ie8Exit/plug.png") no-repeat;
	background-position: 0px bottom; 
	padding: 2% 2% 40px 210px;
    width: 70%;
	float:none;
	margin:auto;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;	
	overflow:hidden;
	text-align:left;
	${fontColour}
}


#ie8Stage3  .ie8header1{
	color:#ffffff;
	font-family:"FNBSansThin";
	font-size:36px;
	margin:0px;
	padding-bottom:5px;
	${fontColour}
}

#ie8Stage3  .ie8header2{
	color:#333333;
	font-family:"FNBSansThin";
	margin:0px;
	font-size:36px;
	padding-bottom:10px;
	${fontColour}
}

#ie8Stage3 .ie8p1{
	color:#ffffff;
	font-size:18px;
	margin:0px;
	padding:0;
	line-height: 20px;
	padding-bottom:10px;
	${fontColour}
}

#ie8Stage3 .ie8p2{
	color:#333333;
	font-size:14px;
	padding:0;
	margin:0px;
	${fontColour}
}

#ie8Stage3 .ie8p2 a{
	cursor:pointer;
}

#ie8Stage3 .ie8BtnWrapper{
	width:100%;
	margin:auto;
	text-align:center;
	float:none;
	margin-top:5%;
}

#ie8Stage3 .ie8Btn{
	border:none;
	padding: 10px 15px 10px 25px;
	cursor:pointer;
}

#ie8Stage3 .ie8DownloadBtn{
	background-position: 5px center;
	color:#ffffff;
	float:left;
	cursor:pointer;
	background: ${buttonColour};	
}

#ie8Stage3 .ie8FindOutMoreBtn{
	background: #ffffff url("${path}/00Assets/skins/${skin}/images/ie8Exit/orangeArrow.png") no-repeat;
	background-position: 5px center;
	float:right;
}

.clearBoth{
	clear:both;
}

#ie8Stage3 .ie8bottom{
	height:15px;
	width:60%;
	background: #ffffff url("${path}/00Assets/skins/${skin}/images/ie8Exit/bottom.png") repeat;
	background-size: 5px 16px;
	background-position:left bottom;
	float:right;
}

.ie8DisplayNone{
	display:none;
}

#ie8Stage3  .ie8ContentWrapper2{
	background:url("${path}/00Assets/skins/${skin}/images/ie8Exit/plug.png") no-repeat;
	background-position: 0px bottom; 
	padding: 2% 2% 40px 210px;
    width: 70%;
	float:none;
	margin:auto;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;	
	overflow:hidden;
	color:#ffffff!important;
	text-align:left;
	}

#ie8Stage3 .ie8ContentWrapper2 .ie8header1 {
    color: #ffffff;
    font-family: "FNBSansThin";
    font-size: 38px;
	margin:0px;
	${fontColour}
}

#ie8Stage3 .ie8ContentWrapper2 .ie8header2 {
    color: #ffffff;
    font-family: "FNBSansThin";
    font-size: 20px;
    margin:0px;
    margin-bottom: 1%;
    margin-top:15px;
    ${fontColour}
}

#ie8Stage3 .ie8ContentWrapper2 .ie8p1 {
    color: #FFFFFF;
    font-size: 14px;
    line-height: 20px;
    margin-bottom: 0%;
    padding: 0;
    margin:0px;
    ${fontColour}
}

#ie8Stage3 .ie8ContentWrapper2 .ie8p2 {
    color: #FFFFFF;
    font-size: 14px;
    line-height: 20px;
    margin-bottom: 0%;
    padding: 0;
    margin:0px;
    ${fontColour}
}

#ie8Stage3 .ie8ContentWrapper2 a {
    text-decoration:underline;
    color:#ffffff;
    ${fontColour}
}




</style>

<div id="ie8Stage3" class="">
	<div class="ie8popupOverlay " id="ie8Overlay">
		<div class="ie8popupBar">	
			<div class="ie8ContentWrapper " id="content1">
				<h1 class="ie8header1">We've pulled the plug on</h1>
				<h1 class="ie8header2">old technology.</h1>
				<p class="ie8p1">In keeping up with the times to keep your online banking experience tops, we will no longer be supporting Internet Explorer 8.</p>
				<p class="ie8p2">If you're still using an outdated browser, you will no longer be able to login to your Online Banking profile.</p>
				<p class="ie8p2">Upgrading is free and simple - and keeps you up to date with the latest technology.</p>
				<div class="ie8BtnWrapper">
					<button class="ie8Btn ie8DownloadBtn" id="downloadButton3" name="downloadButton3" onclick="openStage3Message();return false;">Download newer browser</button>
				</div>
			</div>
			<div class="ie8ContentWrapper2 ie8DisplayNone" id="content2">
				<h1 class="ie8header1">Downloads</h1>
				<h3 class="ie8header2">Windows XP</h3>
				<p class="ie8p1">If you are still using Windows XP, please download one of the newer browsers below as Internet Explorer will no longer work.</p>
				<p class="ie8p2"><a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank" class="reactNormal">Mozilla Firefox</a> (3.6 or higher)</p>
				<p class="ie8p2"><a href="https://www.google.com/intl/en/chrome/browser/" target="_blank" class="reactNormal">Google Chrome</a></p>
				<h3 class="ie8header2">Windows Vista/Windows 8</h3>
				<p class="ie8p2"><a href="http://www.microsoft.com/en-za/download/internet-explorer-9-details.aspx" target="_blank" class="reactNormal">Internet Explorer</a> (9 and 10)</p>
				<p class="ie8p2"><a  href="http://www.mozilla.org/en-US/firefox/new/"  target="_blank" class="reactNormal">Mozilla Firefox</a> (3.6 or higher)</p>
				<p class="ie8p2"><a href="https://www.google.com/intl/en/chrome/browser/" target="_blank" class="reactNormal">Google Chrome</a></p>
			</div>
			<div class="clearBoth"></div>
		</div>
	</div>
</div>

