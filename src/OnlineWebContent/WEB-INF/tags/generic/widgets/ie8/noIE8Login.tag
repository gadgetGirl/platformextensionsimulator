<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="false" name="skin"%>
<%@ attribute required="false" name="layout"%>

<c:set  var="path" value="${session_contextPath}"></c:set>

<c:set  var="buttonColour" value="color:#ffffff"></c:set>

<c:choose>
	<c:when test="${layout == 'banking'}">
		<c:choose>
			<c:when test="${skin == 6}">
				<c:set  var="primaryColour" value="#89c13c"></c:set>
				<c:set  var="secondaryColour" value="#747474"></c:set>
			</c:when>
			<c:when test="${skin == 12}">
				<c:set  var="primaryColour" value="#FFD200"></c:set>
				<c:set  var="secondaryColour" value="#094F97"></c:set>
				<c:set  var="buttonColour" value="color:#000000"></c:set>
			</c:when>
			<c:when test="${skin == 11}">
				<c:set  var="primaryColour" value="#435e93"></c:set>
				<c:set  var="secondaryColour" value="#ce0683"></c:set>
			</c:when>
			<c:when test="${skin == 1}">
				<c:set  var="primaryColour" value="#0f66ad"></c:set>
				<c:set  var="secondaryColour" value="#747474"></c:set>
			</c:when>			
			<c:when test="${skin == 2}">
				<c:set  var="primaryColour" value="#e7e7e7"></c:set>
				<c:set  var="secondaryColour" value="#5c447e"></c:set>
				<c:set  var="buttonColour" value="color:#000000;background: url('${path}/00Assets/skins/02/images/ie8Exit/gradient.png') repeat-x"></c:set>
			</c:when>
			<c:otherwise>
				<c:set  var="primaryColour" value="#ff9900"></c:set>
				<c:set  var="secondaryColour" value="#747474"></c:set>
			</c:otherwise>
		</c:choose>
 <!--[if lt IE 9]>			
<style>

#login-block-top.stage2Active .grid33{
display:none;
}

#login-block-top.stage2Active {
height:50px;
}


#ie8Stage2{
	position:absolute;
	height:auto;
	width:83%;
	left:5px;	
	top: 10px;
	z-index:29;
}

#ie8Stage2 .ie8loginCover{
	width:75%;
	background:${secondaryColour};
	color:#ffffff;
	float:left;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
	height:34px;
	line-height: 34px;
	padding: 0 10px;
	font-size: 14px;
}

#ie8Stage2 .ie8loginBlock{
	background:#D8D7D5;
	height: 34px;
    width: 100%;
}

#ie8Stage2  .ie8loginButton{
	width:15%;
	background:${primaryColour};
	${buttonColour};
	float:left;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
	height:34px;
	line-height: 34px;
	padding: 0 10px;
	text-align: center;
	cursor:pointer;
	font-size: 14px;
	margin-left:10px;
}

#ie8Stage2  #ie8loginMessage:before{
    background: url("${path}/00Assets/skins/00/images/ie8Exit/topArrow.png") no-repeat;
    content: "";
    display: block;
    height: 20px;
    left: 68%;
    position: relative;
    top: -26px;
    width: 70px;
}

#ie8Stage2 #ie8loginMessage{
	border:3px solid #ffffff;
	padding: 7px;
	position:relative;
	margin-top: 0px;
	background: #f1f1f1 ;
	width:75%;
}

#ie8Stage2 .ie8loginMessageContent{
	margin-top: -10px;
	width:80%;
}

#ie8Stage2 .ie8loginMessageContent p{
	color:#333333;
}

#ie8Stage2 .ie8BtnWrapper{
	width:100%;
	margin:auto;
	text-align:center;
	float:right;
	margin-top:15px;
}

#ie8Stage2 .ie8Btn{
	border:none;
	padding: 4px 15px 7px 25px;
	clear: both;
	width:230px;
	text-align:left;
}

#ie8Stage2 .ie8DownloadBtn2{
	background: ${primaryColour};
	${buttonColour};
	float:left;
}

#ie8Stage2 .ie8FindOutMoreBtn2{
	background: #ffffff url("${path}/00Assets/skins/00/images/ie8Exit/orangeArrow.png") no-repeat;
	background-position: 5px center;
	float:left;
	margin-top:10px;
}

#ie8Stage2 .ie8DisplayNone{
	display:none;
}


#ie8Stage2 #ie8loginCloseBtn {
    background: url("${path}/00Assets/skins/00/images/ie8Exit/close.png") no-repeat;
    cursor: pointer;
    height: 25px;
    position: absolute;
    right: 5%;
    top: 6px;
    width: 26px;
}

@media only screen and (max-width: 816px) {

#ie8Stage2{
	width:100%;
	background:#D8D7D5;
	left:0px;
	margin-top:-3px;
}

#ie8Stage2 .ie8loginBlock{
    height: 140px;
}

#ie8Stage2 #ie8loginMessage{
    margin-top: -105px;
}

}


</style>

		<div id="ie8Stage2" class="ie8DisplayNone">
			<div class="ie8loginBlock">
				<div class="ie8loginCover">Internet Explorer 8 to be phased out</div>
				<div class="ie8loginButton" id="ie8loginButton" name="ie8loginButton">OK</div>
			</div>
			<div class="ie8loginMessage" id="ie8loginMessage">
				<div class="ie8loginMessageContent">
					<p>In keeping up with the times to keep your online banking experience tops, we will no longer be supporting IE 8.</p>
					<div class="ie8BtnWrapper">
						<button class="ie8Btn ie8DownloadBtn2" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: '/banking/Controller?nav=softwaredownloads.SoftwareDownloads'}); return false;">Download newer browser</button>
					</div>
					<div style="clear:both;"></div>	
					<div id="ie8loginCloseBtn" name="ie8loginCloseBtn"></div>
							
				</div>
			</div>
		</div>
		<script class="pageScript" type="text/javascript">
		try{fnb.ie8Exit.init();}catch(e){};
		</script>
<![endif]-->		
	</c:when>
	<c:otherwise>
 <!--[if lt IE 9]>
<style>
#ie8Stage2{
	position:absolute;
	height:auto;
	width:47.5%;
	right:-2px;	
	top:0px;
	z-index:999999;
}

#ie8Stage2 .ie8loginCover{
	width:80%;
	background:#000000;
	color:#ffffff;
	float:left;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
	height:24px;
	line-height: 24px;
	padding: 0 0 0 5px;
	text-align:left;
	font-size:12px;
	
}

#ie8Stage2 .ie8loginButton{
	width:20%;
	background:#ff9900;
	color:#ffffff;
	float:left;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
	height:24px;
	line-height: 24px;
	padding: 0 10px;
	text-align: center;
	cursor:pointer;
}

#ie8loginMessage:before{
    background: url("${path}/00Assets/skins/00/images/ie8Exit/topArrow.png") no-repeat;
    content: "";
    display: block;
    height: 20px;
    left: 68%;
    position: relative;
    top: -26px;
    width: 70px;
}

#ie8loginMessage{
	border:3px solid #ffffff;
	padding: 7px;
	position:relative;
	margin-top: 27px;
	background: #f1f1f1 ;
	text-align:left;
}

#ie8Stage2 .ie8loginMessageContent{
	margin-top: -10px;
	width:80%;
}

#ie8Stage2 .ie8loginMessageContent p{
	color:#333333;
}

#ie8Stage2 .ie8BtnWrapper{
	width:100%;
	margin:auto;
	text-align:center;
	float:right;
	margin-top:5%;
}

#ie8Stage2 .ie8Btn{
	border:none;
	padding: 7px 15px 7px 25px;
	width:200px;
	text-align:left;
	display:block;
	text-decoration:none;
}

#ie8Stage2 .ie8DownloadBtn2{
	background: #ff9a00 url("${path}/00Assets/skins/00/images/ie8Exit/whiteArrow.png") no-repeat;
	background-position: 5px center;
	color:#ffffff;
	float:left;
	margin-bottom:10px;
	margin-right:20%;
}

#ie8Stage2 .ie8FindOutMoreBtn2{
	background: #ffffff url("${path}/00Assets/skins/00/images/ie8Exit/orangeArrow.png") no-repeat;
	background-position: 5px center;
	float:left;
	color:#000000;
}

.ie8DisplayNone{
	display:none;
}


#ie8loginCloseBtn {
    background: url("${path}/00Assets/skins/00/images/ie8Exit/closeAlt.png") no-repeat;
    cursor: pointer;
    height: 25px;
    position: absolute;
    right: 20px;
    top: 18px;
    width: 25px;
}

</style>

		<div id="ie8Stage2" class="ie8DisplayNone">
			<div class="ie8loginCover">Internet Explorer 8 to be phased out</div>
			<div class="ie8loginButton" id="ie8loginButton" name="ie8loginButton">OK</div>
			<div class="ie8loginMessage" id="ie8loginMessage">
				<div class="ie8loginMessageContent">
					<div id="ie8loginCloseBtn" name="ie8loginCloseBtn"></div>	
					<p>In keeping up with the times to keep your online banking experience tops, we will no longer be supporting IE 8.</p>
					<div class="ie8BtnWrapper">
						<a class="ie8Btn ie8DownloadBtn2 reactNormal" href="${session_contextPath}/tools/softwareDownloads.html">Download newer browser</a>
						<a class="ie8Btn ie8FindOutMoreBtn2 reactNormal" href="${session_contextPath}/promotions/PullingThePlugIE8/index.html" >Find out more</a>
						<div style="clear:both;"></div>	
					</div>
					<div style="clear:both;"></div>		
				</div>
			</div>
		</div>
<![endif]-->

	</c:otherwise>

</c:choose>