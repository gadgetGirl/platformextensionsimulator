<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld" %>

<%@ attribute name="utilityTitle" required="false" rtexprvalue="true"%>
<%@ attribute name="javascript" required="false" rtexprvalue="true"%>
<%@ attribute name="css" required="false" rtexprvalue="true"%>

<!doctype html>
<html>
	<head>
		<c:if test="${!empty css}">
			<link class="customCSS" rel="stylesheet" type="text/css" href="/01css/utilities/${css}.css"/>
		</c:if>
	
	</head>

	<ui:body>
		<div id="ui-utilityPage">
			<jsp:doBody></jsp:doBody>
		</div>
	</ui:body>
	
	
		<%-- moved the js import to the bottom of the page --%>
		<c:if test="${!empty javascript}">
			<c:choose>
				<c:when test="${fn:contains(javascript, 'http')}">
					<script class="customJs" type="text/javascript" src="${javascript}"></script>
				</c:when>
				<c:otherwise>
					<script class="customJs" type="text/javascript" src="/apps/02js/${javascript}.js"></script>
				</c:otherwise>
			</c:choose>
		</c:if>
</html>