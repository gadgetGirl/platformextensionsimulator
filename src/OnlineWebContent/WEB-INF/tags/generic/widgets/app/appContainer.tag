<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="id" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="app" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="parameters" required="false" rtexprvalue="true" type="java.lang.String"%>
<c:set var="parameters" value="${parameters}&id=${id}" />
<div id="${id}" class="application-widget ${not empty className}" data-application="${app}${parameters}">
	<jsp:doBody></jsp:doBody>
</div>