<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="href"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="className"%>
<c:if test="${not empty href}"><c:set var="href" value="href='${session_contextPath}${href}'"/></c:if>

<c:set var="body"><jsp:doBody /></c:set>
<a ${href}><li class="relatedPage ${className}">${label}</li></a>
