<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.widgets.redirect" tagdir="/WEB-INF/tags/generic/widgets/redirect"%>
<%@ attribute name="redirectPage" required="false" rtexprvalue="true" %>


<c:if test='${browserInfo.capabilities["is_wireless_device"]}'>
	<%-- Get device_os --%>
	<c:set var="device_os" value="${browserInfo.capabilities['device_os']}"/>
	<%-- Redirect if not matching --%>
	<c:if test="${device_os!='Android'&&device_os!='iOS'}">
		<c:choose>
			<%-- ZA --%>
			<c:when test="${session_country == 15}">
			
				<c:choose>
					<%-- RMB --%>
					<c:when test="${skin == 1}">
						<c:set var="redirect" value="http://rmbprivatebank.mobi"/>
					</c:when>
					<%-- FNB --%>
					<c:otherwise>
						<c:set var="redirect" value="http://fnb.mobi"/>
					</c:otherwise>
				</c:choose>
				
			</c:when>
			<%-- GG --%>
			<c:when test="${session_country == 25}">
				<c:set var="redirect" value=""/>
			</c:when>
			<%-- NA --%>
			<c:when test="${session_country == 45}">
				<c:set var="redirect" value="http://www.fnbna.mobi"/>
			</c:when>
			<%-- BW --%>
			<c:when test="${session_country == 55}">
				<c:set var="redirect" value="http://www.fnbbw.mobi"/>
			</c:when>
			<%-- SZ --%>
			<c:when test="${session_country == 65}">
				<c:set var="redirect" value="http://www.fnbswaziland.mobi"/>
			</c:when>
			<%-- LS --%>
			<c:when test="${session_country == 80}">
				<c:set var="redirect" value="http://www.fnblesotho.mobi"/>
			</c:when>
			<%-- ZM --%>
			<c:when test="${session_country == 81}">
				<c:set var="redirect" value="http://www.fnbzambia.mobi"/>
			</c:when>
			<%-- TZ --%>
			<c:when test="${session_country == 82}">
				<c:set var="redirect" value="http://www.fnbtz.mobi"/>
			</c:when>
			<%-- IN --%>
			<c:when test="${session_country == 83}">
				<c:set var="redirect" value=""/>
			</c:when>
		</c:choose>
		<c:if test="${not empty redirect}">
			<fnb.generic.widgets.redirect:redirect redirectLink="${redirect}"/>
		</c:if>
		
	</c:if>
</c:if>




