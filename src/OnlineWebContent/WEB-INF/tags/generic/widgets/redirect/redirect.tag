<%@ attribute name="redirectLink" required="false" rtexprvalue="true" %>


<jsp:forward page="/err/redirect.jsp" >
	<jsp:param name="redirectLink" value="${redirectLink}"/>
</jsp:forward>