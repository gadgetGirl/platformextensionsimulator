<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute name="id" required="false" rtexprvalue="true" description="id of paragraph" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="accountName" required="false" rtexprvalue="true" %>
<%@ attribute name="onclick" required="false" rtexprvalue="true" %>
<%@ attribute name="rate" required="false" rtexprvalue="true" %>
<%@ attribute name="img" required="false" rtexprvalue="true" %>
<%@ attribute name="canChangeCard" required="true" rtexprvalue="true" %>
<div class="selectedCardContainer">
	<div class="selectedCard">
		${accountName}
		<c:if test="${canChangeCard == true}">
			<fnb.generic.forms:dataButton label="Change" url="${onclick}" event="loadIntoEzi"/>
		</c:if>
	</div>
	<c:if test="${not empty rate}">
		<p>Interest rate: ${rate}</p>
	</c:if>
	<img src="${img}" width="50%" />
</div>

