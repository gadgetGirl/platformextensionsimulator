<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/fn.tld" prefix="fn"%>
<%@ attribute required="true" name="heading"%>
<%@ attribute required="true" name="imageSrc"%>
<%@ attribute required="true" name="bodyCopy"%>
<%@ attribute required="true" name="href"%>
<%@ attribute required="true" name="id"%>
<a id="${id}" href="${fn:contains(href,'://')?href:session_contextPath}${href}" data-role="thumbnailBanner">
	<div data-role="thumbnailBannerInner">
		<div data-role="thumbnailBannerHeading">${heading}</div>
 		<div data-role="thumbnailImgTagWrapper">
 			<img src="${imageSrc}" />
 		</div>
		<div data-role="thumbnailBannerCopy">${bodyCopy}</div>
	</div>
</a>
