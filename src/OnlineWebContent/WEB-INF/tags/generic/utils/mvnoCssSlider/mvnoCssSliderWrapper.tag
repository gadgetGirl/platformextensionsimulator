<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoCssSlider" tagdir="/WEB-INF/tags/generic/utils/mvnoCssSlider"%>


<%@ attribute required="false"  name="sliderHeading" %>

<c:if test="${not empty sliderHeading }">
	<c:set var="sliderHeading"><span data-role="sliderHeading">${sliderHeading }</span></c:set>
</c:if>  
 
<div data-role="mvnoSliderWrapper">
${sliderHeading }
	 <jsp:doBody />
</div>