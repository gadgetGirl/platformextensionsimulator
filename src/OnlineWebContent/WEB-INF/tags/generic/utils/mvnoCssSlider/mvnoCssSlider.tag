<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false"  name="sliderLabel"%>
<%@ attribute required="false"  name="sliderLeft"%>
<%@ attribute required="false"  name="sliderUsed"%>
<%-- sliderUsage need to be a percentage value (0-100) --%>
<%@ attribute required="false"  name="sliderUsage"%>


<c:if test="${empty sliderUsage }">
	<c:set var="sliderUsage">0</c:set> 
</c:if>

 <c:if test="${not empty sliderLabel }">
 	<c:set var="sliderLabel">
 		<span data-role="mvnoCssSliderLabel">${sliderLabel }</span>
 	</c:set> 
 </c:if>
 
  <c:if test="${not empty sliderLeft }">
 	<c:set var="sliderLeft">
 		<span data-role="mvnoCssSliderLeft">${sliderLeft }</span>
 	</c:set> 
 </c:if>
 
  <c:if test="${not empty sliderUsed }">
 	<c:set var="sliderUsed">
 		<span data-role="mvnoCssSliderUsed">${sliderUsed }</span>
 	</c:set> 
 </c:if>
  
 
<div data-role="mvnoCssSlidertableWrapper">
	<div data-role="mvnoCssSliderTableCell">
		<div data-role="mvnoCssSliderLabelContainer">
			${sliderLabel }
			${sliderLeft }
			${sliderUsed }
		</div>
		<div class="mvnoCssSliderProgressContainer"><div class="mvnoCssSliderProgressBar" style='width:${sliderUsage }%;'></div></div>
	</div>
</div>