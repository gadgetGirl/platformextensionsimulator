<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoCssSlider" tagdir="/WEB-INF/tags/generic/utils/mvnoCssSlider"%>


<%@ attribute required="false"  name="sliderHeading" %>

<c:if test="${not empty sliderHeading }">
	<c:set var="sliderHeading"><span data-role="mvnoCssSliderheading">${sliderHeading }</span></c:set>
</c:if>  
 
<div data-role="mvnoCssSliderContainer">
${sliderHeading }
	 <jsp:doBody />
</div>