<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="visible"%>
<c:if test="${not empty id}">
	<c:set var="id">
	id="${id}"
	</c:set>
</c:if>
<c:if test="${not empty className}">
	<c:set var="className">
	class="${className}"
	</c:set>
</c:if>
<c:if test="${empty visible}">
	<c:set var="visible" value="false"/>
</c:if>

<div ${id} ${className} data-role="subTabContent" data-visible="${visible}" class="Hhide">
	<jsp:doBody/>
</div>