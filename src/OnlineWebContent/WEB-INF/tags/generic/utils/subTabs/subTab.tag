<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="selected"%>
<%@ attribute required="false" name="contentArea"%>
<%@ attribute required="false" name="url"%>
<%@ attribute required="false" name="event"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="target"%>
<%@ attribute required="false" name="targetElement"%>
<%@ attribute required="false" name="dataTarget"%>
<%@ attribute required="false" name="clearHtmlTemplates"%>
<%@ attribute required="false" name="clearPageModuleObject"%>
<%@ attribute required="false" name="clearPageEventsArray"%>
<%@ attribute required="false" name="clearPageTemplatesArray"%>
<c:set var="className">
	<c:if test="${not empty className}">class="${className}" </c:if>
</c:set>
<c:set var="selected">
	<c:if test="${not empty selected}">data-selected="true" </c:if>
</c:set>



<div data-role="subTab" id="${id}SubTab"${className}${selected}data-content="forMeContent">${label}</div>



data-settings="[{"event": "loadEzi","url": "pages/eziPage.jsp","target": "", "urlTarget": "","dataTarget": "", "clearHtmlTemplates": false, "clearPageModuleObject": false, "clearPageEventsArray": false, "clearPageTemplatesArray": false}]"