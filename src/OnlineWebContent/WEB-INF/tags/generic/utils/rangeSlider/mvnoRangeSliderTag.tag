<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="true"  name="totalSnapPoints" %>
<%@ attribute required="true"  name="sliderName" %>
<%@ attribute required="true"  name="snapPointSelected" %>
<%@ attribute required="false" name="sliderColor" %>
<%@ attribute required="true"  name="sliderId"%> 
<%@ attribute required="true"  name="snapPointSelectedValue"%>
<%@ attribute required="true"  name="snapPointSelectedLabel"%>	
<%@ attribute required="false"  name="btnLabelClassName"%>
<%@ attribute required="false"  name="disabled"%>
<%@ attribute required="false"  name="hideButton"%>

<c:if test="${ not empty snapPointSelected  && not empty totalSnapPoints}">

	<%-- preset progress class  --%>	
	<c:set var="progressClass">
		class="sp_${snapPointSelected-1}_snapPoints${totalSnapPoints}"
	</c:set>

 	<%-- preset btn class --%>
	<c:set var="btnClass">
		class="btn_${snapPointSelected-1}_snapPoints${totalSnapPoints}"
	</c:set>
</c:if>

<c:if test="${ empty snapPointSelected  && empty totalSnapPoints}">
	<%-- preset progress class  --%>	
	<c:set var="progressClass">
		class="sp_${snapPointSelected-1}_snapPoints${totalSnapPoints}"
	</c:set>

 	<%-- preset btn class --%>
	<c:set var="btnClass">
		class="btn_${snapPointSelected-1}_snapPoints${totalSnapPoints}"
	</c:set>

</c:if>

<c:if test="${not empty sliderColor }">
	<c:set var="sliderColor">
		style="background-color:${sliderColor };"
	</c:set>
</c:if>

<c:set var="halfValue" value="${totalSnapPoints / 2 }" />
<c:choose>
	<c:when test="${halfValue < snapPointSelected}">
		<c:set var="btnLabelClassName" value="btnLeft" />
	</c:when>
	<c:otherwise>
		<c:set var="btnLabelClassName" value="btnRight" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${hideButton == 'true' }">
		<c:set  var="hideButton"> Hhide</c:set>
	</c:when>
	<c:otherwise>
		<c:set  var="hideButton" value=""/>
	</c:otherwise>
</c:choose>

<c:set var="btnLabelClassName">
	<c:choose>
		<c:when test="${totalSnapPoints == '0' }">
			class="btnRight ${hideButton }"
		</c:when>
		<c:otherwise>
			class="${btnLabelClassName } ${hideButton }"
	   </c:otherwise>
	</c:choose>	
</c:set>

<c:choose>

	<c:when test="${ disabled == 'true' || totalSnapPoints == '0' }">
		<c:set var="btnLabel">data-role="btnLabelZero"</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="btnLabel">data-role="btnLabel"</c:set>
	</c:otherwise>

</c:choose>
	
	<div data-role="sliderContainer" id="sliderContainer">
	    <span data-role="sliderProgress" id="sliderProgress"  ${progressClass }  ${sliderColor } >
	   	<span data-role="sliderBtn" data-sliderId="${sliderId }" id="sliderBtn${sliderId}" ${btnClass } ></span>
	    <span ${btnLabel} ${btnLabelClassName }  id="btnLabel${sliderId}" data-sliderId="${sliderId }">${snapPointSelectedLabel }</span></span>  
	</div>
	<%-- <input name="${sliderName }" value="${snapPointSelectedValue }" type="hidden"> --%>
