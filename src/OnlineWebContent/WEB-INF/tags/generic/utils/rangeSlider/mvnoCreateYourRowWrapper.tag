<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute name="className" required="false" %>

<div class="mvnoCreateYourOwnTableRowWrapper ${className }">	
 	<jsp:doBody />
</div>