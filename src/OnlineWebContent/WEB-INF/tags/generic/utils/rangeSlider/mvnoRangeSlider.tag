<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.rangeSlider" tagdir="/WEB-INF/tags/generic/utils/rangeSlider"%>

<%@ attribute required="false"  name="totalSnapPoints" %>
<%@ attribute required="false"  name="sliderName" %>
<%@ attribute required="false"  name="snapPointSelected" %>
<%@ attribute required="false"  name="sliderColor"%>
<%@ attribute required="true"   name="sliderId" %>
<%@ attribute required="false"  name="sliderLabel" %>
<%@ attribute name="sliderRate" required="false" %>
<%@ attribute name="youPayCurrancy" required="false" %>
<%@ attribute name="inputName" required="false" %>
<%@ attribute name="inputValue" required="false" %>
<%@ attribute name="currency" required="false" %>
<%@ attribute name="itemCode" required="false" %>
<%@ attribute name="itemSkuCode" required="false" %>
<%@ attribute name="disabled" required="false" %>
<%@ attribute name="hideButton" required="false" %>
<%@ attribute name="sliderType" required="true" %>
<%--
        SET TEST DATA ON or OFF
--%>
<c:set var="testData" value="false" />

<%@ attribute required="false"  name="snapPointsBean" type="java.util.ArrayList" description="A list products"%>

<c:choose>	
	<c:when test="${disabled == 'true' }">	
		<c:set var="disabled" value="true"/>
	</c:when>
	<c:otherwise>
		<c:set var="disabled" value="false"/> 
	</c:otherwise>
</c:choose>		



<%-- get SnapPoint add save to variable snapPoints--%>
<c:set var="snapPointsHTML">
	<div id="snapPoints" >
	<%-- Start for each --%>
		<%-- build script for now that will loop for totalSnapPoints times --%>
	
	<c:choose>
		<%-- build slider snapPoint if total snapPoint is greater than one --%>
		<c:when test="${totalSnapPoints != '1' }">
					<%-- Do For each Test Data 
					<c:forEach var="i" begin="1" end="${totalSnapPoints}" varStatus="tabCount" > --%>
			    
					<%-- Do For each Bean Data --%>
					
			    	<c:forEach items="${snapPointsBean}" var="mvnoSliderItem" varStatus="tabCount" >
		    	    <c:set var="snapPointSelceted" value="false" />
					
					
						<c:if test="${testData == 'true' }" >
						<%-- TEST DATA,  snapPoint Value, Label --%>
			 				<c:set var="snapPointSelectedValue"  value="${tabCount.count}00" /> 
							<c:set var="snapPointSelectedLabel"  value="${tabCount.count}00mb" />
							
							<c:choose>
								<c:when test="${tabCount.count == '1' }"> 
										<c:set var="fistSnapPointValue" value="0" />
										<c:set var="snapPointBtnLabel"  value="${fistSnapPointValue}" />
								</c:when>
								<c:otherwise>
									<c:set var="snapPointBtnLabel"> ${fistSnapPointValue} + ${snapPointSelectedLabel} </c:set>
								</c:otherwise>
							</c:choose>
						</c:if>			
								
						<c:if test="${tabCount.count == snapPointSelected  }">
							<%-- snapPoint selected set equals to true else equals to false  --%>
							<c:set var="snapPointSelceted" value="true" />
							<%-- <c:set var="sliderBtnSelectedLabel">${snapPointBtnLabel}</c:set> --%>
							<c:set var="sliderBtnSelectedLabel">${mvnoSliderItem.buttonLabel}</c:set>
						 	
						 	<%-- build html for you pay cell  check for test Data or real Data--%>
						 	<c:choose>
							 	<c:when test="${testData == 'true' }" >
									<c:set var="youPayCellHTML">
							 			<span class="rateLabel">Rate</span>
										<span class="rateValue" id="youPayRate${sliderId}">0.25/MIN</span>
										
										<span class="youPayLabel">You Pay</span>
										<span class="youPay"><span class="currency"><c:if test="${empty currency }">R </c:if> ${currency }</span>
												<span id="youPayValue${sliderId}" class="youPayValue" data-itemcode="PT00${tabCount.count}" data-itemskucode="SKU00${tabCount.count}">${ snapPointSelectedValue}</span>
												<c:if test="${sliderType == 'banking' }">  <input  id="youPayInput${sliderId }" name="${inputName}" value="PT00${tabCount.count}" type="hidden" /> </c:if>
									    </span>
								 	</c:set>
							 	</c:when>
							 	<%-- Use Bean Data --%>
							 	<c:otherwise>
								 	<%-- build html for you pay cell --%>
								 	<c:set var="youPayCellHTML">
								 			<span class="rateLabel">${mvnoSliderItem.bundleRateLabel }</span>
											<span class="rateValue" id="youPayRate${sliderId }">${mvnoSliderItem.bundleRateValue }</span>
											
											<span class="youPayLabel">You Pay</span>
											<span class="youPay"><span class="currency"><c:if test="${empty currency }">R </c:if> ${currency }</span>
													<span id="youPayValue${sliderId}" class="youPayValue" data-itemcode="${mvnoSliderItem.productId}" data-itemskucode="${mvnoSliderItem.productCode }">${ mvnoSliderItem.value}</span>
													<c:choose>
													
														<c:when test="${empty mvnoSliderItem.productId}">
															<c:set var="productId" value="0" />
														</c:when>
														<c:otherwise>
															<c:set var="productId" value="${mvnoSliderItem.productId}" />
														</c:otherwise>
																				
													</c:choose>
													
													<input  id="youPayInput${sliderId }" name="${inputName}" value="${productId}" type="hidden" />
										    </span>
								 	</c:set>
							 	</c:otherwise>

						 	</c:choose>
						</c:if>
					<c:choose>
					
						<%-- check if if snapPoint is the first one --%>
						<c:when test="${tabCount.count == '1' }">							
							<%-- add snapPoint check for test or Bean Data to use --%>
						 	<c:choose>
							 	<c:when test="${testData == 'true' }" >
							 		<fnb.generic.rangeSlider:mvnoSnapPoints  disabled="${disabled }"   id="pointer${tabCount.count}" totalSnapPoints="${totalSnapPoints-1 }" value="${snapPointSelectedValue}" label="${snapPointSelectedLabel}" sliderId="${sliderId }" snapPointNr="${tabCount.count -1 }" listPosition="first" firstlabel="0" itemSkuCode="SKU00${tabCount.count}" itemCode="PT00${tabCount.count}" dataBtnLabel="${snapPointBtnLabel}" rate="R0.${tabCount.count }/mb" snapPointSelceted="${snapPointSelceted}" />
				   				</c:when>
				   				<c:otherwise>
				   					<fnb.generic.rangeSlider:mvnoSnapPoints  disabled="${disabled }"   id="pointer${tabCount.count}" totalSnapPoints="${totalSnapPoints-1 }" value="${mvnoSliderItem.value}" label="${mvnoSliderItem.snapPointLabel}" sliderId="${sliderId }" snapPointNr="${tabCount.count -1 }" listPosition="first" firstlabel="0" itemSkuCode="${mvnoSliderItem.productCode}" itemCode="${mvnoSliderItem.productId}" dataBtnLabel="${mvnoSliderItem.buttonLabel}" rate="${mvnoSliderItem.rate}" snapPointSelceted="${snapPointSelceted}" />
				   				</c:otherwise>
				   			</c:choose>
						</c:when>
						<%-- check if if snapPoint is the last one --%>
						<c:when test="${tabCount.last}">
							<%-- add snapPoint check for test or Bean Data to use --%>	
							<c:choose>
							 	<c:when test="${testData == 'true' }" >
				   					<fnb.generic.rangeSlider:mvnoSnapPoints id="pointer${tabCount.count}" totalSnapPoints="${totalSnapPoints-1 }" value="${snapPointSelectedValue}" label="${snapPointSelectedLabel}" sliderId="${sliderId }" snapPointNr="${tabCount.count-1}" listPosition="last" itemSkuCode="SKU00${tabCount.count}" itemCode="PT00${tabCount.count}" dataBtnLabel="${snapPointBtnLabel}" disabled="${disabled }" rate="R0.${tabCount.count }/mb" snapPointSelceted="${snapPointSelceted}" />  
								</c:when>
								<c:otherwise>
									<fnb.generic.rangeSlider:mvnoSnapPoints id="pointer${tabCount.count}" totalSnapPoints="${totalSnapPoints-1 }" value="${mvnoSliderItem.value}" label="${mvnoSliderItem.snapPointLabel}" sliderId="${sliderId }" snapPointNr="${tabCount.count-1}" listPosition="last" itemSkuCode="${mvnoSliderItem.productCode}" itemCode="${mvnoSliderItem.productId}" dataBtnLabel="${mvnoSliderItem.buttonLabel}" disabled="${disabled }" rate="${mvnoSliderItem.rate}" snapPointSelceted="${snapPointSelceted}" />  
								</c:otherwise>
							</c:choose>
						
						</c:when>
						<%-- add snapPoint --%>
		 		        <c:otherwise>
			 		         <%-- check for test or Bean Data to use --%>	
			 		         <c:choose>
			 		         	<c:when test="${testData == 'true' }">
			 		         		<fnb.generic.rangeSlider:mvnoSnapPoints id="pointer${tabCount.count}" totalSnapPoints="${totalSnapPoints-1 }" value="${snapPointSelectedValue}" label="${snapPointSelectedLabel}" sliderId="${sliderId}" snapPointNr="${tabCount.count-1}" itemSkuCode="SKU00${tabCount.count}" itemCode="PT00${tabCount.count}" dataBtnLabel="${snapPointBtnLabel}" disabled="${disabled }" rate="R0.${tabCount.count }/mb" snapPointSelceted="${snapPointSelceted}"  />
			 		         	</c:when>
			 		         	<c:otherwise>
			 		         		<fnb.generic.rangeSlider:mvnoSnapPoints id="pointer${tabCount.count}" totalSnapPoints="${totalSnapPoints-1 }" value="${mvnoSliderItem.value}" label="${mvnoSliderItem.snapPointLabel}" sliderId="${sliderId}" snapPointNr="${tabCount.count-1}" itemSkuCode="${mvnoSliderItem.productCode}" itemCode="${mvnoSliderItem.productId}" dataBtnLabel="${mvnoSliderItem.buttonLabel}" disabled="${disabled }" rate="${mvnoSliderItem.rate}" snapPointSelceted="${snapPointSelceted}" />
			 		         	</c:otherwise>
			 		         </c:choose>
						</c:otherwise>
					</c:choose>
			</c:forEach>
			
		</c:when>
		<%-- build slider snapPoint if total snapPoint is equals one --%>
		<c:otherwise>
				<%-- add snapPoint check for test or Bean Data to use --%>	
				<c:choose>
				
					<c:when test="${testData == 'true' }" >
							<c:set var="youPayCellHTML">
						 			<span class="rateLabel">Rate</span>
									<span class="rateValue" id="youPayRate${sliderId}">0.25/MIN</span>
									
									<span class="youPayLabel">You Pay</span>
									<span class="youPay"><span class="currency"><c:if test="${empty currency }">R </c:if> ${currency }</span>
											<span id="youPayValue${sliderId}" class="youPayValue" data-itemcode="PT00${tabCount.count}" data-itemskucode="SKU00${tabCount.count}">300</span>
											<input  id="youPayInput${sliderId }" name="${inputName}" value="PT00${tabCount.count}" type="hidden" />
								    </span>
			  		
						 	</c:set>
						 	<c:set var="sliderBtnSelectedLabel" value="300mb" />
						 	<fnb.generic.rangeSlider:mvnoSnapPoints id="pointer0" totalSnapPoints="1" value="300" label="300mb" sliderId="${sliderId }" snapPointNr="0" listPosition = "first"  itemSkuCode="SKU001" itemCode="PT001" dataBtnLabel="300mb" snapPointSelceted="true" />
							<fnb.generic.rangeSlider:mvnoSnapPoints id="pointer1" totalSnapPoints="1" value="" label="" sliderId="${sliderId }" snapPointNr="1" listPosition="last"   itemCode="" dataBtnLabel=""/>
					</c:when>
					<c:otherwise>
						<c:forEach items="${snapPointsBean}" var="mvnoSliderItem" varStatus="tabCount" >
							 
							 <c:set var="sliderBtnSelectedLabel" value="${mvnoSliderItem.buttonLabel}" />
								<c:set var="youPayCellHTML">
							 			<span class="rateLabel">${mvnoSliderItem.bundleRateLabel }</span>
										<span class="rateValue" id="youPayRate${sliderId}">${mvnoSliderItem.bundleRateValue }</span>
										
										<span class="youPayLabel">You Pay</span>
										<span class="youPay"><span class="currency"><c:if test="${empty currency }">R </c:if> ${currency }</span>
												<span id="youPayValue${sliderId}" class="youPayValue" data-itemcode="${mvnoSliderItem.productId}" data-itemskucode="${mvnoSliderItem.productCode}">${mvnoSliderItem.snapPointLabel }</span>
													<c:choose>
														<c:when test="${empty mvnoSliderItem.productId}">
															<c:set var="productId" value="0" />
														</c:when>
														<c:otherwise>
															<c:set var="productId" value="${mvnoSliderItem.productId}" />
														</c:otherwise>												
													</c:choose>
												
												
												
												<input  id="youPayInput${sliderId }" name="${inputName}" value="${mvnoSliderItem.value}" type="hidden" />
									    </span>
							 	</c:set>
							 	
							 	<fnb.generic.rangeSlider:mvnoSnapPoints id="pointer0" totalSnapPoints="1" value="${mvnoSliderItem.value}" label="${mvnoSliderItem.snapPointLabel}" sliderId="${sliderId }" snapPointNr="0" listPosition = "first"  itemSkuCode="${mvnoSliderItem.productCode}" itemCode="${mvnoSliderItem.productId}" dataBtnLabel="${mvnoSliderItem.buttonLabel}" rate="${mvnoSliderItem.rate}" snapPointSelceted="${snapPointSelceted}"/>
								<fnb.generic.rangeSlider:mvnoSnapPoints id="pointer1" totalSnapPoints="1" value="" label="" sliderId="${sliderId }" snapPointNr="1" listPosition="last"   itemCode="" dataBtnLabel=""/>
						</c:forEach>
					</c:otherwise>	
				</c:choose>	
		</c:otherwise>
		
	</c:choose>
	
	<%-- ForEach ends --%>		
    </div>
</c:set>

<%-- table cell wrapper for Create your own package rangeSlider --%>
<div class="mvnoCreateYourOwnTableCell sliderCell">
	
	<fnb.generic.rangeSlider:mvnoRangeSliderWrapper sliderNameB="${sliderName }"  sliderId="${sliderId }" sliderLabel="${sliderLabel }" sliderImgSrc=""> 
		
		 <%-- preset slider and slider btn to selected snap-point --%>																																																										 
	    <fnb.generic.rangeSlider:mvnoRangeSliderTag disabled="${disabled }" sliderName="${customSimPackageVoice }" sliderId="${sliderId }" snapPointSelected="${snapPointSelected }" totalSnapPoints="${totalSnapPoints-1 }" sliderColor="${sliderColor }"  snapPointSelectedValue="${snapPointSelectedValue}" snapPointSelectedLabel="${sliderBtnSelectedLabel}" hideButton="${hideButton }"/>
	    ${snapPointsHTML }
	    	   
	</fnb.generic.rangeSlider:mvnoRangeSliderWrapper>
	
</div>



<%-- table cell wrapper for Create your own package detail cell --%>				
<div id="youPayForm" class="mvnoCreateYourOwnTableCell youPayCell">
		${youPayCellHTML }
</div>



