<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="true" name="sliderNameB" %>
<%@ attribute required="true" name="sliderId" %>
<%@ attribute required="true" name="sliderLabel" %>
<%@ attribute required="true" name="sliderImgSrc" %>

<c:choose>
	<c:when test="${sliderLabel == 'SMS' }">
		<c:set var="sliderImgClass" value="sliderImgSMS" />
	</c:when>
	<c:when test="${sliderLabel == 'DATA' }">
		<c:set var="sliderImgClass" value="sliderImgDATA" />
	</c:when>
	<c:when test="${sliderLabel == 'VOICE' }">
		<c:set var="sliderImgClass" value="sliderImgVOICE" /> 
	</c:when>
</c:choose>


<%-- wrapper for rangeSlider   --%>
<div data-role="rangeSliderContainer" id="rangeSliderContainer${sliderId}"  data-sliderName="${sliderName }">
 <div class="sliderRow">
   <span class="sliderLabel">${sliderLabel}</span>
   <span class="sliderImgWrapper ${sliderImgClass }"></span>
  
 </div>
 <jsp:doBody />



<%-- <input name="${sliderName}" type="hidden" value=""> --%>
</div>