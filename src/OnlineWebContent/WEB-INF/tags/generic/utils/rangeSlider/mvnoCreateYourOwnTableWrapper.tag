<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.rangeSlider" tagdir="/WEB-INF/tags/generic/utils/rangeSlider"%>

<%@ attribute name="className" required="false" %>
<%@ attribute name="reInt" required="false"%>
<%@ attribute name="trusted" required="false"%>
<%@ attribute name="availableCredit" required="false"%>
<c:choose>
	<c:when test="${empty trusted  }">
		<c:set var="trusted" > data-trusted="true" </c:set>
	</c:when> 
	<c:otherwise>
		<c:set var="trusted" > data-trusted="${trusted}" </c:set>
	</c:otherwise>
</c:choose>

<c:if test="${not empty reInt }"> 
	<c:set var="reInt" > data-reInt="true" </c:set> 
</c:if>

<c:if test="${not empty className }">
	<c:set var="className" > class="${className }" </c:set> 
</c:if>

<c:choose>
	<c:when test="${empty availableCredit  }">
		<c:set var="availableCredit" > data-availableCredit="true" </c:set>
	</c:when> 
	<c:otherwise>
		<c:set var="availableCredit" > data-availableCredit="${availableCredit}" </c:set>
	</c:otherwise>
</c:choose>

<div class="mvnoCreateYourOwnTableWrapper ${className }">
	<%-- Add Flag to ReInt --%>
	<span id="mvnoSettings" ${reInt } ${trusted } ${availableCredit }></span>
	<fnb.generic.rangeSlider:mvnoCreateYourRowWrapper>
		<div class="mvnoCreateYourOwnTableCell sliderCell">
	
		</div>
		<%-- table cell wrapper for Create your own package detail cell --%>				
		<div class="mvnoCreateYourOwnTableCell youPayCell">
				
		</div>

	</fnb.generic.rangeSlider:mvnoCreateYourRowWrapper>
	
	<%-- add body content --%>
	<jsp:doBody />
</div>