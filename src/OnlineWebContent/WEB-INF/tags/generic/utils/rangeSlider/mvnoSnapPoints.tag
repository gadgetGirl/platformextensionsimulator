<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute required="false"  name="id" %>
<%@ attribute required="true"   name="totalSnapPoints" %>
<%@ attribute required="false"  name="value" %>
<%@ attribute required="false"  name="label" %>
<%@ attribute required="true"   name="sliderId" %>
<%@ attribute required="true"   name="snapPointNr" %>
<%@ attribute required="false"   name="firstlabel" %>
<%@ attribute required="false"   name="listPosition" %>
<%@ attribute required="false"   name="itemCode" %>
<%@ attribute required="false"   name="itemSkuCode" %>
<%@ attribute required="false"   name="dataBtnLabel" %>
<%@ attribute required="false"   name="rate" %>
<%@ attribute required="false"   name="disabled" %> 
<%@ attribute required="false"   name="snapPointSelceted" %>

<%-- set id html for snapPoint --%>
<c:if test="${not empty id }">
	<c:set var="id">
		id="${id }" 
	</c:set>
</c:if>

<%-- preset the classes by using the totalSnapPoint  --%>
<c:choose>
	<c:when test="${not empty totalSnapPoints}">
		<c:if test="${totalSnapPoints <= 20 }">
			<c:set var="className"> point snapPoints${totalSnapPoints} </c:set>
		</c:if>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${disabled == 'true' }">
				<c:set var="className"> pointDisabled </c:set>
			</c:when>
			<c:otherwise>
				<c:set var="className"> point </c:set>
			</c:otherwise>
		</c:choose>
		
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${not empty value }">
		<c:set var="value">
			 data-value="${value }"
		</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="value">
			 data-value="0"
		</c:set>
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty itemCode}">
		<c:set var="itemCode"> data-itemcode="${itemCode}"</c:set>
	</c:when>
	<c:otherwise>
			<c:set var="itemCode"> data-itemcode="none"</c:set>
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty itemSkuCode}">
		<c:set var="itemSkuCode">data-itemskucode="${itemSkuCode}"</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="itemSkuCode">data-itemskucode="none"</c:set>	
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty rate}">
		<c:set var="rate"> data-rate="${rate }" </c:set>
	</c:when>
	<c:otherwise>
		<c:set var="rate"> data-rate="0" </c:set>
	</c:otherwise>
</c:choose>

<c:if test="${not empty snapPointSelceted }">
	<c:set var="snapPointSelceted"> data-snapPointSelceted="${snapPointSelceted }" </c:set>
</c:if>

<c:if test="${not empty dataBtnLabel}">
	<c:set var="dataBtnLabel">data-BtnLabel="${dataBtnLabel }" </c:set>
</c:if>


<c:set var="snapPointPercentage"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${ 100 / totalSnapPoints}" /></c:set>

<c:set var="dataPercentage"> data-percentage="${snapPointPercentage * snapPointNr }"</c:set>

<%-- need total nr of Snappoint  --%>  


   <c:choose>
   		<c:when test="${listPosition =='last' && totalSnapPoints != '1'}">
   			<span data-sliderId="${sliderId }"  ${id }  class="${className }" ${value }  data-percentage="100" ${itemCode }  ${itemSkuCode} ${dataBtnLabel} ${rate } ${snapPointSelceted }>
   	   			<span class="pointLabel">${label}</span>	
   	   		</span>
   		</c:when>
   		<c:when test="${listPosition =='first' && totalSnapPoints != '1' }">
   			<span data-sliderId="${sliderId }"  ${id }  class="firstPoint ${className }" ${value }   ${dataPercentage }  ${itemCode } ${itemSkuCode} ${dataBtnLabel} ${rate } ${snapPointSelceted }>
	   			<%-- <span class="firstPoint"> <c:if test="${empty firstlabel}">0</c:if> ${firstlabel}</span> --%>
	   			<span class="pointLabel firstPointLabel">${label}</span>
   			</span>
   		</c:when>
   		<c:when test="${listPosition =='first' && totalSnapPoints == '1'  }">
   			<span data-sliderId="${sliderId }"  ${id }  class="firstPoint"  ${value }   ${dataPercentage }  ${itemCode } ${itemSkuCode} ${dataBtnLabel} ${rate } ${snapPointSelceted }>
	   			<%-- <span class="firstPoint"> <c:if test="${empty firstlabel}">0</c:if> ${firstlabel}</span> --%>
	   			<span class="pointLabel firstPointLabel">${label}</span>
   			</span>
   		</c:when>
   		<c:when test="${listPosition =='last' && totalSnapPoints == '1'}">
   			<span data-sliderId="${sliderId }"  ${id } ${value }  data-percentage="100%" ></span>
   		</c:when>
   		<c:otherwise>
   			<span data-sliderId="${sliderId }"  ${id }  class="${className }" ${value }   ${dataPercentage }  ${itemCode } ${itemSkuCode} ${dataBtnLabel } ${rate } ${snapPointSelceted }>
   				<span class="pointLabel">${label}</span>
   			</span>
   		</c:otherwise>
   		
   </c:choose>

	
	
	


