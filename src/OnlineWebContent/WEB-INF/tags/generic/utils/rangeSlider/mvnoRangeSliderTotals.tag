<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.rangeSlider" tagdir="/WEB-INF/tags/generic/utils/rangeSlider"%>	

<%@ attribute name="currentPackageLabel" required="false" %>		
<%@ attribute name="currentPackageValue" required="false" %>
<%@ attribute name="recurringBundlesClassName" required="false" %>
<%@ attribute name="newPackageClassName" required="false" %>
<%@ attribute name="recurringBundlesLabel" required="false" %>
<%@ attribute name="recurringBundlesValue" required="false" %>
<%@ attribute name="newPackageLabel" required="false" %>
<%@ attribute name="newPackageValue" required="false" %> 
<%@ attribute name="maxValue" required="false" %>
<%@ attribute name="minValue" required="false" %>
<%@ attribute name="sliderType" required="true" %>


<c:if test="${empty maxValue }"><c:set var="maxValue" value="0" /></c:if>
<c:if test="${empty minValue }"><c:set var="minValue" value="0" /></c:if>

<c:if test="${ sliderType == 'banking' }"> 
<%-- set up default values if current package are empty --%>
 <c:choose>
 	<c:when test="${empty currentPackageLabel }">
 		<c:set var="currentPackageLabel" value="Current Package Total:" />
 	</c:when>
 	<c:otherwise>
 		<c:set var="currentPackageLabel" value="${currentPackageLabel }" />
 	</c:otherwise> 
 </c:choose>
 
  <c:choose>
 	<c:when test="${empty currentPackageValue }">
 		<c:set var="currentPackageValue" value="0" />
 	</c:when>
 	<c:otherwise>
 		<c:set var="currentPackageValue" value="${currentPackageValue }" />
 	</c:otherwise> 
 </c:choose>
 
<%-- set up default values if recurring bundles are empty --%>
	 <c:choose>
	 	<c:when test="${empty recurringBundlesLabel }">
	 		<c:set var="recurringBundlesLabel" value="Recurring Bundles Total:" />
	 	</c:when>
	 	<c:otherwise>
	 		<c:set var="recurringBundlesLabel" value="${recurringBundlesLabel }" />
	 	</c:otherwise> 
	 </c:choose>
	 
	  <c:choose>
	 	<c:when test="${empty recurringBundlesValue }">
	 		<c:set var="recurringBundlesValue" value="0" />
	 	</c:when>
	 	<c:otherwise>
	 		<c:set var="recurringBundlesValue" value="${recurringBundlesValue }" />
	 	</c:otherwise> 
	 </c:choose>
	 
	 <%-- set up default values if new package totals are empty --%>
	 <c:choose>
	 	<c:when test="${empty newPackageLabel }">
	 		<c:set var="newPackageLabel" value="New Package Total:" />
	 	</c:when>
	 	<c:otherwise>
	 		<c:set var="newPackageLabel" value="${newPackageLabel }" />
	 	</c:otherwise> 
	 </c:choose>
	 
	  <c:choose>
	 	<c:when test="${empty newPackageValue }">
	 		<c:set var="newPackageValue" value="0" />
	 	</c:when>
	 	<c:otherwise>
	 		<c:set var="newPackageValue" value="${newPackageValue }" />
	 	</c:otherwise> 
	 </c:choose>
	 
	 <%-- Current Package Total  --%>
			<fnb.generic.rangeSlider:mvnoCreateYourRowWrapper>
				
				<div class="mvnoCreateYourOwnTableCell totalPrice">
					<span class="packageTotalHeading">${currentPackageLabel }</span>
				</div>
				<div class="mvnoCreateYourOwnTableCell totalPriceValue">
					<%-- main product code for custom Contract  --%>
					<span class="packageValue">
						<span class="currany">R</span> <span id="currentPackageValue">${currentPackageValue }</span>
					</span>
				</div>
			</fnb.generic.rangeSlider:mvnoCreateYourRowWrapper>
			
			<%-- recurring Bundles Total  --%>
			<fnb.generic.rangeSlider:mvnoCreateYourRowWrapper className="${recurringBundlesClassName }">				
				<div class="mvnoCreateYourOwnTableCell totalPrice">
					<span class="packageTotalHeading">${recurringBundlesLabel }</span>
				</div>
				<div class="mvnoCreateYourOwnTableCell totalPriceValue">
					<%-- main product code for custom Contract  --%>
					<span class="packageValue">
						<span class="currany">R</span> <span id="recurringBundles">${recurringBundlesValue }</span>
					</span>
				</div>
			</fnb.generic.rangeSlider:mvnoCreateYourRowWrapper>
</c:if>

 <c:if test="${sliderType == 'sales' }">
 	<c:set var="newPackageLabel" value="Your package Total:" />
 	<c:set var="maxValue" value="-1" />
	<c:set var="minValue" value="-1" />
 </c:if>
 
			<%-- New Package Total equals recurring plus current value --%>	
			<fnb.generic.rangeSlider:mvnoCreateYourRowWrapper className="${newPackageClassName }">
				<div class="mvnoCreateYourOwnTableCell totalPrice">
					<span class="packageTotalHeading">${newPackageLabel }</span>
				</div>
				<div class="mvnoCreateYourOwnTableCell totalPriceValue">
					<%-- main product code for custom Contract  --%>
					<span class="packageValue"><span class="currany">R</span> <span
						id="newPackageTotal" data-maxValue="${maxValue }" data-minValue="${minValue }">${newPackageValue }</span></span>
				</div>
				<jsp:doBody />
			</fnb.generic.rangeSlider:mvnoCreateYourRowWrapper>
			