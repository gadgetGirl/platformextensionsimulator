<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.rangeSlider" tagdir="/WEB-INF/tags/generic/utils/rangeSlider"%>


<%@ attribute name="sliderId" required="false" %>
<%@ attribute name="sliderLabel" required="false" %>
<%@ attribute name="totalSnapPoints" required="false" %>
<%-- <%@ attribute name="snapPointsBean" required="false" type="java.util.ArrayList" %> --%>
<%@ attribute name="sliderName" required="false" %>
<%@ attribute name="snapPointSelected" required="false" %>
<%@ attribute name="sliderColor" required="false" %>
<%@ attribute name="sliderRate" required="false" %>
<%@ attribute name="youPayCurrancy" required="false" %>
<%@ attribute name="inputName" required="false" %>
<%@ attribute name="inputValue" required="false" %>
<%@ attribute name="currency" required="false" %>



			<%-- snapPointsBean="${snapPointsBean}"  --%>
	<fnb.generic.rangeSlider:mvnoRangeSlider sliderId="${sliderId }" sliderLabel="${sliderLabel }" 
											totalSnapPoints="${totalSnapPoints }" sliderType="banking" 
										 sliderName="${sliderName }" snapPointSelected="${snapPointSelected }" sliderColor="${sliderColor }" />


