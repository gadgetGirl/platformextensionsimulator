<%@ taglib prefix="c" 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"	uri="/WEB-INF/fmt.tld" %>
<%@ taglib prefix="fn" 	uri="/WEB-INF/fn.tld" %>
<%@ taglib prefix="pp" 	uri="/WEB-INF/PropertyProvider.tld" %>
<%@ taglib prefix="ui" 	tagdir="/WEB-INF/tags/chameleon" %>

<%@ taglib prefix="fnb.generic.ratesTable" tagdir="/WEB-INF/tags/generic/ratesTable"%>

<%@ attribute name="headings" required="true" rtexprvalue="true"%>
<%@ attribute name="widths" required="true" rtexprvalue="true"%>

<c:set var="headings" value="${headings}" scope="request" />
<c:set var="widths" value="${widths}" scope="request" />
				
	<div data-role="ratesTable">
		<div data-role="ratesTableRow">
	
			<c:set var="colWidth" />	
			<c:forTokens items="${headings}" delims="," var="heading" begin="0" end="0" >
				<c:forTokens items="${widths}" delims="," var="width" begin="0" end="0" ><c:set var="colWidth" value="${width}"/></c:forTokens>
				<fnb.generic.ratesTable:headerCell width="${colWidth}">${heading}</fnb.generic.ratesTable:headerCell>
			</c:forTokens>
			
			<c:choose>
				<c:when test="${fn:length(headings) > 2}">
					<fnb.generic.ratesTable:headerCellGroup width="${100 - colWidth}">
						<c:forTokens items="${headings}" delims="," var="heading" varStatus="stat" begin="1" >
							<c:set var="subColWidth" />	
							<c:forTokens items="${widths}" delims="," var="width" begin="${stat.index}" end="${stat.index}" ><c:set var="subColWidth" value="${width}"/></c:forTokens>
							<fnb.generic.ratesTable:headerCell width="${subColWidth}">${heading}</fnb.generic.ratesTable:headerCell>
						</c:forTokens>
					</fnb.generic.ratesTable:headerCellGroup>
				</c:when>
				<c:otherwise>
					<c:forTokens items="${headings}" delims="," var="heading" varStatus="stat" begin="1" >
						<c:set var="subColWidth" />	
						<c:forTokens items="${widths}" delims="," var="width" begin="${stat.index}" end="${stat.index}" ><c:set var="subColWidth" value="${width}"/></c:forTokens>
						<fnb.generic.ratesTable:cell width="${subColWidth}">${heading}</fnb.generic.ratesTable:cell>
					</c:forTokens>
				</c:otherwise>
			</c:choose>
		
		</div>
		<jsp:doBody />
	</div>
