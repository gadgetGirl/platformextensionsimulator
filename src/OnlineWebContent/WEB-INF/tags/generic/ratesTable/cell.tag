<%@ taglib prefix="c" 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"	uri="/WEB-INF/fmt.tld" %>
<%@ taglib prefix="fn" 	uri="/WEB-INF/fn.tld" %>
<%@ taglib prefix="pp" 	uri="/WEB-INF/PropertyProvider.tld" %>
<%@ taglib prefix="ui" 	tagdir="/WEB-INF/tags/chameleon" %>

<%@ taglib prefix="fnb.generic.ratesTable" tagdir="/WEB-INF/tags/generic/ratesTable"%>

<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="heading" required="false" rtexprvalue="true"%>
<%@ attribute name="width" required="true" rtexprvalue="true"%>
<%@ attribute name="onclick" required="false" rtexprvalue="true"%>

<div data-role="ratesTableCell" class="td${empty width?'100':width}${' '}${className}" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if> ><c:if test="${not empty heading}"><span>${heading}:</span></c:if> <jsp:doBody /></div>
