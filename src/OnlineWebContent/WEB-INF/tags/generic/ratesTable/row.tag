<%@ taglib prefix="c" 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"	uri="/WEB-INF/fmt.tld" %>
<%@ taglib prefix="fn" 	uri="/WEB-INF/fn.tld" %>
<%@ taglib prefix="pp" 	uri="/WEB-INF/PropertyProvider.tld" %>
<%@ taglib prefix="ui" 	tagdir="/WEB-INF/tags/chameleon" %>

<%@ taglib prefix="fnb.generic.ratesTable" tagdir="/WEB-INF/tags/generic/ratesTable"%>

<%@ attribute name="values" required="true" rtexprvalue="true"%>
				
	<div data-role="ratesTableRow">
	
		<c:set var="colWidth" />	
		<c:forTokens items="${values}" delims="," var="cellValue" begin="0" end="0" >
			<c:forTokens items="${widths}" delims="," var="width" begin="0" end="0" ><c:set var="colWidth" value="${width}"/></c:forTokens>
			<c:choose>
				<c:when test="${browserInfo.capabilities['fnb_is_mobile']}">
					<fnb.generic.ratesTable:cell width="${colWidth}" className="expander">${cellValue}</fnb.generic.ratesTable:cell>
				</c:when>
				<c:otherwise>
					<fnb.generic.ratesTable:cell width="${colWidth}">${cellValue}</fnb.generic.ratesTable:cell>
				</c:otherwise>
			</c:choose>
		</c:forTokens>
		
		<c:choose>
			<c:when test="${fn:length(values) > 2}">
				<fnb.generic.ratesTable:cellGroup width="${100 - colWidth}">
					<c:forTokens items="${values}" delims="," var="cellValue" varStatus="stat" begin="1" >
						<c:set var="subColWidth" />	
						<c:forTokens items="${widths}" delims="," var="width" begin="${stat.index}" end="${stat.index}" ><c:set var="subColWidth" value="${width}"/></c:forTokens>
						<c:set var="subColHeading" />	
						<c:forTokens items="${headings}" delims="," var="heading" begin="${stat.index}" end="${stat.index}" ><c:set var="subColHeading" value="${heading}"/></c:forTokens>
						<fnb.generic.ratesTable:cell width="${subColWidth}" heading="${subColHeading}">${cellValue}</fnb.generic.ratesTable:cell>
					</c:forTokens>
				</fnb.generic.ratesTable:cellGroup>
			</c:when>
			<c:otherwise>
				<c:forTokens items="${values}" delims="," var="cellValue" varStatus="stat" begin="1" >
					<c:set var="subColWidth" />	
					<c:forTokens items="${widths}" delims="," var="width" begin="${stat.index}" end="${stat.index}" ><c:set var="subColWidth" value="${width}"/></c:forTokens>
					<c:set var="subColHeading" />	
					<c:forTokens items="${headings}" delims="," var="heading" begin="${stat.index}" end="${stat.index}" ><c:set var="subColHeading" value="${heading}"/></c:forTokens>
					<fnb.generic.ratesTable:cell width="${subColWidth}" heading="${subColHeading}">${cellValue}</fnb.generic.ratesTable:cell>
				</c:forTokens>
			</c:otherwise>
		</c:choose>
		
	</div>
