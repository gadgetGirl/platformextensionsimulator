<%@ taglib prefix="c" 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"	uri="/WEB-INF/fmt.tld" %>
<%@ taglib prefix="fn" 	uri="/WEB-INF/fn.tld" %>
<%@ taglib prefix="pp" 	uri="/WEB-INF/PropertyProvider.tld" %>
<%@ taglib prefix="ui" 	tagdir="/WEB-INF/tags/chameleon" %>

<%@ taglib prefix="fnb.generic.ratesTable" tagdir="/WEB-INF/tags/generic/ratesTable"%>

<%@ attribute name="width" required="true" rtexprvalue="true"%>

<div data-role="ratesTableHeaderCell" class="td${width}"><jsp:doBody /></div>
