<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="false" name="actionMenu" description="Side menu items(Action menu)"%>
<%@ attribute required="false" name="contextPath" description="Context Path"%>
<%@ attribute required="false" name="systemContextPath" description="Context Path"%>
<%@ attribute required="false" name="pageTitle" description="Page Title"%>
<%@ attribute required="false" name="initialCssOveride" description=""%>
<%@ attribute required="false" name="systemIncludes" description="Web,Sales and Banking"%>
<%@ attribute required="false" name="includesVersion" description="Version for script includes"%>

<fnb.generic.frame:coreDocument contextPath="${contextPath}" includesVersion="${includesVersion}">
	
	<head>

		<title>${(empty request_pageTitle) ? (empty pageTitle) ? '' : pageTitle : request_pageTitle}</title>
		
		<fnb.generic.frame:coreMetaTags>
			${not empty systemMetaTags?systemMetaTags:''}
		</fnb.generic.frame:coreMetaTags>
		
	 	<fnb.generic.frame:assetIncludes includesVersion="${includesVersion}" systemIncludes="${systemIncludes}" initialCssOveride="${initialCssOveride}" systemContextPath="${systemContextPath}" contextPath="${contextPath}" systemJs="${systemJs}" systemCss="${systemCss}"/>
		
		<c:if test="${not empty session_ga_account}">
			<fnb.generic.frame:googleAnalytics />
		</c:if>
		
	</head>
	
	<fnb.generic.frame:coreBody actionMenu="${actionMenu}" mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}" >
			<jsp:doBody />
	</fnb.generic.frame:coreBody>
	
</fnb.generic.frame:coreDocument>


