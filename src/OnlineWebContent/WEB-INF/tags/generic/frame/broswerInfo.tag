<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>

<fnb.generic.markup:ul>
	<fnb.generic.markup:li><b>Operating System: </b>${browserInfo.os.name}<br /></fnb.generic.markup:li>
	<fnb.generic.markup:li><b>Cookies Enabled: </b>${browserInfo.capabilities.cookie_support?'Yes':'No'}<br /></fnb.generic.markup:li>
	<fnb.generic.markup:li><b>Screen Resolution : </b>???<br /></fnb.generic.markup:li>
	<fnb.generic.markup:li><b>Browser Internal Name: </b>${browserInfo.agent.name}<br /></fnb.generic.markup:li>
	<fnb.generic.markup:li><b>Browser Version: </b>${browserInfo.agent.version}<br /></fnb.generic.markup:li>
	<fnb.generic.markup:li><b>Browser UserAgent: </b>???<br /></fnb.generic.markup:li>
	<fnb.generic.markup:li><b>Java enabled: </b>???<br /></fnb.generic.markup:li>
</fnb.generic.markup:ul>

<%--
	<hr />
	<b>browserInfo.os: </b><fnb.generic.markup:p>
		<c:forTokens items="${browserInfo.os}" var="item" varStatus="stat1" delims=",">
			${item}<br />
		</c:forTokens></fnb.generic.markup:p>
	<hr />
	<b>browserInfo.osVersion: </b><fnb.generic.markup:p>
		<c:forTokens items="${browserInfo.osVersion}" var="item" varStatus="stat1" delims=",">
			${item}<br />
		</c:forTokens></fnb.generic.markup:p>
	<hr />
	<b>browserInfo.agent: </b><fnb.generic.markup:p>
		<c:forTokens items="${browserInfo.agent}" var="item" varStatus="stat1" delims=",">
			${item}<br />
		</c:forTokens></fnb.generic.markup:p>
	<hr />
	<b>browserInfo.capabilities: </b><fnb.generic.markup:p>
		<c:forTokens items="${browserInfo.capabilities}" var="item" varStatus="stat1" delims=",">
			${item}<br />
		</c:forTokens></fnb.generic.markup:p>
--%>