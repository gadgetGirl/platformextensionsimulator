<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ attribute required="false" name="event"%>
 <%@ attribute required="false" name="url"%>
 <%@ attribute required="true" name="label"%>
 <%@ attribute required="false" name="target"%>
 <%@ attribute required="false" name="targetElement"%>
 <%@ attribute required="false" name="dataTarget"%>
 <%@ attribute required="false" name="disabled"%>
 <c:choose>
 	<c:when test="${not empty url}">
 footerData['templateElements'].push(
			    {   templateName : 'footerContactInfo',
					templateData : {
						event : '${not empty event ? event : ''}',
						url : '${not empty url ? url: ''}',
						target : '${not empty target ? target: ''}',
						targetElement : '${not empty targetElement ? targetElement: ''}',
						dataTarget : '${not empty dataTarget ? dataTarget: null}',
						label : '${not empty label ? label : ''}',
						id : 'footerBtt_'+footerData['templateElements'].length,
						clearHtmlTemplates : false,
						clearPageModuleObject : false,
						clearPageEventsArray : false,
						clearPageTemplatesArray : true
	}
});   
	</c:when>
	<c:otherwise>
 footerData['templateElements'].push(
			    {   templateName : 'footerContactInfoSimple',
					templateData : {
						label : '${not empty label ? label : ''}',
						clearHtmlTemplates : false,
						clearPageModuleObject : false,
						clearPageEventsArray : false,
						clearPageTemplatesArray : true
	}
});   
	</c:otherwise>
</c:choose>