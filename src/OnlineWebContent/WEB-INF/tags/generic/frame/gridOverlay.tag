<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${session_gridButton eq 'true'}">
	<div id="theGridOverlay" class="theGrigWidth">
		<div id="theGridOverlayRows" data-visible="false" class="Hhide">
			<c:forEach var="i" varStatus="status1" begin="1" end="200">
				<div class="theGridOverlayRow"><span class="theGridRowCount">${status1.count}</span></div>
			</c:forEach>
		</div>
		<div id="theGridOverlayCols" data-visible="false" class="Hhide">
			<c:forEach var="j" varStatus="status2" begin="1" end="10">
				<div class="theGridOverlayCol ${status2.last?'last':''}"><span class="theGridColCount">${status2.count}</span>
					<c:forEach var="k" varStatus="status3" begin="1" end="11">
						<c:set var="pos">
							<c:choose>
								<c:when test="${status3.count eq 1}">first</c:when>
								<c:when test="${status3.count eq 2}">two</c:when>
								<c:when test="${status3.count eq 3}">three</c:when>
								<c:when test="${status3.count eq 4}">four</c:when>
								<c:when test="${status3.count eq 5}">five</c:when>
								<c:when test="${status3.count eq 6}">six</c:when>
								<c:when test="${status3.count eq 7}">seven</c:when>
								<c:when test="${status3.count eq 8}">eight</c:when>
								<c:when test="${status3.count eq 9}">nine</c:when>
								<c:when test="${status3.count eq 10}">ten</c:when>
								<c:when test="${status3.count eq 11}">last</c:when>
							</c:choose>
						</c:set>
						<div class="theGridOverlaySubCol ${pos}"><br /><span class="theGridSubColCount"></span></div>
					</c:forEach>
				</div>
			</c:forEach>
		</div>
		<div id="theGridToggleButton" data-role="theGridToggle">${browserInfo.capabilities['fnb_is_mobile']?'G':'Show/Hide the Grid'}</div>
	</div>
</c:if>