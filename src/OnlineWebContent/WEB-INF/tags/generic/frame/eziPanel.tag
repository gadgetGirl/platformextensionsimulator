<div id="eziOverlay" class="overlayContainer index30 bodyWidth Hhide">
		<div class="overlayInner">
			<div class="gridGroupInner">
				<div data-role="gridColEzi" data-type="eziWrapperLeft"  class="gridColEzi eziWrapperLeft borderRight">&nbsp;</div>
				<div data-role="gridColEzi" data-type="eziWrapperRight" class="gridColEzi eziWrapperRight borderRight borderLeft">
					<div class="eziLoader" class="Hhide">
						<div class="eziLoaderInner">
							<div class="eziLoaderProgress">
							</div>
							<span class="eziLoaderText">Loading</span>
						</div>
					</div>
					<div class="eziInner">
						<div id="eziPageContent" class="eziInnerTwo">
						<jsp:doBody/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>