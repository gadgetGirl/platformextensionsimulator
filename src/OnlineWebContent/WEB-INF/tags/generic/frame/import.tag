<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="true" name="type" description="TYPES: js, css"%>
<%@ attribute required="true" name="link" %>
<%@ attribute required="false" name="coreAsset" %>
<%@ attribute required="false" name="ie8" %>
<%@ attribute required="false" name="mobile" %>
<%@ attribute required="false" name="data" %>
<%@ attribute required="false" name="version" %>

<c:if test="${empty version}">
	<c:set var="version" value="2.0"></c:set>
</c:if>

<c:set var="className">
<c:if test="${coreAsset ne 'true'}">
    <c:choose>
        <c:when test="${type eq 'css'}">class="pageCSS"</c:when>
        <c:when test="${type eq 'js'}">class="pageScript"</c:when>
    </c:choose>
</c:if>
</c:set>

<c:choose>
<c:when test="${type eq 'css'}">
    <c:choose>
        <c:when test="${ie8 ne 'true'}">
        
            <c:choose>
                <c:when test='${mobile eq "true" and (browserInfo.capabilities["fnb_is_mobile"])}'>
                    <link ${className} type="text/css" rel="stylesheet" href="${link}?v=${version}"/>
                </c:when>
                <c:when test='${mobile ne "true"}'>
                    <link ${className} type="text/css" rel="stylesheet" href="${link}?v=${version}"/>
                </c:when>
            </c:choose>
        </c:when>
        <c:when test="${ie8 == true and fullUserBrowser eq 'IE8'}">
            <link ${className} type="text/css" rel="stylesheet" href="${link}?v=${version}"/>
        </c:when>
    </c:choose>
</c:when>
<c:when test="${type eq 'js'}"><script ${className} type="text/javascript" src="${link}?v=${version}" data-jsp='${data}'></script></c:when>
<c:when test="${type eq 'print'}">
    <link rel="stylesheet" type="text/css" media="print" href="${link}?v=${version}"/>
</c:when>
<c:when test="${type eq 'image/x-icon'}">
    <link rel="icon" type="image/x-icon" href="${link}"/>
</c:when>
</c:choose>