<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="fullWidth"%>
<%@ attribute required="false" name="contextPath" description="Context Path"%>
<%@ attribute required="false" name="includesVersion" description="Version for script includes"%>

<c:choose>
	 <c:when test='${browserInfo.capabilities["fnb_is_mobile"]}'>
	 	<c:set var="browserType" value="mobileBrowser"/>
	 </c:when>
	 <c:otherwise>
	 	<c:set var="browserType" value="desktopBrowser"/>
	 </c:otherwise>
</c:choose>
<c:set var="browserName">
	<c:choose>
		<c:when test="${not empty browserInfo.agent.version}">
				${browserInfo.agent.name}${browserInfo.agent.version}
			</c:when>
		<c:otherwise>
				${browserInfo.agent.name}
			</c:otherwise>
	</c:choose>
</c:set>
<!DOCTYPE html>

<html class="${fullWidth eq 'true'?'':'bodyWidth'} ${not empty browserType?browserType:''} ${not empty browserName?fn:replace(browserName,':', ''):''} ${not empty className?className:''}">

<jsp:doBody/>

<c:set var="context" value="${(contextPath!='') ? contextPath : session_contextPath}" />

<fnb.generic.frame:import version="${includesVersion}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/init.js" />

</html>