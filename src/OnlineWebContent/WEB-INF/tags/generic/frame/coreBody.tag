<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="ie8" tagdir="/WEB-INF/tags/generic/widgets/ie8"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="false" name="extraContent"%>
<%@ attribute required="false" name="actionMenu"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${not empty actionMenu}">
	<c:set var="hasActionMenu">
		class="hasActionMenu"
	</c:set>
</c:if>

<c:set var="extraContent">
	<ie8:noIE8Login layout="banking" skin="${skin}"/>
	<ie8:noIE8PopUp skin="${skin}"/>
</c:set>

<body id="bodyContainer" ${hasActionMenu}>

	<fnb.generic.frame:header mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}"/>

	<fnb.generic.frame:gridOverlay />
	
	<fnb.generic.frame:actionMenuButton/>
	
	<section id="pageContentWrapper" class="content">
		
		<div id="pageContent" class="contentInner">

			<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
		
			<div class="pageWrapper">
			
				<fnb.generic.frame:extraContent extraContent="${extraContent}"/>
			
				<jsp:doBody />
				
			</div>
			
		</div>
		
	</section> 
	
	<fnb.generic.frame:errorPanel /> 
	
	<fnb.generic.frame:eziPanel />
	
	<fnb.generic.frame:loader />
	
	<fnb.generic.frame:datePicker/>
	
	<fnb.generic.frame:notifications />
	
	<fnb.generic.frame:timeOut />

	<fnb.generic.frame:footer />
	
</body>
