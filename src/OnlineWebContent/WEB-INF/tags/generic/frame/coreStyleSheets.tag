<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ attribute required="false" name="contextPath" description="Context Path"%>
<%@ attribute required="false" name="includeMode" description="Include Mode"%>
<%@ attribute required="false" name="webSkins" description="Include Web skins Mode"%>
<%@ attribute required="false" name="skin" description="Include Web skins Mode"%>
<%@ attribute required="false" name="systemIncludes" description="Web,Sales and Banking"%>
<%@ attribute required="false" name="initialCssOveride" description="Web,Sales and Banking"%>
<%@ attribute required="false" name="version" description="File Version"%>

<c:set var="context" value="${(contextPath!='') ? contextPath : session_contextPath}" />
<%-- NB: Never commit the below to variables... they are there for local testing only...--%>
<%-- <c:set var="mode">minified</c:set> --%>
<%-- <c:set var="skin">12</c:set> --%>
<c:set var="defaultSkinStandard">
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/reset/reset.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/initials/frame.css" />
	${initialCssOveride}

	<c:if test="${systemIncludes == 'web'}">
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/initials/content.css" />
	</c:if>
	
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/initials/core.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/initials/grid.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/initials/markup.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/frame/actionMenu.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/frame/footer.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/frame/header.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/frame/notificationsContainer.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/markup/table.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/forms/forms.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/utils/menu.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/utils/datePicker.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/utils/timeoutContainer.css" />
	
	<c:if test="${systemIncludes == 'web'}">
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/utils/pageMenu.css" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/utils/subMenu.css" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/widgets/apply.css" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/widgets/contactInfo.css" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/widgets/whatYouNeed.css" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/widgets/banners.css" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/widgets/bannerRotator.css" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/widgets/slider.css" />
	</c:if>
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/sales/css/instantSales.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/final/final.css" />
</c:set>
<c:set var="defaultSkinCombined">
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/reset/reset.css" />
	${initialCssOveride}
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/initials/initials_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/frame/frame_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/forms/forms_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/markup/markup_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/utils/utils_combined.css" />
	<c:if test="${systemIncludes == 'web'}">
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/widgets/widgets_combined.css" />
	</c:if>
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/sales/css/instantSales.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/final/final_combined.css" />
</c:set>
<c:set var="defaultSkinMinified">
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/reset/reset.css" />
	${initialCssOveride}
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/initials/initials_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/frame/frame_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/forms/forms_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/markup/markup_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/utils/utils_combined.css" />
	<c:if test="${systemIncludes == 'web'}">
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/widgets/widgets_combined.css" />
	</c:if>
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/sales/css/instantSales.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/00/css/final/final_combined.css" />
</c:set>
<c:set var="rmbWebSkinStandard">
	<%-- RMB --%>
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/initials/frame.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/initials/core.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/initials/grid.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/frame/header.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/frame/footer.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/frame/sideMenu.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/frame/notificationsContainer.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/markup/markup.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/markup/table.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/forms/forms.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/widgets/banners.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/widgets/bannerRotator.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/widgets/slider.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/widgets/relatedPages.css" />
</c:set>
<c:set var="rmbWebSkinCombined">
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/initials/initials_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/frame/frame_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/forms/forms_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/markup/markup_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/utils/utils_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/widgets/widgets_combined.css" />
</c:set>
<c:set var="rmbWebSkinMinified">
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/initials/initials_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/frame/frame_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/forms/forms_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/markup/markup_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/utils/utils_combined.css" />
	<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/widgets/widgets_combined.css" />
</c:set>
<c:set var="includedCSS">
	<c:choose>
		<c:when test="${includeMode eq 'standard'}">
			${defaultSkinStandard}
		</c:when>
		<c:when test="${includeMode eq 'combined'}">
			${defaultSkinCombined}	
		</c:when>
		<c:otherwise>
			${defaultSkinMinified}
		</c:otherwise>
	</c:choose>
</c:set>
<c:choose>
	<c:when test="${skin eq 1}">
		<%-- RMB --%>
		<c:choose>
			<c:when test="${webSkins == 'true'}">
			${rmbWebSkinStandard}
			</c:when>
			<c:otherwise>
			${includedCSS}
			<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/01/css/skin.css" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${skin eq 2}">
		<%-- DISCOVERY --%>
		${includedCSS}
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/02/css/skin.css" />
	</c:when>
	<c:when test="${skin eq 6}">
		<%-- KULULA --%>
		${includedCSS}
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/06/css/skin.css" />
	</c:when>
	<c:when test="${skin eq 11}">
		<%-- CLICKS --%>
		${includedCSS}
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/11/css/skin.css" />
	</c:when>
	<c:when test="${skin eq 12}">
		<%-- FRB --%>
		${includedCSS}
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/12/css/skin.css" />
	</c:when>
	<c:when test="${skin eq 13}">
		<%-- RAS --%>
		${includedCSS}
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="css" link="${context}/00Assets/skins/13/css/skin.css" />
	</c:when>
	<c:otherwise>
	  ${includedCSS}
	</c:otherwise>
</c:choose>
<jsp:doBody />