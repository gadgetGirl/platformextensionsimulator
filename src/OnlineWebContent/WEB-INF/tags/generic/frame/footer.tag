
<%@ attribute name="tref" required="false" rtexprvalue="true" type="java.lang.String"%>

<footer id="footerTag" class="bodyWidth index60" >
	<div id="footerCornerImg"></div>
	<div id="support-reference" class="support-reference">${tref}</div>
	<div id="footerContainer" class="w100 frameOffset">
		<div id="eziFooterButtonGroup"  data-visible="false" class="Hhide">
			<div id="eziFooterButtonsContainer" >
			
			</div>
		</div>
		<div id="footerButtonGroup" class="Hhide">
			<div id="footerButtonsContainer">
			
			</div>
		</div>
	</div>
</footer>