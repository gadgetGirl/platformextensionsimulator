<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="dataUrl"%>
<%@ attribute required="false" name="last"%>
<%@ attribute required="false" name="selected"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="contextPath"%>

<c:if test="${not empty dataUrl}">
	<c:choose>
		<c:when test="${not empty contextPath}">
			<c:set var="url" value="${dataUrl}"/>
		</c:when>
		<c:otherwise>
			<c:set var="url" value="${session_contextPath}${dataUrl}"/>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${not empty selected}"><c:set var="dataSelected" value="data-selected='${selected}'"/></c:if>
<c:if test="${not empty className}"><c:set var="class" value="${className}"/></c:if>

<c:choose>
	<c:when test="${not empty url}">
		<span class="mainTab ${className}" data-submenu="false" data-url="${url}" ${not empty last?'data-position="last"':''} ${not empty dataSelected?dataSelected:''}><span><jsp:doBody /></span></span>
	</c:when>
	<c:otherwise>
		<span id="${id}" class="mainTab ${className}" data-submenu="true" ${not empty last?'data-position="last"':''} ${not empty dataSelected?dataSelected:''}><span><jsp:doBody /></span></span>
	</c:otherwise>
</c:choose>
