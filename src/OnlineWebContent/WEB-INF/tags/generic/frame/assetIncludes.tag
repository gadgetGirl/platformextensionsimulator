<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%--Developer: Donovan --%>
<%--Import frame tags--%>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%--Tag attributes--%>
<%@ attribute required="false" name="contextPath" description="Context Path"%>
<%@ attribute required="false" name="systemContextPath" description="Context Path"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="webSkins" description="Set this to true to use webSkins"%>
<%@ attribute required="false" name="skin" description="Set the skin manually for web"%>
<%@ attribute required="false" name="initialCssOveride" description="Set the skin manually for web"%>
<%@ attribute required="false" name="systemIncludes" description="Web,Sales and Banking"%>
<%@ attribute required="false" name="includesVersion" description="Version for script includes"%>

<%--Set main hashmaps--%>
<jsp:useBean id="assetIncludes" class="java.util.HashMap"/>
<jsp:useBean id="browserTests" class="java.util.HashMap"/>
<jsp:useBean id="browserWebSkins" class="java.util.HashMap"/>

<%--List of tests--%>
<jsp:useBean id="ie6" class="java.util.HashMap"/>
<jsp:useBean id="ie7" class="java.util.HashMap"/>
<jsp:useBean id="ie8" class="java.util.HashMap"/>
<jsp:useBean id="ie9" class="java.util.HashMap"/>
<jsp:useBean id="ie10" class="java.util.HashMap"/>
<jsp:useBean id="ie" class="java.util.HashMap"/>

<%--Variable to decide if we include web or banking extras for IE--%>
<c:choose>
	<c:when test="${empty systemContextPath}">
		<c:set var="systemContextPath" value="web/" />
	</c:when>
	<c:otherwise>
	<c:set var="systemContextPath" value="${systemContextPath}/" />
	</c:otherwise>
</c:choose>
<%--
FrontEnd Script Includes
--%>
<c:set target="${assetIncludes}" property="scriptAssets" value="/00Assets/generic/genericJs/browsers/${systemContextPath}"/>
<c:set target="${assetIncludes}" property="cssAssets" value="/00Assets/skins/00/css/browsers/${systemContextPath}"/>
<c:set target="${assetIncludes}" property="webSkinPath" value="/00Assets/skins/"/>

<c:set target="${browserWebSkins}" property="1" value="01"/>
<c:set target="${browserWebSkins}" property="2" value="02"/>
<c:set target="${browserWebSkins}" property="6" value="06"/>
<c:set target="${browserWebSkins}" property="11" value="11"/>
<c:set target="${browserWebSkins}" property="13" value="13"/>
<c:set target="${browserWebSkins}" property="16" value="16"/>

<%-- Do extra files for ie8--%>
<c:set target="${ie8}" property="style" value="true"/>
<c:set target="${ie8}" property="script" value="true"/>
<c:set target="${ie8}" property="extraScripts" value="/00Assets/generic/genericJs/libs/html5shiv.js"/>
<c:set target="${ie8}" property="class" value="false"/>
<c:set target="${ie8}" property="version" value="8"/>
<c:set target="${ie8}" property="skins" value="${browserWebSkins}"/>

<c:set target="${browserTests}" property="IE${ie8.version}" value="${ie8}"/>

<%-- Do extra files for ie9--%>
<c:set target="${ie9}" property="script" value="true"/>
<c:set target="${ie9}" property="version" value="9"/>
<c:set target="${browserTests}" property="IE${ie9.version}" value="${ie9}"/>

<%-- Do extra files for ie10--%>
<c:set target="${ie10}" property="script" value="true"/>
<c:set target="${ie10}" property="version" value="10"/>
<c:set target="${browserTests}" property="IE${ie10.version}" value="${ie10}"/>

<%-- Do extra files for ie--%>
<c:set target="${ie}" property="style" value="false"/>
<c:set target="${ie}" property="script" value="true"/>
<c:set target="${ie}" property="class" value="false"/>
<c:set target="${browserTests}" property="IE" value="${ie}"/>

<%-- Set test --%>
<c:set target="${assetIncludes}" property="tests" value="${browserTests}"/>

<%-- Set test --%>
<c:set var="agentName" value="${browserInfo.agent.name}"/>

<%-- Setup file version --%>
<c:if test="${empty includesVersion}">
	<c:set var="includesVersion" value="2.0"></c:set>
</c:if>

<%-- Setup include mode --%>
<c:set var="mode">
	
	<c:choose>
		<%-- Do MSIE modes --%>
		<c:when test="${agentName=='IE'}">
			<c:choose>
				<c:when test="${fn:contains(header.host, '.i.')}">combined</c:when>
				<c:otherwise>minified</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${fn:contains(header.host, '.i.')}">combined</c:when>
				<c:when test="${fn:contains(header.host, 'localhost')}">standard</c:when>
				<c:otherwise>minified</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>

</c:set>

<%-- Import Core stylesheets--%>
<fnb.generic.frame:coreStyleSheets version="${includesVersion}" initialCssOveride="${initialCssOveride}" skin="${skin}" webSkins="${webSkins}" contextPath="${contextPath}" includeMode="${mode}">
		${not empty systemCss?systemCss:''}
</fnb.generic.frame:coreStyleSheets>

<%-- Loop tests and include stylesheets--%>
<c:forEach var="browserStyle" items="${assetIncludes.tests}">
	
	<%-- Create user browser string --%>
	<c:set var="fullUserBrowser" scope="session">
		<c:choose>
			<c:when test="${not empty browserStyle.value.version}">
				${agentName}${browserInfo.agent.version}
			</c:when>
			<c:otherwise>
				${agentName}
			</c:otherwise>
		</c:choose>
	</c:set>
			
	<%-- Check browser match for stylesheets--%>
	<c:if test="${browserStyle.key==fullUserBrowser}">
		
		<%-- Check if styles need to be included --%>
		<c:if test="${not empty browserStyle.value.style}">
			<c:if test="${browserStyle.value.style=='true'}">
				<link type="text/css" rel="stylesheet" href="${session_contextPath}${assetIncludes.cssAssets}${fullUserBrowser}.css?v=${includesVersion}"/>
								
			</c:if>
		</c:if>
		<c:forEach var="browserSkin" items="${browserStyle.value.skins}">
			<c:if test="${browserSkin.key==skin}">
				<link type="text/css" rel="stylesheet" href="${session_contextPath}${assetIncludes.webSkinPath}${browserSkin.value}/css/browsers/${fullUserBrowser}.css?v=${includesVersion}"/>
			</c:if>
		</c:forEach>
		
	</c:if>

</c:forEach>
	
<%-- Import Core javascript--%>
<fnb.generic.frame:coreJavascript version="${includesVersion}" contextPath="${contextPath}" includeMode="${mode}">
		${not empty systemJs?systemJs:''}
</fnb.generic.frame:coreJavascript>

<%-- Loop tests and include stylesheets--%>
<c:forEach var="browserScripts" items="${assetIncludes.tests}">
		
	<%-- Create user browser string --%>
	<c:set var="fullUserBrowser">
		<c:choose>
			<c:when test="${not empty browserScripts.value.version}">
				${agentName}${browserInfo.agent.version}
			</c:when>
			<c:otherwise>
				${agentName}
			</c:otherwise>
		</c:choose>
	</c:set>
	
	<%-- Check browser match for javascript--%>
	<c:if test="${browserScripts.key==fullUserBrowser}">
			
		<%-- Check if scripts need to be included --%>
		<c:if test="${not empty browserScripts.value.script}">
			<c:if test="${browserScripts.value.script=='true'}">
				<script type="text/javascript" src="${session_contextPath}${assetIncludes.scriptAssets}${fullUserBrowser}.js?v=${includesVersion}"></script>
			</c:if>
		</c:if>
		
		<%-- Check if extraScripts need to be included --%>
		<c:if test="${not empty browserScripts.value.extraScripts}">
			<script type="text/javascript" src="${session_contextPath}${browserScripts.value.extraScripts}?v=${includesVersion}"></script>
		</c:if>
		
		<%-- Check if a class needs to be included --%>
		<c:if test="${not empty browserScripts.value.class}">
			<c:if test="${browserScripts.value.class=='true'}">					
				<script type="text/javascript">
					fnb.hyperion.controller.document.documentElement.className+=' '+"${fullUserBrowser}";
				</script>
			</c:if>
		</c:if>
		
	</c:if>

</c:forEach>