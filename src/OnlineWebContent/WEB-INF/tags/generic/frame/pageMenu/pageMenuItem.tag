<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="dataUrl"%>
<%@ attribute required="true" name="dataLevel"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="last"%>
<%@ attribute required="false" name="selected"%>
<c:if test="${not empty dataUrl}"><c:set var="href" value="href='${session_contextPath}${dataUrl}'"/></c:if>
<c:if test="${not empty selected}"><c:set var="dataSelected" value="data-selected='${selected}'"/></c:if>

<c:set var="body"><jsp:doBody /></c:set>
<c:choose>
	<c:when test="${dataLevel eq 1 and (empty href)}">
		<div data-role="pageMenuHeading" data-level="1" ${not empty last?'data-position="last"':''} ${not empty dataSelected?dataSelected:''}>
			<div>${label}</div>
		</div>
	</c:when>
	<c:when test="${dataLevel eq 2 and (empty href)}">
		<div data-role="pageMenuHeading2" data-level="2" ${not empty last?'data-position="last"':''} data-state="inactive" ${not empty dataSelected?dataSelected:''}>
			<div>${label}</div>
		${body}
		</div>
	</c:when>
	<c:otherwise>
		<a data-role="pageMenuItem" data-level="${dataLevel}" ${href} ${not empty last?'data-position="last"':''} ${not empty dataSelected?dataSelected:''}><span>${label}</span></a>
	</c:otherwise>
</c:choose>
