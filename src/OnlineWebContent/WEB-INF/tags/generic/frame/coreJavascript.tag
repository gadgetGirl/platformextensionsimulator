<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

<%@ attribute required="false" name="contextPath" description="Context Path"%>
<%@ attribute required="false" name="includeMode" description="Include Mode"%>
<%@ attribute required="false" name="version" description="File Version"%>

<c:set var="context" value="${(contextPath!='') ? contextPath : session_contextPath}" />

<script type="text/javascript">
var fnbIsMobile = '${browserInfo.capabilities["fnb_is_mobile"]}';
var fnbContextPath = '${context}';
</script>

<!-- Scripts -->

<c:choose>
	<c:when test="${includeMode eq 'standard'}">
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/initials/namespace.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/initials/selector.js" />
		
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/base/actions.js"/>
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/base/controller.js"/>
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/base/events.js"/>
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/base/page.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/base/sweeper.js"/>
		
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/delegate/delegate.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/assetLoader/assetLoader.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/animate/animate.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/horizontalScroller/horizontalScroller.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/isReady/isReady.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/serialize/serialize.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/debounce/debounce.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/cookies/cookies.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/audit/audit.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/extentions/ajax/ajax.js" />
		
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/mappings/mappings.js" />
		
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/functions/functions.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/functions/scrollTo/scrollTo.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/functions/supportPositionFixed/supportPositionFixed.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/functions/callback/callback.js" />
		
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/forms.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/input/input.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/input/mask/mask.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/input/number/number.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/textArea/textArea.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/dropdown/dropdown.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/checkbox/checkbox.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/datePicker/datePicker.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/button/button.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/radioButton/radioButton.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/forms/rangeSlider/rangeSlider.js" />
		
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/utils.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/actionMenu/actionMenu.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/ajax/filter/filter.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/ajax/content/content.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/ajax/parameters/parameters.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/comparisonGrid/comparisonGrid.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/designGrid/designGrid.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/error/error.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/expandables/expandables.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/eziPanel/eziPanel.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/footer/footer.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/hyperlinks/hyperlinks.js" />
		
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/notifications/notifications.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/overlay/overlay.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/progressBar/progressBar.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/templates/templates.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/topTabs/topTabs.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/widgets/bannerManager.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/widgets/contentSlider.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/utils/widgets/widgets.js" />
		
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/templates/eventTemplates.js" />
		<fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/templates/htmlTemplates.js" />
	</c:when>
	<c:when test="${includeMode eq 'combined'}">
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/combined/initials.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/combined/base.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/combined/mappings.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/combined/utils.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/combined/functions.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/combined/forms.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/combined/templates.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/combined/extentions.js"/>
	</c:when>
	<c:otherwise>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/minified/initials.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/minified/base.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/minified/mappings.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/minified/utils.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/minified/functions.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/minified/forms.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/minified/templates.js"/>
	   <fnb.generic.frame:import version="${version}" coreAsset="true" type="js" link="${context}/00Assets/generic/genericJs/minified/extentions.js"/>
	</c:otherwise>
</c:choose>

<jsp:doBody/>

