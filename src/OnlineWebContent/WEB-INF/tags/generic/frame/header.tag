<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld" %>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>

<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<c:if test="${not empty selectedTopTab}">
 <c:set var="selectedTopTab">
 	data-selectedTab="${selectedTobTab}"
 </c:set>
</c:if>


	<header id="header" class="bodyWidth index50">
		
		<c:if test="${not empty mainMenu}">
		
			<div id="headerCornerImg"></div>
			<div id="headerInner" class="frameOffset">
	
				<iframe id="homeLogin" name="homeLogin" height="0" src="/empty.html" width="0" frameborder="0"></iframe>
					
				<div id="headerButtons" class="clearfix">
				
					<div id="headerButtonsInner">
						<fnb.generic.markup:a href="${session_contextPath}/search/index.html" id="searchButton">Search</fnb.generic.markup:a>
						<div id="SecurityButtons">
							<fnb.generic.markup:a href="${session_contextPath}/security-centre/webroot-secureAnywhere.html" id="WebrootButton">Webroot</fnb.generic.markup:a>
		
							<span id="VerifyButton" onclick="fnb.hyperion.utils.login.submitFormFromLink('verifyremittance.VerifyRemittanceAdvise','LOGIN_FORM');" >&nbsp;&bull; Verify payments</span>
		
							<span id="ResetButton" onclick="fnb.hyperion.utils.login.submitFormFromLink('loginproblems.FnbLoginProblems','LOGIN_FORM');" >&nbsp;&bull; Forgot  password or username?</span>
						</div>
		
						<span id="RegisterButton" onclick="fnb.hyperion.utils.login.submitFormFromLink('enrollment.navigator.RegistrationSummary','LOGIN_FORM');">Register</span>
						
						<div id="login">
							
							<c:set var="loginButtonType" value="popupLoginButton"/>
							
							<c:if test="${!browserInfo.capabilities['fnb_is_mobile']}">
							
								<c:set var="loginButtonType" value="loginButton"/>
								
								<form autocomplete="off" method="post" id="LOGIN_FORM" name="LOGIN_FORM" action="${session_bankingUrl}/login/Controller" target="homeLogin">
		
									<input type="hidden" name="browserName" id="browserType" value="${browserInfo.agent.name}">
		
									<input type="hidden" name="browserVersion" id="browserVersion" value="${browserInfo.agent.version}">
		
									<input type="hidden" id="country" name="country" value="${session_country}">
		
									<input type="hidden" name="formname" id="formname" value="LOGIN_FORM"> 
		
									<input type="hidden" name="homePageLogin" id="homePageLogin" value="true">
		
									<input type="hidden" id="nav" name="nav" value="navigator.UserLogon">
		
									<input type="hidden" name="LoginButton" id="LoginButton" value="Login">
									
									<input type="hidden" name="bankingUrl" id="bankingUrl" value="${session_bankingUrl}"/>
									
									<input type="hidden" name="action" id="formAction" value="login"/>
									
									<input type="hidden" name="simple" id="simple" value="false" disabled="true"/>
									
									<fnb.generic.forms:input.text inlineError="Please complete the User ID field." id="Username" placeholder="Username"/>
		
									<fnb.generic.forms:input.password inlineError="Please complete the Password field." id="Password" placeholder="Password"/>
									
									<input style="display: none;" type="submit" value="submit">
																
								</form>
								
							</c:if>
							
							<fnb.generic.forms:button role="" label="Login" id="${loginButtonType}"/>
							
						</div>
		
					</div>
				</div>
	
				
				<div id="menuWrapper" class="clearfix">
					<nav data-role="topTabs" id="topTabs" data-disbaled="true" class="clearfix" ${selectedTobTab}>
						${mainMenu}
						<div id="mainTabSlider"></div>
					</nav>
				</div>
				
				<button type="button" id="topTabScrollButtonLeft"  data-visible="false" class="topTabScrollButton topTabScrollButtonLeft Hhide"></button>
				<button type="button" id="topTabScrollButtonRight" data-visible="false" class="topTabScrollButton topTabScrollButtonRight Hhide"></button>
				
			</div>
		
		</c:if>
		
	</header>
	
	<c:if test="${not empty mainMenu}">
	
		<c:if test="${browserInfo.capabilities['fnb_is_mobile']}">
		
			<div id="mobileLogin" class="bodyWidth Hhide" data-visible="false">
		
				<form autocomplete="off" method="post" id="LOGIN_FORM" name="LOGIN_FORM" action="${session_bankingUrl}/login/Controller" target="homeLogin">
		
					<input type="hidden" name="browserName" id="browserType" value="${browserInfo.agent.name}">
					
					<input type="hidden" name="browserVersion" id="browserVersion" value="${browserInfo.agent.version}">
									
					<input type="hidden" id="country" name="country" value="${session_country}">
					
					<input type="hidden" name="formname" id="formname" value="LOGIN_FORM"> 
					
					<input type="hidden" name="homePageLogin" id="homePageLogin" value="true">
		
					<input type="hidden" id="nav" name="nav" value="navigator.UserLogon">
		
					<input type="hidden" name="LoginButton" id="LoginButton" value="Login">
					
					<input type="hidden" name="bankingUrl" id="bankingUrl" value="${session_bankingUrl}"/>
					
					<input type="hidden" name="action" id="formAction" value="login"/>
					
					<div data-role="greyHeaderWrpaper">
						<fnb.generic.markup:h level="3">Login to Online Banking</fnb.generic.markup:h>
						<div data-role="blackCloseButton">
							
						</div>
					</div>
					
					<fnb.generic.forms:input.text inlineError="Please complete the User ID field." label="Username" id="Username" placeholder="Username"/>
		
					<fnb.generic.forms:input.password inlineError="Please complete the Password field." label="Password" id="Password" placeholder="Password"/>
									
					<div data-role="formElWrapper">
						<fnb.generic.forms:button role="" label="Login" id="loginButton"/>
					</div>
							
					<div data-role="greyHeaderWrpaper">
						<fnb.generic.markup:h level="3">Haven't registered?</fnb.generic.markup:h>
					</div>
					
					<span id="RegisterButtonMobile" class="loginPopupLinks" onclick="fnb.hyperion.utils.login.submitFormFromLink('enrollment.navigator.RegistrationSummary','LOGIN_FORM');">Sign up now...</span>
					
					<div data-role="greyHeaderWrpaper">
						<fnb.generic.markup:h level="3">More Online Banking options</fnb.generic.markup:h>
					</div>
					
					<fnb.generic.markup:a href="${session_contextPath}/security-centre/webroot-secureAnywhere.html">Webroot</fnb.generic.markup:a>
		
					<span id="ResetButtonMobile" class="loginPopupLinks" onclick="fnb.hyperion.utils.login.submitFormFromLink('loginproblems.FnbLoginProblems','LOGIN_FORM');" >Forgot password?</span>
					
					<span id="VerifyButtonMobile" class="loginPopupLinks" onclick="fnb.hyperion.utils.login.submitFormFromLink('verifyremittance.VerifyRemittanceAdvise','LOGIN_FORM');" >Verify payments</span>
							
					<input style="display: none;" type="submit" value="submit">
		
				</form>
		
			</div>
		
		</c:if>
	
	</c:if>	
	
	${not empty subMenu?subMenu:''}
