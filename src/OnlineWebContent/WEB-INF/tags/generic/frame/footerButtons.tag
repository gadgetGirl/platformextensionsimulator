<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute required="false" name="template"%>
<%@ attribute required="false" name="target"%>

<!-- Test for empty template attribute and set default -->
<c:if test="${empty template}"> 
	<c:set var="template" value="footer"/>
</c:if>
<!-- Test for empty target attribute and set default -->
<c:if test="${empty target}"> 
	<c:set var="target" value="#footerButtonsContainer"/>
</c:if>

<script class="pageScript" type="text/javascript">
var footerData = {
				templateGroupName : "${template}",
				templateTarget : "${target}",
				templateElements : []};
<jsp:doBody/>
fnb.hyperion.controller.pageTemplates.push(footerData);
</script>