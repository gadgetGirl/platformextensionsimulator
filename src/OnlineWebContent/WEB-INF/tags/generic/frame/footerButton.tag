<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute required="false" name="event"%>
<%@ attribute required="false" name="url"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="target"%>
<%@ attribute required="false" name="targetElement"%>
<%@ attribute required="false" name="dataTarget"%>

<%@ attribute required="false" name="clearHtmlTemplates"%>
<%@ attribute required="false" name="clearPageModuleObject"%>
<%@ attribute required="false" name="clearPageEventsArray"%>
<%@ attribute required="false" name="clearPageTemplatesArray"%>
<%@ attribute required="false" name="preLoadingCallback"%>
<%@ attribute required="false" name="postLoadingCallback"%>
<%@ attribute required="false" name="onClick"%>

 <%@ attribute required="false" name="id" %>

 <c:if test="${empty id }">
      <c:set var="idEmpty">'footerBtt_'+footerData['templateElements'].length</c:set>
</c:if>

<c:set var="clearHtmlTemplates" value="${not empty clearHtmlTemplates ? (clearHtmlTemplates eq 'true'? true : false) : false}"/>
<c:set var="clearPageModuleObject" value="${not empty clearHtmlTemplates ? (clearHtmlTemplates eq 'true'? true : false) : false}"/>
<c:set var="clearPageEventsArray" value="${not empty clearHtmlTemplates ? (clearHtmlTemplates eq 'true'? true : false) : false}"/>
<c:set var="clearPageTemplatesArray" value="${not empty clearHtmlTemplates ? (clearHtmlTemplates eq 'true'? true : false) : true}"/>


footerData['templateElements'].push(
                        {   templateName : 'footerButton',
                                 templateData : {
                                 event : '${not empty event ? event : ''}',
                                 url : '${not empty url ? url: ''}',
                                 target : '${not empty target ? target: ''}',
                                 targetElement : '${not empty targetElement ? targetElement: ''}',
                                 dataTarget : '${not empty dataTarget ? dataTarget: null}',
                                 label : '${not empty label ? label : ''}',
                                 preLoadingCallback : '${not empty preLoadingCallback ? preLoadingCallback: ''}',
                                 postLoadingCallback : '${not empty postLoadingCallback ? postLoadingCallback: ''}',
                                 id : <c:if test="${empty idEmpty}">'${id}'</c:if><c:if test="${not empty idEmpty}">${idEmpty}</c:if>,
                                 clearHtmlTemplates :  ${clearHtmlTemplates},
                                 clearPageModuleObject : ${clearPageModuleObject},
                                 clearPageEventsArray : ${clearPageEventsArray},
                                 clearPageTemplatesArray : ${clearPageTemplatesArray},
                                 onClick : '${not empty onClick ? onClick: ''}'
                                                    
       }
});   
