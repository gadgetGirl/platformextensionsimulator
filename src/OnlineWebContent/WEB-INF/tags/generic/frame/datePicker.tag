<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<div id="dateOverlay" class="overlayContainer bodyWidth index30 Hhide">
	<div class="overlayInner">
		<div class="datePickerContainer">
			<div class="datePickerInner">
				<button type="button" class="datePickerClose"></button>
				<button type="button" class="dateLeftScrollArrow"></button>
				<div class="datePickerScrollerWrapper">
					<div class="datePickerScroller"></div>
					
					<div data-role="calendarDropdown">
					   <select id="datePickerDropdown">
						   <c:forEach begin="1900" end="2025" step="1" var="year" varStatus="loop">
							    <option>${year}</option>
							</c:forEach>
					   </select>
					</div>

				</div>
				<button type="button" class="dateRightScrollArrow"></button>
			</div>
		</div>
	</div>
</div>