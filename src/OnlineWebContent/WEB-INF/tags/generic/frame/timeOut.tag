<div id="timeOut" class="overlayContainer bodyWidth index30 Hhide">

	<div class="overlayInner">
		
		<div class="timeOutWrapper">
				
			<div class="timeOutInner">
			
				<div class="clock">
					<div class="timer">60</div>
					<div id="timerText">seconds</div>
				</div>
				
				<div class="timeOutText">Your Online Banking session is about to time out...</div>
				
				<div class="timeOutButtonsWrapper">
					<a class="timeOutClose">Close</a>
					<a class="timeOutContinue">Continue</a>
				</div>
				
			</div>
			
		</div>
		
	</div>
	
</div>