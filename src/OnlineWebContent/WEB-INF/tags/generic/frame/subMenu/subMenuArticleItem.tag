<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="true" name="dataUrl"%>
<%@ attribute required="false" name="image"%>
<%@ attribute required="true" name="label"%>

<c:if test="${not empty dataUrl}"><c:set var="href" value="href='${dataUrl}'"/></c:if>

<c:set var="body"><jsp:doBody /></c:set>
<div data-role="subMenuAtricleWrapper" class="${className}">
	<fnb.generic.markup:img src="${image}" className="subMenuAtricleNumber"></fnb.generic.markup:img>
	<a data-role="subMenuAtricleItem" data-level="${dataLevel}" ${href} ${not empty last?'data-position="last"':''} ><span>${empty body?label:body}</span></a>
</div>