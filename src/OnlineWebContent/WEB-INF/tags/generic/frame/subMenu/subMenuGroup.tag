<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/fn.tld" prefix="fn"%>

<%@ attribute required="true" name="sets"%>
<%@ attribute required="true" name="setIds"%>
<%@ attribute required="false" name="menuTitle"%>
<%@ attribute required="false" name="atriclesTitle"%>

<div data-role="subMenuGroup"> <%-- cell --%>

		<div data-role="subMenuSetsWrapper"> <%-- table --%>
			<div data-role="subMenuSets"> <%-- row --%>
					<div data-role="subMenuMenu"> <%-- cell --%>
						${empty menuTitle ? 'MENU' : ''} <%-- attribute --%>
					</div>
					
					<div data-role="subMenuSetLinks"> <%-- cell --%>
						<c:forTokens delims="," items="${sets}" var="set" varStatus="stat" begin="0" end="3">
							<div id="pl" data-role="subMenuSetLink" <c:if test="${stat.first}">data-active="true"</c:if>>${set}</div>
						</c:forTokens>
					</div>
					<div data-role="subMenuSetLinks"> <%-- cell --%>
						<c:if test="${fn:length(sets) > 4}">
							<c:forTokens delims="," items="${sets}" var="set" begin="4" end="7" >
								<div data-role="subMenuSetLink" data-state="inactive" >${set}</div>
							</c:forTokens>
						</c:if>
					</div>
					<div data-role="subMenuSetLinks"> <%-- cell --%>
						<c:if test="${fn:length(sets) > 4}">
							<c:forTokens delims="," items="${sets}" var="set" begin="8"  >
								<div data-role="subMenuSetLink" data-state="inactive" >${set}</div>
							</c:forTokens>
						</c:if>
					</div>
					
					<div data-role="subMenuArticles"> <%-- cell --%>
						${atriclesTitle} <%-- attribute --%>
					</div>
			</div>
			
				<jsp:doBody />
		</div>

</div>