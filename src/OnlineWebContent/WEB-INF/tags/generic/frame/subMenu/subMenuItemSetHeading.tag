<%@ attribute required="false" name="label"%>
<c:set var="body"><jsp:doBody /></c:set>
<div data-role="subMenuItemSetHeading">${empty body?label:body}</div>