<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld" %>
<%@ attribute required="false" name="dataUrl"%>
<%@ attribute required="true" name="dataLevel"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="last"%>
<%@ attribute required="false" name="selected"%>
<%@ attribute required="false" name="thirdParty"%>
<c:set var="externalLink" value="false" scope="request"/>
<c:if test="${not empty dataUrl}">
	<c:choose>
		<c:when test="${fn:contains(dataUrl, '://')}">
			<c:set var="href" value="href='${dataUrl}'"/>
			<c:set var="externalLink" value="true" scope="request" />
		</c:when>
		<c:otherwise>
			<c:set var="href" value="href='${session_contextPath}${dataUrl}'"/>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${not empty selected}"><c:set var="dataSelected" value="data-selected='${selected}'"/></c:if>

<c:set var="body"><jsp:doBody /></c:set>
<c:choose>
	<c:when test="${dataLevel eq 1 and (empty href)}">
		<div data-role="subMenuHeading" data-level="1" ${not empty last?'data-position="last"':''} ${not empty dataSelected?dataSelected:''}>
			<div>${label}</div>
		</div>
	</c:when>
	<c:when test="${dataLevel eq 2 and (empty href)}">
		<div data-role="subMenuHeading2" data-level="2" ${not empty last?'data-position="last"':''} data-state="inactive" ${not empty dataSelected?dataSelected:''}>
			<div>${label}</div>
		${body}
		</div>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${externalLink eq 'true'}">
				<a class="subMenuItem" target="_blank" data-level="${dataLevel}" ${href} ${not empty last?'data-position="last"':''} ${not empty dataSelected?dataSelected:''}><span>${label}</span></a>
			</c:when>
			<c:otherwise>
				<a class="subMenuItem" data-role="subMenuItem" data-level="${dataLevel}" ${href} ${not empty last?'data-position="last"':''} ${not empty dataSelected?dataSelected:''}><span>${label}</span></a>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>
