<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="visible"%>

<div id="${id}" data-role="subMenuSet"  data-visible="${empty visible?'false':visible}" class="${empty visible?'Hhide':''}"> <%-- row --%>
	<jsp:doBody />
</div>
