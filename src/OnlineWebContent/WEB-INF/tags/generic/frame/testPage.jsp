
<div data-role="gridGroup">
	<div data-role="gridGroupInner">
		<div data-role="gridCol" data-width="80" data-border="borderRight" data-background="white">
<h1>Page heading</h1>
				<h2>Group heading</h2>
				<h3>HEADING</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean porta massa ac velit semper, et mollis turpis convallis. Phasellus faucibus leo sed diam malesuada pretium. Ut condimentum tristique quam sit amet cursus. Proin tempus tempor arcu id lobortis. Maecenas lorem enim, varius sed commodo in, mollis et elit. Duis congue, est aliquam ullamcorper eleifend, orci urna tincidunt felis, non molestie leo mauris sit amet nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras viverra augue vitae ligula hendrerit, non viverra lacus auctor. Quisque dapibus sem eu bibendum commodo. Aenean sodales sagittis metus, sed luctus nibh pharetra in. </p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean porta massa ac velit semper, et mollis turpis convallis. Phasellus faucibus leo sed diam malesuada pretium. Ut condimentum tristique quam sit amet cursus. Proin tempus tempor arcu id lobortis. Maecenas lorem enim, varius sed commodo in, mollis et elit. Duis congue, est aliquam ullamcorper eleifend, orci urna tincidunt felis, non molestie leo mauris sit amet nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras viverra augue vitae ligula hendrerit, non viverra lacus auctor. Quisque dapibus sem eu bibendum commodo. Aenean sodales sagittis metus, sed luctus nibh pharetra in. </p>
				
				<!--FROM ELEMENTS START-->
				
					<!-- Developer: Donovan
					Single tier dropdown -->
					<div data-type="formElement" data-width="100">
						<label data-role="formElementLabel" data-width="10">Single Tier:</label>
						<div data-role="dropdown" data-type="singleTier" data-event="touch" data-width="20">
							<span data-role="dropdownSelectWrapper">
								<select data-role="dropdownItemSelectWrapper" data-transparent="true" tabindex="0">
									<option class="label" value="">Please Select</option>
									<option value="1">Current (cheque/bond) Account</option>
									<option value="2">Savings Account</option>
								    <option value="3">Transmission Account</option>
								    <option value="4">Bond Account</option>
								    <option value="5">Subscription Share Account</option>
								    <option value="6">Fnbcard Account</option>
								</select>
							</span>
							<span data-role="dropdownSelected" data-state="closed" data-rows="4">Please Select</span>
							<span data-role="dropdownCarat"></span>
							<div data-role="dropdownContent" data-transparent="true">
								<ul data-role="dropdownItemWrapper" data-scrollable="">
									<li data-role="dropdownItem" data-value="1">Current (cheque/bond) Account</li>
									<li data-role="dropdownItem" data-value="2">Savings Account</li>
									<li data-role="dropdownItem" data-value="3">Transmission Account</li>
									<li data-role="dropdownItem" data-value="4">Bond Account</li>
									<li data-role="dropdownItem" data-value="5">Subscription Share Account</li>
									<li data-role="dropdownItem" data-value="6">Fnbcard Account</li>
								</ul>
							</div>
						</div>
					</div>
					<!--Single tier dropdown END-->
								
					<!-- Developer: Donovan
					Three tier dropdown -->
					<div data-type="formElement" data-width="100">
						<label data-role="formElementLabel" data-width="10">Three Tier:</label>
						<div data-role="dropdown" data-type="threeTier" data-event="touch" data-width="20">
							<span data-role="dropdownSelectWrapper">
								<select data-role="dropdownItemSelectWrapper" data-transparent="true" tabindex="0">
									<option class="label" value="">Please Select</option>
									<optgroup label="Aoo Flow On My Sole P">
										<option value="1">62004370045</option>
									</optgroup>
									<optgroup label="Business Account">
										<option value="2">62003772937</option>
									</optgroup>
									<optgroup label="Business Cheque">
										<option value="3">62002296095</option>
									</optgroup>
									<optgroup label="Business Cheque">
								    	<option value="4">62003182607</option>
									</optgroup>
									<optgroup label="Business Cheque Account">
								    	<option value="5">62001856262</option>
									</optgroup>
									<optgroup label="Business Cheque Account">
								    	<option value="6">62002298645</option>
									</optgroup>
								</select>
							</span>
							<span data-role="dropdownSelected" data-state="closed" data-rows="4">Please Select</span>
							<span data-role="dropdownCarat"></span>
							<div data-role="dropdownContent" data-transparent="true">
								<ul data-role="dropdownItemWrapper" data-scrollable="">
									<li data-role="dropdownItem" data-value="1"><span data-role="dropdownItemLabel">Aoo Flow On My Sole P</span><span data-role="dropdownItemLabel">62004370045</span></li>
									<li data-role="dropdownItem" data-value="2"><span data-role="dropdownItemLabel">Business Account</span><span data-role="dropdownItemLabel">62003772937</span></li>
									<li data-role="dropdownItem" data-value="3"><span data-role="dropdownItemLabel">Business Cheque</span><span data-role="dropdownItemLabel">62002296095</span></li>
									<li data-role="dropdownItem" data-value="4"><span data-role="dropdownItemLabel">Business Cheque</span><span data-role="dropdownItemLabel">62003182607</span></li>
									<li data-role="dropdownItem" data-value="5"><span data-role="dropdownItemLabel">Business Cheque Account</span><span data-role="dropdownItemLabel">62001856262</span></li>
									<li data-role="dropdownItem" data-value="6"><span data-role="dropdownItemLabel">Business Cheque Account</span><span data-role="dropdownItemLabel">62002298645</span></li>
								</ul>
							</div>
						</div>
					</div>
					
					<!--Three tier dropdown END-->
					<!-- Developer: Donovan
					Three tier dropdown -->
					<div data-type="formElement" data-width="100">
						<button id="testButton" type="button" data-role="standardButton">Click</button>
					</div>
					<!--Three tier dropdown END-->
					
					<div data-type="formElement" data-width="100">
						<div data-role="checkBox" data-state="checked">
			            	<input type="checkbox" checked="checked" data-visible="false">
			          	</div>
					</div>
					
					<div data-type="formElement" data-width="100">
						<label data-role="formElementLabel" data-width="10">Currency Input:</label>
						<input data-role="input" data-width="20" data-type="currency"/>
					</div>
				
					<div data-type="formElement" data-width="100">
						<label data-role="formElementLabel" data-width="10">Number Input:</label>
						<input data-role="input" data-width="20" data-type="number"/>
					</div>
					
					<div data-type="formElement" data-width="100">
						<label data-role="formElementLabel" data-width="10">Masked Input:</label>
						<input data-role="input" data-width="20" data-type="text" data-mask="true" data-maskValue="Some masked text" value="Some masked text"/>
					</div>
					
					<div data-type="formElement" data-width="100">
						<label data-role="formElementLabel" data-width="10">Datepicker:</label>
						<div data-role="datePicker" data-months="18" data-startBefore="8">
							<input type="hidden" name="datePickerValue" data-value="2013-10-30">
							<span data-role="datePickerSelected">
								<span data-role="datePickerLabelTop">Today</span>
								<span data-role="datePickerLabelBottom"></span>
							</span>
							<span data-role="datePickerCarat"></span>
						</div>
						<!-- <div data-role="datePicker" data-months="9" data-startBefore="3">
							<input type="hidden" name="datePickerValue" data-value="2013-10-30">
							<span data-role="datePickerSelected">Today</span>
							<span data-role="datePickerCarat"></span>
						</div> -->
					</div>
					
				<!--FROM ELEMENTS END-->
				<table data-role="bankingTable">
					<thead>
						<tr>
							<th colspan="2">Group 1</th>
							<th colspan="2">Group 2</th>
						</tr>
						<tr>
							<th>TH1</th>
							<th>TH2</th>
							<th>TH3</th>
							<th>TH4</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="4">Group 1</td>
						</tr>
						<tr>
							<td>str1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td>str2</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td colspan="4">Group 2</td>
						</tr>
						<tr>
							<td>str3</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td>str4</td>
							<td>1</td>
							<td>1</td>
							<td>3</td>
						</tr>
						<tr>
							<td>str5</td>
							<td>1</td>
							<td>1</td>
							<td>3</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>total</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
						</tr>
					</tfoot>
				</table>
								
				<table data-role="bankingTable">
					<thead>
						<tr>
							<th colspan="2">Group 1</th>
							<th colspan="2">Group 2</th>
						</tr>
						<tr>
							<th>TH1</th>
							<th>TH2</th>
							<th>TH3</th>
							<th>TH4</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="4">Group 1</td>
						</tr>
						<tr>
							<td>str1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td>str2</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td colspan="4">Group 2</td>
						</tr>
						<tr>
							<td>str3</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td>str4</td>
							<td>1</td>
							<td>1</td>
							<td>3</td>
						</tr>
						<tr>
							<td>str5</td>
							<td>1</td>
							<td>1</td>
							<td>3</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>total</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
						</tr>
					</tfoot>
				</table>
				</div>
				<div data-role="gridCol" data-width="20" data-border="borderLeft">&nbsp;</div>
				</div>
				</div>
				<!-- 
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
				<div data-role="formElementContainer">
					<div data-role="checkBox" data-state="checked">
			            	<input type="checkbox" checked="checked" data-visible="false">
			          	</div>
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formNote">Note</div>
					</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						</div>
				<div data-role="formElementContainer">
					<input data-role="input" data-type="currency" />
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
						<div data-role="formElementContainer" data-focused="true">
						<input data-role="input" data-type="currency"  />
							<div data-role="formInlineNote">Note</div>
							<div data-role="formInlineError">Error</div></div>
				</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						</div>
				<div data-role="formElementContainer">
					<input data-role="input" data-type="currency" />
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
						<div data-role="formElementContainer" data-focused="true">
						<input data-role="input" data-type="currency"  />
							<div data-role="formInlineNote">Note</div>
							<div data-role="formInlineError">Error</div></div>
				</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						</div>
				<div data-role="formElementContainer">
					<input data-role="input" data-type="currency" />
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
						<div data-role="formElementContainer" data-focused="true">
						<input data-role="input" data-type="currency"  />
							<div data-role="formInlineNote">Note</div>
							<div data-role="formInlineError">Error</div></div>
				</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						</div>
				<div data-role="formElementContainer">
					<input data-role="input" data-type="currency" />
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
						<div data-role="formElementContainer" data-focused="true">
						<input data-role="input" data-type="currency"  />
							<div data-role="formInlineNote">Note</div>
							<div data-role="formInlineError">Error</div></div>
				</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						</div>
				<div data-role="formElementContainer">
					<input data-role="input" data-type="currency" />
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
						<div data-role="formElementContainer" data-focused="true">
						<input data-role="input" data-type="currency"  />
							<div data-role="formInlineNote">Note</div>
							<div data-role="formInlineError">Error</div></div>
				</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						</div>
				<div data-role="formElementContainer">
					<input data-role="input" data-type="currency" />
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
						<div data-role="formElementContainer" data-focused="true">
						<input data-role="input" data-type="currency"  />
							<div data-role="formInlineNote">Note</div>
							<div data-role="formInlineError">Error</div></div>
				</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						</div>
				<div data-role="formElementContainer">
					<input data-role="input" data-type="currency" />
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
						<div data-role="formElementContainer" data-focused="true">
						<input data-role="input" data-type="currency"  />
							<div data-role="formInlineNote">Note</div>
							<div data-role="formInlineError">Error</div></div>
				</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						</div>
				<div data-role="formElementContainer">
					<input data-role="input" data-type="currency" />
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
						<div data-role="formElementContainer" data-focused="true">
						<input data-role="input" data-type="currency"  />
							<div data-role="formInlineNote">Note</div>
							<div data-role="formInlineError">Error</div></div>
				</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						</div>
				<div data-role="formElementContainer">
					<input data-role="input" data-type="currency" />
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
						<div data-role="formElementContainer" data-focused="true">
						<input data-role="input" data-type="currency"  />
							<div data-role="formInlineNote">Note</div>
							<div data-role="formInlineError">Error</div></div>
				</div>
				</div>
			</div>
			<div data-role="formElementWrapper"><div data-role="formElementLabel"><div data-role="formElementLabelInner">Currency Input:</div>
						<div data-role="toolTip"></div></div>
				<div data-role="formElementContainer">
					<div data-role="checkBox" data-state="checked">
			            	<input type="checkbox" checked="checked" data-visible="false">
			          	</div>
					<div data-role="formInlineError">Error</div>
				</div>
				<div data-role="formElementContent">
					<div data-role="formElementContentInner" class="clearfix">
						<div data-role="formNote">Note</div>
				</div>
				</div>
			</div> -->
				
				
			
				<!-- Developer Donovan
				For Testing -->
				<script>
					var footerData = {templateGroupName : 'footer', templateTarget:fnb.controller.footerButtonGroup, templateElements:[
					                           {templateName : 'footerButton', templateData: {label: 'Cancel'}},
					                           {templateName : 'footerButton', templateData: {label: 'Edit'}},
					                           {templateName : 'footerButton', templateData: {label: 'Pay'}}]}
					
					fnb.hyperion.controller.pageTemplates.push(footerData);
				</script>
				
