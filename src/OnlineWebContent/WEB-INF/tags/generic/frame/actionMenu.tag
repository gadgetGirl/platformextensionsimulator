<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="actionMenu"%>
<%@ attribute required="false" name="actionMenuLabelClass"%>
<c:set var="theBody">
	<jsp:doBody />
</c:set>
<c:if test="${not empty theBody}">
	<div id="actionMenu" class="index30 Hhide">
		<div id="actionMenuGrid">
			<div id="actionMenuContent" class="actionMenuGridInner">${theBody}</div>
		</div>
	</div>
</c:if>
