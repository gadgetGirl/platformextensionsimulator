<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}">
<%@ attribute required="false" name="sideBar"%>
<div data-role="gridGroup">
	<div data-role="gridGroupInner">
		<div data-role="gridCol" data-width="80" data-background="white">
			<jsp:doBody />
		</div>
		<div data-role="gridCol" data-width="20" data-background="grey1" class="borderLeft borderRight">
			${not empty sideBar ? sideBar : '&nbsp'}
		</div>
	</div>
</div>
</fnb.generic.frame:coreContainer>