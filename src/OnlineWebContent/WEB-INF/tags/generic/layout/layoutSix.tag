<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="true" name="pageHeading"%>
<%@ attribute required="false" name="actionMenu"%>
<fnb.generic.frame:coreContainer selectedTopTab="${selectedTopTab}" mainMenu="${mainMenu}" subMenu="${subMenu}">

	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
<div data-role="gridGroup" id="layoutSix">
	<div data-role="gridGroupInner">
		<div data-role="gridCol" class="borderRightImage" data-width="100">
			<div data-role="layoutSixHeader" >
				<h1>${pageHeading}</h1>
				<div data-role="layoutSixSubTabsContainer">
					<div data-role="layoutSixSubTab">For Me</div>
					<div data-role="layoutSixsubTab">For My Business</div>
				</div>
				<jsp:doBody/>
			</div>
		</div>
	</div>
</div>
</fnb.generic.frame:coreContainer>