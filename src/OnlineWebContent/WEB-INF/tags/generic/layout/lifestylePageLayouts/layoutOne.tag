<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>
<%@ taglib prefix="fnb.generic.frame.pageMenu" tagdir="/WEB-INF/tags/generic/frame/pageMenu"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="lifestyleLayoutOneA_className"%>
<%@ attribute required="false" name="lifestyleLayoutOneB_className"%>
<%@ attribute required="false" name="lifestyleLayoutOneC_className"%>
<%@ attribute required="false" name="subSection" description="place holder"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageImage" description="graphic used to as a background on the page"%>
<%@ attribute required="false" name="pageMenu" description="content of the pageMenu"%>
<%@ attribute required="false" name="heading" description="file location to page menu"%>
<%@ attribute required="false" name="subHeading" description="file location to page menu"%>

<%--
	lifestyleLayoutOne:  3 coloum layout 
		lifestyleLayoutOneA: page thumbnail & menu
		lifestyleLayoutOneB: heading, subheading and content
		lifestyleLayoutOneC: place holder
 --%>
 <c:choose>
 <c:when test='${browserInfo.capabilities["fnb_is_mobile"]}'>
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">
 
	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/lifestylePageLayouts/layoutOne.css" />
	
	<div id="${id}" data-role="lifestyleLayoutOne">
		<div data-role="lifestyleLayoutOneInner">
			<div class="${lifestyleLayoutOneB_className}" data-role="lifestyleLayoutOneB" >
				<div data-role="lifestyleLayoutOneHeadings" >
					<h1>${heading}</h1>
					<h2>${subHeading}</h2>
				</div>
				<div data-role="lifestyleLayoutOneContent" >
					<jsp:doBody />
				</div>
			</div>
			<div class="${lifestyleLayoutOneC_className}" data-role="lifestyleLayoutOneC" >
				${subSection}
			</div>
		</div>
	</div>
</fnb.generic.frame:coreContainer> 
</c:when>
<c:otherwise>
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/lifestylePageLayouts/layoutOne.css" />
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/lifestylePageLayouts/ie8.css" ie8="true" />
	<div id="${id}" data-role="lifestyleLayoutOne">
		<div data-role="lifestyleLayoutOneInner">
			<div class="${lifestyleLayoutOneA_className}" data-role="lifestyleLayoutOneA">
				
					<div data-role="lifestyleLayoutOneThumbnail">
						<c:if test="${(not empty SSACode) or (not empty pageThumbnail)}">
							<c:choose>
				 				<c:when test="${not empty SSACode}">
									<img src="/downloads/sales_downloads/ssa/${session_country}/${session_skin}/images/lifestyles/${SSACode}/Tabs.png"></img>
								</c:when>
								<c:when test="${not empty pageThumbnail}">
				 					<img src="${session_contextPath}${pageThumbnail}"></img>
				 				</c:when>
							</c:choose>
						</c:if>
					</div>
				
				<fnb.generic.frame.pageMenu:pageMenu >${pageMenu}</fnb.generic.frame.pageMenu:pageMenu>
			</div>
			<div class="${lifestyleLayoutOneB_className}" data-role="lifestyleLayoutOneB" >
				<div data-role="lifestyleLayoutOneHeadings" >
					<h1>${heading}</h1>
					<h2>${subHeading}</h2>
				</div>
				<div data-role="lifestyleLayoutOneContent" >
					<jsp:doBody />
				</div>
			</div>
			<div class="${lifestyleLayoutOneC_className}" data-role="lifestyleLayoutOneC" >
				${subSection}
			</div>
		</div>
	</div>
</fnb.generic.frame:coreContainer>
</c:otherwise>
</c:choose>