<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="false" name="actionMenu"%>
<%@ attribute required="false" name="sideBar"%>
<fnb.generic.frame:coreContainer selectedTopTab="${selectedTopTab}" mainMenu="${mainMenu}" subMenu="${subMenu}">

	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/leadFormPageLayouts/layoutOne.css" />
	<fnb.generic.frame:import ie8="true" type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/leadFormPageLayouts/ie8.css" />
<div data-role="gridGroup">
	<div data-role="gridGroupInner">
		<div data-role="gridCol" data-width="80" data-background="white"<c:if test="${not empty id}"> id="${id}"</c:if>>
			<jsp:doBody />
		</div>
		<div data-role="gridCol" data-width="20" data-background="grey1" class="borderLeft borderRight">
			${not empty sideBar ? sideBar : '&nbsp'}
		</div>
	</div>
</div>
</fnb.generic.frame:coreContainer>