<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.frame.pageMenu" tagdir="/WEB-INF/tags/generic/frame/pageMenu"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="layoutFiveA_className"%>
<%@ attribute required="false" name="layoutFiveB_className"%>
<%@ attribute required="false" name="section1" description="place holder"%>
<%@ attribute required="false" name="section2" description="place holder"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageImage" description="graphic used to as a background on the page"%>
<%@ attribute required="false" name="pageMenu" description="content of the pageMenu"%>
<%@ attribute required="false" name="heading" description="file location to page menu"%>
<%@ attribute required="false" name="subHeading" description="file location to page menu"%>

<%--
	layoutFive:  2 row layout 
		layoutFiveA: page heading, subheading, place holder
		layoutFiveB: place holder
		layoutFiveC: place holder
 --%>
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">
	<div id="${id}" data-role="layoutFive">
		<div data-role="layoutFiveA">
			<div data-role="layoutFiveAinner">
				<h1>${heading}</h1>
				<h2>${subHeading}</h2>
				${section1}
			</div>
		</div>
		<div data-role="layoutFiveB" >
			<div data-role="layoutFiveBinner">
				${section2}
			</div>
		</div>
	</div>
</fnb.generic.frame:coreContainer>