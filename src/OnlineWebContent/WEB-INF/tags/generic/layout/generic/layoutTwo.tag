<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.frame.pageMenu" tagdir="/WEB-INF/tags/generic/frame/pageMenu"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="layoutTwoA_className"%>
<%@ attribute required="false" name="layoutTwoB_className"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageImage" description="graphic used to as a background on the page"%>
<%@ attribute required="false" name="pageMenu" description="content of the pageMenu"%>

<%--
	layoutTwo:  2 coloum layout 
		layoutTwoA: page thumbnail & menu
		layoutTwoB: content
 --%>
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">
	<div id="${id}" data-role="layoutTwo">
		<div data-role="layoutTwoInner">
			<div class="${layoutTwoA_className}" data-role="layoutTwoA">
				<div data-role="layoutTwoThumbnail">
					<c:if test="${not empty pageThumbnail}"><img src="${session_contextPath}${pageThumbnail}"></img></c:if>
				</div>
				<fnb.generic.frame.pageMenu:pageMenu >${pageMenu}</fnb.generic.frame.pageMenu:pageMenu>
			</div>
			<div class="${layoutTwoB_className}" data-role="layoutTwoB" >
				<div data-role="layoutTwoContent" >
					<jsp:doBody />
				</div>
			</div>
		</div>
	</div>
</fnb.generic.frame:coreContainer>