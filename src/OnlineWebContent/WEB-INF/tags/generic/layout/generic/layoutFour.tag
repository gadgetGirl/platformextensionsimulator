<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>
<%@ taglib prefix="fnb.generic.frame.pageMenu" tagdir="/WEB-INF/tags/generic/frame/pageMenu"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="layoutFourA_className"%>
<%@ attribute required="false" name="layoutFourB_className"%>
<%@ attribute required="false" name="layoutFourC_className"%>
<%@ attribute required="false" name="layoutFourD_className"%>
<%@ attribute required="false" name="layoutFourE_className"%>
<%@ attribute required="false" name="section1" description="place holder 1"%>
<%@ attribute required="false" name="section2" description="place holder 2"%>
<%@ attribute required="false" name="section3" description="place holder 3"%>
<%@ attribute required="false" name="section4" description="place holder 4"%>
<%@ attribute required="false" name="section5" description="place holder 5"%>

<%--
	layoutFour:  4 section layout with place holders in all four sections
		designed for home pages
 --%>
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">

<c:choose>
	<c:when test="${browserInfo.capabilities['fnb_is_mobile']}">
			<fnb.generic.markup:img  src="/00Assets/skins/00/images/logoMobile.png" />
			<div id="${id}" data-role="layoutFour">
				<div data-role="layoutFourInner">
					<div class="${layoutFourA_className}" data-role="layoutFourA" >
						<div data-role="layoutFourAInner">
							${section1}
						</div>
					</div>
					
					<div class="${layoutFourB_className}" data-role="layoutFourB" >
						<div data-role="layoutFourBInner">
							${section2}
						</div>
					</div>
				</div>
			</div>
	
			<div id="${id}" data-role="layoutFour">
				<div data-role="layoutFourInner">
					<div class="${layoutFourC_className}" data-role="layoutFourC" >
						<div data-role="layoutFourCInner">
							${section3}
						</div>
					</div>
				</div>
			</div>
							
			<div id="${id}" data-role="layoutFour">
				<div data-role="layoutFourInner">
					<div class="${layoutFourD_className}" data-role="layoutFourD" >
						<div data-role="layoutFourDInner">
							${section4}
						</div>
					</div>

					<div class="${layoutFourE_className}" data-role="layoutFourE" >
						<div data-role="layoutFourEInner">
								${section5}
						</div>
					</div>
				</div>
			</div>
			
	</c:when>
	<c:otherwise>
	
			<div id="${id}" data-role="layoutFour">
				<div data-role="layoutFourInner">
					<div class="${layoutFourA_className}" data-role="layoutFourA" >
						<div data-role="layoutFourAInner">
							${section1}
						</div>
					</div>
					
					<div class="${layoutFourB_className}" data-role="layoutFourB" >
						<div data-role="layoutFourBInner">
								<table style="height: 100%; width: 100%;">
									<tr>
										<td style="border-bottom: 4px solid white; vertical-align: middle; font-size: 2em; color: #666666;">
											${section2}
										</td>
									</tr>
									<tr>
										<td id="popular" style="vertical-align: top; text-align: left; color: #666666; padding: 5%">
											${section3}
										</td>
									</tr>
								</table>
						</div>
					</div>
					<div class="${layoutFourA_className}" data-role="layoutFourBspacer" >
						<div class="${layoutFourA_className}" data-role="layoutFourBspacerInner" >&nbsp;</div>
					</div>
					
					<div class="${layoutFourC_className}" data-role="layoutFourC" >
						<div data-role="layoutFourCInner">
							<div data-role="layoutFourCInnerInner">
								<div class="${layoutFourD_className}" data-role="layoutFourD" >
									${section4}
								</div>
								<div class="${layoutFourE_className}" data-role="layoutFourE" >
									${section5}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
	</c:otherwise>
</c:choose>
</fnb.generic.frame:coreContainer>