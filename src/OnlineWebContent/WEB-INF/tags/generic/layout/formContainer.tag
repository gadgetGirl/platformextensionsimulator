<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="false" name="actionMenu"%>
<%@ attribute required="false" name="sideBar"%>
<%@ attribute required="false" name="systemJs"%>
<fnb.generic.frame:coreContainer systemJs="${systemJs}" actionMenu="${actionMenu}" selectedTopTab="${selectedTopTab}" mainMenu="${mainMenu}" subMenu="${subMenu}">

<div data-role="formsLayoutOneGroup">
	<div data-role="formsLayoutOneInner">
		<div data-role="formsLayoutOneContentOne" data-background="white"<c:if test="${not empty id}"> id="${id}"</c:if>>
			<jsp:doBody />
		</div>
		<div data-role="formsLayoutOneContentTwo" data-background="grey1" class="borderLeft borderRight">
			${not empty sideBar ? sideBar : '&nbsp'}
		</div>
	</div>
</div>

</fnb.generic.frame:coreContainer>