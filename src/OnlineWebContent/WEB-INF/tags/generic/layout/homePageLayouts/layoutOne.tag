<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ attribute required="false" name="actionMenu"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="section1" description="place holder 1"%>
<%@ attribute required="false" name="section2" description="place holder 2"%>
<%@ attribute required="false" name="section3" description="place holder 3"%>
<%@ attribute required="false" name="section4" description="place holder 4"%>
<%--
	homeLayoutOne:  4 section layout with place holders in all four sections
		designed for home pages
 --%>
<fnb.generic.frame:coreContainer
	mainMenu="${mainMenu}"
	subMenu="${subMenu}"
	selectedTopTab="${selectedTopTab}"
	systemCss="${systemCss}"
	systemJs="${systemJs}"
	systemMetaTags="${systemMetaTags}">
		
	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/homePageLayouts/layoutOne.css" />
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/homePageLayouts/ie8.css" ie8="true"/>
	
	<div
		id="${id}"
		class="homeLayoutOne">
		<div class="homeLayoutOneInner clearfix">
			<div class="homeLayoutOneA">${section1}</div>
			<div class="homeLayoutOneB ">${section2}</div>
			<div class="homeLayoutOneC">${section3}</div>
			<div class="homeLayoutOneD">${section4}</div>
		</div>
	</div>
</fnb.generic.frame:coreContainer>