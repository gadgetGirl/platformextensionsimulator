<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.frame.pageMenu" tagdir="/WEB-INF/tags/generic/frame/pageMenu"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="calculatorLayoutOneA_className"%>
<%@ attribute required="false" name="calculatorLayoutOneB_className"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageImage" description="graphic used to as a background on the page"%>
<%@ attribute required="false" name="pageMenu" description="content of the pageMenu"%>

<%--
	calculatorLayoutOne:  2 coloum layout 
		calculatorLayoutOneA: page thumbnail & menu
		calculatorLayoutOneB: content
 --%>
 
 <c:set var="actionMenu" value="${browserInfo.capabilities['fnb_is_mobile']?pageMenu:''}" />
 
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">
 
	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/calculatorPageLayouts/layoutOne.css" />
	<fnb.generic.frame:import ie8="true" type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/calculatorPageLayouts/ie8.css" />
	<div id="${id}" data-role="calculatorLayoutOne">

		<div data-role="calculatorLayoutOneInner">
			<c:if test='${!browserInfo.capabilities["fnb_is_mobile"]}'>
				<div class="${calculatorLayoutOneA_className}" data-role="calculatorLayoutOneA">
					<div data-role="calculatorLayoutOneThumbnail">
						<c:if test="${not empty pageThumbnail}"><img src="${session_contextPath}${pageThumbnail}"></img></c:if>
					</div>
					<fnb.generic.frame.pageMenu:pageMenu >${pageMenu}</fnb.generic.frame.pageMenu:pageMenu>
				</div>
			</c:if>
			
			<div class="${calculatorLayoutOneB_className}" data-role="calculatorLayoutOneB" >
				<div data-role="calculatorLayoutOneContent" >
					<jsp:doBody />
				</div>
			</div>
		</div>
	</div>
</fnb.generic.frame:coreContainer>