<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="true" name="pageHeading"%>
<%@ attribute required="false" name="actionMenu"%>
<fnb.generic.frame:coreContainer selectedTopTab="${selectedTopTab}" mainMenu="${mainMenu}" subMenu="${subMenu}">

	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/allPromotionsPageLayouts/layoutOne.css" />
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/allPromotionsPageLayouts/ie8.css" ie8="true" />
	
	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
	<div data-role="gridGroup" id="allPromotionslayoutOne">
		<div data-role="gridGroupInner">
			<div data-role="gridCol" class="borderRightImage" data-width="100">
				<div data-role="allPromotionslayoutOneHeader" >
					<h1>${pageHeading}</h1>
					<div data-role="subTabsContainer" class="allPromotionslayoutOneSubTabsContainer">
						<div data-role="subTab" id="forMeContentSubTab" class="allPromotionslayoutOneSubTab" data-selected="true" data-content="forMeContent">For Me</div>
						<div data-role="subTab" id="forMyBusinessContentSubTab" class="allPromotionslayoutOneSubTab" data-selected="false" data-content="forMyBusinessContent">For My Business</div>
					</div>
				</div>
				<jsp:doBody/>
			</div>
		</div>
	</div>

	<fnb.generic.frame:import type="js" link="${session_contextPath}/00Assets/generic/genericJs/layouts/allPromotionsLayouts/layoutOne.js" />
</fnb.generic.frame:coreContainer>