<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute required="false" name="width"%>
<%@ attribute required="false" name="href"%>
<%@ attribute required="false" name="moreButton"%>
<c:set var="body"><jsp:doBody /></c:set>
<c:set var="body" value="${empty body?'&nbsp;':body}" />
<c:set var="href"><c:if test="${not empty href}"> href="${session_contextPath}${href}"</c:if></c:set>
<c:set var="clickable">
	<c:choose>
		<c:when test="${not empty href and moreButton eq 'false'}"> class="clickable"</c:when>
		<c:when test="${not empty href}"> class="moreText"</c:when>
	</c:choose>
</c:set>
<c:choose>
	<c:when test="${not empty href}">
		<a id="gridItem" data-role="overviewLayoutOneGridItem" data-width="${empty width?100:width}"${href}${clickable}>
			<div data-role="overviewLayoutOneGridItemInner">${body}</div>
		</a>
	</c:when>
	<c:otherwise>
		<div id="gridItem" data-role="overviewLayoutOneGridItem" data-width="${empty width?100:width}">
			<div data-role="overviewLayoutOneGridItemInner">${body}</div>
		</div>
	</c:otherwise>
</c:choose>