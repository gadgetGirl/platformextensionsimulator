<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>
<%@ attribute required="true" name="href"%>
<%@ attribute required="true" name="label"%>
<fnb.generic.markup:a href="${href}" newTab="false" className="overviewLayoutOneGridItemLink">${label}</fnb.generic.markup:a>