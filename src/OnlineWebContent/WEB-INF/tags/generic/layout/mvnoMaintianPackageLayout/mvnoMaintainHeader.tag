<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute required="false" name="heading"%>
<%@ attribute required="false"  name="subHeading"%>
<%@ taglib prefix="fnb.hyperion.banking.markup" tagdir="/WEB-INF/tags/banking/markup"%>

<div class=mvnocontractHeading>
	<div class="mvnocontractHeadingRow">
		<fnb.hyperion.banking.markup:h level="2"> ${heading }</fnb.hyperion.banking.markup:h>
	</div>
	<div class="mvnocontractHeadingRow">
		<span class="mvnocontractSubHeading">${subHeading }</span>
	</div>
</div>