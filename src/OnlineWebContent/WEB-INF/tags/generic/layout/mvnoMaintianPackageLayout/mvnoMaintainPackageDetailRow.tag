<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<!-- Import Generic tags-->
<%@ taglib prefix="fnb.generic.mvnoMaintainPackageLayout" tagdir="/WEB-INF/tags/generic/layout/mvnoMaintianPackageLayout"%>

<%@ attribute required="false" name="label"%>
<%@ attribute required="false"  name="value"%>
<%@ attribute required="false"  name="availableCredit"%>
<%@ attribute required="false"  name="currency"%>

<c:choose>
	<c:when test="${availableCredit == 'true' }">
		<c:set var="rowHTML">
			<div  class="availableCreditCell availableCreditValue"><span class="currency"><c:if test="${empty currency }">R </c:if>${currency } </span><span id="availableCredit">${value }</span> </div>
			<div class="availableCreditCell">${label } </div> 
		</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="rowHTML">
			<div class="contractDetailedCell">${label } </div> <div class="contractDetailedCell"> ${value }</div>
		</c:set>
	</c:otherwise>

</c:choose>
<div class="contractDetailedRow">
		${rowHTML}		
</div>