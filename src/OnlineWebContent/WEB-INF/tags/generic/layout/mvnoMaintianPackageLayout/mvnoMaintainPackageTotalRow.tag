<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute required="false" name="label"%> 
<%@ attribute required="false"  name="value"%>
<%@ attribute required="false"  name="isTotal"%>

<c:choose>
	<c:when test="${isTotal == 'true' }">
		<c:set var="isTotalRowClass" value="isTotalRow" />
		<c:set var="rowHTML">
			<span class="isTotalCell isTotalValue">${value }</span>
			<div class="isTotalCell isTotalCellLabel">${label } </div> 
		</c:set>
	</c:when>
	<c:otherwise>
	<c:set var="isTotalRowClass" value="" />
		<c:set var="rowHTML">
			  <div class="totalDetailedCell totalvalue"> ${value }</div>
			  <div class="totalDetailedCell totalLabel">${label } </div> 
		</c:set>
	</c:otherwise>

</c:choose>
<div class="totalDetailedRow ${isTotalRowClass }">
		${rowHTML}		
</div>