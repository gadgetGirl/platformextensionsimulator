<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute required="false" name="className"%>
<%@ attribute required="false"  name="id"%>

<div <c:if test="${not empty id }"> id="${id }" </c:if> class="mvnoMaintainLayoutWrapper ${className }">
	
	<jsp:doBody />
	
</div>