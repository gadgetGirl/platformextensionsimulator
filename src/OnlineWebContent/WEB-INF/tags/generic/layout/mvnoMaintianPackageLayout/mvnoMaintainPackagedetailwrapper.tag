<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<!-- Import Generic tags-->
<%@ taglib prefix="fnb.generic.mvnoMaintainPackageLayout" tagdir="/WEB-INF/tags/generic/layout/mvnoMaintianPackageLayout"%>

<%@ attribute required="false" name="heading"%>
<%@ attribute required="false"  name="mvnoMaintainPackageBean" type="java.util.ArrayList"%>

<c:if test="${empty heading }"> 
	<c:set var="heading" value="FNB Postpaid 100" />
</c:if>

<div class="contractDetailed">
<c:if test="${not empty heading }" >
	<div class="contractDetailedRow">
		<div class="contractDetailedCell contractHeading">${heading }</div>
	</div>
</c:if>

	<%-- add For each  --%>	
	<c:forEach items="${mvnoMaintainPackageBean}" var="topDataInfo" varStatus="tabCount" >
		<fnb.generic.mvnoMaintainPackageLayout:mvnoMaintainPackageDetailRow
				label="${topDataInfo.firstColumn}" value="${topDataInfo.secondColumn}" availableCredit="${topDataInfo.availableCredit}">
		</fnb.generic.mvnoMaintainPackageLayout:mvnoMaintainPackageDetailRow>
	</c:forEach>
	<%-- end foreach --%>
	
</div>