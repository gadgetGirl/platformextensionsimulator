<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="false" name="pageHeading"%>
<%@ attribute required="false" name="actionMenu"%>
<fnb.generic.frame:coreContainer selectedTopTab="${selectedTopTab}" mainMenu="${mainMenu}" subMenu="${subMenu}">

	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
	<jsp:doBody/>
</fnb.generic.frame:coreContainer>