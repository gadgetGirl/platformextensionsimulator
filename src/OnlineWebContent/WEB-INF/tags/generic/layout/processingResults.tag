<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="className" %>
<%@ attribute name="hidePrintAndDownload" required="false" rtexprvalue="true"%>

<jsp:useBean id="processingResult" class="java.util.HashMap"/>
<c:set target="${processingResult}" property="errorCode" value="0"/>
<c:set target="${processingResult}" property="pending" value="false"/>
<c:set target="${processingResult}" property="partial" value="false"/>
<c:set target="${processingResult}" property="processingMessage" value="Your SIM has been activated successfully and will be active within 2 hours."/>
<c:set target="${processingResult}" property="traceId" value="asas"/>


<jsp:useBean id="printButton" class="java.util.HashMap"/>
<c:set target="${printButton}" property="url" value="asassz"/>
<c:set target="${printButton}" property="downloadButton" value="asassz"/>

<c:set target="${processingResult}" property="printButton" value="${printButton}"/>

<c:if test="${not empty processingResult.footerButtons}">
	<c:set var="footerButtonsResult" scope="request" value="${processingResult.footerButtons}"/>
</c:if>

	<c:choose>
		<c:when test="${processingResult.errorCode =='true'}">
				<div id="eziCoreContainer" class="processingResultErrorCode0">
				<div class="whiteMessageContainer">
						<fnb.generic.markup:h level="5">Thank you</fnb.generic.markup:h>
				</div>
				<fnb.generic.forms:output.labelValuePair id="processingMessage" label="${processingResult.processingMessage}" value="Ref: ${processingResult.traceId}"/>
											
				<jsp:doBody />
				</div>
			
		</c:when>
		<c:when test="${processingResult.pending =='true'}">
				<div id="eziCoreContainer" class="processingResultPartial">
					<div class="whiteMessageContainer">
							<fnb.generic.markup:h level="5">Take Note...</fnb.generic.markup:h>
					</div>
					<fnb.generic.markup:p id="processingMessage">${processingResult.processingMessage}</fnb.generic.markup:p> 
					<fnb.generic.markup:p id="processTraceID">Ref: ${processingResult.traceId}</fnb.generic.markup:p>
												
					<jsp:doBody />
				</div>
			
		</c:when>
		<c:when test="${processingResult.errorCode == 0}">
				<div id="eziCoreContainer" class="processingResultErrorCode0">
					<div class="whiteMessageContainer">
							<fnb.generic.markup:h level="5">Thank you</fnb.generic.markup:h>
					</div>
					<div class="rightBoxInner">
						<fnb.generic.markup:p id="processingMessage">${processingResult.processingMessage}</fnb.generic.markup:p> 
						<c:if test="${not empty processingResult.traceId}">
							<fnb.generic.markup:p id="processTraceID">Ref: ${processingResult.traceId}</fnb.generic.markup:p>
						</c:if>
					</div>
												
					<jsp:doBody />
					
						<c:if test="${!hidePrintAndDownload}">
									<div class="printAndDownloadContainer ">
										<c:choose>
											<c:when test="${!empty processingResult.printButton.url }">
												<div class="printDiv" onclick="window.print(); return false;">
													<a type="button" data-role="downloadPrintButton" href="#" target="printWindow">
														<span data-role="downloadPrintButtonLabel">Print</span>
													</a>
												</div>
											</c:when>
										<c:otherwise>
												<div class="printDiv" onclick="fnb.functions.loadUrlToPrintDiv.load('${processingResult.downloadButton.url}'); return false;">
													<a type="button" data-role="downloadPrintButton" href="#" >
														<span data-role="downloadPrintButtonLabel">Print</span>
													</a>
												</div>
											</c:otherwise>
										</c:choose>
										
										<c:choose>
											<c:when test="${!empty processingResult.downloadButton.url }">
												<div class="downloadDiv"  onclick="window.print(); return false;">
													<a type="button" data-role="downloadPrintButton" href="#" >
														<span data-role="downloadPrintButtonLabel">Download</span>
													</a>
												</div>
											</c:when>
											<c:otherwise>
												<div class="downloadDiv"  onclick="fnb.functions.loadUrlToPrintDiv.load('${processingResult.downloadButton.url}'); return false;">
													<a type="button" data-role="downloadPrintButton" href="#" >
														<span data-role="downloadPrintButtonLabel">Download</span>
													</a>
												</div>
											</c:otherwise>
										</c:choose> 
									</div>
								</c:if>
					
				</div>
			
		</c:when>
		<c:when test="${processingResult.errorCode == 350}">
				<div id="eziCoreContainer" class="processingResultErrorCode350">
					<div class="whiteMessageContainer">
							<fnb.generic.markup:h level="5">Take Note...</fnb.generic.markup:h>
					</div>
					<fnb.generic.forms:output.labelValuePair id="processingMessage" label="${processingResult.processingMessage}" value="Ref: ${processingResult.traceId}"/>
												
					<jsp:doBody />
				</div>
			
		</c:when>
	</c:choose>
	



 
