<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>
<%@ attribute name="label" required="false" %>
<%@ attribute name="image" required="false" %>

<c:choose>
	<c:when test="${not browserInfo.capabilities['fnb_is_mobile']}">
		<table class="solutions">
			<tr>
				<td class="solutionsLabel" width="33.3333%">
					<c:if test="${not empty image}">
						<div class="solutionsImgAnchor"><img class="solutionsImg" src="${session_contextPath}${image}"></img></div>
					</c:if>
					${fn:replace(label," ","<br />")}
				</td>
				<jsp:doBody/>
			</tr>
		</table>
	</c:when>
	<c:otherwise>
		<div class="solutions">
				<div class="solutionsLabel">
					<c:if test="${not empty image}">
						<div class="solutionsImgAnchor"><img class="solutionsImg" src="${session_contextPath}${image}"></img></div>
					</c:if>
					${label}
				</div>
				<jsp:doBody/>
		</div>
	</c:otherwise>
</c:choose>
