<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>

<%@ attribute name="value" required="false" %>
<%@ attribute name="span" required="false" %>

<tr id="">
	<td>&nbsp;</td>
	<td colspan="${span}" class="compareGroupHeading">${value}</td>
</tr>
