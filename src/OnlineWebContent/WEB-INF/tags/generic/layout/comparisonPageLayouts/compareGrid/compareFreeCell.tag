<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="span" required="false" %>
<tr class="tr" class="compareFreeCell">
	<td>&nbsp;</td>
	<td colspan="${span}"><jsp:doBody/></td>
</tr>
