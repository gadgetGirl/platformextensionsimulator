<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="height" required="false" %>
<%@ attribute name="cols" required="false" %>
<c:set var="colwidth" value="${80 / cols}" scope="request" />
<c:set var="colheight" value="${height}" scope="request" />

<tr class="compareBenefits">
	<td>&nbsp;</td>
	<jsp:doBody></jsp:doBody>
</tr>
