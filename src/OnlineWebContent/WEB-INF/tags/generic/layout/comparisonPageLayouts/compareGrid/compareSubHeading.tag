<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="id" required="false" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="href" required="false" rtexprvalue="true"%>
<c:set var="body"><jsp:doBody/></c:set>
<th width="${colwidth}%" <c:if test="${not empty id}">id="${id}"</c:if> class="compareSubHeading" href="${href}" >
	${body}
</th>   