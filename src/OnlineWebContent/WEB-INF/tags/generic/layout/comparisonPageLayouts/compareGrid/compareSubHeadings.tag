<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="label" required="false" %>
<%@ attribute name="cols" required="false" %>
<c:set var="colwidth" value="${80 / cols}" scope="request" />

<thead class="compareSubHeadings">
	<tr class="tr">
		<th class="compareSubHeadingsLabel" width="20%">${not empty label?label:"&nbsp"}</th>
		<jsp:doBody></jsp:doBody>
	</tr>
</thead>
