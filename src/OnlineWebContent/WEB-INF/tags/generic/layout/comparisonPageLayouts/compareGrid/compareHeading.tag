<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="id" required="false" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="href" required="false" rtexprvalue="true"%>
<c:set var="body"><jsp:doBody/></c:set>
<th width="${colwidth}%" <c:if test="${not empty id}">id="${id}"</c:if> class="compareHeading ${className}" href="${href}" >
	<c:choose>
		<c:when test="${not empty href}">
			<a href="${session_contextPath}${href}">${body}</a>
		</c:when>
		<c:otherwise>
			${body}
		</c:otherwise>
	</c:choose>
</th>   