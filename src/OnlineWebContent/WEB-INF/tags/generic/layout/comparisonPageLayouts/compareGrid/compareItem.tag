<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>

<%@ attribute name="className" required="false" %>
<%@ attribute name="value" required="false" %>
<%@ attribute name="span" required="false" %>
<%@ attribute name="label" required="false" %>
<%@ attribute name="href" required="false" %>
<%@ attribute name="popupMessage" required="false" %>
<%@ attribute name="popupGroup" required="false" %>
<%@ attribute name="compareValues" required="false" %>

<tr data-role="compareItem">
	<td class="compareItemLabel ${className}">
		<c:choose>
			<c:when test="${not empty href}">
				<a href="${session_contextPath}${href}">${label}</a>
			</c:when>
			<c:otherwise>
				${label}
			</c:otherwise>
		</c:choose>
		
		<c:set var="helpText">
<%-- 			<ui:helpText helpId="${popupMessage}" groupId="${popupGroup}" /> --%>
			This is some help text
		</c:set>
		<c:if test="${not empty helpText}">
			<div class="popupMessageAnchor Hhide" data-visible="false">
				<div class="popupMessage">
					<div class="popupMessageClose"></div>
					${helpText}
					<div class="popupMessagePointer"></div>
				</div>
			</div>
		</c:if>
		
	</td>
	
	<c:forTokens items="${compareValues}" delims=";" var="value" varStatus="stat">
				<c:choose>
					<c:when test="${value eq 'true'}">
						<td class="compareItemValue ${className}"><div class="compareItemValueInner available">&nbsp;</div></td>
					</c:when>
					<c:when test="${value eq 'false'}">
						<td class="compareItemValue ${className}"><div class="compareItemValueInner unavailable">&nbsp;</div></td>
					</c:when>
					<c:otherwise>
						<td class="compareItemValue ${className}"><div class="compareItemValueInner">${value}</div></td>
					</c:otherwise>
				</c:choose>

	</c:forTokens>
</tr>