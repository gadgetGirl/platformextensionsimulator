<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="wizard" required="false" %>
<%@ attribute name="height" required="false" %>
<%@ attribute name="cols" required="false" %>
<c:set var="colwidth" value="${80 / cols}" scope="request" />
<c:set var="colheight" value="${height}" scope="request" />

<thead class="compareHeadings">
	<tr class="tr">
		<th class="compareWizard" width="20%">
			<c:choose>
				<c:when test="${not empty wizard}">
<%-- 					<ui:productWizardColumn display="yes" hasOptionalCol="yes" />  --%>
				</c:when>
				<c:otherwise>&nbsp;</c:otherwise>
			</c:choose>
		</th>
		<jsp:doBody/>
	</tr>
</thead>
