<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="id" required="false"%>
<%@ attribute name="className" required="false"%>
<%@ attribute name="numberOfBenefits" required="false"%>
<%@ attribute name="href" required="false"%>
<%@ attribute name="hide" required="false"%>

<%-- if there are no benefits don't display --%>
<c:if test="${numberOfBenefits ne 0}">
	<c:set var="body"><jsp:doBody /></c:set>
	
	<td class="compareBenefit ${className}">
		<c:choose>
			<c:when test="${not empty href}">
				<a href="${href}">
					<div class="compareBenefitInner">
						<span class="compareBenefitNumber">${numberOfBenefits}</span> benefits
					</div>
				</a>
			</c:when>
			<c:otherwise>
				<div class="compareBenefitInner">
					<span class="compareBenefitNumber">${numberOfBenefits}</span> benefits
				</div>
			</c:otherwise>
		</c:choose>
	</td>
</c:if>