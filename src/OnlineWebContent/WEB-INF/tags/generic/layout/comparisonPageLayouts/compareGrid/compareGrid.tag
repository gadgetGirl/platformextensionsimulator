<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="id" required="false" %>
<%@ attribute name="title" required="false" %>
<%@ attribute name="cols" required="true" %>
<table id="" class="compareGrid">
	<c:if test="${not empty title}">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th class="compareGridTitle" colspan="${cols}">${title}</th>
			</tr>
		</thead>
	</c:if>
	<jsp:doBody/>
</table>