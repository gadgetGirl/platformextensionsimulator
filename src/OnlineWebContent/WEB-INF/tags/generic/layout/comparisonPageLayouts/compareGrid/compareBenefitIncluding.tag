<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="id" required="false"%>
<%@ attribute name="className" required="false"%>
<%@ attribute name="benefits" required="false"%>
<c:set var="body"><jsp:doBody /></c:set>
<td class="compareBenefitIncludes ${className}">
	<ul>
		<li class="compareBenefitIncludesTitle">Benefits include</li>
			${body}
	</ul>
</td>