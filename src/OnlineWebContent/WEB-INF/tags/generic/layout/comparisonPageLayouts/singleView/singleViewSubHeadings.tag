<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="label" required="false" %>

<div class="singleViewSubHeadings">
	<div class="singleViewSubHeadingsLabel">${not empty label?label:"&nbsp"}</div>
	<jsp:doBody/>
</div>
