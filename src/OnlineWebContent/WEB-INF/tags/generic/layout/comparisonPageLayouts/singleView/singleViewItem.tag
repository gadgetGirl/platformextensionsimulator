<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>

<%@ attribute name="className" required="false" %>
<%@ attribute name="value" required="false" %>
<%@ attribute name="span" required="false" %>
<%@ attribute name="label" required="false" %>
<%@ attribute name="href" required="false" %>
<%@ attribute name="popupMessage" required="false" %>
<%@ attribute name="popupGroup" required="false" %>
<%@ attribute name="singleViewValues" required="false" %>

<div data-role="singleViewItem">
	<div class="singleViewItemLabel ${not empty href?'hrefRedirect clickable ':''} ${className} td" href="${href}">
		${label}
		
		<c:set var="helpText">
<%-- 			<ui:helpText helpId="${popupMessage}" groupId="${popupGroup}" /> --%>
			This is some help text
		</c:set>
		<c:if test="${not empty helpText}">
			<div class="popupMessageAnchor Hhide" data-visible="false">
				<div class="popupMessage">
					<div class="popupMessageClose"></div>
					${helpText}
					<div class="popupMessagePointer"></div>
				</div>
			</div>
		</c:if>
		
	</div>
</div>