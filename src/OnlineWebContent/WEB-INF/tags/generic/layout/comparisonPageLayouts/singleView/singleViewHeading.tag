<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="id" required="false" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="href" required="false" rtexprvalue="true"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup/"%>
<c:set var="body"><jsp:doBody/></c:set>
<fnb.generic.markup:a href="${href}" id="${id}" className="${className}">
${body}
</fnb.generic.markup:a>
