<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup" %>
<%@ taglib prefix="fnb.generic.widgets.slider" tagdir="/WEB-INF/tags/generic/widgets/slider" %>
<%@ attribute name="title" required="false" %>
<c:set var="sliderDivId">
slider<%=Math.round((Math.random() * 1000))%>
</c:set>

<fnb.generic.widgets.slider:sliderHeader value="Swipe to see more products" />

<div class="sliderContainer">
<div id="${sliderDivId}" class="singleView">
	<div id="" class="singleViewSlider">
		<c:if test="${not empty title}">
			<fnb.generig.markup:h level="1" class="singleViewTitle">${title}</fnb.generig.markup:h>
		</c:if>
		<jsp:doBody/>
	</div>
</div>
</div>