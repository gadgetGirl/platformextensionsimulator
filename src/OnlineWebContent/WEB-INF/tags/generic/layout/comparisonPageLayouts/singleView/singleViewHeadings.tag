<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="wizard" required="false" %>

<div class="singleViewHeadings">
		<c:if test="${not empty wizard}">
			<div class="singleViewWizard">
<%-- 				<ui:productWizardColumn display="yes" hasOptionalCol="yes" />  --%>
			</div>
		</c:if>
		<jsp:doBody/>
</div>
