<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/chameleon"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="id" required="false"%>
<%@ attribute name="className" required="false"%>
<%@ attribute name="numberOfBenefits" required="false"%>
<%@ attribute name="href" required="false"%>

<c:set var="body"><jsp:doBody /></c:set>

<td class="singleViewBenefit ${className}">
	<div class="singleViewBenefitInner">
		<span class="singleViewBenefitNumber">${numberOfBenefits}</span> benefits
	</div>
</td>