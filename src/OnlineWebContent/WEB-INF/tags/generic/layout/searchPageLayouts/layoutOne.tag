<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="searchLayoutOneA_className"%>
<%@ attribute required="false" name="searchLayoutOneB_className"%>
<%@ attribute required="false" name="section1" description="place holder"%>
<%@ attribute required="false" name="section2" description="place holder"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageImage" description="graphic used to as a background on the page"%>
<%@ attribute required="false" name="pageMenu" description="content of the pageMenu"%>
<%@ attribute required="false" name="heading" description="file location to page menu"%>
<%@ attribute required="false" name="subHeading" description="file location to page menu"%>

<%--
	searchLayoutOne:  2 row layout 
		searchLayoutOneA: page heading, subheading, place holder
		searchLayoutOneB: place holder
 --%>
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/searchPageLayouts/layoutOne.css" />
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/searchPageLayouts/searchResults/searchResults.css" />
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/searchPageLayouts/searchResults/searchResults.css" ie8="true" />
	
	<div id="${id}" class="searchLayoutOne">
		<div class="searchLayoutOneA">
			<div class="searchLayoutOneAinner">
				<h1>${heading}</h1>
				${section1}
			</div>
		</div>
		<div class="searchLayoutOneB" >
			<div class="searchLayoutOneBinner">
				${section2}
			</div>
		</div>
	</div>
</fnb.generic.frame:coreContainer>