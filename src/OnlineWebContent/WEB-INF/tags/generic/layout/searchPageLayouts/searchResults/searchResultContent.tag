<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup" %>

<%@ attribute name="title" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="content" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="url" required="false" rtexprvalue="true" type="java.lang.String"%>

<fnb.generic.markup:h level="4">
	<fnb.generic.markup:a href="${session_contextPath}${url}">${title}</fnb.generic.markup:a>
</fnb.generic.markup:h>
<fnb.generic.markup:p>${content}</fnb.generic.markup:p>