<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoPageLayouts.mvnoAllBrands" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoAllBrands"%>


<%@ attribute required="false" name="buttonLabel" %>


<c:if test="${not empty buttonLabel}">
	<c:set var="buttonLabel">
		${buttonLabel}
	</c:set>

</c:if>

<div data-role="mvnoAllBrandButtonWrapper">
	<button name="mvnoBrandSelect">${buttonLabel}</button>
</div>
