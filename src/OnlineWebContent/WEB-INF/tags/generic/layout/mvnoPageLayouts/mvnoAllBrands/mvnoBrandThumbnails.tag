<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="true"  name="mvnoBrandImageSrc"%>
<%@ attribute required="true"  name="mvnoBrandImageHref"%> 	
	
<div data-role="mvnoBrandthumbnails">
	<a href="${mvnoBrandImageHref}"><img src="${mvnoBrandImageSrc}" width="200" height="100"></a>
</div>
	
	
