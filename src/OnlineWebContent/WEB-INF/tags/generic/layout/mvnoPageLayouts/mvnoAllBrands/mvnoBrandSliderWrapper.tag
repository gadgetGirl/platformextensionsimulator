<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false" name="className"%>


<c:if test="${not empty className}">
   <c:set var="className">
    class="${className }"
   </c:set>
</c:if>

<%-- wrapper for mvno Brand Slide Container --%>




<%-- wrapper for mnvoProductDetial --%>
<div data-role="mvnoBrandSliderWrapper" id="mvnoBrandSliderWrapper" class="bodyWidth">
	<div data-role="mvnoBrandSliderWrapperNewAbsolute">
		<div data-role="sliderContainer" ${className} id="horizontalBrandSliderWapper">
			<jsp:doBody />
		</div>
	</div>
</div>

<div data-role="SliderButtonContainer" >
	<span data-role="mvnoBrandSliderButton"><a href="0" class="sliderButton"></a><a href="1"  class="sliderButton"></a><a href="2" class="sliderButton"></a></span>
</div>