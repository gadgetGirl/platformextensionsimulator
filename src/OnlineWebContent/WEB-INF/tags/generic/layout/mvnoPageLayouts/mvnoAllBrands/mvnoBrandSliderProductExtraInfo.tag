<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false"  name="mvnoBrandSliderProductInfoOrange"%>
<%@ attribute required="false"  name="mvnoBrandSliderProductExtraInfoValue"%>  

	<%-- add all mvno Extra Product Details --%>
	<c:choose>
		<c:when test= "${mvnoBrandSliderProductInfoOrange == 'true'}">					
			<span class="BrandSilderprodctInfoOrange">${mvnoBrandSliderProductExtraInfoValue}</span>
		</c:when>
		<c:otherwise>
			<span class="BrandSilderprodctInfoNormal">${mvnoBrandSliderProductExtraInfoValue}</span>
		</c:otherwise>	
	</c:choose>	
