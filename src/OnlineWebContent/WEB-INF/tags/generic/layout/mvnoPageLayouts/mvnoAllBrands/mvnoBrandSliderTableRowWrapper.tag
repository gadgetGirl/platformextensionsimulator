<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%@ attribute required="false" name="className" %>

<c:if test="${not empty className}">
	<c:set var="className">
		class="${className }"
	</c:set>
</c:if>

<%-- wrapper for mvno Brand Slide Table Cell One --%>
<div data-role="mvnoBrandSliderTableRow" ${className }>
	<jsp:doBody />
</div>