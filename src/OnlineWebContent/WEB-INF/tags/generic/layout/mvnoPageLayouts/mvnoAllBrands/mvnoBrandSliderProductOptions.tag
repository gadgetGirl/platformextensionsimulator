<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="true" name="mvnoBrandSliderProductDesc" %>
<%@ attribute required="true" name="mvnoBrandSliderProductAmount" %>

<c:if test="${not empty className}">
	<c:set var="className">
		class="${className }"
	</c:set>
</c:if>

<div data-role="mvnoBrandSliderTableCell" >
	<span class="mvnoBrandSliderProductGray">${mvnoBrandSliderProductDesc }</span>
	<span class="mvnoBrandSliderProductBlack">${mvnoBrandSliderProductAmount }</span>
</div>

