<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false" name="mvnoBrandSliderHead"%>

<c:if test="${not empty mvnoBrandSliderHead }">
	<div data-role="mvnoBrandSliderTableRow" class="mvnoSliderHead">
		<span class="brandSliderHead">${mvnoBrandSliderHead}</span>
	</div>
</c:if>


