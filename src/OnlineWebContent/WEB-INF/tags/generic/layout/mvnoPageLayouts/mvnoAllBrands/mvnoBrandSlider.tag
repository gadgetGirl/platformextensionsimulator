<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoPageLayouts.mvnoAllBrands" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoAllBrands"%>


<%@ attribute required="false"  name="mvnoBrandSliderData"%>


	<!--The Silder Container (contains all sildes[table slide]) -->
	<%-- Foreach start here!!! --%>
	
		
			<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableWrapper>
					<!-- Cell one will contain or hold all data -->
					
				<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellOne>
					<!-- header information -->
						<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderHead mvnoBrandSliderHead="XBOX 360 bundle 1"></fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderHead>
								
							<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
								<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductOptions mvnoBrandSliderProductDesc="250GB" mvnoBrandSliderProductAmount="16GB" ></fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductOptions>
							</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
							
							<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
								<%--<c:forEach items="${mvnoBrandSliderProductExtraInfo}" var="rowExtraInformation">--%>  
							    	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="true" mvnoBrandSliderProductExtraInfoValue="Plus"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="Cover +"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="16GbIpodNano +"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="ScreenGaurd +"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="R100iStoreVoucher"/>
								<%--</c:forEach> --%>
							</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
		
				</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellOne>
				  <!-- Cell one will contain or hold Product Image and And status Image -->
				<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellTwo></fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellTwo>
			</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableWrapper>	
			
						<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableWrapper>
					<!-- Cell one will contain or hold all data -->
					
				<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellOne>
					<!-- header information -->
						<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderHead mvnoBrandSliderHead="XBOX 360 bundle 2"></fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderHead>
								
							<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
								<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductOptions mvnoBrandSliderProductDesc="250GB" mvnoBrandSliderProductAmount="16GB" ></fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductOptions>
							</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
							
							<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
								<%--<c:forEach items="${mvnoBrandSliderProductExtraInfo}" var="rowExtraInformation">--%>  
							    	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="true" mvnoBrandSliderProductExtraInfoValue="Plus"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="Cover +"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="16GbIpodNano +"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="ScreenGaurd +"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="R100iStoreVoucher"/>
								<%--</c:forEach> --%>
							</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
		
				</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellOne>
				  <!-- Cell one will contain or hold Product Image and And status Image -->
				<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellTwo></fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellTwo>
			</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableWrapper>	
			
			<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableWrapper>
					<!-- Cell one will contain or hold all data -->
					
				<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellOne>
					<!-- header information -->
						<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderHead mvnoBrandSliderHead="XBOX 360 bundle 3"></fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderHead>
								
							<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
								<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductOptions mvnoBrandSliderProductDesc="250GB" mvnoBrandSliderProductAmount="16GB" ></fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductOptions>
							</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
							
							<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
								<%--<c:forEach items="${mvnoBrandSliderProductExtraInfo}" var="rowExtraInformation">--%>  
							    	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="true" mvnoBrandSliderProductExtraInfoValue="Plus"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="Cover +"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="16GbIpodNano +"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="ScreenGaurd +"/>
								 	<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderProductExtraInfo mvnoBrandSliderProductInfoOrange="false" mvnoBrandSliderProductExtraInfoValue="R100iStoreVoucher"/>
								<%--</c:forEach> --%>
							</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableRowWrapper>
		
				</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellOne>
				  <!-- Cell one will contain or hold Product Image and And status Image -->
				<fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellTwo></fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableCellTwo>
			</fnb.generic.mvnoPageLayouts.mvnoAllBrands:mvnoBrandSliderTableWrapper>		
