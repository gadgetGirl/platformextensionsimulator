<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- wrapper for mvnoSimShopWrapper --%>
<div data-role="mvnoLandingPageWrapper">
	<div data-role="mvnoLandingPageThumbnailsWrapper">
		<jsp:doBody />
	</div>
</div>