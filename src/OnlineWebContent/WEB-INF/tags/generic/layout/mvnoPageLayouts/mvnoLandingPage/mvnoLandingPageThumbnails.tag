<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false"  name="className"%>
<%@ attribute required="false"  name="mvnoLandingPageHead"%>
<%@ attribute required="false"  name="mvnoLandingPageDeviceSimImage"%>
<%@ attribute required="false"  name="mvnoLandingPageDeviceSimImageHref"%>
<%@ attribute required="flase"  name="mnvoLadingPageButtonLabel" %>


<c:if test="${not empty className }">
	<c:set var="className" >
	class = "${className }"
	</c:set>
</c:if>

	<div data-role="mvnoLandingPagethumbnails" ${className }>
		<h1 class="mvnoLandingPageHead">${mvnoLandingPageHead }</h1>
		<a href="${mvnoLandingPageDeviceSimImageHref}" class="mvnoLandPageImg"><img src="${mvnoLandingPageDeviceSimImage}" width="115" height="100"></a>
		<div class="mvnoLandingPageButtonWrapper">
		   <div class="mvnolandingPageButton" > ${mnvoLadingPageButtonLabel }</div>
		</div>
	</div>