<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoPageLayouts" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoProductTable"%>

<%@ attribute required="false" name="mvnoProductTableData"%>
<%@ attribute required="false" name="mvnoProductSelected"%>

<%@ attribute required="false" name="tableWrapperSelected"%>
<%@ attribute required="false" name="mvnoProductSelectedHeadClassName"%>

<%-- If mvnoProductSelected equals true Set variables for selected Classes --%>
<c:if test="${mvnoProductSelected == 'true' }">
	<c:set var="tableWrapperSelected">
	mvnoProductTableSelectedColor
	</c:set>
	
	<c:set var="mvnoProductSelectedClassName">
	 productWhite
	</c:set>
</c:if>

<div data-role="mvnoProductTableWrapper">

 <fnb.generic.mvnoPageLayouts:mvnoProductDivTableWrapper className="${tableWrapperSelected}">
     <fnb.generic.mvnoPageLayouts:mvnoProductCellOneWrapper>
      	
    	<%-- send Product header data to mvnoProductHead Component --%>
		<fnb.generic.mvnoPageLayouts:mvnoProductHead  mvnoProductHead="iPhone5s" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}" />
		<%-- send Product Options data to mvnoProductOptions Component --%>
		
		<fnb.generic.mvnoPageLayouts:mvnoProductTableRowWrapper mnvoProductOptionClass="mnvoProductOptions">
		<%--	<c:forEach items="${mvnoProductOptions}" var="mnvoProductOptions">--%>
				   <fnb.generic.mvnoPageLayouts:mvnoProductTableRowOptions productDetail="16GB" productAmount="R595" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}"  />
				   <fnb.generic.mvnoPageLayouts:mvnoProductTableRowOptions productDetail="16GB" productAmount="R595" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}" />
				   <fnb.generic.mvnoPageLayouts:mvnoProductTableRowOptions productDetail="16GB" productAmount="R595" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}"/>
				   <fnb.generic.mvnoPageLayouts:mvnoProductTableRowOptions productDetail="16GB" productAmount="R595" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}" />
		<%--	</c:forEach> --%>
		</fnb.generic.mvnoPageLayouts:mvnoProductTableRowWrapper>
		
		<%-- send Product Extra Detail to mvnoProductExtraInfo Component --%>
      	<fnb.generic.mvnoPageLayouts:mvnoProductTableRowWrapper>
		   <%--<c:forEach items="${mvnoProductExtraInfo}" var="rowExtraInformation">--%>
			    	<fnb.generic.mvnoPageLayouts:mvnoProductTableRowExtraInfo  mvnoProductInfoOrange="true" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}" mvnoProductExtraInfoValue="Plus"/>
				 	<fnb.generic.mvnoPageLayouts:mvnoProductTableRowExtraInfo mvnoProductInfoOrange="false" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}" mvnoProductExtraInfoValue="Cover +"/>
				 	<fnb.generic.mvnoPageLayouts:mvnoProductTableRowExtraInfo mvnoProductInfoOrange="false" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}" mvnoProductExtraInfoValue="16GbIpodNano +"/>
				 	<fnb.generic.mvnoPageLayouts:mvnoProductTableRowExtraInfo mvnoProductInfoOrange="false" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}" mvnoProductExtraInfoValue="ScreenGaurd +"/>
				 	<fnb.generic.mvnoPageLayouts:mvnoProductTableRowExtraInfo mvnoProductInfoOrange="false" mvnoProductSelectedClassName="${mvnoProductSelectedClassName}" mvnoProductExtraInfoValue="R100iStoreVoucher"/>
			<%--</c:forEach> --%>
		</fnb.generic.mvnoPageLayouts:mvnoProductTableRowWrapper>
						<%-- add all mvno Product Color Options --%>
				<fnb.generic.mvnoPageLayouts:mvnoProductTableRowWrapper mnvoProductOptionClass="productColorOptions">
				<%--	<c:forEach items="${mvnoProductColorOptions}" var="rowColorOptions">--%>
						<fnb.generic.mvnoPageLayouts:mvnoProductTableRowColorOptions colorClass="Gray" colorDesc="Gray" />
						<fnb.generic.mvnoPageLayouts:mvnoProductTableRowColorOptions colorClass="Gold" colorDesc="Gold" />
						<fnb.generic.mvnoPageLayouts:mvnoProductTableRowColorOptions colorClass="Silver" colorDesc="Silver" />
				<%--	</c:forEach>--%>
				</fnb.generic.mvnoPageLayouts:mvnoProductTableRowWrapper>
     
     </fnb.generic.mvnoPageLayouts:mvnoProductCellOneWrapper>

     <fnb.generic.mvnoPageLayouts:mvnoProductCellTwoWrapper></fnb.generic.mvnoPageLayouts:mvnoProductCellTwoWrapper>
     
 </fnb.generic.mvnoPageLayouts:mvnoProductDivTableWrapper>


 

</div>