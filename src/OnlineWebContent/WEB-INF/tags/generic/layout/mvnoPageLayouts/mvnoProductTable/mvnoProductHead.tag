<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false"  name="mvnoProductHead"%>   	 	
<%@ attribute required="false"  name="mvnoProductSelectedClassName"%>  
     	 	
<%-- add mvno Product Header --%>		
<c:if test="${not empty mvnoProductHead}">
	<div data-role="mvnoProductTableRow" class="productHead ${mvnoProductSelectedClassName}">
		<span>${mvnoProductHead}</span>
	</div>
</c:if>