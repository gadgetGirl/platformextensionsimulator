<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false"  name="mvnoProductInfoOrange"%>
<%@ attribute required="false"  name="mvnoProductExtraInfoValue"%>
<%@ attribute required="false"  name="mvnoProductSelectedClassName" %>  

	<%-- add all mvno Extra Product Details --%>
	<c:choose>
		<c:when test= "${mvnoProductInfoOrange == 'true'}">					
			<span class="mvnoProdctInfoOrange">${mvnoProductExtraInfoValue}</span>
		</c:when>
		<c:when test="${mvnoProductSelectedClassName == 'productWhite' }">
			<span class="mvnoProductInfoWhite">${mvnoProductExtraInfoValue}</span>
		</c:when>
		<c:otherwise>
			<span class="mvnoProductInfoNormal">${mvnoProductExtraInfoValue}</span>
		</c:otherwise>	
	</c:choose>	
	
	
