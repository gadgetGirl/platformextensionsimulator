<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false"  name="mnvoProductOptionClass"%>

<c:if test="${not empty mnvoProductOptionClass }" >
	<c:set var="mnvoRowClass">
	 class="${mnvoProductOptionClass}"
	</c:set>
</c:if>

<%-- wrapper for mnvoProductDetial --%>
<div data-role="mvnoProductTableRow" ${mnvoRowClass}>
	<jsp:doBody />
</div>
