<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoPageLayouts" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts"%>

<%@ attribute required="false"  name="colorClass"%> 
<%@ attribute required="false"  name="colorDesc"%>
<c:set var="class">
	<c:if test="${not empty colorClass}">
	class="${colorClass} "
	</c:if>
	</c:set> 

<%-- add all mvno Product Color Options --%>
<div data-role="mvnoProductTableCell" data-celType="colorOption" ${class}>${colorDesc}</div> 

	