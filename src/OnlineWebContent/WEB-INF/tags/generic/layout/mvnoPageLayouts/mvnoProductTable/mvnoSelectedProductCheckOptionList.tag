<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
		
<%@ attribute required="flase" name="optionLabel" %>	
<%@ attribute required="flase" name="optionHeading" %>
<%@ attribute required="flase" name="optionId" %>

	<c:choose>
		<c:when test="${not empty optionHeading }" >					
			<li class="mvnoProductSelectCheckOptionsHeading">${optionHeading }</li>
		</c:when>
		<c:otherwise>
			<li class="mvnoProductSelectCheckOption"><fnb.generic.forms:checkbox id="${optionId }" label="${optionLabel}"/></li>
		</c:otherwise>	
	</c:choose>			
		
		
		
		
