<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="true" name="imgSrc"%>


<c:if test="${not empty imgSrc }">
	<c:set var="imgSrc">
			<img src="${imgSrc }">
	</c:set> 
</c:if>

${imgSrc }

