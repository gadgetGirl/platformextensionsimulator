<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoProductTable" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoProductTable/"%>

<%@ attribute required="false"  name="productFeatureListData"%> 


<div data-role="mvnoProductSelectTableCell">
	<ul class="mvnoProductFeatureList">
<%--	<c:forEach items="${productFeatureListData}" var="productFeatureListDataRow">--%>
     	<fnb.generic.mvnoProductTable:mvnoProductFeatureItems  productFeature="Cellular Functions" productFeatureDsc="Yes" />
		<fnb.generic.mvnoProductTable:mvnoProductFeatureItems  productFeature="Model" productFeatureDsc="MF3580S0/A, MF360S0/A" />
		<fnb.generic.mvnoProductTable:mvnoProductFeatureItems  productFeature="Memory" productFeatureDsc="n/a" />
		<fnb.generic.mvnoProductTable:mvnoProductFeatureItems  productFeature="Storage Capacitys" productFeatureDsc="64GB" />
		<fnb.generic.mvnoProductTable:mvnoProductFeatureItems  productFeature="Upgradable Storage" productFeatureDsc="n/a" />
		<fnb.generic.mvnoProductTable:mvnoProductFeatureItems  productFeature="Processor and Speed" productFeatureDsc="A7 chip with 64-bit architecture & M7 motion coprocessor" />
		<fnb.generic.mvnoProductTable:mvnoProductFeatureItems  productFeature="Color" productFeatureDsc="Space Gray/Gold/Silver" />
	</ul>
</div>

