<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoProductTable" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoProductTable"%>

<%@ attribute required="false" name="selectedProductOptions" %>

<div data-role="mvnoProductSelectTableCell">
 <fnb.generic.mvnoProductTable:mvnoSelectedProductDetail detailHeader="iPhones" productExtraInfo="${selectedProductOptions}" />
</div>