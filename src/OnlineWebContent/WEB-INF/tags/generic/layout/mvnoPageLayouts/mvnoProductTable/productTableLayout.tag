<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoPageLayouts" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts"%>

<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="desc"%>
<%@ attribute required="false" name="testClass" %>
<%@ attribute required="false" name="label" %>
<%@ attribute required="false" name="type" %>
<%@ attribute required="false" name="value" %>



<div data-role="tableWrapper">
	<div data-role="tableRow" data-rowType="heading">
	  <h1>iPhone5s</h1>
	</div>
	<div data-role="tableRow" data-rowType="productOptions">
		 <div data-role="tableCell" data-celType="lightGray">16GB</div>
		 <div data-role="tableCell" data-celType="black">R595</div>
	     <div data-role="tableCell" data-celType="lightGray">16GB</div>
		 <div data-role="tableCell" data-celType="black">R595</div>
		 <div data-role="tableCell" data-celType="lightGray">16GB</div>
		 <div data-role="tableCell" data-celType="black">R595</div>
		 <div data-role="tableCell" data-celType="lightGray">16GB</div>
		 <div data-role="tableCell" data-celType="black">R595</div>
	</div>
	<div data-role="tableRow" data-rowType="productInfo">
		<p><span class="yellow">Plus</span>Cover+16GbIpodNano+ScreenGaurd+R100iStoreVoucher</p>
	</div>
	<div data-role="tableRow" data-rowType="productColorOptions">
	  <div data-role="tableCell" data-celType="colorOption">SpaceGray</div>
	  <div data-role="tableCell" data-celType="colorOption">Gold</div> 
	   <div data-role="tableCell" data-celType="colorOption">Silver</div>
	</div>
</div>