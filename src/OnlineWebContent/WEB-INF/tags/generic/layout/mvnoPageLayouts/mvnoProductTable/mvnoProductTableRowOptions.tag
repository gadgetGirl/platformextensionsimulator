<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoPageLayouts" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts"%>

<%@ attribute required="false"  name="productDetail"%>
<%@ attribute required="false"  name="productAmount"%> 
<%@ attribute required="false"  name="mvnoProductSelectedClassName"%>	

 	

<%-- add all mvno Product Options Description and Amount --%>	
<div data-role="mvnoProductTableCell" >
	<span class="mvnoProductLightGray">${productDetail}</span>
	<span class="mvnoProductBlack ${mvnoProductSelectedClassName}">${productAmount}</span>
</div>

