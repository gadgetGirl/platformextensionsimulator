<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoProductTable" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoProductTable"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false"  name="detailHeader"%>
<%@ attribute required="false"  name="productExtraInfo"%>

<span class="mvnoProductSelectName">${detailHeader}</span>
  
<div data-role="mvnoProductSelectTableRow">
	<%-- forEach start --%>
	<fnb.generic.mvnoProductTable:mvnoProductTableRowExtraInfo mvnoProductInfoOrange="true"  mvnoProductExtraInfoValue="Plus" />
	<fnb.generic.mvnoProductTable:mvnoProductTableRowExtraInfo mvnoProductInfoOrange="false"  mvnoProductExtraInfoValue="Cover + " />
	<fnb.generic.mvnoProductTable:mvnoProductTableRowExtraInfo mvnoProductInfoOrange="false"  mvnoProductExtraInfoValue="16GbIpodNano + " />
	<fnb.generic.mvnoProductTable:mvnoProductTableRowExtraInfo mvnoProductInfoOrange="false"  mvnoProductExtraInfoValue="ScreenGaurd + " />
	<fnb.generic.mvnoProductTable:mvnoProductTableRowExtraInfo mvnoProductInfoOrange="false"  mvnoProductExtraInfoValue="R100iStoreVoucher" />
	<%-- forEach ends --%>
</div>

<div data-role="mvnoShopProductImages">
	<%-- forEach start --%>
	<fnb.generic.mvnoProductTable:mvnoSelectedProductImg imgSrc="#" />
	<fnb.generic.mvnoProductTable:mvnoSelectedProductImg imgSrc="#" />
	<fnb.generic.mvnoProductTable:mvnoSelectedProductImg imgSrc="#" />
	<fnb.generic.mvnoProductTable:mvnoSelectedProductImg imgSrc="#" />
	<fnb.generic.mvnoProductTable:mvnoSelectedProductImg imgSrc="#" />
	<fnb.generic.mvnoProductTable:mvnoSelectedProductImg imgSrc="#" />
	<%-- forEach ends --%>
</div>

<div data-role="mvnoProductSelectCheckOptions">
  	<ul class="mvnoProductSelectCheckOptionsList"><!-- optionHeading -->
	  	<%-- forEach Starts --%>
	  	<fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionHeading="Which device do you want?" />
	  	<fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="16 GB" />
	    <fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="32 GB" />
	    <fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="64 GB" />
	    <fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="128 GB" />
		<%-- forEach Ends --%>
	</ul>
 	<ul class="mvnoProductSelectCheckOptionsList"><!-- optionHeading -->
	  	<%-- forEach Starts --%>
	  	<fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionHeading="Which device do you want?" />
	  	<fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="16 GB" />
	    <fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="32 GB" />
	    <fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="64 GB" />
	    <fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="128 GB" />
		<%-- forEach Ends --%>
	</ul>
  	<ul class="mvnoProductSelectCheckOptionsList"><!-- optionHeading -->
	  	<%-- forEach Starts --%>
	  	<fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionHeading="Which device do you want?" />
	  	<fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="16 GB" />
	    <fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="32 GB" />
	    <fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="64 GB" />
	    <fnb.generic.mvnoProductTable:mvnoSelectedProductCheckOptionList optionId="checkbox" optionLabel="128 GB" />
		<%-- forEach Ends --%>
	</ul>
</div>