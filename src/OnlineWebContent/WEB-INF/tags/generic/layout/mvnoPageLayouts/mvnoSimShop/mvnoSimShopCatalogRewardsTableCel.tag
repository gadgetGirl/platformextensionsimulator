<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false"  name="mvnoSimCatalogRewardsTableCellData"%>
<%@ attribute required="false"  name="className"%>

<c:if test="${not empty className }">
	<c:set var="className">
		class="${className }"	
	</c:set>
</c:if>
       

<div data-role="mvnoSimCatalogRewardsTableCell" ${className }>
		${mvnoSimCatalogRewardsTableCellData}
</div>