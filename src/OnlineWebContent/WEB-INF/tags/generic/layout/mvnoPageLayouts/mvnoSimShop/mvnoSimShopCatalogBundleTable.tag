<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoSimShop" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoSimShop"%>

<%-- All attributes for Sim Shop Catalog --%>
<%@ attribute required="false"  name="mvnoSimCatalogPackageTableData"%>
<%@ attribute required="false"  name="mvnoSimShopCatalogBundleTableHead" %>
   
<c:if test="${not empty mvnoSimShopCatalogBundleTableHead }" >
	<c:set var="mvnoSimShopCatalogBundleTableHead">
		<span class="mvnoSimCatalogBundleRatesSubHeading">${mvnoSimShopCatalogBundleTableHead}</span>
   </c:set>
</c:if>



<div data-role="mvnoSimCatalogBundleRatesTable">
${mvnoSimShopCatalogBundleTableHead}
<%--<c:forEach items="${mvnoSimCatalogPackageTableData}" var="mvnoSimCatalogPackageTableRow">--%>
	    <div data-role="mvnoSimCatalogBundleRatesTableRow">
	      <%-- <c:forEach items="mvnoSimCatalogPackageTableRow" var="mvnoSimCatalogPackageTableColData" --%>
	        <fnb.generic.mvnoSimShop:mvnoSimCatalogBundleRatesTableCel mvnoSimCatalogBundleRatesTableCellData="Bronze"/>
	        <fnb.generic.mvnoSimShop:mvnoSimCatalogBundleRatesTableCel mvnoSimCatalogBundleRatesTableCellData="R 0.95" />
	        <fnb.generic.mvnoSimShop:mvnoSimCatalogBundleRatesTableCel mvnoSimCatalogBundleRatesTableCellData="R 0.21" />
			<fnb.generic.mvnoSimShop:mvnoSimCatalogBundleRatesTableCel mvnoSimCatalogBundleRatesTableCellData="R 0.33"/>
	    <%--</c:forEach> --%>
	    </div>
 </div>