<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoSimShop" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoSimShop"%>

<%-- All attributes for Sim Shop Catalog --%>
<%@ attribute required="false"  name="mvnoSimCatalogOptionHead"%>
<%@ attribute required="false"  name="mvnoSimCatalogOptionDesc"%>
<%@ attribute required="false"  name="mvnoSimCatalogOptionMonthOption"%>
<%@ attribute required="false"  name="mvnoSimCatalogPackageData"%>
<%@ attribute required="false"  name="mvnoSimCatalogBundleRatesTable"%>
<%@ attribute required="false"  name="mvnoSimCatalogRewardsTable"%>



<%-- Build mvnoSimCatalogOptionTag --%>
<div data-role="mvnoSimCatalogOptionTag">
	<div data-role="mvnoSimShopTableRow">
		<span data-role="mvnoSimCatalogOptionHead">${mvnoSimCatalogOptionHead}</span>
	</div>
	<div data-role="mvnoSimShopTableRow" class="mvnoSimShopRowLines" >
		<span data-role="mvnoSimCatalogOptionDesc" class="mvnoSimShopDesc">${mvnoSimCatalogOptionDesc}</span>
		<span data-role="mvnoSimCatalogOptionMonthOption" class="mvnoSimShopMonthOption">
			<img src="#" class="mvnoSimShopDescImg">${mvnoSimCatalogOptionMonthOption}
		</span>
	</div>
	
	<jsp:doBody />
	
</div>









