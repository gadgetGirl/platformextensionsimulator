<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoSimShop" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoSimShop"%>

<%-- All attributes for Sim Shop Catalog --%>
<%@ attribute required="false"  name="mvnoSimCatalogPackageTableData"%>

<div data-role="mvnoSimCatalogPackageTable">
	<%--<c:forEach items="${mvnoSimCatalogPackageTableData}" var="mvnoSimCatalogPackageTableRow">--%>
      <div data-role="mvnoSimCatalogPackageTableRow">
      <%-- <c:forEach items="mvnoSimCatalogPackageTableRow" var="mvnoSimCatalogPackageTableColData" --%>
      	<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableCellData="FNB Bronze"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableCellData="R100" className="mvmvnoSimCatalogPackageTableColPrice"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100min" mvnoSimCatalogPackageTableColrate="R0.60 per MB"/>
		<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100MB" mvnoSimCatalogPackageTableColrate="R0.50 per MB"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100SMS" mvnoSimCatalogPackageTableColrate="R0.60 per SMS"/>     
		<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageButtonLabel="Add To Basket"/>      
       <%--</c:forEach> --%>
      </div>  
     <%--end foreach --%>
     	<%--<c:forEach items="${mvnoSimCatalogPackageTableData}" var="mvnoSimCatalogPackageTableRow">--%>
      <div data-role="mvnoSimCatalogPackageTableRow" class="mvnoSimShopPackageTableRowColorGray">
      <%-- <c:forEach items="mvnoSimCatalogPackageTableRow" var="mvnoSimCatalogPackageTableColData" --%>
      	<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableCellData="FNB Bronze"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableCellData="R100" className="mvmvnoSimCatalogPackageTableColPrice" />
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100min" mvnoSimCatalogPackageTableColrate="R0.60 per MB"/>
		<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100MB" mvnoSimCatalogPackageTableColrate="R0.50 per MB"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100SMS" mvnoSimCatalogPackageTableColrate="R0.60 per SMS"/>     
		<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageButtonLabel="Add To Basket"/>        
       <%--</c:forEach> --%>
      </div>  
     <%--end foreach --%>   
      	<%--<c:forEach items="${mvnoSimCatalogPackageTableData}" var="mvnoSimCatalogPackageTableRow">--%>
      <div data-role="mvnoSimCatalogPackageTableRow">
      <%-- <c:forEach items="mvnoSimCatalogPackageTableRow" var="mvnoSimCatalogPackageTableColData" --%>
      	<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableCellData="FNB Bronze"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableCellData="R100" className="mvmvnoSimCatalogPackageTableColPrice"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100min" mvnoSimCatalogPackageTableColrate="R0.60 per MB"/>
		<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100MB" mvnoSimCatalogPackageTableColrate="R0.50 per MB"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100SMS" mvnoSimCatalogPackageTableColrate="R0.60 per SMS"/>     
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageButtonLabel="Add To Basket"/>  
       <%--</c:forEach> --%>
      </div>  
     <%--end foreach --%>   
      	<%--<c:forEach items="${mvnoSimCatalogPackageTableData}" var="mvnoSimCatalogPackageTableRow">--%>
      <div data-role="mvnoSimCatalogPackageTableRow" class="mvnoSimShopPackageTableRowColorGray">
      <%-- <c:forEach items="mvnoSimCatalogPackageTableRow" var="mvnoSimCatalogPackageTableColData" --%>
      	<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableCellData="FNB Bronze"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableCellData="R100" className="mvmvnoSimCatalogPackageTableColPrice"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100min" mvnoSimCatalogPackageTableColrate="R0.60 per MB"/>
		<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100MB" mvnoSimCatalogPackageTableColrate="R0.50 per MB"/>
        <fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageTableColGet="100SMS" mvnoSimCatalogPackageTableColrate="R0.60 per SMS"/>     
       	<fnb.generic.mvnoSimShop:mvnoSimShopPackageCel mvnoSimCatalogPackageButtonLabel="Add To Basket"/>         
       <%--</c:forEach> --%>
      </div>  
     <%--end foreach --%>   
</div>