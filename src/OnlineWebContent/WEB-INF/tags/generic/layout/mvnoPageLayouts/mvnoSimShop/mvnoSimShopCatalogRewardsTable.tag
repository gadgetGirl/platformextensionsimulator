<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoSimShop" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoSimShop"%>

<%-- All attributes for Sim Shop Catalog --%>
<%@ attribute required="false"  name="mvnoSimShopCatalogRewardsTableData"%>
<%@ attribute required="false"  name="mvnoSimShopCatalogRewardsTableHead" %>
   
<c:if test="${not empty mvnoSimShopCatalogRewardsTableHead }" >
	<c:set var="mvnoSimShopCatalogRewardsTableHead">
		<span class="mvnoSimCatalogRewardsHeading">${mvnoSimShopCatalogRewardsTableHead}</span>
   </c:set>
</c:if>



<div data-role="mvnoSimCatalogRewardsTable">
${mvnoSimShopCatalogRewardsTableHead}
<%--<c:forEach items="${mvnoSimCatalogPackageTableData}" var="mvnoSimCatalogPackageTableRow">--%>
	    <div data-role="mvnoSimCatalogRewardsTableRow">
	      <%-- <c:forEach items="mvnoSimCatalogPackageTableRow" var="mvnoSimCatalogPackageTableColData" --%>
	        <fnb.generic.mvnoSimShop:mvnoSimShopCatalogRewardsTableCel mvnoSimCatalogRewardsTableCellData="Talk For"/>
	        <fnb.generic.mvnoSimShop:mvnoSimShopCatalogRewardsTableCel mvnoSimCatalogRewardsTableCellData="20 min" />
	        <fnb.generic.mvnoSimShop:mvnoSimShopCatalogRewardsTableCel mvnoSimCatalogRewardsTableCellData="You get" />
			<fnb.generic.mvnoSimShop:mvnoSimShopCatalogRewardsTableCel mvnoSimCatalogRewardsTableCellData="200 eb or 20min"/>
	    <%--</c:forEach> --%>
	    </div>
 </div>