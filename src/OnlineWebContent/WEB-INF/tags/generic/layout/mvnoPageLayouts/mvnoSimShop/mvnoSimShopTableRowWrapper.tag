<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false"  name="className"%>

<C:if test="${not empty className }">
	<c:set var="className">
		class="${className }"	
	</c:set>
</C:if>


<%-- wrapper for mvnoSimShopTableRow --%>
<div data-role="mvnoSimShopTableRow" ${className}>
	<jsp:doBody />
</div>
