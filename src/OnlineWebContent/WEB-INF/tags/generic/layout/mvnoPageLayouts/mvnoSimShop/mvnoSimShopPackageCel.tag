<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="false"  name="mvnoSimCatalogPackageTableCellData"%>
<%@ attribute required="false"  name="className"%>
<%@ attribute required="false"  name="mvnoSimCatalogPackageTableColGet"%>
<%@ attribute required="false"  name="mvnoSimCatalogPackageTableColrate"%>
<%@ attribute required="flase"  name="mvnoSimCatalogPackageButtonLabel" %>

<c:if test="${not empty className }">
	<c:set var="className">
		class="${className }"	
	</c:set>
</c:if>
       
 <c:if test="${empty mvnoSimCatalogPackageTableCellData }">
 	<c:if test="${not empty mvnoSimCatalogPackageTableColGet || not empty mvnoSimCatalogPackageTableColrate}">
		<c:set var="mvnoSimCatalogPackageTableCellData">
			<span class="mvnoSimCatalogPackageTableCellColGet">${mvnoSimCatalogPackageTableColGet }</span>	
			<span class="mvnoSimCatalogPackageTableCellColColrate">${mvnoSimCatalogPackageTableColrate }</span>
		</c:set>
	</c:if>
 </c:if>
 
 <c:if test="${not empty  mvnoSimCatalogPackageButtonLabel }">
  	<c:set var="mvnoSimCatalogPackageButtonLabel">
		 <div data-role="mvnoSimCatalogPackageButtonLabel"><span class="mvnoSimCatalogPackageButtonImg"></span>  ${mvnoSimCatalogPackageButtonLabel }</div>
	</c:set>
 </c:if>

<div data-role="mvnoSimCatalogPackageTableCell" ${className }>
		${mvnoSimCatalogPackageTableCellData}
		${mvnoSimCatalogPackageButtonLabel }
</div>
