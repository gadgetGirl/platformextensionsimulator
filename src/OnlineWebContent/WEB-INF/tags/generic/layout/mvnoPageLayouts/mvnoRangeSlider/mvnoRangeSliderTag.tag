<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="true"  name="totalSnapPoints" %>
<%@ attribute required="true"  name="sliderName" %>
<%@ attribute required="true"  name="snapPointSelected" %>
<%@ attribute required="true"  name="snapPointSelectedValue" %>
<%@ attribute required="false" name="sliderColor" %>
<%@ attribute required="true"  name="sliderId"%>
	
	
<c:if test="${ not empty snapPointSelected  && not empty totalSnapPoints}">

	<%-- preset progress class  --%>	
	<c:set var="progressClass">
		class="sp_${snapPointSelected}_snapPoints${totalSnapPoints}"
	</c:set>

 	<%-- preset btn class --%>
	<c:set var="btnClass">
		class="btn_${snapPointSelected}_snapPoints${totalSnapPoints}"
	</c:set>
</c:if>
<c:if test="${ empty snapPointSelected  && empty totalSnapPoints}">
	<%-- preset progress class  --%>	
	<c:set var="progressClass">
		class="sp_${snapPointSelected}_snapPoints${totalSnapPoints}"
	</c:set>

 	<%-- preset btn class --%>
	<c:set var="btnClass">
		class="btn_${snapPointSelected}_snapPoints${totalSnapPoints}"
	</c:set>

</c:if>

<c:if test="${not empty sliderColor }">
	<c:set var="sliderColor">
		style="background-color:${sliderColor };"
	</c:set>


</c:if>

	
	<div data-role="sliderContainer" id="sliderContainer">
	    <span data-role="sliderProgress" id="sliderProgress"  ${progressClass }  ${sliderColor } ><span data-role="sliderBtn" data-sliderId="${sliderId }" id="sliderBtn" ${btnClass } ></span></span>  
	    
	</div>
	<input name="${sliderName }" value="${snapPointSelectedValue }" type="hidden">
