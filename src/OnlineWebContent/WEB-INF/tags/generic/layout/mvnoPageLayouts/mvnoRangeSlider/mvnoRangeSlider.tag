<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.mvnoLayout" tagdir="/WEB-INF/tags/generic/layout/mvnoPageLayouts/mvnoRangeSlider"%>


<%@ attribute required="false"  name="totalSnapPoints" %>
<%@ attribute required="false"  name="sliderName" %>
<%@ attribute required="false"  name="snapPointSelected" %>
<%@ attribute required="flase"  name="sliderColor"%>
<%@ attribute required="true"  name="sliderId" %>

  
<%--  <c:set var="totalSnapPoints">4</c:set> --%>
<%--  <c:set var="snapPointSelected">3</c:set> --%>
<%-- <c:set var="sliderColor">pink</c:set> --%>
 
<fnb.generic.mvnoLayout:mvnoRangeSliderWrapper sliderName="${sliderName }"  sliderId="${sliderId }" > 
	
	<div id="snapPoints" >
	<%-- Start for each --%>
		<%-- build script for now that will loop for totalSnapPoints times --%>
		<c:forEach var="i" begin="1" end="${totalSnapPoints }">
   			<fnb.generic.mvnoLayout:mvnoSnapPoints id="pointer${i }" totalSnapPoints="${totalSnapPoints}" value="${i }00" label="${i }00mb" sliderId="${sliderId }" snapPointNr="${i }" />
		</c:forEach>
	<%-- ForEach ends --%>		
    </div>
    
    <%-- preset slider and slider btn to selected snap-point --%>
    <fnb.generic.mvnoLayout:mvnoRangeSliderTag sliderName="${customSimPackageVoice }" sliderId="${sliderId }" snapPointSelected="${snapPointSelected }" totalSnapPoints="${totalSnapPoints }" snapPointSelectedValue="500mb" sliderColor="${sliderColor }" />

</fnb.generic.mvnoLayout:mvnoRangeSliderWrapper>

