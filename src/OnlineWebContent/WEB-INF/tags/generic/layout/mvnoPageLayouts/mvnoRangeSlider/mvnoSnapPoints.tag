<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@ attribute required="false"  name="id" %>
<%@ attribute required="true"   name="totalSnapPoints" %>
<%@ attribute required="false"  name="value" %>
<%@ attribute required="false"  name="label" %>
<%@ attribute required="true"   name="sliderId" %>
<%@ attribute required="true"   name="snapPointNr" %>

<%-- set id html for snapPoint --%>
<c:if test="${not empty id }">
	<c:set var="id">
		id="${id }" 
	</c:set>
</c:if>


<%-- class="point ${ }"  --%>
<%-- preset the classes by using the totalSnapPoint  --%>
<c:choose>
	<c:when test="${not empty totalSnapPoints}">
		<c:if test="${totalSnapPoints <= 10 }">
			<c:set var="className">
				class="point snapPoints${totalSnapPoints}"
			</c:set>
		</c:if>
	</c:when>
	<c:otherwise>
		<c:set var="className">
			class="point"
		</c:set>
	</c:otherwise>
</c:choose>

<c:if test="${not empty value }">
	<c:set var="value">
		 data-value="${value }"
	</c:set>
</c:if>

<c:set var="snapPointPercentage"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${ 100 / totalSnapPoints}" /></c:set>

<c:set var="dataPercentage">	data-percentage="${snapPointPercentage * snapPointNr }"</c:set>


<%-- need total nr of Snappoint  --%>  
	
<span data-sliderId="${sliderId }"  ${id }  ${className } ${value }   ${dataPercentage } >${label}</span>
	
	
	


