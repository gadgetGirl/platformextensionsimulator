<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="true" name="sliderName" %>
<%@ attribute required="true" name="sliderId" %>


<%-- wrapper for rangeSlider --%>
<div data-role="rangeSliderContainer${sliderId}" id="rangeSliderContainer${sliderId}"  data-sliderName="${sliderName }">
 <jsp:doBody />



<%-- <input name="${sliderName}" type="hidden" value=""> --%>
</div>