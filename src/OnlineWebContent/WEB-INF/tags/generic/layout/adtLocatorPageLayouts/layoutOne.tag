<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.frame.pageMenu" tagdir="/WEB-INF/tags/generic/frame/pageMenu"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="layoutOneA_className"%>
<%@ attribute required="false" name="layoutOneB_className"%>
<%@ attribute required="false" name="layoutOneC_className"%>
<%@ attribute required="false" name="subSection" description="place holder"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageImage" description="graphic used to as a background on the page"%>
<%@ attribute required="false" name="pageMenu" description="content of the pageMenu"%>
<%@ attribute required="false" name="heading" description="file location to page menu"%>
<%@ attribute required="false" name="subHeading" description="file location to page menu"%>

<%--
	layoutOne:  3 coloum layout 
		layoutOneA: page thumbnail & menu
		layoutOneB: heading, subheading and content
		layoutOneC: place holder
 --%>
 
 <c:set var="actionMenu" value="${browserInfo.capabilities['fnb_is_mobile']?pageMenu:''}" />
 
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">
 
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/adtLocatorLayouts/layoutOne.css" />
	
	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
	<div id="${id}" data-role="layoutOne">
		<div data-role="layoutOneInner">
		
 			<c:if test='${!browserInfo.capabilities["fnb_is_mobile"]}'>
				<div class="${layoutOneA_className}" data-role="layoutOneA">
					<div data-role="layoutOneThumbnail">
						<c:if test="${not empty pageThumbnail}"><img src="${session_contextPath}${pageThumbnail}"></img></c:if>
					</div>
					<fnb.generic.frame.pageMenu:pageMenu >${pageMenu}</fnb.generic.frame.pageMenu:pageMenu>
				</div>
			</c:if>
			
			<div class="${layoutOneB_className}" data-role="layoutOneB" >
				<div data-role="layoutOneHeadings" >
					<h1>${heading}</h1>
					<h2>${subHeading}</h2>
				</div>
				<div data-role="layoutOneContent" >
					<jsp:doBody />
				</div>
			</div>
			<div class="${layoutOneC_className}" data-role="layoutOneC" >
				${subSection}
			</div>
		</div>
	</div>
</fnb.generic.frame:coreContainer>