<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="className" %>


<style>
	[data-role='eziInner'] {
		background: #009999;
	}
	#finishImage{
		margin: 20px 0 10px 20px;
	}
	#eziFinishMessage{
		margin: 20px 20px 20px 20px;
		text-align: right;
		color: #FFF;
		border-bottom: 2px solid #FFF;
	}
	#finishHeader {
		font-size: 24px;
		margin: 0 0 5px 0;
		padding: 0;
		
	}
	.finishSubHeader {
		font-size: 14px;
		margin: 0 0 20px 0;
		padding: 0;
	}
</style>

<c:set var="className">
	<c:choose>
		<c:when test="${not empty className}">
        class="${className}"
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
</c:set>



<div id="eziCoreContainer" ${className}>
	<img id="finishImage" src="00Assets/skins/00/images/widgets/thankYou.png" />
	<div id="eziFinishMessage">
		<h2 id="finishHeader">Congratulations on your new Cheque Account</h2>
		<p class="finishSubHeader">Your account number is 345678974132</p>
		<p class="finishSubHeader">We will send you an email with everything you need to know about this product.</p>
	</div>
	<jsp:doBody />
</div>
<div id="eziPannelButtonsWrapper">
    <div class="eziWrapperInner \">
         <div id="eziPannelButtons">
         </div>
    </div>
</div> 

