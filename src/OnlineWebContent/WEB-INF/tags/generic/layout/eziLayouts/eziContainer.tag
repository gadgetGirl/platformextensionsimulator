<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="className" %>
<c:set var="className">
	<c:choose>
		<c:when test="${not empty className}">
        class="${className}"
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
</c:set>

<div id="eziCoreContainer" ${className}>
   <jsp:doBody />
    
</div>
<div id="eziPannelButtonsWrapper">
    <div class="eziWrapperInner">
         <div id="eziPannelButtons">
         </div>
    </div>
</div> 

