<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="subHeader" %>
<%@ attribute required="false" name="className" %>

<c:if test="${not empty subHeader && empty className }">
<c:set var="subHeaderClass" value=" subHeader"/>
</c:if>

<c:if test="${empty subHeader &&  not empty className}">
<c:set var="subHeaderClass" value=" ${className}"/>
</c:if>

<c:if test="${not empty subHeader &&  not empty className}">
<c:set var="subHeaderClass" value=" subHeader ${className }"/>
</c:if>


<div id="eziHeaderDiv" class="${subHeaderClass}">
   <jsp:doBody />
</div>