<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.banking.frame" tagdir="/WEB-INF/tags/banking/frame"%>
<%@ taglib prefix="fnb.banking.markup" tagdir="/WEB-INF/tags/banking/markup"%>
<%@ taglib prefix="fnb.banking.forms" tagdir="/WEB-INF/tags/banking/forms"%>

<%-- required attributes --%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="true" name="skin"%>
<%@ attribute required="true" name="isZBI"%>

<%-- optional attributes --%>
<%@ attribute required="false" name="standin" type="java.lang.Boolean"%>
<%@ attribute required="false" name="hasRewards" type="java.lang.Boolean"%>

<%-- optional attributes --%>
<%@ attribute required="false" name="bankingLayoutOneA"%>
<%@ attribute required="false" name="bankingLayoutOneA_className"%>
<%@ attribute required="false" name="bankingLayoutOneB"%>
<%@ attribute required="false" name="bankingLayoutOneB_className"%>
<%@ attribute required="false" name="bankingLayoutOneB1"%>
<%@ attribute required="false" name="bankingLayoutOneB2"%>
<%@ attribute required="false" name="bankingLayoutOneB3"%>
<%@ attribute required="false" name="bankingLayoutOneC"%>
<%@ attribute required="false" name="bankingLayoutOneC_className"%>

<%@ attribute required="false" name="bankingLayoutOneD1"%>
<%@ attribute required="false" name="bankingLayoutOneD2"%>
 
<c:set var="actionMenu" value="${browserInfo.capabilities['fnb_is_mobile']?pageMenu:''}" />
 
 <%-- Build webroot --%>
<c:set var="webroot">
 	<fnb.banking.markup:p>
		<img src="03images/base/getcommunications/webroot/WebrootLogo.png" width="181" height="27" alt="Webroot Logo" class="webrootLogo" />
		<fnb.banking.markup:h className="biggerFont2" level="3" >As an Online Banking client you can download this free software programme that can protect you from malicious attacks such as phishing and prevent spyware.</fnb.banking.markup:h>
		<fnb.banking.markup:p className="biggerFont2 bolderFont">Download Webroot now. It's free*!</fnb.banking.markup:p>
		<fnb.banking.markup:p className="fontBold">Note:</fnb.banking.markup:p>
		<fnb.banking.markup:p>This software is for PC use only. For comprehensive protection you need to purchase an anti-virus software programme that best suits you.</fnb.banking.markup:p>
		<fnb.banking.forms:dataLink url="http://anywhere.webrootcloudav.com/zerol/wsafnbidsn.exe" className="webrootDownloadBtt amber1">Download</fnb.banking.forms:dataLink>
	</fnb.banking.markup:p>
</c:set>

 <%-- Build rewards container --%>
<c:set var="hasRewardsBanner">
	<fnb.banking.markup:h level="2" className="extraLarge" >eBucks Rewards</fnb.banking.markup:h>
	<fnb.banking.markup:p id="hasRewardsRewardsContainer" />
</c:set>

<%-- Build login status --%>
<c:set var="loginStatus">
	<div id="loginStatus"></div>
</c:set>
 
<fnb.banking.frame:bankingPageWrapper>
 
	<fnb.banking.frame:import type="css" link="/banking/00Assets/skins/00/css/layouts/newsPageLayouts/bankingLayoutOne.css" />
	<fnb.banking.frame:import type="css" link="/banking/00Assets/skins/00/css/layouts/newsPageLayouts/bankingLayoutOne-ie8.css" ie8="true" />
	
	<c:if test="${standin}">
		<div id="standInMessage">
			Due to system maintenance, certain <span onclick="fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '/banking/Controller?nav=standIn.StandInMessagePopup')">functionality is currently unavailable</span>.
		</div>
	</c:if>
	<div id="${id}" class="bankingLayoutOne">
		<div class="bankingLayoutOneInner">
		
			
			<div class="bankingLayoutOneA ${bankingLayoutOneA_className}">
				<div class="bankingLayoutOneAInner" >
					<div id="bannerContainer" width="60" className="borderRightWhite borderTopWhite">
						${bankingLayoutOneA}
					</div>
				</div>
			</div>
			
			<div class="bankingLayoutOneB ${bankingLayoutOneB_className}" >
				<div class="bankingLayoutOneBInner" >
					<div class="bankingLayoutOneB1">
						${bankingLayoutOneB1}
					</div>

					<div class="bankingLayoutOneB2">
						${bankingLayoutOneB2}
					</div>
					
					<c:if test="${hasRewards}">
						<div class="bankingLayoutOneB3">
							${bankingLayoutOneB3}
						</div>
					</c:if>
					
				</div>
			</div>
			
			<div class="bankingLayoutOneC ${bankingLayoutOneC_className}" >
				<div class="bankingLayoutOneCInner" >
					<c:choose>
						<c:when test="${hasRewards}">
							<div id="rewardsContainer">
								${bankingLayoutOneC}
							</div>
						</c:when>
						<c:otherwise>
							<div class="bankingLayoutOneC1" >
								${bankingLayoutOneB3}
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		
		<div class="bankingLayoutOneD">
			<div class="bankingLayoutOneDInner">
				<div class="bankingLayoutOneD1">
					${bankingLayoutOneD1}
				</div>
				<div class="bankingLayoutOneD2">
					${bankingLayoutOneD2}
				</div>
			</div>
		</div>
	</div>
	
</fnb.banking.frame:bankingPageWrapper>

