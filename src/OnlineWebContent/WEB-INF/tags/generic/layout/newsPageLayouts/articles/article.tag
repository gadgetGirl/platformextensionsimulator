<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/fn.tld" prefix="fn"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>
<%@ attribute required="false" name="timeStamp"%>
<%@ attribute required="false" name="href"%>
<%@ attribute required="false" name="description"%>
<fnb.generic.markup:p> 
	<a class="articleLink" href="${fn:contains(href,'://')?'':session_contextPath}${href}">${description}</a><br/>  
	<span class="articleTimestamp">${timeStamp}</span>
</fnb.generic.markup:p>