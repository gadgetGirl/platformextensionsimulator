<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.frame.pageMenu" tagdir="/WEB-INF/tags/generic/frame/pageMenu"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="newsLayoutOneA_className"%>
<%@ attribute required="false" name="newsLayoutOneB_className"%>
<%@ attribute required="false" name="subSection" description="place holder"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageImage" description="graphic used to as a background on the page"%>
<%@ attribute required="false" name="pageMenu" description="content of the pageMenu"%>
<%@ attribute required="false" name="heading" description="heading of the page"%>

<%--
	newsLayoutOne:  2 coloum layout 
		newsLayoutOneA: page thumbnail & menu
		newsLayoutOneB: content
 --%>
 
 <c:set var="actionMenu" value="${browserInfo.capabilities['fnb_is_mobile']?pageMenu:''}" />
 
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">
 
	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/newsPageLayouts/layoutOne.css" />
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/newsPageLayouts/articles/article.css" />
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/newsPageLayouts/ie8.css" ie8="true" />
	<div id="${id}" data-role="newsLayoutOne">
		<div data-role="newsLayoutOneInner">
			
 			<c:if test='${!browserInfo.capabilities["fnb_is_mobile"]}'>
				<div class="${newsLayoutOneA_className}" data-role="newsLayoutOneA">
					<div data-role="newsLayoutOneThumbnail">
						<c:if test="${not empty pageThumbnail}"><img src="${session_contextPath}${pageThumbnail}"></img></c:if>
					</div>
					<fnb.generic.frame.pageMenu:pageMenu >${pageMenu}</fnb.generic.frame.pageMenu:pageMenu>
				</div>
			</c:if>
			
			<div class="${newsLayoutOneB_className}" data-role="newsLayoutOneB" >
				<div data-role="newsLayoutOneHeadings" >
					<h1>${heading}</h1>
					<h2>${subHeading}</h2>
				</div>
				<div data-role="newsLayoutOneContent" >
					<jsp:doBody />
				</div>
			</div>
			<div class="${newsLayoutOneC_className}" data-role="newsLayoutOneC" >
				<div data-role="newsLayoutOneCInner" >
					${subSection}
				</div>
			</div>
		</div>
	</div>
</fnb.generic.frame:coreContainer>