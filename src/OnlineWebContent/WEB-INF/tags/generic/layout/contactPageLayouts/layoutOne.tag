<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.generic.frame.pageMenu" tagdir="/WEB-INF/tags/generic/frame/pageMenu"%>
<%@ taglib prefix="fnb.generic.utils.banners" tagdir="/WEB-INF/tags/generic/utils/banners"%>
<%@ taglib prefix="fnb.generic.widgets.waysToApply" tagdir="/WEB-INF/tags/generic/widgets/waysToApply"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="layoutThreeA_className"%>
<%@ attribute required="false" name="layoutThreeB_className"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageMenu" description="file location to page menu"%>
<%@ attribute required="false" name="heading" description="file location to page menu"%>
<%@ attribute required="false" name="subHeading" description="file location to page menu"%>
<%@ attribute required="false" name="bannerGroup" description="Banner group to be displayed on page"%>
<%@ attribute required="false" name="whatYouNeed" description="what you need in order to apply"%>
<%@ attribute required="false" name="apply" description="what you can do to apply"%>
<%@ attribute required="false" name="contact" description="contact information related to applying"%>

<%--
	layoutThree:  2 coloum layout 
		layoutThreeA: page thumbnail & menu
		layoutThreeB: heading, subheading and content
 --%>
 
 <c:set var="actionMenu" value="${browserInfo.capabilities['fnb_is_mobile']?pageMenu:''}" />
 
 <fnb.generic.frame:coreContainer mainMenu="${mainMenu}" subMenu="${subMenu}" selectedTopTab="${selectedTopTab}">
 
	<fnb.generic.frame:actionMenu>${actionMenu}</fnb.generic.frame:actionMenu>
	
	<fnb.generic.frame:import type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/contactPageLayouts/layoutOne.css" />
	<fnb.generic.frame:import ie8="true" type="css" link="${session_contextPath}/00Assets/skins/00/css/layouts/contactPageLayouts/ie8.css" />
	<div id="${id}" data-role="layoutThree">
		<div data-role="layoutThreeInner">
			
 			<c:if test='${!browserInfo.capabilities["fnb_is_mobile"]}'>
				<div class="${layoutThreeA_className}" data-role="layoutThreeA">
					<div data-role="layoutThreeThumbnail">
						<c:if test="${not empty pageThumbnail}"><img src="${session_contextPath}${pageThumbnail}"></img></c:if>
					</div>
					<fnb.generic.frame.pageMenu:pageMenu >${pageMenu}</fnb.generic.frame.pageMenu:pageMenu>
				</div>
			</c:if>
			
			<div class="${layoutThreeB_className}" data-role="layoutThreeB" >
				<div data-role="layoutThreeHeadings" >
					<h1>${heading}</h1>
					<h2>${subHeading}</h2>
				</div>
				<div data-role="layoutThreeContent" >
					<jsp:doBody />
				</div>
			</div>
				
		</div>
	</div>
</fnb.generic.frame:coreContainer>