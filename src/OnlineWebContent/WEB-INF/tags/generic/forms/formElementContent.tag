<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="customType"%>
<div class="formElContent">
	<div class="formElContentInner clearfix"><jsp:doBody /></div>
</div>
