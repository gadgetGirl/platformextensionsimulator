<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="maxItems"%>
<%@ attribute required="false" name="dataModel"%>

<div class="duplicatorOutput">	
	<div data-role="formDuplicator" id="${id}" data-model="${dataModel}" data-maxItems="${maxItems}">
		<div>	
		<jsp:doBody />
		    <div data-model="${id}" class='formDuplicatorRemoveButton'>			  
		           <span style='color:#cdcddc;'>Remove</span>
		    </div>	
		  
		</div>		
	</div>	
	<div class="formDuplicatorContainer">
		<div data-model="${id}" id="addDuplicate" class="formDuplicatorAddDupliator">Add Duplicate</div>	
		<div id="duplicateCounter" class="formDuplicatorCounter">0</div>
		  <div class="updateModel">
		    	<div>Submit</div>
		    </div>
	</div>
</div>
