<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="true" name="id"%>
<%@ attribute required="true" name="className"%>


<div class="simpleformElWrapper<c:if test="${not empty className}"> ${className}</c:if> clearfix" id="simpleFormEl_${id}">
	<jsp:doBody />	
</div>