<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="buttonLabel"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="value"%>
<%@ attribute required="false" name="placeholder"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="action"%>
<%@ attribute required="false" name="formToSubmit"%>
<%@ attribute required="false" name="target"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
<c:if test="${empty bodyContent}">
<c:set var="noBodyContent" value=" noBodyContent"/>
</c:if>
<c:set var="value">
	<c:choose>
		<c:when test="${not empty placeholder}">
			${placeholder}
		</c:when>
		<c:otherwise>
			${value}
		</c:otherwise>
	</c:choose>
</c:set>
					
<fnb.generic.forms:formElementWrapper toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}" optional="${optional}">
	<fnb.generic.forms:formElementContainer customType="${customType}" id="${id}" className="${className}${noBodycontent}" inlineError="${inlineError}" noteContent="${noteContent}">
		<input id="${id}" class="input validationTarget" name="${id}" data-type="text"<c:if test="${not empty placeholder}"> data-placeholderIs="true" data-placeholder="${placeholder}"</c:if><c:if test="${not empty value}"> value="${value}"</c:if><c:if test="${not empty disabled}"> disabled="true"</c:if>/>
	</fnb.generic.forms:formElementContainer>	
	<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
		<fnb.generic.forms:dataButton
			event="validateAndSubmit"
			id="searchSubmit"
			label="${empty buttonLabel?'Search':buttonLabel}"
			dataTarget="#${formToSubmit}"
			target="${empty target?'#pageContent':target}"
			targetElement="#pageContent" 
			url="${action}"/>
		${bodyContent}
	</fnb.generic.forms:formElementContent>
</fnb.generic.forms:formElementWrapper>