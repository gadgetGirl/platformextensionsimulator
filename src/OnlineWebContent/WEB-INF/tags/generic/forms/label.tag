<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="role"%>
<%@ attribute required="false" name="toolTipContent"%>

<div class="labelWrapper"><div<c:if test="${ not empty role}"> data-role="${role}" class="${role}"</c:if>>${label}</div><c:if test="${not empty toolTipContent}"><fnb.generic.forms:toolTip content="${toolTipContent}" id="toolTip_${id}"/></c:if></div>