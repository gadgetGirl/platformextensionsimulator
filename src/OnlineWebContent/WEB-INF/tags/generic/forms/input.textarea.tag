<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="rows"%>
<%@ attribute required="false" name="characterCount"%>
<%@ attribute required="false" name="leftInnerLabel"%>
<%@ attribute required="false" name="rightInnerLabel"%>
<%@ attribute required="false" name="leftInnerLabelSize"%>
<%@ attribute required="false" name="rightInnerLabelSize"%>
<c:if test="${empty rows}">
	<c:set var="rows" value="2"/>
</c:if>
<c:if test="${not empty characterCount}">
	<c:set var="charCount">
 		data-charCount="${characterCount}" 
	</c:set>
</c:if>
<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
<c:if test="${empty bodyContent}">
<c:set var="noBodyContent" value=" noBodyContent"/>
</c:if>
<fnb.generic.forms:formElementWrapper optional="${optional}" toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}">
	<fnb.generic.forms:formElementContainer leftInnerLabel="${leftInnerLabel}" rightInnerLabel="${rightInnerLabel}" leftInnerLabelSize="${leftInnerLabelSize}" rightInnerLabelSize="${rightInnerLabelSize}" elType="textArea" customType="${customType}" id="${id}" className="${className}${noBodyContent}" inlineError="${inlineError}" noteContent="${noteContent}">
		<textarea name="${id}" id="${id}" class="validationTarget" cols="1" data-charactersUsed="0" rows="${rows}" ${charCount}></textarea>
		<c:if test='${not empty characterCount}'>
			<div class="characterCount">${characterCount} characters remaining</div>
		</c:if>
	</fnb.generic.forms:formElementContainer>	
	<c:if test="${not empty bodyContent}">
		<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
			${bodyContent}
		</fnb.generic.forms:formElementContent>
	</c:if>
</fnb.generic.forms:formElementWrapper>


