<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="fullWidthItems" rtexprvalue="true"%>
<%@ attribute required="false" name="selectedValue" rtexprvalue="true"%>
<%@ attribute required="false" name="disabled"%>

<%@ attribute required="false" name="bean" rtexprvalue="true" type="java.util.List"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<% jspContext.setAttribute("FE_CheckboxGroupBean", bean); %>

<fnb.generic.forms:formElementWrapper optional="${optional}" toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}">
	<fnb.generic.forms:formElementContainer customType="${className}" id="${id}" className="${className}" inlineError="${inlineError}" noteContent="${noteContent}">
		<div class="checkboxGroupWrapper"<c:if test="${not empty disabled}"> data-disabled="true"</c:if>>
			<c:forEach items="${FE_CheckboxGroupBean}" var="item" varStatus="status">
				<c:set var="isSelected">
					<c:choose>
						<c:when test="${item.selected}">
							true
						</c:when>
						<c:when test="${item.value==selectedValue}">
							true
						</c:when>
						<c:otherwise>
							
						</c:otherwise>
					</c:choose>
				</c:set>
				<c:set var="isDisabled">
					<c:choose>
						<c:when test="${item.disabled}">
							true
						</c:when>
						<c:otherwise>
							
						</c:otherwise>
					</c:choose>
				</c:set>
				<div id="checkBox_${item.id}" data-role="checkBox"<c:if test="${not empty isSelected}"> data-state="checked"</c:if><c:if test="${not empty isDisabled}"> data-disabled="true"</c:if>>
					<input id="${item.id}" type="checkbox"<c:if test="${not empty isSelected}"> checked="checked"</c:if> class="checkbox Hhide">
				</div>
			</c:forEach>
		</div>
	</fnb.generic.forms:formElementContainer>	
	<c:if test="${not empty bodyContent}">
		<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
			${bodyContent}
		</fnb.generic.forms:formElementContent>
	</c:if>
</fnb.generic.forms:formElementWrapper>


