<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- 
	 close types: 'closeAll', will close all open accordion items and only open the item clicked. 
				  'leaveOpen' will leave all open accordion item open and only close the item if the item clicked was open.
--%>

<%@ attribute required="false" name="closetype" %>

<%-- set default close type if empty --%>
<c:if test="${empty closetype }">
	<c:set var="closetype" value="leaveOpen" />
</c:if> 

<div class="accordianContainer" id="accordianContainer" data-closetype="${closetype }">
 	<jsp:doBody />
</div>