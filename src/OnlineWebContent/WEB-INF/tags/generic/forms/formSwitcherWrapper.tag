<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="true" name="maxItems"%>
<%@ attribute required="false" name="dataModel"%>

	<div data-role="formSwitcherWrapper" class="" id="${id}" data-model="${dataModel}" data-maxItems="${maxItems}">		
		<jsp:doBody />		 
	</div>	

