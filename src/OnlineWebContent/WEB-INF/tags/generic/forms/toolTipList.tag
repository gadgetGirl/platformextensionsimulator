<%@ attribute required="true" name="id"%>
<%@ attribute required="true" name="className"%>

<div id="${id}Message" class="toolTipMessage ${className}">
	<div class="tableHeaderTooltipListButtonsWrapper">
		<jsp:doBody />
	</div>
</div>