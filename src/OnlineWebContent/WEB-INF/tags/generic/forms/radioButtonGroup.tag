<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="false" name="rightAlignTooltip" %>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="fullWidthItems" rtexprvalue="true"%>
<%@ attribute required="false" name="selectedValue" rtexprvalue="true"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="true" name="name" rtexprvalue="true"%>
<%@ attribute required="false" name="defaultSelected" %>

<%@ attribute required="false" name="bean" rtexprvalue="true" type="java.util.List"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<c:set var="required" value="${optional eq 'true'?'false':'true'}" />

<% jspContext.setAttribute("FE_RadioButtonGroupBean", bean); %>

<c:set var="itemWidth">
	<c:choose>
		<c:when test="${not empty fullWidthItems}">
			width100
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${fn:length(FE_RadioButtonGroupBean)==1}">
					width100
				</c:when>
				<c:when test="${fn:length(FE_RadioButtonGroupBean)==2}">
					width50
				</c:when>
				<c:when test="${fn:length(FE_RadioButtonGroupBean)==3}">
					width33
				</c:when>
				<c:when test="${fn:length(FE_RadioButtonGroupBean)==4}">
					width25
				</c:when>
				<c:when test="${fn:length(FE_RadioButtonGroupBean)==5}">
					width20
				</c:when>
				<c:when test="${fn:length(FE_RadioButtonGroupBean)==6}">
					width16
				</c:when>
			</c:choose>
		</c:otherwise>
	</c:choose>
</c:set>
	
<fnb.generic.forms:formElementWrapper optional="${optional}" toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}" rightAlignTooltip="${rightAlignTooltip }">
	<fnb.generic.forms:formElementContainer customType="${className}" id="${id}" className="${className}" inlineError="${inlineError}" noteContent="${noteContent}">
		<div class="radioGroupWrapper"<c:if test="${not empty disabled}"> data-disabled="true"</c:if>>
			<c:forEach items="${FE_RadioButtonGroupBean}" var="item" varStatus="status">
				<c:set var="isSelected">
					<c:choose>
						<c:when test="${item.selected}">
							true
						</c:when>
						<c:when test="${item.value==selectedValue}">
							true
						</c:when>
						<c:otherwise>
							
						</c:otherwise>
					</c:choose>
				</c:set>
				<c:set var="isDefaultSelected">
					<c:choose>
						<c:when test="${item.defaultSelected}">
							true
						</c:when>
						<c:otherwise>
							false
						</c:otherwise>
					</c:choose>
				</c:set>
				
				<span class="radioButton ${itemWidth}"<c:if test="${not empty isSelected}"> data-state="checked"</c:if> <c:if test="${isDefaultSelected == 'true'}"> data-defaultSelected="true"</c:if> id="${item.id}">
					<span class="radioButtonLabel" >${item.description}</span>
					<input type="radio" class="validationTarget Hhide" name="${name}" id="radioInput${item.id}" value="${item.value}"<c:if test="${not empty isSelected}"> checked="checked"</c:if> data-visible="false">
				</span>
			</c:forEach>
		</div>
	</fnb.generic.forms:formElementContainer>	
	<c:if test="${not empty bodyContent}">
		<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
			${bodyContent}
		</fnb.generic.forms:formElementContent>
	</c:if>
</fnb.generic.forms:formElementWrapper>


