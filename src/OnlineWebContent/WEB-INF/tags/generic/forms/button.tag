<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute required="false" name="id"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="role"%>
<%--
Developer: Donovan
Show and hide any elements by selector
--%>
<%@ attribute required="false" name="show"%>
<%@ attribute required="false" name="hide"%>

<button id="${id}" name="${id}"<c:if test="${not empty role}"> data-role="${role}" class="${role}"</c:if><c:if test="${not empty show}"> data-show="${show}"</c:if><c:if test="${not empty hide}"> data-hide="${hide}"</c:if> type="button">${label}</button>