<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="value"%>
<%@ attribute required="false" name="placeholder"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="leftInnerLabel"%>
<%@ attribute required="false" name="rightInnerLabel"%>
<%@ attribute required="false" name="leftInnerLabelSize"%>
<%@ attribute required="false" name="rightInnerLabelSize"%>
<%@ attribute required="false" name="simple"%>
<%@ attribute required="false" name="hidden"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<c:if test="${empty bodyContent}">
	<c:set var="noBodyContent" value="noBodyContent"/>
	<c:set var="noBody" value="true"/>
</c:if>

<c:set var="value">
	<c:choose>
		<c:when test="${not empty placeholder}">
			${placeholder}
		</c:when>
		<c:otherwise>
			${value}
		</c:otherwise>
	</c:choose>
</c:set>

<c:set var="dataType" value="simpleDate"/>
	
<c:set var="input">

	<input id="${id}" name="${id}" class="class validationTarget" data-type="${dataType}"<c:if test="${not empty placeholder}"> data-placeholderIs="true" data-placeholder="${placeholder}"</c:if><c:if test="${not empty value}"> value="${value}"</c:if><c:if test="${not empty disabled}"> disabled="true"</c:if>/>

</c:set>

<c:choose>
	<c:when test="${empty simple}">
	
		<fnb.generic.forms:formElementWrapper toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}" optional="${optional}">
			<fnb.generic.forms:formElementContainer leftInnerLabel="${leftInnerLabel}" rightInnerLabel="${rightInnerLabel}" leftInnerLabelSize="${leftInnerLabelSize}" rightInnerLabelSize="${rightInnerLabelSize}" customType="${customType}" id="${id}" className="${className}${noBodyContent}" inlineError="${inlineError}" noteContent="${noteContent}">
				
			${input}
			
			</fnb.generic.forms:formElementContainer>	
			<c:if test="${not empty bodyContent}">
				<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
					${bodyContent}
				</fnb.generic.forms:formElementContent>
			</c:if>
		</fnb.generic.forms:formElementWrapper>
	
	</c:when>
	<c:otherwise>
		${input}
	</c:otherwise>
</c:choose>