<%-- Developer: Donovan --%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>

<%-- Tag attributes --%>
<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="heading"%>

<%-- Default Note heading --%>
<c:set var="defaultHeading" value="Note: " />

<%-- Create heading --%>
<c:set var="headingValue">
	<c:choose>
		<c:when test="${empty heading}">
			${defaultHeading}
		</c:when>
		<c:otherwise>
			${heading}
		</c:otherwise>
	</c:choose>
</c:set>

<%-- Markup starts here --%>
<fnb.generic.markup:div className="noteWrapper ${className}" id="${className}">
	<fnb.generic.markup:p className="noteHeading">${headingValue}</fnb.generic.markup:p><jsp:doBody />
</fnb.generic.markup:div>
