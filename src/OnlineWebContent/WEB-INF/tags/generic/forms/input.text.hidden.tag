<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="value"%>

<input id="${id}" name="${id}" value="${value }" type="hidden"/>