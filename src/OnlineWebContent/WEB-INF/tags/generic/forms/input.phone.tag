<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="codeValue"%>
<%@ attribute required="false" name="numberValue"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="areaCodeInput"%>
<%@ attribute required="false" name="numberIdOverride"%>
<%@ attribute required="false" name="codeIdOverride"%>
<%-- the below hack is for sales as they need a different pair of ID's on the back end --%>
<c:choose>
	<c:when test="${empty numberIdOverride}">
		<c:set var="numberID">${id}Number</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="numberID">${numberIdOverride}</c:set>
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${empty codeIdOverride}">
		<c:set var="codeID">${id}Code</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="codeID">${codeIdOverride}</c:set>
	</c:otherwise>
</c:choose>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
<c:if test="${empty bodyContent}">
<c:set var="noBodyContent" value=" noBodyContent"/>
</c:if>
<c:set var="value">
	<c:choose>
		<c:when test="${not empty placeholder}">
			${placeholder}
		</c:when>
		<c:otherwise>
			${value}
		</c:otherwise>
	</c:choose>
</c:set>
			
<fnb.generic.forms:formElementWrapper toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}" optional="${optional}">
	<fnb.generic.forms:formElementContainer customType="${className}" id="${id}" className="${className}${noBodyContent}" inlineError="${inlineError}" noteContent="${noteContent}">
		<c:choose>
			<c:when test="${areaCodeInput ne 'false'}">
				<input class="input validationTarget width30" id="${codeID}" name="${codeID}" data-type="number" <c:if test="${empty codeValue}">placeholder="Code"</c:if><c:if test="${not empty codeValue}"> value="${codeValue}"</c:if>/>
				<input class="input validationTarget width70" id="${numberID}" name="${numberID}" data-type="number" <c:if test="${empty numberValue}">placeholder="Number"</c:if><c:if test="${not empty numberValue}"> value="${numberValue}"</c:if><c:if test="${not empty disabled}"> disabled="true"</c:if>/>
			</c:when>
			<c:otherwise>
				<input class="input validationTarget width100" id="${id}" name="${id}" data-type="number"<c:if test="${empty numberValue}">placeholder="Number"</c:if><c:if test="${not empty numberValue}"> value="${numberValue}"</c:if><c:if test="${not empty disabled}"> disabled="true"</c:if>/>
			</c:otherwise>
		</c:choose>
	</fnb.generic.forms:formElementContainer>	
	<c:if test="${not empty bodyContent}">
		<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
			${bodyContent}
		</fnb.generic.forms:formElementContent>
	</c:if>
</fnb.generic.forms:formElementWrapper>