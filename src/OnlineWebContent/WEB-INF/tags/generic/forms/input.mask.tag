<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="value"%>
<%@ attribute required="false" name="placeholder"%>
<%@ attribute required="false" name="isNumber"%>
<%@ attribute required="false" name="decimals"%>
<%@ attribute required="false" name="decimalsPoints"%>
<%@ attribute required="false" name="thousandsSeperator"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="leftInnerLabel"%>
<%@ attribute required="false" name="rightInnerLabel"%>
<%@ attribute required="false" name="leftInnerLabelSize"%>
<%@ attribute required="false" name="rightInnerLabelSize"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
<c:if test="${empty bodyContent}">
<c:set var="noBodyContent" value=" noBodyContent"/>
</c:if>
<c:set var="value">
	<c:choose>
		<c:when test="${not empty placeholder}">
			${placeholder}
		</c:when>
		<c:otherwise>
			${value}
		</c:otherwise>
	</c:choose>
</c:set>
			
<fnb.generic.forms:formElementWrapper toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}" optional="${optional}">
	<fnb.generic.forms:formElementContainer leftInnerLabel="${leftInnerLabel}" rightInnerLabel="${rightInnerLabel}" leftInnerLabelSize="${leftInnerLabelSize}" rightInnerLabelSize="${rightInnerLabelSize}" customType="${className}" id="${id}" className="${className}${noBodyContent}" inlineError="${inlineError}" noteContent="${noteContent}">
		<input id="${id}" name="${id}" class="input validationTarget" data-type="masked"<c:if test="${not empty placeholder}"> data-placeholderIs="true" data-placeholder="${placeholder}"</c:if><c:if test="${not empty value}"> value="${value}"</c:if><c:if test="${not empty isNumber}"> data-isNumber="${isNumber}"</c:if><c:if test="${not empty decimals}"> data-decimals="${decimals}"</c:if><c:if test="${not empty decimalsPoints}"> data-decimalsPoints="${decimalsPoints}"</c:if><c:if test="${not empty thousandsSeperator}"> data-thousandsSeperator="${thousandsSeperator}"</c:if><c:if test="${not empty disabled}"> disabled="true"</c:if>/>
	</fnb.generic.forms:formElementContainer>	
	<c:if test="${not empty bodyContent}">
		<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
			${bodyContent}
		</fnb.generic.forms:formElementContent>
	</c:if>
</fnb.generic.forms:formElementWrapper>

<c:if test="${not empty value}">
	<script class="pageScript" type="text/javascript">
		//Format value if value is set
		var currentInput = fnb.hyperion.$('#${id}');
		fnb.hyperion.forms.input.mask.value(currentInput,'${value}')
	</script>
</c:if>