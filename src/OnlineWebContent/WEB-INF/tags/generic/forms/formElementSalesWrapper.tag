<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="true" name="id"%>
<%@ attribute required="true" name="className"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="formElLabelInnerClassName"%>
<%@ attribute required="false" name="isSelected" %>


<c:set var="theBody">
	<jsp:doBody />	
</c:set>


<div class="formElWrapper<c:if test="${not empty className}"> ${className}</c:if>${labelRightClass} clearfix"<c:if test="${empty optional||optional=='false'}"> data-required="true"</c:if> id="formEl_${id}">
	<div class="formElLabel">
		<div class="formElLabelInner ${formElLabelInnerClassName }">${label}</div> 
		<c:if test="${optional == true}">
			<div class="formElOptionalLabelInner">(Optional)</div>
		</c:if>
	</div>
	<%-- test if bodyContent is not empty if not add to formElWrapper div --%>
	<c:if test="${not empty theBody}">
		<div class="formElContentWrapper">
				<div id="01" class="radioOptionsWrapper">
		 			<div class="radioOption" data-state="uncheked" data-value="blue">${checklabelLeft }</div>
		 			<div class="radioOption checked" data-state="uncheked" data-value="red">${checklabelRight }</div>
		 				<input id="${item.id}" type="checkbox"<c:if test="${not empty isSelected}"> checked="checked"</c:if> class="checkbox Hhide">
		 		</div>	
		</div>
	</c:if>
	<%-- test if tooltipContent is not empty if not add to formElWrapper div  --%>
    <c:if test="${not empty toolTipContent}">
		<div class="formElTooltipContainer">  
			<fnb.generic.forms:toolTip content="${toolTipContent}" id="toolTip_${id}"/>
		</div>
	</c:if>
</div>