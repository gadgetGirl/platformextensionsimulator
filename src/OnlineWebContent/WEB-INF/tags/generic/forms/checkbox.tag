<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="name"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="wrapperClassName"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="selected"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="unCheckedValue" %>
<%@ attribute required="false" name="checkedValue" %>
<%@ attribute required="false" name="labelPosition"%>

<c:if test="${ empty name}">
	<c:set var="name"> name="${id }"</c:set>	
</c:if>

<c:if test="${not empty unCheckedValue}">
	<c:set var="unCheckedValue">data-unCheckedValue="${unCheckedValue}"</c:set>	
</c:if>

<c:if test="${not empty checkedValue}">
	<c:set var="checkedValue">data-checkedValue="${checkedValue}"</c:set>	
</c:if>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
<c:if test="${empty bodyContent}">
<c:set var="noBodyContent" value=" noBodyContent"/>
</c:if>
<fnb.generic.forms:formElementWrapper labelPosition="${labelPosition}" optional="${optional}" toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}">
	
	<fnb.generic.forms:formElementContainer customType="${className}" id="${id}" className="${className}${noBodyContent}" inlineError="${inlineError}" noteContent="${noteContent}">
		<div id="checkBox_${id}" class="checkBox"<c:if test="${not empty selected}"> data-state="checked"</c:if><c:if test="${not empty disabled}"> data-disabled="true"</c:if>>
			<input id="${id}"<c:if test="${not empty name}"> ${name}</c:if><c:if test="${not empty checkedValue}"> ${checkedValue}</c:if><c:if test="${not empty unCheckedValue}"> ${unCheckedValue}</c:if> type="checkbox"<c:if test="${not empty selected}"> checked="checked"</c:if> class="Hhide">
		</div>
		
	</fnb.generic.forms:formElementContainer>
		
	<c:if test="${not empty bodyContent && empty labelPosition}">
		<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
			${bodyContent}
		</fnb.generic.forms:formElementContent>
	</c:if>
</fnb.generic.forms:formElementWrapper>