<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ tag import="java.util.*"%>
<%@ tag import="fnb.online.fe.core.tags.utils.BeanUtilities"%>
<%@ tag import="fnb.online.fe.core.tags.beans.checkboxgroup.*"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="true" rtexprvalue="true"%>
<%@ attribute name="value" required="true" rtexprvalue="true"%>
<%@ attribute name="selected" required="false" rtexprvalue="true"%>
<%
{
	CheckboxItem ch = new CheckboxItem();
	ch.setDescription(label);
	ch.setValue(value);
	ch.setSelected(Boolean.valueOf(selected));
	ch.setId(id);
	
	List items = (List) BeanUtilities.getFieldValue(getParent(),"bean");
	if(items == null){
		items = new ArrayList();
	}
	items.add(ch);
	BeanUtilities.setFieldValue(getParent(),"bean",items);
}  

%>