<%--Developer: Donovan --%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%--Import frame tags--%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%--Tag attributes--%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="value"%>

<%--Parse string to date object--%>
<c:set var="today" value="<%=new java.util.Date()%>" />

<%--Parse string to date object--%>
<fmt:parseDate value="${value}" var="parsedDateValue" pattern="yyyy-MM-dd" />

<%--Format date object--%>
<fmt:formatDate var="formattedDate" type="both" value="${parsedDateValue}" pattern="E d MMM yyyy"/>

<%--Split date into parts--%>
<fmt:formatDate var="formattedTodayDate" type="both" value="${today}" pattern="E d MMM yyyy"/>

<%--Test if date is today--%>
<c:choose>
	<c:when test="${formattedDate == formattedTodayDate}">
		
		<%--Set label values--%>
		<c:set var="topLabelValue" value="Today"/>
		<c:set var="bottomLabelValue" value=""/>
		
	</c:when>
	<c:otherwise>
	
		<%--Split date string--%>
		<c:set var="dateParts" value="${fn:split(formattedDate, ' ')}" />
		
		<%--Set label values--%>
		<c:set var="topLabelValue" value="${dateParts[0]} ${dateParts[1]}"/>
		<c:set var="bottomLabelValue" value="${dateParts[2]} ${dateParts[3]}"/>
		
	</c:otherwise>
</c:choose>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:formElementWrapper toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}">
	
	<fnb.generic.forms:formElementContainer customType="${className}" id="${id}" className="${className}" inlineError="${inlineError}" noteContent="${noteContent}">
		
		<div id="datePicker" data-months="18" data-startBefore="8">
			<input type="hidden" name="${id}" data-value="${value}" value="${value}">
			<span class="datePickerSelected">
				<span class="datePickerLabelTop">${topLabelValue}</span>
				<span class="datePickerLabelBottom">${bottomLabelValue}</span>
			</span>
			<span class="datePickerCarat"></span>
		</div>
		
	</fnb.generic.forms:formElementContainer>	
	
	<c:if test="${not empty bodyContent}">
		<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
			${bodyContent}
		</fnb.generic.forms:formElementContent>
	</c:if>
	
</fnb.generic.forms:formElementWrapper>