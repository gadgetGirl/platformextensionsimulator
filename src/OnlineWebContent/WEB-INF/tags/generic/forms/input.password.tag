<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="value"%>
<%@ attribute required="false" name="placeholder"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="maxlength"%> 
<%@ attribute required="false" name="numericOnly"%>
<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
<c:if test="${empty bodyContent}">
<c:set var="noBodyContent" value=" noBodyContent"/>
</c:if>
<c:set var="value">
	<c:choose>
		<c:when test="${not empty placeholder}">
			${placeholder}
		</c:when>
		<c:otherwise>
			${value}
		</c:otherwise>
	</c:choose>
</c:set>
			
<fnb.generic.forms:formElementWrapper toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}" optional="${optional}">
	<fnb.generic.forms:formElementContainer customType="${className}" id="${id}" className="${className}${noBodyContent}" inlineError="${inlineError}" noteContent="${noteContent}">
		<input id="${id}" name="${id}" class="input validationTarget" type="password"  
		<c:choose>
			<c:when test="${numericOnly eq true}">data-type="number"</c:when>
			<c:otherwise>data-type="password"</c:otherwise>
		</c:choose>
		<c:if test="${not empty placeholder}"> data-placeholderIs="true" data-placeholder="${placeholder}"</c:if><c:if test="${not empty value}"> value="${value}"</c:if><c:if test="${not empty disabled}"> disabled="true"</c:if><c:if test="${not empty maxlength}"> maxlength="${maxlength}"</c:if> />
	</fnb.generic.forms:formElementContainer>	
	<c:if test="${not empty bodyContent}">
		<fnb.generic.forms:formElementContent className="${className}"  id="${id}" customType="${customType}">
			${bodyContent}
		</fnb.generic.forms:formElementContent>
	</c:if>
</fnb.generic.forms:formElementWrapper>