 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ attribute required="false" name="id"%>
 <%@ attribute required="false" name="className"%>
 <%@ attribute required="false" name="event"%>
 <%@ attribute required="false" name="url"%>
 <%@ attribute required="true" name="label"%>
 <%@ attribute required="false" name="target"%>
 <%@ attribute required="false" name="targetElement"%>
 <%@ attribute required="false" name="dataTarget"%>

 <%@ attribute required="false" name="clearHtmlTemplates"%>
 <%@ attribute required="false" name="clearPageModuleObject"%>
 <%@ attribute required="false" name="clearPageEventsArray"%>
 <%@ attribute required="false" name="clearPageTemplatesArray"%>
 
 <c:set var="clearHtmlTemplates" value="${not empty clearHtmlTemplates ? (clearHtmlTemplates eq 'true'? true : false) : false}"/>
 <c:set var="clearPageModuleObject" value="${not empty clearHtmlTemplates ? (clearHtmlTemplates eq 'true'? true : false) : false}"/>
 <c:set var="clearPageEventsArray" value="${not empty clearHtmlTemplates ? (clearHtmlTemplates eq 'true'? true : false) : false}"/>
 <c:set var="clearPageTemplatesArray" value="${not empty clearHtmlTemplates ? (clearHtmlTemplates eq 'true'? true : false) : true}"/>
<c:set var="settings">[{"event" : "${not empty event ? event : ''}", "url" : "${not empty url ? url: ''}", "target" : "${not empty target ? target: '#pageContent'}", "targetElement" : "${not empty targetElement ? targetElement: '#pageContent'}", "dataTarget" : "${not empty dataTarget ? dataTarget: null}"}]</c:set>

<span type="button" id="${id}" data-role="submitButton" class="submitButton ${className}" data-settings='${settings}'>${label}</span>