<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute required="false" name="id"%>
<%@ attribute required="true" name="label" description="label of the option"%>
<%@ attribute required="true" name="role" description="role of the button, example: select, submit, apply etc"%>

<style>
	.optionSelectContainer {
		position: relative;
		margin: 10px 0;
	}
	.optionSelect {
		position: absolute;
		right: 0;
	}
</style>

<div class="optionSelectContainer">
	${label}
	<button id="${id}" class="optionSelect <c:if test='${ not empty role}'>${role}</c:if>" <c:if test="${ not empty role}"> data-role="${role}" </c:if> type="button" >${role}</button>
</div>

