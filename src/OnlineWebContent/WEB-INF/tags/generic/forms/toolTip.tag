<%@ attribute required="false" name="content"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="customType"%>

<div id="${id}" class="toolTipIcon">
	<div id="${id}Message" class="toolTipMessage">
		${content}
	</div>
</div>