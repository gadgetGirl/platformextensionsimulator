<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
<fnb.generic.forms:formElementWrapper optional="${optional}" toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}">
	<fnb.generic.forms:formElementContainer customType="${className}" id="${id}" className="${className}" inlineError="${inlineError}" noteContent="${noteContent}">
		<%--Your input goes here--%>
		Input goes here
	</fnb.generic.forms:formElementContainer>	
	<c:if test="${not empty bodyContent}">
		<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
			${bodyContent}
		</fnb.generic.forms:formElementContent>
	</c:if>
</fnb.generic.forms:formElementWrapper>


