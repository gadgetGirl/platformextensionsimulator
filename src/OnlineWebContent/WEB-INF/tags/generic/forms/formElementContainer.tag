<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="elType"%>
<%@ attribute required="false" name="leftInnerLabel"%>
<%@ attribute required="false" name="rightInnerLabel"%>
<%@ attribute required="false" name="leftInnerLabelSize"%>
<%@ attribute required="false" name="rightInnerLabelSize"%>
<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
<c:if test="${not empty leftInnerLabel}">
	<c:set var="hasLeftInnerLabel" value="hasLeftInnerLabel"/>
	<c:choose>
		<c:when test="${not empty leftInnerLabelSize}">
			<c:set var="leftAdjustSize">leftAdjustSize${leftLabelInnerSize}</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="leftAdjustSize" value="leftAdjustSize1"></c:set>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${not empty rightInnerLabel}">
	<c:set var="hasRightInnerLabel" value="hasRightInnerLabel"/>
	<c:choose>
		<c:when test="${not empty rightInnerLabelSize}">
			<c:set var="rightAdjustSize">rightAdjustSize${leftLabelInnerSize}</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="rightAdjustSize" value="rightAdjustSize1"></c:set>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${not empty elType}"><c:set var="elType" value="container_${elType}"/></c:if>
<c:if test="${not empty className}">
	<c:set var="className">class='${elType} ${className}  ${leftAdjustSize}'</c:set>
</c:if>
<div class="formElContainer" ${className}>
		<c:choose>
			<c:when test="${not empty rightInnerLabel}">
				<div class="formElContainerInner" class="${rightAdjustSize}">${bodyContent}<div data-role="formElRightLabel">${rightInnerLabel}</div></div>
			</c:when>
			<c:when test="${not empty leftInnerLabel}">
				<div class="formElContainerInner" class="${leftAdjustSize}">${bodyContent}<div data-role="formElLeftLabel">${leftInnerLabel}</div></div>
			</c:when>
			<c:when test="${not empty leftInnerLabel && not empty leftInnerLabel}">
				<div class="formElContainerInner">${bodyContent}</div>
			</c:when>
			<c:otherwise>
				${bodyContent}
			</c:otherwise>
		</c:choose>
		<c:if test="${not empty noteContent}">
			<div id="inlineNote_${id}" class="formInlineNote">${noteContent}</div>
		</c:if>
		<c:if test="${not empty inlineError}">
			 <div id="inlineError_${id}" class="formInlineError">${inlineError}</div>
		</c:if>
</div>
