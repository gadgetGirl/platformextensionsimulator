<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%--  <%@ attribute name="checkboxRadioBean" required="true"%> --%>
<%@ attribute name="testData" required="false"%>

<%-- use test data remove loop use attributes --%>
<%@ attribute name="id" required="false"%> 
<%@ attribute name="name" required="false"%>
<%@ attribute name="label" required="false"%>
<%@ attribute name="toolTipContent" required="false"%>

<%-- <p>Test data :  ${testData}</p> --%>

		<%-- loop trough radio buttons group 
		<c:forEach items="${checkboxRadioBean}" var="buttonGroup" varStatus="tabCount" > --%>
		
			<fnb.generic.forms:radioButtonGroup id="${id }" name="${id }" className="checkRadioButtons" label="${label }" toolTipContent="${toolTipContent }" rightAlignTooltip="true">
				
				<%-- build loop in here radio buttons --%> 
						<fnb.generic.forms:radioButtonItem label="Yes" value="D0012" selected="false" defaultSelected="false"/>
						<fnb.generic.forms:radioButtonItem label="No" value="no" selected="true" defaultSelected="true"/>
			
			</fnb.generic.forms:radioButtonGroup>
			
		<%-- </c:forEach>  --%>
