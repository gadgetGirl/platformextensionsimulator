<%@ attribute required="false" name="content"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="customType"%>
<div id="${id}" class="toolTip">
${content}
</div>