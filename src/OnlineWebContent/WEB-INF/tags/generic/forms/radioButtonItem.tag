<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ tag import="java.util.*"%>
<%@ tag import="fnb.online.fe.core.tags.utils.BeanUtilities"%>
<%@ tag import="fnb.online.fe.core.tags.beans.radiogroup.*"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="true" rtexprvalue="true"%>
<%@ attribute name="value" required="true" rtexprvalue="true"%>
<%@ attribute name="selected" required="false" rtexprvalue="true"%>
<%@ attribute name="defaultSelected" required="false" rtexprvalue="true"%>
<%
{
	RadioItem ri = new RadioItem();
	ri.setDescription(label);
	ri.setValue(value);
	ri.setSelected(Boolean.valueOf(selected));
	ri.setDefaultSelected(Boolean.valueOf(defaultSelected));
	ri.setId(id);
		
	List items = (List) BeanUtilities.getFieldValue(getParent(),"bean");
	if(items == null){
		items = new ArrayList();
	}
	items.add(ri);
	BeanUtilities.setFieldValue(getParent(),"bean",items);
}  

%>