<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="value"%>
<%@ attribute required="false" name="placeholder"%>
<%@ attribute required="false" name="disabled"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
					
<input id="${id}" name="${id}" data-role="inputSimple" data-type="text"<c:if test="${not empty placeholder}"> placeholder="${placeholder}"</c:if><c:if test="${not empty value}"> value="${value}"</c:if><c:if test="${not empty disabled}"> disabled="true"</c:if><c:if test="${optional ne 'true'}"> data-required="true"</c:if>/>