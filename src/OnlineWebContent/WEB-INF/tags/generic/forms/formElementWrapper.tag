<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="true" name="id"%>
<%@ attribute required="true" name="className"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="false" name="rightAlignTooltip"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="labelPosition"%>
<%@ attribute required="false" name="formElLabelInnerClassName"%>
<c:set var="theBody">
	<jsp:doBody />	
</c:set>

<c:choose>
	<c:when test="${labelPosition == 'right'}">
		<c:set var="labelRight">${theBody}</c:set>
		<c:set var="labelRightClass" value=" labelPositionRight "/>
	</c:when>
	<c:otherwise>
		<c:set var="labelLeft">${theBody}</c:set>
		<c:set var="labelRightClass" value=""/>
	</c:otherwise>
</c:choose>

	<c:if test="${rightAlignTooltip == 'true' }">
		<c:set var="rightAlignTooltip">
			<div class="rigthToolTipwrapper">
				<fnb.generic.forms:toolTip content="${toolTipContent}" id="toolTip_${id}"/>
			</div>
		</c:set>
		<c:set var="toolTipContent" value="" />
	</c:if>

<div class="formElWrapper<c:if test="${not empty className}"> ${className}</c:if>${labelRightClass} clearfix"<c:if test="${empty optional||optional=='false'}"> data-required="true"</c:if> id="formEl_${id}">
	 ${labelRight}
	<div class="formElLabel">
		<div class="formElLabelInner ${formElLabelInnerClassName }">${label}</div> 
		<c:if test="${not empty toolTipContent}">
			<fnb.generic.forms:toolTip content="${toolTipContent}" id="toolTip_${id}"/>
		</c:if>
		<c:if test="${optional == true}">
			<div class="formElOptionalLabelInner">(Optional)</div>
		</c:if>
	</div>
	${labelLeft}
	${rightAlignTooltip }
</div>
