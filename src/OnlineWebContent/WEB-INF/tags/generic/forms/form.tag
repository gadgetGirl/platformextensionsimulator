<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ attribute name="name" required="true" rtexprvalue="true"%>
<%@ attribute name="action" required="true" rtexprvalue="true"%>
<%@ attribute name="method" required="true" rtexprvalue="true"%>
<%@ attribute name="onsubmit" required="false" rtexprvalue="true"%>
<%@ attribute name="redirectjsp" required="true" rtexprvalue="true"%>
<%@ attribute name="nav" required="false" rtexprvalue="true"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>
<div id="${name}" class="formWrapper"<c:if test="${not empty action}"> data-action="${action}"</c:if><c:if test="${not empty method}"> data-method="${method}"</c:if><c:if test="${not empty nav}"> data-nav="${nav}"</c:if>>
	${bodyContent}
</div>