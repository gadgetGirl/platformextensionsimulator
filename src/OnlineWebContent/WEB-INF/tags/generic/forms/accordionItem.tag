<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute required="false" name="isDefault" %>
<%@ attribute required="false" name="itemState" %>		
<%@ attribute required="true" name="itemHeading" %>			

<c:set var="itemSettings" value="" />

<c:choose>
	<c:when test="${isDefault == 'true' || itemState == 'open' }">
		<c:set var="itemSettings"> class="accGroupItemWrapper" data-state="open" </c:set>
		<c:set var="contentShow"> accShow </c:set>
	</c:when>
	<c:otherwise>
		<c:set var="itemSettings"> class="accGroupItemWrapper" data-state="closed" </c:set>
		<c:set var="contentShow" value="" /> 
	</c:otherwise>
</c:choose>		
		
		<span ${itemSettings }>
			<div class="accGroupItemHeader">${itemHeading }<span class="accArrow accArrow-down"></span></div>
			<div class="accGroupItemContentWrapper ${contentShow } ">
				<div class="accGroupItemContent">
					<jsp:doBody />
				</div>
			</div>
		</span>