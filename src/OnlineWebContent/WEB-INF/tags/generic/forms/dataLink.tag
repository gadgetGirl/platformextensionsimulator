<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="event"%>
<%@ attribute required="false" name="url"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="target"%>
<%@ attribute required="false" name="targetElement"%>
<%@ attribute required="false" name="dataTarget"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="onClick"%>
<c:set var="body">${label}<jsp:doBody /></c:set>

<a id="${id}" type="button" data-role="dataButton" data-settings='[{"event": "${event}","url": "${url}","target": "${target}", "urlTarget": "${targetElement}","dataTarget": "${dataTarget}","onClick": "${onClick}"}]'<c:if test="${not empty className}"> class="${className}"</c:if>>
	${body}
</a>