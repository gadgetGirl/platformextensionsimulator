<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- Import generic forms Tags --%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%-- Tag attributes --%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="search"%>
<%@ attribute required="false" name="simple"%>
<%@ attribute required="false" name="selectedValue"%>

<%-- Create list bean --%>
<%@ attribute required="false" name="bean" rtexprvalue="true" type="java.util.List"%>

<%-- Dropdown bean attributes - For banking beans --%>
<%@ attribute name="bankingBean" required="false" type="fnb.online.fe.core.tags.beans.dropdown.DropDown"%>

<%-- Set required flag --%>
<c:set var="required" value="${optional eq 'true'?'false':'true'}" />

<%-- Do body content if any --%>
<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<%-- Do no bodyContent flag --%>
<c:if test="${empty bodyContent}">
	<c:set var="noBodyContent" value=" noBodyContent"/>
</c:if>

<c:if test="${empty bankingBean}">
	<% jspContext.setAttribute("FE_DropdownBean", bean); %>
</c:if>

<%-- Flag for testing if a selected value was found --%>
<c:set var="selectedIndex" value="0" />
<%-- Var for selected display value --%>
<c:set var="selectedDisplayContent" value="" />

<%-- Test if a banking bean was added and set it otherwise set List bean --%>
<c:choose>
	<c:when test="${not empty bankingBean}">
			
			<%-- Find the selected Index --%>
			<c:forEach items="${bankingBean.items}" var="bankingBeanItem" varStatus="bankingBeanItemStatus">
			
				<%-- Test if selected item--%>
				<c:set var="itemSelected" value="${((bankingBeanItem.returnValue == selectedValue) || (bankingBeanItem.selected == true)) ? true : false}"/>
				
				<%-- Set selected index if selected was found --%>
				<c:if test="${itemSelected == true}">
					<c:set var="selectedIndex" value="${bankingBeanItemStatus.index}" />
				</c:if>
				
			</c:forEach>
			
			<%-- Create dropdown list items --%>
			<c:forEach items="${bankingBean.items}" var="item" varStatus="itemStatus">
				
				<%-- Set current item index--%>
				<c:set var="currentItemIndex" value="${itemStatus.index}"/>
				
				<%-- Reset selected flag --%>
				<c:set var="selected" value="false" />
				
				<%-- Test if current item is the selected one --%>
				<c:if test="${currentItemIndex == selectedIndex}">
					<c:set var="selected" value="true" />
				</c:if>
							
				<%-- Set returnValue, displayValue and selected etc--%>
				<c:set var="returnValue" value="${item.returnValue}"/>
				<c:set var="displayValue" value="${item.displayValue}"/>
				<c:set var="topHeading" value="${item.topHeading}"/>
				<c:set var="bottomHeading" value="${item.topSubHeading}"/>
				
				<%-- Create dropdown list items mobile--%>
				<c:set var="dropdownItemsMobile">
					
					<%-- Prepend dropdownItemsMobile with previous content --%>
					${dropdownItemsMobile}
					
					<%-- Test if multi row dropdown --%>
					<c:choose>
						<c:when test="${not empty bottomHeading}">
						
							<%-- Flag for type of dropdown --%>
							<c:set var="dropdownType" value="threeTier" />
							
							<optgroup label="${empty topHeading?displayValue:topHeading}">
								<option class="label" value="${returnValue}"<c:if test="${selected == 'true'}"> selected</c:if>>${bottomHeading}</option>
							 </optgroup>
							 
						</c:when>
						<c:otherwise>
							
							<%-- Flag for type of dropdown --%>
							<c:set var="dropdownType" value="singleTier" />
							
							<option class="label" value="${returnValue}"<c:if test="${selected == 'true'}"> selected</c:if>>${empty topHeading?displayValue:topHeading}</option>
							
						</c:otherwise>
					</c:choose>
					
				</c:set>
			
				<%-- Create dropdown list items desktop--%>
				<c:set var="dropdownItemsDesktop">
										
					<%-- Prepend dropdownItemsDesktop with previous content --%>
					${dropdownItemsDesktop}
					
					<%-- Test if multi row dropdown --%>
					<c:choose>
						<c:when test="${not empty bottomHeading}">
												
							<%-- Flag for type of dropdown --%>
							<c:set var="dropdownType" value="threeTier" />
							
							<%-- Var for display content --%>
							<c:set var="displayContent">
								<span class="dropdownItemLabel">${empty topHeading?displayValue:topHeading}</span>
								<span class="dropdownItemLabel">${bottomHeading}</span>
							</c:set>
							
							<li class="dropdownItem" data-visible="true" data-value="${returnValue}"<c:if test="${selected == 'true'}"> data-selected="true"</c:if>>
								${displayContent}
							</li>
							
							<%-- Test if current item is the selected one --%>
							<c:if test="${selected == 'true'}">
								<%-- Var for selected display value --%>
								<c:set var="selectedDisplayContent" value="${displayContent}"/>
							</c:if>
							
						</c:when>
						<c:otherwise>
						
							<%-- Flag for type of dropdown --%>
							<c:set var="dropdownType" value="singleTier" />
							
							<li class="dropdownItem" data-visible="true" data-value="${returnValue}"<c:if test="${selected == 'true'}"> data-selected="true"</c:if>>${empty topHeading?displayValue:topHeading}</li>
							
							<%-- Test if current item is the selected one --%>
							<c:if test="${selected == 'true'}">
								<%-- Var for selected display value --%>
								<c:set var="selectedDisplayContent" value="${empty topHeading?displayValue:topHeading}"/>
							</c:if>
							
						</c:otherwise>
						
					</c:choose>
							
				</c:set>

			</c:forEach>
			
	</c:when>
	<c:otherwise>

			<%-- Find the selected Index --%>
			<c:forEach items="${FE_DropdownBean}" var="dropdownItem" varStatus="dropdownItemStatus">
			
				<%-- Test if selected item--%>
				<c:set var="itemSelected" value="${((dropdownItem.returnValue == selectedValue) || (dropdownItem.selected == true)) ? true : false}"/>
				
				<%-- Set selected index if selected was found --%>
				<c:if test="${itemSelected == true}">
					<c:set var="selectedIndex" value="${dropdownItemStatus.index}" />
				</c:if>
				
			</c:forEach>
			
			<%-- Create dropdown list items --%>
			<c:forEach items="${FE_DropdownBean}" var="item" varStatus="itemStatus">
							
				<%-- Set current item index--%>
				<c:set var="currentItemIndex" value="${itemStatus.index}"/>
				
				<%-- Reset selected flag --%>
				<c:set var="selected" value="false" />
				
				<%-- Test if current item is the selected one --%>
				<c:if test="${currentItemIndex == selectedIndex}">
					<c:set var="selected" value="true" />
				</c:if>
				
				<%-- Set returnValue, displayValue and selected etc--%>
				<c:set var="returnValue" value="${item.returnValue}"/>
				<c:set var="topHeading" value="${item.topHeading}"/>
				<c:set var="displayValue" value="${item.displayValue}"/>

				<%-- Create dropdown list items mobile--%>
				<c:set var="dropdownItemsMobile">
				
					<%-- Prepend dropdownItemsMobile with previous content --%>
					${dropdownItemsMobile}
					
					<%-- Test if multi row dropdown --%>
					<c:choose>
						<c:when test="${not empty topHeading}">
						
							<%-- Flag for type of dropdown --%>
							<c:set var="dropdownType" value="threeTier" />
							
							<optgroup label="${topHeading}">
								<option class="label" value="${returnValue}"<c:if test="${selected == 'true'}"> selected</c:if>>${displayValue}</option>
							 </optgroup>
							 
						</c:when>
						<c:otherwise>
							
							<%-- Flag for type of dropdown --%>
							<c:set var="dropdownType" value="singleTier" />
							
							<option class="label" value="${returnValue}"<c:if test="${selected == 'true'}"> selected</c:if>>${displayValue}</option>
							
						</c:otherwise>
					</c:choose>

				</c:set>
			
				<%-- Create dropdown list items desktop--%>
				<c:set var="dropdownItemsDesktop">
									
					<%-- Prepend dropdownItemsDesktop with previous content --%>
					${dropdownItemsDesktop}
					
					<%-- Test if multi row dropdown --%>
					<c:choose>
						<c:when test="${not empty topHeading}">
												
							<%-- Flag for type of dropdown --%>
							<c:set var="dropdownType" value="threeTier" />
							
							<%-- Var for display content --%>
							<c:set var="displayContent">
								<span class="dropdownItemLabel">${topHeading}</span>
								<span class="dropdownItemLabel">${displayValue}</span>
							</c:set>
							
							<li class="dropdownItem" data-visible="true" data-value="${returnValue}"<c:if test="${selected == 'true'}"> data-selected="true"</c:if>>
								${displayContent}
							</li>
							
							<%-- Test if current item is the selected one --%>
							<c:if test="${selected == 'true'}">
								<%-- Var for selected display value --%>
								<c:set var="selectedDisplayContent" value="${displayContent}"/>
							</c:if>
							
						</c:when>
						<c:otherwise>
						
							<%-- Flag for type of dropdown --%>
							<c:set var="dropdownType" value="singleTier" />
							
							<li class="dropdownItem" data-visible="true" data-value="${returnValue}"<c:if test="${selected == 'true'}"> data-selected="true"</c:if>>${displayValue}</li>
							
							<%-- Test if current item is the selected one --%>
							<c:if test="${selected == 'true'}">
								<%-- Var for selected display value --%>
								<c:set var="selectedDisplayContent" value="${displayValue}"/>
							</c:if>
							
						</c:otherwise>
						
					</c:choose>
					
				</c:set>

			</c:forEach>
			
	</c:otherwise>
</c:choose>

<%-- Create var with dropdown markup --%>
<c:set var="dropdown">

	<div class="dropdown ${dropdownType}" data-type="${dropdownType}" data-event="touch"<c:if test="${not empty simple}"> class="simple"</c:if>>
			<span class="dropdownSelectWrapper">
				<select id="${id}" name="${id}" class="validationTarget dropdownItemSelectWrapper" data-transparent="true" tabindex="0">
				
					${dropdownItemsMobile}
					
				</select>
			</span> 
			<span class="dropdownSelected closed" data-rows="4"<c:if test="${not empty disabled}"> data-disabled="true"</c:if>>${selectedDisplayContent}</span><span class="dropdownCarat closed"></span>
				<div class="dropdownContent index10" data-transparent="true">
					<ul class="dropdownItemWrapper" data-scrollable="">
					
						<%-- Test for Search needs to be enabled --%>
						<c:if test="${search eq 'true'}">
							<li class="dropdownItem" data-visible="true" data-type="dropdownInput">
								<input name="${id}_search" class="dropdownInput" data-type="text" data-placeholderIs="true" data-placeholder="Search" value="Search">
							</li>
						</c:if>
						
						${dropdownItemsDesktop}
						
					</ul>
			</div>
		</div>
		
</c:set>

<%-- Test if the simple flag is set and wrap or not in more content --%>
<c:choose>
	<c:when test="${empty simple}">
	
		<fnb.generic.forms:formElementWrapper optional="${optional}" toolTipContent="${toolTipContent}" label="${label}" customType="${customType}" id="${id}" className="${className}">
			
			<fnb.generic.forms:formElementContainer customType="${className}" id="${id}" className="${className}${noBodyContent}" inlineError="${inlineError}" noteContent="${noteContent}">
	
			${dropdown}
			
			</fnb.generic.forms:formElementContainer>	
			<c:if test="${not empty bodyContent}">
				<fnb.generic.forms:formElementContent className="${className}" id="${id}" customType="${customType}">
				
					${bodyContent}
					
				</fnb.generic.forms:formElementContent>
			</c:if>
			
		</fnb.generic.forms:formElementWrapper>
	
	</c:when>
	<c:otherwise>
	
		${dropdown}
		
	</c:otherwise>
</c:choose>
				