<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ tag import="java.util.*"%>
<%@ tag import="fnb.online.fe.core.tags.utils.BeanUtilities"%>
<%@ tag import="fnb.online.fe.core.tags.beans.dropdown.*"%>

<%@ attribute name="label" required="true" rtexprvalue="true"%>
<%@ attribute name="value" required="true" rtexprvalue="true"%>
<%@ attribute name="topHeading" required="false" rtexprvalue="true"%>
<%@ attribute name="selected" required="false" rtexprvalue="true"%>

<%
{
	Boolean selectedBool = Boolean.valueOf(selected);
	
	ThreeTierDropDownItem dd = new ThreeTierDropDownItem();
	dd.setDisplayValue(label);
	dd.setReturnValue(value);
	dd.setTopHeading(topHeading);
	dd.setSelected(selectedBool);
	
	List items = (List) BeanUtilities.getFieldValue(getParent(),"bean");
	if(items == null){
		items = new ArrayList();
	}
	items.add(dd);
	BeanUtilities.setFieldValue(getParent(),"bean",items);
}  

%>