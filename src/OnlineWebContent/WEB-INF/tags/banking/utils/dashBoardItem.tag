<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ tag import="java.util.*"%>
<%@ tag import="fnb.online.fe.core.tags.utils.BeanUtilities"%>
<%@ tag import="fnb.online.fe.core.tags.beans.dashboard.*"%>

<%@ attribute name="widgetId" required="false" rtexprvalue="true"%>

<%@ attribute name="column" required="false" rtexprvalue="true"%>

<%@ attribute name="mobilePosition" required="true" rtexprvalue="true"%>

<%@ attribute name="action" required="false" rtexprvalue="true"%>

<%@ attribute name="method" required="false" rtexprvalue="true"%>

<%@ attribute name="nav" required="false" rtexprvalue="true"%>

<%

{
	DashBoardItem di = new DashBoardItem();
	di.setId(widgetId);
	di.setColumn(column);
	di.setMobilePosition(mobilePosition);
	di.setAction(action);
	di.setMethod(method);
	di.setNav(nav);

	List items = (List) BeanUtilities.getFieldValue(getParent(),"bean");
	if(items == null){
		items = new ArrayList();
	}
	
	items.add(di);
	BeanUtilities.setFieldValue(getParent(),"bean",items);
}  

%>