<%-- Developer: Donovan --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%-- Tag attributes --%>
<%@ attribute name="action" required="false" rtexprvalue="true"%>
<%@ attribute name="method" required="false" rtexprvalue="true"%>
<%@ attribute name="nav" required="false" rtexprvalue="true"%>

<%-- Import Forms tags --%>
<%@ taglib prefix="fnb.hyperion.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%-- Import Forms tags --%>
<%@ taglib prefix="fnb.hyperion.banking.forms" tagdir="/WEB-INF/tags/banking/forms"%>
<%-- Import Markup tags --%>
<%@ taglib prefix="fnb.hyperion.banking.markup" tagdir="/WEB-INF/tags/banking/markup"%>
<%-- Import mvnoCssSlider tags --%>
<%@ taglib prefix="fnb.hyperion.generic.cssSlider" tagdir="/WEB-INF/tags/generic/utils/mvnoCssSlider"%>

<%-- Create bean --%>
<%@ attribute required="false" name="bean" rtexprvalue="true" type="java.util.List"%>

<%-- Set Global var for mobile--%>
<c:set var="mobile" value="${browserInfo.capabilities['fnb_is_mobile']}"/>

<%-- Set dashboard items --%>
<c:set var="bodyContent">
	<jsp:doBody />
	<c:if test='${mobile}'>
	    <% java.util.Collections.sort(bean,new mammoth.navigator.accountsettings.dashboard.util.DashBoardMobileSorter()); %>
	</c:if>
</c:set>

<%-- Create leftColumn data wrapper --%>
<c:set var="leftColumn" value=""/>	
<%-- Create rightColumn data wrapper --%>
<c:set var="rightColumn" value=""/>	
<%-- Create rowItems data wrapper --%>
<c:set var="rowItems" value=""/>
<%-- Create rowItemColumns data wrapper --%>
<c:set var="rowItemColumns" value=""/>	

<%-- Create bean for dashBoard items from jsp--%>
<% jspContext.setAttribute("FE_DashboardBean", bean); %>
				
<%-- Loop required order as set on jsp --%>
<c:forEach items="${FE_DashboardBean}" var="widget" varStatus="status">

	<%-- Store current widget id --%>
	<c:set var="currentWidgetId" value="${widget.id}"/>
	<%-- Store current widget column --%>
	<c:set var="currentWidgetColumn" value="${widget.column}"/>
	<%-- Store current widget mobilePosition --%>
	<c:set var="currentWidgetMobilePosition" value="${widget.mobilePosition}"/>
	<%-- Store current widget action --%>
	<c:set var="currentWidgetAction" value="${widget.action}"/>
	<%-- Store current widget method --%>
	<c:set var="currentWidgetMethod" value="${widget.method}"/>
	<%-- Store current widget nav --%>
	<c:set var="currentWidgetNav" value="${widget.nav}"/>

	<%-- Loop widget list from backend and find id --%>
	<c:forEach items="${dashboardBean.widgetList}" var="widgetListItem">
		
		<%-- if a match is found loop components --%>
		<c:if test="${currentWidgetId==widgetListItem.id}">
			
	   		<%-- Store widget itemmap --%>
	   		<c:set var="widgetRowMap" value="${widgetListItem.rowMap}"/>
			<%-- Get length widgetRowMap --%>
	 		<c:set var="widgetRowMapLength" value="${fn:length(widgetRowMap)}"/>


	 		<%-- if widget has rowitems widgetRowMapLength --%>
			<c:if test="${widgetRowMapLength>0}">
			
				<%-- Store widget heading --%>
			   	<c:set var="widgetHeading" value=""/>
			   	
		   		<%-- Create heading if it exists --%>
				<c:if test="${not empty widgetListItem.heading}">
				
					<%-- Set Widget Heading--%>
			    	<c:set var="widgetHeading">
			    			<%-- Create Heading H wrapper--%>
							<h2 id="${currentWidgetId}heading" data-role="widgetHeading" class="widgetHeading">${widgetListItem.heading}</h2>
					
					</c:set>
					
				</c:if>

				<c:choose>
					
						<c:when test='${mobile}'>
		
							<c:set var="leftColumn">${leftColumn}<div data-role="widgetWrapper" id="${currentWidgetId}" name="${currentWidgetId}" class="clearfix"<c:if test="${not empty currentWidgetAction}"> action="${currentWidgetAction}"</c:if><c:if test="${not empty currentWidgetMethod}"> method="${currentWidgetMethod}"</c:if>><c:if test="${not empty currentWidgetNav}"><input type="hidden" name="nav" id="nav" value="${currentWidgetNav}"></c:if>${widgetHeading}</c:set>
							
						</c:when>
						
						<c:otherwise>
						
							<c:choose>
				
								<c:when test="${currentWidgetColumn=='1'}">
								
						    		<c:set var="leftColumn">${leftColumn}<div data-role="widgetWrapper" id="${currentWidgetId}" name="${currentWidgetId}" class="clearfix"<c:if test="${not empty currentWidgetAction}"> action="${currentWidgetAction}"</c:if><c:if test="${not empty currentWidgetMethod}"> method="${currentWidgetMethod}"</c:if>><c:if test="${not empty currentWidgetNav}"><input type="hidden" name="nav" id="nav" value="${currentWidgetNav}"></c:if>${widgetHeading}</c:set>
									
								</c:when>
								
								<c:when test="${currentWidgetColumn=='2'}">
								
						    		<c:set var="rightColumn">${rightColumn}<div data-role="widgetWrapper" id="${currentWidgetId}" name="${currentWidgetId}" class="clearfix"<c:if test="${not empty currentWidgetAction}"> action="${currentWidgetAction}"</c:if><c:if test="${not empty currentWidgetMethod}"> method="${currentWidgetMethod}"</c:if>><c:if test="${not empty currentWidgetNav}"><input type="hidden" name="nav" id="nav" value="${currentWidgetNav}"></c:if>${widgetHeading}</c:set>
									
								</c:when>
								
							</c:choose>
							
						</c:otherwise>
							
					</c:choose>
									
		   		<%-- Loop widget rowMap and get items --%>
		   		<c:forEach items="${widgetRowMap}" var="widgetRowMapItem" varStatus="widgetRowMapItemStatus">
						
					<%-- Get widget row value --%>
					<c:set var="widgetRowValue" value="${widgetRowMapItem.value}"/>
					<%-- Get widget row itemList --%>
					<c:set var="widgetRowItemList" value="${widgetRowValue.itemList}"/>
					<%-- Get widget row itemList --%>
					<c:set var="widgetRowId" value="${widgetRowValue.rowId}"/>
					<%-- Get widget row cssClass --%>
					<c:set var="widgetRowCssClass" value="${widgetRowValue.cssClass}"/>
					<%-- Create heading if it exists --%>

					<%-- Open rowItems wrapper--%>
			    	<c:set var="rowItems"><div data-role="widgetRow" id="widgetRow${currentWidgetId}${widgetRowId}" data-columns="${fn:length(widgetRowItemList)}" class="clearfix<c:if test='${not empty widgetRowCssClass}'> ${widgetRowCssClass}</c:if>"></c:set>
			    	<%-- Clear rowItems columns --%>
					<c:set var="rowItemColumns" value=""/>
			    	
					<%-- Loop widgetRowItemList and setup items --%>
			   		<c:forEach items="${widgetRowItemList}" var="widgetRowItemListItem" varStatus="widgetRowItemListItemStatus">
	
						<%-- Get Item Type --%>
		 				<%-- Store widgetRowItemValue class --%>
		 				<c:set var="widgetRowItemValueClass" value="${widgetRowItemListItem.class}"/>
		 				<%-- Split widgetRowItemValueClass --%>
		 				<c:set var="widgetRowItemValueClassParts" value="${fn:split(widgetRowItemValueClass, '.')}"/>
		 				<%-- Get length widgetRowItemValueClassParts --%>
		 				<c:set var="widgetRowItemValueClassPartsLength" value="${fn:length(widgetRowItemValueClassParts)}"/>
		 				<%-- Get last part of widgetRowItemValueClassParts --%>
		 				<c:set var="widgetRowItemValueClassLastPart" value="${widgetRowItemValueClassParts[widgetRowItemValueClassPartsLength-1]}"/>
	 				
	 					<%-- Get Some global vars --%>
	 					<%-- Store widgetRowItemListItem Key --%>
		 				<c:set var="itemId" value="${widgetRowItemListItem.id}"/>
						<%-- Store Item Name --%>
					 	<c:set var="itemName" value="${widgetRowItemListItem.name}"/>
						<%-- Store Item Column --%>
					 	<c:set var="itemColumn" value="${widgetRowItemListItem.column}"/>

		 				<%-- Set Widget Item--%>
						<c:choose>

							<%-- Test if InputItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='InputItem'}">

								<%-- Get InputItem vars --%>
								<%-- Store InputItem CssClass --%>
				 				<c:set var="itemCssClass" value="${widgetRowItemListItem.cssClass}"/>
								<%-- Store InputItem Data --%>
				 				<c:set var="itemData" value="${widgetRowItemListItem.data}"/>
								<%-- Store InputItem displayStyle --%>
				 				<c:set var="itemDisplayStyle" value="${widgetRowItemListItem.displayStyle}"/>
								<%-- Store InputItem target --%>
				 				<c:set var="itemHidden" value="${widgetRowItemListItem.hidden}"/>
								<%-- Store ButtonItem Text --%>
				 				<c:set var="itemText" value="${widgetRowItemListItem.text}"/>
								<%-- Store ButtonItem toolTipMessage --%>
				 				<c:set var="itemToolTipMessage" value="${widgetRowItemListItem.toolTipMessage}"/>
				 				<%-- get inputType --%>
				 				<c:set var="inputType" value="${widgetRowItemListItem.inputType}"/>
				 				<%-- get maxLength  --%>
				 				<c:set var="maxlength" value="${widgetRowItemListItem.maxlength}"/>
				 			
				 				<%-- TYPE  --%>

								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
								
									<c:choose>
									
										<c:when test="${inputType == 'numeric'}">
											<fnb.hyperion.banking.forms:input.number id="${itemId}" value="${itemText}" className="${itemCssClass}" maxlength="${maxlength}" toolTipContent="${itemToolTipMessage}" simple="true"/>
										</c:when>
										
										<c:otherwise>
											<%-- REMOVED OLD FRAME WORK BASE INPUT --%>
											<fnb.hyperion.banking.forms:input.text id="${itemId}" simple="true" value="${itemText}" className="${itemCssClass}" maxlength="${maxlength }" hidden="${itemHidden}" toolTipContent="${itemToolTipMessage}"/>
										</c:otherwise>
										
									</c:choose>
								</c:set>
								
							</c:when>
							<%-- Test if TextItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='TextItem'}">

								<%-- Get TextItem vars --%>
								<%-- Store TextItem CssClass --%>
				 				<c:set var="itemCssClass" value="${widgetRowItemListItem.cssClass}"/>
								<%-- Store TextItem Data --%>
				 				<c:set var="itemData" value="${widgetRowItemListItem.data}"/>
								<%-- Store TextItem displayStyle --%>
				 				<c:set var="itemDisplayStyle" value="${widgetRowItemListItem.displayStyle}"/>
								<%-- Store TextItem Text --%>
				 				<c:set var="itemText" value="${widgetRowItemListItem.text}"/>
								<%-- Store TextItem toolTipMessage --%>
				 				<c:set var="itemToolTipMessage" value="${widgetRowItemListItem.toolTipMessage}"/>

								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
									
								<%-- NEW FRAME --%>
								<fnb.hyperion.banking.forms:label id="${itemId}" label="${itemText}" role="widgetLabel" toolTipContent="${itemToolTipMessage}">
									${itemText}
								</fnb.hyperion.banking.forms:label>
						
								<%-- OLD FRAME REMOVE --%>
								<%--
								<div class="formElementLabel ${itemCssClass}">
									${itemText}
								</div>
								 --%>
								</c:set>
	
							</c:when>
							
							<%-- Add New Empty Label --%>
							<c:when test="${widgetRowItemValueClassLastPart=='TextItemEmpty'}">

								<%-- Get TextItem vars --%>
								<%-- Store TextItem CssClass --%>
				 				<c:set var="itemCssClass" value="${widgetRowItemListItem.cssClass}"/>
								<%-- Store TextItem Data --%>
				 				<c:set var="itemData" value="${widgetRowItemListItem.data}"/>
								<%-- Store TextItem displayStyle --%>
				 				<c:set var="itemDisplayStyle" value="${widgetRowItemListItem.displayStyle}"/>
								<%-- Store TextItem Text --%>
				 				<c:set var="itemText" value="${widgetRowItemListItem.text}"/>
								<%-- Store TextItem toolTipMessage --%>
				 				<c:set var="itemToolTipMessage" value="${widgetRowItemListItem.toolTipMessage}"/>

								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
							
									<%-- NEW FRAME --%>
									<fnb.hyperion.banking.forms:label id="${itemId}" label="" role="widgetLabel" toolTipContent="${itemToolTipMessage}"/>
								
								</c:set>
								
							</c:when>
							
							<%-- Test if HyperLinkItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='HyperLinkItem'}">
	
								<%-- Store HyperLinkItem Text --%>
									<c:set var="itemText" value="${widgetRowItemListItem.text}"/>
									<%-- Store HyperLinkItem url --%>
									<c:set var="url" value="${widgetRowItemListItem.url}"/>
									<%-- Store HyperLinkItem target --%>
									<c:set var="itemTarget" value="${widgetRowItemListItem.target}"/>
									<c:set var="classCss" value="${widgetRowItemListItem.cssClass}"/>
								<c:choose>
								    <c:when test="${itemTarget == '0'}">
										<c:set var="event" value="loadPage"/>
								    </c:when>
								    <c:when test="${itemTarget == '3'}">
								        <c:set var="event" value="loadEzi"/>
								    </c:when>
								</c:choose>
								<%-- Append rowItemColumns--%>
							    <c:set var="widgetItem">
									 <fnb.hyperion.banking.forms:hyperLinkItem event="${event}"  label="${itemText}" url="${url}" className="${classCss }"/>  
								</c:set>
								
							</c:when>
							<%-- Test if DropdownItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='DropdownItem'}">
	
								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
								
									<%-- Get DropdownItem vars --%>
									<%-- Store DropdownItem CssClass --%>
					 				<c:set var="itemCssClass" value="${widgetRowItemListItem.cssClass}"/>
									<%-- Store DropdownItem Text --%>
					 				<c:set var="itemText" value="${widgetRowItemListItem.text}"/>
									<%-- Store DropdownItem bankingBean --%>
					 				<c:set var="itemBankingBean" value="${widgetRowItemListItem.dropDown}"/>
					 				
					 				<fnb.hyperion.banking.forms:dropdown id="${itemId}" selectedValue="${itemBankingBean.selectedValue}" className="${itemCssClass}" label="${itemText}" bankingBean="${itemBankingBean}" simple="dashBoard"/>
									
								</c:set>
	
							</c:when>
							<%-- Test if ButtonItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='ButtonItem'}">
	
								<%-- Get ButtonItem vars --%>
								<%-- Store ButtonItem childFunctionRef --%>
				 				<c:set var="itemChildFunctionRef" value="${widgetRowItemListItem.childFunctionRef}"/>
								<%-- Store ButtonItem CssClass --%>
				 				<c:set var="itemCssClass" value="${widgetRowItemListItem.cssClass}"/>
								<%-- Store ButtonItem Data --%>
				 				<c:set var="itemData" value="${widgetRowItemListItem.data}"/>
								<%-- Store ButtonItem disabled --%>
				 				<c:set var="itemDisabled" value="${widgetRowItemListItem.disabled}"/>
								<%-- Store ButtonItem displayStyle --%>
				 				<c:set var="itemDisplayStyle" value="${widgetRowItemListItem.displayStyle}"/>
								<%-- Store ButtonItem target --%>
				 				<c:set var="itemTarget" value="${widgetRowItemListItem.target}"/>
								<%-- Store ButtonItem Text --%>
				 				<c:set var="itemText" value="${widgetRowItemListItem.text}"/>
								<%-- Store ButtonItem toolTipMessage --%>
				 				<c:set var="itemToolTipMessage" value="${widgetRowItemListItem.toolTipMessage}"/>
								<%-- Store ButtonItem url --%>
				 				<c:set var="itemUrl" value="${widgetRowItemListItem.url}"/>

								<%-- Create event var --%>
				 				<c:set var="event" value=""/>
				 				
				 				<%-- NEW FRAME --%>
				 				<%-- Set event based on target --%>
								<c:choose>
								    <c:when test="${itemTarget == '0'}">
										<c:set var="event" value="loadPage"/>
								    </c:when>
								    <c:when test="${itemTarget == '1'}">
										<c:set var="event" value="loadIntoActionMenu"/>
								    </c:when>
								    <c:when test="${itemTarget == '2'}">
								    	
								    </c:when>
								    <c:when test="${itemTarget == '3'}">
										<c:set var="event" value="loadEzi"/>
								    </c:when>
								    <c:when test="${itemTarget == '5'}">
										<c:set var="event" value="download"/>
								    </c:when>
								    <c:when test="${itemTarget == '6'}">
										<c:set var="event" value="loadPopup"/>
								    </c:when>
								    <c:when test="${itemTarget == '7'}">
										<c:set var="event" value="loadPopup"/>
								    </c:when>
								</c:choose> 

								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
								
									<%-- NEW FRAME --%>
									<fnb.hyperion.banking.forms:button event="${event}" url="${itemUrl}" label="${itemText}" id="${itemId}" role="widgetButton"/>
									
								   <%--  <base:formsButton type="button" text="${itemText}" id="${itemId}" link="${itemUrl}" target="${itemTarget}"/>--%> 
			 					</c:set>

							</c:when>
							<%-- Test if DateBoxItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='DateBoxItem'}">
	
								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
								
									DateBoxItem

								</c:set>
								
							</c:when>
							<%-- Test if SliderItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='SliderItem'}">
	
								
								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
								 <%--
								    	all slider detail will be inslide 
								    	the	group (sliderHeading sliderLabel
								        			sliderLeft sliderUsage and sliderUsed) 
								      --%>
								   <c:set var="sliderGroup" value="${widgetRowItemListItem.sliderGroup}"/>
								   <c:set var="sliderGroupHeading" value="${sliderGroup.sliderheading}"/>
								  
								   <fnb.hyperion.generic.cssSlider:mvnoCssSliderWrapper sliderHeading="${sliderGroupHeading }">
	
									   <c:forEach items="${sliderGroup.sliders}" var="slider" varStatus="status">
	
											<c:set var="sliderLeft" value="${slider.left}"/>
											<c:set var="sliderUsed" value="${slider.used}"/>
											<c:set var="sliderUsage" value="${slider.usage}"/>
											<c:set var="sliderLabel" value="${slider.label}"/>
											
											<fnb.hyperion.generic.cssSlider:mvnoCssSlider sliderLabel="${slider.label}" sliderLeft="${slider.left}" sliderUsage="${slider.usage}"  sliderUsed="${slider.used}"/>
						
									   </c:forEach>
									   
								   </fnb.hyperion.generic.cssSlider:mvnoCssSliderWrapper>
															        
								
								
								</c:set>
								
							</c:when>
	

							<%-- Test if CheckBoxItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='CheckBoxItem'}">
	
								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
								
									CheckBoxItem

								</c:set>
								
							</c:when>
							<%-- Test if RadioButtonItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='RadioButtonItem'}">
	
								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
								
									<c:set var="radioGroup" value="${widgetRowItemListItem.radioGroup}" />
									<%-- Store RadioButtonItem id --%>
									<c:set var="groupId" value="${radioGroup.id}" />
									<%-- Store RadioButtonItem className--%>
	    							<c:set var="groupClassName" value="${radioGroup.name}" />
	    							<%-- Store RadioButtonItem label --%>
	    							<c:set var="groupLabel" value="${radioGroup.label}" />
	    							<%-- Store RadioButtonItem selectedValue --%>
	    							<c:set var="groupSelectedValue" value="${radioGroup.selectedValue}" />
	    							<%-- Store RadioButtonItem radioItems --%>
	    							<c:set var="radioItems" value="${radioGroup.radioItems}" />
									<%-- Store ButtonItem toolTipMessage --%>
				 					<c:set var="itemToolTipMessage" value="${radioGroup.toolTipMessage}"/>

	    						    <fnb.hyperion.generic.forms:radioButtonGroup toolTipContent="${itemToolTipMessage}" id="${groupId}" name="${groupId}" className="${groupClassName}" label="${groupLabel}" selectedValue="${groupSelectedValue}" bean="${radioItems}" />
					 				
					 				
					 				<%@ attribute required="false" name="customType"%>

								</c:set>
								
							</c:when>
							<%-- Test if AmountInputItem --%>
							<c:when test="${widgetRowItemValueClassLastPart=='AmountInputItem'}">
	
								<%-- Append rowItemColumns--%>
								<c:set var="widgetItem">
								
									AmountInputItem

								</c:set>
								
							</c:when>
		
						</c:choose>
		
						<%-- Create rowItemColumns wrapper--%>
			    		<c:set var="rowItemColumns">${rowItemColumns}<div data-role="widgetRowItem${widgetRowItemListItemStatus.count}" id="${currentWidgetId}${widgetRowId}widgetRowItem${itemId}" class="clearfix">${widgetItem}</div></c:set>
	
					</c:forEach>
					
					<%-- Close rowItems wrapper--%>
			    	<c:set var="rowItems">${rowItems}${rowItemColumns}</div></c:set>
			    	
			    	<%-- Test if Item Needs to be in the left column or the right--%>
					<c:choose>
					
						<c:when test='${mobile}'>
		
							<c:set var="leftColumn">${leftColumn}${rowItems}</c:set>
							
						</c:when>
						
						<c:otherwise>
						
							<c:choose>
					
								<c:when test="${currentWidgetColumn=='1'}">
				
									<c:set var="leftColumn">${leftColumn}${rowItems}</c:set>
									
								</c:when>
								
								<c:when test="${currentWidgetColumn=='2'}">
								
									<c:set var="rightColumn">${rightColumn}${rowItems}</c:set>
									
								</c:when>
									
							</c:choose>
							
						</c:otherwise>
							
					</c:choose>
					
				</c:forEach>
	
				<%-- Test if left column or the right needs to be coles--%>
				<c:choose>
				
					<c:when test="${currentWidgetColumn=='1'}">
		
						<c:set var="leftColumn">${leftColumn}</div></c:set>
						
					</c:when>
					
					<c:when test="${currentWidgetColumn=='2'}">
					
						<c:set var="rightColumn">${rightColumn}</div></c:set>
						
					</c:when>
						
				</c:choose>
				
			</c:if>
		
		</c:if>

	</c:forEach>
				
</c:forEach>

<c:set var="leftColumn"><div data-role="dashBoardLeftColumn" id="dashBoardLeftColumn" class="clearfix<c:if test='${mobile}'> mobileDashBoard</c:if>">${leftColumn}</div></c:set>

<c:if test='${!mobile}'>
	<c:set var="rightColumn"><div data-role="dashBoardRightColumn" id="dashBoardRightColumn" class="clearfix">${rightColumn}</div></c:set>
</c:if>

<div data-role="dashBoard" class="clearfix<c:if test='${mobile}'> mobileDasboard</c:if>"<c:if test="${not empty action}"> data-action="${action}"</c:if><c:if test="${not empty method}"> data-method="${method}"</c:if><c:if test="${not empty nav}"> data-nav="${nav}"</c:if>>
	
	${leftColumn}
	
	<c:if test='${!mobile}'>
		${rightColumn}
	</c:if>
	
</div>
