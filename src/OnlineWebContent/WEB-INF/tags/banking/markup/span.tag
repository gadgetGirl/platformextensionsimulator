<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>

<%@ attribute name="id" required="false" rtexprvalue="true" description="id of listitem" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="dataAttribute" required="false" rtexprvalue="true" %>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.markup:span id="${id}" className="${className}" dataAttribute="${dataAttribute}">
	${bodyContent}
</fnb.generic.markup:span>