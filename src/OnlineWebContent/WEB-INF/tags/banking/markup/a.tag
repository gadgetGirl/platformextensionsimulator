<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>

<%@ attribute name="id" required="false" rtexprvalue="true" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="href" required="true" rtexprvalue="true" description="page to link to" %>
<%@ attribute name="newTab" required="false" rtexprvalue="true" description="open page in new tab" %>
<%@ attribute name="ajax" required="false" rtexprvalue="true" description="turn ajax on or off" %>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.markup:a id="${id}" className="${className}" href="${href}" newTab="${newTab}" ajax="${ajax}">
	${bodyContent}
</fnb.generic.markup:a>