<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="id" required="false" rtexprvalue="true" description="id of listitem" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="dataAttribute" required="false" rtexprvalue="true" %>

<div<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty className}"> class="${className}"</c:if><c:if test="${not empty dataAttribute}"> data-${dataAttribute}="${dataAttribute}"</c:if>><jsp:doBody /></div>