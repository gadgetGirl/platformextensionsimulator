<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>

<%@ attribute name="id" required="false" %>
<%@ attribute name="src" required="true" %>
<%@ attribute name="altSrc" required="false" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="onclick" required="false" %>
<%@ attribute name="alt" required="false" %>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.markup:img id="${id}" src="${src}" altSrc="${altSrc}" className="${className}" onclick="${onclick}" alt="${alt}">
	${bodyContent}
</fnb.generic.markup:img>