<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>

<%@ attribute name="id" required="false" rtexprvalue="true" description="id of paragraph" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>

<fnb.generic.markup:note id="${id}" className="${className}"><jsp:doBody /></fnb.generic.markup:note>