<%@ taglib prefix="fnb.generic.markup.submitable" tagdir="/WEB-INF/tags/generic/markup/submitable"%>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="id" required="true" %>
<%@ attribute name="value" required="flase" %>

<fnb.generic.markup.submitable:note id="${id }" className="${className }" value="${value }" >
	<jsp:doBody />
</fnb.generic.markup.submitable:note>
