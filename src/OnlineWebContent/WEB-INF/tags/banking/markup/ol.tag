<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>

<%@ attribute name="id" required="false" rtexprvalue="true" description="id of list" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.markup:ol id="${id}" className="${className}">
	${bodyContent}
</fnb.generic.markup:ol>