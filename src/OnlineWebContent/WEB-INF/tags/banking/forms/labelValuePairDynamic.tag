<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>
<%@ taglib prefix="fnb.generic.markup" tagdir="/WEB-INF/tags/generic/markup"%>

<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="submitable"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="true" name="value"%>
<%@ attribute required="false" name="isCurrency"%>
<%@ attribute required="false" name="placeholder"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="leftInnerLabel"%>
<%@ attribute required="false" name="rightInnerLabel"%>
<%@ attribute required="false" name="leftInnerLabelSize"%>
<%@ attribute required="false" name="rightInnerLabelSize"%>
<%@ attribute required="false" name="labelClassName"%>
<%@ attribute required="false" name="valueClassName"%>

<fnb.generic.forms:labelValuePairDynamic id="${id }" label="${label }" labelClassName="${labelClassName }" valueClassName="${valueClassName }" isCurrency="${isCurrency }" value="${value}" customType="${customType }" submitable="${submitable }" optional="${optional }"  toolTipContent="${toolTipContent }" className="${className }" noteContent="${noteContent }" inlineError="${inlineError }" placeholder="${ placeholder}" disabled="${disabled }" leftInnerLabel="${leftInnerLabel }" rightInnerLabel="${rightInnerLabel }" rightInnerLabelSize="${rightInnerLabelSize }" >
	<jsp:doBody />
</fnb.generic.forms:labelValuePairDynamic>