<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="true" name="value"%>
<%@ attribute required="false" name="placeholder"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="leftInnerLabel"%>
<%@ attribute required="false" name="rightInnerLabel"%>
<%@ attribute required="false" name="leftInnerLabelSize"%>
<%@ attribute required="false" name="rightInnerLabelSize"%>

<fnb.generic.forms:output.labelValuePair id="${id}" label="${label}" value="${value}" className="${className}" label="${label}" noteContent="${noteContent}" inlineError="${inlineError }" placeholder="${placeholder }" disabled="${disabled }"  leftInnerLabel="${leftInnerLabe l}"  rightInnerLabel="${rightInnerLabel }" leftInnerLabelSize="${leftInnerLabelSize }" rightInnerLabelSize="${rightInnerLabelSize }">
	<jsp:doBody />
</fnb.generic.forms:output.labelValuePair>