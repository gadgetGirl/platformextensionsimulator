<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="value"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:definitionList customType="${customType}" optional="${optional}" toolTipContent="${toolTipContent}" id="${id}" className="${className}" label="${label}" noteContent="${noteContent}" inlineError="${inlineError}" value="${value}">
	${bodyContent}
</fnb.generic.forms:definitionList>