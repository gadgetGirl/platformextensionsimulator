<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="true" rtexprvalue="true"%>
<%@ attribute name="value" required="true" rtexprvalue="true"%>
<%@ attribute name="selected" required="false" rtexprvalue="true"%>

<fnb.generic.forms:checkBoxItem id="${id}" label="${label}" value="${value}" selected="${selected}"></fnb.generic.forms:checkBoxItem>