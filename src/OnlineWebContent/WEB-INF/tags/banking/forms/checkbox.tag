<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="wrapperClassName"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="selected"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="labelPosition"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:checkbox id="${id}" label="${label}" labelPosition="${labelPosition}" inlineError="${inlineError}" noteContent="${noteContent}" customType="${customType}" optional="${optional}" toolTipContent="${toolTipContent}" className="${className}" wrapperClassName="${wrapperClassName}" selected="${selected}" disabled="${disabled}">
	${bodyContent}
</fnb.generic.forms:checkbox> 