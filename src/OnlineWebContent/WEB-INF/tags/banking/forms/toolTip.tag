
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="content"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="customType"%>

<fnb.generic.forms:toolTip id="${id}" content="${content}" customType="${customType}"></fnb.generic.forms:toolTip>