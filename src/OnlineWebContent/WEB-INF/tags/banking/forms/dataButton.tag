<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="event"%>
<%@ attribute required="false" name="url"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="target"%>
<%@ attribute required="false" name="targetElement"%>
<%@ attribute required="false" name="dataTarget"%>
<%@ attribute required="false" name="onClick"%>

<fnb.generic.forms:dataButton id="${id}" onClick="${onClick}" event="${event}" url="${url}" label="${label}" target="${target}" targetElement="${targetElement}" dataTarget="${dataTarget}"></fnb.generic.forms:dataButton>