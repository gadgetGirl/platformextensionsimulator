<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="fullWidthItems" rtexprvalue="true"%>
<%@ attribute required="false" name="selectedValue" rtexprvalue="true"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="dataItemSelected" %>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:radioButtonGroup id="${id}" className="${className}" customType="${customType}" optional="${optional}" toolTipContent="${toolTipContent}" label="${label}" noteContent="${noteContent}" inlineError="${inlineError}" disabled="${disabled}" fullWidthItems="${fullWidthItems}" selectedValue="${selectedValue}" >
	${bodyContent}
</fnb.generic.forms:input.radioButtonGroup>