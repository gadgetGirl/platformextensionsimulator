<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="fullWidthItems" rtexprvalue="true"%>
<%@ attribute required="false" name="selectedValue" rtexprvalue="true"%>
<%@ attribute required="false" name="disabled"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:selectCheckBoxGroup customType="${customType}" optional="${optional}"  toolTipContent="${toolTipContent}"  id="${id}"  className="${className}"  label="${label}"   noteContent="${noteContent}"   inlineError="${inlineError}"   fullWidthItems="${fullWidthItems}"   selectedValue="${selectedValue}"   disabled="${disabled}" >
	${bodyContent}
</fnb.generic.forms:selectCheckBoxGroup>