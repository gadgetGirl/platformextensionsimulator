<%-- Developer: Donovan --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- Import Forms tags --%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%-- Tag Attributes --%>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="role"%>
<%@ attribute required="true" name="label"%>

<%-- Button Execution Attributes --%>
<%-- Event Attribute REQUIRED--%>
<%@ attribute required="true" name="event"%>

<%-- if event make key value for object--%>
<c:set var="event" value='"event": "${event}"'/>

<%-- Url Attribute --%>
<%@ attribute required="false" name="url"%>

<%-- if url make key value for object--%>
<c:if test="${not empty url}">
	<c:set var="url" value='"url": "${url}",'/>
</c:if>

<%-- Target Attribute --%>
<%@ attribute required="false" name="target"%>

<%-- if target make key value for object--%>
<c:if test="${not empty target}">
	<c:set var="target" value='"target": "${target}",'/>
</c:if>

<%-- urlTarget Attribute --%>
<%@ attribute required="false" name="urlTarget"%>

<%-- if urlTarget make key value for object--%>
<c:if test="${not empty urlTarget}">
	<c:set var="urlTarget" value='"urlTarget": "${urlTarget}",'/>
</c:if>

<%-- dataTarget Attribute --%>
<%@ attribute required="false" name="dataTarget"%>

<%-- if dataTarget make key value for object--%>
<c:if test="${not empty dataTarget}">
	<c:set var="dataTarget" value='"dataTarget\": "${dataTarget}",'/>
</c:if>

<%-- clearHtmlTemplates Attribute --%>
<%@ attribute required="false" name="clearHtmlTemplates"%>

<%-- if clearHtmlTemplates make key value for object--%>
<c:if test="${not empty clearHtmlTemplates}">
	<c:set var="clearHtmlTemplates" value='"clearHtmlTemplates": "${clearHtmlTemplates}",'/>
</c:if>

<%-- clearPageModuleObject Attribute --%>
<%@ attribute required="false" name="clearPageModuleObject"%>

<%-- if clearPageModuleObject make key value for object--%>
<c:if test="${not empty clearPageModuleObject}">
	<c:set var="clearPageModuleObject" value='"clearPageModuleObject": "${clearPageModuleObject}",'/>
</c:if>

<%-- clearPageEventsArray Attribute --%>
<%@ attribute required="false" name="clearPageEventsArray"%>

<%-- if clearPageEventsArray make key value for object--%>
<c:if test="${not empty clearPageEventsArray}">
	<c:set var="clearPageEventsArray" value='"clearPageEventsArray": "${clearPageEventsArray}",'/>
</c:if>

<%-- clearPageTemplatesArray Attribute --%>
<%@ attribute required="false" name="clearPageTemplatesArray"%>

<%-- if clearPageTemplatesArray make key value for object--%>
<c:if test="${not empty clearPageTemplatesArray}">
	<c:set var="clearPageTemplatesArray" value='"clearPageTemplatesArray": "${clearPageTemplatesArray}"'/>
</c:if>

<script type="text/javascript">
	
</script>
		
<span data-type="button"<c:if test="${not empty id}"> id="${id}"</c:if> data-role="${role}" data-settings='[{${url}${target}${urlTarget}${dataTarget}${clearHtmlTemplates}${clearPageModuleObject}${clearPageEventsArray}${clearPageTemplatesArray}${event}}]'>
	<span data-role="buttonLabel">${label}</span>
</span>
