<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="action"%>
<%@ attribute required="false" name="method"%>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="name"%>
<%@ attribute required="false" name="nav" %>

<%-- From Wrapper for New Framework --%>
<fnb.generic.forms:formWrapper id="${id}" name="${name }"  method="${method }" action="${action }" nav="${nav }" className="${className }" >
		<jsp:doBody />
</fnb.generic.forms:formWrapper>