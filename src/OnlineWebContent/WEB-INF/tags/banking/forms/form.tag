<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute name="name" required="true" rtexprvalue="true"%>
<%@ attribute name="action" required="true" rtexprvalue="true"%>
<%@ attribute name="method" required="true" rtexprvalue="true"%>
<%@ attribute name="onsubmit" required="false" rtexprvalue="true"%>
<%@ attribute name="redirectjsp" required="false" rtexprvalue="true"%>
<%@ attribute name="nav" required="false" rtexprvalue="true"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:form name="${name}" action="${action}" method="${method}" onsubmit="${onsubmit}" redirectjsp="${redirectjsp}" nav="${nav}">
	${bodyContent}
</fnb.generic.forms:form>