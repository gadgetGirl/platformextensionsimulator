<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="customType"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:formElementContent id="${id}" className="${className}" customType="${customType}" >
	${bodyContent}
</fnb.generic.forms:formElementContent>