<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="value"%>
<%@ attribute required="false" name="placeholder"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="role"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="hidden"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:input.simpleInput customType="${customType}" optional="${optional}" className="${className}" toolTipContent="${toolTipContent}" id="${id}" value="${value}" placeholder="${placeholder}" role="${role}" disabled="${disabled}" hidden="${hidden}">
	${bodyContent}
</fnb.generic.forms:input.simpleInput>