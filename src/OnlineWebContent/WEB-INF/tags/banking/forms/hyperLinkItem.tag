<%-- Developer: Donovan --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- Import Forms tags --%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%-- Tag Attributes --%>
<%@ attribute required="false" name="id"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="flase" name="className"%>

<%-- Hyperlink Execution Attributes --%>
<%-- Event Attribute REQUIRED--%>
<%@ attribute required="true" name="event"%>

<%-- if event make key value for object--%>
<c:set var="event" value='"event": "${event}"'/>

<%-- Url Attribute --%>
<%@ attribute required="false" name="url"%>

<%-- if url make key value for object--%>
<c:if test="${not empty url}">
	<c:set var="url" value='"url": "${url}",'/>
</c:if>

		
<span href="#" <c:if test="${not empty id}"> id="${id}" </c:if> <c:if test="${not empty className }"> class="${className }" </c:if>data-role="hyperlink" data-settings='[{${url}${event}}]' >${label}</span>

