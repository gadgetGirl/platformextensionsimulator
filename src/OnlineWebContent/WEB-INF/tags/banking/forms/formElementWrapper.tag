<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="true" name="id"%>
<%@ attribute required="true" name="className"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="false" name="label"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:formElementWrapper id="${id}" className="${className}" customType="${customType}" optional="${optional}" toolTipContent="${toolTipContent}" label="${label}" >
	${bodyContent}
</fnb.generic.forms:formElementWrapper>