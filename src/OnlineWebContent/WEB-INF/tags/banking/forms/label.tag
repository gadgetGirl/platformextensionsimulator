<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="role"%>
<%@ attribute required="false" name="toolTipContent"%>
 
<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:label id="${id}" label="${label}" role="${role}" toolTipContent="${toolTipContent}">
	${bodyContent}
</fnb.generic.forms:label>