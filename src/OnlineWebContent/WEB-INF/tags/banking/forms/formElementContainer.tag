<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="elType"%>
<%@ attribute required="false" name="leftInnerLabel"%>
<%@ attribute required="false" name="rightInnerLabel"%>
<%@ attribute required="false" name="leftInnerLabelSize"%>
<%@ attribute required="false" name="rightInnerLabelSize"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:formElementContainer id="${id}" className="${className}" customType="${customType}" noteContent="${noteContent}" inlineError="${inlineError}" elType="${elType}" leftInnerLabel="${leftInnerLabel}" rightInnerLabel="${rightInnerLabel}" leftInnerLabelSize="${leftInnerLabelSize}" rightInnerLabelSize="${rightInnerLabelSize}">
	${bodyContent}
</fnb.generic.forms:formElementContainer>