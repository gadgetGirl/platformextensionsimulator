<%-- Developer: Donovan --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- Import Forms tags --%>
<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%-- Import Holiday Java classes --%>
<%@tag import="mammoth.util.HolidayCalendar"%>
<%@tag import="mammoth.util.Holiday"%>

<%-- Tag attributes --%>
<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="value"%>

<%-- Bean attributes --%>
<%@ attribute name="publicHolidaysCurrentBean" required="false" rtexprvalue="true" type="mammoth.util.HolidayCalendar"%>
<%@ attribute name="publicHolidaysNextBean" required="false" rtexprvalue="true" type="mammoth.util.HolidayCalendar"%>
<%@ attribute name="holidayCalendar" required="false" rtexprvalue="true" type="mammoth.util.HolidayCalendar"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:datePicker customType="${customType}" optional="${optional}" toolTipContent="${toolTipContent}" id="${id}" className="${className}" label="${label}" noteContent="${noteContent}" inlineError="${inlineError}" value="${value}">
	${bodyContent}
</fnb.generic.forms:datePicker>