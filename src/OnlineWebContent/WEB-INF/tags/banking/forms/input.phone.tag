<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/forms"%>

<%@ attribute required="false" name="customType"%>
<%@ attribute required="false" name="optional"%>
<%@ attribute required="false" name="toolTipContent"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="inlineError"%>
<%@ attribute required="false" name="codeValue"%>
<%@ attribute required="false" name="numberValue"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="areaCodeInput"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.forms:input.phone id="${id}" className="${className}" customType="${customType}" optional="${optional}" toolTipContent="${toolTipContent}" label="${label}" noteContent="${noteContent}" inlineError="${inlineError}" disabled="${disabled}"  codeValue="${codeValue}"  numberValue="${numberValue}" areaCodeInput="${areaCodeInput}">
	${bodyContent}
</fnb.generic.forms:input.phone>