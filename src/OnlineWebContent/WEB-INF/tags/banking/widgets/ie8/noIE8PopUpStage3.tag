<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/widgets/ie8"%>

<%@ attribute required="false" name="skin"%>
<%@ attribute required="false" name="path"%>

<fnb.generic.forms:noIE8PopUpStage3 path="${path}" skin="${skin}"/>