<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.forms" tagdir="/WEB-INF/tags/generic/widgets/ie8"%>

<%@ attribute required="false" name="skin"%>
<%@ attribute required="false" name="layout"%>

<fnb.generic.forms:noIE8Login layout="${layout}" skin="${skin}"/>