<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fnb.generic.layout" tagdir="/WEB-INF/tags/generic/layout/eziLayouts"%>

<%@ attribute required="false" name="className" %>

<fnb.generic.layout:eziContainer className="${className }" >
  <jsp:doBody />
</fnb.generic.layout:eziContainer>