<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute required="false" name="subHeader" %>
<%@ attribute required="false" name="className" %>
<%@ taglib prefix="fnb.generic.layout" tagdir="/WEB-INF/tags/generic/layout/eziLayouts"%>


<fnb.generic.layout:eziHeader subHeader="${subHeader }" className="mvnoEziHeader">
   <jsp:doBody />
</fnb.generic.layout:eziHeader>