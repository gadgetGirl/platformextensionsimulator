<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.layout" tagdir="/WEB-INF/tags/generic/layout"%>

<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.layout:pageContainer mainMenu="${mainMenu}" subMenu="${subMenu}">
	${bodyContent}
</fnb.generic.layout:pageContainer>