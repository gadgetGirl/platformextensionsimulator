<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.banking.frame" tagdir="/WEB-INF/tags/banking/frame"%>

<%@ attribute required="false" name="id"%>

<%@ attribute name="action" required="false" rtexprvalue="true"%>

<%@ attribute name="method" required="false" rtexprvalue="true"%>

<%@ attribute name="nav" required="false" rtexprvalue="true"%>

<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<fnb.banking.frame:bankingPageWrapper>
	<div data-role="layoutOne" class="pageWrapper"<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty action}"> data-action="${action}"</c:if><c:if test="${not empty method}"> data-method="${method}"</c:if><c:if test="${not empty nav}"> data-nav="${nav}"</c:if>>
	 	<c:choose>
			<c:when test="${!empty subTabs}">
				<base:subTabsMenuHolder>
					<div id="subTabsPageHeader"></div>
					<script type="text/javascript">
						firstSubtab = "";
						selected = "";
					</script>
					<div class="subTabScrollLeft hidden" onclick="fnb.utils.mobile.subtabs.scrollLeft()"></div>
						<div id="subTabsContainer" class="subTabsContainer">
							<div id="subTabsScrollable" class="subTabsScrollable">
								<c:forEach items="${subTabs}" var="tab" varStatus="tabCount">
									<script type="text/javascript">
										if (firstSubtab == "") {
											firstSubtab = "${tab.parentFunctionRef}" + "_" + "${tab.childFunctionRef}";
										}
									</script>
									<c:if test="${tab.selected}">
										<base:subTabsMenu url="${tab.url}" text="${tab.label}" isActiveTab="true" />
									</c:if>
									<c:if test="${not tab.selected}">
										<base:subTabsMenu url="${tab.url}" text="${tab.label}" isActiveTab="false" isDisabled="${tab.disabled}" />
									</c:if>
								</c:forEach>
							</div>
						</div>
					<div class="subTabScrollRight hidden" onclick="fnb.utils.mobile.subtabs.scrollRight()"></div>
				</base:subTabsMenuHolder>
				<base:subTabsSection number="1">
				<jsp:doBody />
				</base:subTabsSection>
			</c:when>
			<c:otherwise>
				<base:subTabsSection className="noTabs" number="1">
				<jsp:doBody />
				</base:subTabsSection>
			</c:otherwise>
		</c:choose>
		

	</div>
	
</fnb.banking.frame:bankingPageWrapper>