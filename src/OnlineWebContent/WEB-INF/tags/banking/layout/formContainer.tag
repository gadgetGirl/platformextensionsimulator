<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.banking.frame" tagdir="/WEB-INF/tags/banking/frame"%>

<%@ attribute required="false" name="id"%>

<fnb.banking.frame:bankingPageWrapper>

	<div data-role="formsLayoutOneGroup">
		<div data-role="formsLayoutOneInner">
			<div data-role="formsLayoutOneContentOne" data-background="white"<c:if test="${not empty id}"> id="${id}"</c:if>>
				<jsp:doBody />
			</div>
			<div data-role="formsLayoutOneContentTwo" data-background="grey1" class="borderLeft borderRight">
	
			</div>
		</div>
	</div>
	
</fnb.banking.frame:bankingPageWrapper>
