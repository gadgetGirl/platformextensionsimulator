<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fnb.generic.frame.layout.generic" tagdir="/WEB-INF/tags/generic/frame/layout/generic"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="layoutFiveA_className"%>
<%@ attribute required="false" name="layoutFiveB_className"%>
<%@ attribute required="false" name="section1" description="place holder"%>
<%@ attribute required="false" name="section2" description="place holder"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageImage" description="graphic used to as a background on the page"%>
<%@ attribute required="false" name="pageMenu" description="content of the pageMenu"%>
<%@ attribute required="false" name="heading" description="file location to page menu"%>
<%@ attribute required="false" name="subHeading" description="file location to page menu"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.layout.generic:layoutFive id="${id}" layoutFiveA_className="${layoutFiveA_className}" layoutFiveB_className="${layoutFiveB_className}" section1="${section1}" section2="${section2}" pageThumbnail="${pageThumbnail}" pageImage="${pageImage}" pageMenu="${pageMenu}" heading="${heading}" subHeading="${subHeading}" doMenuLoad="${doMenuLoad}" selectedTopTab="${selectedTopTab}" systemMetaTags="${systemMetaTags}" systemJs="${systemJs}" systemCss="${systemCss}" mainMenu="${mainMenu}" subMenu="${subMenu}">
	${bodyContent}
</fnb.generic.frame.layout.generic:layoutFive>