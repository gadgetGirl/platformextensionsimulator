<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fnb.generic.frame.layout.generic" tagdir="/WEB-INF/tags/generic/frame/layout/generic"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.layout.generic:layoutZero>
	${bodyContent}
</fnb.generic.frame.layout.generic:layoutZero>