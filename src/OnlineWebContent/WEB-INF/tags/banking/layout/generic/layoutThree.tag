<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fnb.generic.frame.layout.generic" tagdir="/WEB-INF/tags/generic/frame/layout/generic"%>

<%@ attribute required="false" name="doMenuLoad"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="layoutThreeA_className"%>
<%@ attribute required="false" name="layoutThreeB_className"%>
<%@ attribute required="false" name="pageThumbnail" description="graphic used to describe the page"%>
<%@ attribute required="false" name="pageMenu" description="file location to page menu"%>
<%@ attribute required="false" name="heading" description="file location to page menu"%>
<%@ attribute required="false" name="subHeading" description="file location to page menu"%>
<%@ attribute required="false" name="bannerGroup" description="Banner group to be displayed on page"%>
<%@ attribute required="false" name="whatYouNeed" description="what you need in order to apply"%>
<%@ attribute required="false" name="apply" description="what you can do to apply"%>
<%@ attribute required="false" name="contact" description="contact information related to applying"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.layout.generic:layoutThree id="${id}" layoutThreeA_className="${layoutThreeA_className}" layoutThreeB_className="${layoutThreeB_className}" pageThumbnail="${pageThumbnail}" pageMenu="${pageMenu}" heading="${heading}" subHeading="${subHeading}" bannerGroup="${bannerGroup}" whatYouNeed="${whatYouNeed}" apply="${apply}" contact="${contact}" doMenuLoad="${doMenuLoad}" selectedTopTab="${selectedTopTab}" systemMetaTags="${systemMetaTags}" systemJs="${systemJs}" systemCss="${systemCss}" mainMenu="${mainMenu}" subMenu="${subMenu}">
	${bodyContent}
</fnb.generic.frame.layout.generic:layoutThree>