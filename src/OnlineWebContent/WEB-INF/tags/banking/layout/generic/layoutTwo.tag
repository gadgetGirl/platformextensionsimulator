<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.banking.frame" tagdir="/WEB-INF/tags/banking/frame"%>

<%@ attribute required="false" name="id"%>

<fnb.banking.frame:bankingPageWrapper>

	<div data-role="layoutTwo"<c:if test="${not empty id}"> id="${id}"</c:if>>

		<jsp:doBody />

	</div>
	
	<div id="eziPannelButtonsWrapper">
		<div class="eziWrapperInner">
			<div id="eziPannelButtons">

			</div>
		</div>
	</div>
	
</fnb.banking.frame:bankingPageWrapper>