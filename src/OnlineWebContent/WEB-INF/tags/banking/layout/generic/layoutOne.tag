<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.banking.frame" tagdir="/WEB-INF/tags/banking/frame"%>

<%@ attribute required="false" name="id"%>

<%@ attribute name="action" required="false" rtexprvalue="true"%>

<%@ attribute name="method" required="false" rtexprvalue="true"%>

<%@ attribute name="nav" required="false" rtexprvalue="true"%>

<fnb.banking.frame:bankingPageWrapper>

	<div data-role="layoutOne"<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty action}"> data-action="${action}"</c:if><c:if test="${not empty method}"> data-method="${method}"</c:if><c:if test="${not empty nav}"> data-nav="${nav}"</c:if>>

		<jsp:doBody />

	</div>
	
</fnb.banking.frame:bankingPageWrapper>