<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.layout" tagdir="/WEB-INF/tags/generic/frame/layout"%>

<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="true" name="pageHeading"%>
<%@ attribute required="false" name="actionMenu"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.layout:layoutSix selectedTopTab="${selectedTopTab}" id="${id}" mainMenu="${mainMenu}" subMenu="${subMenu}" actionMenu="${actionMenu}" pageHeading="${pageHeading}">
	${bodyContent}
</fnb.generic.frame.layout:layoutSix>