<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	

<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame:coreJavascript>
	${bodyContent}
</fnb.generic.frame:coreJavascript>