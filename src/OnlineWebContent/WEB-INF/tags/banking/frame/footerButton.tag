 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

 <%@ attribute required="false" name="event"%>
 <%@ attribute required="false" name="url"%>
 <%@ attribute required="true" name="label"%>
 <%@ attribute required="false" name="target"%>
 <%@ attribute required="false" name="targetElement"%>
 <%@ attribute required="false" name="dataTarget"%>

 <%@ attribute required="false" name="clearHtmlTemplates"%>
 <%@ attribute required="false" name="clearPageModuleObject"%>
 <%@ attribute required="false" name="clearPageEventsArray"%>
 <%@ attribute required="false" name="clearPageTemplatesArray"%>
<%@ attribute required="false" name="onClick"%>

<fnb.generic.frame:footerButton onClick="${onClick}" event="${event}" url="${url}" label="${label}" target="${target}" targetElement="${targetElement}" dataTarget="${dataTarget}" clearHtmlTemplates="${clearHtmlTemplates}" clearPageModuleObject="${clearPageModuleObject}" clearPageEventsArray="${clearPageEventsArray}" clearPageTemplatesArray="${clearPageTemplatesArray}"></fnb.generic.frame:footerButton>