<% String skin = (String)request.getAttribute("skin");
	if (skin == null) {
		skin = (String)session.getAttribute("skin");
	}
	if (skin == null || skin.length() == 0) {
		skin = (String)request.getParameter("skin");
	}
	if (skin == null || skin.length() == 0) {
		skin = "0";
	}
	int skinInt = 0;
	try {
		skinInt = Integer.parseInt(skin);
	} catch (Exception e) {
		skin = "0";
	}
	//Logic for pages going to main.jsp that require an override of the simple cookie
	if(request.getParameter("simple") != null && !request.getParameter("simple").equals("") ){
		Cookie simpleCookie = new Cookie("zobCookie", request.getParameter("simple"));
		simpleCookie.setPath("/");
		simpleCookie.setMaxAge(-1);	// Only needed for current session
		response.addCookie(simpleCookie);
	}
%>

<%=skin%>