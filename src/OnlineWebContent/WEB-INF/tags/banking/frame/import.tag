<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>
<%@ taglib prefix="fnb.hyperion.banking.frame" tagdir="/WEB-INF/tags/banking/frame"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute required="true" name="type" description="TYPES: js, css"%>
<%@ attribute required="true" name="link" %>
<%@ attribute required="false" name="coreAsset" %>
<%@ attribute required="false" name="ie8" %>
<%@ attribute required="false" name="mobile" %>
<%@ attribute required="false" name="version" %>
<%@ attribute required="false" name="data" %>

<c:if test="${empty version}">
	<%-- Developer: Donovan 
		Get JS and CSS versions
	--%>
	<c:set var="version">
	        <fnb.hyperion.banking.frame:version/>
	</c:set>
</c:if>

<fnb.generic.frame:import data="${data}" version="${version}" type="${type}" link="${link}" coreAsset="${coreAsset}" ie8="${ie8}" mobile="${mobile}"></fnb.generic.frame:import>