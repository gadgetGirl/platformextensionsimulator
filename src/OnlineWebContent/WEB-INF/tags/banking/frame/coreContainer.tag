<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="systemMetaTags"%>
<%@ attribute required="false" name="systemJs" description="Specific javascript used by system implementing the core framework"%>
<%@ attribute required="false" name="systemCss" description="Specific StyleSheet used by system implementing the core framework"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="false" name="actionMenu" description="Side menu items(Action menu)"%>
<%@ attribute required="false" name="contextPath" description="Context Path"%>
<%@ attribute required="false" name="pageTitle" description="Page Title"%>
<%@ attribute required="false" name="includesVersion" description="Version for script includes"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame:coreContainer includesVersion="${includesVersion}" systemContextPath="banking" contextPath="${contextPath}" pageTitle="${pageTitle}" selectedTopTab="${selectedTopTab}" systemMetaTags="${systemMetaTags}" systemJs="${systemJs}" systemCss="${systemCss}" mainMenu="${mainMenu}" subMenu="${subMenu}" actionMenu="${actionMenu}">
	${bodyContent}
</fnb.generic.frame:coreContainer>


