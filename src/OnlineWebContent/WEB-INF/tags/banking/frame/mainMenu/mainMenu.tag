<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.mainMenu" tagdir="/WEB-INF/tags/generic/frame/mainMenu"%>

<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="dataUrl"%>
<%@ attribute required="false" name="last"%>
<%@ attribute required="false" name="selected"%>
<%@ attribute required="false" name="className"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.mainMenu:mainMenu contextPath="false" id="${id}" dataUrl="${dataUrl}" last="${last}" selected="${selected}" className="${className}">
	${bodyContent}
</fnb.generic.frame.mainMenu:mainMenu>