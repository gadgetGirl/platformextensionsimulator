<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.subMenu" tagdir="/WEB-INF/tags/generic/frame/subMenu"%>

<%@ attribute required="true" name="sets"%>
<%@ attribute required="true" name="setIds"%>
<%@ attribute required="false" name="menuTitle"%>
<%@ attribute required="false" name="atriclesTitle"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.subMenu:subMenuGroup sets="${sets}" setIds="${setIds}" menuTitle="${menuTitle}" atriclesTitle="${atriclesTitle}">
	${bodyContent}
</fnb.generic.frame.subMenu:subMenuGroup>