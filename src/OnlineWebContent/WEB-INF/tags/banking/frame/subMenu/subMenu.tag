<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.subMenu" tagdir="/WEB-INF/tags/generic/frame/subMenu"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.subMenu:subMenu>
	${bodyContent}
</fnb.generic.frame.subMenu:subMenu>