<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.subMenu" tagdir="/WEB-INF/tags/generic/frame/subMenu"%>

<%@ attribute required="false" name="className"%>
<%@ attribute required="true" name="dataUrl"%>
<%@ attribute required="false" name="image"%>
<%@ attribute required="true" name="label"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.subMenu:subMenuArticleItem className="${className}" dataUrl="${dataUrl}" image="${image}" label="${label}">
	${bodyContent}
</fnb.generic.frame.subMenu:subMenuArticleItem>