<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.subMenu" tagdir="/WEB-INF/tags/generic/frame/subMenu"%>

<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="visible"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.subMenu:subMenuGroupSet id="${id}" visible="${visible}">
	${bodyContent}
</fnb.generic.frame.subMenu:subMenuGroupSet>