<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.subMenu" tagdir="/WEB-INF/tags/generic/frame/subMenu"%>

<%@ attribute required="false" name="dataUrl"%>
<%@ attribute required="true" name="dataLevel"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="last"%>
<%@ attribute required="false" name="selected"%>
<%@ attribute required="false" name="thirdParty"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.subMenu:subMenuItem dataUrl="${dataUrl}" dataLevel="${dataLevel}" label="${label}" last="${last}" selected="${selected}" thirdParty="${thirdParty}">
	${bodyContent}
</fnb.generic.frame.subMenu:subMenuItem>