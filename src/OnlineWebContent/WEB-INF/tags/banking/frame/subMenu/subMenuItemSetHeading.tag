<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.subMenu" tagdir="/WEB-INF/tags/generic/frame/subMenu"%>

<%@ attribute required="false" name="label"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.subMenu:subMenuItems label="${label}">
	${bodyContent}
</fnb.generic.frame.subMenu:subMenuItems>