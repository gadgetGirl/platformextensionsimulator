<div id="eziOverlay" data-index="30" data-role="overlay" class="dataVisible bodyWidth">
		<div data-role="overlayInner">
			<div data-role="gridGroupInner">
				<div data-role="gridColEzi" data-type="eziWrapperLeft"  class="borderRight">&nbsp;</div>
				<div data-role="gridColEzi" data-type="eziWrapperRight" class="borderRight borderLeft">
					<div data-role="eziInner">
						<div data-role="eziInnerTwo">
						<jsp:doBody/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>