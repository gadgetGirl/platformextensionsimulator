<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@tag import="fnb.online.tags.beans.AbstractViewBean"%>
<%@tag import="java.util.Map"%>
<%@tag import="fnb.online.tags.validation.handlers.ValidationHandler"%>

<%@ taglib prefix="fnb.banking.frame" tagdir="/WEB-INF/tags/banking/frame"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<jsp:useBean id="validationBeans" class="java.util.ArrayList" scope="request" />

<jsp:useBean id="technicalReference" class="java.lang.String" scope="request"/>

<script type="text/javascript">

    <%for(Object viewBean: validationBeans) {
        System.err.println("in jsp: viewBean : " + viewBean);
        Class viewBeanClass = (Class) viewBean;
        AbstractViewBean viewBeanInstance = (AbstractViewBean) viewBeanClass.newInstance();
        Map<String, ValidationHandler> handlers = viewBeanInstance.getFieldValidationHandlers();
        for(String fieldName: handlers.keySet()) {
           //System.err.println("in jsp: viewBean : " + viewBean + " fieldName: " + fieldName + " handler: " + handlers.get(fieldName));%>
            	console.log("fieldName: <%=fieldName%> handler: <%=handlers.get(fieldName).getClass().getSimpleName()%>");
                $("#<%=fieldName%>").attr("validationHandler","<%=handlers.get(fieldName).getClass().getSimpleName()%>");<%}}%>
	
     $('#footerWrapper').find('#support-reference').html('${technicalReference}');
     
     
 		if(typeof window.genericPageObject != 'undefined') {
    	 
			 window.genericPageObject = undefined;
    	 
     	}
         
     if(typeof _pageSpecificJavaScriptObject != 'undefined') {
    	 
    	 _pageSpecificJavaScriptObject = undefined;
    	 
     }
 	if(typeof _pageDataObject != 'undefined') {
    	 
	 _pageDataObject = undefined;
    	 
     }
     
</script>

<!-- Do action menu -->
<base:bnkActionBarHolder id="pageActionBar" />

<jsp:doBody />
