<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="fullWidth"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame:coreDocument className="${className}" fullWidth="${fullWidth}">
	${bodyContent}
</fnb.generic.frame:coreDocument>