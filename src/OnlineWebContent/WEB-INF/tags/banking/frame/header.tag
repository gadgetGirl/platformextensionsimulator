<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>

<fnb.generic.frame:header mainMenu="${mainMenu}" selectedTopTab="${selectedTopTab}" subMenu="${subMenu}"></fnb.generic.frame:header>