<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

<%@ attribute required="false" name="actionMenu"%>
<%@ attribute required="false" name="actionMenuLabelClass"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame:actionMenu actionMenu="${actionMenu}" actionMenuLabelClass="${actionMenuLabelClass}">
	${bodyContent}
</fnb.generic.frame:actionMenu>