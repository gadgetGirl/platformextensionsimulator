<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.pageMenu" tagdir="/WEB-INF/tags/generic/frame/pageMenu"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.pageMenu:pageMenu>
	${bodyContent}
</fnb.generic.frame.pageMenu:pageMenu>