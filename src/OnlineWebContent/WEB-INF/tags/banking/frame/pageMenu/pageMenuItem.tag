<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

<%@ attribute required="false" name="dataUrl"%>
<%@ attribute required="true" name="dataLevel"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="last"%>
<%@ attribute required="false" name="selected"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame:pageMenuItem dataUrl="${dataUrl}" dataLevel="${dataLevel}" label="${label}" last="${last}" selected="${selected}">
	${bodyContent}
</fnb.generic.frame:pageMenuItem>