<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

 <%@ attribute required="false" name="event"%>
 <%@ attribute required="false" name="url"%>
 <%@ attribute required="true" name="label"%>
 <%@ attribute required="false" name="target"%>
 <%@ attribute required="false" name="targetElement"%>
 <%@ attribute required="false" name="dataTarget"%>
 <%@ attribute required="false" name="disabled"%>
 
<fnb.generic.frame:footerContactInfo event="${event}" url="${url}" label="${label}" target="${target}" targetElement="${targetElement}" dataTarget="${dataTarget}" disabled="${disabled}"></fnb.generic.frame:footerContactInfo>