<%@tag import="fnb.online.bifrost.server.system.Controller"%>
<%@tag import="mammoth.cache.CacheImplFactory"%>

<%!
static long newestFileTimeStamp = -1;
static long lastTimeChecked = -1;
static long checkInterval = 1000*60;

%>

<%


if(System.currentTimeMillis()-lastTimeChecked>checkInterval){
    lastTimeChecked = System.currentTimeMillis();
    try{
        Long ts = (Long) CacheImplFactory.getInstance().get("/imports/js","newestFileTimeStamp");
        if(ts==null){
            ts = -1L;
        }
    newestFileTimeStamp = ts;
    }catch(Exception e1){
        System.err.println("import.tag: Error while finding newest timestamp from cache ");
        e1.printStackTrace();
    }
}

if(newestFileTimeStamp==-1){
    
    try{
        java.nio.file.Files.walkFileTree(java.nio.file.Paths.get(Controller.getDataPath()+"/../../00Assets"),
                new java.nio.file.SimpleFileVisitor<java.nio.file.Path>() {
                    @Override
                    public java.nio.file.FileVisitResult visitFile(java.nio.file.Path file,
                            java.nio.file.attribute.BasicFileAttributes attrs) {
                        newestFileTimeStamp = Math.max(newestFileTimeStamp, attrs.lastModifiedTime().toMillis());
                        return java.nio.file.FileVisitResult.CONTINUE;
                    }    
                 }   
        );
        CacheImplFactory.getInstance().put("/imports/js","newestFileTimeStamp",new Long(newestFileTimeStamp),false);
    }catch(Exception e){
        System.err.println("import.tag: Error while finding newest timestamp ");
        e.printStackTrace();
    }
}

%>

<%=newestFileTimeStamp%>