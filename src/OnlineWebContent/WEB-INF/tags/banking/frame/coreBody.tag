<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

<%@ attribute required="false" name="selectedTopTab"%>
<%@ attribute required="false" name="mainMenu" description="main navigation items"%>
<%@ attribute required="false" name="subMenu" description="submenu linked to main navigation items"%>
<%@ attribute required="false" name="actionMenu"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame:coreBody selectedTopTab="${selectedTopTab}" mainMenu="${mainMenu}" subMenu="${subMenu}" actionMenu="${actionMenu}">
	${bodyContent}
</fnb.generic.frame:coreBody>