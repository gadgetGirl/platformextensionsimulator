
<%@ taglib prefix="fnb.generic.frame" tagdir="/WEB-INF/tags/generic/frame"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame:footerButtonGroup>
	${bodyContent}
</fnb.generic.frame:footerButtonGroup>