<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.grid" tagdir="/WEB-INF/tags/generic/frame/grid"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.grid:gridRow>
	${bodyContent}
</fnb.generic.frame.grid:gridRow>