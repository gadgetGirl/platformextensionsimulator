<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.grid" tagdir="/WEB-INF/tags/generic/frame/grid"%>

<%@ attribute required="false" name="width"%>
<%@ attribute required="false" name="span"%>
<%@ attribute required="false" name="className"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.grid:gridCol width="${width}" span="${span}" className="${className}">
	${bodyContent}
</fnb.generic.frame.grid:grid>