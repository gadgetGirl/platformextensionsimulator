<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.grid" tagdir="/WEB-INF/tags/generic/frame/grid"%>

<%@ attribute required="false" name="width"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.grid:grid width="${width}">
	${bodyContent}
</fnb.generic.frame.grid:grid>