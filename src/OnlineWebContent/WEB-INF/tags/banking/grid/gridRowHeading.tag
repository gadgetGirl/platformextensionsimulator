<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fnb.generic.frame.grid" tagdir="/WEB-INF/tags/generic/frame/grid"%>

<%@ attribute required="false" name="width"%>
<%@ attribute required="false" name="span"%>

<c:set var="bodyContent">
	<jsp:doBody />
</c:set>

<fnb.generic.frame.grid:gridRowHeading width="${width}" span="${span}">
	${bodyContent}
</fnb.generic.frame.grid:gridRowHeading>