<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag import="fnb.online.tags.beans.table.TableOptions"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="tableBean" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableBean"%>

<% 

TableOptions tableOptions = tableBean.getTableOptions();
if(tableOptions == null || tableBean.getOverwriteTableOptions()){
	tableOptions = new TableOptions();
	tableBean.setTableOptions(tableOptions);
}
request.setAttribute(id,tableOptions);

%>
	
<c:if test="${tableBean.overwriteTableOptions }">
	<jsp:doBody></jsp:doBody>
</c:if>
