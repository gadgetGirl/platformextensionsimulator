<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>


<div id="sessionTimedOutOverlay" class="hidden">
	<form name="FORM_SESSION_TIME_OUT" id="FORM_SESSION_TIME_OUT">
		<div id="logOffTimerPopUp">
			<div class="clockContainer">
				<div class="clock">
					<div id="timer">120</div>
					<div id="timerText">seconds</div>
				</div>
				<div id="messageText">Your Online Banking session is about to time out...</div>
				<div id="buttonRow">
					<div class="floatContainer">
						<div class="buttoncell" id="OK" onclick="fnb.functions.logOff.exit();">Continue Banking</div>
						<div class="buttoncell" id="LOGOFF" onclick="fnb.functions.logOff.logOff();">Log off</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div id="logOff" class="hidden"></div>