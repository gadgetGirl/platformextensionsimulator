<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute required="true"  rtexprvalue="true" name="href"%>
<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="target"%>
<%@ attribute required="false" rtexprvalue="true" name="media"%>
<%@ attribute required="false" rtexprvalue="true" name="rel"%>
<%@ attribute required="false" rtexprvalue="true" name="hreflang"%>
<%@ attribute required="false" rtexprvalue="true" name="onclick"%>
<%@ attribute required="false" rtexprvalue="true" name="type"%>

<%-- only render non-empty attributes on hyper link :: it means cleaner HTML --%>

<c:set var="id"><c:choose><c:when test="${not empty id}">id="${id}"</c:when><c:otherwise></c:otherwise></c:choose></c:set>
<c:set var="className"><c:choose><c:when test="${not empty className}">class="${className}"</c:when><c:otherwise></c:otherwise></c:choose></c:set>
<c:set var="target"><c:choose><c:when test="${not empty target}">target="${target}"</c:when><c:otherwise></c:otherwise></c:choose></c:set>
<c:set var="media"><c:choose><c:when test="${not empty media}">media="${media}"</c:when><c:otherwise></c:otherwise></c:choose></c:set>
<c:set var="rel"><c:choose><c:when test="${not empty rel}">rel="${rel}"</c:when><c:otherwise></c:otherwise></c:choose></c:set>
<c:set var="hreflang"><c:choose><c:when test="${not empty hreflang}">hreflang="${hreflang}"</c:when><c:otherwise></c:otherwise></c:choose></c:set>
<c:set var="obeTablet">${sessionScope.SessionData["HyphenUser"].obeTablet}</c:set>

<c:if test="${obeTablet && href != '#'}">
	<c:set var="href" value="ui://open.window(${href},90,90,Banking Info)" />
	<c:set var="onclick" value="window.external.notify('${href}');" />
</c:if>

<c:set var="onclick"><c:choose><c:when test="${not empty onclick}">onclick="${onclick}"</c:when><c:otherwise></c:otherwise></c:choose></c:set>

<a href="<c:if test="${not empty type}">${type}:</c:if>${href}" ${id} ${className} ${target} ${media} ${rel} ${hreflang} ${onclick}><jsp:doBody /></a>