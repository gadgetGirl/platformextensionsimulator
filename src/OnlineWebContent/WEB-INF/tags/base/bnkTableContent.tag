<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="tableBean" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableBean"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="downloadUrl" required="false" rtexprvalue="true"%>
<%@ attribute name="isActionMenu" required="false" rtexprvalue="true"%>
<%@ attribute name="searchHeading" required="false" rtexprvalue="true"%>
<%@ attribute name="eziItemClickLoad" required="false" rtexprvalue="true"%>
<%@ attribute name="noSorting" required="false" rtexprvalue="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="headerContent" required="false" rtexprvalue="true"%>
<%@ attribute name="newFrameWorkFlag" required="false" %>


<c:choose>
<c:when test="${not empty tableBean.tableHeading}">
<c:set var="tableHeading" value="${tableBean.tableHeading}" />
</c:when>
<c:when test="${not empty searchHeading}">
<c:set var="tableHeading" value="${searchHeading}" />
</c:when>
<c:otherwise>
<c:set var="tableHeading" value="&nbsp;" />
</c:otherwise>

</c:choose>
<c:set var="enableJSobject" value="${tableBean.enableJSobject}" />
<c:set var="rowClick" value="${tableBean.onRowClick}" />
<c:set var="phoneContent" value="${tableBean.phoneContent}" />
<script type="text/javascript">
	var currentPageTotal = 0;
	var currentPageTotalItems = [];
	var tableTotal = 0;
	var enabableTotal = 'false';
</script>
<!----------------------------------------- items ----------------------------------------->
<c:set var="idCounter" value="-1" />
<c:if test="${empty tableBean.items}">
	
	<jsp:useBean id="emptygroup" class="fnb.online.tags.beans.table.TableRowGroup" />
	
	<base:tableHeader tableBean="${tableBean}" action="yes" rowGroupHeader="true" tableRowGroup="${emptygroup}">
		<!-- the last class, tableRowHeader, is required for selection purposes, but doesn't require an actual css definition as such. MG -->
		<base:tableCellGroupHeader content="${tableHeading }" imgSrc="${group.code}" groupIndex="${groupIndex.index}" downloadUrl="${downloadUrl}" />
		<!-- the key is the header value -->
	</base:tableHeader>
	
	<base:tableHeader action="yes">
		<c:set var="columnCounter" value="0" />
		<c:set var="descriptionCounter" value="1" />
		<c:set var="headingsHaveGroup" value="false"/>
		
		<c:forEach items="${tableBean.tableOptions.columnGroups}" var="headingColumnGroup">
			<div id="rowGroup_${groupCounter}_${headingColumnGroup.groupName}_heading_group" class="tableHeaderInner groupColCell groupedCol${descriptionCounter} clearfix">
				<div class="groupedHeaderInner <c:if test='${headingColumnGroup.groupBorderLeft == true}'>borderLeft</c:if> <c:if test='${headingColumnGroup.groupBorderRight == true}'>borderRight</c:if> clearfix">
					<c:if test="${tableBean.tableOptions.showColumnGroupDesc}">
						<div class="headingGroup">
							<div class="headingGroupInner">
								<c:choose>
									<c:when test="${!empty headingColumnGroup.groupDescription}">${headingColumnGroup.groupDescription}<c:set var="headingsHaveGroup" value="true"/></c:when>
									<c:otherwise>
									&nbsp;
								</c:otherwise>
								</c:choose>
							</div>
						</div>
					</c:if>
					<c:forEach items="${headingColumnGroup.columnOptions}" var="option">
						<c:set var="columnCounter" value="${columnCounter+1}" />
						<base:tableCellHeader tableId="${id}" columnOptions="${option}" columnIndex="${columnCounter}" tableRowGroup="${group}" groupCount="${groupCounter}" hasGroup="${headingsHaveGroup}" />
					</c:forEach>
				</div>
			</div>
			<c:set var="descriptionCounter" value="${descriptionCounter+1}" />
		</c:forEach>
		
	</base:tableHeader>
	
	<c:choose>
		<c:when test="${empty tableBean.tableOptions.emptyDataSetMessage}">
			<base:paragraph className="tableNoDataText">There are no items to display.</base:paragraph>
		</c:when>
		<c:otherwise>
			<base:paragraph className="tableNoDataText">${tableBean.tableOptions.emptyDataSetMessage}</base:paragraph>
		</c:otherwise>
	</c:choose>
	
</c:if>

<c:set var="groupCounter" value="0" />
<c:forEach items="${tableBean.rowGroups}" var="group" varStatus="groupIndex">

	<c:set var="tableHeadingString" value="${group.heading}" />
	
	<!-- iterate through the groups -->
	<c:choose>
		<c:when test="${empty group.heading && !empty tableHeading}">
			<c:set var="tableHeadingString" value="${tableHeading}" />
		</c:when>
		<c:otherwise>
			<c:set var="tableHeadingString" value="${group.heading}" />
		</c:otherwise>
	</c:choose>
	<c:if test="${!empty tableHeadingString||!empty downloadUrl}">
		<base:tableHeader tableBean="${tableBean}" action="yes" rowGroupHeader="true" tableRowGroup="${group}">
			<!-- the last class, tableRowHeader, is required for selection purposes, but doesn't require an actual css definition as such. MG -->
			<base:tableCellGroupHeader headerCustomContent="${headerContent}" content="${tableHeadingString}" imgSrc="${group.code}" groupIndex="${groupIndex.index}" downloadUrl="${downloadUrl}" />
			<!-- the key is the header value -->
		</base:tableHeader>
	</c:if>
	
	<base:tableHeader index="${groupIndex.index}" action="yes">
		<c:set var="columnCounter" value="0" />
		<c:set var="descriptionCounter" value="1" />
		<c:set var="headingsHaveGroup" value="false"/>
		
		<c:forEach items="${tableBean.tableOptions.columnGroups}" var="headingColumnGroup">
			<div id="rowGroup_${groupCounter}_${headingColumnGroup.groupName}_heading_group" class="tableHeaderInner groupColCell groupedCol${descriptionCounter} clearfix">
				<div class="groupedHeaderInner <c:if test='${headingColumnGroup.groupBorderLeft == true}'>borderLeft</c:if> <c:if test='${headingColumnGroup.groupBorderRight == true}'>borderRight</c:if> clearfix">
					<c:if test="${tableBean.tableOptions.showColumnGroupDesc}">
						<div class="headingGroup">
							<div class="headingGroupInner">
								<c:choose>
									<c:when test="${!empty headingColumnGroup.groupDescription}">${headingColumnGroup.groupDescription}<c:set var="headingsHaveGroup" value="true"/></c:when>
									<c:otherwise>
									&nbsp;
								</c:otherwise>
								</c:choose>
							</div>
						</div>
					</c:if>
					<c:forEach items="${headingColumnGroup.columnOptions}" var="option">
						<c:set var="columnCounter" value="${columnCounter+1}" />
						<base:tableCellHeader disableSorting="${noSorting}" tableId="${id}" columnOptions="${option}" columnIndex="${columnCounter}" tableRowGroup="${group}" groupCount="${groupCounter}" hasGroup="${headingsHaveGroup}" />
					</c:forEach>
				</div>
			</div>
			<c:set var="descriptionCounter" value="${descriptionCounter+1}" />
		</c:forEach>
	</base:tableHeader>
	
	<c:set var="isRows" value="false" />
	
	<c:forEach items="${group.tableRows}" var="row">
		<c:set var="isRows" value="true" />
		<c:set var="idCounter" value="${row.counter}" />
		<c:set var="tableRowId" value="${tableBean.tableId}_row_${idCounter + 1}" />
		
		<!-- the values are an array list of rows, iterate through each  -->
		<base:tableRow onClickFunction="${rowClick}" id="${idCounter+1}" className="${row.cssClass}" groupCount="${groupCounter}">

			<div class="tableRowInner clearfix">
				<c:set var="counter" value="0" />
				<c:set var="cellGroupCounter" value="1" />
				<c:forEach items="${tableBean.tableOptions.columnGroups}" var="columnGroup">
					<div id="${columnGroup.groupName}_column_group" class="tableGroup groupColCell ${columnGroup.groupName} groupedCol${cellGroupCounter}<c:if test='${columnGroup.hideForMobile}'> hideForMobile</c:if> clearfix ">
						
						<c:if test="${browserInfo.capabilities['fnb_is_mobile']}">
						
							<c:if test="${columnGroup.hideForMobile}">
								<div class="phoneContentButton" onclick="fnb.utils.mobile.table.expandRow(this)"></div>
							</c:if>
						
						</c:if>
						
						<div class="tableRowGroupInner">
							<c:forEach items="${columnGroup.columnOptions}" var="columnOption">
								<c:set var="fieldName" value="${columnOption.fieldName}" />
								<c:set var="counter" value="${counter+1}" />
								<c:set var="showHideEnabled" value="false" />
								<c:if test="${not empty columnOption.cssClass}">
									<c:set var="showHideEnabled" value="true" />
								</c:if>
								<c:set var="rowColumnCssClass" value="<%=
                                        ((fnb.online.tags.beans.table.TableRow)jspContext.getAttribute(\"row\"))
                                        .getItem((String) jspContext.getAttribute(\"fieldName\"))
                                        .getCssClass()
                                 %>"/>
								<div class="tableCell col${counter}<c:if test='${columnOption.selectAll}'> selectAllActive</c:if>  ${columnOption.align}  ${columnOption.hideColumn} ${columnOption.cssClass} ${rowColumnCssClass}" data-value="tableHeaderRow_${groupIndex.index}">
									
									<%-- Ezi buttons get loaded into this TAG!!! --%>
									<base:tableCell newFrameWorkFlag="${newFrameWorkFlag}" showHideColumns="${showHideEnabled}" colCount="${counter}" enableJSobject="${enableJSobject}" tableRowId="${tableRowId}" count="${idCounter}" tableRow="${row}" columnOptions="${columnOption}" isActionMenu="${isActionMenu}" eziItemClickLoad="${eziItemClickLoad}" />
								</div>
							</c:forEach>
						</div>
					</div>
					
					<c:set var="cellGroupCounter" value="${cellGroupCounter+1}" />
					
				</c:forEach>
				
			</div>
			
			<c:if test="${browserInfo.capabilities['fnb_is_mobile']}">
			
				<c:if test="${!empty phoneContent}">
					<jsp:include page='${phoneContent}' />
				</c:if>
			
			</c:if>
			
		</base:tableRow>

		
			<div id="expandableRow_${tableRowId}" class="expandableTableRow"></div>
			

			
	</c:forEach>
	
	<c:if test="${tableBean.tableOptions.showTotalFooter && isRows=='true'}">
		<base:tableRow id="total_${groupCounter}" className="totalRow">
			<div class="tableRowInner clearfix">
				<c:set var="counter" value="0" />
				<c:set var="cellGroupCounter" value="1" />
				<c:set var="headCount" value="1" />
				<c:forEach items="${tableBean.tableOptions.columnGroups}" var="columnGroup">
					<div id="${columnGroup.groupName}_column_group" class="tableGroup groupColCell ${columnGroup.groupName} groupedCol${cellGroupCounter} clearfix ">
						<c:forEach items="${columnGroup.columnOptions}" var="columnOption">
							<c:set var="counter" value="${counter+1}" />
							<div class="tableCell col${counter} ${columnOption.align} ${columnOption.hideColumn}">
								
								<base:bnkTableTotalFooter action="yes" tableRowGroup="${group}" columnOptions="${columnOption}" number="${headCount}" />
								
							</div>
							<c:set var="headCount" value="${headCount+1}" />
						</c:forEach>
					</div>
					<c:set var="cellGroupCounter" value="${cellGroupCounter+1}" />
				</c:forEach>
			</div>
		</base:tableRow>
	</c:if>
	<c:if test="${not empty group.tableGroupButtonItems}">
		<base:tableRowButtonGroup id="rowButtonGroup_${groupCounter}"  className="buttonGroupRow">
			<c:forEach items="${group.tableGroupButtonItems}" var="tableGroupButtonItem" varStatus="status">
				<c:if test='${not empty tableGroupButtonItem.url}'>
					<base:tableRowButton id="rowButton_${status.index}" newFrameWorkFlag = "${newFrameWorkFlag }" className="buttonRow" url="${tableGroupButtonItem.url}" label="${tableGroupButtonItem.text}" target="${tableGroupButtonItem.target}">
					</base:tableRowButton>
				</c:if>
			</c:forEach>
		</base:tableRowButtonGroup>
	</c:if>
	
	<c:set var="groupCounter" value="${groupCounter+1}" />

</c:forEach>

<base:footerMessage id="footerMessage" text="You are about to pay"></base:footerMessage>

<script type="text/javascript">

	var tableTotalFloatInt = parseFloat(tableTotal);


	//Removed next line as was messing up TOTALS. PS-11533
	//tableTotal -= currentPageTotal;  

	if (tableTotalFloatInt > 0) {
		if ($('#footerMessage').hasClass('hideElement')) $('#footerMessage').removeClass('hideElement');
		$('#footerMessageTotal').text(tableTotalFloatInt.toFixed(2))
	}
</script>

<base:bnkPaging tableBean="${tableBean}" />