<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<div<c:if test="${not empty id}"> id="${id}"</c:if> class="gridGroup grid100 ${className} clearfix"><jsp:doBody /></div>