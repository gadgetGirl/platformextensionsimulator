<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="label" required="false" rtexprvalue="true"%>
<%@ attribute name="tableId" required="true" rtexprvalue="true"%>
<%@ attribute name="target" required="false" rtexprvalue="true"%>
<%@ attribute name="tableBean" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableBean"%>

<c:choose>
	<c:when test="${not empty target}">
		<c:set var="targetValue" value="${target}"/>
	</c:when>
	<c:otherwise>
		<c:set var="targetValue" value="3"/>
	</c:otherwise>
</c:choose>

 

	<div class="tableSearchBarWrapper">
	<c:if test="${tableBean.tableOptions.showSearchFilter == true}" >
		<base:input label="${label}" className="searchField" id="${tableId}_searchField" value="" defaultValue="Search"/>
		
	</c:if>
	
	<c:if test="${tableBean.tableOptions.showAdvancedLink == true && not empty tableBean.tableOptions.advancedSearchUrl }" >
		<base:eziLink className="tableAdvancedSearch" text="Advanced Search" url="${tableBean.tableOptions.advancedSearchUrl}" target="${targetValue}" />
	</c:if>
	</div>