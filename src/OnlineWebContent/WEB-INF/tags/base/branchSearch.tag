<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="fnb.hyperion.banking.forms" tagdir="/WEB-INF/tags/banking/forms" %>

<%@ attribute required="true"  rtexprvalue="true" name="lowerCodes" type="java.util.Vector"%>
<%@ attribute required="true"  rtexprvalue="true" name="bankNames" type="java.util.Vector"%>
<%@ attribute required="true"  rtexprvalue="true" name="nums" type="java.util.Vector"%>

<%@ attribute required="false" rtexprvalue="true" name="type"%>
<%@ attribute required="false" rtexprvalue="true" name="editCode"%>
<%@ attribute required="false" rtexprvalue="true" name="editName"%>

<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="disabled"%>

<%@ attribute required="false" rtexprvalue="true" name="labelPosition" description="DEPRECATED"%>


<script type="text/javascript">
	fnb.functions.branchSearch.initialize('${lowerCodes}','${bankNames}','${nums}', '${xRefRanges}', '${country}');
</script>

<div class="formElementWrapper branchcodesearch input-wrapper<c:if test="${type == 'hidden'}"> hidden</c:if><c:if test='${not empty disabled}'> disabled</c:if>  clearfix ${className}">
	<div class="formElementLabel gridCol grid40 input-label"><div class="input-label-inner">Bank</div></div>
	<div class="formElementContainer gridCol grid60">
		<div class="gridCol grid40">
			<base:formsButton type="button" text="Choose a Bank" id="branchSearchButton" onclick="fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '/banking/Controller?nav=navigator.branchsearch.simple.BranchSearchLanding'});" />
		</div>
		<div class="searchOrText gridCol grid10">or</div>
		<div id="BranchSearchCode" class="gridCol grid50 input-container clearfix">
			<div class="input-container-inner clearfix bankNameInput">
				<%-- <fnb.hyperion.banking.forms:input.number id="branchSearchCode" maxlength="11" className="searchInput-input input-input" value="${editCode}" placeholder="Enter branch code" simple="true" />
				<input id="branchSearchCode" maxlength="11" class="searchInput-input input-input" name="branchSearchCode" value="${editCode}" onblur="fnb.functions.branchSearch.inputBlur(${country})" onkeydown="return fnb.functions.branchSearch.inputKeypress(this,event)" />  --%>
			<input id="branchSearchCode" name="branchSearchCode" class="input validationTarget" data-type="number" data-placeholderis="true" data-placeholder="Enter branch code" value="Enter branch code" maxlength="11" onblur="fnb.functions.branchSearch.inputBlur(${country})" onkeydown="return fnb.functions.branchSearch.inputKeypress(this,event)">
			</div>
		</div>
		<div class="inputNote grey2 branchSearchBank">
			<div class="inputNoteArrow"></div>
			<div class="inputNoteInner">
				<input id="branchSearchName" name="branchSearchName" class="bankNameInput" disabled="disabled" value="${ ( empty editName ) ? 'Not Selected' : editName}" />
			</div>
		</div>
	</div>
</div>