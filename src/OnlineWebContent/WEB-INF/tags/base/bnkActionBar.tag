<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="bean" required="true" rtexprvalue="true" type="fnb.online.tags.beans.actionbar.ActionBarBean"%>
<%@ attribute name="id" required="true" rtexprvalue="true"%>
<div class="pageGroup" id="${id}">
	
	<base:divContainer id="actionCols" className="clearfix" >
		<base:gridGroup innerClass="actionMenu" id="${id}Grid">
		<base:gridQuart position="firstChild" >
			<base:actionMenuMainHeading headingText="${bean.optionHeading}" color="amberBg" />
			
			<base:actionMenuCol width="singleWidth" custom="equalHeight">
				<base:actionMenuItems items="${bean.options}" color="amber" />
			</base:actionMenuCol>
		</base:gridQuart>
		<base:divContainer id="actionSubMenu">
			<div>Close menu</div>
			<base:divContainer id="actionAjaxContainer">

			</base:divContainer>
		</base:divContainer>
		<c:if test="${bean.displayMoreOptions}">
			<base:gridThreeQuart position="lastChild" >
				<base:actionMenuMainHeading headingText="${bean.moreOptionHeading}" color="tealBg" />
				<base:actionMenuCol width="thirdWidth" custom="equalHeight">
					<base:actionMenuItems items="${bean.moreOptionsColumnOne}" color="teal" />
				</base:actionMenuCol>
				<base:actionMenuCol width="thirdWidth" custom="equalHeight">
					<base:actionMenuItems items="${bean.moreOptionsColumnTwo}" color="teal" />
				</base:actionMenuCol>
				<base:actionMenuCol width="thirdWidth" className="lastCol" custom="equalHeight">
					<base:actionMenuItems items="${bean.moreOptionsColumnThree}" color="teal" />
				</base:actionMenuCol>
			</base:gridThreeQuart>
		</c:if>
	</base:gridGroup>
	</base:divContainer>
	<base:divContainer id="actionBarFooterTop">
		<base:gridQuart position="firstChild">
			<base:actionMenuMainFooter headingText="" color="amberBg" />
		</base:gridQuart>
		<base:gridThreeQuart position="lastChild">
			<base:actionMenuMainFooter headingText="" color="tealBg" />
		</base:gridThreeQuart>
	</base:divContainer>
	<base:divContainer id="actionBarFooter">
	
		<base:divContainer className="actionBarFooterTop">
		</base:divContainer>
	</base:divContainer>
</div>
<script type="text/javascript">
if(isMobile.iOS()) {
	equalCols.sizeColumns($('#actionCols'),'multi');
} else {
	equalCols.sizeColumns($('#actionCols'),'multi');
	//equalCols.sizeColumns($('#actionSubMenu'),'single',44);
}
</script>
