<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="name" required="true" rtexprvalue="true"%>
<%@ attribute name="action" required="true" rtexprvalue="true"%>
<%@ attribute name="method" required="true" rtexprvalue="true"%>
<%@ attribute name="onsubmit" required="false" rtexprvalue="true"%>
<%@ attribute name="enctype" required="false" rtexprvalue="true"%>
<%@ attribute name="nav" required="false" rtexprvalue="true"%>
<%@ attribute name="target" required="false" rtexprvalue="true"%>

<%-- Please use base:form --%>

<base:form name="${name}" action="${action}" method="${method}" onsubmit="${onsubmit}" nav="${nav}" enctype="${enctype}" target="${target}"><jsp:doBody /></base:form>