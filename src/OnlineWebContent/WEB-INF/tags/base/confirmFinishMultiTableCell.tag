<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="id" required="false"%>
<%@ attribute name="className" required="false"%>
<%@ attribute name="value" required="false"%>
<%@ attribute name="heading" required="false"%>
<%@ attribute name="tooltip" required="false"%>
<%@ attribute name="tooltipDistance" required="false"%>

<c:set var="divTop">
<div class="toolTipTweak">
</c:set>
<c:set var="divBottom">
</div>
</c:set>

<c:if test="${not empty tooltipDistance}">
	<style>.multiTableCellTooltip{margin: 0 0 0 ${tooltipDistance}px;}</style>
</c:if>
<div class="tableCell grid50 <c:if test="${not empty className}">${className}</c:if>" <c:if test="${not empty id}"> id="${id}"</c:if>>
	
	<c:if test="${not empty tooltip}">${divTop}</c:if>	
	<c:if test="${not empty heading}">
		<div class="tableCellItem doubleItemTop stack <c:if test='${not empty tooltip}'>tooltipPad</c:if>">
			${heading}&nbsp;
			
		</div>
	</c:if>
	<div class="tableCellItem doubleItemBottom stack <c:if test='${not empty tooltip}'>tooltipPad</c:if>">${value}<jsp:doBody />&nbsp;</div>

	<c:if test="${not empty tooltip}">${divBottom}</c:if>	
	<c:if test="${not empty tooltip}">
				<base:tooltip className="confirmFinishTooltip ${empty heading ? 'singleRowTooltip' : 'doubleRowTooltip'}" content="${tooltip}"/>
			</c:if>	
</div>