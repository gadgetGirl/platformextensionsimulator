<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute required="true"  rtexprvalue="true" name="description" %>
<%@ attribute required="false"  rtexprvalue="true" name="value" %>

<base:divContainer className="formElementLabel">${description}</base:divContainer>
<base:divContainer className="formElementLabel marginBottom10">${value}<jsp:doBody /></base:divContainer>