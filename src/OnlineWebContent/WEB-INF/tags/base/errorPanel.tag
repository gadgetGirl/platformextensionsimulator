<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<div id="errorPanel" class="error hidden">
<div class="errorPanelInner">
	<div id='errorButtonWrapper' class='errorButtonWrapper'></div>
	<div id='errorMessageWrapper' class='errorMessageWrapper'></div>
	<div id='errorsWrapper' class='errorsWrapper clearfix'></div>
	</div> 
</div>