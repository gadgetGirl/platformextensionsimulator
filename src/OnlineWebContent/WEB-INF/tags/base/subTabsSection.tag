<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="number" required="true" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<div id="currentSubtab" class="subTab subTab${number} ${className} <c:if test="${number=='1'}">first</c:if> clearfix"><jsp:doBody /></div>