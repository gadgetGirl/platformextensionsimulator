<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false" name="id" %>
<%@ attribute required="false" name="className" %>
<%@ attribute required="true"  name="label" %>
<%@ attribute required="false" name="formToSubmit" %>
<%@ attribute required="false" name="onclick" %>
<%@ attribute required="false" name="icon" %>		<%-- ICONS: bigCircLeft, bigPlusLeft, bigMinusLeft, smallMinusRight, smallTurqPlus--%>
<%@ attribute required="false" name="url" %>
<div<c:if test='${not empty id}'> id="${id}"</c:if> class="button ${className}" <c:if test="${not empty formToSubmit}">onclick="fnb.functions.submitFormToWorkspace.submit('${formToSubmit}','',this)"</c:if> <c:if test='${not empty onclick}'> onclick="${onclick}"</c:if>>
	<div<c:if test='${not empty icon}'> class="${icon}"</c:if>>${label}</div>
	<jsp:doBody />
</div>