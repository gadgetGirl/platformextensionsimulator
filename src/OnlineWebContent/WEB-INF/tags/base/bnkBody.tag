<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<body id="banking" class="clearfix">
	<base:progressBar id="Standard"/>
	<base:eziPanel/>
	<base:logOffCountdown/>
	<base:slowConnectionPopup/>
	<base:mobileLandscapeError/>
	<div id="bodyGlobalWidth" class="bodyGlobalWidth">
		<base:errorPanel/>
		<base:overlay/>
		<jsp:doBody></jsp:doBody>
		<base:popup/>
	</div>
	<base:calendarWrapper/> 
</body>