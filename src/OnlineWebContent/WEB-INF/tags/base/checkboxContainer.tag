<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="true"  name="id"%>
<%@ attribute required="false" name="className" description="optional className: greyBg"%>
<%@ attribute required="false" name="toggleAll"%>
<%@ attribute required="false" name="toggleAllLabel"%>
<%@ attribute required="false" name="groupLabel"%>

<c:if test="${toggleAll == 'true'}">
	<div class="toggleAll" onclick="checkBox.toggleAll('#${id}')">
		${toggleAllLabel}
		<input class="selectAllInput" type="hidden" value="false" />
	</div>
</c:if>
<div id="${id}" class="checkboxGroup ${className} formElementWrapper">
	<jsp:doBody />
	<%--
	<div class="errorConsoleContainer">
		<div class="errorConsoleArrow"></div>
		<div class="errorConsole">Something happened and this is the error</div>
	</div>
	--%>
</div>