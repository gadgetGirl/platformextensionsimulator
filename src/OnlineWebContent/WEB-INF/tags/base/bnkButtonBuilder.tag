<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="buttonList" required="false" rtexprvalue="true" type="mammoth.jsp.viewbean.ButtonViewBeanList"%>

<c:if test="${not empty buttonList}">
	<base:footerButtonGroup>
		<c:if test="${buttonList.list}">
			<c:forEach var="item" items="${buttonList.list}">
					<base:footerButton className="main" text="${item.labelCaption}"  
									formToSubmit="${item.formToSubmit}" 
									onclick="${item.onClick}" 
									url="${item.url}" />
			</c:forEach>
		</c:if>
	</base:footerButtonGroup>
</c:if>

