<%@ tag import="fnb.online.tags.beans.table.TableTextItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableTextItem"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>

<c:choose>
	<c:when test="${tableDoubleItem == true}">
		${tableItem.text}
	</c:when> 
	<c:otherwise>
		<div id="${tableItem.id}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size} <c:if test='${tableItem.displayStyle > 0}'>warning${tableItem.displayStyle}</c:if> <c:if test='${not empty tableItem.toolTipMessage}'>tooltipPad</c:if>" >
			<c:choose>
				<c:when test="${not empty tableItem.text}">
					${tableItem.text}<c:if test='${not empty tableItem.toolTipMessage}'>
						<base:tooltip content="${tableItem.toolTipMessage}"/>
					</c:if>
				</c:when>
			</c:choose>
		</div>
	</c:otherwise>
</c:choose>