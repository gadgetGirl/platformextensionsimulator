<%@ taglib prefix="uitags" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ tag import="mammoth.cardmaintenance.viewbean.CardTypeRadioItemViewBean"%>

<%@ attribute name="cardReplacementBean" required="false" rtexprvalue="true" type="mammoth.cardmaintenance.viewbean.CardReplacementViewBean" %>
<%@ attribute name="showPostal" required="false" rtexprvalue="true" type="java.lang.String" %>
<%@ attribute name="showCourier" required="false" rtexprvalue="true" type="java.lang.String" %>
<%@ attribute name="showBranch" required="false" rtexprvalue="true" type="java.lang.String" %>
<%@ attribute name="showPrivateFNB" required="false" rtexprvalue="true" type="java.lang.String" %>
<%@ attribute name="showPrivateRMB" required="false" rtexprvalue="true" type="java.lang.String" %>

<% 
	showPostal = "false";
	showCourier = "false";
	showBranch = "false";
	showPrivateFNB = "false";
	showPrivateRMB = "false";
	
	for(CardTypeRadioItemViewBean item: cardReplacementBean.getCardTypeRadioItems(cardReplacementBean.getCardType())){
		if(item.getMethod().equalsIgnoreCase("P")){
			showPostal = "true";
		}
		
		if(item.getMethod().equalsIgnoreCase("C")){
			showCourier = "true";
		}
		
		if(item.getMethod().equalsIgnoreCase("B")){
			showBranch = "true";
			if(cardReplacementBean.isPrivateClientFNB() == true){
				showPrivateFNB = "true";
			}else if(cardReplacementBean.isPrivateClientRMB()==true){
				showPrivateRMB = "true";
			}
			
		}
		

		if(item.getMethod().equalsIgnoreCase("PFNB")){
			showBranch = "false";
			if(cardReplacementBean.isPrivateClientFNB() == true){
				showPrivateFNB = "true";
			}else if(cardReplacementBean.isPrivateClientRMB()==true){
				showPrivateRMB = "true";
			}
			
		}
		

		if(item.getMethod().equalsIgnoreCase("PRMB")){
			showBranch = "false";
			if(cardReplacementBean.isPrivateClientFNB() == true){
				showPrivateFNB = "true";
			}else if(cardReplacementBean.isPrivateClientRMB()==true){
				showPrivateRMB = "true";
			}
			
		}
		
	}
%>

<base:radioButtonContainer className="margin50" id="cardDelivery" name="cardDelivery" value="${cardReplacementBean.deliveryMethod}" >	
	<base:radioButtonItem id="postalRadio" description="Post to me" value="P" onclick="fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},{show:'false',element:'.collectFromAnFNBBranch'},{show:'true',element:'.postItToMe'},{show:'false',element:'.privateClientBranchFNB'},{show:'false',element:'.privateClientBranchRMB'}])" selectedValue="${cardReplacementBean.deliveryMethod}"/>
	<base:radioButtonItem id="courierRadio" description="Deliver to me" value="C" onclick="fnb.functions.showHideToggleElements.showHideToggle([{show:'true',element:'.deliveredToMe'},{show:'false',element:'.collectFromAnFNBBranch'},{show:'false',element:'.postItToMe'},{show:'false',element:'.privateClientBranchFNB'},{show:'false',element:'.privateClientBranchRMB'}])" selectedValue="${cardReplacementBean.deliveryMethod}"/>
	<base:radioButtonItem id="branchRadio" description="Collect from an FNB Branch" value="B" onclick="fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},{show:'true',element:'.collectFromAnFNBBranch'},{show:'false',element:'.postItToMe'},{show:'false',element:'.privateClientBranchFNB'},{show:'false',element:'.privateClientBranchRMB'}])" selectedValue="${cardReplacementBean.deliveryMethod}"/>
	<c:if test="${cardReplacementBean.privateClientFNB == true}">
	<base:radioButtonItem id="privateRadioFNB" description="FNB Private Clients Suite" value="PFNB" onclick="fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},{show:'false',element:'.collectFromAnFNBBranch'},{show:'false',element:'.postItToMe'},{show:'true',element:'.privateClientBranchFNB'},{show:'false',element:'.privateClientBranchRMB'}])"  selectedValue="${cardReplacementBean.deliveryMethod}"/>
	</c:if>
	<c:if test="${cardReplacementBean.privateClientRMB == true}">
	<base:radioButtonItem id="privateRadioRMB" description="RMB Private Bank Suite" value="PRMB" onclick="fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'.deliveredToMe'},{show:'false',element:'.collectFromAnFNBBranch'},{show:'false',element:'.postItToMe'},{show:'true',element:'.privateClientBranchRMB'}])"  selectedValue="${cardReplacementBean.deliveryMethod}"/>	
	</c:if>
</base:radioButtonContainer>

<script type="text/javascript">
	//author L.M
	function showHideOnLoad(){
		var showPostal = "${showPostal}";
		var showCourier = "${showCourier}";
		var showBranch = "${showBranch}";
		var showPrivateFNB = "${showPrivateFNB}";
		var showPrivateRMB = "${showPrivateRMB}";

		fnb.functions.showHideToggleElements.showHideToggle([{show:showPostal,element:'#postalRadio'}, 
		                   {show:showCourier,element:'#courierRadio'}, 
		                   {show:showBranch,element:'#branchRadio'}, 
		                   {show:showPrivateFNB,element:'#privateRadioFNB'}, 
		                   {show:showPrivateRMB,element:'#privateRadioRMB'}]);
	}
	showHideOnLoad();
</script>	
