<%@tag import="fnb.online.tags.beans.table.TableTextItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableTextItem"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>


<c:if test="${tableItem.processingResultViewBean != null and not empty tableItem.processingResultViewBean.footerButtons}">
	<c:set var="footerButtonsResult" value="${tableItem.processingResultViewBean.footerButtons}" scope="request"/>
</c:if>
<c:if test="${tableDoubleItem == true}">
   ${tableItem.text}
</c:if>
<c:if test="${tableDoubleItem == false and tableItem.processingResultViewBean != null}">
	<c:choose>
		<c:when test="${tableItem.processingResultViewBean.errorMessage != null && !tableItem.processingResultViewBean.errorMessage['empty'] && tableItem.processingResultViewBean.errorCode > 0 && !tableItem.processingResultViewBean.pending}">
			<div id="${tableItem.id}" class="tableCellItem ${columnOptions.size} badStatus">
				${tableItem.processingResultViewBean.errorMessage}
			</div>
		</c:when>
		<c:when test="${tableItem.processingResultViewBean.errorMessage != null && !tableItem.processingResultViewBean.errorMessage['empty'] && tableItem.processingResultViewBean.errorCode > 0 && !tableItem.processingResultViewBean.pending}">
			<div id="${tableItem.id}" class="tableCellItem ${columnOptions.size} goodStatus">
				${tableItem.processingResultViewBean.errorMessage} ${tableItem.processingResultViewBean.traceId} 
			</div>
		</c:when>
		<c:when test="${tableItem.processingResultViewBean.errorCode == 0 && !tableItem.processingResultViewBean.pending}">
			<div id="${tableItem.id}" class="tableCellItem ${columnOptions.size} goodStatus">
				${tableItem.processingResultViewBean.processingMessage} ${tableItem.processingResultViewBean.traceId} 
			</div>
		</c:when>
		<c:when test="${tableItem.processingResultViewBean.pending && tableItem.processingResultViewBean.errorCode == 0}">
			<div id="${tableItem.id}" class="tableCellItem ${columnOptions.size} pendingStatus">
				${tableItem.processingResultViewBean.processingMessage}
			</div>
		</c:when>
		<c:otherwise>
			<div id="${tableItem.id}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size} badStatus">Processing Failed</div>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${tableItem.processingResultViewBean == null}">
	<div id="${tableItem.id}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size} badStatus">Processing Failed</div>
</c:if>