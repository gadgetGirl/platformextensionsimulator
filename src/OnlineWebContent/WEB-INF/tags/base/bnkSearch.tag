<!-- THIS TAG HAS BEEN DEPRECATED IN FAVOUR OF THE base:tableSearchBar.tag. MG -->


<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="tableId" required="true" rtexprvalue="true"%>

<%@ attribute name="advancedSearchUrl" required="false" rtexprvalue="true"%>
<%@ attribute name="showAdvancedLink" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>


	<c:if test="${showAdvancedLink == 'true'}">
		<base:eziLink text="Advanced Search" url="${advancedSearchUrl}" target="panel" />
	</c:if>

	<base:input id="${tableId}_searchField" defaultValue="Search" className="${className}" value=""/>
