<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ tag import="fnb.online.tags.beans.table.TableOptions"%>
<%@ tag import="fnb.online.tags.beans.table.BeanUtilities"%>

<%@ attribute name="advancedSearchUrl" required="false" rtexprvalue="true"%>
<%@ attribute name="showAdvancedLink" required="false" rtexprvalue="true"%>
<%@ attribute name="showSearchFilter" required="false" rtexprvalue="true"%>

<%   

String optionsId = BeanUtilities.getFieldValuePairs(getParent(),false).get("id");
TableOptions tableOptions = (TableOptions) request.getAttribute(optionsId);
tableOptions.setAdvancedSearchUrl(advancedSearchUrl);
tableOptions.setShowAdvancedLink(Boolean.parseBoolean(showAdvancedLink));
tableOptions.setShowSearchFilter(Boolean.parseBoolean(showSearchFilter));

%>