<%@ tag import="fnb.online.tags.beans.dropdown.*"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="dropdownBean" required="false" rtexprvalue="true" type="fnb.online.tags.beans.dropdown.DropDown"%>

<%@ attribute required="true" rtexprvalue="true"  name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="name" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="overflow" %>
<%@ attribute required="false" rtexprvalue="true" name="label" %>
<%@ attribute required="false" rtexprvalue="true" name="labelWidth" %>
<%@ attribute required="false" rtexprvalue="true" name="labelTop" %>
<%@ attribute required="false" rtexprvalue="true" name="selectWidth" %>
<%@ attribute required="false" rtexprvalue="true" name="searchClass" %>
<%@ attribute required="false" rtexprvalue="true" name="selectedValue" %>

<%@ attribute required="false" rtexprvalue="true" name="color" description="Default: white, Alt: teal"%><%-- unique to singleTier --%>
<%@ attribute required="false" rtexprvalue="true" name="onchange" %><%-- not yet fully tested, added for backwards compatibility --%>
<%@ attribute required="false" rtexprvalue="true" name="margin" %>

<%@ attribute required="false" rtexprvalue="true" name="disabled" %>
<%@ attribute required="false" rtexprvalue="true" name="reverse" %>
<%@ attribute required="false" rtexprvalue="true" name="search" %>

<%@ attribute required="false" rtexprvalue="true" name="height" %>
<%@ attribute required="false" rtexprvalue="true" name="tooltipContent" %>
<%@ attribute required="false" rtexprvalue="true" name="noteContent"%>

<c:if test="${empty overflow}"><c:set var="overflow" value="''" /></c:if>

<c:set var="searchClass" value="${(not empty searchClass) ? searchClass : 'dropdown-h4'}" />
<c:set var="name" value="${(not empty name) ? name : id}" />
<c:set var="selectedValue" value="${(not empty dropdownBean.selectedValue) ? dropdownBean.selectedValue : selectedValue}"/>
<c:set var="disabledClass" value="${disabled == 'true' ? ' dropdown-disabled' : ''}" />
<c:set var="disabledAttributes">
	<c:choose>
		<c:when test="${disabled == 'true'}">
			data-disabled-overflow="${overflow}" data-disabled-searchClass="${searchClass}" data-disabled-reverse="${reverse}" data-disabled-height="${height}"
		</c:when>
		<c:otherwise>
			<%--onclick="fnb.forms.dropdown.expand($(this),${overflow},'${searchClass}','${reverse}','${height}');" --%>
		</c:otherwise>
	</c:choose>
</c:set>
<c:choose>
	<c:when test="${empty label}">
		<c:set var="containerWidth" value="grid100" />
	</c:when>
	<c:otherwise>
		<c:set var="containerWidth" value="grid60" />
	</c:otherwise>
</c:choose>
<div id="${id}_parent" class="formElementWrapper clearfix singleTierDropDown ${className}${disabledClass} <c:if test='${not empty tooltipContent}'> tooltipPad</c:if>">
<input id="${id}" name="${name}" value="${selectedDropdownValue}" onchange="${valueClickedFunction}" class="dropdown-hidden-input dropdown-keyboard-tab"/>
<c:if test="${not empty label}"><div class="gridCol grid40 formElementLabel">${label}</div></c:if>
<div class="gridCol ${containerWidth} dropdown-select dropdownElementWrapper">
	<div class="downloadValue"></div>
	<div id="${id}_dropId" class="dropdown-initiator" ${disabledAttributes} onclick="fnb.forms.dropdown.expand($(this),${overflow},'${searchClass}','${reverse}','${height}');">
		<div class="dropdown-trigger singleTier-trigger"></div>
		<div class="dropdown-selection-white dropdown-h4"></div>
	</div>
	<div class="dropdown-content-wrapper">
		<c:if test="${not empty label}"><div class="mobiDropdownInnerLabelWrapper">${label}</div><div class="mobiDropdownClose"></div></c:if>
		<ul class="dropdown-content">
			<div class="dropdown-content-top">
				<c:choose>
					<c:when test="${search == false}">
					</c:when>
					<c:otherwise>
						<div class="dropdown-input-wrapper searchBar">
							<div class="dropdown-input-icon-search"></div>
							<div class="dropdown-input-container"><input class="dropdown-input" placeholder="Search..." value="" /></div>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
			<c:choose>
				<c:when test="${not empty dropdownBean.items}">
					<c:forEach items="${dropdownBean.items}" var="item" >
						<c:set var="passSelected" value="${((item.returnValue == selectedValue) || (item.selected == true)) ? true : false}"/>
						<c:if test="${not empty className}"><c:set var="classNameItem" value="${className}_item" /></c:if>
						<base:dropdownSingleTierItem
							id=""
							className=		"${classNameItem}"
							onclick=		"${onchange}"
							content=		"${empty item.topHeading?item.displayValue:item.topHeading}"
							returnValue=	"${item.returnValue}"
							selected=		"${passSelected}" />
					</c:forEach>
				</c:when>
				<c:otherwise>
					<jsp:doBody />
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
	<c:if test="${not empty noteContent}">
		<div class="inputNote teal8">
			<div class="inputNoteArrow"></div>
			<div class="inputNoteInner">${noteContent}</div>
		</div>
	</c:if>
</div>
<c:if test="${not empty tooltipContent}">
<base:tooltip content="${tooltipContent}" />
</c:if>
<c:set var="selectedDropdownValue" />
<c:choose>
	<c:when test="${not empty dropdownBean.selectedValue}">
		<c:set var="selectedDropdownValue" value="${dropdownBean.selectedValue}" />
	</c:when>
	<c:otherwise>
		<c:set var="selectedDropdownValue" value="${selectedValue}" />
	</c:otherwise>
</c:choose>
</div>
<script type="text/javascript">
	var idTemp = "${id}"
	fnb.forms.dropdown.init($('#'+idTemp+'_dropId'),'${selectedDropdownValue}');
</script>