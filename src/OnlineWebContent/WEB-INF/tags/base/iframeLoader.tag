<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="name" required="true" rtexprvalue="true"%>
<%@ attribute name="id" required="true" rtexprvalue="true"%>
<%@ attribute name="src" required="true" rtexprvalue="true"%>

<iframe name="${name}" id="${id}" src="${src}"></iframe>