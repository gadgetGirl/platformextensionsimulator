<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<base:import type="js" link="/banking/mammoth/loggedoff/RegenerateCaptchaImage.js"/>
<%-- No variables needed as this will be the same on all pages --%>
<div id="captcha-wrapper" class="formElementWrapper clearfix ${className}">
	<div class="gridCol grid40 formElementLabel">
		<img src="/banking/Captcha" id="captchaA1" height="70" width="250"/>
	</div>
	<div class="gridCol grid60 formElementContainer input-container-inner">
		<base:formsButton id="CaptchaButton" className="${className} grid60" text="Enter the verification code or try another" onclick="regenerateCaptchaA();return false;"/>
		<div id="captcha-input-container" class="clearfix">
			<input id="capture" name="capture" class="input-input" type="text" autocomplete="off" />
		</div>
	</div>
</div>