<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%--
RATHER USE base:pageHeader
--%>
<%@ attribute required="false" rtexprvalue="true" name="width" description="DEPRECATED" %>
<%@ attribute required="false" rtexprvalue="true" name="id" description="DEPRECATED" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="value" description="Don't set value if you are using extended functionality"%>
<%@ attribute required="false" rtexprvalue="true" name="style" description="DEPRECATED" %>
<%@ attribute required="false" rtexprvalue="true" name="hsize" description="Integer: 1-6" %>
<%@ attribute required="false" name="extended" description="Boolean, default: false" %>
<%@ attribute required="false" rtexprvalue="true" name="color" description="DEPRECATED" %>
<base:pageHeader id="extendedPageHeader" className="${className}" style="${style}" hsize="${hsize}" extended="${extended}" color="${color}" value="${value}">
<jsp:doBody/>
</base:pageHeader>