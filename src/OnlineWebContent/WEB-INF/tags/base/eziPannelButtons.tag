<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<div id="eziPannelButtonsWrapper"><div class="eziWrapperInner"><div id="eziPannelButtons"<c:if test='${not empty className}'> class="${className}"</c:if>><jsp:doBody /></div></div></div>