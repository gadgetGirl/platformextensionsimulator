<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="label" required="false" %>
<%@ attribute name="className" required="false" %>

<div class="actionMenuHeader ${className}">
	<c:if test='${not empty label}'>${label}</c:if><jsp:doBody />
</div>