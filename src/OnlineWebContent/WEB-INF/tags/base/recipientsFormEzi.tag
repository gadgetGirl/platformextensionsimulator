<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="max" required="true" rtexprvalue="true"%>

<%@ attribute name="editViewBean" required="false" rtexprvalue="true" type="fnb.online.mammoth.notifications.NotificationsViewBean"%>

<%@ attribute name="addButtonLabel" required="true" rtexprvalue="true"%>
<%@ attribute name="removeButtonLabel" required="true" rtexprvalue="true"%>

		<div id="addRecipientExtender" class="addRecipientExtender sendNotification clearfix">
				<script>

					var emailLimit = 5;
					var smsLimit = 2;
					var faxLimit = 2;
					
					fnb.functions.notifications.initialize($('#addRecipientExtender'),$('#addRecipientRowParent'),${max},emailLimit,smsLimit,faxLimit);
				</script>

			<c:choose>
				<c:when test="${not empty editViewBean.methodContactInput}">		

						<c:forEach items="${editViewBean.methodContactInput}" var="inputItem" varStatus="inputItemStatus">

							<div id="addRecipientRowParent" class="addRecipientRowParent">
								<base:gridGroup id="addRecipientRow" className="addRecipientRow">

									<base:gridCol width="100">

										<base:dropdownSingleTier color="white" id="selectedTypeInput${inputItemStatus.index}" name="selectedTypeInput${inputItemStatus.index}" className="margin50" label="Method" dropdownBean="${recipientsOptionsList}" selectedValue="${editViewBean.selectedTypeInput[inputItemStatus.index]}">
											
											<base:dropdownSingleTierItem id="dropDownItem_1" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="${editViewBean.selectedTypeInput[inputItemStatus.index]}" returnValue="${editViewBean.selectedTypeInput[inputItemStatus.index]}" selected="true" />
											<c:choose>
												<c:when test="${editViewBean.selectedTypeInput[inputItemStatus.index]=='Email address'}">
													<base:dropdownSingleTierItem id="dropDownItem_2" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Cell number" returnValue="Cell number" selected="false" />
													<base:dropdownSingleTierItem id="dropDownItem_3" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Fax number" returnValue="Fax number" selected="false" />
												</c:when>
												<c:when test="${editViewBean.selectedTypeInput[inputItemStatus.index]=='Cell number'}">
													<base:dropdownSingleTierItem id="dropDownItem_2" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Email address" returnValue="Email address" selected="false" />
													<base:dropdownSingleTierItem id="dropDownItem_3" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Fax number" returnValue="Fax number" selected="false" />
												</c:when>
												<c:when test="${editViewBean.selectedTypeInput[inputItemStatus.index]=='Fax number'}">
													<base:dropdownSingleTierItem id="dropDownItem_2" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Email address" returnValue="Email address" selected="false" />
													<base:dropdownSingleTierItem id="dropDownItem_3" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Cell number" returnValue="Cell number" selected="false" />
												</c:when>
											</c:choose>

										</base:dropdownSingleTier>
										
									<base:input type="hidden" id="methodCodeInput${inputItemStatus.index}" name="methodCodeInput${inputItemStatus.index}" value="" label=""/>
									<c:choose>
										<c:when test="${editViewBean.selectedTypeInput[inputItemStatus.index] == 'Email address'}">
											<base:input className="margin50" type="inner" id="methodContactInput${inputItemStatus.index}" name="methodContactInput${inputItemStatus.index}" value="${editViewBean.methodContactInput[inputItemStatus.index]}" label="${editViewBean.selectedTypeInput[inputItemStatus.index]}"/>
										</c:when>
										<c:otherwise>
											<base:input className="margin50" defaultValue="phoneNumber" type="phone" id="methodContactInput${inputItemStatus.index}"  name="methodContactInput${inputItemStatus.index}" defaultCode="${editViewBean.methodCodeInput[inputItemStatus.index]}" value="${editViewBean.methodContactInput[inputItemStatus.index]}" label="${editViewBean.selectedTypeInput[inputItemStatus.index]}"/>
										</c:otherwise>
									</c:choose>
									
									</base:gridCol>
									
									<base:gridCol id="subjectInputWrapper${inputItemStatus.index}" className="subjectInputWrapper" width="100">
										<base:input maxlength="500" className="margin50" type="inner" id="subjectInput${inputItemStatus.index}" name="subjectInput${inputItemStatus.index}" value="${editViewBean.subjectInput[inputItemStatus.index]}" label="Subject"/>
									</base:gridCol>
																
									<base:gridCol width="100">
									
										<div id="addRecipientsRemoveButton${inputItemStatus.index}" onclick="fnb.functions.notifications.removeRow(this)" class="addRecipientsRemoveButton">
											<a class="addRecipientsRemoveButtonLink">${removeButtonLabel}</a>
										</div>
										
									</base:gridCol>
										<c:if test="${editViewBean.selectedTypeInput[inputItemStatus.index]=='Cell number'}">
											<script>
												fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#subjectInputWrapper${inputItemStatus.index}'}]);
											</script>
										</c:if>
										<c:if test="${inputItemStatus.index == 0}">
											<script>
												fnb.functions.showHideToggleElements.showHideToggle([{show:'false',element:'#addRecipientsRemoveButton${inputItemStatus.index}'}]);
											</script>
										</c:if>
									
								</base:gridGroup>
							</div>

						</c:forEach>
						<script>
							fnb.functions.notifications.overrideElementAttributes(fnb.functions.notifications.parent,'${editViewBean.selectedTypeInput}');
						</script>
				</c:when>
				<c:otherwise>
					
					<div id="addRecipientRowParent" class="addRecipientRowParent">
						<base:equalHeightsRow>
							<base:equalHeightsColumn width="100" className="white">
								<base:gridGroup id="addRecipientRow" className="addRecipientRow">
									
									<base:gridCol width="50">
									
										<base:dropdownSingleTier labelTop="true" color="white" id="selectedTypeInput0" name="selectedTypeInput0" label="Method" dropdownBean="${recipientsOptionsList}" selectedValue="Email address">
											<base:dropdownSingleTierItem id="dropDownItem_1" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Email address" returnValue="Email address" selected="true" />
											<base:dropdownSingleTierItem id="dropDownItem_2" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Cell number" returnValue="Cell number" selected="false" />
											<base:dropdownSingleTierItem id="dropDownItem_3" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Fax number" returnValue="Fax number" selected="false" />
										</base:dropdownSingleTier>
									
									</base:gridCol>
		
									<base:gridCol width="50" className="subjectInputWrapper">
										<base:input type="hidden" id="methodCodeInput0" name="methodCodeInput0" value="" label=""/>
										<base:input className="" type="inner" id="methodContactInput0" name="methodContactInput0" value="" label="Email address"/>
									</base:gridCol>
		
									<base:gridCol className="subjectInputWrapper" width="100">
										<base:input maxlength="500" className="" type="inner" id="subjectInput0" name="subjectInput0" value="" label="Subject"/>
									</base:gridCol>
		
									<base:gridCol width="100">
		
										<div style="display: none" id="addRecipientsRemoveButton${inputItemStatus.index}" onclick="fnb.functions.notifications.removeRow(this)" class="addRecipientsRemoveButton">
											<a class="addRecipientsRemoveButtonLink">${removeButtonLabel}</a>
										</div>
		
									</base:gridCol>
									
								</base:gridGroup>
							</base:equalHeightsColumn>

						</base:equalHeightsRow>
					</div>
	
					<script>
						fnb.functions.notifications.overrideElementAttributes(fnb.functions.notifications.templateElement,"")
						fnb.functions.notifications.currentSelections.push({element:0,objectType:'Email Address'});
					</script>
				</c:otherwise>
			</c:choose>

			<base:equalHeightsRow>
				<base:equalHeightsColumn width="100">
					<div class="addRecipientsAddButtonWrapper" onclick="fnb.functions.notifications.cloneRow(this)">
						<div id="addRecipientsAddButton" class="addRecipientsAddButton">
							<a class="addRecipientsAddButtonLink">${addButtonLabel}</a>
						</div>
					</div>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn className="ghostBlock">
					&nbsp;
				</base:equalHeightsColumn>
			</base:equalHeightsRow>

	</div>