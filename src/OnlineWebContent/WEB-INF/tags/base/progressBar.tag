<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false"  name="id"%>

<div id="ProgressWrapperContainer" class="ProgressWrapperContainer">
	<div id="${id}ProgressWrapper">
	    <div id="${id}ProgressBar"><span id="loading">Loading</span><span id="${id}ProgressBarPercent"></span></div>
	</div>
</div>