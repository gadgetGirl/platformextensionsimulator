<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="true"  rtexprvalue="true" name="value" description="Heading content"%>
<%@ attribute required="true"  rtexprvalue="true" name="level" description="Integer: 1 to 6"%>
<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="style" description="Heading class, NOT html attribute 'style'"%>
<c:if test="${not empty id}"><c:set var="id">id="${id}"</c:set></c:if>
<c:if test="${not empty className || not empty style}"><c:set var="className">class="${className} ${style}"</c:set></c:if>
<c:set var="level" value="${level < 1 || level > 6 ? 1 : level}" />
<h${level} ${id} ${className}>${value}<jsp:doBody /></h${level}>