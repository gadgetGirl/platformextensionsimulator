<%@ attribute name="menu" required="false" rtexprvalue="true"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<base:equalHeightsRow>
	<base:equalHeightsColumn width="100" className="white">
		<div class="subTabsMenu clearfix"><div id="subTabsMenuInner" class="subTabsMenuInner clearfix"><jsp:doBody/></div></div>
	</base:equalHeightsColumn>
	<base:equalHeightsColumn ghostBlock="true" />
</base:equalHeightsRow>