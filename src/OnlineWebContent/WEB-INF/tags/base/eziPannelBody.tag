<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="resultPage"%>

<div<c:if test="${not empty id}"> id="${id}"</c:if> class="eziPage<c:if test="${resultPage == 'true'}"> eziSuccess</c:if> ${className}">
	<jsp:doBody />
</div>