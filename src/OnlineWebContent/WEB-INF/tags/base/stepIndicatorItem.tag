<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>
<%@ attribute required="true" rtexprvalue="true" name="stepCount"%>
<%@ attribute required="true" rtexprvalue="true" name="currentStepNumber"%>
<%@ attribute required="true" rtexprvalue="true" name="stepNumber"%>
<%@ attribute required="true" rtexprvalue="true" name="title"%>
<%@ attribute required="false" rtexprvalue="true" name="desc"%>
<div class="stepIndicatorItemWrapper" style="width:${(100 / stepCount)}%">
	<div class="stepIndicatorItem step${stepNumber} ${stepNumber lt currentStepNumber?'completed':''} ${stepNumber eq currentStepNumber?'currentStep':''} borderImgRightWhite clearfix">
		<div class="stepNumber">${stepNumber}</div>
		<div class="title">${title}</div>
		<c:choose>
			<c:when test="${fn:length(desc) > 40}">
				<div class="desc">${fn:substring(desc, 0, 40)}...</div>
			</c:when>
			<c:otherwise>
				<div class="desc">${desc}</div>
			</c:otherwise>
		</c:choose>
	</div>
</div>