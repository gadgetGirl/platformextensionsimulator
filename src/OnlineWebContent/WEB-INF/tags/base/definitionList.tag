<%@ tag import="fnb.online.tags.beans.definitionList.*"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute required="false" rtexprvalue="true" name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="titleOnTop" %>
<%@ attribute required="false" rtexprvalue="true" name="marginClass" description="DEPRECATED" %>

<div<c:if test="${not empty id}"> id="${id}"</c:if> class="${className} ${titleOnTop ? 'dlOnTop' : ''} dlGroup clearfix"><jsp:doBody /></div>