<%@tag import="fnb.online.tags.beans.table.TableGraphicalItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableGraphicalItem"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="false" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>

<div id="${tableItem.id}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size}" >
	<div style="position:absolute;top:4px;height:160px;z-index:9">
		<img src="${tableItem.src}" alt="${tableItem.alt}" height="${tableItem.height}" width="${tableItem.width}" />
	</div>
	&nbsp;
</div>
