<!--DON
ADDED ATTRIBUTE TO PREVENT SOME SITE WIDE PAGE BREAKS /-->
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="false" rtexprvalue="true" %>

<div id="${id}" class="tableRow tableDataRow ${className}">
	<div class="tableRowInner">
		<jsp:doBody></jsp:doBody>
	</div>
</div>
