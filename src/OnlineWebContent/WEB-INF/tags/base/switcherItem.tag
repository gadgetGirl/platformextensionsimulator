<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="name" required="false" rtexprvalue="true"%>
<%@ attribute name="active" required="false" rtexprvalue="true"%>
<%@ attribute name="description" required="false" rtexprvalue="true"%>
<%@ attribute name="selector" required="false" rtexprvalue="true"%>
<%@ attribute name="url" required="false" rtexprvalue="true"%>
<%@ attribute name="onclick" required="false" rtexprvalue="true"%>

<c:choose>
	<c:when test="${not empty onclick}">
		<a class="switchItem ${active == 'true'?'active':''}" 
			onclick="switcher.setSelected(this);${onclick}" >
			${description}
		</a>
	</c:when>
	<c:when test="${not empty selector}">
		<a class="switchItem ${active == 'true'?'active':''}" 
			onclick="switcher.show('${selector}',this)" >
			${description}
		</a>
	</c:when>
	<c:otherwise>
		<a class="switchItem ${active == 'true'?'active':''}" 
			onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: '${url}',preLoadingCallBack:${onClick}}); return false;" >
			${description}
		</a>
	</c:otherwise>
</c:choose>