<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="target" required="false" rtexprvalue="true"%>
<%@ attribute name="url" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="false" rtexprvalue="true"%>

<%-- 

Developer: LM
=============
This is used on PayPalLandingWithCis, I needed a html link "<a/>" implementation.

--%>
<base:divContainer id="${id}">
	<a href="#" target="${target}" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${url}');">${label}</a>
</base:divContainer>