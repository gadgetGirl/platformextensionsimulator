<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="value" required="true"%>
<%@ attribute name="colNo" required="true"%>
<%@ attribute name="label" required="true"%>
<c:forEach var="i" begin="1" end="${colNo}" step="1" varStatus="status">
	<div class="tableCell col${i}">
		<div class="tableCellItem">
			<c:choose>
				<c:when test="${i == colNo}">
					<div class="totalCellInner clearfix ">${value}</div>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${i == 1}">
							<div class="clearfix ">${label}</div>
						</c:when>
						<c:otherwise>
							<div class="clearfix ">&nbsp;</div>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</c:forEach>