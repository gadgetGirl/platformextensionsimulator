<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>

<base:equalHeightsRow>
	<base:equalHeightsColumn width="100" className="white">
		<div<c:if test="${not empty id}"> id="${id}"</c:if> class="gridGroup grid100 ${className} clearfix"><div class="mobi-header-trigger"></div><jsp:doBody /></div>
	</base:equalHeightsColumn>
	<base:equalHeightsColumn ghostBlock="true" />
</base:equalHeightsRow>
