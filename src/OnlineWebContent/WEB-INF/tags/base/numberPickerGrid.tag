<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="boardMax" required="false" rtexprvalue="true"%>
<%@ attribute name="onClick" required="false" rtexprvalue="true"%>
<%@ attribute name="type" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>

<c:choose>
	<c:when test="${not empty onClick}">
		<c:set var="click" value="${onClick}" />
	</c:when>
	<c:otherwise>
		<c:set var="click" value="fnb.functions.lotto.numberPickerSelectNumber(this)" />
	</c:otherwise>
</c:choose>

<div id="${id}" class="${className}">
	
	<div id="${id}_Grid" class="numberPickerGrid">
		<c:choose>
			<c:when test="${not empty boardMax}">
				<c:forEach begin="1" end="${boardMax}" varStatus="loop">
					<div class="numberPickerCell <c:if test='${loop.index == boardMax}'>lastNumber</c:if>" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">${loop.index}</div></div></div>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">1</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">2</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">3</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">4</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">5</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">6</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">7</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">8</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">9</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">10</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">11</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">12</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">13</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">14</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">15</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">16</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">17</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">18</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">19</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">20</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">21</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">22</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">23</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">24</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">25</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">26</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">27</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">28</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">29</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">30</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">31</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">32</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">33</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">34</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">35</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">36</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">37</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">38</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">39</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">40</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">41</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">42</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">43</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">44</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">45</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">46</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">47</div></div></div>
		        <div class="numberPickerCell" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">48</div></div></div>
		        <div class="numberPickerCell lastNumber" onclick="${click}"><div class="numberPickerCellText change"><div class="numberPickerCellTextInner change">49</div></div></div>
			</c:otherwise>
		</c:choose>
		
    </div>
    
</div>