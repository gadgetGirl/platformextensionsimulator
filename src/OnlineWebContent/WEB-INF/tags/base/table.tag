<%@ attribute name="id" required="true" rtexprvalue="true"%>
<%@ attribute name="className" required="true" rtexprvalue="true"%>
<%@ attribute name="smallSize" required="false" rtexprvalue="true"%>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>

<base:equalHeightsRow>
	<base:equalHeightsColumn width="100">
		<div class="tableList ${smallSize} ${className} clearfix" id="${id}">	
			<jsp:doBody></jsp:doBody>	
		</div>  
	</base:equalHeightsColumn>
	<base:equalHeightsColumn ghostBlock="true" />
</base:equalHeightsRow>




