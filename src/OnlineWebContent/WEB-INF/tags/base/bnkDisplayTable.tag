<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="caption" required="false" rtexprvalue="false" type="java.lang.String"%>
<%@ attribute name="id" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="isEmpty" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true" %>

<c:choose>
	<c:when test="${not empty id}">
		<div class="tableList table ${className}" id="${id}">
	</c:when>
	<c:otherwise>
		<div class="tableList table ${className}" id="bnkTable">
	</c:otherwise>
</c:choose>

	<jsp:doBody></jsp:doBody>
</div>  	
<base:divContainer className="clear" />