<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="colNo" required="true" rtexprvalue="true"%>
<%@ attribute name="type" required="true" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="tooltip" required="false" rtexprvalue="true"%>
<%@ attribute name="hiddenLabel" required="false" rtexprvalue="true"%>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>
<c:set var="theBody">
<jsp:doBody/>
</c:set>
<c:if test="${empty theBody}">
 <c:set var="theBody" value="&nbsp;"/>
 </c:if>
<c:choose>
	<c:when test="${type =='headingCell'}">
		<div class="tableCell col${colNo} ${className}">
			
			<div class="tableHeaderCellItem">
				<a>${theBody}</a>
			</div>
		</div>
	</c:when>
	<c:when test="${type =='normalCell'}">
		<div class="tableCell col${colNo} ${className}">
			<c:if test="${not empty hiddenLabel}">
			<div class="hiddenLabel">${hiddenLabel}</div>
			</c:if>
			<div class="tableCellItem <c:if test='${not empty tooltip}'>tooltipPad floatLeft</c:if>">
				${theBody}
				<c:if test='${not empty tooltip}'><base:tooltip content="${tooltip}"/></c:if>
			</div>
			
		</div>
	</c:when>
</c:choose>


