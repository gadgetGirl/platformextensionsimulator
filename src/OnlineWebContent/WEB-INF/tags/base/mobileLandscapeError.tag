<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<div id="mobileOrientationError" class="mobileOrientationError hidden">
	<div id='mobileOrientationErrorWrapper' class='mobileOrientationErrorWrapper'>
		<div id='mobileOrientationLogo' class='mobileOrientationLogo'></div>
		<div id='mobileOrientationText' class='mobileOrientationText'>This site is optimized for portrait view on a device with these dimensions.</div>
	</div>
</div>