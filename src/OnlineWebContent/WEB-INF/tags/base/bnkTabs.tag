<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>

<%@ taglib prefix="fnb.hyperion.banking.frame.mainMenu" tagdir="/WEB-INF/tags/banking/frame/mainMenu"%>

<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ attribute name="menuViewBean" required="true" rtexprvalue="true" type="mammoth.utility.HyphenSortedVector"%>
<%@ attribute name="topMenuOptionsViewBean" required="true" rtexprvalue="true" type="mammoth.utility.HyphenSortedVector"%>
<%@ attribute name="dispayHeader" rtexprvalue="true"%>


		<div id="headerCornerImg"></div>
		
		<div id="headerInner" class="frameOffset">

			<div id="headerButtons" class="clearfix">
			
				<div id="headerButtonsInner">
				
					<c:if test="${empty dispayHeader || dispayHeader == false}">
			
							<c:forEach items="${topMenuOptionsViewBean}" var="itemOption" varStatus="itemOptionStatus">
								<c:choose>
									<c:when test="${itemOption.label.length() > 20}">
										<c:set var="colWidth" value="grid30" />
									</c:when>
									<c:otherwise>
										<c:set var="colWidth" value="grid10" />
									</c:otherwise>
								</c:choose>
								<c:set var="itemCount" value="${itemOptionStatus.count}" />
								<div id="headerButton_${itemCount}" class="headerButton gridCol ${colWidth}<c:if test="${itemOption.label == 'Apply Online Now'}"> applyHeaderButton</c:if><c:if test="${not empty itemOption.styleClass}"> headerButton${itemOption.styleClass}</c:if>" data-value="${itemOption.url}">
									<a <c:if test="${not empty itemOption.styleClass}">class='${itemOption.styleClass}'</c:if>><span>${itemOption.label}</span></a>
								</div>
							</c:forEach>
							<c:if test="${showStandinIcon}">
								<div id="standInHeaderButton" class="headerStandInIcon">
									<img src="03images/fnb/custom/standInMessage/standInIcon.png" />
								</div>
							</c:if>
							<c:if test="${not empty helpURL}">
							<div class="headerButton" id="helpText">
								<base:helpTextIcon/>		
							</div>
							</c:if>
			
					</c:if>

				</div>
				
			</div>
			
		<div id="menuWrapper" class="clearfix">
			<nav data-role="topTabs" id="topTabs" data-disbaled="true" class="clearfix" ${selectedTobTab}>
						
				<c:forEach items="${menuViewBean}" var="topMenuItem" varStatus="topMenuItemStatus">
				
				   		<c:if test="${topMenuItemStatus.last}">
				   			<c:set var="className" value="lastTab"/>
				   		</c:if>
				
						<fnb.hyperion.banking.frame.mainMenu:mainMenu className="${className}" dataUrl="${topMenuItem.url}">${topMenuItem.label}</fnb.hyperion.banking.frame.mainMenu:mainMenu>
				
				</c:forEach>
		
				<div id="mainTabSlider"></div>
			</nav>
		</div>
		<button type="button" id="topTabScrollButtonLeft"  data-visible="false" class="topTabScrollButton topTabScrollButtonLeft Hhide"></button>
		<button type="button" id="topTabScrollButtonRight" data-visible="false" class="topTabScrollButton topTabScrollButtonRight Hhide"></button>
	
		
	</div>
