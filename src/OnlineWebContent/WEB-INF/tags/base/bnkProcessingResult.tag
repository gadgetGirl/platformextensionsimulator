<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="processingResult" required="true" type="mammoth.jsp.viewbean.ProcessingResultViewBean" %>

<c:if test="${not empty processingResult.footerButtons}">
	<c:set var="footerButtonsResult" value="${processingResult.footerButtons}" scope="request"/>
</c:if>

<base:definitionList>
	<base:definitionListItem description="Processing Message:" value="${processingResult.processingMessage}" />
	<c:if test="${processingResult.errorCode != 0}">
		<base:definitionListItem description="Error Message:" value="${processingResult.errorMessage}" />
		<base:definitionListItem description="Error Code:" value="${processingResult.errorCode}" />
	</c:if>
</base:definitionList>
