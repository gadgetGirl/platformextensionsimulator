<%@ attribute name="title" required="true" rtexprvalue="true"%>
<%@ attribute name="subTitle" required="true" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>

<div id="${id}" class="numberPickerHeaderTitle">
	<div id="numberPickerHeader_left" class="numberPickerHeader_left">
		<h3>${title}</h3>
		<p>${subTitle}</p>
	</div>
	<div id="numberPickerHeader_right" class="numberPickerHeader_right">
			<div id="numberPickerHeaderNumber_wrapper" class="numberPickerHeaderNumber_wrapper">
			</div>
	</div>
</div>