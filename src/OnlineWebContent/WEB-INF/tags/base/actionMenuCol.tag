<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="style" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="width" required="false" rtexprvalue="true"%>
<%@ attribute name="custom" required="false" rtexprvalue="true"%>

    	
<div id="${id}" style="${style}" class="actionCol ${width} ${className}">    	
	
		<div class="actionColInner ${custom}">
		<jsp:doBody />
		</div>
		
</div>