<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="id" required="true" rtexprvalue="true"%>
<%@ attribute name="tabClassName" required="false" rtexprvalue="true"%>
<%@ attribute name="tabName" required="false" rtexprvalue="true"%>

<div id="subtabContent_${id}" class="hiddenSubtabContent<c:if test="${!empty tabClassName}"> ${tabClassName}</c:if>">
	<input id="hidden_${id}" type="hidden" value="${tabName}" name="hidden_${id}"/>
	<jsp:doBody></jsp:doBody>
</div>