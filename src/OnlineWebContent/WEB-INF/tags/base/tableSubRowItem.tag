<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="tableSubRow" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableSubRowItem"%>
<%@ attribute name="tableRowId" required="true" rtexprvalue="true" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<div id="${tableSubRow.id}" class="tableCellItem">
	<c:set var="rowCounter" value="1" />
	<c:forEach items="${tableSubRow.rows}" var="row">
		<c:set var="tableSubRowId" value="${tableRowId}_subrow_${rowCounter}" />
		<!-- the values are an array list of rows, iterate through each  -->
		<div id="tabelSubRow_${rowCounter+1}" class="${row.cssClass}">
			<div class="tableRowInner clearfix">
				<c:set var="counter" value="0" />
				<c:forEach items="${row.itemMap}" var="tableItem">
					<c:set var="counter" value="${counter+1}" />
					<div class="tableCell">
						<base:tableCellItem tableItem="${tableItem.value }" ></base:tableCellItem>
					</div>
				</c:forEach>
			</div>
		</div>
		<c:set var="rowCounter" value="${rowCounter + 1}" />
	</c:forEach>
</div>
