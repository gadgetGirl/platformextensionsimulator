<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%-- <%@ attribute required="true" rtexprvalue="true" name="switcherViewBean" type="fnb.online.tags.beans.tablecontrolsswitcher.Switcher"%> --%>
<%@ attribute required="false" rtexprvalue="true" name="switcherObject" type="fnb.online.tags.beans.tablecontrolsswitcher.Switcher"%>
<%@ attribute required="false" rtexprvalue="true" name="label"%>
<c:set var="label" value="${not empty label ? label : 'Viewing'}" />
<c:set var="id" value="${empty switcherViewBean.id ? '' : switcherViewBean.id}" />
<c:if test="${empty id}">
	<c:set var="id">${tableId}_switcher</c:set>
</c:if>
<%--<base:headerControlLabel text="${label}" /> --%>
<c:set var="counter" value="1" />
<div class="mobi-dropdown-trigger" onclick="fnb.utils.mobile.switcher.expandSwitcher(this);"></div>
<div class="tableSwitcherItemsContainer">
	
	<c:forEach items="${switcherObject.actions}" var="action">
		<base:tableSwitcherButtonItem description="${action.label}" id="tableSwitcherButton_${counter}" url="${action.url}" value="${action.rfn}" selected="${action.selected}" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url:'${action.url}'});" />
		<c:set var="counter" value="${counter+1}" />
	</c:forEach>
	<jsp:doBody />
</div>
