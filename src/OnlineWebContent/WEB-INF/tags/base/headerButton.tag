<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="id" required="false" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="label" required="false" %>
<%@ attribute name="onClick" required="false" %>

<span<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty onClick}"> onclick="${onClick}"</c:if> class="headerButton<c:if test='${not empty className}'> ${className}</c:if>"><c:if test="${not empty label}">${label}</c:if></span>
