<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="tableBean" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableBean"%>

<c:set var="tableId" scope="session" value="${tableBean.tableId}"/>

<script type="text/javascript">
	var timer = null;
	var _pageNumber;
	$('#${tableId}_pager').keyup(function(){
		_pageNumber = $('#${tableId}_pager').val();
	    clearTimeout(timer); 
	    timer = setTimeout(changePage, 1000);
	});
	
	function changePage() {
		${tableId}_tableController.pageTable(_pageNumber);
	}
</script>

<div class="clear"></div>
<c:if test="${tableBean.pagingViewBean.numberOfItems > 10 || tableBean.pagingViewBean.numberOfPages > 1}">
	<div class="pagingWrapper clearfix">
		<c:if test="${tableBean.pagingViewBean.numberOfItems > 10}">
			<div class="pagination">
				<base:equalHeightsRow className="pageLimits">
					<base:equalHeightsColumn className="grid30 valignMiddle">
						Show
					</base:equalHeightsColumn>
					<base:equalHeightsColumn className="grid30">
						<base:dropdownSingleTier reverse="true" className="limiterDropDown" id="${tableId}_limitSelectionDropdown" selectedValue="${tableBean.pagingViewBean.pageSize}">
							<c:forEach var="i" begin="10" end="150" step="10">
								<c:choose>
									<c:when test="${tableBean.pagingViewBean.pageSize == i}">
										<base:dropdownSingleTierItem returnValue="${i}" content="${i}" onclick="${tableId}_tableController.changeTableSize(${i});"></base:dropdownSingleTierItem>
									</c:when>
									<c:otherwise>
										<base:dropdownSingleTierItem returnValue="${i}" content="${i}" onclick="${tableId}_tableController.changeTableSize(${i});"></base:dropdownSingleTierItem>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</base:dropdownSingleTier>
					</base:equalHeightsColumn>
					<base:equalHeightsColumn className="grid30 valignMiddle">
						Entries
					</base:equalHeightsColumn>
				</base:equalHeightsRow>
			</div>
		</c:if>
	
		<c:if test="${tableBean.pagingViewBean.numberOfPages > 1}">
			<base:equalHeightsRow className="pagination2">
				<c:if test="${(tableBean.pagingViewBean.numberOfPages > 1) && (tableBean.pagingViewBean.currentPageNumber > 1)}">
					<base:equalHeightsColumn className="pagingPrevious" onclick="${tableId}_tableController.pageTable('${tableBean.pagingViewBean.previousPageNumber}')">
						<span class="eziPagingHide">Previous</span><span class="eziPagingShow">&lt;</span>
					</base:equalHeightsColumn>
				</c:if>
				<base:equalHeightsColumn className="pagingChoice">
					Page <input id="${tableId}_pager" value="${tableBean.pagingViewBean.currentPageNumber}" /> of ${tableBean.pagingViewBean.numberOfPages}
				</base:equalHeightsColumn>
				<c:if test="${tableBean.pagingViewBean.numberOfPages > 1 && tableBean.pagingViewBean.currentPageNumber < tableBean.pagingViewBean.numberOfPages}">
					<base:equalHeightsColumn className="pagingNext" onclick="${tableId}_tableController.pageTable('${tableBean.pagingViewBean.nextPageNumber}')">
						<span class="eziPagingHide">Next</span><span class="eziPagingShow">&gt;</span>
					</base:equalHeightsColumn>
				</c:if>
			</base:equalHeightsRow>
		</c:if>
	</div>
</c:if>