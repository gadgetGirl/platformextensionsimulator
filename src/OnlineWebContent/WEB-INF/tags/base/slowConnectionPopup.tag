<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>


<div id="slowConnectionContainer" class="hidden">
	<div id="SlowConnectionPopUp">
		<div id="messageText">Your Internet connection appears to be slow. Do you want to continue with the current request, or end?</div>
		<div id="messageText">Note: Your request may not be fully processed.  Please refer to your history to verify the status of your request.</div>
		<div id="buttonRow">
			<div class="floatContainer">
				<div class="buttoncell" id="OK" onclick="fnb.functions.slowConnection.continueWaiting();">Continue</div>
				<div class="buttoncell" id="LOGOFF" onclick="fnb.functions.slowConnection.endNow();">End Now</div>
			</div>
		</div>
	</div>
</div>
