<%@ attribute name="id" required="true" rtexprvalue="true"%>

<span class="tableControlsSwitcher" id="${id}">
	<span class="tableControlsSwitcherHolder">
			
			<jsp:doBody></jsp:doBody>
			
	</span>
</span>