<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="true"  name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="groupClass" description="parent element class of grouped checkboxes, eg. '.group1'"%>
<%@ attribute required="false" name="labelCheck" description="DEFAULT: Select All"%>
<%@ attribute required="false" name="labelUncheck" description="DEFAULT: Deselect All"%>
<%@ attribute required="false" name="initialState" description="boolean: true is 'check all', false is 'uncheck all' "%>

<%-- WORKS ONLY WITH NEW CHECKBOXES --%>

<c:set var="labelCheck" value="${not empty labelCheck ? labelCheck : 'Select all'}" />
<c:set var="labelUncheck" value="${not empty labelUncheck ? labelUncheck : 'Deselect all'}" />
<c:set var="initialState" value="${empty initialState ? true : initialState}" />

<div id="checkall-toggle-wrapper-${id}" class="checkall-toggle-wrapper">
	<a id="check-all-${id}" class="checkall-check-link checkall-link ${className}" data-check-group="${groupClass}"<c:if test="${not initialState}"> style="display:none;"</c:if>>${labelCheck}</a>
	<a id="uncheck-all-${id}" class="checkall-uncheck-link checkall-link ${className}" data-check-group="${groupClass}"<c:if test="${initialState}"> style="display:none;"</c:if>>${labelUncheck}</a>
</div>