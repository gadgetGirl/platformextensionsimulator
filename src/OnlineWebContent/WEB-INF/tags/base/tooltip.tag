<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="content"%>

<div class="tooltipParent<c:if test="${not empty className}"> ${className}</c:if>">
	<div <c:if test="${not empty id}"> id="${id}"</c:if> class="tooltipButton"></div>
	<div class="tooltip"  ><div class="tooltip-text">${content}<jsp:doBody /></div><span class="tooltip-pointer">&nbsp;</span></div>
	<span class=".tooltip-pointer-left"></span>
</div>

