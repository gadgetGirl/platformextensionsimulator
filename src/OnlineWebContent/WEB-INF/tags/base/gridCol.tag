<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="width"%>
<c:set var="width" value="${not empty width ? width : '100'}" />
<div<c:if test="${not empty id}"> id="${id}"</c:if> class="gridCol grid${width} ${className} clearfix"><jsp:doBody /></div>