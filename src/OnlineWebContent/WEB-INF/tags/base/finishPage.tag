<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="type" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="backgroundTarget" required="false" rtexprvalue="true"%>
<%@ attribute name="eziFinish" required="false" rtexprvalue="true"%>
<%@ attribute name="hidePrintAndDownload" required="false" rtexprvalue="true"%>
<%@ attribute name="resultBean" required="true" rtexprvalue="true" type="mammoth.jsp.viewbean.ProcessingResultViewBean"%>

<c:if test="${not empty resultBean.footerButtons}">
	<c:set var="footerButtonsResult" value="${resultBean.footerButtons}" scope="request"/>
</c:if>

<c:if test="${not empty backgroundTarget}">
	<c:choose>
		<c:when test="${not empty eziFinish}">
			<script>
				var eziFinish = true;
			</script>
		</c:when>
		<c:otherwise>
			<script>
				var eziFinish = false;
			</script>
		</c:otherwise>
	</c:choose>
	<script>
		var error = "${resultBean.errorCode}";
		var pending = "${resultBean.pending}";
		var partial = "${resultBean.partial}";
		var color;
		
		if (pending == 'true') {
			className = 'pending';
		} else if (partial == 'true') {
			className = 'partial';
		} else if (error == '0') {
			className = 'success';
		} else if (error == '350') {
			className = 'error350';
		} else {
			className = 'error';
		}
		
		$('.rightBox').addClass(className);

		if (eziFinish) {
			$('.eziPageTitle, #eziPannelButtons, #eziWrapper .rightBoxContent').addClass(className);
			
			$('#eziPannelButtons').find('.formButton').addClass(className);
		};
		
		if(!fnb.hyperion.utils.eziPanel.active&&($(window).height()-199)>$('#main').height()){
			$('#currentSubtab').css({'min-height':$(window).height()-199})
		}
	</script>
</c:if>
<div class="rightBox">
	<div class="rightBoxContent">
		<c:choose>
			<c:when test="${resultBean.pending == 'true'}">
			
				<base:divContainer className="whiteMessageContainer">
					<base:heading level="5" value="Take Note..." />
				</base:divContainer>
				<div class="finishMessageArrow">
							<img src="/banking/03images/fnb/icons/finishMessageArrow.PNG" alt="">
						</div>
				<div class="rightBoxInner">
					<base:paragraph>${resultBean.processingMessage}</base:paragraph>
					
					<br />
					<br />
					<c:if test="${not empty resultBean.traceId}">
						<base:heading level="6" value="Ref: ${resultBean.traceId}" />
					</c:if>
				</div>
			</c:when>
			<c:when test="${resultBean.partial == 'true'}">
				
				<base:divContainer className="whiteMessageContainer">
					<base:heading level="5" value="Take Note..." />
				</base:divContainer>
				<div class="finishMessageArrow">
					<img src="/banking/03images/fnb/icons/finishMessageArrow.PNG" alt="">
				</div>
				<div class="rightBoxInner">
					<base:paragraph>${resultBean.processingMessage}</base:paragraph>
					<br />
					<br />
					<c:if test="${not empty resultBean.traceId}">
						<base:paragraph>Ref: ${resultBean.traceId}</base:paragraph>
					</c:if>
				</div>
			</c:when>

			<c:otherwise>
				<c:choose>
					<c:when test="${resultBean.errorCode == 0}">
						<base:divContainer className="whiteMessageContainer">
							<base:heading level="5" value="Thank You!" />
								
						</base:divContainer>
						<div class="finishMessageArrow">
							<img src="/banking/03images/fnb/icons/finishMessageArrow.PNG" alt="" />
						</div>
						<div class="rightBoxInner">
							<c:if test="${not empty resultBean.processingMessage}">
							<base:paragraph>${resultBean.processingMessage}</base:paragraph>
							</c:if>
						
							<c:if test="${not empty resultBean.traceId}">
								<base:paragraph>Ref: ${resultBean.traceId}</base:paragraph>
							</c:if>
						</div>
						<c:if test="${!hidePrintAndDownload}">
									<base:divContainer className="printAndDownloadContainer ">
										<c:choose>
											<c:when test="${!empty resultBean.printButton.url }">
												<base:formsButton id="printBtn" text="Print" className="print eziHidePrint" buttonBean="${resultBean.printButton}" target="print"/>
											</c:when>
										<c:otherwise>
												<base:formsButton id="printBtn" text="Print" className="print eziHide" target="printWindow"/>
											</c:otherwise>
										</c:choose>
										
										<c:choose>
											<c:when test="${!empty resultBean.downloadButton.url }">
												<base:formsButton id="downloadBtn" text="Download" className="download" buttonBean="${resultBean.downloadButton}" target="download"/>
											</c:when>
											<c:otherwise>
												<base:formsButton id="downloadBtn" text="Download" className="download eziHide" target="downloadPage"/>
											</c:otherwise>
										</c:choose> 
									</base:divContainer>
								</c:if>
						<jsp:doBody />
					</c:when>
					
					<c:when test="${resultBean.errorCode == 350}">
						<base:divContainer className="whiteMessageContainer">
							<base:heading level="5" value="Take Note..." />
						</base:divContainer>
						<div class="rightBoxInner">
							<base:paragraph>${resultBean.processingMessage}</base:paragraph>
							<br />
							<br />
							<c:if test="${not empty resultBean.traceId}">
								<base:paragraph>Ref: ${resultBean.traceId}</base:paragraph>
							</c:if>
						</div>
					</c:when>
					<c:otherwise>
						<base:divContainer className="whiteMessageContainer">
							<base:heading level="5" value="Sorry!" />
						</base:divContainer>
						<div class="finishMessageArrow">
							<img src="/banking/03images/fnb/icons/finishMessageArrow.PNG" alt="">
						</div>
						<div class="rightBoxInner">
							<c:choose>
								<c:when test="${not empty resultBean.errorMessage}">
									<base:paragraph>${resultBean.errorMessage}</base:paragraph>
								</c:when>
								<c:otherwise>
									<base:paragraph>${resultBean.processingMessage}</base:paragraph>
								</c:otherwise>
							</c:choose>
							<br />
							<br />
							<c:if test="${resultBean.errorCode > 0}">
								<base:paragraph>Error code: ${resultBean.errorCode}</base:paragraph>
							</c:if>
							<br />
							<br />
							<c:if test="${not empty resultBean.traceId}">
								<base:paragraph>Ref: ${resultBean.traceId}</base:paragraph>
							</c:if>
						</div>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
		
	</div>

</div>
