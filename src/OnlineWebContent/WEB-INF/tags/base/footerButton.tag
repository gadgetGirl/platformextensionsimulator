<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute required="true"  rtexprvalue="true" name="text" %>
<%@ attribute required="false" rtexprvalue="true" name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="onclick" %>
<%@ attribute required="false" rtexprvalue="true" name="formToSubmit" %>
<%@ attribute required="false" rtexprvalue="true" name="url" %>

<c:set var="idAttr">
	<c:if test="${not empty id}">id="${id}"</c:if>
</c:set>

<div ${idAttr} class="gridCol grid10 footerBtn ${className}">
	<c:choose>
		<c:when test="${not empty formToSubmit}">
			<a<c:if test="${not empty id}"> id="${id}Href"</c:if> onclick="fnb.functions.submitFormToWorkspace.submit('${formToSubmit}',${not empty onclick ? onclick : '&#39;&#39;'},this, {alternateUrl: '${url}'}); return false;" href="#">${text}</a>
		</c:when>
		<c:when test="${not empty url}">
			<a<c:if test="${not empty id}"> id="${id}Href"</c:if> onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: '${url}',preLoadingCallBack:${not empty onclick ? onclick : '&#39;&#39;'}}); return false;" href="#">${text}</a>
		</c:when>
		<c:otherwise>
			<a<c:if test="${not empty id}"> id="${id}Href"</c:if> href="#" target="_self" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if>>${text}</a>
		</c:otherwise>
	</c:choose>
	
	<jsp:doBody />
</div>