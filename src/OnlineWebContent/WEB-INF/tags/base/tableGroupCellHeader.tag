<%@ attribute name="tableId" required="true" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="columnIndex" required="true" rtexprvalue="true" %>



<div class="tableCell col${columnIndex} ${columnOptions.align}">
	<div class="tableHeaderCellItem">
	
		<span class="hinner ${columnOptions.cssClass} ${columnOptions.align} ${columnOptions.hideColumn}">
			${columnOptions.heading}
		</span>
	
	</div>
</div>






