<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="tabLabel" required="true" rtexprvalue="true"%>
<%@ attribute name="link" required="false" rtexprvalue="true"%>

<li class="gridCol grid10 selected">
	<a href="${link}">${tabLabel}</a>
</li>