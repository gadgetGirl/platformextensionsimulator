<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="id" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="tref" required="false" rtexprvalue="true" type="java.lang.String"%>

<div id="${id}" <c:if test="${!empty className}"> class="${className}"</c:if>>
	<div class="left-sidebar-bottom"></div>
		<div id="fnb-logo" class="fnb-logo"></div>
		<div id="support-reference" class="support-reference">${tref}</div>
		<div id="footerContent" class="footerContent">
		<div class="footerBorder"></div>
			<div id="footerElements" class="footerElements gridGroup clearfix">
		</div>
	</div>
</div>