<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>

<%@ attribute name="dropdown" required="false" rtexprvalue="true" type="fnb.online.tags.beans.dropdown.AbstractDropdown" %>
<%@ attribute name="tableOptions" required="false" rtexprvalue="true" type="fnb.online.tags.beans.dropdown.AbstractDropdown" %>

<%@ attribute name="columns" required="true" rtexprvalue="true" %>
<c:set var="tableControlColumns" scope="request" value="${columns}" />

<base:gridGroup id="tableControls">
	<jsp:doBody />	
	<div style="clear:both"></div>
</base:gridGroup>