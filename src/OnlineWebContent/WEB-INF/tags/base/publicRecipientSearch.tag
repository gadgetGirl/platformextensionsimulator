<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%--
The following TAGs should someday be consolidated into one:
	publicRecipientSearch.tag
	branchSearch.tag
	phoneBookSearch.tag
--%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="label"%>

<%@ attribute required="false" rtexprvalue="true" name="type"%>
<%@ attribute required="false" rtexprvalue="true" name="name"%>
<%@ attribute required="false" rtexprvalue="true" name="value"%>
<%@ attribute required="false" rtexprvalue="true" name="defaultValue"%>
<%@ attribute required="false" rtexprvalue="true" name="maxlength"%>

<%@ attribute required="false" rtexprvalue="true" name="readonly"%>
<%@ attribute required="false" rtexprvalue="true" name="onclick" type="java.lang.String"%>
<%@ attribute required="false" rtexprvalue="true" name="onkeyup" type="java.lang.String"%>
<%@ attribute required="false" rtexprvalue="true" name="onchange" type="java.lang.String"%>

<%@ attribute required="false" rtexprvalue="true" name="labelPosition" description="DEPRECATED"%>

<%-- Attribute(s) NOT implemented --%>
<%--
<%@ attribute required="false" rtexprvalue="true" name="disabled"%>
--%>

<div class="input-wrapper formElementWrapper clearfix <c:if test="${type == 'hidden'}"> hidden</c:if> ${className}">

	<c:if test="${not empty label}">
		<div class="gridCol grid40 formElementLabel"><div class="input-label-inner">${label}</div></div>
	</c:if>
	<div id="publicRecipientSearchContainer" class="gridCol grid40 formElementContainer input-container clearfix">
		<div id="publicRecipientSearchInput" class="input-container-inner<c:if test="${empty value}"> displayNone</c:if>">
			<input id="publicRecipientSearch"
				name="publicRecipientSearch"
				class="searchInput-input input-input"
				value="${ ( not empty value ) ? value : defaultValue }"		
				<c:if test="${ not empty onclick}">onclick="${onclick}"</c:if> 
				<c:if test="${ not empty onkeyup}">onkeyup="${onkeyup}"</c:if> 
				<c:if test="${ not empty onchange}">onchange="${onchange}"</c:if>
				disabled="disabled"
				<c:if test="${not empty readonly}">readonly="readonly"</c:if> 
				<c:if test='${not empty maxlength}'>maxlength="${maxlength}"</c:if> />
		</div>
	</div>
	<div class="gridCol formElementContainer grid20">
		<base:formsButton className="settingsButtonPadding margin50 settingsRightAlignPadding" text="Search" id="publicRecipientSearchButton" onclick="fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '/banking/Controller?nav=navigator.publicrecipientsearch.simple.PublicRecipientSearchLanding'});" />
	</div>
</div>
