<%@tag import="fnb.online.tags.beans.table.TableExpandLinkItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableExpandLinkItem"%>
<%@ attribute name="tableRowId" required="true" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>

  
<base:formsButton text="${tableItem.text}" id="${tableItem.id}${count}"  onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToExpandableRow',{url:'${tableItem.url}', target:'#expandableRow_${tableRowId}', buttonTarget: this}); return false;">
</base:formsButton>
