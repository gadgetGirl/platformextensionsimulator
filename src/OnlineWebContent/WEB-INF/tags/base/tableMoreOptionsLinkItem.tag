<%@tag import="fnb.online.tags.beans.table.TableHyperLinkItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableMoreOptionsItem"%>
<%@ attribute name="count" required="false" rtexprvalue="true"%>

<div id="rowMoreButton${count}" class="rowMoreButton" data-value="${tableItem.url}">
	<div class="arrow" ></div>
</div>
