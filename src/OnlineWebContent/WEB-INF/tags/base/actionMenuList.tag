<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="items" required="true" rtexprvalue="true" type="java.util.List"%>
<%@ attribute name="color" required="false" rtexprvalue="true"%>
<%@ attribute name="onclick" required="false" rtexprvalue="true"%>

<c:forEach items="${items}" var="group">
	<base:divContainer className="actionGroups">
		<c:choose>
			<c:when test="${not empty group.heading}">
				<div class="actionOneItemHeading ${color}">${group.heading}</div>
			</c:when>
		</c:choose>
		<c:forEach items="${group.actions}" var="actionItems" varStatus="actionItemStatus">
			<div class="actionOneItems ${color}"><c:if test="${actionItemStatus.index == 0}">
				<base:divContainer className="actionOneItemsSpacer"></base:divContainer>
			</c:if> 
			 <c:set var="onclickFunction">
                  <c:choose>
                      <c:when test="${actionItems.target == '0'}">fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${actionItems.url}');</c:when>
                      <c:when test="${actionItems.target == '1'}">fnb.utils.actionMenu.loadTargetToActionMenu('${actionItems.url}',this);</c:when>
                      <c:when test="${actionItems.target == '2'}">fnb.functions.showDiv.show('#${actionItems.contentIdentifier}');</c:when>
                      <c:when test="${actionItems.target == '5'}">fnb.controls.controller.eventsObject.raiseEvent('doDownload','${actionItems.url}');</c:when>
                  </c:choose>
             </c:set>
			
			<a id="actionBtt_${actionItemStatus.index}" type="${actionItems.target}" onclick="${onclickFunction};${onclick}"><div class="actionBttHead">${actionItems.description}</div>
			<c:choose>
			<c:when test="${not empty actionItems.comment}">
				
				<div class="actionBttDesc">${actionItems.comment}</div>
			</c:when></c:choose></a> <base:divContainer className="actionOneItemsSpacer"></base:divContainer>
			</div>
		</c:forEach>
	</base:divContainer>
</c:forEach>
