<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ tag import="fnb.online.tags.beans.table.TableOptions"%>
<%@ tag import="fnb.online.tags.beans.table.BeanUtilities"%>

<%@ attribute name="sortingCallbackFunction" required="false" rtexprvalue="true"%>
<%@ attribute name="pagingCallbackFunction" required="false" rtexprvalue="true"%>
<%@ attribute name="searchCallbackFunction" required="false" rtexprvalue="true"%>
<%@ attribute name="changePageSizeCallbackFunction" required="false" rtexprvalue="true"%>
<%@ attribute name="formToSubmit" required="false" rtexprvalue="true"%>
<%@ attribute name="validationNavigator" required="false" rtexprvalue="true"%>

<% 

String optionsId = BeanUtilities.getFieldValuePairs(getParent(),false).get("id");
TableOptions tableOptions = (TableOptions) request.getAttribute(optionsId);
tableOptions.setSortingCallbackFunction(sortingCallbackFunction);
tableOptions.setPagingCallbackFunction(pagingCallbackFunction);
tableOptions.setSearchCallbackFunction(searchCallbackFunction);
tableOptions.setChangePageSizeCallbackFunction(changePageSizeCallbackFunction);
tableOptions.setFormToSubmit(formToSubmit);
tableOptions.setValidationNavigator(validationNavigator);
%>
