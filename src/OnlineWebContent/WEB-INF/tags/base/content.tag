<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="id" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true" type="java.lang.String"%>

<div id="${id}"<c:if test="${!empty className}"> class="${className}"</c:if>>

	<jsp:doBody></jsp:doBody>
	
</div>