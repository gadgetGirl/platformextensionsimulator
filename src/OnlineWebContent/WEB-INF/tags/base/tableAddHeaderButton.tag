<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute required="false" name="id" %>
<%@ attribute required="false" name="className" description="A popular class to add is 'mobiHidden'" %>
<%@ attribute required="true"  name="label" %>
<%@ attribute required="false" name="onclick" %>
<%@ attribute required="false" name="icon" description="Default is 'smallTurqPlus'" %>
<c:set var="icon" value="${empty icon ? '' : icon}" />

<base:button id="${id}" icon="${icon}" className="tableHeaderAddButton ${className}" label="${label}" onclick="${onclick}" />
