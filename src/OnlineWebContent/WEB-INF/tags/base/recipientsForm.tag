<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="max" required="true" rtexprvalue="true"%>

<%@ attribute name="editViewBean" required="false" rtexprvalue="true" type="fnb.online.mammoth.notifications.NotificationsViewBean"%>

<%@ attribute name="addButtonLabel" required="true" rtexprvalue="true"%>
<%@ attribute name="removeButtonLabel" required="true" rtexprvalue="true"%>

		<div id="addRecipientExtender" class="addRecipientExtender clearfix">
		
				<div id="addRecipientRowParent" class="addRecipientRowParent">
					<base:equalHeightsRow>
						<base:equalHeightsColumn width="100" className="white">
							<base:gridGroup id="addRecipientRow" className="addRecipientRow">
								<base:gridCol width="50">
									<base:dropdownSingleTier color="white" margin="margin50" id="selectedTypeInput0" name="selectedTypeInput0" label="Method" dropdownBean="${recipientsOptionsList}" selectedValue="Email address">
										<base:dropdownSingleTierItem id="dropDownItem_1" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Email address" returnValue="Email address" selected="true" />
										<base:dropdownSingleTierItem id="dropDownItem_2" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Cell number" returnValue="Cell number" selected="false" />
										<base:dropdownSingleTierItem id="dropDownItem_3" className="" onclick="fnb.functions.notifications.dropdownSelect(this)" content="Fax number" returnValue="Fax number" selected="false" />
									</base:dropdownSingleTier>
								</base:gridCol>
								<base:gridCol width="50" className="methodInputWrapper">
									<base:input type="phone" maxlength="45" id="methodContactInput0" name="methodContactInput0" value="" label="Email address"/>
								</base:gridCol>
								<base:gridCol id="subjectInputWrapper0"  className="subjectInputWrapper" width="50">
									<base:input inputWidth="30" labelWidth="20" maxlength="100" type="text" id="subjectInput0" name="subjectInput0" value="Proof of Payment" label="Subject"/>
								</base:gridCol>
								<base:gridCol width="100">
									<div id="addRecipientsRemoveButton${inputItemStatus.index}" onclick="fnb.functions.notifications.removeRow(this)" class="addRecipientsRemoveButton hidden">
										<a class="addRecipientsRemoveButtonLink">${removeButtonLabel}</a>
									</div>
								</base:gridCol>
							</base:gridGroup>
						</base:equalHeightsColumn>
						<base:equalHeightsColumn ghostBlock="true" />
					</base:equalHeightsRow>
				</div>
		
				<script>
					var emailLimit = 4;
					var smsLimit = 1;
					var faxLimit = 1;

					fnb.functions.notifications.initialize($('#addRecipientExtender'),$('#addRecipientRowParent'),${max},emailLimit,smsLimit,faxLimit);
				</script>
				<c:choose>
					<c:when test="${not empty editViewBean.methodContactInput}">
						<c:forEach items="${editViewBean.methodContactInput}" var="inputItem" varStatus="inputItemStatus">
							<c:choose>
								<c:when test="${editViewBean.selectedTypeInput[inputItemStatus.index]=='Email address'}">
									<c:set var="option" value="0" />
								</c:when>
								<c:when test="${editViewBean.selectedTypeInput[inputItemStatus.index]=='Cell number'}">
									<c:set var="option" value="1" />
								</c:when>
								<c:when test="${editViewBean.selectedTypeInput[inputItemStatus.index]=='Fax number'}">
									<c:set var="option" value="2" />
								</c:when>
							</c:choose>
							<c:choose>
								<c:when test="${inputItemStatus.index==0}">
									<script>
										var valObj = {code:'${editViewBean.methodCodeInput[inputItemStatus.index]}',method:'${editViewBean.methodContactInput[inputItemStatus.index]}',subject:'${editViewBean.subjectInput[inputItemStatus.index]}'}
										fnb.functions.notifications.setOptions($('#addRecipientRowParent'),parseInt('${option}'),valObj);
									</script>
								</c:when>
								<c:otherwise>
									<script>
										var valObj = {code:'${editViewBean.methodCodeInput[inputItemStatus.index]}',method:'${editViewBean.methodContactInput[inputItemStatus.index]}',subject:'${editViewBean.subjectInput[inputItemStatus.index]}'}
										fnb.functions.notifications.cloneRow(valObj,parseInt('${option}'));
									</script>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<script>
							fnb.functions.notifications.setOptions($('#addRecipientRowParent'),0)
						</script>
					</c:otherwise>
				</c:choose>
			
			<base:equalHeightsRow>
				<base:equalHeightsColumn width="100">
					<div class="addRecipientsAddButtonWrapper" onclick="fnb.functions.notifications.cloneRow()">
						<div id="addRecipientsAddButton" class="addRecipientsAddButton">
							<a class="addRecipientsAddButtonLink">${addButtonLabel}</a>
						</div>
					</div>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
	</div>