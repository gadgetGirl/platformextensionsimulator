<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="visible" required="false" rtexprvalue="true"%>
<%@ attribute name="doSpacing" required="false" rtexprvalue="true"%>

<c:set var="scrollingBannerId">
	<c:choose>
		<c:when test="${not empty id}">
			scrollingBannerContainer${id}
		</c:when>
		<c:otherwise>
			scrollingBannerContainer
		</c:otherwise>
	</c:choose>
</c:set>

<c:set var="visible">
	<c:choose>
		<c:when test="${empty visible}">
			true
		</c:when>
		<c:otherwise>
			${visible}
		</c:otherwise>
	</c:choose>
</c:set>

<div id="${scrollingBannerId}" class="scrollingBannerContainer<c:if test="${not empty className}"> ${className}</c:if> hidden">
	<div id="scrollableBanner" class="scrollableBanner">
		<div id="scrollWrapper" class="scrollWrapper">
	 		<jsp:doBody/>
	 	</div>
	</div>
</div>
<script>
	fnb.utils.scrollingBanner.init('${scrollingBannerId}','${visible}','${doSpacing}');
</script>