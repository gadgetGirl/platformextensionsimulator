<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="false" rtexprvalue="true" name="radioGroupViewBean" type="fnb.online.tags.beans.radiogroup.RadioGroup"%>
<%@ attribute required="true"  rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="name"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="label"%>
<%@ attribute required="false" rtexprvalue="true" name="labelPosition" description="DEPRECATED"%>
<%@ attribute required="false" rtexprvalue="true" name="labelClassName" description="DEPRECATED"%>
<%@ attribute required="false" rtexprvalue="true" name="value"%>
<%@ attribute required="false" rtexprvalue="true" name="count" type="java.lang.String"%>
<%@ attribute required="false" rtexprvalue="true" name="switcher"%>
<%@ attribute required="false" rtexprvalue="true" name="switcherType"%>
<%@ attribute required="false" rtexprvalue="true" name="width" description="DEPRECATED"%>
<%@ attribute required="false" rtexprvalue="true" name="disabled" description="NOT implemented"%>
<%@ attribute required="false" rtexprvalue="true" name="onclick"%>
<%@ attribute name="isTableObject" required="false" rtexprvalue="true"%>
<%@ attribute name="tooltipContent" required="false" rtexprvalue="true"%>
<%@ attribute name="collapseMobile" required="false" rtexprvalue="true"%>

<%-- <c:set var="switcherWidth"	value="${switcher == true ? 'grid90' : 'grid60'}" />
<c:set var="labelWidth"		value="${switcher == true ? 'grid10' : 'grid40'}" /> --%>
<c:choose>
	<c:when test="${switcher == true}">
		<c:set var="switcherWidth" value="grid90" />
		<c:set var="labelWidth" value="grid10" />
	</c:when>
	<c:when test="${empty label&&switcher != true}">
		<c:set var="switcherWidth" value="grid100" />
	</c:when>
	<c:otherwise>
		<c:set var="switcherWidth" value="grid60" />
		<c:set var="labelWidth" value="grid40" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${empty isTableObject}">
	 <div class="formElementWrapper gridGroup radioButtonContainer ${className}<c:if test='${not empty collapseMobile}'> tableHeaderControls mobiHeaderControls</c:if><c:if test='${switcher == true}'> teal7 switcherAdjust</c:if> clearfix">
			<c:if test='${not empty collapseMobile}'><div class="mobi-header-trigger"></div></c:if>
			<c:if test='${not empty label}'>
				<div class="formElementLabel gridCol ${labelWidth} switcherLabel">${label}</div>
			</c:if>
			<div class="gridCol ${switcherWidth} formElementContainer">
				<div id="${id}" class="radioButtonsGroup <c:if test='${switcher == true}'>switcherGroup</c:if><c:if test="${disabled == 'true'}"> disabled</c:if>">
					<div class="mobi-dropdown-trigger" onclick="fnb.utils.mobile.switcher.expandSwitcher(this);"></div>
					<c:choose>
						<c:when test="${not empty radioGroupViewBean}">
							<c:set var="selectedVal" value=""></c:set>
							<c:forEach items="${radioGroupViewBean.radioItems}" var="radioItem">
								<c:choose>
									<c:when test='${radioItem.value == radioGroupViewBean.selectedValue}'>
										<c:set var="selectedVal" value="${radioItem.value}"></c:set>
										<base:radioButtonItem id="${id}" value="${radioItem.value}" description="${radioItem.description}" selected="true" onclick="${onclick}" />
									</c:when>
									<c:otherwise>
										<base:radioButtonItem id="${id}" value="${radioItem.value}" description="${radioItem.description}" onclick="${onclick}" />
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<input class="radioGroupValue" name="${name}" id="${id}" value="${selectedVal}" />
						</c:when>
						<c:otherwise>
							<jsp:doBody />
							<input class="radioGroupValue" name="${name}" id="${id}" value="${value}" />
						</c:otherwise>
					</c:choose>
				</div>
				<c:if test="${not empty tooltipContent}">
					<base:tooltip id="${id}Tooltip" content="${tooltipContent}" />
				</c:if>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div id="${id}" class="radioButtonsGroup">
			<c:choose>
				<c:when test="${not empty radioGroupViewBean}">
					<c:set var="selectedVal" value=""></c:set>
					<c:forEach items="${radioGroupViewBean.radioItems}" var="radioItem">
						<c:choose>
							<c:when test='${radioItem.value == radioGroupViewBean.selectedValue}'>
								<c:set var="selectedVal" value="${radioItem.value}"></c:set>
								<base:radioButtonItem id="${id}" value="${radioItem.value}" description="${radioItem.description}" selected="true" onclick="${onclick}" />
							</c:when>
							<c:otherwise>
								<base:radioButtonItem id="${id}" value="${radioItem.value}" description="${radioItem.description}" onclick="${onclick}" />
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<input class="radioGroupValue" type="hidden" name="${name}" id="${id}" value="${selectedVal}" />
				</c:when>
				<c:otherwise>
					<jsp:doBody />
					<input class="radioGroupValue" type="hidden" name="${name}" id="${id}" value="${value}" />
				</c:otherwise>
			</c:choose>
		</div>
	</c:otherwise>
</c:choose>