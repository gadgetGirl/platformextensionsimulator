<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="number" required="true" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="heading" required="true" rtexprvalue="true"%>
<%@ attribute name="description" required="true" rtexprvalue="true"%>
<%@ attribute name="descriptionContinue" required="true" rtexprvalue="true"%>
<%@ attribute name="link" required="true" rtexprvalue="true"%>
	
	<div id="banner${number}" class="${className} bannerContainer">
		<div class="bannerHeading">${heading}</div>
		<div class="bannerColumn1">
			<div class="bannerDescription">${description}</div>
		</div>
		<div class="bannerColumn2">
			<div class="bannerDescriptionContainer">
				<div class="bannerDescriptionContinue">${descriptionContinue}</div>
				<div class="bannerLinkThrough"><a href="${link}"><img src="../03images/fnb/arrowRightBlue.png"></a></div>
			</div>
			
		</div>
		<div style="clear: both;"></div>
	</div>