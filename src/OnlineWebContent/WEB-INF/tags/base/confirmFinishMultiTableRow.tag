<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="id" required="false"%>

<div id="${id}" class="tableRow tableDataRow clearfix">
	<div class="tableRowInner clearfix">
		<div class="tableGroup groupColCell grid100 clearfix">
			<div class="tableRowGroupInner">
				<jsp:doBody></jsp:doBody>
			</div>
		</div>
	</div>
</div>