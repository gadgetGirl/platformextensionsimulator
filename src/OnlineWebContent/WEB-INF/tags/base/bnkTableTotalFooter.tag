<%@tag import="fnb.online.tags.beans.table.TableUtilities"%>
<%@ tag import="fnb.online.tags.beans.table.TableColumnGroup"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="tableRowGroup" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableRowGroup"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="action" required="false" rtexprvalue="true"%>
<%@ attribute name="number" required="false" rtexprvalue="true"%>
<jsp:useBean id="currency" class="java.lang.String" scope="request" />
<div class="tableCellItem ">
	<div class="totalCellInner ${action} <c:if test='${number == 1}'>left</c:if>">
		
		<c:choose>
		<c:when test="${number == 1}">
		Total
		</c:when>
		<c:otherwise>
		<%
			String content = "";
			if (columnOptions.isCalculateTotal()) {
				content =  currency + TableUtilities.calculateColumnTotal(tableRowGroup,columnOptions.getFieldName());
			}else{
			    content = columnOptions.getFixedTotal();
			}
		%>
		<%=("").equals(content) ? "&nbsp;" : content%>
		</c:otherwise>
		</c:choose>
	</div>
</div>
