<%@ tag import="fnb.online.tags.beans.table.*"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableEziButtonItem"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="newFrameWorkFlag" required="false" %>
 
<c:set var="buttons" value="<%=tableItem.getButtons()%>" />	
<c:set var="buttonItem" value="${buttons.size()}"/>

<base:eziLinks>
	<c:forEach items="${buttons}" var="button" varStatus="buttonIndex">
		<base:eziLink id="eziBtn${buttonIndex.index+1}" url="${button.url}" target="${button.target}" text="${button.text}" newFrameWorkFlag="${newFrameWorkFlag }" number="${buttonIndex.index+1}" disabled="${button.disabled}" className="${button.cssClass}"/>
	</c:forEach>
</base:eziLinks>