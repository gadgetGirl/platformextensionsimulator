<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="description" required="false"%>
<%@ attribute name="applylink" required="true"%>
<%@ attribute name="applytext" required="true"%>
<%@ attribute name="callmebacklink" required="true"%>
<%@ attribute name="callmebacktext" required="true"%>

<p>${description}</p>
<a class="amber1 applyOnlineBtt" href="#" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen',$(this).attr('href'));" class="amber1 applyOnlineBtt">${applytext}</a>
<a class="teal5 callMeBackBtt" href="#" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen',$(this).attr('href'));" class="teal5 callMeBackBtt">${callmebacktext}</a>
