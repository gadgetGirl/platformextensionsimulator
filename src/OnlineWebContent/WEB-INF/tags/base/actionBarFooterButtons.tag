<%@tag import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>

<div id="actionBarFormFooterButtons">
	<c:forEach items="${footerButtons}" var="button">
		<base:footerButton text="${button.label}" className="main" url="${button.url}" />
	</c:forEach>
	<jsp:doBody></jsp:doBody>
</div>