<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%-- <%@ attribute required="true" rtexprvalue="true" name="switcherViewBean" type="fnb.online.tags.beans.tablecontrolsswitcher.Switcher"%> --%>
<%@ attribute required="true" rtexprvalue="true" name="tableId"%>
<%@ attribute required="true" rtexprvalue="true" name="tableBean" type="fnb.online.tags.beans.table.TableBean"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="label"%>
<%@ attribute required="false" rtexprvalue="true" name="actionButtonsLabel"%>
<c:set var="label" value="${not empty label ? label : 'Viewing'}" />
<c:set var="id" value="${empty switcherViewBean.id ? '' : switcherViewBean.id}" />
<c:if test="${empty id}">
	<c:set var="id">${tableId}_switcher</c:set>
</c:if>
<%--
Current item:
EITHER
	<radioButtonContainer value="current RFN">
OR
	<radioButtonItem selected="true">
--%>
<base:gridGroup id="${tableId}_headerControls" className="tableHeaderControls mobiHeaderControls ${className}">
	<base:headerControlLabel text="${label}" />
	<c:set var="counter" value="1" />
	<div class="mobi-dropdown-trigger" onclick="fnb.utils.mobile.switcher.expandSwitcher(this);"></div>
	<div class="tableSwitcherItemsContainer">
		<c:forEach items="${tableBean.switcher.actions}" var="action">
			<base:tableSwitcherButtonItem description="${action.label}" id="tableSwitcherButton_${counter}" url="${action.url}" value="${action.rfn}" selected="${action.selected}" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url:'${action.url}'});" />
			<c:set var="counter" value="${counter+1}" />
		</c:forEach>
		<jsp:doBody />
	</div>
	<div class="tableActionButtonContainer">
		<div class="headerControlLabel">
			<div class="labelInner">${actionButtonsLabel}</div>
		</div>
		<c:forEach items="${tableBean.tableHeaderButtons}" var="buttonGroups">
			<div class="tableActionButton">
				<div class="tableActionButtonLabel">${buttonGroups.key}</div>
				<c:forEach items="${buttonGroups.value.buttons}" var="headerButton">
					<div class="tableActionButtonContent">
						<c:set var="onclickFunction">
							<c:choose>
								<c:when test="${headerButton.target == '0'}">fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${headerButton.url}');</c:when>
								<c:when test="${headerButton.target == '1'}">fnb.utils.actionMenu.loadTargetToActionMenu('${headerButton.url}',this);</c:when>
								<c:when test="${headerButton.target == '3'}">fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '${headerButton.url}'});</c:when>
								<c:when test="${headerButton.target == '4'}">fnb.functions.loadUrlToPrintDiv.load('${headerButton.url}');</c:when>
								<c:when test="${headerButton.target == '5'}">fnb.controls.controller.eventsObject.raiseEvent('doDownload','${headerButton.url}');</c:when>
								<c:when test="${headerButton.target == '6'}">fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '${headerButton.url}');</c:when>
							</c:choose>
						</c:set>
						<a onclick="${onclickFunction};return false;">${headerButton.label}</a>
					</div>
				</c:forEach>
			</div>
		</c:forEach>
		<div class="tableActionButton">
			<div class="tableActionButtonLabel">Search</div>
			<base:tableSearchBar tableId="${tableId}" tableBean="${tableBean}" />
		</div>
		<div class="tableActionButton">
			<div class="tableActionButtonLabel">Close</div>
		</div>
		<div class="tableActionButton">
			<div class="tableActionButtonLabel">Download</div>
		</div>
		<div class="tableActionButton">
			<div class="tableActionButtonLabel">Print</div>
		</div>
	</div>
</base:gridGroup>