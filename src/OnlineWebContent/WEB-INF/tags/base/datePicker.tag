<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>

<%@ attribute required="true"  rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="name"%>
<%@ attribute required="false" rtexprvalue="true" name="label"%>
<%@ attribute required="false" rtexprvalue="true" name="value"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="width"%>
<%@ attribute required="false" rtexprvalue="true" name="fancyDate"%>
<%@ attribute required="false" rtexprvalue="true" name="past"%>
<%@ attribute required="false" rtexprvalue="true" name="future"%>
<%@ attribute required="false" rtexprvalue="true" name="parentClassName"%>
<%@ attribute required="false" rtexprvalue="true" name="noticedays"%>

<%@ attribute required="false" rtexprvalue="true" name="tooltipContent"%>

<%@ attribute name="publicHolidaysCurrentBean" required="false" rtexprvalue="true" type="mammoth.util.HolidayCalendar"%>
<%@ attribute name="publicHolidaysNextBean" required="false" rtexprvalue="true" type="mammoth.util.HolidayCalendar"%>

<%@tag import="mammoth.util.HolidayCalendar"%>
<%@tag import="mammoth.util.Holiday"%>
<%@tag import="java.text.SimpleDateFormat"%>
<%@tag import="java.util.Iterator"%>

<c:set var="name" value="${not empty name ? name : id}" />
<c:set var="value" value="${fn:substring(value, 0, 10)}" />

<c:if test="${not empty width}">
	<c:set var="widthLabelOpen"><div class="grid${width} floatLeft"></c:set>
	<c:set var="widthPickerOpen"><div class="floatLeft"></c:set>
	<c:set var="widthClose"></div></c:set>
</c:if>

<div id="${id}_datePicker" class="datePicker formElementWrapper<c:if test='${not empty parentClassName}'> ${parentClassName}</c:if><c:if test='${not empty tooltipContent}'> toolTipSpacer</c:if><c:if test='${not empty fancyDate}'> fancyDate</c:if> clearfix"<c:if test='${not empty past}'> data-past="${past}"</c:if><c:if test='${not empty future}'> data-future="${future}"</c:if>>
	<c:if test="${not empty label}">
		<div class="formElementLabel gridCol grid40 ${className}">
			${label}
		</div>
	</c:if>

	<div class="gridCol grid60 formElementContainer ${className}">
		
		<div class="datePickerDate">
		
			<div class="datepickerWrapper">
			<div id="dropArrow" class="dropArrow"></div>	
				<div class="datePickerWeekday"></div>
				<div class="datePickerDay"></div>
				<div class="datePickerDayYear">
					<div class="monthName"></div>
					<div class="datePickerYear"></div>
				</div>
			</div>
			</div>
		<input class="formsDatePickerValue" id="${id}" name="${name}" value="${value}" type="hidden"/>
		<c:if test="${not empty tooltipContent}">
			<base:tooltip content="${tooltipContent}" />
		</c:if>
	</div>

	<span id="calendar_1"></span>
</div>

<script type="text/javascript">
	_datePicker.init('${id}_datePicker','${value}','${noticedays}');
	var disableHolidayCheck = false;
</script>

<c:if test="${empty publicHolidaysCurrentBean}">
	<script type="text/javascript">
		disableHolidayCheck = true;
	</script>
</c:if>

<script>

$(function(){
	function datePickerVerify(){
		this.dateVerified = false;
		this.validDate = true;
	};
	datePickerVerify.prototype = {
		init: function(value){
			this.dateVerified = false;
			this.validDate = true;
			<%	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Iterator i = null;
				Holiday holiday = null;
				java.util.Date disabledDate = null;
				if (publicHolidaysCurrentBean!=null && publicHolidaysCurrentBean.getHolidays()!=null) {
					i= publicHolidaysCurrentBean.getHolidays().iterator();
				  	while(i.hasNext()) {
				  		holiday = (Holiday)i.next();
				  		disabledDate = holiday.getDate(publicHolidaysCurrentBean.get(HolidayCalendar.YEAR));%>
			<%	  	}
				}
				if (publicHolidaysNextBean!=null && publicHolidaysNextBean.getHolidays()!=null) {
					i= publicHolidaysNextBean.getHolidays().iterator();
				  	while(i.hasNext()) {
				  		holiday = (Holiday)i.next();
				  		disabledDate = holiday.getDate(publicHolidaysNextBean.get(HolidayCalendar.YEAR));%>			
			<%	  	}
				}%>

			fnb.functions.datePickerVerify.verifyDateValid(value);
		},
		verifyDateValid: function(field) {
			this.verifyDateValidCustomString(field, "Date entered is a Sunday or Public Holiday, would you like to continue?");
		},
		verifyDateValidOnPay: function(field, postCallback) {
			this.verifyDateValidCustomString(field, "Date entered is a Sunday or Public Holiday, would you like to continue?");
			postCallback();
		},
		verifyDateValidCustomString: function(field, confirmString) {
			this.dateVerified = true;
			this.validDate = true;
			var date = field;
			var selectedDate=new Date();
			var d = date.split("-");
			selectedDate.setFullYear(d[0],d[1]-1,d[2]);

			if (selectedDate.getDay()==0) {
				this.confirmHoliday(confirmString);
			}
			<%	sdf.applyPattern("yyyy-MM-dd");
				
				if (publicHolidaysCurrentBean!=null && publicHolidaysCurrentBean.getHolidays()!=null) {
					i= publicHolidaysCurrentBean.getHolidays().iterator();
				  	while(i.hasNext()) {
				  		holiday = (Holiday)i.next();
				  		disabledDate = holiday.getDate(publicHolidaysCurrentBean.get(HolidayCalendar.YEAR));%>			
				  		if ("<%=sdf.format(disabledDate)%>" == date) {
				  			this.confirmHoliday(confirmString);
						}
				<%	}
				}
				if (publicHolidaysNextBean!=null && publicHolidaysNextBean.getHolidays()!=null) {
					i= publicHolidaysNextBean.getHolidays().iterator();
				  	while(i.hasNext()) {
				  		holiday = (Holiday)i.next();
				  		disabledDate = holiday.getDate(publicHolidaysNextBean.get(HolidayCalendar.YEAR));%>					  	
				  		if ("<%=sdf.format(disabledDate)%>" == date) {
				  			this.confirmHoliday(confirmString);
						}
				<%	}
				}%>
				return true;
		},
		confirmHoliday: function(confirmString) {
			if (disableHolidayCheck) {
				this.validDate = true;
				this.dateVerified = true;
			}
			else {
				this.validDate = false;
			}
		}
	};
	namespace("fnb.functions.datePickerVerify",datePickerVerify);
});

namespace("fnb.functions.datePickerVerify",new fnb.functions.datePickerVerify());

fnb.functions.datePickerVerify.init('${value}');

</script>
