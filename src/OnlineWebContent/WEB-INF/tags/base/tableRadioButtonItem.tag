<%@tag import="fnb.online.tags.beans.table.TableRadioButtonItem"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableRadioButtonItem"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="count" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="row" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="isTableObject" required="false" rtexprvalue="true"%>
<%@ attribute name="onclick" required="false" rtexprvalue="true" %>

<c:choose>
	<c:when test="${tableDoubleItem == true}">
		<base:radioButtonContainer isTableObject="${isTableObject}" id="" count="${count}" radioGroupViewBean="${tableItem.radioGroup}" onclick="${onclick}" />
	</c:when>
	<c:otherwise>
		<div id="${tableItem.id}_${row}" name="${tableItem.name}" class="tableCellItem tableRadioButtonCell ${columnOptions.size} ${tableItem.name}" >
			<base:radioButtonContainer isTableObject="${isTableObject}" count="${tableItem.useCount?count:''}" id="${tableItem.id}_${count}" name="${tableItem.name}${count}" radioGroupViewBean="${tableItem.radioGroup}" onclick="${onclick}" />
		</div>
	</c:otherwise>
</c:choose>