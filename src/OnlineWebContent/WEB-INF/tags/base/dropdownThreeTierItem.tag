<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute required="true"  rtexprvalue="true" name="h1" %>
<%@ attribute required="true"  rtexprvalue="true" name="h2" %>
<%@ attribute required="false" rtexprvalue="true" name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="onclick" %>
<%@ attribute required="false" rtexprvalue="true" name="returnValue" %>
<%@ attribute required="false" rtexprvalue="true" name="selected" %>
<%@ attribute required="false" rtexprvalue="true" name="controller" %>

<c:set var="returnValue" value="${not empty returnValue ? returnValue : content}" />
<c:set var="selectedClass" value="${selected ? ' selected-item' : ''}" />

<li <c:if test="${not empty id}">id="${id}"</c:if> class="dropdown-item clearfix ${className}${selectedClass}" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if> data-value="${returnValue}">
	<div class="dropdown-item-row-h1"><div class="dropdown-h1">${h1}</div></div>
	<div class="dropdown-item-row-h2"><div class="dropdown-h2">${h2}</div></div>
	<c:if test="${not empty controller}">
		<div class="ammountsHolder dropdown-item-row"></div>
		<input type="hidden" id="${id}Controller" name="${name}Controller" value="${controller}" class="nonSubmittable"/>
	</c:if>
</li>