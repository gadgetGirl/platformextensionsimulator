<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="type"			description="tc, note, custom"%>
<%@ attribute required="false" rtexprvalue="true" name="noteHeading"	description="Defaults for-> Note: 'Please Note', TC: 'Terms & Conditions'"%>
<%@ attribute required="false" rtexprvalue="true" name="content"%>
<%@ attribute required="false" rtexprvalue="true" name="onclick"%>
<%-- DEFAULT TITLES: uncomment when decision is finalized --%>

<c:set var="note_default_title" value="Please Note" />
<c:set var="tc_default_title" value="Terms &amp; Conditions" />
<c:set var="top_default_title" value="" />

<c:set var="type" value="${not empty type ? type : 'note'}" />
<c:if test="${not empty content}"><c:set var="content"><base:paragraph>${content}</base:paragraph></c:set></c:if>
<c:choose>
	<c:when test="${type == 'note'}">
		<c:set var="noteHeading" value="${not empty noteHeading ? noteHeading : note_default_title}" />
		<c:set var="className" value="note note-normal teal7 ${className}" />
	</c:when>
	<c:when test="${type == 'tc'}">
		<c:set var="noteHeading" value="${not empty noteHeading ? noteHeading : tc_default_title}" />
		<c:set var="className" value="note note-tc teal7 ${className}" />
	</c:when>
	<c:when test="${type == 'top'}">
		<c:set var="noteHeading" value="${not empty noteHeading ? noteHeading : top_default_title}" />
		<c:set var="className" value="note note-top teal7 ${className}" />
	</c:when>
	<c:when test="${type == 'custom'}">
		<%-- Only way to override default background-color, which is not recommended. -- lotto and powerball uses it --%>
		<c:set var="className" value="note ${className}" />
	</c:when>
</c:choose>
<c:if test="${not empty id}">
<c:set var="outerId" value="noteOuter_${id}"/>
</c:if>

<base:equalHeightsRow className="noteOuterWrap" id="${outerId}">
	<base:equalHeightsColumn width="100">
		<div<c:if test="${not empty id}"> id="${id}"</c:if> class="${className} clearfix"<c:if test="${not empty onclick}"> onclick="${onclick}"</c:if>>
		<c:if test="${not empty noteHeading}"><base:heading level="4" value="${noteHeading}" className="note-heading" /></c:if>
		${content}<jsp:doBody />
		</div>
	</base:equalHeightsColumn>
	<base:equalHeightsColumn ghostBlock="true" />
</base:equalHeightsRow>