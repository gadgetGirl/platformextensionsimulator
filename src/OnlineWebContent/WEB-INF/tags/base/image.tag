<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="true"  name="src"%>
<%@ attribute required="false" name="id"		description="id is added on the wrapping DIV not the image itself"%>
<%@ attribute required="false" name="className"	description="class is added on the wrapping DIV not the image itself"%>
<%@ attribute required="false" name="width"%>
<%@ attribute required="false" name="height"%>
<%@ attribute required="false" name="alt"%>
<%@ attribute required="false" name="title"%>
<%@ attribute required="false" name="onclick"%>

<div<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty className}"> class="${className}"</c:if>>
<img src="${src}"<c:if test="${not empty className}"> class="${className}image"</c:if>
<c:if test="${not empty onclick}"> onclick="${onclick}"</c:if>
<c:if test="${not empty width}"> width="${width}"</c:if>
<c:if test="${not empty height}"> height="${height}"</c:if>
<c:if test="${not empty alt}"> alt="${alt}"</c:if>
<c:if test="${not empty title}"> alt="${title}"</c:if>
/>
</div>