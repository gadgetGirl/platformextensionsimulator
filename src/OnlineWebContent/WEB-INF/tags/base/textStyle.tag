<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="true"  rtexprvalue="true" name="content"%>
<%@ attribute required="true"  rtexprvalue="true" name="type" description="valid type value: RESULTING HTML TAG [b,strong,bold: STRONG] [i,em,italic: EM] [del,strike: DEL] [u,underline: U] [sup: SUP] [sub: SUB] [empty type: SPAN]"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<c:set var="classAttr"><c:if test="${not empty className}">class="${className}"</c:if></c:set>
<c:set var="content">${content}<jsp:doBody/></c:set>
<c:choose>
<%-- B, STRONG, BOLD	--%><c:when test="${type == 'b' || type == 'strong' || type == 'bold'}"><c:set var="htmlTag" value="strong"/></c:when>
<%-- I, EM, ITALIC 		--%><c:when test="${type == 'i' || type == 'em' || type == 'italic'}"><c:set var="htmlTag" value="em"/></c:when>
<%-- DEL, STRIKE 		--%><c:when test="${type == 'strike' || type == 'del'}"><c:set var="htmlTag" value="del"/></c:when>
<%-- UNDERLINE 			--%><c:when test="${type == 'u' || type == 'underline'}"><c:set var="htmlTag" value="u"/></c:when>
<%-- SUP (SUPER SCRIPT) --%><c:when test="${type == 'sup'}"><c:set var="htmlTag" value="sup"/></c:when>
<%-- SUB (SUB SCRIPT) 	--%><c:when test="${type == 'sub'}"><c:set var="htmlTag" value="sub"/></c:when>
<%-- SPAN 				--%><c:otherwise><c:set var="htmlTag" value="span"/></c:otherwise>
</c:choose>
<${htmlTag} ${classAttr}>${content}</${htmlTag}>