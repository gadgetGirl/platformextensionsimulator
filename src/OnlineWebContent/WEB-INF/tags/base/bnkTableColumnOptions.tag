<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ tag import="fnb.online.tags.beans.table.TableColumnGroup"%>
<%@ tag import="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ tag import="fnb.online.tags.beans.table.BeanUtilities"%>

<%@ attribute name="heading" required="true" rtexprvalue="true"%>
<%@ attribute name="shortHeading" required="false" rtexprvalue="true"%>
<%@ attribute name="fieldName" required="false" rtexprvalue="true"%>
<%@ attribute name="align" required="false" rtexprvalue="true" description="Column CSS class. eg. left, right OR center"%>
<%@ attribute name="size" required="false" rtexprvalue="true" description="Cell item CSS class. eg. large OR mediumText"%>
<%@ attribute name="headingSize" required="false" rtexprvalue="true" description="NOT IMPLEMENTED"%>
<%@ attribute name="topSize" required="false" rtexprvalue="true" description="Double stacked cell, TOP item CSS class. eg. large OR mediumText"%>
<%@ attribute name="bottomSize" required="false" rtexprvalue="true" description="Double stacked cell, BOTTOM item CSS class. eg. large OR mediumText"%>
<%@ attribute name="cssClass" required="false" rtexprvalue="true" description="Column heading cell only CSS class"%>
<%@ attribute name="calculateTotal" required="false" rtexprvalue="true"%>
<%@ attribute name="hideColumn" required="false" rtexprvalue="true"%>
<%@ attribute name="calcFieldTotals" required="false" rtexprvalue="true"%>
<%@ attribute name="onClick" required="false" rtexprvalue="true"%>
<%@ attribute name="onKeyup" required="false" rtexprvalue="true"%>
<%@ attribute name="onChange" required="false" rtexprvalue="true"%>
<%@ attribute name="maxLength" required="false" rtexprvalue="true"%>
<%@ attribute name="selectAll" required="false" rtexprvalue="true"%>
<%@ attribute name="icon" required="false" rtexprvalue="true"%>
<%@ attribute name="checkBoxGroup" required="false" rtexprvalue="true"%>
<%@ attribute name="duplicateColumnValues" required="false" rtexprvalue="true" description="NOT IMPLEMENTED"%>
<%@ attribute name="fixedTotal" required="false" rtexprvalue="true"%>

<%   
String group_id = BeanUtilities.getFieldValuePairs(getParent(),false).get("group_id");
if (group_id==null){
	group_id="group1";
}
TableColumnGroup tableColumnGroup = (TableColumnGroup) request.getAttribute(group_id);
TableColumnOptions tableColumnOptions = new TableColumnOptions(heading, shortHeading, fieldName, align, size, headingSize, topSize, 
		bottomSize, cssClass, Boolean.valueOf(calculateTotal),hideColumn, calcFieldTotals,onClick,onKeyup,onChange,maxLength,Boolean.valueOf(selectAll),icon,Boolean.valueOf(checkBoxGroup));
tableColumnGroup.getColumnOptions().add(tableColumnOptions);
tableColumnOptions.setFixedTotal(fixedTotal==null?"":fixedTotal);
  
%>