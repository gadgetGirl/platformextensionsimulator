<%@ tag import="java.util.List"%>

<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute required="true" rtexprvalue="true" name="value"%>
<%@ attribute required="true" rtexprvalue="true" name="name"%>
<%@ attribute required="true" rtexprvalue="true" name="list" type="java.util.List"%>
<%@ attribute required="false" rtexprvalue="true" name="description"%>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="type"%>

<c:forEach var="item" items="${list}">
	<c:if test="${item eq name}">
		<c:set var="className" value="${className} pending" />
	</c:if>
</c:forEach>

<c:choose>
	<c:when test="${type == 'list'}">
		<base:definitionList>
			<base:definitionListItem description="${description}" value="${value}" className="${className}" />
		</base:definitionList>
	</c:when>
	<c:otherwise>
		<base:spanContainer className="${className}">${value}</base:spanContainer>
	</c:otherwise>
</c:choose>