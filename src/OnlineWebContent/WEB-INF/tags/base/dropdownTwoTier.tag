<%@ tag import="fnb.online.tags.beans.dropdown.*"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="dropdownBean" required="false" rtexprvalue="true" type="fnb.online.tags.beans.dropdown.DropDown"%>

<%@ attribute required="true" rtexprvalue="true"  name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="name" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="overflow" %>
<%@ attribute required="false" rtexprvalue="true" name="label" %>
<%@ attribute required="false" rtexprvalue="true" name="labelWidth" %>
<%@ attribute required="false" rtexprvalue="true" name="labelTop" %>
<%@ attribute required="false" rtexprvalue="true" name="selectWidth" %>
<%@ attribute required="false" rtexprvalue="true" name="searchClass" %>
<%@ attribute required="false" rtexprvalue="true" name="selectedValue" %>
<%@ attribute required="false" rtexprvalue="true" name="onchange" %><%-- not yet fully tested, added for backwards compatibility --%>
<%@ attribute required="false" rtexprvalue="true" name="margin" %>

<%@ attribute required="false" rtexprvalue="true" name="disabled" %>
<%@ attribute required="false" rtexprvalue="true" name="reverse" %>

<%@ attribute required="false" rtexprvalue="true" name="height" %>
<%-- SET overflow
	overflow(true)  = 1 = overflow
	overflow(false) = 2 = static
--%>

<c:choose>
	<c:when test="${not empty overflow}">
		<c:set var="overflow" value="${overflow ? 1 : 2}" />
	</c:when>
	<c:otherwise>
		<c:set var="overflow" value="1" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${(not empty labelTop) && (labelTop == true)}">
		<c:set var="labelWidth" value="100" />
		<c:set var="selectWidth" value="100" />
	</c:when>
	<c:when test="${empty label}">
		<c:set var="labelWidth" value="0" />
		<c:set var="selectWidth" value="100" />
	</c:when>
	<c:otherwise>
		<c:set var="labelWidth" value="${(not empty labelWidth) ? labelWidth : 40}" />
		<c:set var="selectWidth" value="${(not empty selectWidth) ? selectWidth : 100-labelWidth}" />
	</c:otherwise>
</c:choose>

<c:if test="${(selectWidth+labelWidth) == 100 }">
	<c:set var='marginLeft' value='marginLeft${labelWidth}'/>
</c:if>

<c:set var="searchClass" value="${(not empty searchClass) ? searchClass : 'dropdown-h4'}" />
<c:set var="name" value="${(not empty name) ? name : id}" />
<c:set var="selectedValue" value="${(not empty dropdownBean.selectedValue) ? dropdownBean.selectedValue : selectedValue}"/>
<c:set var="disabledClass" value="${disabled == 'true' ? ' dropdown-disabled' : ''}" />
<c:set var="disabledAttributes">
	<c:choose>
		<c:when test="${disabled == 'true'}">
			data-disabled-overflow="${overflow}" data-disabled-searchClass="${searchClass}" data-disabled-reverse="${reverse}" data-disabled-height="${height}"
		</c:when>
		<c:otherwise>
			onclick="fnb.forms.dropdown.expand($(this),${overflow},'${searchClass}','${reverse}','${height}');"
		</c:otherwise>
	</c:choose>
</c:set>

<div id="${id}_parent" class="dropdown-wrapper dropdown-two-wrapper ${margin}${disabledClass}">
	<input id="${id}" name="${name}" value="${selectedValue}" class="dropdown-hidden-input dropdown-keyboard-tab"/>
<c:if test="${not empty label}"><div class="gridCol grid${labelWidth} dropdownLabelWrapper"><div class="dropdown-label-inner">${label}</div></div></c:if>
	<div class="gridCol grid${selectWidth} dropdown-select dropdownElementWrapper">
		<div id="${id}_dropId" class="dropdown-initiator ${className}" ${disabledAttributes}>
			<div class="dropdown-trigger twoTier-trigger"></div>
			<div class="dropdown-selection-white dropdown-h4"></div>
		</div>
		<div class="dropdown-content-wrapper">
		<c:if test="${not empty label}"><div class="mobiDropdownInnerLabelWrapper">${label}</div><div class="mobiDropdownClose"></div></c:if>
				<ul class="dropdown-content" data-labelwidth="${labelTop ? 20 : labelWidth}" data-selectwidth="${labelTop ? 100 : selectWidth}">
				<div class="dropdown-content-top">
					<div class="dropdown-input-wrapper">
						<div class="dropdown-input-icon-search"></div>
						<div class="dropdown-input-container ${labelTop ? 'grid100' : ''}"><input class="dropdown-input" placeholder="Search..." value=""></div>
					</div>
				</div>
				<c:choose>
					<c:when test="${not empty dropdownBean.items}">
						<c:forEach items="${dropdownBean.items}" var="item" >
							<c:set var="passSelected" value="${((item.returnValue == selectedValue) || (item.selected == true)) ? true : false}"/>
							<c:if test="${not empty className}"><c:set var="classNameItem" value="${className}_item" /></c:if>
							<base:dropdownTwoTierItem
								id=""
								className=		"${classNameItem}"
								onclick=		"${onchange}"
								content1=		"${item.topHeading}"
								content2=		"${item.topSubHeading}"
								returnValue=	"${item.returnValue}"
								selected=		"${passSelected}" />
						</c:forEach>
					</c:when>
					<c:otherwise>
						<jsp:doBody />
					</c:otherwise>
				</c:choose>
			</ul>
			
		</div>
	</div>

</div>
<script type="text/javascript">

var idTemp = "${id}"

	fnb.forms.dropdown.init($('#'+idTemp+'_dropId'),'${selectedValue}');

</script>