<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld" %>


<%@ attribute required="true"  rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="name"%>
<%@ attribute required="false" rtexprvalue="true" name="label"%>
<%@ attribute required="false" rtexprvalue="true" name="value"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="description"%>

<style>
.selectorSelectedImageBlock{
	width: 100%;
}
.selectorSelectedImageBlockImage{
	width: 190px;
	height: 130px;
	border: thin solid Silver;
	cursor: pointer;
	float: left;
	background-color: #F6F6F6;
}
.selectorSelectedImageBlockContent{
	float: left;
}
.labelTextWidthFix{
	width: 220px;;
}
.SelectorImageEmpty{
	padding-top: 38px;
}
#selectorProductDescription{
	width: 190px;
	height: 30px;
	text-align: center;
	vertical-align: text-top;
	padding-bottom: 5px;
}
#selectorProductDescriptionBottom{
	width: 190px;
	height: 30px;
	text-align: center;
	vertical-align: text-top;
	padding-top: 5px;
}
#selectorTabletScrollerLeft{
	cursor: pointer;
	float: left;
	padding: 80px 10px 0 5px;
	/* display: none; */
}

#selectorTabletScrollerRight{
	cursor: pointer;
	float: right;
	padding: 80px 45px 0 10px;
	/* display: none; */
}
#cardImageHolder{
	border-bottom-width: 6px;
	border-bottom-style: solid;
	border-bottom-color: #57b9ba;
	border-top-width: 6px;
	border-top-style: solid;
	border-top-color: #57b9ba;
}

.selectorSelectedImage, .pricingOption, .threeGdata, .usbDataDongle, .accessory, .deviceInsurance, .productName {
	display: none;
} 

deviceInsuranceText{
	float: left;
}
</style>

<!-- First Display ----------------------------------------------------------------------------------------------------------------------------------- -->

	
	<div class="selectorSelectedImageBlock ${className}" id="${id}">
		<div class="selectorSelectedImageBlockImage margin50" onclick="fnbDeviceSelectionPickerObject.show();">
			<img class="selectorFirstImage" id="selectorFirstImage" src="" onclick="fnbDeviceSelectionPickerObject.show();">
			<base:paragraph className="SelectorImageEmpty margin50">
				${description}
			</base:paragraph>
			<!-- form data xxxxxxxxxxxxxxxxxxxxxx -->
			<input type="hidden" id="device" name="device" value="" >
			<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
			
			<input type="hidden" id="devicetab${i.count-1}" name="devicetab${i.count-1}" class="selectorFirstField" value="" >
			<input type="hidden" id="deviceDescription" name="" value="" >
		</div>
		<div class="selectorSelectedImageBlockContent">
			<base:heading className="productName" level="5" value="391W8 Acer S3 II 500GB HDD + 20GB SSD "/>
			
			<base:radioButtonContainer className="pricingOption margin50" id="devicePricing" label="Please select the pricing option for your Cheque Account." labelPosition="labelLeft labelTextWidthFix" name="devicePricing" value="" >	
				<base:radioButtonItem className="mobileDevicePricingOption" description="Mobile Device Pricing Option" id="" value="mobile" />
				<base:radioButtonItem className="payAsYouUse" description="Pay As You Use" id="" value="pay" selectedValue="no"/>
			</base:radioButtonContainer>
			
			<base:radioButtonContainer className="threeGdata margin50" id="3Gdata" label="Would you like to include a(n) 3G Sim card Data Contract?" labelPosition="labelLeft labelTextWidthFix" name="3Gdata" value="" >	
				<base:radioButtonItem description="Yes" value="yes" />
				<base:radioButtonItem description="No" value="no" selectedValue="no"/>
			</base:radioButtonContainer>
			
			<base:radioButtonContainer className="usbDataDongle margin50" id="usbDataDongle" label="Would you like to include a(n) USB Data Dongle?" labelPosition="labelLeft labelTextWidthFix" name="usbDataDongle" value="" >	
				<base:radioButtonItem description="Yes" value="yes" />
				<base:radioButtonItem description="No" value="no" selectedValue="no"/>
			</base:radioButtonContainer>
			
			<base:radioButtonContainer className="accessory margin50" id="accessory" label="Would you like to include an Accessory?" labelPosition="labelLeft labelTextWidthFix" name="accessory" value="" >	
				<base:radioButtonItem description="Yes" value="yes" />
				<base:radioButtonItem description="No" value="no" selectedValue="no"/>
			</base:radioButtonContainer>

			<base:radioButtonContainer labelClassName="deviceInsuranceText" className="deviceInsurance margin50" id="deviceInsurance" label="Would you like to add device insurance for R65.09 per month?" labelPosition="labelLeft labelTextWidthFix" name="deviceInsurance" value="" >	
				<base:radioButtonItem description="Yes" value="yes" />
				<base:radioButtonItem description="No" value="no" selectedValue="no"/>
			</base:radioButtonContainer>
		</div>
	</div>
	
<br/>

<!-- Popup Display ----------------------------------------------------------------------------------------------------------------------------------- -->
<div id="cardSelectorOverlay"></div>
<div  id="selectorHolder"  class="">
	<!-- Title row -->
	<div id="selectorHolderBorderTop" class="borderBottomWhite">
		<h2 id="selectorHolderBorderTopText">${description}</h3>
	</div>
	
	<!-- Get the item count -->
	<c:forEach var="item" items="${offerviewbean.devices}" varStatus="loopCount">
		<c:set var="itemLoopCount" value="${loopCount.count}" />
	</c:forEach>

	<!-- Scroller Left div -->
	<div id="selectorTabletScrollerLeft" onclick="fnbDeviceSelectionPickerObject.scrollRight();"><div id="selectorScrollerLeftImage"></div></div>
	<!-- Scroller Right div -->
	<div id="selectorTabletScrollerRight" onclick="fnbDeviceSelectionPickerObject.scrollLeft(${itemLoopCount});"><div id="selectorScrollerRightImage"></div></div>
	
	<!-- Main body (cards) here -->
	<div id="selectorHolderBody" class="">
       <c:forEach var="item" items="${offerviewbean.devices}" varStatus="i">
        <div id="cardImageHolder"  onClick="fnbDeviceSelectionPickerObject.pickCard('${i.count-1}');" class="cardImageHolder${i.count-1}">
       		<div id="selectorProductDescription">${fn:trim(item.name)}</div>
			<img class="" id="img${i.count-1}" src="${fn:trim(item.image)}">
			<div id="selectorProductDescriptionBottom">Monthly Repayment: R ${item.monthlyRepayment} for 24 Months</div>
			<input type="hidden" id="deviceDescription${i.count-1}" name="" value="${item.description}" >
			<input type="hidden" id="deviceInsuranceValue${i.count-1}" name="" value="${item.insuranceValue}" >
		</div>
	</c:forEach>
	</div>
	
	<!-- Buttons row -->
	<div id="selectorHolderBorderBottom" class="borderTopWhite">
		<div id="selectorButtonHolder">
		<base:button label="Cancel" icon="bigCircLeft"  onclick="fnbDeviceSelectionPickerObject.hide();" id="selectorButtons" className=""/>
		<base:button label="Select" icon="bigCircLeft" onclick="fnbDeviceSelectionPickerObject.selectCard();" id="selectorButtons" className=""/>
		</div>
	</div>
</div>

