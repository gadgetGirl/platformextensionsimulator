<%@tag import="fnb.online.tags.beans.table.*"%>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="tableRow" required="false" rtexprvalue="true" type="fnb.online.tags.beans.table.TableRow"%>
<%@ attribute name="columnOptions" required="false" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="count" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="colCount" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="tableRowId" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="enableJSobject" required="false" rtexprvalue="true"%>
<%@ attribute name="isActionMenu" required="false" rtexprvalue="true"%>
<%@ attribute name="showHideColumns" required="false" rtexprvalue="true"%>
<%@ attribute name="eziItemClickLoad" required="false" rtexprvalue="true"%>
<%@ attribute name="newFrameWorkFlag" required="false" %>

<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<c:choose>
	<c:when test="${empty columnOptions}">
		<div class="tableCellItem">
			<jsp:doBody></jsp:doBody>
		</div>
	</c:when>
	<c:otherwise>
		<%
			String currentColumnTotal = columnOptions.getTotal();
			TableItem tableItem = tableRow.getItem(columnOptions.getFieldName());
			Class classItem = tableItem.getClass();
			
		%>
		<c:set var="itemValue" value="<%=tableItem.getCellValue()%>"></c:set>
		
		<c:set var="currentColumnTotal" value="<%=currentColumnTotal%>" />

		<c:set var="item" value="<%=tableItem%>"></c:set>
		
		<c:set var="sizeConfig" value="${columnOptions}"></c:set>
		
		<c:set var="onClick" value="${columnOptions.onClick}"></c:set>
		
		<c:set var="onKeyup" value="${columnOptions.onKeyup}"></c:set>
		
		<c:set var="onChange" value="${columnOptions.onChange}"></c:set>

		<c:if test="${browserInfo.capabilities['fnb_is_mobile']}">
		
			<c:if test='${itemValue!="&nbsp;"}'>
				<c:choose>
				<c:when test="${not empty columnOptions.shortHeading}">
				   <div class="hiddenLabel<c:if test='${item.displayStyle > 0}'> forcedHideInMobile</c:if>">${columnOptions.shortHeading}</div>
				</c:when>
				<c:otherwise>
				 	<div class="hiddenLabel<c:if test='${item.displayStyle > 0}'> forcedHideInMobile</c:if>">${columnOptions.heading}</div>
					</c:otherwise>
				</c:choose>
			</c:if>

		</c:if>

		<%//Do not use instance of as this will return true for the parent classes too%>
		<%if(classItem == TableTextItem.class){%>
			<base:tableTextItem tableItem="${item}" columnOptions="${sizeConfig}" tableDoubleItem="false"/>
		<%} else if(classItem == TableHyperLinkItem.class){%>
		 	<base:tableHyperLinkItem count="${count}" tableItem="${item}" columnOptions="${sizeConfig}" tableDoubleItem="false" isActionMenu="${isActionMenu}" eziItemClickLoad="${eziItemClickLoad}" />
	 	<%} else if(classItem == TableDoubleItem.class){%>
		 	<base:tableDoubleItem count="${count}" tableItem="${item}" tableRowId="${tableRowId}" columnOptions="${sizeConfig}" enableJSobject="${enableJSobject}"/> 
		<%} else if(classItem == TableEziButtonItem.class){%>
		 	<base:tableEziButtonItem tableItem="${item}" columnOptions="${sizeConfig}" newFrameWorkFlag="${newFrameWorkFlag}" />
		<%} else if(classItem == TableResultItem.class){%>
		 	<base:tableResultItem tableItem="${item}" columnOptions="${sizeConfig}" tableDoubleItem="false"/> 
		<%} else if(classItem == TableInputItem.class){%>
		 	<base:tableInputItem isTableObject="true" count="${count}" tableItem="${item}" columnOptions="${sizeConfig}" tableDoubleItem="false" onClick="${onClick}" onKeyup="${onKeyup}" onChange="${onChange}"/>
		<%} else if(classItem == TableDropdownItem.class){%>
		 	<base:tableDropdownItem count="${count}" tableItem="${item}" columnOptions="${sizeConfig}" tableDoubleItem="false" onChange="${onChange}"/>
		<%} else if(classItem == TableButtonItem.class){%>
		 	<base:tableButtonItem tableItem="${item}" columnOptions="${sizeConfig}" count="${count}" />
		<%} else if(classItem == TableDateBoxItem.class){%>
		 	<base:tableDateBoxItem tableItem="${item}" columnOptions="${sizeConfig}" tableDoubleItem="false"/>
		<%} else if(classItem == TableCheckBoxItem.class){%>
		 	<base:tableCheckBoxItem isTableObject="true" count="${count}" tableItem="${item}" columnOptions="${sizeConfig}" tableDoubleItem="false"/>
		<%} else if(classItem == TableRadioButtonItem.class){%>
		 	<base:tableRadioButtonItem isTableObject="true" count="${count}" tableItem="${item}" columnOptions="${sizeConfig}" row="${count}" tableDoubleItem="false" onclick="${onClick}"/>
		<% }else if(classItem == TableExpandLinkItem.class){%>
		 	<base:tableExpandLinkItem tableItem="${item}" tableRowId="${tableRowId}" columnOptions="${sizeConfig}" tableDoubleItem="false"/>
		<%} else if(classItem == TableMoreOptionsItem.class){%>
		 	<base:tableMoreOptionsLinkItem count="${count}" tableItem="${item}"/> 
		<%} else if(classItem == TableGraphicalItem.class){%>
		 	<base:tableGraphicalItem tableItem="${item}"/>
		<%} else if(classItem == TableSubRowItem.class){%> 
			<base:tableSubRowItem tableRowId="${tableRowId}" tableSubRow="${item}"/>
		<%} else if(classItem == TableAmountInputItem.class){%>
			<script type="text/javascript">
				enableTotal='${columnOptions.calcColumnTotal}';
				tableTotal='${currentColumnTotal}';
				if(tableTotal==""){
					tableTotal = 0
				}else{
					tableTotal = parseFloat(tableTotal);
				}
				if(enableTotal=='true'){
					var itemText = '${item.text}'
					currentPageTotal += parseFloat(itemText.replace(/ /g,''));
					currentPageTotalItems.push('${item.id}${count}');
				}
				rowVal = parseFloat('${item.text}');
			</script>
		 	<base:tableAmountInputItem isTableObject="true" count="${count}" tableItem="${item}" columnOptions="${sizeConfig}" tableDoubleItem="false" onClick="${onClick}" onKeyup="${onKeyup}" onChange="${onChange}"/>
		<%}%>
	</c:otherwise>
</c:choose>
