<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="name"%>
<%@ attribute required="false" name="className"%>

<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="buttonLabel"%>
<%@ attribute required="false" name="size"%>

<%@ attribute required="false" name="onclick"%>
<%@ attribute required="false" name="onkeyup"%>

<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="readonly"%>

<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="tooltipContent"%>

<%@ attribute required="false" name="showUgly"%>

<c:set var="showUgly" value="${not empty showUgly && showUgly ? true : false}" />
<c:set var="name" value="${not empty name ? name : id}" />
<c:set var="disabled" value="${ ( empty disabled || disabled == 'false' ) ? '' : true }" />
<c:set var="containerWidth" value="${empty label ? 'grid100' : 'grid60'}" />
<c:set var="dynamicBorder" value="${empty label ? 'formContainerBorder' : ''}" />
<c:set var="buttonLabel" value="${empty buttonLabel ? 'Browse' : buttonLabel}" />
<c:set var="size" value="${empty size ? 30 : size}" />

<c:if test="${not showUgly}">
	<style>
	.file-input-wrapper .input-container-inner{position:relative;}
	.file-input-wrapper .fake-input-wrapper{position:absolute;width:100%;z-index:1;}
	.file-input-wrapper #${id}-fake-input.file-input-fake{float:left;width:65%;}
	.file-input-wrapper #${id}-fake-button.file-button-fake{float:left;width:34%;margin-left:1%; font-size: 14px}
	.file-input-wrapper #${id}.input-file{position:relative;width:100%;z-index:2;opacity:0;-moz-opacity:0;filter:alpha(opacity: 0);}
	.file-input-wrapper.disabled .file-input-fake,
	.file-input-wrapper.disabled .file-button-fake{background-color:#DDD; border-color:#999; color:#666;}
	</style>

	<%--
	<script type="text/javascript">
	var fakeInput = document.getElementById('${id}-fake-input');
	document.getElementById('${id}').onchange = function(){
		realInput.value = this.value;
	}
	</script>
	--%>

	<script type="text/javascript">
	var elId='#'+'${id}';
	var fakeId='#'+'${id}'+'-fake-input';
	$(elId).change(function(){
		var fileValue=$(this).val();
		if(fileValue.match(/fakepath/)) {
            // update the file-path text using case-insensitive regex
            fileValue = fileValue.replace(/C:\\fakepath\\/i, '');
        }
		
		$(fakeId).val(fileValue);
	})
	
	$(elId).on('click', function(event) {$(event.currentTarget).trigger('click')});
	</script>
</c:if>

<div class="formElementWrapper input-wrapper clearfix file-input-wrapper ${className} ${dynamicBorder}<c:if test="${not empty disabled}"> disabled</c:if>">
	<c:if test="${not empty label}">
		<div class="gridCol grid40 formElementLabel">${label}</div>
	</c:if>
	<div class="gridCol ${containerWidth} formElementContainer">
		<div class="input-container-inner clearfix ${not empty tooltipContent ? 'toolTipSpacer' : ''}">

			<c:if test="${not showUgly}">
				<div class="fake-input-wrapper">
					<input id="${id}-fake-input" name="${name}-fake-input" class="input-input file-input-fake" type="text" <c:if test="${not empty disabled}">disabled="disabled"</c:if>/>
					<input type="button" id="${id}-fake-button" class="formButton file-button-fake" value="${buttonLabel}" <c:if test="${not empty disabled}">disabled="disabled"</c:if>/>
				</div>
			</c:if>

			<input id="${id}" name="${name}" class="input-input input-file" type="file" size="${size}"
				<c:if test="${not empty onclick}">onclick="${onclick}"</c:if>
				<c:if test="${not empty onkeyup}">onkeyup="${onkeyup}"</c:if>
				<c:if test="${not empty disabled}">disabled="disabled"</c:if>
				<c:if test="${not empty readonly}">readonly="readonly"</c:if> />

			<c:if test="${not empty tooltipContent}">
				<base:tooltip id="${id}Tooltip" content="${tooltipContent}" />
			</c:if>
			<c:if test="${not empty noteContent}">
				<div class="inputNote teal8">
					<div class="inputNoteArrow"></div><div class="inputNoteInner">${noteContent}</div>
				</div>
			</c:if>

		</div>
	</div>
</div>