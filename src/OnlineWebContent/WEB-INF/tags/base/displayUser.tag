<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="user" rtexprvalue="true" required="true" type="mammoth.services.User"%>
<%@ attribute name="commsBeans" rtexprvalue="true" required="true" type="mammoth.utility.HyphenSortedVector"%>

<%@ attribute name="message" rtexprvalue="true" required="true"%>
            
<h2 style="margin: 0;">Hi ${user.fullName}, ${message}</h2>
            