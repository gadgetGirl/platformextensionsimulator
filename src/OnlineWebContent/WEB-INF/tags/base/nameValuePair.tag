<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="true" rtexprvalue="true"  name="id" %>
<%@ attribute required="true" rtexprvalue="true"  name="label" %>
<%@ attribute required="true" rtexprvalue="true"  name="value" %>
<%@ attribute required="false" rtexprvalue="true" name="noteContent"%>
<div id="${id}_parent" class="formElementWrapper nameValuePair clearfix ${className}${disabledClass} <c:if test='${not empty tooltipContent}'> tooltipPad</c:if>">
	<div class="gridCol grid40 formElementLabel">${label}</div>
	<div class="gridCol grid60 formElementLabel nameValuePairVal">${value}
	<c:if test="${not empty noteContent}">
		<div class="inputNote teal8">
			<div class="inputNoteArrow"></div>
			<div class="inputNoteInner">${noteContent}</div>
		</div>
	</c:if>
	</div>
</div>