<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<c:if test="${!empty subTabs}">
<base:subTabsMenuHolder>
	<div id="subTabsPageHeader"></div>
	<script>
		firstSubtab = "";
		selected = "";
	</script>
	<c:forEach items="${subTabs}" var="tab">
		<script text="text/javascript">
			if (firstSubtab == "") {
				firstSubtab = "${tab.parentFunctionRef}" + "_"
						+ "${tab.childFunctionRef}";
			}
		</script>
		<c:if test="${tab.selected}">
			<base:subTabsMenu url="${tab.url}" showTab="" text="${tab.label}" isActiveTab="true" />
		</c:if>
		<c:if test="${not tab.selected}">
			<base:subTabsMenu url="${tab.url}" showTab="" text="${tab.label}" isActiveTab="false" />
		</c:if>
	</c:forEach>
</base:subTabsMenuHolder>
</c:if>