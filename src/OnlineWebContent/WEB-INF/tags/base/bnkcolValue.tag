<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="id" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="elementType" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="displayType" required="false" rtexprvalue="true" %>


<c:choose>
	
<c:when test="${displayType eq 'input'}">	
	<span<c:if test="${!empty className}"> class="${className}"</c:if>>
		<c:choose>
			<c:when test="${elementType eq 'text'}">
				<span class="double item">
				<span class="rinnerDouble">
					<span class="phoneLabel">Name</span>
						<span class="cellContent" id="${id}">
							<span class="larget">
							<jsp:doBody/>
							</span>
						</span>
					</span>
				</span>
			</c:when>
		<c:otherwise>
			<jsp:doBody/>
		</c:otherwise>
		</c:choose>
	</span>
</c:when>

<c:otherwise>
	<span  class="tableCell ${className}">
		<c:choose>
			<c:when test="${elementType eq 'text'}">
				<span class="double item">
				<span class="rinnerDouble">
					<span class="phoneLabel">Name</span>
						<span class="cellContent" id="${id}">
							<span class="larget">
							<jsp:doBody/>
							</span>
						</span>
					</span>
				</span>
			</c:when>
		<c:otherwise>
			<jsp:doBody/>
		</c:otherwise>
		</c:choose>
	</span>
</c:otherwise>

</c:choose>