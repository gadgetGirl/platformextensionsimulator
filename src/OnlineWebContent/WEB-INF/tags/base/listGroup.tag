<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="true"  rtexprvalue="true" name="type" description="ul, ol"%>
<%@ attribute required="false" rtexprvalue="true" name="style" description="none, decimal, disc, circle, square, lowerAlpha, upperAlpha"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>

<%--
// USAGE EXAMPLE //
<base:listGroup type="ul" style="disc">
	<base:listItem>List Item 1</base:listItem>
	<base:listItem>List Item 2 with child items
</base:listGroup>
--%>
<c:choose>
	<c:when test="${type == 'ol'}">
		<ol class="bulletedList
			<c:if test="${style == 'decimal'}">listDecimal</c:if>
			<c:if test="${style == 'lowerAlpha'}">listLowerAlpha</c:if>
			<c:if test="${not empty className}">${className}</c:if>">
			<jsp:doBody />
		</ol>
	</c:when>
	<c:when test="${type == 'ul'}">
		<ul class="bulletedList
			<c:if test="${style == 'decimal'}">listDecimal</c:if>
			<c:if test="${style == 'disc'}">listDisc</c:if>
			<c:if test="${style == 'circle'}">listCircle</c:if>
			<c:if test="${style == 'square'}">listSquare</c:if>
			<c:if test="${style == 'lowerAlpha'}">listLowerAlpha</c:if>
			<c:if test="${style == 'upperAlpha'}">listUpperAlpha</c:if>
			<c:if test="${style == 'none'}">listNone</c:if>
			<c:if test="${not empty className}">${className}</c:if>">
			<jsp:doBody />
		</ul>
	</c:when>
	<c:otherwise>
		<ul class="bulletedList <c:if test="${not empty className}">${className}</c:if>"><jsp:doBody /></ul>
	</c:otherwise>
</c:choose>