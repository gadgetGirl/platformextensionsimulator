<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
    
<%@ attribute name="id" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="tref" required="false" rtexprvalue="true" type="java.lang.String"%>

<div id="${id}" class="<c:if test="${!empty className}">${className} </c:if>visibilityHidden">
	<div id="forMore" class="forMore visibilityHidden">Scroll for more</div>
	<div class="left-sidebar-bottom"></div>
		<div id="fnb-logo" class="fnb-logo"></div>
		<div id="footerContent" class="footerContent">
		<div class="footerBorder"></div>
	</div>
</div>
<div id="support-referenceWrapper" class="support-referenceWrapper"><div id="support-reference" class="support-reference">${tref}</div></div>
