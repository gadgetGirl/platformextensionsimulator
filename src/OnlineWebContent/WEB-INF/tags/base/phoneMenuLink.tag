<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="contentGroup" required="true" %>
<%@ attribute name="text" required="true" %>

<li onclick="_phoneMenu.showContent(this)" data-productname="${contentGroup}" class="<c:if test='${not empty className}'>${className}</c:if> clearfix"><div>${text}</div></li>

