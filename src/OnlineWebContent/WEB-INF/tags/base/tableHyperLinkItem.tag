<%@tag import="fnb.online.tags.beans.table.TableHyperLinkItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableHyperLinkItem"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="count" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="isActionMenu" required="false" rtexprvalue="true"%>
<%@ attribute name="eziItemClickLoad" required="false" rtexprvalue="true"%>

<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<c:choose>

	<c:when test="${tableItem.name == 'more'}">
		<div class="rowMoreButton"  onclick="fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${tableItem.url}'); return false;">
			<div class="arrow" ></div>
		</div>
	</c:when>
	<c:when test="${tableDoubleItem == true}">
		<c:choose>
			<c:when test="${columnOptions.icon == 'download'}">
				<a onclick="fnb.controls.controller.eventsObject.raiseEvent('doDownload','${tableItem.url}');">${tableItem.text}</a>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${not empty columnOptions.onClick}">
						<a onclick="${columnOptions.onClick}; return false;">${tableItem.text}</a>
					</c:when>
					<c:otherwise>
						<c:set var="onclickFunction">
					       <c:choose>
					          <c:when test="${tableItem.target == '0'}">
					          	<c:choose>
									<c:when test="${isActionMenu=='true'}">
										fnb.controls.controller.eventsObject.raiseEvent('loadResultScreenFromActionMenu',{url:'${tableItem.url}'});
									</c:when>
									<c:otherwise>
										fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${tableItem.url}');
									</c:otherwise>
								</c:choose>
					          </c:when>
					          <c:when test="${tableItem.target == '1'}">fnb.utils.actionMenu.loadTargetToActionMenu('${tableItem.url}',this);</c:when>
					          <c:when test="${tableItem.target == '3'}">fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '${tableItem.url}'});</c:when>
					          <c:when test="${tableItem.target == '4'}">fnb.functions.loadUrlToPrintDiv.load('${tableItem.url}');</c:when>
					          <c:when test="${tableItem.target == '5'}">fnb.controls.controller.eventsObject.raiseEvent('doDownload','${tableItem.url}');</c:when>
					          <c:when test="${tableItem.target == '6'}">fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '${tableItem.url}');</c:when>
					          <c:when test="${tableItem.target == '7'}">fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '${tableItem.url}');</c:when>
					       </c:choose>
					    </c:set>
						<a onclick="${onclickFunction}; return false;">${tableItem.text}</a>
						<c:if test='${not empty tableItem.toolTipMessage}'><base:tooltip content="${tableItem.toolTipMessage}"/></c:if>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</c:when> 

	<c:when test="${tableDoubleItem == false}">
		<div id="${tableItem.id}_${count}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size} <c:if test='${tableItem.displayStyle > 0}'>warning${tableItem.displayStyle}</c:if><c:if test='${not empty tableItem.toolTipMessage}'>
		tooltipPad
		</c:if> ${tableItem.cssClass}" >
			<c:choose>
				<c:when test="${columnOptions.icon == 'download'}">
					<a onclick="fnb.controls.controller.eventsObject.raiseEvent('doDownload','${tableItem.url}');">${tableItem.text}</a>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${not empty columnOptions.onClick}">
							<a onclick="${columnOptions.onClick};">${tableItem.text}</a>
						</c:when>
					<c:otherwise>
						<c:set var="onclickFunction">
					       <c:choose>
					          <c:when test="${tableItem.target == '0'}">
								<c:choose>
									<c:when test="${isActionMenu=='true'}">
										fnb.controls.controller.eventsObject.raiseEvent('loadResultScreenFromActionMenu',{url:'${tableItem.url}'});
									</c:when>
									<c:when test="${eziItemClickLoad=='true'}">
										fnb.controls.controller.eventsObject.raiseEvent('loadUrl',{target:_workspace,url:'${tableItem.url}'});
									</c:when>
									<c:otherwise>
										fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${tableItem.url}');
									</c:otherwise>
								</c:choose>
							  </c:when>
					          <c:when test="${tableItem.target == '1'}">fnb.utils.actionMenu.loadTargetToActionMenu('${tableItem.url}',this);</c:when>
					          <c:when test="${tableItem.target == '3'}">fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '${tableItem.url}'});</c:when>
					          <c:when test="${tableItem.target == '4'}">fnb.functions.loadUrlToPrintDiv.load('${tableItem.url}');</c:when>
					          <c:when test="${tableItem.target == '5'}">fnb.controls.controller.eventsObject.raiseEvent('doDownload','${tableItem.url}');</c:when>
					          <c:when test="${tableItem.target == '6'}">fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '${tableItem.url}');</c:when>
					          <c:when test="${tableItem.target == '7'}">fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '${tableItem.url}');</c:when>
					       </c:choose>
					    </c:set>
						<a onclick="${onclickFunction}; return false;">${tableItem.text}</a>
					</c:otherwise>
					</c:choose>
					</c:otherwise>
			</c:choose>
			<c:if test='${not empty tableItem.toolTipMessage}'><base:tooltip content="${tableItem.toolTipMessage}"/></c:if>
		</div>
	</c:when>

</c:choose>