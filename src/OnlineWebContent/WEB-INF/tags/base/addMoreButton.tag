<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="onclick" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="false" rtexprvalue="true"%>

<div class="addMore"<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty onclick}"> onclick="${onclick}"</c:if>>
	<div id="addMoreAddButton" class="addMoreAddButton">
		<a class="addMoreAddButtonLink"><c:if test="${not empty label}"> ${label}</c:if></a>
	</div>
</div>

