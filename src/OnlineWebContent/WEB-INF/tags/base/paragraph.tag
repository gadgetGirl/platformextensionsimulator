<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute required="false" rtexprvalue="true" name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="value"%>

<p<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty className}"> class="${className}"</c:if>>
${value}<jsp:doBody />
</p>