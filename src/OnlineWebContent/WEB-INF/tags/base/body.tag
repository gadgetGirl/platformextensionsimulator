<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="id" required="false" rtexprvalue="true" type="java.lang.String"%>

<body<c:if test="${!empty id}"> id="${id}"</c:if>>

	<jsp:doBody></jsp:doBody>
	
</body>