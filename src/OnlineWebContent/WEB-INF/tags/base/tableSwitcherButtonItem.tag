<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>

<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="description"%>
<%@ attribute required="false" rtexprvalue="true" name="radioItemViewBean"%>
<%@ attribute required="false" rtexprvalue="true" name="value"%>
<%@ attribute required="false" rtexprvalue="true" name="selectedValue"%>
<%@ attribute required="false" rtexprvalue="true" name="selected"%>
<%@ attribute required="false" rtexprvalue="true" name="onchange"%>
<%@ attribute required="false" rtexprvalue="true" name="onclick"%>
<%@ attribute required="false" rtexprvalue="true" name="mobi"%>
<%@ attribute required="false" rtexprvalue="true" name="disabled"%>
<%@ attribute required="false" rtexprvalue="true" name="url"%>

<div id="${id}" <c:if test='${not empty onclick}'>data-postcallback="${onclick}"</c:if> <c:if test='${not empty url}'> data-url="${url}"</c:if> class="tableSwitcherButton <c:if test="${selectedValue == value || selected == 'true'}">tableSwitcherSelected</c:if> ${className}"
>${description}</div>
