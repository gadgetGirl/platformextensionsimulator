<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="value" required="true"%>
<div class="tableHeaderRow  tableGroupHeader">
	<!-- the tableRowHeader class is required for selection purposes, but doesn't require an actual css definition as such. MG -->
	<div class="tableHeaderInnerRow">
		<!-- the last class, tableRowHeader, is required for selection purposes, but doesn't require an actual css definition as such. MG -->
		<span class="tableHinnerImage"></span>${value}
		<!-- the key is the header value -->
		<jsp:doBody></jsp:doBody>
	</div>
</div>