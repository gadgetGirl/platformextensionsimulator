<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<base:import type="css" link="/banking/01css_new/pages/loggedin/settings/cuac/UserMaintenanceLanding.css" />

<base:note id="roleDefButtonBusinesses" type="top">
			<base:button id="viewRoleDefinitionsBusinesses" icon="" className="tableAddButton printDisplayNone" label="View the role definitions here" onclick="fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:'#roleDefinitionsBusinesses'});" />
		</base:note>
		<base:divContainer id="roleDefinitionsBusinesses" className="displayNone">
			<base:simpleTableContainer>
				<base:simpleTableRow id="tableHeadings" type="groupHeading">
					<base:gridCol width="70">
						Use the matrix to help you select the appropriate role
					</base:gridCol>
					<base:gridCol width="30">
						<base:button label="Close" className="closeDefinitions" onclick="fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#roleDefinitionsBusinesses'});"></base:button>
					</base:gridCol>
				</base:simpleTableRow>
				
				<base:simpleTableRow type="tableHeading">
					<base:simpleTableCell type="headingCell" colNo="1">
						Functions you can perform
					</base:simpleTableCell>
					<base:simpleTableCell type="headingCell" colNo="2" className="alignCenter">
						<base:divContainer className="mobileHide">Business Transactor</base:divContainer>
						<base:divContainer className="mobileDisplay">
							<base:image src="/banking/03images/base/cuac/headingMobileTransactor.PNG"></base:image>
						</base:divContainer>
					</base:simpleTableCell>
					<base:simpleTableCell type="headingCell" colNo="3" className="alignCenter">
						<base:divContainer className="mobileHide">Business Viewer</base:divContainer>
						<base:divContainer className="mobileDisplay">
							<base:image src="/banking/03images/base/cuac/headingMobileViewer.PNG"></base:image>
						</base:divContainer>
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						View account via personal electronic banking services 
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Transact via personal electronic banking services
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Can appoint View permissions Online Banking [Enterprise] profile users 
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Can appoint Transact permissions Online Banking [Enterprise] profile users 
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
				</base:simpleTableRow>	

			</base:simpleTableContainer>
			<base:divContainer className="mobileHide">
				<base:equalHeightsRow className="marginBottom5">
					<base:equalHeightsColumn width="30" className=" white">
						<base:hyperlink className="detailedDefinitionLink printDisplayNone" href="https://www.fnb.co.za/downloads/online-banking-documentation/B2B_Roles_Matrix.pdf">View Detailed Role Definition</base:hyperlink>
					</base:equalHeightsColumn>
				</base:equalHeightsRow>
			</base:divContainer>
		</base:divContainer>