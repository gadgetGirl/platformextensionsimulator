<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:useBean id="ACTION_BAR_BEAN" class="fnb.online.tags.beans.actionbar.ActionBarBean" scope="request" />

<%@ attribute name="id" required="true" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>

<%@ taglib prefix="fnb.banking.frame" tagdir="/WEB-INF/tags/banking/frame"%>

<c:choose>
<c:when test="${ACTION_BAR_BEAN.hideActionBar}">
</c:when>
<c:otherwise>
	<c:if test="${not empty ACTION_BAR_BEAN}">
		<c:choose>
		<c:when test="${empty ACTION_BAR_BEAN.options && empty ACTION_BAR_BEAN.moreOptionsColumnOne && empty ACTION_BAR_BEAN.moreOptionsColumnOne && empty ACTION_BAR_BEAN.moreOptionsColumnTwo && empty ACTION_BAR_BEAN.moreOptionsColumnThree}">
		</c:when>
		<c:otherwise>
			<%-- <div id="actionMenuButtonWrapper" class="actionMenuButtonWrapper notExpanded">
			<div class="actionMenuButtonWrapperInner"> 
			<div id="actionMenuTextWrapper" class="actionMenuTextWrapper amber1"><div id="actionMenuIcon" class="actionMenuIcon"></div><div id="moreoptionsActionMenuTextSwap" class="moreoptionsActionMenuTextSwap"><p id="actionMenuText" class="actionMenuText">${ACTION_BAR_BEAN.moreOptionHeading}</p></div></div></div>
			</div>
			<div id="actionWrap"> --%>
			<div id="actionMenu" class="index30 Hhide">
			

		<div id="actionMenuGrid">
			<div id="actionMenuContent" class="actionMenuGridInner">
			<base:actionMenuGroup>
				<c:set var="defaultClassName" value="${fn:toLowerCase(fn:replace(ACTION_BAR_BEAN.moreOptionHeading,' ', ''))}"/>

				<base:actionMenuColumn id="actionMenuOrangeBanner" columnMainHeading="${ACTION_BAR_BEAN.optionHeading}" columnMainHeadingClass="orange" columnBodyClassName="${defaultClassName}BannerOne" columnIndex="0" columnWidth="1" columnItems="${ACTION_BAR_BEAN.options}">
					
				</base:actionMenuColumn>
				<c:set var="className" value="actionMenuColUp"/>
				<c:set var="isSingle" value="false"/>
				<c:set var="isDouble" value="false"/>
				<c:if test="${empty ACTION_BAR_BEAN.moreOptionsColumnTwo}"><c:set var="isSingle" value="true"/></c:if>
				<c:if test="${empty ACTION_BAR_BEAN.moreOptionsColumnThree}"><c:set var="isDouble" value="true"/></c:if>
				<c:if test="${empty ACTION_BAR_BEAN.moreOptionsColumnTwo&&!empty ACTION_BAR_BEAN.moreOptionsColumnThree}"><c:set var="isDouble" value="true"/></c:if>
				<c:if test="${not empty ACTION_BAR_BEAN.displayMoreOptions}">
					<c:if test="${not empty ACTION_BAR_BEAN.moreOptionsColumnOne}">
						<base:actionMenuColumn isSingle="${isSingle}" isDouble="${isDouble}" id="moreoptionsBannerTwo" columnClassName="${className}" columnBodyClassName="${defaultClassName}BannerTwo actionMenuContents" columnIndex="1" columnWidth="1" columnItems="${ACTION_BAR_BEAN.moreOptionsColumnOne}">
						</base:actionMenuColumn>
					</c:if> 
					<c:if test="${not empty ACTION_BAR_BEAN.moreOptionsColumnTwo}">
						<base:actionMenuColumn isDouble="${isDouble}" id="moreoptionsBannerThree" columnClassName="${className}" columnBodyClassName="${defaultClassName}BannerThree actionMenuContents" columnIndex="2" columnWidth="1" columnItems="${ACTION_BAR_BEAN.moreOptionsColumnTwo}">
						</base:actionMenuColumn>
					</c:if> 
					<c:if test="${not empty ACTION_BAR_BEAN.moreOptionsColumnThree}">
						<base:actionMenuColumn isDouble="${isDouble}" id="moreoptionsBannerFour" columnClassName="${className}" columnBodyClassName="${defaultClassName}BannerFour actionMenuContents" columnIndex="3" columnWidth="1" columnItems="${ACTION_BAR_BEAN.moreOptionsColumnThree}">
						</base:actionMenuColumn>
					</c:if>
				</c:if>

			</base:actionMenuGroup>
			<base:divContainer id="moreOptionsDownArrow" className="moreOptionsDownArrow displayNone"/>
					<span id="actionMenuBottomLabel" class="actionMenuBottomLabel displayNone">${ACTION_BAR_BEAN.moreOptionHeading}</span>
			
			</div>
		</div>

</div>
			
			<!-- </div> -->
		</c:otherwise>
		</c:choose>
	</c:if>
</c:otherwise>
</c:choose>