<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="label" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="val" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="id" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="isEmpty" required="false" rtexprvalue="true" type="java.lang.String"%>

<li class="tabler action ">
		<span class="tabler2 yes">
		<span class="tableGroup ">
			<c:if test="${not empty label}">
				<span class="item  " id="">
					<span class="rinner ">
						<span class="phoneLabel"></span>
						<span class="cellContent">${label}</span>
					</span>
				</span>
			</c:if>

			<c:if test="${not empty val or isEmpty eq 'true' }">
				<span class="item  " id ="${id}">
				<span class="rinner ">
					<span class="phoneLabel" id ="${id}"></span>
					<span class="cellContent" id ="${id}">${val}</span>
				</span>
				</span>
			</c:if>
		</span></span>
</li>

