<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="groupCount" required="false" rtexprvalue="true"%>
<%@ attribute name="url" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="false" rtexprvalue="true"%>
<%@ attribute name="target" required="false" rtexprvalue="true"%>
<%@ attribute name="newFrameWorkFlag" required="false" %>

<c:choose>
	<c:when test="${newFrameWorkFlag == 'true'}">

		<%-- if event make key value for object--%>
		<c:set var="event">
			<c:choose>
				<c:when test="${target == '1'}"></c:when>
				<c:when test="${target == '2'}"></c:when>
				<c:when test="${target == '3'}">"event": "loadEzi"</c:when>
				<c:when test="${target == '4'}"></c:when>
				<c:when test="${target == '5'}"></c:when>
				<c:when test="${target == '6'}"></c:when>
				<c:when test="${target == '7'}">"event": "loadPopup"</c:when>
				<c:when test="${target == '8'}"></c:when>
			</c:choose>
		</c:set>
		
		<%-- if url make key value for object--%>
		<c:if test="${not empty url}">
			<c:set var="url" value='"url": "${url}",'/>
		</c:if>

		<div id="tabelRowButton_${id}" class="tabelRowButton">
			<span class="tabelRowButtonIcon"></span>
			<span class="tabelRowButtonLink" data-role="hyperlink" data-settings='[{${url}${event}}]' >${label}</span>
		</div>
		
	</c:when>
	
	<c:otherwise>
				
		<c:set var="onclickFunction">
			<c:choose>
				<c:when test="${target == '1'}"></c:when>
				<c:when test="${target == '2'}"></c:when>
				<c:when test="${target == '3'}">fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '${url}'});</c:when>
				<c:when test="${target == '4'}">fnb.functions.loadUrlToPrintDiv.load('${url}');</c:when>
				<c:when test="${target == '5'}">fnb.controls.controller.eventsObject.raiseEvent('doDownload','${url}');</c:when>
				<c:when test="${target == '6'}">fnb.controls.controller.eventsObject.raiseEvent('actionMenuPopupLoadUrl','${url}');</c:when>
				<c:when test="${target == '7'}">fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl','${url}');</c:when>
				<c:when test="${target == '8'}">fnb.controls.controller.eventsObject.raiseEvent('openWindow','${url}');</c:when>
			</c:choose>
		</c:set>
	
		<div id="tabelRowButton_${id}"  class="tabelRowButton"<c:if test="${not empty onclickFunction}"> onclick="${onclickFunction}"</c:if>>
			<span class="tabelRowButtonIcon"></span>
			<span class="tabelRowButtonLink">${label}</span>
		</div>
	</c:otherwise>
		
</c:choose>
			            	

