<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="heading1" required="false" rtexprvalue="true"%>
<%@ attribute name="entry1" required="false" rtexprvalue="true"%>
<%@ attribute name="doubleEntry1" required="false" rtexprvalue="true"%>

<%@ attribute name="heading2" required="false" rtexprvalue="true"%>
<%@ attribute name="entry2" required="false" rtexprvalue="true"%>
<%@ attribute name="doubleEntry2" required="false" rtexprvalue="true"%>

<%@ attribute name="heading3" required="false" rtexprvalue="true"%>
<%@ attribute name="entry3" required="false" rtexprvalue="true"%>
<%@ attribute name="doubleEntry3" required="false" rtexprvalue="true"%>

<%@ attribute name="heading4" required="false" rtexprvalue="true"%>
<%@ attribute name="entry4" required="false" rtexprvalue="true"%>
<%@ attribute name="doubleEntry4" required="false" rtexprvalue="true"%>

<%@ attribute name="recipCount" required="false" rtexprvalue="true"%>
<%@ attribute name="clearNow" required="false" rtexprvalue="true"%>
<%@ attribute name="notification" required="false" rtexprvalue="true"%>
<%@ attribute name="notificationContainer" required="false" rtexprvalue="true"%>

<%@ attribute name="finishPage" required="false" rtexprvalue="true"%>

<c:if test="${not empty recipCount}">
	<base:equalHeightsRow className="confirmRowOne" width="60">
		<base:equalHeightsColumn width="25">
			<base:confirmRecipientCount count="${recipCount}" payAndClearNow="${clearNow}" />
		</base:equalHeightsColumn>
		<base:equalHeightsColumn width="75">
			&nbsp;
		</base:equalHeightsColumn>
	</base:equalHeightsRow>
</c:if>

<c:choose>
	<c:when test="${not empty recipCount}">
		<base:equalHeightsRow className="confirmRowTwo floatLeft" width="60">
			<base:equalHeightsColumn width="25">
					${heading1}
			</base:equalHeightsColumn>
			<base:equalHeightsColumn width="25">
					${heading2}
			</base:equalHeightsColumn>
			<base:equalHeightsColumn width="25">
					${heading3}
			</base:equalHeightsColumn>
			<base:equalHeightsColumn width="25">
					${heading4}
			</base:equalHeightsColumn>
		</base:equalHeightsRow>
	</c:when>
	<c:otherwise>
		<base:equalHeightsRow className="confirmRowOne floatLeft" width="60">
			<base:equalHeightsColumn width="25">
				${heading1}
			</base:equalHeightsColumn>
			<base:equalHeightsColumn width="25">
					${heading2}
			</base:equalHeightsColumn>
			<base:equalHeightsColumn width="25">
					${heading3}
			</base:equalHeightsColumn>
			<base:equalHeightsColumn width="25">
					${heading4}
			</base:equalHeightsColumn>
		</base:equalHeightsRow>
	</c:otherwise>
</c:choose>

<base:equalHeightsRow className="confirmRowTwo floatLeft" width="60">
	<base:equalHeightsColumn width="25" className="detailsColumn">
		${entry1}
		<c:if test="${not empty doubleEntry1}"><br />${doubleEntry1}</c:if>
	</base:equalHeightsColumn>
	<base:equalHeightsColumn width="25"  className="detailsColumn">
		${entry2}
		<c:if test="${not empty doubleEntry2}"><br />${doubleEntry2}</c:if>
	</base:equalHeightsColumn>
	<base:equalHeightsColumn width="25" className="detailsColumn">
		${entry3}
		<c:if test="${not empty doubleEntry3}"><br />${doubleEntry3}</c:if>
	</base:equalHeightsColumn>
	<base:equalHeightsColumn width="25" className="detailsColumn">
		${entry4}
		<c:if test="${not empty doubleEntry4}"><br />${doubleEntry4}</c:if>
		<c:if test="${not empty notification}">
			<c:choose>
				<c:when test="${notification == true}">
					<div class="orange" onclick="showNotifications('${notificationContainer}')">Yes</div>
				</c:when>
				<c:otherwise>
					No
				</c:otherwise>
			</c:choose>
		</c:if>
	</base:equalHeightsColumn>
	
</base:equalHeightsRow>

<base:equalHeightsRow className="floatLeft" width="40">
	<base:equalHeightsColumn width="50" className="">
		<c:if test="${finishPage == true}">
			<base:resultIndicator />
		</c:if>
	</base:equalHeightsColumn>
	<base:equalHeightsColumn width="50">
		
	</base:equalHeightsColumn>
</base:equalHeightsRow>
