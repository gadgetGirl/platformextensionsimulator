<%@tag import="mammoth.cache.CacheImplFactory"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="true" name="type" description="TYPES: cssBase, jsBase, js, css"%>
<%@ attribute required="true" name="link" %>
<c:set var="cssBaseVersion" value="1.50"></c:set>
<c:set var="jsBaseVersion" value="1.50"></c:set>
<c:set var="cssVersion" value="1.50"></c:set>
<c:set var="jsVersion" value="1.50"></c:set>

<%!
static long newestFileTimeStamp = -1;
static long lastTimeChecked = -1;
static long checkInterval = 1000*60;

%>

<%


if(System.currentTimeMillis()-lastTimeChecked>checkInterval){
    lastTimeChecked = System.currentTimeMillis();
    try{
		Long ts = (Long) CacheImplFactory.getInstance().get("/imports/js","newestFileTimeStamp");
		if(ts==null){
		    ts = -1L;
		}
	newestFileTimeStamp = ts;
    }catch(Exception e1){
        System.err.println("import.tag: Error while finding newest timestamp from cache ");
        e1.printStackTrace();
    }
}

if(newestFileTimeStamp==-1){
    
    try{
        java.nio.file.Files.walkFileTree(java.nio.file.Paths.get(mammoth.services.Controller.getDataPath()+"/../../02javascript"),
                new java.nio.file.SimpleFileVisitor<java.nio.file.Path>() {
                    @Override
                    public java.nio.file.FileVisitResult visitFile(java.nio.file.Path file,
                            java.nio.file.attribute.BasicFileAttributes attrs) {
                        newestFileTimeStamp = Math.max(newestFileTimeStamp, attrs.lastModifiedTime().toMillis());
                        return java.nio.file.FileVisitResult.CONTINUE;
                    }    
                 }   
        );
        CacheImplFactory.getInstance().put("/imports/js","newestFileTimeStamp",new Long(newestFileTimeStamp),false);
    }catch(Exception e){
        System.err.println("import.tag: Error while finding newest timestamp ");
        e.printStackTrace();
    }
}

%>

<c:set var="fileVersions" value="<%=newestFileTimeStamp%>"></c:set>

<c:choose>
<c:when test="${type == 'cssBase'}"><link type="text/css" rel="stylesheet" href="${link}?v=${fileVersions}"/></c:when>
<c:when test="${type == 'jsBase'}"> <script type="text/javascript" src="${link}?v=${fileVersions}"></script></c:when>
<c:when test="${type == 'css'}">    <link type="text/css" rel="stylesheet" href="${link}?v=${fileVersions}"/></c:when>
<c:when test="${type == 'js'}">     <script type="text/javascript" src="${link}?v=${fileVersions}"></script></c:when>
<c:when test="${type == 'print'}">
    <link rel="stylesheet" type="text/css" media="print" href="${link}?v=${fileVersions}"/>
</c:when>
</c:choose>