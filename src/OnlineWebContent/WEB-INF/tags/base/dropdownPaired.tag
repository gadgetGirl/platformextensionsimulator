<%@ tag import="fnb.online.tags.beans.dropdown.*"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute required="true" rtexprvalue="true"  name="controller"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>

<%@ attribute required="true"  rtexprvalue="true" name="id1"%>
<%@ attribute required="false" rtexprvalue="true" name="name1"%>
<%@ attribute required="false" rtexprvalue="true" name="label1"%>
<%@ attribute required="false" rtexprvalue="true" name="value1"%>
<%@ attribute required="false" rtexprvalue="true" name="type1"%>
<%@ attribute required="false" rtexprvalue="true" name="onchange1"%>
<%@ attribute required="false" rtexprvalue="true" name="dropdown1" type="fnb.online.tags.beans.dropdown.DropDown"%>

<%@ attribute required="true"  rtexprvalue="true" name="id2"%>
<%@ attribute required="false" rtexprvalue="true" name="name2"%>
<%@ attribute required="false" rtexprvalue="true" name="label2"%>
<%@ attribute required="false" rtexprvalue="true" name="value2"%>
<%@ attribute required="false" rtexprvalue="true" name="type2"%>
<%@ attribute required="false" rtexprvalue="true" name="onchange2"%>
<%@ attribute required="false" rtexprvalue="true" name="dropdown2" type="fnb.online.tags.beans.dropdown.DropDown"%>

<%@ attribute required="false"  rtexprvalue="true" name="oneLine"%>

<c:choose>
	<c:when test="${oneLine == 'false'}">
		<base:divContainer className="dropDownPairedLeft">
			<base:dropDown type="${type1}" className="${className}" dropdown="${dropdown1}" id="${id1}" name="${name1}" label="${label1}" value="${value1}" onchange="fnb.functions.reloadDropdown.reload(this,'#${id2}_dropdownPaired_dropId','${controller}','${name1}'); ${onchange1}"/>
		</base:divContainer>
		<base:divContainer className="dropDownPairedRight">
			<base:dropDown type="${type2}" className="${className}" dropdown ="${dropdown2}" id="${id2}_dropdownPaired" name="${name2}" label="${label2}" value="${value2}" onchange="${onchange2}" />
		</base:divContainer>
	</c:when>
	<c:otherwise>
		<base:equalHeightsColumn width="50" className="white borderRightGrey dropDownPairedLeft">
			<base:dropDown type="${type1}" className="${className}" dropdown="${dropdown1}" id="${id1}" name="${name1}" label="${label1}" value="${value1}" onchange="fnb.functions.reloadDropdown.reload(this,'#${id2}_dropdownPaired_dropId','${controller}','${name1}'); ${onchange1}"/>
		</base:equalHeightsColumn>
		<base:equalHeightsColumn width="50" className="white dropDownPairedRight">
			<base:dropDown type="${type2}" className="${className}" dropdown ="${dropdown2}" id="${id2}_dropdownPaired" name="${name2}" label="${label2}" value="${value2}" onchange="${onchange2}" />
		</base:equalHeightsColumn>
	</c:otherwise>
</c:choose>

