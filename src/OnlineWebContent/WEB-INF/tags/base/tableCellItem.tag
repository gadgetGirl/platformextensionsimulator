<%@tag import="fnb.online.tags.beans.table.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableItem"%>
<%@ attribute name="columnOptions" required="false" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>

<%//Do not use instance of as this will return true for the parent classes too%>
<%if(tableItem.getClass() == TableTextItem.class){%>
	<base:tableTextItem tableItem="${tableItem}" columnOptions="${columnOptions}"/>
<%} else if(tableItem.getClass() == TableHyperLinkItem.class){%>
 	<base:tableHyperLinkItem tableItem="${tableItem}" columnOptions="${columnOptions}"  />
<%} else if(tableItem.getClass() == TableInputItem.class){%>
 	<base:tableInputItem tableItem="${tableItem}" columnOptions="${columnOptions}"  />
<%} else if(tableItem.getClass() == TableDropdownItem.class){%>
 	<base:tableDropdownItem tableItem="${tableItem}" columnOptions="${columnOptions}"  />
<%} else if(tableItem.getClass() == TableDateBoxItem.class){%>
 	<base:tableDateBoxItem tableItem="${tableItem}" columnOptions="${columnOptions}"  />
<%} else if(tableItem.getClass() == TableCheckBoxItem.class){%>
 	<base:tableCheckBoxItem tableItem="${tableItem}" columnOptions="${columnOptions}"  />
<%} else if(tableItem.getClass() == TableRadioButtonItem.class){%>
 	<base:tableRadioButtonItem tableItem="${tableItem}" columnOptions="${columnOptions}"   />
<%}%>