<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<base:divContainer>
	<base:gridGroup className="borderTopWhite borderBottomWhite">
		<jsp:doBody />
	</base:gridGroup>
</base:divContainer>