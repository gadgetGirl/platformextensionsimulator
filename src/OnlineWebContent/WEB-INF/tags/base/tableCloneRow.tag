<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute required="true"  rtexprvalue="true" name="label"%>
<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="onclick"%>
<%@ attribute required="false" rtexprvalue="true" name="targetClass"%>
<%@ attribute required="false" rtexprvalue="true" name="targetParentClass"%>

<base:gridGroup className="clone-row-wrapper ${className}">
	<div class="clone-row-button"<c:if test="${not empty id}"> id="${id}"</c:if><c:if test="${not empty onclick}"> onclick="${onclick}"</c:if>>${label}</div>
</base:gridGroup>