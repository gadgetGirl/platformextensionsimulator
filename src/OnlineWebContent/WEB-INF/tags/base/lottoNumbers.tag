<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="numbersBean" required="false" rtexprvalue="true" type="shareinfo.utility.HyphenSortedVector"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="type" required="false" rtexprvalue="true"%>

<div id="lottoNumbersList_${id}">

	<script>
		fnb.functions.lotto.populateColoredNumbers('${type}','${id}','${numbersBean}');
	</script>

</div>