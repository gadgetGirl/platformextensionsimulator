<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="formViewBean" required="true" rtexprvalue="true" type="fnb.online.tags.beans.form.Form"%>

<%@ attribute name="formwidth" required="false" rtexprvalue="true"%>
<%@ attribute name="elemSize" required="false" rtexprvalue="true"%>

<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="name" required="true" rtexprvalue="true"%>
<%@ attribute name="action" required="false" rtexprvalue="true"%>
<%@ attribute name="method" required="false" rtexprvalue="true"%>
<%@ attribute name="onsubmit" required="false" rtexprvalue="true"%>

<div class="form">
	<div class="formInner">
		<form id="id_${id}" name="${name}"
			<c:if test="${not empty onsubmit}">onsubmit="${onsubmit}"</c:if>
			<c:if test="${not empty action}">action="${action}"</c:if>
			<c:choose>
				<c:when test="${not empty method}">method="${method}"</c:when>
				<c:otherwise>method="POST"</c:otherwise>
			</c:choose>
		>
			<div class="${elemSize}">
				<jsp:doBody></jsp:doBody>
			</div>
		</form>
	</div>
</div>