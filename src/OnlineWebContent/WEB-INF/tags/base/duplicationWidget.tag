<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute required="false"  rtexprvalue="true" name="lowerCodes" type="java.util.Vector"%>
<%@ attribute required="false"  rtexprvalue="true" name="nums" type="java.util.Vector"%>
<%@ attribute required="false"  rtexprvalue="true" name="bean" type="java.util.List"%>

<%@ attribute required="true"  rtexprvalue="true" name="context" description="recipient OR owner OR addAccountRelationship"%>
<%@ attribute required="true"  rtexprvalue="true" name="max"%>
<%@ attribute required="false"  rtexprvalue="true" name="noBranchCodeAndModel"%>
<%@ attribute required="false" rtexprvalue="true" name="removeButtonLabel" description="default: 'Remove'"%>
<%@ attribute required="false" rtexprvalue="true" name="addButtonLabel" description="default: 'Add Another'"%>
<%@ attribute required="false" rtexprvalue="true" name="accountTypesDropDown" type="fnb.online.tags.beans.dropdown.DropDown"%>
<%@ attribute required="false" rtexprvalue="true" name="fieldIds" description="Comma delimited. Default depends on context. Order is VERY IMPORTANT"%>

<base:import type="js" link="/banking/02javascript/pages/tags/base/duplication-widget.js"/>

<c:set var="removeButtonLabel" value="${ not empty removeButtonLabel ? removeButtonLabel : 'Remove' }" />
<c:set var="addButtonLabel" value="${ not empty addButtonLabel ? addButtonLabel : 'Add Another' }" />
<c:set var="noBranchCodeAndModel" value="${ not empty noBranchCodeAndModel ? noBranchCodeAndModel : false }" />

<%-- fieldNames attributes added so that B-E can change the IDs/Names of the input elements  --%>
<c:choose>
	<c:when test="${context == 'recipient'}">
		<c:if test="${empty fieldIds}">
			<c:set var="fieldIds">accountType,accountNumber,branchCode</c:set>
		</c:if>
	</c:when>
	<c:when test="${context == 'owner'}">
		<c:set var="fieldIds">accountNumber,branchCode,idRegNumber,initials,surnameBusName</c:set>
	</c:when>
	<c:when test="${context == 'addAccountRelationship'}">
		<c:set var="fieldIds">contactPerson,emailAddresses,cellPhoneNumber</c:set>
	</c:when>
</c:choose>
<c:set var="fieldIds" value="${fn:split(fieldIds, ',')}" />

<base:divContainer id="dwWrapper">

<c:choose>
	<c:when test="${context == 'recipient'}">

		<base:divContainer id="dwParent" className="dwParent">
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:dropdownSingleTier id="${fieldIds[0]}" name="${fieldIds[0]}" className="dwInput" margin="margin50" label="Account Type" dropdownBean="${accountTypesDropDown}" />
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[1]}" name="${fieldIds[1]}" className="dwInput" label="Account Number" type="number" />
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[2]}" name="${fieldIds[2]}" className="dwInput" label="Branch Code" type="number" />
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<span id="dwRemoveButton" class="dwRemoveButton">
						<span class="dwRemoveButtonLabel">${removeButtonLabel}</span>
					</span>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
		</base:divContainer>

	</c:when>
	<c:when test="${context == 'owner'}">
	
	    <c:choose>
	    <c:when test="${not empty bean}">
	    <div id="templateContainer">
	    <base:divContainer id="dwParent${count.index}" className="dwParent">
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[0]}" name="${fieldIds[0]}" className="dwInput" label="Account Number" value="${details.accountNumber}" type="number" maxlength="23"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[1]}" name="${fieldIds[1]}" className="dwInput branchCodeCheck" value="${details.branchCode}" label="Branch Code" type="number" maxlength="6"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[2]}" name="${fieldIds[2]}" className="dwInput" label="ID/Co. Reg. Number" type="text"  maxlength="23"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[3]}" name="${fieldIds[3]}" className="dwInput" label="Initial" type="text" maxlength="5"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[4]}" name="${fieldIds[4]}" className="dwInput" label="Surname/Business Name (optional)" type="text" labelMultiline="true" maxlength="30"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					&nbsp;
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">&nbsp;</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<span id="dwRemoveButton${count.index}" class="dwRemoveButton">
						<span class="dwRemoveButtonLabel">${removeButtonLabel}</span>
					</span>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
		</base:divContainer>
	    </div>
	    <c:forEach items="${bean}" var="details" varStatus="count">
		<base:divContainer id="dwParent${count.index}" className="dwParent">
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[0]}${count.index}" name="${fieldIds[0]}${count.index}" className="dwInput" label="Account Number" value="${details.accountNumber}" type="number" maxlength="23"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[1]}${count.index}" name="${fieldIds[1]}${count.index}" className="dwInput branchCodeCheck" value="${details.branchCode}" label="Branch Code" type="number" maxlength="6"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[2]}${count.index}" name="${fieldIds[2]}${count.index}" className="dwInput" label="ID/Co. Reg. Number" type="text"  maxlength="23"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[3]}${count.index}" name="${fieldIds[3]}${count.index}" className="dwInput" label="Initial" type="text" maxlength="5"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[4]}${count.index}" name="${fieldIds[4]}${count.index}" className="dwInput" label="Surname/Business Name" type="text" labelMultiline="true" maxlength="30"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					&nbsp;
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">&nbsp;</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<span id="dwRemoveButton${count.index}" class="dwRemoveButton">
						<span class="dwRemoveButtonLabel">${removeButtonLabel}</span>
					</span>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
		</base:divContainer>
		</c:forEach>
		</c:when>
		<c:otherwise>
			<base:divContainer id="dwParent" className="dwParent">
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[0]}" name="${fieldIds[0]}" className="dwInput" label="Account Number" type="number" maxlength="23"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[1]}" name="${fieldIds[1]}" className="dwInput branchCodeCheck" label="Branch Code" type="number" maxlength="6"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[2]}" name="${fieldIds[2]}" className="dwInput" label="ID/Co. Reg. Number" type="text"  maxlength="23"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[3]}" name="${fieldIds[3]}" className="dwInput" label="Initial" type="text" maxlength="5"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[4]}" name="${fieldIds[4]}" className="dwInput" label="Surname/Business Name (optional)" type="text" labelMultiline="true" maxlength="30"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					&nbsp;
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">&nbsp;</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<span id="dwRemoveButton" class="dwRemoveButton">
						<span class="dwRemoveButtonLabel">${removeButtonLabel}</span>
					</span>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
		</base:divContainer>
		</c:otherwise>
		</c:choose>

	</c:when>
	<c:when test="${context == 'addAccountRelationship'}">

		<base:divContainer id="dwParent" className="dwParent">
			
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[0]}" name="${fieldIds[0]}" className="dwInput" label="Contact Person"/>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white"/>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>

			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[1]}" name="${fieldIds[1]}" className="dwInput" label="Email Address/es" type="email" />
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white"/>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
			
			<base:equalHeightsRow className="dwRow">
				<base:equalHeightsColumn width="50" className="dwCell white">
					<base:input id="${fieldIds[2]}" name="${fieldIds[2]}" className="dwInput" label="Cellphone Number" type="number" />
				</base:equalHeightsColumn>
				<base:equalHeightsColumn width="50" className="dwCell white">
					<span id="dwRemoveButton" class="dwRemoveButton">
						<span class="dwRemoveButtonLabel">${removeButtonLabel}</span>
					</span>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
		</base:divContainer>

	</c:when>	
</c:choose>

</base:divContainer>

<base:equalHeightsRow id="dwAddButtonWrapper">
	<base:equalHeightsColumn width="100">
		<span id="dwAddButton">
			<span id="dwAddButtonLabel">${addButtonLabel}</span>
		</span>
	</base:equalHeightsColumn>
	<base:equalHeightsColumn ghostBlock="true" />
</base:equalHeightsRow>

<script type="text/javascript">
var noBranchCodeAndModel = ${noBranchCodeAndModel}
if(noBranchCodeAndModel){
	fnb.hyperion.pages.branchCodeUtils.init('${lowerCodes}','${nums}');
}
	$('#dwWrapper').duplicationWidget({
		wrapper			:	'#dwWrapper',
		templateParent	:	'dwParent',
		max				:	${max},
		addButton		:	'dwAddButton',
		removeButton	:	'dwRemoveButton',
		<c:choose>
			<c:when test="${context == 'recipient'}">
				inputIds	:	['${fieldIds[0]}','${fieldIds[1]}','${fieldIds[2]}']
			</c:when>
			<c:when test="${context == 'owner'}">
				inputIds	:	['${fieldIds[0]}','${fieldIds[1]}','${fieldIds[2]}','${fieldIds[3]}','${fieldIds[4]}']
			</c:when>
			<c:when test="${context == 'addAccountRelationship'}">
				inputIds	:	['${fieldIds[0]}','${fieldIds[1]}','${fieldIds[2]}']
			</c:when>
		</c:choose>
		<c:if test="${not empty bean}">
		,multiple : true
		</c:if>
	});
	
	
  

</script> 