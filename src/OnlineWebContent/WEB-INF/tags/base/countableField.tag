<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="value" required="true"%>
<%@ attribute name="label" required="true"%>
<%@ attribute name="labelWidth" required="false"%>
<%@ attribute name="valueWidth" required="false"%>
<%@ attribute name="id" required="false"%>
<%@ attribute name="className" required="false"%>
<%@ attribute name="innerClassName" required="false"%>
<%@ attribute name="swap" required="false"%>

<div <c:if test='${not empty id}'>id="${id}"</c:if> class="countableField gridGroup <c:if test='${not empty className}'>${className}</c:if> <c:if test='${swap == true}'>swap</c:if> clearfix">
	<div class="innerWrap clearfix <c:if test='${not empty innerClassName}'>${innerClassName}</c:if>">
		<span class="countableFieldValue gridCol <c:if test='${empty valueWidth}'>grid10</c:if><c:if test='${not empty valueWidth}'>grid${valueWidth}</c:if>">${value}</span><span
			class="countableFieldLabel gridCol <c:if test='${empty labelWidth}'>grid90</c:if><c:if test='${not empty labelWidth}'>grid${labelWidth}</c:if>"
		>${label}</span>
	</div>
</div>
