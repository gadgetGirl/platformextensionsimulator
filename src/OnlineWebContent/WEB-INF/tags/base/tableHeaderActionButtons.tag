<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="false" rtexprvalue="true" name="tableBean" type="fnb.online.tags.beans.table.TableBean"%>
<%@ attribute required="false" rtexprvalue="true" name="actionButtonsLabel"%>
<%@ attribute required="false" rtexprvalue="true" name="overrideDownload"%>
<%@ attribute required="false" rtexprvalue="true" name="tableId"%>
<%@ attribute required="false" rtexprvalue="true" name="disableSearch"%>
<%@ attribute required="false" rtexprvalue="true" name="target"%>
<%@ attribute required="false" name="newFrameWorkFlag" %>
<c:set var="obeTablet">${sessionScope.SessionData["HyphenUser"].obeTablet}</c:set>

	<div id="tableActionButtons" class="tableActionButtonContainer"> 
	<%--<c:if test="${not empty actionButtonsLabel}">
		 <div class="headerControlLabel">
			<div class="labelInner">${actionButtonsLabel}</div>
		</div>-
		</c:if>--%>
		<c:if test="${not empty tableBean.tableOptions.showSearchFilter}">
		<base:tableSearchBar tableId="${tableId}" tableBean="${tableBean}" target="${target}"/>
		</c:if>
		<c:forEach items="${tableBean.tableHeaderButtons}" var="buttonGroups">
			<div data-type="0" class="tableActionButton addButton">
				
				<div class="tableActionButtonLabel">${buttonGroups.key}</div>
				<div class="tableActionButtonContent">
				<div class="tableActionButtonContentClose"></div>
				<div class="tableActionButtonContentArrow"></div>
				
				<c:forEach items="${buttonGroups.value.buttons}" var="headerButton">
				
						<c:set var="onclickFunction">
						
							<c:choose>
							
								<c:when test="${headerButton.target == '0'}">fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${headerButton.url}');</c:when>
								
								<c:when test="${headerButton.target == '1'}">fnb.utils.actionMenu.loadTargetToActionMenu('${headerButton.url}',this);</c:when>
								
								<c:when test="${headerButton.target == '3'}">
										fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '${headerButton.url}',postLoadingCallBack: fnb.utils.eziSlider.checkLoadError()});
								</c:when>
								
								<c:when test="${headerButton.target == '4'}">fnb.functions.loadUrlToPrintDiv.load('${headerButton.url}');</c:when>
								
								<c:when test="${headerButton.target == '5'}">fnb.controls.controller.eventsObject.raiseEvent('doDownload','${headerButton.url}');</c:when>
								
								<c:when test="${headerButton.target == '6'}">fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '${headerButton.url}');</c:when>
							
							</c:choose>
							
						</c:set>
						
					<c:choose>
						<c:when test="${newFrameWorkFlag == 'true' && headerButton.target == '3'}">
							<%-- Hyperlink Execution Attributes --%>
							
							<c:set var="event" value='"event": "loadEzi"'/>
							
							<%-- Url Attribute --%>
							<c:set var="url" value="${headerButton.url}" />
							
							<%-- if url make key value for object--%>
							<c:if test="${not empty url}">
								<c:set var="url" value='"url": "${url}",'/>
							</c:if>						
								
							<a href="#" <c:if test="${not empty id}"> id="${id}" </c:if> data-role="hyperlink" data-settings='[{${url}${event}}]' >${headerButton.label}</a>
					
						</c:when>
						<c:otherwise>
								<a onclick="${onclickFunction} return false;">${headerButton.label}</a>
						</c:otherwise>
					</c:choose>
					
						
					
				</c:forEach>
				
				</div>
				
			</div>
		</c:forEach>
		<jsp:doBody/>
		<c:if test="${tableBean.tableOptions.showSearchFilter}">
			<c:if test="${disableSearch != true}">
				<div data-type="1" class="tableActionButton searchButton">
					<div class="tableActionButtonLabel">Search</div>
				</div>
			</c:if>
		</c:if>
	<c:choose>
		<c:when test="${not empty overrideDownload}">
			<c:set var="downloadLink" value="fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url:'${overrideDownload}',postLoadingCallBack: fnb.utils.eziSlider.checkLoadError()})" />
		</c:when>
		<c:otherwise>
			<c:set var="downloadLink" value="fnb.controls.controller.eventsObject.raiseEvent('doDownload','/banking/Controller?nav=navigator.DownloadBodyPage&download=true'); return false;" />
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${obeTablet != true}">
			<div data-type="2" class="tableActionButton downloadButton" onclick="${downloadLink}">
					<div class="tableActionButtonLabel">Download</div>
				</div>
				<div  data-type="3" class="tableActionButton printButton" onclick="window.print(); return false;">
					<div class="tableActionButtonLabel">Print</div>
				</div>
		</c:when>
	</c:choose>
	
	<div data-type="4" class="tableActionButton searchClose">
		<div class="tableActionButtonLabel">Close</div>
	</div>
		
	</div>
	<script>
	fnb.forms.tableUtils.countButtons();
	</script>

