<%@ taglib prefix="base" 		tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="id" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="heading" required="false" rtexprvalue="true" type="java.lang.String"%>

<div class="bigThree">
	<div class="bigThreeHeadingWrapper">
		<div class="bigThreeHeading">
			<c:choose>
				<c:when test="${not empty heading}">
					${heading}
				</c:when>
				<c:otherwise>
					${sessionScope.SessionData["HyphenUser"].firstNames}, FNB Recommends...
				</c:otherwise>
			</c:choose>
				
		</div>
	</div>
		<jsp:doBody></jsp:doBody>
</div>
