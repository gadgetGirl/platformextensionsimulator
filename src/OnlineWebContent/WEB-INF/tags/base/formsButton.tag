<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="text" required="true" rtexprvalue="true"%>
<%@ attribute name="link" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="true" rtexprvalue="true"%>
<%@ attribute name="type" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="target" required="false" rtexprvalue="true"%>
<%@ attribute name="count" required="false" rtexprvalue="true"%>
<%@ attribute name="onclick" required="false" rtexprvalue="true"%>

<%@ attribute name="buttonBean" required="false" rtexprvalue="true" type="fnb.online.tags.beans.button.ButtonBean"%>
<c:if test="${empty count}">
	<c:set var="count" value=""></c:set>
</c:if>
<c:if test="${not empty buttonBean}">
	<c:set var="link" value="${buttonBean.url}"></c:set>
	<c:set var="onclick" value="${buttonBean.onClick}"></c:set>
</c:if>
	
	<c:choose>
		<c:when test="${not empty link}">
			<c:choose>
				<c:when test="${target == 'pannel'}">
					<div id="formElement_${id}${count}" class="${className}">
						<a class="formButton" href="#" id="${id}${count}" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:'${link}', target:'#eziPannelContnet'}); return false;" >${text}</a>
					</div>
				</c:when>
				<c:when test="${target == 'print'}">
					<div id="formElement_${id}${count}" class="${className}" onclick="fnb.functions.loadUrlToPrintDiv.load('${link}'); return false;">
						<a class="formButton" href="#" id="${id}${count}">${text}</a>
					</div>
				</c:when>
				<c:when test="${target == 'download'}">
					<div id="formElement_${id}${count}" class="${className}" onclick="fnb.controls.controller.eventsObject.raiseEvent('doDownload','${link}'); return false;">
						<a class="formButton" href="#" id="${id}${count}">${text}</a>
					</div>
				</c:when>
				<c:when test="${target == 'tableDownload'}">
					<div id="formElement_${id}${count}" class="${className}" onclick="${onclick}">
						<a class="formButton" href="#" id="${id}${count}">${text}</a>
					</div>
				</c:when>
				<c:when test="${target == 'confirmFinish'}">
					<div id="formElement_${id}${count}" class="${className}">
						<div class="formButton hinner">${text}</div>
						<a class="formButton" href="#" id="${id}${count}" onclick="${onclick}">${text}</a>
					</div>
				</c:when>
				<c:otherwise>
					<div id="formElement_${id}${count}" class="${className}">
						<a class="formButton" href="#" id="${id}${count}" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${link}'); return false;" >${text}</a>
					</div>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:when test="${target == 'printWindow'}">
					<div id="formElement_${id}${count}" class="${className}" onclick="window.print(); return false;">
						<a class="formButton" href="#" id="${id}${count}">${text}</a>
					</div>
		</c:when>
		<c:when test="${target == 'downloadPage'}">
					<div id="formElement_${id}${count}" class="${className}" onclick="fnb.controls.controller.eventsObject.raiseEvent('doDownload','/banking/Controller?nav=navigator.DownloadBodyPage&download=true'); return false;">
						<a class="formButton" href="#" id="${id}${count}">${text}</a>
					</div>
		</c:when>
		
		<c:otherwise>
			<div id="formElement_${id}${count}" class="${className}">
				<input <c:if test="${ not empty onclick}">onclick="${onclick}"</c:if>
					<c:choose>
						<c:when test="${not empty type}">type="${type}"</c:when>
						<c:otherwise>type="button"</c:otherwise>
					</c:choose>
					class="formButton" id="${id}${count}" value="${text}" />
			</div>
		</c:otherwise>
	</c:choose>
