<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>

<base:footerButtonGroup className="${className}">

	<jsp:doBody />

</base:footerButtonGroup>