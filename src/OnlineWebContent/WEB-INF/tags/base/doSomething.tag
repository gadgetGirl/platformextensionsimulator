<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="text" required="true" rtexprvalue="true"%>
<%@ attribute name="href" required="false" rtexprvalue="true"%>
<%@ attribute name="onclick" required="false" rtexprvalue="true"%>
<%@ attribute name="type" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>

<div id="${id}" class="doSomething roundedSmall<c:if test="${not empty className}"> ${className}</c:if>">
	<c:choose>
		<c:when test="${not empty href}">
			<a href="#" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${href}'); return false;">
				<base:icon size="small" type="${type} transparent" />
				<span class="content">${text}</span>
			</a>
		</c:when>
		<c:otherwise>
			<div onclick="${onclick}">
				<base:icon size="small" type="${type} transparent" />
				<div class="doSomethingText">${text}</div>
			</div>
		</c:otherwise>
	</c:choose>
</div>