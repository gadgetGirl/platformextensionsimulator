<%@ tag import="fnb.online.tags.beans.dropdown.*"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute required="false" rtexprvalue="true" name="dropdown" type="fnb.online.tags.beans.dropdown.AbstractDropdown" %>

<%-- old attributes kept for backwards compatibility, with comments as to how they were implemented on the old code --%>

<%@ attribute required="false" rtexprvalue="true" name="predictive" %>	<%-- NOT IMPLEMENTED --%>
<%@ attribute required="false" rtexprvalue="true" name="helpText" %>	<%-- NOT IMPLEMENTED --%>
<%@ attribute required="false" rtexprvalue="true" name="errorMessage" %><%-- NOT IMPLEMENTED --%>
<%@ attribute required="false" rtexprvalue="true" name="readonly" %>	<%-- NOT IMPLEMENTED --%>
<%@ attribute required="false" rtexprvalue="true" name="labelPosition"%><%-- N/A IN 9+ --%>

<%-- old same as 9+ --%>

<%@ attribute required="true"  rtexprvalue="true" name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="name" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="value" %>		<%-- selectedValue --%>
<%@ attribute required="false" rtexprvalue="true" name="label" %>
<%@ attribute required="false" rtexprvalue="true" name="onchange" %>

<%-- new 9+ only --%>
<%--COLUMNS:										ATTRIBUTE NAME			 TYPE		DEFAULT	DESCRIPTION --%>
<%@ attribute required="false" rtexprvalue="true" name="type" %>		<%-- string		'singleTier', 'twoTier', 'threeTier' --%>
<%@ attribute required="false" rtexprvalue="true" name="overflow" %>	<%-- boolean	TRUE	 --%>
<%@ attribute required="false" rtexprvalue="true" name="labelWidth" %>	<%-- integer	40		multiples of 10 (10 >= labelWidth <= 90) --%>
<%@ attribute required="false" rtexprvalue="true" name="labelTop" %>	<%-- boolean	FALSE	if set to TRUE, labelWidth AND selectWidth are both automatically set to 100 --%>
<%@ attribute required="false" rtexprvalue="true" name="selectWidth" %>	<%-- integer	60		multiples of 10 (10 >= selectWidth <= 90) --%>
<%@ attribute required="false" rtexprvalue="true" name="color" %>		<%-- string		-		Options-> white, unique to singleTier --%>
<%@ attribute required="false" rtexprvalue="true" name="searchClass" %>	<%-- string		-		see available search classes for each tier below --%>
<%@ attribute required="false" rtexprvalue="true" name="margin" %>		<%-- string		-		used for adding margins/padding classes on the wrapping element --%>
<%@ attribute required="false" rtexprvalue="true" name="disabled" %>	<%-- boolean	FALSE	Enable drop-down again with JS _dropdown.enable('#dropdownToEnableId') --%>
<%@ attribute required="false" rtexprvalue="true" name="reverse" %>		<%-- boolean	TRUE	 --%>

<%@ attribute required="false" rtexprvalue="true" name="valueClickedFunction" %>

<%@ attribute required="false" rtexprvalue="true" name="tooltipContent" %>
<%@ attribute required="false" rtexprvalue="true" name="noteContent"%> <%-- only for single tier so far --%>
<%-- Disabled all empty dropdowns --%>
<c:if test="${empty dropdown.items}">
	<c:set var="disabled" value="true"/>
</c:if>
<%--

NB:	It's currently not possible to add additional classNames to dropdownItems and then try to search by that className (eg. grouping)
		Ask Don if you need that functionality.
--%>

<c:choose>
	<c:when test="${type == 'threeTier'}">
<%-- ______________________________________ THREE ________________________________________
Optional search classes:
	dropdown-h1
	dropdown-h2
	dropdown-item-cell-left dropdown-h3
	dropdown-item-cell-right dropdown-h3
	dropdown-item-cell-left dropdown-h4
	dropdown-item-cell-right dropdown-h4
--%>
		<base:dropdownThreeTier
			id="${id}"
			name="${name}"
			className="${className}"
			label="${label}"
			labelWidth="${labelWidth}"
			labelTop="${labelTop}"
			selectWidth="${selectWidth}"
			searchClass="${searchClass}"
			selectedValue="${value}"
			onchange="${onchange}"
			margin="${margin}"
			disabled="${disabled}"
			reverse="${reverse}"
			noteContent="${noteContent}"
			dropdownBean="${dropdown}" ><c:if test="${empty dropdown}"><jsp:doBody /></c:if>
		</base:dropdownThreeTier>
	</c:when>
	<c:when test="${type == 'twoTier'}">
<%-- ______________________________________ TWO ________________________________________
Optional search classes:
	dropdown-h4
	dropdown-h5
 --%>
		<base:dropdownTwoTier
			id="${id}"
			name="${name}"
			className="${className}"
			label="${label}"
			labelWidth="${labelWidth}"
			labelTop="${labelTop}"
			selectWidth="${selectWidth}"
			searchClass="${searchClass}"
			selectedValue="${value}"
			onchange="${onchange}"
			margin="${margin}"
			disabled="${disabled}"
			reverse="${reverse}"
			dropdownBean="${dropdown}" ><c:if test="${empty dropdown}"><jsp:doBody /></c:if>
		</base:dropdownTwoTier>
	</c:when>
	<c:otherwise><!-- DEAFAULT type == 'singleTier' -->
<%-- ______________________________________ SINGLE ________________________________________ 
Optional search class:
	dropdown-h4
--%>
		<base:dropdownSingleTier
			tooltipContent="${tooltipContent}"
			id="${id}"
			name="${name}"
			className="${className}"
			label="${label}"
			labelWidth="${labelWidth}"
			labelTop="${labelTop}"
			selectWidth="${selectWidth}"
			searchClass="${searchClass}"
			selectedValue="${value}"
			color="${color}"
			onchange="${onchange}"
			margin="${margin}"
			disabled="${disabled}"
			reverse="${reverse}"
			noteContent="${noteContent}"
			dropdownBean="${dropdown}"><c:if test="${empty dropdown}"><jsp:doBody /></c:if>
		</base:dropdownSingleTier>
	</c:otherwise>
</c:choose>