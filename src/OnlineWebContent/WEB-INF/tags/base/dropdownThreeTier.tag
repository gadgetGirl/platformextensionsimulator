<%@ tag import="fnb.online.tags.beans.dropdown.*"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="dropdownBean" required="false" rtexprvalue="true" type="fnb.online.tags.beans.dropdown.DropDown"%>

<%@ attribute required="true" rtexprvalue="true"  name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="name" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="overflow" %>
<%@ attribute required="false" rtexprvalue="true" name="label" %>
<%@ attribute required="false" rtexprvalue="true" name="labelWidth" %>
<%@ attribute required="false" rtexprvalue="true" name="labelTop" %>
<%@ attribute required="false" rtexprvalue="true" name="selectWidth" %>
<%@ attribute required="false" rtexprvalue="true" name="searchClass" %>
<%@ attribute required="false" rtexprvalue="true" name="selectedValue" %>
<%@ attribute required="false" rtexprvalue="true" name="onchange" %><%-- not yet fully tested, added for backwards compatibility --%>
<%@ attribute required="false" rtexprvalue="true" name="margin" %>

<%@ attribute required="false" rtexprvalue="true" name="disabled" %>
<%@ attribute required="false" rtexprvalue="true" name="reverse" %>

<%@ attribute required="false" rtexprvalue="true" name="height" %>

<%@ attribute required="false" rtexprvalue="true" name="noteContent"%>

<c:set var="searchClass" value="${(not empty searchClass) ? searchClass : 'dropdown-h1'}" />
<c:set var="name" value="${(not empty name) ? name : id}" />
<c:set var="selectedValue" value="${(not empty dropdownBean.selectedValue) ? dropdownBean.selectedValue : selectedValue}"/>
<c:set var="disabledClass" value="${disabled == 'true' ? ' dropdown-disabled' : ''}" />
<c:set var="disabledAttributes">
	<c:choose>
		<c:when test="${disabled == 'true'}">
			data-disabled-overflow="${overflow}" data-disabled-searchClass="${searchClass}" data-disabled-reverse="${reverse}" data-disabled-height="${height}"
		</c:when>
		<c:otherwise>
			onclick="fnb.forms.dropdown.expand($(this),'${overflow}','${searchClass}','${reverse}','${height}','true');"
		</c:otherwise>
	</c:choose>
</c:set>

<div id="${id}_parent" class="formElementWrapper dropdown-wrapper dropdown-three-wrapper ${disabledClass}"<c:if test="${not empty dropdownBean.accountsDropDown}"> data-value="isAccount"</c:if>>
	<input id="${id}" name="${name}" value="${selectedValue}" class="dropdown-hidden-input dropdown-keyboard-tab"/>
	<c:if test="${not empty label}"><div class="formElementLabel gridCol grid40 dropdownLabelWrapper">${label}</div></c:if>
	<div class="gridCol grid60 dropdown-select dropdownElementWrapper">
		<div id="${id}_dropId" class="dropdown-initiator ${className}" ${disabledAttributes}>
			<div class="dropdown-trigger threeTier-trigger"></div>
			<div class="dropdown-selection-white dropdown-h4 clearfix"></div>
		</div>
		<div class="dropdown-content-wrapper ">
			<c:if test="${not empty label}"><div class="mobiDropdownInnerLabelWrapper">${label}</div><div class="mobiDropdownClose"></div></c:if>
			<ul class="dropdown-content" data-labelwidth="${labelTop ? 20 : labelWidth}" data-selectwidth="${labelTop ? 100 : selectWidth}">
				<div class="dropdown-content-top">
					<div class="dropdown-input-wrapper">
						<div class="dropdown-input-icon-search"></div>
						<div class="dropdown-input-container dropdown-h4 grid100"><input class="dropdown-input" placeholder="Search..." value=""></div>
					</div>
				</div>
				<c:choose>
					<c:when test="${not empty dropdownBean.items}">
						<c:forEach items="${dropdownBean.items}" var="item" varStatus="i">
							<c:set var="passSelected" value="${((item.returnValue == selectedValue) || (item.selected == true)) ? true : false}"/>
							<c:if test="${not empty className}"><c:set var="classNameItem" value="${className}_item" /></c:if>
							<base:dropdownThreeTierItem
								id=""
								className=		"${classNameItem}"
								onclick=		"${onchange}"
								h1=				"${item.topHeading}"
								h2=				"${item.topSubHeading}"
								returnValue=	"${item.returnValue}"
								selected=		"${passSelected}" 
								controller=		"${item.balancesUrl}"/>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<jsp:doBody />
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
		<c:if test="${not empty noteContent}">
			<div class="inputNote teal8">
				<div class="inputNoteArrow"></div>
				<div class="inputNoteInner">${noteContent}</div>
			</div>
		</c:if>
	</div>
</div>
<script type="text/javascript">
	var idTemp = "${id}"
		fnb.forms.dropdown.init($('#'+idTemp+'_dropId'),'${selectedValue}',${dropdownBean.accountsDropDown});
</script>