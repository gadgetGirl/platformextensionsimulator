<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="false" rtexprvalue="true" name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="style" description="DEPRECATED" %>
<%@ attribute required="false" rtexprvalue="true" name="hsize" %>
<%@ attribute required="false" rtexprvalue="true" name="value" description="Don't set value if you are using extended functionality" %>
<%@ attribute required="false" rtexprvalue="true" name="color" description="DEPRECATED" %>
<%@ attribute required="false" name="extended" %>
<%@ attribute required="false" rtexprvalue="true" name="topHeader" %>

<%-- NOTE: 9+ assimilation not complete --%>
<c:set var="pageHasSubtabs" value="${not empty subTabs ? 'subTabHeadingFix' : ''}" />
<c:choose>
	<c:when test="${not empty hsize}">
		<c:set var="hsizeStart" value="<h${hsize}>"/>
		<c:set var="hsizeEnd" value="</h${hsize}>"/>
	</c:when>
	<c:when test="${empty hsize && empty extended}">
		<c:set var="hsizeStart" value="<h1>"/>
		<c:set var="hsizeEnd" value="</h1>"/>
	</c:when>
</c:choose>
<%-- 
<c:if test="${(not empty className) || (not empty color)}">
	<c:set var='className' value='class="${className} ${color}"' />
</c:if>
--%>
<c:set var='id' value='${not empty id ? id : "extendedPageHeader"}' />
<c:set var='bodyContent'><jsp:doBody /></c:set>

<div id="${id}" class="${not empty subTabs ? 'subTabHeadingFix' : 'noSubTabs'} ${not empty bodyContent ? 'withExtraContent' : ''} ${className}">
	<c:choose>
		<c:when test="${not empty value}">
			${hsizeStart}${value}${hsizeEnd}
			${bodyContent}
				<%-- don't set value if you using extended functionality --%>
		</c:when>
		<c:otherwise>
			<c:if test="${not empty topHeader}"><div id="topPageHeader">${topHeader}</div></c:if>
			${hsizeStart}${bodyContent}${hsizeEnd}
		</c:otherwise>
	</c:choose>
</div>
<script type="text/javascript">
fnb.forms.extendedPageHeading.init();
</script>