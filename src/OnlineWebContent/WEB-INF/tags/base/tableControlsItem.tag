<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>

<%@ attribute name="className" required="false" rtexprvalue="true" %>
<%@ attribute name="position" required="false" rtexprvalue="true" %>

<c:choose>
	<c:when test="${tableControlColumns == '4'}">
		<base:gridQuart custom="${className} tableControlsItem${position} ${position=='1'?'firstChild':''}">
			<div class="bottom">
				<jsp:doBody />
			</div>
		</base:gridQuart>
	</c:when>
	<c:when test="${tableControlColumns == '3'}">
		<base:gridThird color="${className} tableControlsItem${position} ${position=='1'?'firstChild':''}">
			<div class="bottom">
				<jsp:doBody />
			</div>
		</base:gridThird>
	</c:when>
	<c:when test="${tableControlColumns == '2'}">
		<base:gridHalf custom="${className} tableControlsItem${position} ${position=='1'?'firstChild':''}">
			<div class="bottom">
				<jsp:doBody />
			</div>
		</base:gridHalf>
	</c:when>
	<c:otherwise>
		<base:gridFull custom="${className} tableControlsItem${position} ${position=='1'?'firstChild':''}">
			<div class="bottom">
				<jsp:doBody />
			</div>
		</base:gridFull>
	</c:otherwise>
</c:choose>