<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute required="true"  rtexprvalue="true" name="content1" %>
<%@ attribute required="true"  rtexprvalue="true" name="content2" %>
<%@ attribute required="false" rtexprvalue="true" name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="onclick" %>
<%@ attribute required="false" rtexprvalue="true" name="returnValue" %>
<%@ attribute required="false" rtexprvalue="true" name="selected" %>

<c:set var="returnValue" value="${not empty returnValue ? returnValue : content1}" />
<c:set var="selectedClass" value="${selected ? ' selected-item' : ''}" />
<c:set var="singleLine" value="${not empty content2 ? '' : 'singleLine'}" />

<li <c:if test="${not empty id}">id="${id}"</c:if> class="dropdown-item ${className}${selectedClass}" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if> data-value="${returnValue}">
	<div class="dropdown-item-row-wrapper"><div class="dropdown-item-row dropdown-h4 ${singleLine}">${content1}</div></div>
	<c:if test="${not empty content2}"><div class="dropdown-item-row-wrapper"><div class="dropdown-item-row dropdown-h5">${content2}</div></div></c:if>
</li>