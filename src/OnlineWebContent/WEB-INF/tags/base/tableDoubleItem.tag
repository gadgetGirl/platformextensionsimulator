<%@tag import="fnb.online.tags.beans.table.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableDoubleItem"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="tableRowId" required="false" rtexprvalue="true"%>
<%@ attribute name="count" required="false" rtexprvalue="true"%>
<%@ attribute name="enableJSobject" required="false" rtexprvalue="true"%>
<% 
String styleTop = "";
String styleBottom = "";
Class topClassItem = tableItem.getTopItem().getClass(); 
Class bottomClassItem = tableItem.getBottomItem().getClass();

if(topClassItem == TableDropdownItem.class){
	styleTop = "overflowOff";
}

if(bottomClassItem == TableDropdownItem.class){
	styleBottom = "overflowOff";
} 

%>

<c:set var="topItem" value="<%=tableItem.getTopItem()%>"></c:set>	
<c:set var="bottomItem" value="<%=tableItem.getBottomItem()%>"></c:set>

	<div id="doubleItem_top_div_${topItem.id}${count}" name="doubleItem_top_div_${topItem.name}${count}" class="tableCellItem doubleItemTop stack ${columnOptions.topSize} ${styleTop}  <c:if test='${topItem.displayStyle > 0}'>warning${topItem.displayStyle}</c:if><c:if test='${not empty topItem.toolTipMessage}'> tooltipPad</c:if>">
		<%//Do not use instance of as this will return true for the parent classes too%>
		<%if(topClassItem == TableTextItem.class){%>
			<base:tableTextItem tableItem="${topItem}" columnOptions="${sizeConfig}" tableDoubleItem="true"/>
		<%} else if(topClassItem == TableHyperLinkItem.class){%>
		 	<base:tableHyperLinkItem tableItem="${topItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
		<%} else if(topClassItem == TableInputItem.class){%>
		 	<base:tableInputItem tableItem="${topItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
		<%} else if(topClassItem == TableDropdownItem.class){%>
		 	<base:tableDropdownItem tableItem="${topItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
		<%} else if(topClassItem == TableDateBoxItem.class){%>
		 	<base:tableDateBoxItem tableItem="${topItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
		<%} else if(topClassItem == TableCheckBoxItem.class){%>
		 	<base:tableCheckBoxItem tableItem="${topItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
		<%} else if(topClassItem == TableRadioButtonItem.class){%>
		 	<base:tableRadioButtonItem tableItem="${topItem}" columnOptions="${sizeConfig}" tableDoubleItem="true"  />
		<%} else if(topClassItem == TableExpandLinkItem.class){%>
	 		<base:tableExpandLinkItem tableItem="${topItem}" tableRowId="${tableRowId}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
		<%}%>
	</div>
	
	<c:if test="${bottomItem.text!='&nbsp;'}">
		<div id="doubleItem_bottom_div_${bottomItem.id}" name="doubleItem_bottom_div_${bottomItem.name}" class="tableCellItem doubleItemBottom stack ${columnOptions.bottomSize} ${styleBottom}  <c:if test='${tableItem.displayStyle > 0}'>warning${tableItem.displayStyle}</c:if><c:if test='${not empty topItem.toolTipMessage}'> tooltipPad</c:if>">
			<%//Do not use instance of as this will return true for the parent classes too%>
			<%if(bottomClassItem == TableTextItem.class){%>
				<base:tableTextItem tableItem="${bottomItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
			<%} else if(bottomClassItem == TableHyperLinkItem.class){%>
			 	<base:tableHyperLinkItem tableItem="${bottomItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
			<%} else if(bottomClassItem == TableInputItem.class){%>
			 	<base:tableInputItem tableItem="${bottomItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
			<%} else if(bottomClassItem == TableDropdownItem.class){%>
			 	<base:tableDropdownItem tableItem="${bottomItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
			<%} else if(bottomClassItem == TableDateBoxItem.class){%>
			 	<base:tableDateBoxItem tableItem="${bottomItem}" columnOptions="${sizeConfig}" tableDoubleItem="true"/>
			<%} else if(bottomClassItem == TableCheckBoxItem.class){%>
			 	<base:tableCheckBoxItem tableItem="${bottomItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
			<%} else if(bottomClassItem == TableRadioButtonItem.class){%>
			 	<base:tableRadioButtonItem tableItem="${bottomItem}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
			<%} else if(bottomClassItem == TableExpandLinkItem.class){%>
		 		<base:tableExpandLinkItem tableItem="${bottomItem}" tableRowId="${tableRowId}" columnOptions="${sizeConfig}" tableDoubleItem="true" />
			<%}%>
		</div>
	</c:if>