<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="value" required="true"%>
<%@ attribute name="label" required="true"%>
<div class="tableRow tableDataRow totalRow clearfix">
	<div class="tableRowInner clearfix">
		<div class="tableGroup groupColCell grid100 clearfix">
			<div class="tableRowGroupInner">
				<div class="tableCell grid50">
					<div class="tableCellItem">
						<div class="clearfix ">${label}</div>
					</div>
				</div>
				<div class="tableCell grid50">
					<div class="tableCellItem">
						<div class="totalCellInner clearfix totalBox">${value}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
