<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<li<c:if test="${not empty className}"> class="${className}"</c:if>><jsp:doBody /></li>