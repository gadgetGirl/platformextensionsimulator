<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="beanName" required="false" rtexprvalue="true" %>
<%@ attribute name="viewBean" required="false" rtexprvalue="true" type="fnb.online.mammoth.forex.simple.ForexPaymentReceivedCaptureViewBean" %>
<%@ attribute name="fieldIndex" required="true" rtexprvalue="true" %>
<%@ attribute name="inOut" required="true" rtexprvalue="true" %>
<%@ attribute name="single" required="false" rtexprvalue="true" %>
<%@ attribute name="showField" required="false" rtexprvalue="true" %>

<%@ attribute name="className" required="false" rtexprvalue="true" %>

<%--
The forex transaction reasons still needed major JS functions
Back-end Developers: Ask for Leon when you come across this tag/page
--%>

<%--
columns taken from BOP code table in DB:
--------------------------------------------------------------------------
FRCCN	FRMRN	FRVAT	FRTAX	FRLOANRFN	FRSARB	FREXPCNO	FRSTANDING
Y/N		Y/N		Y/N		Y/N		Y/N			Y/N		Y/N			Y/N
--------------------------------------------------------------------------

Label						input id prefix			Variable string value
_________________________________________________________________________
Movement Reference Number	transactionRef			FRMRN
SARB Authorisation Number	sarbAuthNum				FRSARB
Export Control Number		exportControlNum		FREXPCNO
Customs Client Number		customsClientNum		FRCCN
Loan Reference Number		loanRef					FRLOANRFN
Tax Number					taxNum					FRTAX
VAT Number					vatNum					FRVAT

Not in DB but variables needed in form (under Loan Reference):
Location Country			locationCountry
Original Currency			originalCurrency
Original Amount				originalAmount
--%>

<c:choose>
	<c:when test="${inOut == 'input'}">
		
<%--  //////////////////  START INPUT  //////////////////////// --%>

<base:divContainer id="multipleReasonWrapper${fieldIndex}" className="multipleReasonWrapping clearfix">
	
	<c:if test="${single != 'true'}">
		<base:gridFull id="mainReasonContainer${fieldIndex}" className="mainReasonDivContainer">
			<base:gridTwoThird><base:dropDown name="transactionReason${fieldIndex}" id="transactionReason${fieldIndex}" dropdown="${multipleTransactionDescriptions}" onchange="showMultipleExtraFields(this,${fieldIndex});" value="${viewBean.transactionReason[fieldIndex]}"/></base:gridTwoThird>
			<base:gridThird><base:input id="amount${fieldIndex}" name="amount${fieldIndex}" className="reasonAmount" value="${viewBean.bopAmount[fieldIndex]}" defaultValue="0.00" type="text"/></base:gridThird>
		</base:gridFull>
	</c:if>
	<base:gridFull id="reasonsContainer${fieldIndex}" className="reasonExtrasDivContainer">
		
		<div id="FRMRN_${fieldIndex}">
			<base:divContainer id="transactionRefDiv${fieldIndex}" className="reasonExtrasDiv">
				<base:input type="text" id="transactionRef${fieldIndex}" name="transactionRef${fieldIndex}" value="${viewBean.transactionRef[fieldIndex]}" label="Movement Reference Number" labelPosition="labelLeft" />
			</base:divContainer>
		</div>
		
		<div id="FRSARB_${fieldIndex}">
			<base:divContainer id="sarbAuthNumDiv${fieldIndex}" className="reasonExtrasDiv">
				<base:input type="text" id="sarbAuthNum${fieldIndex}" name="sarbAuthNum${fieldIndex}" value="${viewBean.sarbAuthNum[fieldIndex]}" label="SARB Authorisation Number" labelPosition="labelLeft" />
			</base:divContainer>
		</div>
		
		<div id="FREXPCNO_${fieldIndex}">
			<base:divContainer id="exportControlNumDiv${fieldIndex}" className="reasonExtrasDiv">
				<base:input type="text" id="exportControlNum${fieldIndex}" name="exportControlNum${fieldIndex}" value="${viewBean.exportControlNum[fieldIndex]}" label="Export Control Number" labelPosition="labelLeft" />
			</base:divContainer>		
		</div>
		
		<div id="FRCCN_${fieldIndex}">
			<base:divContainer id="customsClientNumDiv${fieldIndex}" className="reasonExtrasDiv">
				<base:input type="text" id="customsClientNum${fieldIndex}" name="customsClientNum${fieldIndex}" value="${viewBean.customsClientNum[fieldIndex]}" label="Customs Client Number" labelPosition="labelLeft" />
			</base:divContainer>
		</div>
		
		<div id="FRTAX_${fieldIndex}">
			<base:divContainer id="taxNumDiv${fieldIndex}" className="reasonExtrasDiv">
				<base:input type="text" id="taxNum${fieldIndex}" name="taxNum${fieldIndex}" value="${viewBean.taxNum[fieldIndex]}" label="TAX Number" labelPosition="labelLeft" />
			</base:divContainer>
		</div>
		
		<div id="FRVAT_${fieldIndex}">
			<base:divContainer id="vatNumDiv${fieldIndex}" className="reasonExtrasDiv">
				<base:input type="text" id="vatNum${fieldIndex}" name="vatNum${fieldIndex}" value="${viewBean.vatNum[fieldIndex]}" label="VAT Number" labelPosition="labelLeft" />
			</base:divContainer>
		</div>
		
		<div id="FRLOANRFN_${fieldIndex}">
			<base:divContainer id="loanRefDiv${fieldIndex}" className="reasonExtrasDiv">
				<base:gridHalf className="gridBorderRight">
					<base:dropDown name="locationCountry${fieldIndex}" id="locationCountry${fieldIndex}" dropdown="${countries}" label="Location Country" labelPosition="labelLeft" className="loanfield" />
					<base:input type="text" id="loanRef${fieldIndex}" name="loanRef${fieldIndex}" value="${viewBean.loanRef[fieldIndex]}" label="Loan Reference Number" labelPosition="labelLeft" className="loanfield" />
				</base:gridHalf>
				<base:gridHalf>
					<base:dropDown name="originalCurrency${fieldIndex}" id="originalCurrency${fieldIndex}" dropdown="${currenciesDropdown}" label="Original Currency" labelPosition="labelLeft" className="loanfield" />
					<base:input type="text" id="originalAmount${fieldIndex}" name="originalAmount${fieldIndex}" value="${viewBean.originalAmount[fieldIndex]}" label="Original Amount" labelPosition="labelLeft" className="loanfield" />
				</base:gridHalf>
			</base:divContainer>
		</div>	
	</base:gridFull>
	<div class="centerBorderDivider"><div class="centerBorderDividerDiv"></div></div>

</base:divContainer>

<%--  //////////////////  END INPUT  //////////////////////// --%>

	</c:when>
	<c:when test="${inOut == 'output'}">

<%--  //////////////////  START OUTPUT  //////////////////////// --%>

<base:divContainer id="${id}_${fieldIndex}" className="${className}">

	<base:definitionList className="style2"><base:definitionListItem description="${viewBean.transactionReason[fieldIndex]}" value="${viewBean.bopAmount[fieldIndex]}"/></base:definitionList>
	<%-- <base:gridFull> --%>
			
	<div id="FRMRN_${fieldIndex}">
		<base:definitionList className="style2"><base:definitionListItem description="Movement Reference Number" value="${viewBean.transactionRef[fieldIndex]}"/></base:definitionList>
	</div>
	
	<div id="FRSARB_${fieldIndex}">
		<base:definitionList className="style2"><base:definitionListItem description="SARB Authorisation Number" value="${viewBean.sarbAuthNum[fieldIndex]}"/></base:definitionList>
	</div>
	
	<div id="FREXPCNO_${fieldIndex}">
		<base:definitionList className="style2"><base:definitionListItem description="Export Control Number" value="${viewBean.exportControlNum[fieldIndex]}"/></base:definitionList>
	</div>
	
	<div id="FRCCN_${fieldIndex}">
		<base:definitionList className="style2"><base:definitionListItem description="Customs Client Number" value="${viewBean.customsClientNum[fieldIndex]}"/></base:definitionList>
	</div>
	
	<div id="FRTAX_${fieldIndex}">
		<base:definitionList className="style2"><base:definitionListItem description="TAX Number" value="${viewBean.taxNum[fieldIndex]}"/></base:definitionList>
	</div>
	
	<div id="FRVAT_${fieldIndex}">
		<base:definitionList className="style2"><base:definitionListItem description="VAT Number" value="${viewBean.vatNum[fieldIndex]}"/></base:definitionList>
	</div>
	
	<div id="FRLOANRFN_${fieldIndex}">
		<base:definitionList className="style2">
			<base:definitionListItem description="Location Country" value="${viewBean.locationCountry[fieldIndex]}"/>
			<base:definitionListItem description="Loan Reference Number" value="${viewBean.loanRef[fieldIndex]}"/>
			<base:definitionListItem description="Original Currency" value="${viewBean.originalCurrency[fieldIndex]}"/>
			<base:definitionListItem description="Original Amount" value="${viewBean.originalAmount[fieldIndex]}"/>
		</base:definitionList>
	</div>
			
	<%-- </base:gridFull> --%>

</base:divContainer>

<%--  //////////////////  END OUTPUT  //////////////////////// --%>

	</c:when>
	<c:when test="${inOut == 'outMultiConfirm'}">


<%--  //////////////////  START OUTPUT FOR MULTI CONFIRM  //////////////////////// --%>

<base:divContainer id="${id}" className="${className} clearfix">

	<base:gridHalf className="gridFirstChild" confirm="true">
		<base:definitionList className="style1"><base:definitionListItem description="Reason ${fieldIndex}" value="${viewBean.bopCode[fieldIndex]} - ${viewBean.bopCodeDescription[fieldIndex]}" value2="${viewBean.bopAmount[fieldIndex]}"/></base:definitionList>
		<input id="transactionReason_${fieldIndex}" class="tranReason" type="hidden" value="${viewBean.transactionReason[fieldIndex]}"/>
	</base:gridHalf>
	<base:gridHalf className="gridLastChild" confirm="true">
			<div id ="FRMRN_${fieldIndex}">
				<base:definitionList className="style2"><base:definitionListItem description="Movement Reference Number" value="${viewBean.transactionRef[fieldIndex]}"/></base:definitionList>
			</div>
			<div id="FRSARB_${fieldIndex}">
				<base:definitionList className="style2"><base:definitionListItem description="SARB Authorisation Number" value="${viewBean.sarbAuthNum[fieldIndex]}"/></base:definitionList>
			</div>
			<div id ="FREXPCNO_${fieldIndex}">
				<base:definitionList className="style2"><base:definitionListItem description="Export Control Number" value="${viewBean.exportControlNum[fieldIndex]}"/></base:definitionList>
			</div>
			<div id="FRCCN_${fieldIndex}">
				<base:definitionList className="style2"><base:definitionListItem description="Customs Client Number" value="${viewBean.customsClientNum[fieldIndex]}"/></base:definitionList>
			</div>
			<div id="FRTAX_${fieldIndex}">
				<base:definitionList className="style2"><base:definitionListItem description="TAX Number" value="${viewBean.taxNum[fieldIndex]}"/></base:definitionList>
			</div>
			<div id="FRVAT_${fieldIndex}">
				<base:definitionList className="style2"><base:definitionListItem description="VAT Number" value="${viewBean.vatNum[fieldIndex]}"/></base:definitionList>
			</div>
			<div id="FRLOANRFN_${fieldIndex}">
				<base:definitionList className="style2">
					<base:definitionListItem description="Location Country" value="${viewBean.locationCountry[fieldIndex]}"/>
					<base:definitionListItem description="Loan Reference Number" value="${viewBean.loanRef[fieldIndex]}"/>
					<base:definitionListItem description="Original Currency" value="${viewBean.originalCurrency[fieldIndex]}"/>
					<base:definitionListItem description="Original Amount" value="${viewBean.originalAmount[fieldIndex]}"/>
				</base:definitionList>
			</div>
	</base:gridHalf>

</base:divContainer>

<%--  //////////////////  END OUTPUT FOR MULTI CONFIRM  //////////////////////// --%>

	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>