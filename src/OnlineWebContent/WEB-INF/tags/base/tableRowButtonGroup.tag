<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="groupCount" required="false" rtexprvalue="true"%>
			            	
<div id="tableRowButtonGroup_${id}"  class="tableRowButtonGroup tableRowGroup${groupCount+1} ${className} clearfix">
	<jsp:doBody></jsp:doBody>
</div>
