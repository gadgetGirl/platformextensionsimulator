<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="id" required="false" rtexprvalue="true"	type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true"	type="java.lang.String"%>
<%@ attribute name="type" required="false" rtexprvalue="true"%>
<c:set var="type" value="${type}"/>
<c:choose>
<c:when test="${not empty type && type =='row'}">
<div <c:if test="${not empty id}">id="${id}"</c:if> class="tableRow tableDataRow<c:if test='${not empty className}'> ${className}</c:if> clearfix" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if>>
	<div class="tableRowInner">
	<jsp:doBody />
	</div>
</div>
</c:when>
<c:when test="${not empty type && type =='groupHeading'}">
<div <c:if test="${not empty id}">id="${id}"</c:if> class="tableHeaderRow  tableGroupHeader<c:if test='${not empty className}'> ${className}</c:if>" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if>>
	<div class="tableHeaderInnerRow">
	<jsp:doBody />
	</div>
</div>  
</c:when>
<c:when test="${not empty type && type =='tableHeading'}">
<div <c:if test="${not empty id}">id="${id}"</c:if> class="tableHeaderRow  tableRowHeader<c:if test='${not empty className}'> ${className}</c:if>" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if>>
	<div class="tableHeaderInnerRow">
		<jsp:doBody />
	</div>
</div>
</c:when>
<c:otherwise>
<div <c:if test="${not empty id}">id="${id}"</c:if> class="tableRow tableDataRow<c:if test='${not empty className}'> ${className}</c:if> clearfix" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if>>
	<div class="tableRowInner">
	<jsp:doBody />
	</div>
</div>
</c:otherwise>
</c:choose>


