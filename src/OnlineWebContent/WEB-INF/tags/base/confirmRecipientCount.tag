<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="count" required="true" %>
<%@ attribute name="payAndClearNow" required="true" %>
<%@ attribute name="title" required="false" %>
<%@ attribute name="subTitle" required="false" %>

<base:divContainer className="padLeft15 padBottom5 padTop10 clearfix">

<div class="floatLeftRecipient">
	<c:choose>
		<c:when test="${empty title}">
			Paying
		</c:when>
		<c:otherwise>
			${title}
		</c:otherwise>
	</c:choose>	
</div>
<div class="recipientCount floatLeft">${count}</div>
<div class="floatLeftRecipient">
	<c:choose>
		<c:when test="${payAndClearNow == 'Yes'}">
		<c:choose>
				<c:when test="${count > 1}">
					recipients with Pay & Clear
				</c:when>
				<c:otherwise>
					recipient with Pay & Clear
				</c:otherwise>
			</c:choose>
			
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${count > 1}">
					<c:choose>
						<c:when test="${empty subTitle}">
							recipients
						</c:when>
						<c:otherwise>
							${subTitle}s
						</c:otherwise>
					</c:choose>	
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${empty subTitle}">
							recipient
						</c:when>
						<c:otherwise>
							${subTitle}
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
</div>
</base:divContainer>