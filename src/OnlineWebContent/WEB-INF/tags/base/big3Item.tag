<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="bigContent" required="true" rtexprvalue="true"%>
<%@ attribute name="smallContent" required="true" rtexprvalue="true"%>
<%@ attribute name="bannerIndex" required="false" rtexprvalue="true"%>
<%@ attribute name="preselected" required="false" rtexprvalue="true"%>
<%@ attribute name="preapproved" required="false" rtexprvalue="true"%>
<%@ attribute name="bannerCount" required="false" rtexprvalue="true"%>
<%@ attribute name="offerType" required="false" rtexprvalue="true"%><%-- Not implemented. Not in design. --%>
<%@ attribute name="onclick" required="false" rtexprvalue="true"%>

<c:set var="oneBasedIndex" value="${bannerIndex+1}" />
<c:choose>
	<c:when test="${preselected == true}">
		<c:set var="type" value="" />
	</c:when>
	<c:otherwise>
		<c:set var="type" value="preapproved" />
	</c:otherwise>
</c:choose>

<div id="big3_0${oneBasedIndex}" class="big3Container teal9 ${type}<c:if test='${bannerIndex != 2}'> borderRightWhite</c:if>">
	<div class="big3Inner" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: '${onclick}'})">
		<c:if test='${ ( (bannerCount > 3) && (oneBasedIndex == 3) ) || ( bannerCount == oneBasedIndex ) }'>
			<div id="big3ShoutOutContainer">
				<div id="big3ShoutOut">${sessionScope.SessionData["HyphenUser"].firstNames}, FNB recommends...</div>
				<div id="big3ShoutOutArrow"></div>
			</div>
		</c:if>
		<h2 class="big3Heading">${bigContent}</h2>					
		<p class="big3Paragraph">${smallContent}</p>
		<input name="offerType" value="${offerType}" id="offerType" type="hidden" />
		<input name="preselected" value="${preselected}" id="preselected" type="hidden" />
		<div class="big3MoreButton"></div>
	</div>
</div>