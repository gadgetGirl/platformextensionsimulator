<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="id" required="false"%>
<%@ attribute name="value" required="false"%>
<%@ attribute name="heading" required="false"%>

<div class="tableCell grid100" id="${id}">
	<div class="grid50 tableCellItem cellItemKey stack">${heading}&nbsp;</div>
	<div class="grid40 tableCellItem cellItemValue stack">${value}&nbsp;</div>
</div>

