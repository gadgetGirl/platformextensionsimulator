<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="width"%>
<%@ attribute required="false" name="onclick"%>
<%@ attribute required="false" name="ghostBlock"%><%-- usage: <base:equalHeightsColumn ghostBlock="true" /> --%>

<c:choose>
<c:when test="${not empty width}">
	<c:set var="widthClass">grid${width}</c:set>
</c:when>
<c:otherwise>
	<c:set var="widthClass" value="" />
</c:otherwise>
</c:choose>

<c:choose>
<c:when test="${ghostBlock == 'true'}">
	<div<c:if test="${not empty id}"> id="${id}"</c:if> class="td ${className} ghostBlock clearfix">&nbsp;</div>	
</c:when>
<c:otherwise>
	<div<c:if test="${not empty id}"> id="${id}"</c:if> <c:if test="${not empty onclick}"> onclick="${onclick}" style="cursor:pointer"</c:if> class="td ${widthClass} ${className} clearfix"><jsp:doBody /></div>
</c:otherwise>
</c:choose>