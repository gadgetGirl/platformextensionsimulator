<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="id" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true" type="java.lang.String"%>

<%@ attribute name="menuViewBean" required="true" rtexprvalue="true" type="mammoth.utility.HyphenSortedVector"%>

<div id="${id}" class="${className}">
	<div id="headerButtonsWrapper" class="headerButtonsWrapper">

	</div>
	<div id="topNavWrapper" class="topNavWrapper">
		<div id="topNavContainer" class="topNavContainer">
			<div class="topNavScrollable" id="topNavScrollable">
				<ul class="topNav" id="topNav">

	              	<c:forEach items="${menuViewBean}" var="item" varStatus="itemStatus">
	            		<base:topMenuItem tabLabel="${item.label}" link="${item.url}"/>
	        		</c:forEach>
     	
				</ul>
				<div id="topNavMenuSliderIndicator" class="topNavMenuSliderIndicator"></div>
				<div id="topNavBottomBorderContainer" class="topNavBottomBorderContainer">
					<div id="topNavMenuSlider" class="topNavMenuSlider"></div>
					<div id="topNavBottomBorder"  class="topNavBottomBorder"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="left-sidebar-top"></div>
</div>