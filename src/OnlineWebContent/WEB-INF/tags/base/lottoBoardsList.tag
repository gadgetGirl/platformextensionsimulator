<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="lottoBean" required="false" rtexprvalue="true" type="fnb.online.mammoth.buy.lotto.LottoViewBean"%>
<%@ attribute name="displayTerms" required="false" rtexprvalue="true"%>
<%@ attribute name="type" required="false" rtexprvalue="true"%>
<%@ attribute name="showElements" required="false" rtexprvalue="true"%>
<%@ attribute name="boardPrice" required="false" rtexprvalue="true"%>
<%@ attribute name="maxBoardNumbers" required="false" rtexprvalue="true"%>
<%@ attribute name="isEdit" required="false" rtexprvalue="true"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<script>
fnb.functions.lotto.init('${type}','${boardPrice}','${lottoBean.max}','${maxBoardNumbers}');
</script>

<div class="lottoBoardlist" id="lottoBoardlist">
	<c:choose>
		<c:when test="${type == 'lotto'}">
			<c:choose>
				<c:when test="${not empty lottoBean.board && !isEdit}">
					<c:forEach var="boardItem" items="${lottoBean.board}" varStatus="boardItemIndex">
						<div id="lottoBoardlistRowHolder">
							<div class="lottoBoardlistRow" id="lottoBoardlistRow">
								<div class="lottoBoardlistItem" id="lottoBoardlistItem${boardItemIndex.index}">
									<div class="lottoBoardlistItemName">
										<a target="_self" id="boardLink"></a>
									</div>
									<div class="lottoBoardlistItemNumbers">
										<script>
										fnb.functions.lotto.populateConfirmNumbers('${boardItem}','${boardItemIndex.index}');
										</script>
									</div>
								</div>
								<input id="board${boardItemIndex.count-1}" name="board${boardItemIndex.count-1}" value="${boardItem}" type="hidden">
							</div>
						</div>
						<c:set var="nrElems" value="${boardItemIndex.count}" />
					</c:forEach>
				</c:when>
				<c:otherwise>
					<div id="lottoBoardlistRowHolder">
						<div id="lottoScrollTop" class="lottoScrollTop" onclick="return false;"></div>
						<div id="lottoBoardsHolder">
							<div id="maxMessage" class="teal1">Maximum number of boards reached</div>
							<div class="lottoBoardlistRow" id="lottoBoardlistRow">
								<c:choose>
									<c:when test="${not empty lottoBean.board && isEdit}">
										<c:forEach var="boardItem" items="${lottoBean.board}" varStatus="boardItemIndex">
											<div class="lottoBoardlistItem" id="lottoBoardlistItem${boardItemIndex.index}">
												<div class="lottoBoardlistItemName">
													<a target="_self" id="boardLink" onclick="fnb.functions.lotto.numberPickerInitialize(${boardItemIndex.index});">Board A</a>
												</div>
												<div class="lottoBoardlistItemNumbers">
													<script>
														fnb.functions.lotto.populateConfirmNumbers('${boardItem}','${boardItemIndex.index}');
														//fnb.functions.lotto.setElemCount(${boardItemIndex.count});
													</script>
												</div>
												<input id="board${boardItemIndex.index}" name="board${boardItemIndex.index}" value="${boardItem}" type="hidden">
												<div class="lottoBoardlistClose">
													<a class="formButton inverted small" target="_self" onclick="fnb.functions.lotto.lottoRemoveBoard(${boardItemIndex.index});">X</a>
												</div>
											</div>
											<c:set var="nrElems" value="${boardItemIndex.count}" />
										</c:forEach>
									</c:when>
									<c:otherwise>
										<div class="lottoBoardlistItem" id="lottoBoardlistItem0">
											<div class="lottoBoardlistItemName">
												<a target="_self" id="boardLink" onclick="fnb.functions.lotto.numberPickerInitialize(0);">Board A</a>
											</div>
											<div class="lottoBoardlistItemNumbers">
												<a target="_self" id="numbersLink" onclick="fnb.functions.lotto.numberPickerInitialize(0);">Choose Numbers</a>
											</div>
											<input id="board0" name="board0" value="" type="hidden">
											<div class="lottoBoardlistClose">
												<a class="formButton inverted small" target="_self" onclick="fnb.functions.lotto.lottoRemoveBoard(0);">X</a>
											</div>
										</div>
										<div class="lottoBoardlistItem" id="lottoBoardlistItem1">
											<div class="lottoBoardlistItemName">
												<a target="_self" id="boardLink" onclick="fnb.functions.lotto.numberPickerInitialize(1);">Board B</a>
											</div>
											<div class="lottoBoardlistItemNumbers">
												<a target="_self" id="numbersLink" onclick="fnb.functions.lotto.numberPickerInitialize(1);">Choose Numbers</a>
											</div>
											<input id="board1" name="board1" value="" type="hidden">
											<div class="lottoBoardlistClose">
												<a class="formButton inverted small" target="_self" onclick="fnb.functions.lotto.lottoRemoveBoard(1);">X</a>
											</div>
										</div>
										<c:set var="nrElems" value="2" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div id="lottoScrollBottom" class="lottoScrollBottom" onclick="return false;"></div>
						<div onclick="fnb.functions.lotto.lottoAddBoard();" class="lottoBoardlistButtonHolder" id="lottoBoardlistButtonHolder">
							<base:doSomething text="Add Another Board" />
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:when test="${type == 'powerball'}">
			<c:choose>
				<c:when test="${not empty lottoBean.board && !isEdit}">
					<c:forEach var="boardItem" items="${lottoBean.board}" varStatus="boardItemIndex">
						<div id="lottoBoardlistRowHolder">
							<div class="lottoBoardlistRow" id="lottoBoardlistRow">
								<div class="lottoBoardlistItem" id="lottoBoardlistItem${boardItemIndex.index}">
									<div class="lottoBoardlistItemName">
										<a target="_self" id="boardLink"></a>
									</div>
									<div class="lottoBoardlistItemNumbers">
										<script>
										fnb.functions.lotto.populateConfirmNumbers('${boardItem}','${boardItemIndex.index}');
										</script>
									</div>
								</div>
								<input id="board${boardItemIndex.count-1}" name="board${boardItemIndex.count-1}" value="${boardItem}" type="hidden">
							</div>
						</div>
						<c:set var="nrElems" value="${boardItemIndex.count}" />
					</c:forEach>
				</c:when>
				<c:otherwise>
					<div id="lottoBoardlistRowHolder">
						<div id="lottoScrollTop" class="lottoScrollTop" onclick="return false;"></div>
						<div id="lottoBoardsHolder">
							<div id="maxMessage" class="teal1">Maximum number of boards reached</div>
							<div class="lottoBoardlistRow" id="lottoBoardlistRow">
								<c:choose>
									<c:when test="${not empty lottoBean.board && isEdit}">
										<c:forEach var="boardItem" items="${lottoBean.board}" varStatus="boardItemIndex">
											<div class="lottoBoardlistItem" id="lottoBoardlistItem${boardItemIndex.index}">
												<div class="lottoBoardlistItemName">
													<a target="_self" id="boardLink" onclick="fnb.functions.lotto.numberPickerInitialize(${boardItemIndex.index});">Board A</a>
												</div>
												<div class="lottoBoardlistItemNumbers">
													<script>
														fnb.functions.lotto.populateConfirmNumbers('${boardItem}','${boardItemIndex.index}');
														//fnb.functions.lotto.setElemCount(${boardItemIndex.count});
													</script>
												</div>
												<input id="board${boardItemIndex.index}" name="board${boardItemIndex.index}" value="${boardItem}" type="hidden">
												<div class="lottoBoardlistClose">
													<a class="formButton inverted small" target="_self" onclick="fnb.functions.lotto.lottoRemoveBoard(${boardItemIndex.index});">X</a>
												</div>
											</div>
											<c:set var="nrElems" value="${boardItemIndex.count}" />
										</c:forEach>
									</c:when>
									<c:otherwise>
										<div class="lottoBoardlistItem" id="lottoBoardlistItem0">
											<div class="lottoBoardlistItemName">
												<a target="_self" id="boardLink" onclick="fnb.functions.lotto.numberPickerInitialize(0);">Board A</a>
											</div>
											<div class="lottoBoardlistItemNumbers">
												<a target="_self" id="numbersLink" onclick="fnb.functions.lotto.numberPickerInitialize(0);">Choose Numbers</a>
											</div>
											<input id="board0" name="board0" value="" type="hidden">
											<div class="lottoBoardlistClose">
												<a class="formButton inverted small" target="_self" onclick="fnb.functions.lotto.lottoRemoveBoard(0);">X</a>
											</div>
										</div>
										<div class="lottoBoardlistItem" id="lottoBoardlistItem1">
											<div class="lottoBoardlistItemName">
												<a target="_self" id="boardLink" onclick="fnb.functions.lotto.numberPickerInitialize(1);">Board B</a>
											</div>
											<div class="lottoBoardlistItemNumbers">
												<a target="_self" id="numbersLink" onclick="fnb.functions.lotto.numberPickerInitialize(1);">Choose Numbers</a>
											</div>
											<input id="board1" name="board1" value="" type="hidden">
											<div class="lottoBoardlistClose">
												<a class="formButton inverted small" target="_self" onclick="fnb.functions.lotto.lottoRemoveBoard(1);">X</a>
											</div>
										</div>
										<c:set var="nrElems" value="2" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div id="lottoScrollBottom" class="lottoScrollBottom" onclick="return false;"></div>
						<div onclick="fnb.functions.lotto.lottoAddBoard();" class="lottoBoardlistButtonHolder" id="lottoBoardlistButtonHolder">
							<base:doSomething text="Add Another Board" />
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</c:when>
	</c:choose>
	
	<script>
	fnb.functions.lotto.setElemCount(${nrElems});
	</script>
	
	<c:if test="${displayTerms == true}">
		<base:note type="tc" className="lottoTC" noteHeading="Terms and Conditions">
			<base:checkboxItem id="termsAndConditions" name="termsAndConditions" value="terms">
				I have read and accept the <base:hyperlink href="#" onclick="window.open('/banking/mammoth/staticPdfs/FirstRandPowerballTerms&Conditions.pdf','','width=800, height=600,left=350,top=250,scrollbars=yes,status=yes');return false;">Terms and Conditions</base:hyperlink>
			</base:checkboxItem>
		</base:note>
		<%-- <div class="lottoTC">
			<base:heading level="4" value="Terms and Conditions"/>
			<div class="inputBox" style="float: left; width: 34px;">
				<base:formsCheckboxItem id="termsAndConditions" name="termsAndConditions"/>
				<base:checkboxItem id="termsAndConditions" name="termsAndConditions" value="terms" />
			</div>
			<div class="messageMultiLineText">
				I have read and accept the
				<base:hyperlink href="#" onclick="window.open('https://www.online.fnb.co.za/mammoth/staticPdfs/FirstRandPowerballTerms&Conditions.pdf','','width=800, height=600,left=350,top=250,scrollbars=yes,status=yes');return false;">Terms and Conditions</base:hyperlink>
			</div>
			<div style="clear: both"></div>
		</div> --%>
	</c:if>
</div>