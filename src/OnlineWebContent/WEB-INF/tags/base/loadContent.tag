<%@ attribute name="key" required="java.lang.String" %>
<%@ attribute name="country" required="java.lang.String" %>
<%@ attribute name="skin" required="java.lang.String" %>
<%@ attribute name="language" required="java.lang.String" %>

<jsp:include page="/mammoth/staticContent/${country}/${skin}/${language}/${key}.jsp" />
