<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<%@ attribute required="true"  name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="name"%>
<%@ attribute required="false" name="label"%>

<jsp:useBean id="authorityLevels" type="java.util.ArrayList<java.lang.String>" scope="request" />

<c:if test="${fn:length(authorityLevels) eq '1'}">
	<base:heading level="3" value="Card Details"/>
	<base:input id="cardTypeFunctionality" type="hidden" value="${authorityLevels[0]}" ></base:input>	
	<base:equalHeightsRow>
		<base:equalHeightsColumn width="50" className="white">
			<base:definitionList className="margin50">
				<base:definitionListItem  className="" description="Card Type" value="${authorityLevels[0]}" />
			</base:definitionList>
		</base:equalHeightsColumn>
		<base:equalHeightsColumn width="50" className="white"></base:equalHeightsColumn><base:equalHeightsColumn ghostBlock="true" />
	</base:equalHeightsRow>
</c:if>

<c:if test="${fn:length(authorityLevels) gt '1'}">
	<base:heading level="3" value="Card Details"/>
	<base:equalHeightsRow>
		<base:equalHeightsColumn width="50" className="white">
			<base:radioButtonContainer className="margin50" id="cardTypeFunctionality" name="cardTypeFunctionality" label="Card Type"  labelPosition="labelLeft" value="${cardMaintenanceDTO.cardTypeFunctionality}" >
				<c:forEach var="authorityLevel" items="${authorityLevels}" varStatus="status">
					<base:radioButtonItem description="${authorityLevel}" value="${authorityLevel}" selectedValue="${cardMaintenanceDTO.cardTypeFunctionality}"/>
				</c:forEach>
			</base:radioButtonContainer>
		</base:equalHeightsColumn>
		<base:equalHeightsColumn width="50" className="white"></base:equalHeightsColumn><base:equalHeightsColumn ghostBlock="true" />
	</base:equalHeightsRow>
</c:if>

