<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="style" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="headingText" required="true" rtexprvalue="true"%>
<%@ attribute name="color" required="false" rtexprvalue="true"%>

<div id="${id}" <c:if test='${not empty style}'>style="${style}"</c:if> class="actionMenuHead ${color} ${className}">${headingText}</div>