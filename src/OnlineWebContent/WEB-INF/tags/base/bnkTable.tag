<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@tag import="fnb.online.tags.beans.dropdown.*"%>

<%@ tag import="java.util.HashMap"%>
<%@ tag import="java.math.BigDecimal"%>
 
<%@ attribute name="action" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="true" rtexprvalue="true"%>
<%@ attribute name="phoneContent" required="false" rtexprvalue="true"%>

<%@ attribute name="onRowClick" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="true" rtexprvalue="true"%>
<%@ attribute name="tableHeading" required="false" rtexprvalue="true"%>
<%@ attribute name="tableBean" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableBean"%>
 
<%@ attribute name="switcher" required="false" rtexprvalue="true"%>
<%@ attribute name="doSomethingBar" required="false" rtexprvalue="true"%>
<%@ attribute name="forceTableControls" required="false" rtexprvalue="true"%>

<%@ attribute name="cloneEnabled" required="false" rtexprvalue="true"%>
<%@ attribute name="cloneButtonLabel" required="false" rtexprvalue="true"%>
<%@ attribute name="cloneTarget" required="false" rtexprvalue="true"%>
<%@ attribute name="cloneTargetParent" required="false" rtexprvalue="true"%>
<%@ attribute name="cloneOnClick" required="false" rtexprvalue="true"%>

<%@ attribute name="enableJSobject" required="false" rtexprvalue="true"%>
<%@ attribute name="downloadLink" required="false" rtexprvalue="true"%>
<%@ attribute name="isActionMenu" required="false" rtexprvalue="true"%>
<%@ attribute name="eziItemClickLoad" required="false" rtexprvalue="true"%>

<%@ attribute name="headerContent" required="false" rtexprvalue="true"%>
<%@ attribute name="disableSorting" required="false" rtexprvalue="true"%>

<%@ attribute required="false"  name="newFrameWorkFlag" %>

<% 	
	//this is done to allow paging to pass attributes to the Content 
	tableBean.setTableId(id); 
   	tableBean.setEnableJSobject(enableJSobject);
   	tableBean.setTableHeading(tableHeading);
   	tableBean.setOnRowClick(onRowClick);
%>
<c:choose>
	<c:when test="${not empty disableSorting}">
		<c:set var="disableSorting" value="true" />
	</c:when>
	<c:otherwise>
		<c:set var="disableSorting" value="false" />
	</c:otherwise>

</c:choose>


<div class="tableContainer switchable" id="${id}">
	
	<!----------------------------------------- headings ----------------------------------------->
			
	<base:table id="${id}_table" className="${className}" smallSize="threeLabel">
	<!----------------------------------------- content ----------------------------------------->
	<div id="${id}_tableContent">	
		<base:bnkTableContent newFrameWorkFlag="${newFrameWorkFlag}" noSorting="${disableSorting}"  headerContent="${headerContent}" id="${tableBean.tableId}" tableBean="${tableBean}" downloadUrl="${downloadLink}" isActionMenu="${isActionMenu}" eziItemClickLoad="${tableBean.eziItemClickLoad}" ></base:bnkTableContent>
	</div>		
	</base:table>
		
	<c:if test="${cloneEnabled == true}">
		<base:gridGroup id="addBttBar">
			<base:gridFull position="firstChild" className="turqDarkBg">
				<base:doSomething className="tableDoSomething" type="add" text="${cloneButtonLabel}" onclick="${cloneOnClick}" />
			</base:gridFull>
		</base:gridGroup>
		<%--<base:tableCloneRow label="${cloneButtonLabel}" onclick="${cloneOnClick}" targetClass="${cloneTarget}" targetParentClass="${cloneTargetParent}" />--%>
	</c:if>
	
</div>	

<script type="text/javascript">
	//author M.G
	var ${id}_tableController = new ChameleonTable('${id}');		//create a controller object for this table
	${id}_tableController.setSortingCallbackFunction(${tableBean.tableOptions.sortingCallbackFunction});						//set the sorting callback function
	${id}_tableController.setPagingCallbackFunction(${tableBean.tableOptions.pagingCallbackFunction});							//set the paging callback function
	${id}_tableController.setSearchCallbackFunction(${tableBean.tableOptions.searchCallbackFunction});							//set the search callback function
	${id}_tableController.setChangePageSizeCallbackFunction(${tableBean.tableOptions.changePageSizeCallbackFunction});			//set the search changePageSize function
	${id}_tableController.setServerUrl("${tableBean.pagingViewBean.url}");														//set the server side url
	${id}_tableController.setFormname("${tableBean.tableOptions.formToSubmit}");												
	console.log("created chameleon table controller - #${id}");
</script>
	
