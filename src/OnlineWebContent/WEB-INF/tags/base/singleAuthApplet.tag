<%@ taglib uri="/WEB-INF/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute required="false" name="heading"%>

<c:if test="${not empty heading}">
	<base:confirmFinishMultiTableHeader value="${heading}" />
</c:if>

<base:signingApplet authListViewBean="${authListViewBean}" isHardCert="${theUser.isHardCert()}"></base:signingApplet>
