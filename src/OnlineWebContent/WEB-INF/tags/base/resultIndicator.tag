<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<div class="finishTagButtonSet">
	<base:equalHeightsRow>
		<c:choose>
			<c:when test="${processingResult.pending == 'true'}">
				<base:equalHeightsColumn className="centerContent" width="25">
					<img src="/banking/03images/fnb/finish/pendingColorIcon.png" alt="">
				</base:equalHeightsColumn>
			</c:when>
			
			<c:otherwise>
				<c:choose>
					<c:when test="${processingResult.errorCode == 0}">
						<base:equalHeightsColumn className="centerContent" width="25">
							<img src="/banking/03images/fnb/icons/icon_check.png" alt="">
						</base:equalHeightsColumn>
					</c:when>
					<c:when test="${processingResult.errorCode == 350}">
						<base:equalHeightsColumn className="centerContent" width="25">
							<img src="/banking/03images/fnb/icons/icon_pending.png" alt="">
						</base:equalHeightsColumn>
					</c:when>
					<c:otherwise>
						<base:equalHeightsColumn className="centerContent" width="25">
							<img src="/banking/03images/fnb/icons/icon_cross.png" alt="">
						</base:equalHeightsColumn>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	
		<base:equalHeightsColumn className="centerContent" width="25">
			<c:choose>
				<c:when test="${not empty processingResult.downloadButton}">
					<div class="positionRelative">
						<img class="absolutePosition" src="/banking/03images/fnb/icons/label-download.png" />
						<a href="#" target="_self" class="finishDownloadButton" onclick="fnb.controls.controller.eventsObject.raiseEvent('eziSliderShowBody', '${processingResult.downloadButton.url}')"><img src="/banking/03images/fnb/finish/icon_download.png" alt=""></a>
					</div>
				</c:when>
				<c:otherwise>
					&nbsp;
				</c:otherwise>
			</c:choose>
		</base:equalHeightsColumn>
		
		<base:equalHeightsColumn className="centerContent" width="25">
			<c:choose>
				<c:when test="${not empty processingResult.printButton}">
					<div class="positionRelative">
						<img class="absolutePosition" src="/banking/03images/fnb/icons/label-print.png" />
						<a href="#" target="_self" class="finishPrintButton" onclick="fnb.functions.loadUrlToPrintDiv.load('${processingResult.printButton.url}')"><img src="/banking/03images/fnb/finish/icon_print.png" alt=""></a>
					</div>
				</c:when>
				<c:otherwise>
					&nbsp;
				</c:otherwise>
			</c:choose>
		</base:equalHeightsColumn>
	
		<base:equalHeightsColumn className="centerContent" width="25">
			<c:choose>
				<c:when test="${not empty processingResult.emailButton}">
					<div class="positionRelative">
						<img class="absolutePosition" src="/banking/03images/fnb/icons/label-email.png" />
						<a href="#" target="_self" class="finishEmailButton" onclick="fnb.controls.controller.eventsObject.raiseEvent('eziSliderShowBody', '${processingResult.emailButton.url}')"><img src="/banking/03images/fnb/finish/icon_email.png" alt=""></a>
					</div>
				</c:when>
				<c:otherwise>
					&nbsp;
				</c:otherwise>
			</c:choose>
		</base:equalHeightsColumn>
	
	</base:equalHeightsRow>
</div>
	