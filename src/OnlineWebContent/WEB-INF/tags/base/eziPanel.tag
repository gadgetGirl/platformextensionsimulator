<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<div class="eziWrapperOuter  hidden">
<div class="eziWrapperOuterInner">
<div id="eziWrapper" class="eziWrapper">
	<div id="eziProgressWrapperContents" data-attr="eziPanel"></div>
	<base:progressBar id="Ezi"/>
	<div id='eziSpacer' class='eziSpacer'></div>
</div>
</div>
</div>