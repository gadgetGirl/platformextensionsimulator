<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%-- <jsp:useBean id="footerButtons" class="java.util.Vector" scope="request" /> --%>
<%@ attribute required="false" rtexprvalue="true" name="className" %>

<div id="formFooterButtons" class="gridGroup formFooterButtons ${className} hidden">
	<%-- 
	   B-E uses choose for their Button Builder
	    footerButtons should be a List<ActionViewBean>
	--%>
	<div class="clearfix">
	<jsp:doBody/>
	<c:forEach items="${footerButtons}" var="button">
		<base:footerButton text="${button.label}" formToSubmit="${button.formName}" url="${button.url}" />
	</c:forEach>
	<c:forEach items="${footerButtonsResult}" var="button">
		<base:footerButton text="${button.label}" formToSubmit="${button.formName}" url="${button.url}" />
	</c:forEach>
	</div>
</div>

<script>

fnb.functions.footer.add();

</script>

