<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="className" required="false"%>
<style>
#phoneMenu {display:none;float:left;width:100%;}
</style>
<div id="phoneMenu" <c:if test="${not empty className}">class="${classname}"</c:if> >
<jsp:doBody></jsp:doBody>
</div>