<%@ tag import="fnb.online.tags.beans.table.TableDateBoxItem"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableDateBoxItem"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>

<c:choose>
	<c:when test="${tableDoubleItem == true}">
		<base:datePicker id="${tableItem.id}"/>
	</c:when>
	<c:otherwise>
		<div id="${tableItem.id}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size}" >
			<base:datePicker id="${tableItem.id}"/>
		</div>
	</c:otherwise>
</c:choose>









