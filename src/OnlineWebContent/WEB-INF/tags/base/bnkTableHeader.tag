<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ tag body-content="scriptless"  %>
<%@ attribute name="id" required="false" rtexprvalue="true" %>
<%@ attribute name="className" required="false" rtexprvalue="true" %>

<div class="tableCell col ${className}">
	<div class="hinner tableCellInner " id="${id}">
		<jsp:doBody var="theBody"/>
		${theBody}
	</div>
</div>