<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="showTab" required="false" rtexprvalue="true"%>
<%@ attribute name="text" required="true" rtexprvalue="true"%>
<%@ attribute name="url" required="true" rtexprvalue="true"%>
<%@ attribute name="isActiveTab" required="false" rtexprvalue="true"%>
<%@ attribute name="isDisabled" required="false" rtexprvalue="true"%>
<div data-value="${url}"<c:if test="${isDisabled=='true'}"> data-disabled="true"</c:if> class="subTabButton subTab<c:if test="${showTab}"> subTab${showTab}</c:if><c:if test="${isActiveTab=='true'}"> active</c:if><c:if test="${isDisabled=='true'}"> disabled</c:if>"><div class="subTabText">${text}</div></div>