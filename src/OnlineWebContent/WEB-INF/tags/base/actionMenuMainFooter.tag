<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="style" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="headingText" required="true" rtexprvalue="true"%>
<%@ attribute name="color" required="false" rtexprvalue="true"%>
     	
<div id="${id}" style="${style}" class="actionMenuFooter ${color} ${className}">    	
	
		<div class="innerPad">${headingText}</div>
		
</div>
