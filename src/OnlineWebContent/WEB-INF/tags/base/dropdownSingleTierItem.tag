<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute required="true"  rtexprvalue="true" name="content" %>
<%@ attribute required="false" rtexprvalue="true" name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="onclick" %>
<%@ attribute required="false" rtexprvalue="true" name="returnValue" %>
<%@ attribute required="false" rtexprvalue="true" name="selected" %>

<c:set var="returnValue" value="${not empty returnValue ? returnValue : content}" />
<c:set var="selectedClass" value="${selected ? ' selected-item' : ''}" />

<li <c:if test="${not empty id}">id="${id}"</c:if> class="dropdown-item ${className}${selectedClass}" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if> data-value="${returnValue}">
	<div class="dropdown-item-row-wrapper"><div class="dropdown-item-row dropdown-h4 singleLine">${content}</div></div>
</li>