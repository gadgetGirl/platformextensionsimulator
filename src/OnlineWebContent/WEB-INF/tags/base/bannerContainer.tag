<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="seconds" required="true" rtexprvalue="true"%>
	
	<div id="bannerWrapper" class="${className}">
		<div class="inner">
			<jsp:doBody></jsp:doBody>
		</div>
	</div>
	<div style="clear: both;"></div>
	<div class="bannerBorder"></div>

<base:import type="js" link="/banking/02javascript/tags/banners.js"/>

<script type="text/javascript">
	var seconds = ${seconds};
	bannerRotator.rotater($("#bannerWrapper .inner"), '${seconds}');
</script>
