<%@ attribute name="rowGroupHeader" required="false" rtexprvalue="true"%>
<%@ attribute name="action" required="false" rtexprvalue="true"%>
<%@ attribute name="tableRowGroup" required="false" rtexprvalue="true" type="fnb.online.tags.beans.table.TableRowGroup"%>
<%@ attribute name="tableBean" required="false" rtexprvalue="true" type="fnb.online.tags.beans.table.TableBean"%>
<%@ attribute name="index" required="false" rtexprvalue="true"%>

<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@tag import="java.util.Map"%>
<%@tag import="java.util.Iterator"%>
<%@tag import="fnb.online.tags.beans.table.TableColumnGroup"%>
<%@tag import="fnb.online.tags.beans.table.TableColumnOptions"%>
<c:if test="${rowGroupHeader}">
	<%
			boolean hasOverrideInColumns = false;
			boolean hasSelectAllInColumn = false;
			if(tableRowGroup!=null){
				
				Iterator<TableColumnGroup> columnGroups = tableBean.getTableOptions().getColumnGroups().iterator();
				while (columnGroups.hasNext()) {
					TableColumnGroup tableColumnGroup = (TableColumnGroup)columnGroups.next();
					Iterator<TableColumnOptions> columnOptions = tableColumnGroup.getColumnOptions().iterator();
					while (columnOptions.hasNext()) {
						TableColumnOptions tableColumnOptions = (TableColumnOptions)columnOptions.next();
						if (tableColumnOptions.isSelectAll()) {
							hasSelectAllInColumn = true;
							break;
						}
					}
					if (hasSelectAllInColumn) break;
				}
				
				Map<String, String> overrideColumnUrl = tableRowGroup.getOverrideColumnUrlMap();
				Iterator<Map.Entry<String, String>> OCUIteratorEntries = overrideColumnUrl.entrySet().iterator();
				while (OCUIteratorEntries.hasNext()) {
					OCUIteratorEntries.next();
					hasOverrideInColumns = true;
				}
			}
			
	%>
	<c:set var="hasOverrideInColumns" value="<%=hasOverrideInColumns%>" />
	<c:set var="hasSelectAllInColumn" value="<%=hasSelectAllInColumn%>" />
	</c:if>
<div <c:if test='${not empty index}'>id="tableHeaderRow_${index}" </c:if>class="tableHeaderRow  <%=(Boolean.valueOf(rowGroupHeader) ? "tableGroupHeader" : "tableRowHeader")%> <c:if test='${hasOverrideInColumns || hasSelectAllInColumn}'>selectAdjust</c:if>">  <!-- the tableRowHeader class is required for selection purposes, but doesn't require an actual css definition as such. MG -->	      
	<div class="tableHeaderInnerRow">
		
		<jsp:doBody></jsp:doBody>	    
		 	
	</div>
</div>




