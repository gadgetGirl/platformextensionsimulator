<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="onClick" required="false" rtexprvalue="true"%>

<div<c:if test="${not empty id}"> id="${id}"</c:if> class="scrollingBannerItem<c:if test="${not empty className}"> ${className}</c:if>"<c:if test="${not empty onClick}"> onclick="${onClick}"</c:if>>
	 	<jsp:doBody/>
</div>
