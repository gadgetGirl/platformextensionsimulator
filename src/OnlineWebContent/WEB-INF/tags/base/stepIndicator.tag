<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="id" required="true" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>

<base:import type="css" link="/banking/01css_new/pages/tags/stepIndicator.css" />

<div id="${id}" class="stepIndicator ${className} clearfix">
	<jsp:doBody></jsp:doBody>
</div>