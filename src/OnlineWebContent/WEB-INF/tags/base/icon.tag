<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="size" required="false" rtexprvalue="true"%>
<%@ attribute name="type" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="false" rtexprvalue="true"%>
<%@ attribute name="onclick" required="false" rtexprvalue="true"%>

<div class="ui-iconWrapper ${size}" onclick="${onclick}">
	<div class="eziPannelTitleIcon"></div>
	<div class="ui-icon ${type}"></div>
	<c:if test="${not empty label}">
		<span class="icon-label">${label}</span>
	</c:if>
</div>