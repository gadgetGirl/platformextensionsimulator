<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%--
SOUTHAFRICA = 15;
KULULA 		= 21;
VODACOM 	= 22;
CLICKS 		= 24;
NAMIBIA 	= 45;
BOTSWANA 	= 55;
SWAZILAND 	= 65;
EBUCKS 		= 75;
LESOTHO 	= 80;
ZAMBIA 		= 81;
TANZANIA 	= 82;
INDIA 		= 83;
--%>

<c:choose>
	<c:when test="${country=='21'}">0860 MOOLAH (666524)</c:when>
	<%--<c:when test="${country=='22'}"></c:when>--%>
	<c:when test="${country=='24'}">086 011 2244</c:when>
	<c:when test="${country=='45'}">(00264) 61 299 2187</c:when>
	<c:when test="${country=='55'}">(09267) 364-2600</c:when>
	<c:when test="${country=='65'}">(00268) 518-4637</c:when>
	<%--<c:when test="${country=='75'}"></c:when>--%>
	<c:when test="${country=='80'}">(00266) 222-47100</c:when>
	<c:when test="${country=='81'}">(00260) 21 136-6800</c:when>
	<c:when test="${country=='82'}">(255) 768 989 050</c:when>
	<c:when test="${country=='83'}">1800 266 8888</c:when>
	
	<c:otherwise>087 575 0000</c:otherwise>
</c:choose>