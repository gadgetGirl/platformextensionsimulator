<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%--
The following TAGs should someday be consolidated into one:
	publicRecipientSearch.tag
	branchSearch.tag
	phoneBookSearch.tag
--%>
<%@ attribute required="false" rtexprvalue="true" name="name"%>
<%@ attribute required="false" rtexprvalue="true" name="type"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="value"%>
<%@ attribute required="false" rtexprvalue="true" name="customButtonLabel"%>
<%@ attribute required="false" rtexprvalue="true" name="label"%>
<%@ attribute required="false" rtexprvalue="true" name="maxlength" %>
<%@ attribute required="false" rtexprvalue="true" name="disabled"%>
<%@ attribute required="false" rtexprvalue="true" name="readonly"%>
<%@ attribute required="false" rtexprvalue="true" name="onclick" type="java.lang.String"%>
<%@ attribute required="false" rtexprvalue="true" name="onkeyup" type="java.lang.String"%>
<%@ attribute required="false" rtexprvalue="true" name="onchange" type="java.lang.String"%>
<%@ attribute required="false" rtexprvalue="true" name="searchClick" type="java.lang.String"%>

<%@ attribute required="false" rtexprvalue="true" name="labelPosition" description="DEPRECATED"%>
<%@ attribute required="false" rtexprvalue="true" name="onClick" description="DEPRECATED - use lowercase onclick"%>

<style type="text/css">
#phoneBookSearchContainerText .formElementWrapper{
	padding: 0;
}
#phoneBookSearchContainerText .formElementContainer{
	padding: 0;
}

#phoneBookSearchContainerText .grid60{width:100%;}
</style>

<c:set var="name" value="${not empty name ? name : 'phoneBookSearch'}" />
<c:set var="searchClick">
	<c:choose>
		<c:when test="${empty searchClick}">
			fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '/banking/Controller?nav=navigator.phonebookSearch.simple.PhonebookSearch'});
		</c:when>
		<c:otherwise>
			${searchClick}
		</c:otherwise>
	</c:choose>
</c:set>


<div class="formElementWrapper input-wrapper<c:if test="${type == 'hidden'}"> hidden</c:if> ${className} clearfix">
	<c:if test="${not empty label}">
		<div class="formElementLabel gridCol grid40 input-label"><div class="input-label-inner">${label}</div></div>
	</c:if>
	<div id="phoneBookSearchContainerText" class="formElementContainer gridCol grid30 input-container clearfix">
		<%--Removed by usability--%>
		<%-- <div class="searchArrowBlock"></div>--%>
		<div class="input-container-inner">
		
			<base:input id="phoneBookSearch"
				name="${name}"
				value="${value}"
				type="number"
				maxlength="15"
				disabled="${disabled}"
				readonly="${readonly}"
				onclick="${onclick}" 
				onkeyup="${onkeyup}" 
				onchange="${onchange}">
			</base:input>
		</div>
	</div>
	<div class="searchOrText gridCol  grid5">or</div>
	<div class="gridCol grid25 formElementContainer">
		<base:formsButton text="Phonebook" id="phoneBookSearchButton" type="button" onclick="${searchClick}" />
	</div>
</div>
