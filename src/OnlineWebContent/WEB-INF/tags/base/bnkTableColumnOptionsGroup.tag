<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@tag import="fnb.online.tags.beans.table.BeanUtilities"%>
<%@tag import="fnb.online.tags.beans.table.TableColumnGroup"%>
<%@tag import="fnb.online.tags.beans.table.TableOptions"%>
<%@ attribute name="group_id" required="true" rtexprvalue="true"%>
<%@ attribute name="group_desc" required="false" rtexprvalue="true"%>
<%@ attribute name="group_border_left" required="false" rtexprvalue="true"%>
<%@ attribute name="group_border_right" required="false" rtexprvalue="true"%>
<%@ attribute name="hideForMobile" required="false" rtexprvalue="true"%>

<% 
String optionsId = BeanUtilities.getFieldValuePairs(getParent(),false).get("id");
TableOptions tableOptions = (TableOptions) request.getAttribute(optionsId);
TableColumnGroup tableColumnGroup = new TableColumnGroup(group_id, group_desc, group_border_left, group_border_right,hideForMobile);
tableOptions.addTableColumnGroup(tableColumnGroup);
request.setAttribute(group_id,tableColumnGroup);
%>

<jsp:doBody></jsp:doBody>
