	
	<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute required="false" rtexprvalue="true" name="pageType"%>
<%@ attribute required="false" rtexprvalue="true" name="accNumber"%>
<%@ attribute required="false" rtexprvalue="true" name="accName"%>
<%@ attribute required="false" rtexprvalue="true" name="date"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<style>
#eziHeader_date {float:right;display:block;font-size:1em;position:relative;padding-right:10px;}
</style>
<base:gridGroup className="borderBottomWhite">
	<base:gridCol className="${className}">
		<c:if test="${not empty pageType}"><div id="eziHeader_pageType">${pageType}</div></c:if>
		<c:if test="${not empty accName}"><div id="eziHeader_accName">${accName}</div></c:if>
		<c:if test="${not empty accNumber}"><div id="eziHeader_accNumber">${accNumber}<c:if test="${not empty date}"><div id="eziHeader_date">${date}</div></c:if></div></c:if>
	</base:gridCol>
</base:gridGroup>