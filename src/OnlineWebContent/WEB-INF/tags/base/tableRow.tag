<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="onClickFunction" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="groupCount" required="false" rtexprvalue="true"%>

<!-- "tableDataRow" required for selection purposes. MG -->

<div id="tabelRow_${id}"  class="tableRow tableDataRow tableRowGroup${groupCount+1} ${className} clearfix"<c:if test="${not empty onClickFunction}"> onclick="${onClickFunction}"</c:if>>
	<jsp:doBody></jsp:doBody>
</div>
