<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute required="false" rtexprvalue="true" name="label"%>
<%@ attribute required="false" rtexprvalue="true" name="linklabel"%>
<%@ attribute required="true" rtexprvalue="true" name="url"%>
<%@ attribute required="true" rtexprvalue="true" name="target"%>
<c:set var="onclickFunction">
<c:choose>
				<c:when test="${target == '0'}">fnb.controls.controller.eventsObject.raiseEvent('loadResultScreen','${url}');</c:when>
				<c:when test="${target == '1'}">fnb.utils.actionMenu.loadTargetToActionMenu('${url}',this);</c:when>
				<c:when test="${target == '3'}">fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '${url}'});</c:when>
				<c:when test="${target == '4'}">fnb.functions.loadUrlToPrintDiv.load('${url}');</c:when>
				<c:when test="${target == '5'}">fnb.controls.controller.eventsObject.raiseEvent('doDownload','${url}');</c:when>
				<c:when test="${target == '6'}">fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '${url}');</c:when>
</c:choose>
</c:set>
<div data-type="0" class="tableActionButton addButton" onclick="${onclickFunction};return false;">
	<div class="tableActionButtonLabel">${label}</div>
</div>
