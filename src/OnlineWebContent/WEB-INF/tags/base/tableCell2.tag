<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="align" required="false" rtexprvalue="true"%>
<%@ attribute name="size" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="lineCollapse" required="false" rtexprvalue="true"%>

<span class="item ${align} ${lineCollapse}" id="${id}">
<span class="rinner ${size}">

<c:if test="${not empty label}"><span class="phoneLabel">${label}</span></c:if>

	<span class="cellContent">
		<jsp:doBody></jsp:doBody>
	</span>

</span>
</span>






