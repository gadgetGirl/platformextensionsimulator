<%@ attribute name="className" required="false" rtexprvalue="true"%>

<div class="horizontalLineWrapper">

	<div class="horizontalLine ${className}">

	</div>

	<div class="circle ${className}">

	</div>

</div>