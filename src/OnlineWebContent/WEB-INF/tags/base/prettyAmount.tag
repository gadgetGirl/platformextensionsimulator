<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@tag import="mammoth.utility.HyphenString"%>
<%@ attribute name="amount" required="true" rtexprvalue="true" type="java.math.BigDecimal"%>
<%@ attribute name="currency" required="false" rtexprvalue="true" type="java.lang.Boolean"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="false" rtexprvalue="true"%>

<c:choose>
<c:when test="${not empty currency}">
    <%=HyphenString.formatDecimal(currency,amount)%>
</c:when>
<c:otherwise>
    <%=HyphenString.formatDecimal(amount)%>
</c:otherwise>
</c:choose>
