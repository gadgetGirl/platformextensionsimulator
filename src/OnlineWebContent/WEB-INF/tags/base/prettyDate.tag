<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag import="mammoth.utility.HyphenString"%>
<%@tag import="java.util.Date"%>
<%@tag import="java.lang.String"%>
<%@ attribute name="date" required="false" rtexprvalue="true" type="java.lang.Object"%>
<%@ attribute name="displayTime" required="false" rtexprvalue="true" type="java.lang.Boolean"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="label" required="false" rtexprvalue="true"%>
<%@ attribute name="noDiv" required="false" rtexprvalue="true"%>

<c:if test="${not empty label}"><div class="${className}"></c:if>
	<c:if test="${not empty date}">
		<c:if test="${not empty label}"><div class="prettyDateLabel">${label}</div></c:if>
		<c:if test="${noDiv != 'true'}"><div <c:if test="${not empty label}">class="prettyDateLabel"</c:if><c:if test="${empty label}">class="${className}"</c:if>></c:if>
					<%	
						String prettyDate = HyphenString.EMPTY_STRING;
						if (date instanceof java.lang.String) {
							prettyDate = HyphenString.formatPrettyDate((String)date);
						}
						else if (date instanceof java.util.Date) {
							prettyDate = HyphenString.formatPrettyDate((Date)date);
						}
					%>
					<%=prettyDate %>	
		<c:if test="${noDiv != 'true'}"></div></c:if>
	</c:if>
	
	<c:if test="${displayTime && not empty date}">
		<div class="${className}">
			<%String time = date instanceof java.lang.String ? HyphenString.formatTime((String)date) : HyphenString.EMPTY_STRING;%>	
			<%=time %>
		</div>
		
	</c:if>
<c:if test="${not empty label}"></div></c:if>
