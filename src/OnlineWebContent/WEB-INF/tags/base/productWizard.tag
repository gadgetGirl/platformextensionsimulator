<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="className" required="false"%>

<base:divContainer className="productWizard ${clasSName} clearfix">
	<jsp:doBody></jsp:doBody>
</base:divContainer>