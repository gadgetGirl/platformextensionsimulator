<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="switcherViewBean" required="true" rtexprvalue="true" type="fnb.online.tags.beans.tablecontrolsswitcher.Switcher"%>

<c:set var="setActive" value="true" />

<span class="tableControlsSwitcher" id="${switcherViewBean.id}_tableSwitcher">
	<span class="tableControlsSwitcherHolder">
		<c:forEach items="${switcherViewBean.actions}" var="action" >
			<a class="switchItem <c:if test="${setActive == 'true'}">active</c:if>" onclick="fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url:'${action.url}'}); return false;" >${action.label}</a>
			<c:set var="setActive" value="false" />
		</c:forEach>
	</span>
</span>