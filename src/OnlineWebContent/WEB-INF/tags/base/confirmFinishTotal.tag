<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<div class="tableRow tableDataRow totalRow clearfix ">
<div class="tableRowInner clearfix">
	<div class="tableGroup groupColCell grid100 clearfix">
		<div class="tableRowGroupInner">
			<jsp:doBody></jsp:doBody>
		</div>
	</div>
</div>
</div>