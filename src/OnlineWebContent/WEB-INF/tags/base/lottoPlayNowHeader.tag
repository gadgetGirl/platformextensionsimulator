<%@ attribute name="title" required="true" rtexprvalue="true"%>
<%@ attribute name="subTitle" required="true" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="buttons" required="false" rtexprvalue="true"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<div id="${id}" class="playNowHeaderTitle">
	<div id="playNowHeader_left" class="playNowHeader_left">
		<h3>${title}</h3>
		<p>${subTitle}</p>
	</div>
	<c:if test="${empty buttons}">
		<div id="playNowHeader_right" class="playNowHeader_right">
			<base:radioButtonContainer id="lottoPlus" name="lottoPlus">
				<base:radioButtonItem description="No" selected="true" value="false1"/>
				<base:radioButtonItem description="Yes" value="true"/>
			</base:radioButtonContainer>
		</div>
	</c:if>
</div>