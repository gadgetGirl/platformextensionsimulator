<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="id" required="false" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="width" required="false" %>
<%@ attribute name="onclick" required="false" %>
<%@ attribute name="style" required="false" %>
<c:choose>
<c:when test="${not empty width}">
	<c:set var="widthClass">grid${width}</c:set>
</c:when>
<c:otherwise>
	<c:set var="widthClass" value="grid100" />
</c:otherwise>
</c:choose>
<div <c:if test='${not empty id}'> id="${id}"</c:if> <c:if test='${not empty style}'> style="${style}"</c:if> <c:if test='${not empty onclick}'> onclick="${onclick}"</c:if> class="formTable ${widthClass} ${className}">
	<div class="tr ${widthClass}">
		<jsp:doBody />
	</div>
</div>