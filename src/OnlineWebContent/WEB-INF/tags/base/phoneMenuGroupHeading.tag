<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="className" required="false"%>
<%@ attribute name="id" required="false"%>
<%@ attribute name="heading" required="true"%>
<%@ attribute name="subText" required="false"%>
<div <c:if test="${not empty id}">id="${id}"</c:if> class="gridCol grid100 adPhoneContent groupHeading ${className}">
                <h4>${heading}</h4>
                <c:if test="${not empty subText}"><p>${subText}</p></c:if>
                <div class="groupNumber">
                  
                </div>
</div>


