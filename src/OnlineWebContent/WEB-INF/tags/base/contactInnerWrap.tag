<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<base:gridGroup>
	<base:gridCol width="80" className="teal8 padBottom10"><jsp:doBody /></base:gridCol>
	<base:gridCol width="20"></base:gridCol>
</base:gridGroup>