<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="groupIndex" required="false" rtexprvalue="true"%>
<%@ attribute name="downloadUrl" required="false" rtexprvalue="true"%>

<%@ attribute name="align" required="false" rtexprvalue="true"%>
<%@ attribute name="size" required="false" rtexprvalue="true"%>
<%@ attribute name="content" required="true" rtexprvalue="true"%>
<%@ attribute name="imgSrc" required="false" rtexprvalue="true"%>
<%@ attribute name="headerCustomContent" required="false" rtexprvalue="true"%>

<c:set var="skinNum" value="${not empty skin ? skin : 0}" />
<span class="item ${align}"<c:if test="${not empty downloadUrl}">onclick="fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url:'${downloadUrl}'})"</c:if>>

	<span class="hinner ${size}">
		<c:if test="${not empty imgSrc}">
		<img class="folderImg" src="/banking/04skins/${ skinNum == 13 ? 1 : skinNum }/folders/${imgSrc}.png" />
		</c:if>
		<span class="tableHinnerImage"></span><span class="tableHinnerContent<c:if test='${empty imgSrc}'> noGroupImg</c:if>">${content}</span>
	</span>
	<c:if test="${not empty downloadUrl&&groupIndex=='0'}">
		<base:formsButton id="downloadBtn" text="Download" className="download-w" target="tableDownload"/>
	</c:if>
	
</span>