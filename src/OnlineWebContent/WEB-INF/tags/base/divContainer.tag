<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false" name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="style"%>
<%@ attribute required="false" name="onclick"%>
<%@ attribute required="false" name="dataValue"%>

<div<c:if test='${not empty id}'> id="${id}"</c:if><c:if test='${not empty className}'> class="${className}"</c:if><c:if test='${not empty onclick}'> onclick="${onclick}"</c:if><c:if test='${not empty dataValue}'> data-value="${dataValue}"</c:if><c:if test='${not empty style}'> style="${style}"</c:if>>
	<jsp:doBody />
</div>