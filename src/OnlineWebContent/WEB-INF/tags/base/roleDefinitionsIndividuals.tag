<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<base:import type="css" link="/banking/01css_new/pages/loggedin/settings/cuac/UserMaintenanceLanding.css" />

<base:note id="roleDefButton" type="top">
			<base:button id="viewRoleDefinitions" icon="" className="tableAddButton printDisplayNone" label="View the role definitions here" onclick="fnb.functions.showHideToggleElements.showHideToggle({show:'true',element:'#roleDefinitions'});" />
		</base:note>
		<base:divContainer id="roleDefinitions" className="displayNone">
			<base:simpleTableContainer>
				<base:simpleTableRow id="tableHeadings" type="groupHeading">
					<base:gridCol width="70">
						Use the matrix to help you select the appropriate role
					</base:gridCol>
					<base:gridCol width="30">
						<base:button label="Close" className="closeDefinitions" onclick="fnb.functions.showHideToggleElements.showHideToggle({show:'false',element:'#roleDefinitions'});"></base:button>
					</base:gridCol>
				</base:simpleTableRow>
				
				<base:simpleTableRow id="subHeaders" type="tableHeading">
					<base:simpleTableCell type="headingCell" colNo="1">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="headingCell" colNo="2" className="alignCenter">
						Account Roles
					</base:simpleTableCell>
					<base:simpleTableCell type="headingCell" colNo="3" className="alignCenter">
						Customisable Permissions
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow type="tableHeading">
					<base:simpleTableCell type="headingCell" colNo="1">
						Functions you can perform
					</base:simpleTableCell>
					<base:simpleTableCell type="headingCell" colNo="2" className="alignCenter">
						<base:divContainer className="mobileHide">Viewer</base:divContainer>
						<base:divContainer className="mobileDisplay">
							<base:image src="/banking/03images/base/cuac/headingMobileViewer.PNG"></base:image>
						</base:divContainer>
					</base:simpleTableCell>
					<base:simpleTableCell type="headingCell" colNo="3" className="alignCenter">
						<base:divContainer className="mobileHide">Transactor</base:divContainer>
						<base:divContainer className="mobileDisplay">
							<base:image src="/banking/03images/base/cuac/headingMobileTransactor.PNG"></base:image>
						</base:divContainer>
					</base:simpleTableCell>
					<base:simpleTableCell type="headingCell" colNo="4" className="alignCenter">
						<base:divContainer className="mobileHide">Cardholder</base:divContainer>
						<base:divContainer className="mobileDisplay">
							<base:image src="/banking/03images/base/cuac/headingMobileCardholder.PNG"></base:image>
						</base:divContainer>
					</base:simpleTableCell>
					<base:simpleTableCell type="headingCell" colNo="5" className="alignCenter">
						<base:divContainer className="mobileHide">Sole Account Signatory</base:divContainer>
						<base:divContainer className="mobileDisplay">
							<base:image src="/banking/03images/base/cuac/headingMobileSoleCheque.PNG"></base:image>
						</base:divContainer>
					</base:simpleTableCell>
					<base:simpleTableCell type="headingCell" colNo="6" className="alignCenter">
						<base:divContainer className="mobileHide">Joint Account Signatory</base:divContainer>
						<base:divContainer className="mobileDisplay">
							<base:image src="/banking/03images/base/cuac/headingMobileJointCheque.PNG"></base:image>
						</base:divContainer>
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						View account via branch
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						View account via ATM
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						View account via personal electronic banking services
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Transact using card at a branch, ATM or point of sale
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Transact via personal electronic banking services
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Sole signing authority for cheques and other paper based payment instructions
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Joint signing authority for cheques and other paper based payment instructions
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Full Function Cheque Card*
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Limited Function Cheque Card*
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
				</base:simpleTableRow>
				
				<base:simpleTableRow>
					<base:simpleTableCell type="normalCell" colNo="1">
						Business Petrol Card*
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="2" className="alignCenter">
						&nbsp;
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="3" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="4" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="5" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
					<base:simpleTableCell type="normalCell" colNo="6" className="alignCenter">
						<base:image src="/banking/03images/base/cuac/logs_tick24.png"></base:image>
					</base:simpleTableCell>
				</base:simpleTableRow>
			</base:simpleTableContainer>
			<base:divContainer className="mobileHide">
				<base:equalHeightsRow className="marginBottom5">
					<base:equalHeightsColumn width="100" className="white">
						<base:heading value="*This functionality may also be allowed applicable card permission is given" level="3"></base:heading>
						<base:hyperlink className="detailedDefinitionLink printDisplayNone" href="${individualsCuacsRoleDefinitionsUrl}">View Detailed Role Definition</base:hyperlink>
					</base:equalHeightsColumn>
				</base:equalHeightsRow>
			</base:divContainer>
		</base:divContainer>