<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@tag import="mammoth.utility.HyphenString"%>
<%@ attribute name="amount" required="true" rtexprvalue="true" type="java.math.BigDecimal"%>
<%=HyphenString.formatDecimal(amount)%>
