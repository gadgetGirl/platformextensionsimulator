<%@tag import="fnb.online.tags.beans.table.TableTextItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="tableItem" required="true" rtexprvalue="true"
	type="fnb.online.tags.beans.table.TableTextItem"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true"
	type="fnb.online.tags.beans.table.TableColumnOptions"%>
<c:if test="${tableDoubleItem == true}">
   ${tableItem.text}
</c:if>
<c:if test="${tableDoubleItem == false and tableItem.processingResultViewBean != null}">
	
	<c:choose>
		<c:when test="${tableItem.processingResultViewBean.errorMessage != null && !tableItem.processingResultViewBean.errorMessage['empty'] && tableItem.processingResultViewBean.errorCode > 0}">
			<div id="${tableItem.id}" name="${tableItem.name}"
				class="tableCellItem ${columnOptions.size} badStatus">${tableItem.processingResultViewBean.errorMessage}</div>
		</c:when>
		<c:when test="${tableItem.processingResultViewBean.errorMessage != null && !tableItem.processingResultViewBean.errorMessage['empty'] && tableItem.processingResultViewBean.errorCode > 0}">
			<div id="${tableItem.id}" name="${tableItem.name}"
				class="tableCellItem ${columnOptions.size} goodStatus">${tableItem.processingResultViewBean.errorMessage}</div>
		</c:when>
		<c:when test="${tableItem.processingResultViewBean.errorCode == 0}">
			<div id="${tableItem.id}" name="${tableItem.name}"
				class="tableCellItem ${columnOptions.size} goodStatus">&nbsp;</div>
		</c:when>
		<c:otherwise>
			<div id="${tableItem.id}" name="${tableItem.name}"
				class="tableCellItem ${columnOptions.size} badStatus">&nbsp;</div>
		</c:otherwise>
	</c:choose>
</c:if>