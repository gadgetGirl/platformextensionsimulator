<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ tag import="fnb.online.tags.beans.AbstractViewBean"%>
<%@ tag import="java.util.Map"%>
<%@ tag import="fnb.online.tags.validation.handlers.ValidationHandler"%>

<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="false" name="id"%>
<%@ attribute required="false" rtexprvalue="false" name="showBig3"%>
<%@ attribute required="false" rtexprvalue="false" name="simulateLoggedOff" type="java.lang.Boolean"%>

<jsp:useBean id="validationBeans" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="technicalReference" class="java.lang.String" scope="request"/>
	
<script type="text/javascript">
    <%for(Object viewBean: validationBeans) {
        System.err.println("in jsp: viewBean : " + viewBean);
        Class viewBeanClass = (Class) viewBean;
        AbstractViewBean viewBeanInstance = (AbstractViewBean) viewBeanClass.newInstance();
        Map<String, ValidationHandler> handlers = viewBeanInstance.getFieldValidationHandlers();
        for(String fieldName: handlers.keySet()) {
            //System.err.println("in jsp: viewBean : " + viewBean + " fieldName: " + fieldName + " handler: " + handlers.get(fieldName));%>
            	console.log("fieldName: <%=fieldName%> handler: <%=handlers.get(fieldName).getClass().getSimpleName()%>");
                $("#<%=fieldName%>").attr("validationHandler","<%=handlers.get(fieldName).getClass().getSimpleName()%>");<%}}%>

	$('#support-reference').html('${technicalReference}');
</script>

<base:bnkActionBarHolder id="pageActionBar" />

<div<c:if test="${not empty id}"> id="${id}"</c:if> class="pageWrapper" data-skin="${skin}">
	<div id="main" class="pageGroup ${className}">
	
	<!--Dont not move or hnage the below img tag. It works-->
	<c:if test="${!empty skin}"><img id="printLogo" class="printLogo" src="/banking/04skins/${skin}/images/print-logo.jpg"/></c:if>
		<c:choose>
			<c:when test="${!empty subTabs}">
				<base:subTabsMenuHolder>
					<div id="subTabsPageHeader"></div>
					<script type="text/javascript">
						firstSubtab = "";
						selected = "";
					</script>
					<div class="subTabScrollLeft hidden" onclick="fnb.utils.mobile.subtabs.scrollLeft()"></div>
						<div id="subTabsContainer" class="subTabsContainer">
							<div id="subTabsScrollable" class="subTabsScrollable">
								<c:forEach items="${subTabs}" var="tab" varStatus="tabCount">
									<script type="text/javascript">
										if (firstSubtab == "") {
											firstSubtab = "${tab.parentFunctionRef}" + "_" + "${tab.childFunctionRef}";
										}
									</script>
									<c:if test="${tab.selected}">
										<base:subTabsMenu url="${tab.url}" text="${tab.label}" isActiveTab="true" />
									</c:if>
									<c:if test="${not tab.selected}">
										<base:subTabsMenu url="${tab.url}" text="${tab.label}" isActiveTab="false" isDisabled="${tab.disabled}" />
									</c:if>
								</c:forEach>
							</div>
						</div>
					<div class="subTabScrollRight hidden" onclick="fnb.utils.mobile.subtabs.scrollRight()"></div>
				</base:subTabsMenuHolder>
				<base:subTabsSection number="1"><jsp:doBody /></base:subTabsSection>
			</c:when>
			<c:otherwise>
				<base:subTabsSection className="noTabs" number="1">
				<jsp:doBody /></base:subTabsSection>
			</c:otherwise>
		</c:choose>
	</div>
</div>
<script text="text/javascript">

	if(_smallPort == true||_isMobile=="true"||_isMobile==true){
		fnb.utils.mobile.adjust($(window).width());
		//fnb.utils.mobile.properties.init();
		if(_isMobile){
			setTimeout(function () {
			  window.scrollTo(0, 1);
			}, 1000);
		}
	}
	
</script>
