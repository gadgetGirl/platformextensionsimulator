<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="id" required="true" rtexprvalue="true"%>
<%@ attribute name="tabLabel" required="true" rtexprvalue="true"%>
<%@ attribute name="link" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>

<li class="gridCol grid10 topTab<c:if test='${not empty className}'> ${className}</c:if>" data-value="${link}"><a>${tabLabel}</a></li>