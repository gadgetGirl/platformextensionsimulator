<%@ tag import="fnb.online.tags.beans.dropdown.*"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute required="false" rtexprvalue="true" name="dropdownBean" type="fnb.online.tags.beans.dropdown.AbstractDropdown" %>

<%@ attribute required="true"  rtexprvalue="true" name="id" %>
<%@ attribute required="false" rtexprvalue="true" name="name" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="selectedValue" %>		<%-- previously "value" --%>
<%@ attribute required="false" rtexprvalue="true" name="label" %>
<%@ attribute required="false" rtexprvalue="true" name="onchange" %>

<%--COLUMNS:										ATTRIBUTE NAME			 TYPE		DEFAULT	DESCRIPTION --%>
<%@ attribute required="false" rtexprvalue="true" name="type" %>		<%-- string		'singleTier', 'twoTier', 'threeTier' --%>
<%@ attribute required="false" rtexprvalue="true" name="overflow" %>	<%-- boolean	TRUE	 --%>
<%@ attribute required="false" rtexprvalue="true" name="labelWidth" %>	<%-- integer	40		multiples of 10 (10 >= labelWidth <= 90) --%>
<%@ attribute required="false" rtexprvalue="true" name="labelTop" %>	<%-- boolean	FALSE	if set to TRUE, labelWidth AND selectWidth are both automatically set to 100 --%>
<%@ attribute required="false" rtexprvalue="true" name="selectWidth" %>	<%-- integer	60		multiples of 10 (10 >= selectWidth <= 90) --%>
<%@ attribute required="false" rtexprvalue="true" name="color" %>		<%-- string		-		Options-> white, unique to singleTier --%>
<%@ attribute required="false" rtexprvalue="true" name="searchClass" %>	<%-- string		-		see available search classes for each tier below --%>
<%@ attribute required="false" rtexprvalue="true" name="margin" %>		<%-- string		-		used for adding margins/padding classes on the wrapping element --%>
<%@ attribute required="false" rtexprvalue="true" name="disabled" %>	<%-- boolean	FALSE	Enable drop-down again with JS _dropdown.enable('#dropdownToEnableId') --%>
<%@ attribute required="false" rtexprvalue="true" name="reverse" %>		<%-- boolean	TRUE	 --%>
<%--

NB:	It's currently not possible to add additional classNames to dropdownItems and then try to search by that className (eg. grouping)
		Ask Don if you need that functionality.
--%>

<c:choose>
	<c:when test="${type == 'threeTier'}">
<%-- ______________________________________ THREE ________________________________________
Optional search classes:
	dropdown-h1
	dropdown-h2
	dropdown-item-cell-left dropdown-h3
	dropdown-item-cell-right dropdown-h3
	dropdown-item-cell-left dropdown-h4
	dropdown-item-cell-right dropdown-h4
--%>
		<base:dropdownThreeTier
			id="${id}"
			name="${name}"
			className="${className}"
			label="${label}"
			labelWidth="${labelWidth}"
			labelTop="${labelTop}"
			selectWidth="${selectWidth}"
			searchClass="${searchClass}"
			selectedValue="${selectedValue}"
			onchange="${onchange}"
			margin="${margin}"
			disabled="${disabled}"
			reverse="${reverse}"
			dropdownBean="${dropdownBean}" ><c:if test="${empty dropdownBean}"><jsp:doBody /></c:if>
		</base:dropdownThreeTier>
	</c:when>
	<c:when test="${type == 'twoTier'}">
<%-- ______________________________________ TWO ________________________________________
Optional search classes:
	dropdown-h4
	dropdown-h5
 --%>
		<base:dropdownTwoTier
			id="${id}"
			name="${name}"
			className="${className}"
			label="${label}"
			labelWidth="${labelWidth}"
			labelTop="${labelTop}"
			selectWidth="${selectWidth}"
			searchClass="${searchClass}"
			selectedValue="${selectedValue}"
			onchange="${onchange}"
			margin="${margin}"
			disabled="${disabled}"
			reverse="${reverse}"
			dropdownBean="${dropdownBean}" ><c:if test="${empty dropdownBean}"><jsp:doBody /></c:if>
		</base:dropdownTwoTier>
	</c:when>
	<c:otherwise><!-- DEAFAULT type == 'singleTier' -->
<%-- ______________________________________ SINGLE ________________________________________ 
Optional search class:
	dropdown-h4
--%>
		<base:dropdownSingleTier
			id="${id}"
			name="${name}"
			className="${className}"
			label="${label}"
			labelWidth="${labelWidth}"
			labelTop="${labelTop}"
			selectWidth="${selectWidth}"
			searchClass="${searchClass}"
			selectedValue="${selectedValue}"
			color="${color}"
			onchange="${onchange}"
			margin="${margin}"
			disabled="${disabled}"
			reverse="${reverse}"
			dropdownBean="${dropdownBean}"><c:if test="${empty dropdownBean}"><jsp:doBody /></c:if>
		</base:dropdownSingleTier>
	</c:otherwise>
</c:choose>