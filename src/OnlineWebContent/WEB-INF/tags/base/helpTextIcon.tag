<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<c:set var="skinNum" value="${not empty skin ? skin : 0}" />
<c:set var="obeTablet">${sessionScope.SessionData["HyphenUser"].obeTablet}</c:set>

<c:choose>
	<c:when test="${obeTablet != true}">
		<a href="#" onclick="window.open('${helpURL}','','width=800,height=600,resizable=false,status=false,menubar=false');return false;">
			<img class="helpText" src="/banking/04skins/${ skinNum == 13 ? 1 : skinNum }/images/helpText.png" />
		</a>
	</c:when>
	<c:otherwise>
		<c:set var="href" value="ui://open.window(${helpURL},90,90,Banking Info)" />
		<c:set var="onclick" value="window.external.notify('${href}');" />
		<a href="#" onclick="${onclick}">
			<img class="helpText" src="/banking/04skins/${ skinNum == 13 ? 1 : skinNum }/images/helpText.png" />
		</a>
	</c:otherwise>
</c:choose>

