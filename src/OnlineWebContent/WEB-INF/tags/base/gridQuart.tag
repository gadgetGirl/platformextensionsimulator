<%@ attribute name="type" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="position" required="false" rtexprvalue="true"%>
<%@ attribute name="divide" required="false" rtexprvalue="true"%>
<%@ attribute name="custom" required="false" rtexprvalue="true"%>

<div class="gridCol gridQuart ${position} ${id} ${custom}">
	
	<div class="innerMargin ${divide} ">
	
	<jsp:doBody></jsp:doBody>
	
	</div>
	
</div>
