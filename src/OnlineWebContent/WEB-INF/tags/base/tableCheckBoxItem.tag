<%@ tag import="fnb.online.tags.beans.table.TableCheckBoxItem"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableCheckBoxItem"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="isTableObject" required="false" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="count" required="false" rtexprvalue="true" type="java.lang.String"%>


<c:choose>
	<c:when test="${tableDoubleItem == true}">
		<base:checkboxItem id="${tableItem.id}" name="${tableItem.id}" count="${count}" label="${tableItem.label}" value="${tableItem.value}" selected="${tableItem.selected}" uncheckedValue="${tableItem.uncheckedValue}" onclick="${columnOptions.onClick}" data="${tableItem.data}" disabled="${tableItem.disabled}"/>
	</c:when>
	<c:otherwise>
		<div id="${tableItem.id}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size}" >
			<base:checkboxItem isTableObject="${isTableObject}" id="${tableItem.id}" name="${tableItem.id}" count="${count}" label="${tableItem.label}" value="${tableItem.value}" selected="${tableItem.selected}" uncheckedValue="${tableItem.uncheckedValue}" onclick="${columnOptions.onClick}" data="${tableItem.data}" disabled="${tableItem.disabled}"/>
		</div>
	</c:otherwise>
</c:choose>