<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="true"  rtexprvalue="true" name="name"%>
<%@ attribute required="true"  rtexprvalue="true" name="action"%>
<%@ attribute required="true"  rtexprvalue="true" name="method"%>
<%@ attribute required="false" rtexprvalue="true" name="onsubmit"%>
<%@ attribute required="false" rtexprvalue="true" name="enctype"%>
<%@ attribute required="false" rtexprvalue="true" name="nav"%>
<%@ attribute required="false" rtexprvalue="true" name="target"%>

<form id="${name}" name="${name}" action="${action}" <c:if test="${not empty target}">target='${target}'</c:if> method="${method}"<c:if test="${not empty onsubmit}"> onsubmit="${onsubmit}"</c:if> <c:if test="${not empty enctype}"> enctype="${enctype}"</c:if>>
	<c:if test="${not empty nav}">
		<input type="hidden" name="nav" id="nav" value="${nav}" />
	</c:if>
	<jsp:doBody />
</form>