<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute required="true" rtexprvalue="true" name="authListViewBean" type="mammoth.jsp.viewbean.AuthorisationListViewBean" %>
<%@ attribute required="true" rtexprvalue="true" name="isHardCert" type="java.lang.Boolean" %>

<c:set var="appletUrl" value="/banking/mammoth/version_checking/synchronizer.jsp?${authListViewBean.toParamString()}"></c:set>
<base:divContainer id="appletDiv" className="grid100 displayNone">
	<base:equalHeightsRow>
		<base:equalHeightsColumn className="white" width="100">
			<c:choose>
				<c:when test="${isHardCert}">
					<base:heading level="2" value="Enter Token PIN" className="teal1"/>
					<base:input id="certificatePIN" type="password" label="PIN" value=""></base:input>
					<base:checkboxItem id="disclaimer" label="I have reviewed the items I am authorising and I confirm they are correct. By clicking the Accept button I acknowledge that I take full responsibility for the execution thereof." value="true" uncheckedValue="false" selected="false"></base:checkboxItem>
					<base:checkboxItem id="changePin" label="Do you want to change you token PIN?" value="true" uncheckedValue="false" selected="false" onclick="pages.fnb.authorisations.AuthorisationList.changePin();"></base:checkboxItem>
				</c:when>
				<c:otherwise>
					<base:heading level="2" value="Enter Certificate Password"  className="teal1" />
					<base:input id="certificatePath" type="text" label="Path" value="" maxlength="200"></base:input>
					<base:formsButton className="floatRight marginRight10" id="browse" text="Browse" type="button" onclick="pages.fnb.authorisations.AuthorisationList.browse();"></base:formsButton>
					<base:checkboxItem id="rememberPath" label="Remember Path" value="true" uncheckedValue="false" selected="false"></base:checkboxItem>
					<base:input id="certificatePassword" type="password" label="Password" value=""></base:input>
					<base:checkboxItem id="disclaimer" label="I have reviewed the items I am authorising and I confirm they are correct. By clicking the Accept button I acknowledge that I take full responsibility for the execution thereof." value="true" uncheckedValue="false" selected="false"></base:checkboxItem>
				</c:otherwise>
			</c:choose>
		</base:equalHeightsColumn>
	</base:equalHeightsRow>
</base:divContainer>
<base:divContainer id="appletDivChangePin"  className="floatLeft grid100 displayNone">
	<base:equalHeightsRow>
		<base:equalHeightsColumn className="white" width="100">
			<base:heading level="2" value="Enter new Token PIN"  className="teal1" />
			<base:input id="newPIN" type="password" label="New PIN" value=""></base:input>
			<base:input id="confirmPIN" type="password" label="Confirm PIN" value=""></base:input>
			<base:note >
				<base:paragraph>- The PIN must consist of 4 numbers </base:paragraph>
				<base:paragraph>- The PIN numbers must not be the same (e.g. 1111) </base:paragraph>
				<base:paragraph>- The PIN numbers must not be sequential (e.g. 1234 ) </base:paragraph>
			</base:note>
		</base:equalHeightsColumn>
	</base:equalHeightsRow>
</base:divContainer>
<base:divContainer id="appletDivLoading" className="grid100" style="text-align:center">
	<base:divContainer style="margin:0 auto;text-align:center;width:500px;">
	<base:heading id="appletDivLoadingHeading" level="2" value="Loading" className="extraLarge" />
	<base:paragraph>
		Please be patient...
	</base:paragraph>
	<base:image src="/banking/images/mua_progress_bar.gif"></base:image>
	</base:divContainer>
</base:divContainer>

<!-- cannot use display none as the applet does not load in IE8 -->
<base:divContainer  id="hiddenAppletDiv" style="height:1px,width:1px;">
</base:divContainer>

<script>
	setTimeout("pages.fnb.authorisations.AuthorisationList.loadApplet('${appletUrl}')",1000);
</script>


