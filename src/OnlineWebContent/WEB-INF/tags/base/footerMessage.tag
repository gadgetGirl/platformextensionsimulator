<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="id" required="true" type="java.lang.String" rtexprvalue="true"%>
<%@ attribute name="text" required="false" type="java.lang.String" rtexprvalue="true"%>
<%@ attribute name="total" required="false" type="java.lang.String" rtexprvalue="true"%>
<%@ attribute name="totalId" required="false" type="java.lang.String" rtexprvalue="false"%>
<%@ attribute name="textId" required="false" type="java.lang.String" rtexprvalue="false"%>

<c:if test="${empty totalId}"><c:set var="totalId" value="footerMessageTotal"></c:set></c:if>
<c:if test="${empty textId}"><c:set var="textId" value="footerMessageText"></c:set></c:if>
<div class="footerMessage hideElement" id="${id}" >
	<div class="footerMessageInner">
	<div class="footerMessageInnerTwo">
	<div id="${totalId}" class="footerMessageTotal">${total}</div>
	<div id="${textId}" class="footerMessageText">${text}</div>
	<jsp:doBody/>
	</div>
	</div>
</div>
