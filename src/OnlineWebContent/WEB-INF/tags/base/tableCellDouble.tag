<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="align" required="false" rtexprvalue="true"%>
<%@ attribute name="size1" required="false" rtexprvalue="true"%>
<%@ attribute name="size2" required="false" rtexprvalue="true"%>
<%@ attribute name="content1" required="true" rtexprvalue="true"%>
<%@ attribute name="content2" required="true" rtexprvalue="true"%>
<%@ attribute name="label" required="false" rtexprvalue="true"%>



<span class="double item">
<span class="rinnerDouble">

<c:if test="${not empty label}"><span class="phoneLabel">${label}</span></c:if>

			<span class="cellContent">
				<span class="${size1}">${content1}</span>
				<span class="${size2}">${content2}</span>
			</span>
			
</span>
</span>






