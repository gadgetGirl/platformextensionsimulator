<%@tag import="fnb.online.tags.beans.table.TableTextItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="text" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<div class="headerControlLabel ${className} gridCol grid10">
	<div class="labelInner">${text}</div>
</div>
