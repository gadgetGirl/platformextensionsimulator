<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="max" required="true" rtexprvalue="true"%>
<%@ attribute name="addButtonLabel" required="true" rtexprvalue="true"%>
<%@ attribute name="removeButtonLabel" required="true" rtexprvalue="true"%>
<script>
	$(function() {
		function ContentCloner() {
			this.parentContainer;
			this.widgetContainer;
			this.elementCount = 0;
			this.protoypeElement;
			this.maxItems;
			
		}
		ContentCloner.prototype = {
			init : function(container,clone,max) {
				var parent = this;
				parent.maxItems = max;
				parent.parentContainer = $(container);
				parent.widgetContainer = parent.parentContainer.find('#widgetContainer');
				parent.protoypeElement = $(clone).clone();
				parent.bindEvents(parent.parentContainer);							
			},
			cloneContent: function() {
				var parent = this;
				parent.elementCount++;
				var clonedElement = parent.protoypeElement;
				if(parent.elementCount == parent.maxItems-1) parent.disableBtt($('#addRecipientExtender').find('.addRecipientsAddButtonWrapper'));
				if(parent.elementCount > 0) parent.widgetContainer.find('.addRecipientRow:first-child').find('.addRecipientsRemoveButton').removeClass('displayNone');	
				clonedElement = $(parent.recreateFormIDs(clonedElement,parent.elementCount));
				clonedElement.find('.addRecipientsRemoveButton').removeClass('displayNone');
				parent.widgetContainer.append(clonedElement);
			},
			disableBtt: function(button){

				button.addClass('displayNone');

			},
			enableBtt: function(button){

				button.removeClass('displayNone');

			},
			removeContent: function(index){
				var parentObject = this;
				$('#addRecipientRow_nbr'+index).remove();
				parentObject.elementCount--;
				parentObject.reIndexClones();
				if(parentObject.elementCount < 1){
					$(parentObject.widgetContainer).find('.addRecipientRow:first-child').find('.addRecipientsRemoveButton').addClass('displayNone')
				}
				if(parentObject.elementCount < 9){
					parentObject.enableBtt($('#addRecipientExtender').find('.addRecipientsAddButtonWrapper'))
				}
				
			},
			bindEvents: function(container) {
				var parentObject = this;
				if(_isMobile){
					$(container).on('touchend', '.addRecipientsRemoveButtonLink', function(event) {
						var cloneIndex = $(event.target).attr('id').split('_nbr')[1];	
						parentObject.removeContent(cloneIndex);
					});
					$('#addRecipientExtender').on('touchend', '.addRecipientsAddButtonWrapper', function(event) {
						parentObject.cloneContent();
					});
				}else{
					$(container).on('click', '.addRecipientsRemoveButtonLink', function(event) {
						var cloneIndex = $(event.target).attr('id').split('_nbr')[1];	
						parentObject.removeContent(cloneIndex);
					});
					$('#addRecipientExtender').on('click', '.addRecipientsAddButtonWrapper', function(event) {
						parentObject.cloneContent();
					});
				}
				
			},
			reIndexClones: function() {

			var parentObject = this;
			var container = parentObject.widgetContainer;
			var newHTML = [];
		
			$(container.find('.addRecipientRow')).each(function(index){
				var element = $(this);
				var newElement = $(parentObject.recreateFormIDs(element,index));
				newHTML.push(newElement);
			})
			container.html('');
			container.append(newHTML)
			parentObject.elementCount = newHTML.length -1; 

			},

			recreateFormIDs: function(rowObject,newindex){

			var objectInputValues=[];
			var objectInputs = $(rowObject).find('input')
			$(objectInputs).each(function(){
				objectInputValues.push($(this).val());
			});
			
			var parentRow = rowObject.outerHTML();
			var newRow = parentRow.replace(/_nbr(\d+)/g, "_nbr" + newindex)
			var newRowObject = $(newRow);
			var newInputs = newRowObject.find('input');
			$(newInputs).each(function(index){
				$(this).attr('value',objectInputValues[index]);
			})
	
			newRowObject = $(newRowObject).outerHTML();

			return newRowObject;
			
			}
		};

		namespace("fnb.utils.ContentCloner", ContentCloner);
	});
</script>
		<div id="addRecipientExtender" class="addRecipientExtender clearfix">
				<c:choose>
					<c:when test="${!empty stopChequeViewBeanList}">
					<div id="addRecipientRowParent" class="addRecipientRowParent">
					<c:forEach items="${stopChequeViewBeanList}" var="cheque" varStatus="itemIndex">
						
							<base:equalHeightsRow>
								<base:equalHeightsColumn width="100" className="white" id="widgetContainer">
									<base:gridGroup id="addRecipientRow_nbr${itemIndex.count-1}" className="addRecipientRow borderBottomGrey">
										<base:gridCol width="50">
											<base:input label="From:" value="${cheque.fromChequeNumber}" labelPosition="labelLeft" name="fromChequeNumber" id="fromChequeNumber_nbr${itemIndex.count-1}" maxlength="10" className="fromChequeNumber"/>
										</base:gridCol>
										<base:gridCol width="50">
											<base:input label="To:" value="${cheque.toChequeNumber}" labelPosition="labelLeft" name="toChequeNumber" id="toChequeNumber_nbr${itemIndex.count-1}" maxlength="10"/>	
										</base:gridCol>
										<base:gridCol width="50">
											<base:dropdownSingleTier name="multipleReasons"  id="multipleReasons_nbr${itemIndex.count-1}" label="Reason${inputItemStatus.index}" dropdownBean="${cheque.singleTierDropDown}"></base:dropdownSingleTier>
										</base:gridCol>
										<base:gridCol width="50">
										</base:gridCol>
										<base:gridCol width="100">
											<div class="addRecipientsRemoveButton">
												<a id="addRecipientsRemoveButton_nbr${itemIndex.count-1}" class="addRecipientsRemoveButtonLink">${removeButtonLabel}</a>
											</div>
										</base:gridCol>
									</base:gridGroup>
								</base:equalHeightsColumn>
							<base:equalHeightsColumn ghostBlock="true" />
							</base:equalHeightsRow>
					
						</c:forEach>
						</div>
						</c:when>
						<c:otherwise>
							<div id="addRecipientRowParent" class="addRecipientRowParent">
							<base:equalHeightsRow>
								<base:equalHeightsColumn width="100" className="white" id="widgetContainer">
									<base:gridGroup id="addRecipientRow_nbr0" className="addRecipientRow borderBottomGrey">
										<base:gridCol width="50">
											<base:input label="From:" labelPosition="labelLeft" name="fromChequeNumber" id="fromChequeNumber_nbr0" maxlength="10" className="fromChequeNumber"/>
										</base:gridCol>
										<base:gridCol width="50">
											<base:input label="To:" labelPosition="labelLeft" name="toChequeNumber" id="toChequeNumber_nbr0" maxlength="10"/>	
										</base:gridCol>
										<base:gridCol width="50">
											<base:dropdownSingleTier name="multipleReasons"  id="multipleReasons_nbr0" label="Reason${inputItemStatus.index}" dropdownBean="${multipleReasonDropDown}"></base:dropdownSingleTier>
										</base:gridCol>
										
										<base:gridCol width="50">
										</base:gridCol>
										<base:gridCol width="100">
											<div  class="addRecipientsRemoveButton displayNone">
												<a id="addRecipientsRemoveButton_nbr0" class="addRecipientsRemoveButtonLink">${removeButtonLabel}</a>
											</div>
										</base:gridCol>
									</base:gridGroup>
								</base:equalHeightsColumn>
							<base:equalHeightsColumn ghostBlock="true" />
							</base:equalHeightsRow>
						</div>
						</c:otherwise>
						
						</c:choose>
						
			<base:equalHeightsRow>
				<base:equalHeightsColumn width="100">
					<div class="addRecipientsAddButtonWrapper">
						<div class="addRecipientsAddButton">
							<a class="addRecipientsAddButtonLink">${addButtonLabel}</a>
						</div>
					</div>
				</base:equalHeightsColumn>
				<base:equalHeightsColumn ghostBlock="true" />
			</base:equalHeightsRow>
	</div>
	<script>
	namespace('utils.fnb.ContentCloner',new fnb.utils.ContentCloner());
	utils.fnb.ContentCloner.init('#addRecipientRowParent','#addRecipientRow_nbr0','${max}');
</script>





