<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="fnb.hyperion.banking.forms" tagdir="/WEB-INF/tags/banking/forms"%>

<%@ attribute required="false"  rtexprvalue="true" name="url"%>
<%@ attribute required="false"  rtexprvalue="true" name="className"%>
<%@ attribute required="true"  rtexprvalue="true" name="text"%>
<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="sourceId"%>
<%@ attribute required="false" rtexprvalue="true" name="target"%>
<%@ attribute required="false" rtexprvalue="true" name="firstInRow"%>
<%@ attribute required="false" rtexprvalue="true" name="number"%>
<%@ attribute required="false" rtexprvalue="true" name="disabled"%>
<%@ attribute required="false" rtexprvalue="true" name="onclick"%>
<%@ attribute name="newFrameWorkFlag"  required="false"%>


<c:set var="idClass"><c:if test="${not empty id}">${id}_eziLink</c:if></c:set>
<c:set var="firstClass" value="${firstInRow ? 'first' : ''}"/>


<c:set var="event" value="loadPage"/>

<c:if test="${empty onclick}">
<c:set var="onclick">
	<c:choose>
		<c:when test="${disabled == 'true'}">
		</c:when>
		<c:when test="${target == 0}">
			fnb.controls.controller.eventsObject.raiseEvent('loadUrlToWorkspace', {url: '${url}'});
			<c:set var="event" value="loadPage"/>
		</c:when>
		<c:when test="${target == 3}">
			fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '${url}'});
			<c:set var="event" value="loadEzi"/>
		</c:when>
		<c:when test="${target == 4}">
			fnb.functions.loadUrlToPrintDiv.load('${url}');
		</c:when>
		<c:when test="${target == 5}">
			fnb.controls.controller.eventsObject.raiseEvent('doDownload','${url}');
			<c:set var="event" value="download"/>
		</c:when>
		<c:when test="${target == 6}">
			fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '${url}');
			<c:set var="event" value="loadPopup"/>
		</c:when>
		<c:when test="${target == 7}">
			fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrl', '${url}');
			<c:set var="event" value="loadPopup"/>			
		</c:when>
		<c:when test="${target == 9}">
			fnb.controls.controller.eventsObject.raiseEvent('tableMoreOptions', '${url}')
		</c:when>
		
		
	</c:choose>
</c:set>
</c:if>

	<c:choose>
		<c:when test="${newFrameWorkFlag == 'true'}">
			<%--<span class="eziLink btt_${number} ${idClass} ${firstClass} ${className} ${disabled == 'true' ? 'disabled' : ''}" onclick="${onclick}">${text}-new ${newFrameWorkFlag}</span> --%>
			<span data-type="button" class="eziLink btt_${number} ${idClass} ${firstClass} ${className} ${disabled == 'true' ? 'disabled' : ''}" <c:if test="${not empty id}"> id="${id}"</c:if> data-settings='[{"url":"${url}" , "event" : "${event}"}]'>
				<span  >${text}</span>
			</span>
		</c:when>
		<c:otherwise>
        	<span class="eziLink btt_${number} ${idClass} ${firstClass} ${className} ${disabled == 'true' ? 'disabled' : ''}" onclick="${onclick}">${text}</span>
        </c:otherwise>
	</c:choose>





