<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="pageTitle" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="metaDescriptionContent" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="metaKeywordContent" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="javascript" required="false" rtexprvalue="true"%>

<!doctype html>
<html>
<head>

<title>${pageTitle}</title>

<meta name="description" content="${metaDescriptionContent}" />
<meta name="keywords" content="${metaKeywordContent}" />

<c:if test="${!empty javascript}">
	<base:import type="js" link="/banking/02javascript/pages/${javascript}"/>
</c:if>

</head>

	<base:body>

		<jsp:doBody></jsp:doBody>

	</base:body>

</html>
