<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="className" required="false"%>
<base:gridGroup id="matrix" className="white ${clasSName}">
	<base:divContainer className="matrixBorders clearfix">
		<jsp:doBody></jsp:doBody>
	</base:divContainer>
</base:gridGroup>
