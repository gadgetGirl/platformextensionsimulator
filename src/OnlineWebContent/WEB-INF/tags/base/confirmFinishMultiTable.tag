<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="id" required="false"%>

<div <c:if test="${not empty id}">id="${id}"</c:if> class="confirmFinishMultiTable clearfix">
	<jsp:doBody></jsp:doBody>
</div>