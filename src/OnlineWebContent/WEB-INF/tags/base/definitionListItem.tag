<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute required="true"  rtexprvalue="true" name="description" %>
<%@ attribute required="true"  rtexprvalue="true" name="value" %>
<%@ attribute required="false" rtexprvalue="true" name="value2" %>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="width" %>
<%@ attribute required="false" rtexprvalue="true" name="ignoreValue2Title" %>
<%@ attribute required="false" rtexprvalue="true" name="tooltipContent" %>

<c:set var="titleWidth" value="${(empty width) ? '40' : width}" />
<c:set var="dataWidth" value="${(empty width) ? '60' : 100-width}" />

<div class="dlItemWrapper grid100  formElementWrapper clearfix">
<div class="dlTitle gridCol grid${titleWidth} ${className} formElementLabel">${description}</div>
	<div class="dlData gridCol grid${dataWidth} ${className} formElementContainer">
	<c:choose>
		<c:when test="${not empty tooltipContent}">
			<div class="tooltipFix">${value}<jsp:doBody /></div><base:tooltip content="${tooltipContent}"/>
		</c:when>
		<c:otherwise>
			${value}<jsp:doBody />
		</c:otherwise>
	</c:choose>
	</div>
<c:if test="${not empty value2}">
	<c:if test="${ignoreValue2Title != true}"><div class="dlTitle gridCol grid${titleWidth} dlTitleEmpty ${className}">&nbsp;</div></c:if>
	<div class="dlData gridCol grid${dataWidth} ${className} formElementContainer">${value2}</div>
</c:if>
</div>