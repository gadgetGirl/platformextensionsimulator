<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute required="false" name="className"%>

<span<c:if test='${not empty className}'> class="${className}"</c:if>>
	<jsp:doBody />
</span>