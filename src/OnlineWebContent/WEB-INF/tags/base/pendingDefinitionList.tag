<%@ tag import="java.util.List"%>

<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute required="false" rtexprvalue="true" name="name"%>
<%@ attribute required="false" rtexprvalue="true" name="className" %>
<%@ attribute required="false" rtexprvalue="true" name="list" type="java.util.List"%>
<%@ attribute required="true" rtexprvalue="true" name="description"%>
<%@ attribute required="true" rtexprvalue="true" name="value"%>

<c:forEach var="item" items="${list}">
	<c:if test="${item eq name}">
		<c:set var="className" value="${className} pending" />
	</c:if>
</c:forEach>

<base:definitionList>
	<base:definitionListItem description="${description}" value="${value}" className="${className}" />
</base:definitionList>