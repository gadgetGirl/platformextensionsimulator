<%@ attribute name="id" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="className" required="false" rtexprvalue="true" type="java.lang.String"%>
<div id="${id}" class="tableList simpleTable ${className}">
	<div class="tableContainer">
	 <jsp:doBody/>
	</div>
</div>
