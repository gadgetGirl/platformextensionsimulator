<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="className" required="false"%>
<%@ attribute name="id" required="false"%>

<ul <c:if test="${not empty id}">id="${id}"</c:if> class="teal6 ${className}">
  <jsp:doBody></jsp:doBody>
</ul>


