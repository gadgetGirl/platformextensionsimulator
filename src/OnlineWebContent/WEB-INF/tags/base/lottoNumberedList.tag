<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="detailsBean" required="false" rtexprvalue="true" type="fnb.online.mammoth.buy.lotto.LottoDetailsViewBean" %>
<%@ attribute name="currency" required="true" rtexprvalue="true"%>

<div class="lottoBoardlist" id="lottoBoardlist">
	
	<c:if test="${not empty detailsBean.winners}">
		<c:forEach var="winnerItem" items="${detailsBean.winners}" varStatus="winnerItemIndex">
			<div class="numberedlistItem">
				<div class="numberedlistItemNumber"><div class="numberedlistItemNumberValue">${winnerItemIndex.index+1}</div></div>
				<div class="numberedlistItemContent">
					<input id="contact_${winnerItemIndex.index+1}" value="" type="hidden">
					<div class="numberedlistItemContentValue">
						<c:choose>
							<c:when test="${empty winnerItem.people}">No Winners</c:when>
							<c:otherwise>${winnerItem.people} people won ${currency}${winnerItem.amount} each.</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</c:forEach>
	</c:if>
	
</div>