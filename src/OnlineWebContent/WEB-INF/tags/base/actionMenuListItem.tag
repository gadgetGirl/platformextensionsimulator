<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="id" required="false" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="headerText" required="false" %>
<%@ attribute name="commentText" required="false" %>
<%@ attribute name="onclick" required="false" %>

<div class="actionListLink ${className}"<c:if test='${not empty id}'> id="${id}"</c:if><c:if test='${not empty onclick}'> onclick="${onclick}"</c:if>>
	<c:if test="${not empty headerText}"><span class="actionListText">${headerText}</span></c:if>
	<c:if test="${not empty commentText}">
		<span class="actionListComment">${commentText}</span>
	</c:if>
	<jsp:doBody />
</div>