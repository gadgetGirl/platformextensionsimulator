<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<base:gridCol width="60">
	<base:gridGroup className="marginBottom20">
		<base:gridCol width="20">&nbsp;</base:gridCol>
		<base:gridCol width="80"><jsp:doBody /></base:gridCol>
	</base:gridGroup>
</base:gridCol>