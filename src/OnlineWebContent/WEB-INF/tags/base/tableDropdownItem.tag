<%@tag import="fnb.online.tags.beans.table.TableRadioButtonItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib  prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableDropdownItem"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="count" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="onChange" required="false" rtexprvalue="true" type="java.lang.String"%>

<c:choose>
	<c:when test="${tableDoubleItem == true}">
		<base:dropDown id="" name="" dropdown="${tableItem.dropDown}" />
	</c:when>
	<c:otherwise>
		<div id="${tableItem.id}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size} overflowOff">
			<base:dropdownSingleTier onchange="${onChange}" className="margin50" id="${tableItem.id}${count}" name="${tableItem.name}${count}" dropdownBean="${tableItem.dropDown}" />
		</div>
	</c:otherwise>
</c:choose>