<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>

<%@ attribute required="false" rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="description"%>
<%@ attribute required="false" rtexprvalue="true" name="radioItemViewBean"%>
<%@ attribute required="false" rtexprvalue="true" name="value"%>
<%@ attribute required="false" rtexprvalue="true" name="selectedValue"%>
<%@ attribute required="false" rtexprvalue="true" name="selected"%>
<%@ attribute required="false" rtexprvalue="true" name="onchange"%>
<%@ attribute required="false" rtexprvalue="true" name="onclick"%>
<%@ attribute required="false" rtexprvalue="true" name="mobi"%>
<%@ attribute required="false" rtexprvalue="true" name="disabled"%>

<div id="switcherWrapper${id}" class="switcherWrapper<c:if test="${selectedValue == value || selected == 'true'}"> switcherWrapperSelected </c:if><c:if test="${disabled == 'true'}"> disabled</c:if>">
	<c:if test='${mobi}'><div class="mobi-dropdown-trigger" onclick="fnb.utils.mobile.switcher.expandSwitcher(this);"></div></c:if>
	<div id="${id}" class="radioButton ${className}"
	<c:if test='${not empty value}'> data-value="${value}"</c:if>
	<c:if test='${not empty onclick && empty disabled}'> onclick="${onclick}"</c:if>>${description}</div>
</div>