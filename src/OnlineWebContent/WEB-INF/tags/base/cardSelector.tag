<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>


<%@ attribute required="true"  rtexprvalue="true" name="id"%>
<%@ attribute required="false" rtexprvalue="true" name="name"%>
<%@ attribute required="false" rtexprvalue="true" name="label"%>
<%@ attribute required="false" rtexprvalue="true" name="value"%>
<%@ attribute required="false" rtexprvalue="true" name="className"%>
<%@ attribute required="false" rtexprvalue="true" name="description"%>

<!-- First Display ----------------------------------------------------------------------------------------------------------------------------------- -->
<div id="selectorFirstImageHolder">
<c:forEach var="cardDesign" items="${cardDesigns}" varStatus="loopCount">
	<c:choose>
		<c:when test="${empty value}">
			<c:if test="${loopCount.count eq 1}">
				<div class="${className}" id="${id}">
					<img class="" id="selectorFirstImage" src="${cardDesign.url}" onclick="fnbSelectionPickerObject.show();">
					<input type="hidden" id="${id}" class="selectorFirstField" name="${name}" value="${cardDesign.key}" >
				</div>
			</c:if>
		</c:when>
    	<c:otherwise>
    		<c:if test="${cardDesign.key == value}" >
				<div class="${className}" id="${id}">
					<img class="" id="selectorFirstImage" src="${cardDesign.url}" onclick="fnbSelectionPickerObject.show();">
					<input type="hidden" id="${id}" class="selectorFirstField" name="${name}" value="${cardDesign.key}" >
				</div>
			</c:if>
    	</c:otherwise>
    </c:choose>
    <c:set var="cardLoopCount" value="${loopCount.count}" />
</c:forEach>
<base:divContainer id="mobileDeviceOfferLaunchButtonHolder" className="">
	<base:radioButtonItem description="Select" className="amber1 borderBottomWhite" id="mobileDeviceOfferLaunchButton" value="mobileDeviceOfferLaunchButton" selectedValue="" onclick="fnbSelectionPickerObject.show();" />
</base:divContainer>
</div>
<br/>

<!-- Pop up Display ----------------------------------------------------------------------------------------------------------------------------------- -->
<div  id="selectorHolder"  class="">
	<div class="selectorHolderMiddle teal7">&nbsp;</div>
	
	<div id="backgroundHolder">
		<!-- Scroller Left div (width 60) -->
		<div id="selectorScrollerLeft" onclick="fnbSelectionPickerObject.scrollRight();"><div id="selectorScrollerLeftImage"></div></div>
		<!-- Scroller Right div -->
		<div id="selectorScrollerRight" onclick="fnbSelectionPickerObject.scrollLeft(${cardLoopCount});"><div id="selectorScrollerRightImage"></div></div>
		
		<!-- Main body (cards) here -->
		<div id="selectorHolderBody" class="">
		
	        <c:forEach var="cardDesign" items="${cardDesigns}">
		        <div id="cardImageHolder" class="">
					<img class="cardImageLayout" id="img${cardDesign.key}" src="${cardDesign.url}">
					<input type="hidden" id="${cardDesign.key}" name="${cardDesign.key}" value="${cardDesign.key}" >
					<div class="selectorDeviceHolderBodyProductButton13" id="selectorDeviceHolderBodyProductButton">
						<base:divContainer id="mobileDeviceOfferLaunchButtonHolder" className="borderTopWhite borderRightWhite">
							<base:radioButtonItem description="Change" className="amber1" id="mobileDeviceOfferLaunchButton" value="mobileDeviceOfferLaunchButton" selectedValue="" onclick="fnbSelectionPickerObject.pickCard('${cardDesign.key}');" />
						</base:divContainer>
					</div>
				</div>
			</c:forEach>
			
		</div>
	</div>

</div>

