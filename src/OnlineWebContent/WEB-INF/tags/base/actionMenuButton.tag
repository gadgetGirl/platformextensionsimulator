<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute required="true" name="id"%>
<%@ attribute required="true" name="label"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="onclick"%>
<%@ attribute required="false" name="type" description="set to 'none' to disable icon"%>

<%-- Button normally used on the action menus and eZi panels --%>

<%-- set the default TYPE. Pass type='none' to disable it --%>
<c:set var="type" value="${empty type ? 'actionMenuButtonIcon' : type}"/>
<c:set var="type" value="${type == 'none' ? '' : type}"/>

<%-- set the default CLASS --%>
<c:choose>
	<c:when test="${(empty className) || (className == 'actionMenuButton')}">
		<c:set var="className" value="actionMenuButton" />
	</c:when>
	<c:otherwise>
		<c:set var="className" value="${className} actionMenuButton" />
	</c:otherwise>
</c:choose>

<div id="${id}" class="${className}"<c:if test='${not empty onclick}'> onclick="${onclick}"</c:if>>
	<c:if test='${not empty type}'><div class="${type}"></div></c:if>
	<div class="actionMenuButtonIconText">${label}</div><jsp:doBody/>
</div>