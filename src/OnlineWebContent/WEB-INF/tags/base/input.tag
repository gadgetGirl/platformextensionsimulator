<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>
<%@ attribute required="true" name="id"%>
<%@ attribute required="false" name="name"			description="If ID and NAME are the same, pass only ID"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="type"			description="Possible Types: text, hidden, phone, date, currency, number, password, pin"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="labelWidth"	description="DEPRECATED"%>
<%@ attribute required="false" name="labelMultiline" description="DEPRECATED. If label text wraps to 2 or more lines set attribute to 'true'"%>
<%@ attribute required="false" name="labelTop"		description="DEPRECATED"%>
<%@ attribute required="false" name="labelPosition"	description="DEPRECATED"%>
<%@ attribute required="false" name="inputWidth"	description="DEPRECATED"%>
<%@ attribute required="false" name="value"%>
<%@ attribute required="false" name="defaultValue"%>
<%@ attribute required="false" name="defaultCode"%>
<%@ attribute required="false" name="totaling"%>
<%@ attribute required="false" name="onclick"%>
<%@ attribute required="false" name="onkeyup"%>
<%@ attribute required="false" name="onchange"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="readonly"%>
<%@ attribute required="false" name="maxlength"%>
<%@ attribute required="false" name="noteContent"%>
<%@ attribute required="false" name="tooltipContent"%>
<%@ attribute required="false" name="updateTotal"%>
<%@ attribute required="false" name="isTableObject"%>
<%@ attribute required="false" name="decimal"		description="When using type CURRENCY, specify the number of decimal place to show. DEFAULT is 2"%>
<%@ attribute required="false" name="autocomplete"	description="HTML attribute. Automatically added to PIN and PASSWORD fields. Valid values are: 'on' or 'off' NOT boolean values"%>

<c:set var="name" value="${not empty name ? name : id}" />
<c:set var="hidden" value="${(type == 'hidden') ? 'hidden' : ''}" />
<c:set var="default" value="${value}" />

<%-- Don't wrap the hidden input in a bunch of DIVs --%>
<c:choose>
<c:when test="${type == 'hidden'}">
	<input id="${id}" name="${name}" type="hidden" class="hidden" value="${default}"<c:if test="${not empty default}"> data-value="${default}"</c:if> />
</c:when>
<c:otherwise>

	<%-- <c:set var="labelMultilineClass" value="${ labelMultiline ? 'labelMultiline' : '' }" /> --%>
	<c:if test="${type == 'currency'}">
		<c:choose>
			<c:when test='${not empty value}'>
				<c:set var="default" value="${fn:replace(value, ' ', '')}" />
			</c:when>
			<c:when test='${not empty defaultValue}'>
				<c:set var="default" value="${fn:replace(defaultValue, ' ', '')}" />
			</c:when>
			<c:otherwise>
				<c:set var="default" value="0.00" />
			</c:otherwise>
		</c:choose>
		<c:set var="decimal" value="${not empty decimal ? decimal : 2}" />
	</c:if>
	
	<c:if test="${type == 'phone'}">
		<c:if test="${empty defaultCode}"><c:set var="defaultCode" value="Code" /></c:if>
		<c:if test="${empty default}"><c:set var="default" value="Number" /></c:if>
	</c:if>
	
	<c:set var="type" value="${empty type ? 'text' : type}" />
	<c:set var="default" value="${empty default ? '' : default}" />
	<c:set var="totaling" value="${empty totaling ? false : totaling}" />
	<c:set var="disabled" value="${empty disabled || disabled == false ? '' : true}" />
	<c:set var="containerWidth" value="${empty label ? 'grid100' : 'grid60'}" />
	<c:set var="dynamicBorder" value="${empty label ? 'formContainerBorder' : ''}" />
	<c:set var="maxlength" value="${not empty maxlength ? maxlength : '30'}" />
	<c:set var="isPassOrPin" value="${ type == 'pin' || type == 'password' ? true : false}" /><%-- prevent duplicate rendering of autocomplete attribute --%>

	<%-- Attributes and classes added to input element. Set here to improve readability --%>
	<c:set var="inputClasses">
		<c:if test="${type == 'currency'}">currencyInput</c:if>
		<c:if test="${type == 'phone'}">phoneNumber numberOnly</c:if>
		<c:if test="${type == 'numberOnly'}">numberOnly</c:if>
		<c:if test="${type == 'number' || type2 == 'number' || type == 'pin'}">number</c:if>
	</c:set>
	<c:set var="inputAttributes">
		<c:if test="${not empty onclick}">onclick="${onclick}"</c:if>
		<c:if test="${not empty onkeyup}">onkeyup="${onkeyup}"</c:if>
		<c:if test="${not empty onchange}">onchange="${onchange}"</c:if>
		<c:if test="${not empty disabled}">disabled="disabled"</c:if>
		<c:if test="${not empty readonly}">readonly="readonly"</c:if>
		<c:if test="${not empty default}">data-value="${default}"</c:if>
		<c:if test="${not empty autocomplete && not isPassOrPin}">autocomplete="${autocomplete}"</c:if>
		<c:if test="${isPassOrPin}">type="password" autocomplete="off"</c:if>
		<c:if test="${type == 'pin'}">pattern="[0-9]*"</c:if>
		<c:if test="${type == 'currency'}">data-decimal="${decimal}" pattern="[0-9.]*"</c:if>
		<c:choose>
			<c:when test="${type == 'phone'}">
				maxlength="10"
			</c:when>
			<c:otherwise>
				maxlength="${maxlength}"
			</c:otherwise>
		</c:choose>
	</c:set>

	<c:choose>
		<c:when test="${empty isTableObject}">
			<div class="formElementWrapper input-wrapper clearfix ${className} ${hidden} ${dynamicBorder}">
				<c:if test="${not empty label}">
					<div class="gridCol grid40 formElementLabel">
						${label}
					</div>
				</c:if>
						<div class="formElementContainer floatLeft grid60">
							<div class="input-container-inner <c:if test='${not empty tooltipContent}'> toolTipSpacer</c:if><c:if test='${not empty inputClasses}'> ${inputClasses}Container</c:if> clearfix">
								<c:if test="${type == 'phone'}">
									<div class="phoneCodeContainer">
										<input id="${id}Code" name="${name}Code" value="${defaultCode}" class="input-input phoneNumber phoneCode<c:if test="${defaultCode == 'Code'}"> phoneEmpty</c:if>" maxlength="9" />
									</div>
								</c:if>
								<input id="${id}" name="${name}" value="${default}" class="input-input ${inputClasses}<c:if test="${type == 'phone'}"><c:if test="${default == 'Number'}"> phoneEmpty</c:if></c:if>" ${inputAttributes} />
								<c:if test="${not empty tooltipContent && tooltipContent.length() > 0}">
									<base:tooltip id="${id}Tooltip" content="${tooltipContent}" />
								</c:if>
								<c:if test="${not empty noteContent}">
									<div class="inputNote teal8">
										<div class="inputNoteArrow"></div>
										<div class="inputNoteInner">${noteContent}</div>
									</div>
								</c:if>
							</div>
						</div>
				</div>
		</c:when>
		<c:otherwise>
			<div class="input-wrapper <c:if test='${not empty tooltipContent}'>toolTipSpacer</c:if> ${className} ${hidden} clearfix">
				<input id="${id}" name="${name}" value="${default}" class="input-input ${inputClasses}" ${inputAttributes} />
				<c:if test="${not empty tooltipContent && tooltipContent.length() > 0 }">
					<base:tooltip id="${id}Tooltip" content="${tooltipContent}" />
				</c:if>
				<c:if test="${not empty noteContent}">
					<div class="inputNote teal8">
						<div class="inputNoteArrow"></div>
						<div class="inputNoteInner">${noteContent}</div>
					</div>
				</c:if>
			</div>
		</c:otherwise>
	</c:choose>

</c:otherwise>
</c:choose>