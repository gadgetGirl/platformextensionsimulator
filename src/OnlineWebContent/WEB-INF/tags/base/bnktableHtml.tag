<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<%@ attribute name="id" required="true" rtexprvalue="true"
	type="java.lang.String"%>
<%@ attribute name="name" required="true" rtexprvalue="true"
	type="java.lang.String"%>
<%@ attribute name="type" required="true" rtexprvalue="true"
	type="java.lang.String"%>
<%@ attribute name="value" required="true" rtexprvalue="true"
	type="java.lang.Object"%>
<%@ attribute name="label" required="true" rtexprvalue="true"
	type="java.lang.String"%>
<%@ attribute name="onChange" required="false" rtexprvalue="true"
	type="java.lang.String"%>
	
	<%@ attribute name="length" required="false" rtexprvalue="true"
	type="java.lang.String"%>
	
<li class="tabler action ">

	<span class="tabler2 yes">
		<span class="tableGroup ">
			<span class="item " id="">
				<span class="rinner ">
					<span class="phoneLabel">Their Ref</span>
					<span class="cellContent">${label}</span>
				</span>
			</span>
	
	<c:if test="${type eq 'text'}">
		<div class="input" id="${id}Container">
	
			<span class="rinner larget">
				<span class="phoneLabel">Their Ref</span>
				<span class="cellContent">
				</span>
				<span class="inputColor turqDarkBg roundedLeft">
				</span>
				<span class="inputField">
				<input type="text" name="${name}" id="${id}" value="${value}" onChange="${onChange}" maxlength="${length}" size="4">
				</span>
				</span>

		</div>
	</c:if>
	
	<c:if test="${type eq 'dropdown'}">
		<span class="item  " id="">
			<span class="rinner larget">
				<span class="phoneLabel">Their Ref</span>
					<span class="cellContent">
					<span id="pay" class="input">
						<span class="inputColor"></span>
						<span>
						<base:dropDown id="" name="" dropdown="${value}"></base:dropDown></span>
					</span>
					</span>
			</span>
		</span>
	</c:if>
	</span>
	</span>
</li>