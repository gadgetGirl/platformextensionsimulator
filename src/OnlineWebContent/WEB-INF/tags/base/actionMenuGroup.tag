<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<%@ attribute name="className" required="false" %>

<%-- <div id="actionMenu" class="actionMenu actionMenuGroup<c:if test='${not empty className}'> ${className}</c:if>">--%>	
	<jsp:doBody></jsp:doBody>
	<div id='actionMenuUrlWrapper' class='actionMenuUrlWrapper'><div id='actionMenuUrl' class='actionMenuUrl'></div></div>
<%--</div> --%>