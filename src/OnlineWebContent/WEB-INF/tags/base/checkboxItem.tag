<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="true"  name="id"%>
<%@ attribute required="false" name="className"%>
<%@ attribute required="false" name="name"%>
<%@ attribute required="false" name="onclick"%>
<%@ attribute required="false" name="disabled"%>
<%@ attribute required="false" name="value"%>
<%@ attribute required="false" name="selected"%>
<%@ attribute required="false" name="label"%>
<%@ attribute required="false" name="leftLabel" description="DEFAULT is 'false'"%>
<%@ attribute required="false" name="uncheckedValue"%>
<%@ attribute required="false" name="data"%>
<%@ attribute required="false" name="count"%>
<%@ attribute name="isTableObject" required="false"%>
<c:set var="name" value="${empty name ? id : name}" />
<c:set var="count" value="${empty count ? '' : count}" />
<c:set var="leftLabel" value="${not empty leftLabel ? leftLabel : false}" />
<c:set var="disabledClass" value="${disabled == true ? 'disabled' : ''}" />
<c:set var="selectedClass" value="${selected == true ? 'checked' : ''}" />
<c:set var="label">${label}<jsp:doBody /></c:set>
<%--
NEW CHECKBOXES
Developer: Leon (2012-05-02)
--%>
<c:choose>
<c:when test="${isTableObject == true}">
<div id="${id}${count}-graphic-wrapper" class="checkbox-graphic-wrapper ${disabledClass} ${selectedClass}">
			<input id="${id}${count}" name="${id}${count}" class="checkbox-input transparent" type="checkbox" value="${value}"
				<c:if test='${disabled}'>disabled="disabled"</c:if>
				<c:if test='${selected}'>checked="checked"</c:if>
				<c:if test='${not empty uncheckedValue}'>data-value="${uncheckedValue}"</c:if>
				<c:if test='${not empty onclick}'>onclick="${onclick}"</c:if>
				<c:if test='${not empty data}'> data-extended="${data}"</c:if> />
			<div id="${id}${count}-graphic" class="checkbox-graphic"> </div>
		</div>
		
</c:when>
<c:otherwise>
<div id="${id}${count}-wrapper" class="checkbox-wrapper ${leftLabel ? 'checkbox-leftlabel' : ''} formElementWrapper clearfix ${className}">
	<c:if test="${leftLabel && not empty label}">
		<div class="gridCol grid40 formElementLabel checkbox-label">${label}</div>
	</c:if>
	<div class="gridCol formElementContainer <c:if test="${leftlabel}">${empty label ? 'grid100' : 'grid60'}</c:if>">
		<div id="${id}${count}-graphic-wrapper" class="checkbox-graphic-wrapper ${disabledClass} ${selectedClass}">
			<input id="${id}${count}" name="${id}${count}" class="checkbox-input transparent" type="checkbox" value="${value}"
				<c:if test='${disabled}'>disabled="disabled"</c:if>
				<c:if test='${selected}'>checked="checked"</c:if>
				<c:if test='${not empty uncheckedValue}'>data-value="${uncheckedValue}"</c:if>
				<c:if test='${not empty onclick}'>onclick="${onclick}"</c:if>
				<c:if test='${not empty data}'> data-extended="${data}"</c:if> />
			<div id="${id}${count}-graphic" class="checkbox-graphic"> </div>
		</div>
	</div>
	<c:if test="${not leftLabel && not empty label}">
		<div class="gridCol grid80 formElementLabel checkbox-label">${label}</div>
	</c:if>
</div>

</c:otherwise>
</c:choose>
<%-- OLD CHECKBOXES >> for reference only--%>
<%-- 
<div class="checkbox-wrapper formElementContainer clearfix ${className}">
	<div id="${id}${count}_wrapper" class="checkboxGraphic floatLeft ${disabledClass} <c:if test='${selected}'> selected</c:if>" <c:if test='${not empty onclick}'>onclick="${onclick}"</c:if> data="${data}" >
		<input class="checkboxValue" type="checkbox" id="${id}${count}"<c:if test='${selected}'> checked="checked"</c:if> name="${name}${count}" value="${value}"<c:if test='${disabled == true}'> disabled="disabled"</c:if><c:if test='${not empty uncheckedValue}'> data-value="${uncheckedValue}"</c:if>/>
	</div>
	<div class="checkbox-label floatLeft">${label}</div>
</div>
--%>