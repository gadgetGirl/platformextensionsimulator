<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="heading" required="true" rtexprvalue="true"%>
<%@ attribute name="img" required="false" rtexprvalue="true"%>
<%@ attribute name="bucksValue" required="false" rtexprvalue="true"%>
<%@ attribute name="value" required="true" rtexprvalue="true"%>


<div class="eBucksItem ${className}">
	<div class="eBucksItemBorder">
		<div class="eBucksPadding">
			<base:heading value="${heading}" level="3" />
			<img src="${img}" />
		</div>
		
		<div class="valuesContainer">
			<div class="eBucksValues">
				<base:heading value="eB ${bucksValue}" level="4" />
				<base:heading value="R ${value}" level="4" />
			</div>
			<div class="eBucksMore">
				<base:hyperlink href=""><img src="../03images/fnb/arrows/flushRightOrange.png" /></base:hyperlink>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
</div>