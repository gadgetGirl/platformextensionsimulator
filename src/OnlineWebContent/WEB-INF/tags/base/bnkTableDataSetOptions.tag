<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ tag import="fnb.online.tags.beans.table.TableOptions"%>
<%@ tag import="fnb.online.tags.beans.table.BeanUtilities"%>

<%@ attribute name="showTotalFooter" required="false" rtexprvalue="true"%>
<%@ attribute name="emptyDataSetMessage" required="false" rtexprvalue="true"%>
<%   

String optionsId = BeanUtilities.getFieldValuePairs(getParent(),false).get("id");
TableOptions tableOptions = (TableOptions) request.getAttribute(optionsId);

tableOptions.setShowTotalFooter(Boolean.parseBoolean(showTotalFooter));
tableOptions.setEmptyDataSetMessage(emptyDataSetMessage);

%>