<%@ attribute name="type" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<%@ attribute name="id" required="false" rtexprvalue="true"%>
<%@ attribute name="position" required="false" rtexprvalue="true"%>
<%@ attribute name="divide" required="false" rtexprvalue="true"%>
<%@ attribute name="confirm" required="false" rtexprvalue="true"%>
<%@ attribute name="custom" required="false" rtexprvalue="true"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>

<div id="${id}" class="gridHalf ${position} ${className} ${custom}">
	
	<div class="innerMargin ${divide}">
		<div class="editDiv">
			<c:choose>
				<c:when test="${confirm == 'true'}">
					<div class="editDivInner">
						<jsp:doBody></jsp:doBody>
					</div>	
				</c:when>
				<c:otherwise>
					<jsp:doBody></jsp:doBody>
				</c:otherwise>
			</c:choose>
			
			
		</div>
	</div>
	
</div>
