<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute required="false"  name="id"%>

<div id="${id}popupWrapper" class="popupWrapper hidden">
	<jsp:doBody></jsp:doBody>
</div>