<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ attribute name="className" required="false" rtexprvalue="true"%>

<div id="popupButtonsWrapper">
<div id="popupButtons"<c:if test='${not empty className}'> class="${className}"</c:if>>
	<jsp:doBody />
</div>
</div>