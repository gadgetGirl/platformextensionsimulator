<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>

<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>

<%@ attribute name="columnItems" required="true" rtexprvalue="true" type="java.util.List"%>

<%@ attribute name="id" required="true" %>
<%@ attribute name="columnIndex" required="true" %>
<%@ attribute name="columnWidth" required="true" %>
<%@ attribute name="columnMainHeading" required="false" %>
<%@ attribute name="columnMainHeadingClass" required="false" %>
<%@ attribute name="columnClassName" required="false" %>
<%@ attribute name="columnBodyClassName" required="false" %>
<%@ attribute name="type" required="false" %>

<%@ attribute name="isSingle" required="false" %>
<%@ attribute name="isDouble" required="false" %>

<c:set var="defaultHeadingClass">
	<c:choose>
		<c:when test="${columnIndex == '0'}">bannerSubHeadings</c:when>
	    <c:when test="${columnIndex == '1'}">grey1</c:when>
	    <c:when test="${columnIndex == '2'}">grey1</c:when>
	    <c:when test="${columnIndex == '3'}">grey1</c:when>
	</c:choose>
</c:set>
<c:set var="defaultItemsClass">
	<c:choose>
	    <c:when test="${columnIndex == '1'}">grey2</c:when>
	    <c:when test="${columnIndex == '2'}">grey2</c:when>
	    <c:when test="${columnIndex == '3'}">grey2</c:when>
	</c:choose>
</c:set>
<c:if test="${columnIndex>0}"><c:set var="columnClassName" value="${columnClassName} actionMenuLeftBorder"/></c:if>
<c:if test="${isSingle=='true'&&columnIndex>0}"><c:set var="columnClassName" value="${columnClassName} actionMenuLeftRightBorder actionMenuSingleColumn"/></c:if>
<c:if test="${isDouble=='true'&&columnIndex==1}"><c:set var="columnClassName" value="${columnClassName} actionMenuDoubleColumn"/></c:if>
<c:if test="${isDouble=='true'&&columnIndex==2}"><c:set var="columnClassName" value="${columnClassName} actionMenuLeftRightBorder actionMenuDoubleColumn"/></c:if>

<div class="actionMenuCol col_${columnIndex}_of_4<c:if test='${not empty columnClassName}'> ${columnClassName}</c:if>">
	<div<c:if test="${not empty id}"> id="${id}"</c:if><c:if test='${not empty columnBodyClassName}'> class="${columnBodyClassName}"</c:if>>
		<c:if test="${not empty columnMainHeading}"><base:actionMenuHeader label="${columnMainHeading}" className="columnMainHeading ${columnMainHeadingClass}"/></c:if>
		<c:choose>
		    <c:when test="${not empty columnItems}">
		    	<c:set var="headerClass" value="${defaultHeadingClass}"/>
			    <c:forEach items="${columnItems}" var="columnItem" varStatus="columnItemStatus">
					<c:if test="${not empty columnItem.heading}">
						<base:actionMenuHeader label="${columnItem.heading}" className="actionColHeader ${headerClass}"/>
					</c:if>
					<c:set var="itemsClass" value="${defaultItemsClass} ${id}subHeading${columnItemStatus.index}"/>
					<c:set var="lastClass" value=""/>
					<c:if test="${columnItemStatus.last}">
						<c:set var="lastClass" value=" actionMenuLast"/>
					</c:if>
					<base:actionMenuHeader className="${itemsClass}${lastClass} ${headerClass}">
						<c:forEach items="${columnItem.actions}" var="actionItems" varStatus="actionItemStatus">
							<c:set var="replace" value="'" />
							<c:set var="url" value="${fn:replace(actionItems.url, replace, 'sqo')}" />
							<c:set var="onclickFunction">
			                  <c:choose>
<%-- ACTION_TARGET_URL_WORKSPACE --%>	<c:when test="${actionItems.target == '0'}">
	 										<c:choose>
												<c:when test="${type == 'moreOptions'}">fnb.controls.controller.eventsObject.raiseEvent('eziSliderShowBody','${actionItems.url}');</c:when>
					                       		<c:otherwise>
													fnb.controls.controller.eventsObject.raiseEvent('actionMenuloadResultScreen','${actionItems.url}');
												</c:otherwise>
				                       		</c:choose>
										</c:when>
<%-- ACTION_TARGET_URL_PLACEHOLDER --%>	<c:when test="${actionItems.target == '1'}">fnb.utils.actionMenu.loadTargetToActionMenu('${actionItems.url}',this);</c:when>
<%-- ACTION_TARGET_SHOW_CONTENT --%>	<c:when test="${actionItems.target == '2'}">fnb.functions.showDiv.show('#${actionItems.contentIdentifier}');</c:when>
<%-- ACTION_TARGET_SHOW_EZI_PANEL --%>	<c:when test="${actionItems.target == '3'}">fnb.controls.controller.eventsObject.raiseEvent('eziSliderShow', {url: '${url}'});</c:when>
<%-- ACTION_TARGET_PRINT_DIV --%>		<c:when test="${actionItems.target == '4'}">fnb.functions.loadUrlToPrintDiv.load('${actionItems.url}');</c:when>
<%-- ACTION_TARGET_DOWNLOAD --%>		<c:when test="${actionItems.target == '5'}">fnb.controls.controller.eventsObject.raiseEvent('doDownload','${actionItems.url}');</c:when>
<%-- ACTION_TARGET_REDIRECT --%>		<c:when test="${actionItems.target == '6'}">fnb.controls.controller.eventsObject.raiseEvent('actionMenuPopupLoadUrl', '${actionItems.url}');</c:when>
<%-- ACTION_TARGET_LOAD_IN_POPUP --%>	<c:when test="${actionItems.target == '7'}">fnb.controls.controller.eventsObject.raiseEvent('popupLoadUrlFromActionMenu', {url:'${actionItems.url}',preventDefaults:true});</c:when>
										<c:when test="${actionItems.target == '8'}">fnb.controls.controller.eventsObject.raiseEvent('openWindow','${actionItems.url}');</c:when>
			                  </c:choose>
			            	</c:set>
							<c:choose>
								<c:when test="${columnIndex==0}">
									<base:actionMenuButton id="actionMenuButton${actionItemStatus.index}" type="actionMenuButtonIcon" label="${actionItems.description}" className="clearfix" onclick="${onclickFunction}"/>
								</c:when>
								<c:otherwise>
									<base:actionMenuListItem headerText="${actionItems.description}" commentText="${actionItems.comment}" onclick="${onclickFunction}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</base:actionMenuHeader>
				</c:forEach>
		    </c:when>
		    <c:otherwise>
				<base:actionMenuListItem className="actionMenuNoOptions" headerText="There are no additional options for ${columnMainHeading}"/>
			</c:otherwise>
		</c:choose>
		
		<jsp:doBody></jsp:doBody>
	</div>
</div>