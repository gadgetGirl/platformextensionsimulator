<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="subs" tagdir="/WEB-INF/tags/subs"%>

<%@ attribute required="true" rtexprvalue="true" name="messageKey" description="which resource to retrieve"%>
<%@ attribute required="false" rtexprvalue="true" name="email"%>

<c:if test="${email}">
	<c:set var="emailAddress"><base:localiseEmail /></c:set>
</c:if>

<c:choose>
	<c:when test="${country=='45'}">
		<subs:country45 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='55'}">
		<subs:country55 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='65'}">
		<subs:country65 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='75'}">
		<subs:country75 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='80'}">
		<subs:country80 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='21'}">
		<subs:country21 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='22'}">
		<subs:country22 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='24'}">
		<subs:country24 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='81'}">
		<subs:country81 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='82'}">
		<subs:country82 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	<c:when test="${country=='83'}">
		<subs:country83 messageKey="${messageKey}" email="${emailAddress}" />
	</c:when>
	
	<c:otherwise>
		<subs:subsDefault messageKey="${messageKey}" email="${emailAddress}" />
	</c:otherwise>
</c:choose>