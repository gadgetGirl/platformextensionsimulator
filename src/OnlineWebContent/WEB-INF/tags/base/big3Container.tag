<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="fn" uri="/WEB-INF/fn.tld"%>

<%@ attribute name="event" required="false" rtexprvalue="true"%>

<jsp:useBean id="big3ViewBeanList" class="java.util.ArrayList" scope="request"/>
<base:import type="css" link="/banking/01css_new/pages/loggedin/big3/big3.css"/>

<c:set var="className" value="grid33" />
<c:choose>
	<c:when test='${fn:length(big3ViewBeanList) == 1}'>
		<c:set var="className" value="grid100" />
	</c:when>
	<c:when test='${fn:length(big3ViewBeanList) == 2}'>
		<c:set var="className" value="grid50" />
	</c:when>
	<c:when test='${fn:length(big3ViewBeanList) == 3}'>
		<c:set var="className" value="grid33" />
	</c:when>
	<c:otherwise>

	</c:otherwise>
</c:choose>
	
<c:set var="testFlag" value="false"/>
<c:if test="${fn:length(big3ViewBeanList) > 0}">
<div id="big3OuterWrapper" class="clearfix ${testFlag == 'true' ? 'bigThreeOfferOuter' : ''}">
	<div id="big3InnerWrapper" class="big3InnerWrapper">
	<c:choose>
		<c:when test='${testFlag != "true"}'>
			<div id="big3ShoutOutContainer">
				<div id="big3ShoutOut">${sessionScope.SessionData["HyphenUser"].firstNames}, FNB recommends...<div id="big3ShoutOutArrow"></div></div>
			</div>
			<div class="big3LeftTopCorner"></div>
			<base:scrollingBanner>
				<c:forEach var="item" end="2" items="${big3ViewBeanList}" varStatus="i">
					<c:set var="type" value="" />
					<c:if test="${empty item.event}">
						<c:set var="event" value="loadUrlToWorkspace" />
					</c:if>
					<c:if test='${item.preselected != true}'><c:set var="type" value="preapproved" /></c:if>			
					<base:scrollingBannerItem onClick="${onclick}" id="big3_0${i.index-1}" className="big3Container teal9 ${className} ${type}">
						<div class="big3Inner"<c:if test='${not empty item.url}'> data-value="${item.url}"</c:if> data-event="${item.event}">
							<c:if test='${not empty item.head}'><h2 class="big3Heading">${item.head}</h2></c:if>
							<c:if test='${not empty item.body}'><p class="big3Paragraph">${item.body}</p></c:if>
							<input name="offerType" value="${item.offerType}" id="offerType" type="hidden" />
							<input name="preselected" value="${item.preselected}" id="preselected" type="hidden" />
							<input name="target" value="${item.target}" id="target" type="hidden" />
<%-- 							<div class="callToAction">${item.callToAction}</div> --%>
							<div class="callToAction"></div>
						</div>
					</base:scrollingBannerItem>
				</c:forEach>
			</base:scrollingBanner>
			<script>
				fnb.functions.bigThree.init();
			</script>
		</c:when>
		<c:otherwise>
			<base:form name="bigThreeOffer" action="/banking/Controller" method="POST" nav="offer.navigator.PrepareOfferView">
				<base:input type="text" id="offerType" name="offerType" value="" label="Offer Code" inputWidth="60"/>
				<base:button label="Go" formToSubmit="bigThreeOffer" id="bigThreeGo" icon="bigThreeGoText"/>
			</base:form>
		</c:otherwise>
	</c:choose>
	</div>
</div>
</c:if>