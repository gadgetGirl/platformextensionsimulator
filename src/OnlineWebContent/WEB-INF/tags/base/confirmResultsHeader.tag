<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="pageType" required="false" rtexprvalue="true"%>
<%@ attribute name="confirmType" required="false" rtexprvalue="true"%>
<%@ attribute name="date" required="false" rtexprvalue="true"%>
<%@ attribute name="displayTime" required="false" rtexprvalue="true"%>
<%@ attribute name="accNumber" required="false" rtexprvalue="true"%>
<%@ attribute name="accName" required="false" rtexprvalue="true"%>
<%@ attribute name="className" required="false" rtexprvalue="true"%>
<base:gridGroup className="confirmHeader borderBottomWhite">
	<base:gridCol width="80" className="confirmHeaderInner ${className}">
		<base:divContainer>
			<base:divContainer className="confirmHeaderTopText">${pageType}&nbsp;</base:divContainer>
			<base:divContainer className="confirmHeaderBottomText"><span class="confirmType ">${confirmType} </span><span class="confirmTextOne">${accName} </span><span class="confirmTextTwo">${accNumber}</span></base:divContainer>
		</base:divContainer>
		<base:divContainer className="confirmDate">
			<base:prettyDate displayTime="${displayTime}" date="${date}" label="" />
		</base:divContainer>
	</base:gridCol>
</base:gridGroup>