<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ attribute name="id" required="true"%>
<%@ attribute name="className" required="false"%>
<%@ attribute name="rows" required="true"%>
<%@ attribute name="charCount" required="false"%>
<%@ attribute name="label" required="false"%>
<div id="${id}" class="textAreaContainer ${className} padTop10 clearfix">
	<c:if test='${not empty label}'>
		<div class="gridCol grid40 formElementLabel">
			${label}
		</div>
	</c:if>
	<div class="grid60 floatLeft">
		<div class="padLeft15 padRight11">
			<div class="borderDiv">
				<textarea name="${id}" id="${id}_textarea" <c:if test='${not empty charCount}'>onkeyup="fnb.forms.textAreaHandler.availChars('#${id}',${charCount})"</c:if> rows="${rows}"><jsp:doBody /></textarea>
			</div>
			<c:if test='${not empty charCount}'>
				<span class="charCount padTop10 padBottom20">${charCount}</span>
			</c:if>
		</div>
	</div>
</div>
