<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ attribute name="tableId" required="true" rtexprvalue="true"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="tableRowGroup" required="false" rtexprvalue="true" type="fnb.online.tags.beans.table.TableRowGroup"%>
<%@ attribute name="columnIndex" required="true" rtexprvalue="true"%>
<%@ attribute name="groupCount" required="false" rtexprvalue="true"%>
<%@ attribute name="hasGroup" required="false" rtexprvalue="true"%>
<%@ attribute name="disableSorting" required="false" rtexprvalue="true"%>


<c:if test="${hasGroup == true}">
<c:set var="childHasGroup" value=" childHasGroup"/>
</c:if>


<%@tag import="java.util.Map"%>
<%@tag import="java.util.Iterator"%>

<%
	Boolean overrideUrl = false;
	String key = "";
	String value = "";
	if(tableRowGroup!=null&&tableRowGroup!=null){
		String fieldName = columnOptions.getFieldName();
		Map<String, String> overrideColumnUrl = tableRowGroup.getOverrideColumnUrlMap();
		Iterator<Map.Entry<String, String>> OCUIteratorEntries = overrideColumnUrl.entrySet().iterator();
		while (OCUIteratorEntries.hasNext()) {
		    Map.Entry<String, String> entry = OCUIteratorEntries.next(); 
		    if(entry.getKey()==fieldName){
		    	key = entry.getKey();
		    	value = entry.getValue();
		    	overrideUrl = true;
			}
		}
	}
%>

<c:set var="overrideUrl" value="<%=overrideUrl%>" />
<c:set var="key" value="<%=key%>" />
<c:set var="urlValue" value="<%=value%>" />

<div class="tableCell col${columnIndex} <c:if test='${columnOptions.selectAll}'> selectAllActive</c:if> ${columnOptions.align}">
	<div class="tableHeaderCellItem">
	<c:choose>
		<c:when test="${!columnOptions.selectAll&&overrideUrl==false}">
			<c:choose>
			<c:when test="${disableSorting == false}">
			<a onclick="${tableId}_tableController.sortTable(${columnIndex}, '${columnOptions.fieldName}'); return false;"> <div class="hinner ${columnOptions.cssClass} ${columnOptions.align} ${columnOptions.hideColumn}">${columnOptions.heading}</div></a>
			</c:when>
			<c:otherwise>
			
			<a><div class="hinner ${columnOptions.cssClass} ${columnOptions.align} ${columnOptions.hideColumn}">${columnOptions.heading}</div></a>
			</c:otherwise>
			</c:choose>
		</c:when>
		<c:when test="${overrideUrl==true}">
			<a onclick="${tableId}_tableController.pageTable('${tableBean.pagingViewBean.currentPageNumber}'); fnb.controls.controller.eventsObject.raiseEvent('loadUrlToTarget',{url:'${urlValue}',target:'#${tableId}_tableContent', preLoadingCallBack:MammothTableUtility.prepareActionMenuNotToClose,postLoadingCallBack:MammothTableUtility.resetActionMenu}); return false;" class="copyRows copyLink${columnIndex}${childHasGroup} clearfix" >Copy Last Paid</a>
			<div class="hinner ${columnOptions.cssClass} ${columnOptions.align} ${columnOptions.hideColumn} sizeFix">${columnOptions.heading}</div>
		</c:when>
		<c:otherwise>

			<base:checkboxToggleAll id="${columnIndex}" className="selectAllLink${childHasGroup}" groupClass="#${tableId} .tableRowGroup${groupCount+1} .col${columnIndex}" labelCheck="Select all" labelUncheck="Deselect all" />
			<a class="hinner ${columnOptions.cssClass} ${columnOptions.align} ${columnOptions.hideColumn}">${columnOptions.heading}</a>
		</c:otherwise>
	</c:choose>
	</div>
</div>