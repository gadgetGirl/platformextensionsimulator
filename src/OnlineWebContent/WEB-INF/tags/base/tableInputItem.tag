<%@ tag import="fnb.online.tags.beans.table.TableInputItem"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableInputItem"%>
<%@ attribute name="tableDoubleItem" required="false" rtexprvalue="true"%>
<%@ attribute name="count" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>
<%@ attribute name="onClick" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="onKeyup" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="onChange" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="isTableObject" required="false" rtexprvalue="true"%>

<c:choose>
	<c:when test="${tableDoubleItem == true}">
		<c:choose>
			<c:when test="${tableItem.hidden == true}">
				<base:input isTableObject="${isTableObject}" id="${tableItem.id}${count}" name="${tableItem.name}${count}" type="hidden" value="${tableItem.text}" onclick="${onClick}" onkeyup="${onKeyup}" onchange="${onChange}"/>
			</c:when>
			<c:otherwise>
				<base:input isTableObject="${isTableObject}" id="${tableItem.id}${count}" name="${tableItem.name}${count}" type="text" value="${tableItem.text}" onclick="${onClick}" onkeyup="${onKeyup}" onchange="${onChange}" maxlength="${columnOptions.maxLength}"/>
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${tableItem.hidden == true}">
				<base:input isTableObject="${isTableObject}" id="${tableItem.id}${count}" name="${tableItem.name}${count}" type="hidden" value="${tableItem.text}" onclick="${onClick}" onkeyup="${onKeyup}" onchange="${onChange}"/>
			</c:when>	
			<c:otherwise>
				<div class="tableCellItem ${columnOptions.size} overflowOff" >
			  		<base:input isTableObject="${isTableObject}" id="${tableItem.id}${count}" name="${tableItem.name}${count}" type="text" value="${tableItem.text}" onclick="${onClick}" onkeyup="${onKeyup}" onchange="${onChange}" maxlength="${columnOptions.maxLength}"/>
			  	</div>
		  	</c:otherwise>
	  	</c:choose>
	</c:otherwise>
</c:choose>