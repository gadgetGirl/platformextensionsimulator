<%@tag import="fnb.online.tags.beans.table.TableButtonItem"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="count" required="true" rtexprvalue="true" %>
<%@ attribute name="tableItem" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableButtonItem"%>
<%@ attribute name="columnOptions" required="true" rtexprvalue="true" type="fnb.online.tags.beans.table.TableColumnOptions"%>

<c:choose>
	<c:when test="${tableDoubleItem == true}">
		<base:formsButton id="" text="${tableItem.text}" link="${tableItem.url}"/>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${columnOptions.icon == 'print'}">
				<div id="${tableItem.id}${count}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size} ${columnOptions.icon}" >
					<base:formsButton id="" text="${tableItem.text}" link="${tableItem.url}" count="${count}" target="print"/>
				</div>
			</c:when>
			<c:when test="${columnOptions.icon == 'download'}">
				<div id="${tableItem.id}${count}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size} ${columnOptions.icon}" >
					<base:formsButton id="" text="${tableItem.text}" link="${tableItem.url}" count="${count}" target="download"/>
				</div>
			</c:when>
			<c:otherwise>
				<div id="${tableItem.id}${count}" name="${tableItem.name}" class="tableCellItem ${columnOptions.size} ${columnOptions.icon}" >
					<base:formsButton id="" text="${tableItem.text}" link="${tableItem.url}" count="${count}" target="${tableItem.target}"/>
				</div>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>



 