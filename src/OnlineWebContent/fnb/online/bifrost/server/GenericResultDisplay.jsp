<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<base:bnkContainerPage id="finishBackground" className="confirmPage">

	<base:finishPage backgroundTarget=".rightBox" resultBean="${processingResult}" />

	<base:divContainer className="grid80">
		<base:confirmFinishMultiTable>

			<c:forEach items="${page.form.rows}" var="row" varStatus="rowCount">

				<base:confirmFinishMultiTableHeader value="${row.heading }" />

				<base:confirmFinishMultiTableRow>
					<base:confirmFinishMultiTableCell heading="${row.field1.label }"
						value="${row.field1.value } " />
					<base:confirmFinishMultiTableCell heading="${row.field2.label }"
						value="${row.field2.value } " />
				</base:confirmFinishMultiTableRow>
			</c:forEach>

		</base:confirmFinishMultiTable>
	</base:divContainer>

</base:bnkContainerPage>

<base:footerButtonGroup>
	<c:forEach items="${page.footerButtons}" var="button">
		<base:footerButton text="${button.label}" formToSubmit="genericForm"
			url="${button.url}" />
	</c:forEach>
</base:footerButtonGroup>

