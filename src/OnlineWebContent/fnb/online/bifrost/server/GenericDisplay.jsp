<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>

<base:bnkContainerPage id="genericContent" >
	<base:pageHeader value="${page.header}" />
	<base:form name="genericForm" action="${page.form.action}"
		method="POST">
		<c:forEach items="${page.form.rows}" var="row" varStatus="rowCount">

			<base:heading value="${row.heading }" level="2" />

			<base:equalHeightsRow>

				<base:equalHeightsColumn className="white borderRightGrey"
					width="50">
					<c:choose>
						<c:when test="${row.field1.type == 'input'}">
							<base:input maxlength="150" className="margin50"
								labelPosition="labelLeft" id="${row.field1.id}"
								name="${row.field1.name }" label="${row.field1.label }"
								value="${row.field1.value }" />
						</c:when>
						<c:when test="${row.field1.type == 'currency'}">
							<base:input maxlength="50" className="margin50"
								labelPosition="labelLeft" id="${row.field1.id}"
								name="${row.field1.name }" label="${row.field1.label }"
								type="currency" value="${row.field1.value }" />
						</c:when>
						<c:when test="${row.field1.type == 'dropdown'}">
							<base:dropDown className="margin50" id="${row.field1.id}"
								name="${row.field1.name }" label="${row.field1.label }"
								value="${row.field1.value }" type="singleTier"
								dropdown="${row.field1.dropdownBean}"
								onchange="return submitForm('${row.field1.url}',this,'${row.field1.id}')" />
						</c:when>
						<c:when test="${row.field1.type == 'date'}">
							<base:prettyDate date="${row.field1.value }"
								label="${row.field1.label }" />
						</c:when>
						<c:otherwise>
							<base:paragraph>${row.field1.label } &nbsp; ${row.field1.value }</base:paragraph>
						</c:otherwise>
					</c:choose>
				</base:equalHeightsColumn>

				<base:equalHeightsColumn className="white" width="50">
					<c:choose>
						<c:when test="${row.field2.type == 'input'}">
							<base:input maxlength="150" className="margin50"
								labelPosition="labelLeft" id="${row.field2.id}"
								name="${row.field2.name }" label="${row.field2.label }"
								value="${row.field2.value }" />
						</c:when>
						<c:when test="${row.field2.type == 'dropdown'}">
							<base:dropDown className="margin50" id="${row.field2.id}"
								name="${row.field2.name }" label="${row.field2.label }"
								value="${row.field2.value }" type="singleTier"
								dropdown="${row.field2.dropdownBean}"
								onchange="return submitForm('${row.field2.url}',this,'${row.field2.id}')" />

						</c:when>
						<c:when test="${row.field2.type == 'date'}">
							<base:prettyDate date="${row.field2.value }"
								label="${row.field2.label }" />
							<base:input type="hidden" id="${row.field2.id}"
								name="${row.field2.name }" value="${row.field2.value }" />
						</c:when>
						<c:otherwise>
							<base:paragraph>${row.field2.label } &nbsp; ${row.field2.value }</base:paragraph>
						</c:otherwise>
					</c:choose>
				</base:equalHeightsColumn>

				<base:equalHeightsColumn className=" ghostBlock">&nbsp;</base:equalHeightsColumn>

			</base:equalHeightsRow>
		</c:forEach>
		<c:if test="${page.form.note != null}">
			<base:note type="note">
				<base:paragraph>${page.form.note}</base:paragraph>
			</base:note>
		</c:if>
	</base:form>

</base:bnkContainerPage>

<base:footerButtons>
	<c:forEach items="${page.footerButtons}" var="button">
		<base:footerButton text="${button.label}" formToSubmit="genericForm"
			url="${button.url}" />
	</c:forEach>
</base:footerButtons>

<script>
	function submitForm(reponseUrl, field, fieldName) {
		if (reponseUrl != '') {
			//submit form
			$("#" + fieldName).attr("value", $(field).attr("data-value"));
			fnb.functions.submitFormToWorkspace.submit('genericForm', '',
					field, {
						alternateUrl : reponseUrl
					});

		}
	}
</script>
