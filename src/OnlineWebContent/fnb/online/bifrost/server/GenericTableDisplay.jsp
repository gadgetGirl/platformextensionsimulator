<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>


<base:bnkContainerPage id="genericContent" className="${className}">

			<base:pageHeader value="${page.header}" />

			<base:form name="genericForm" action="" method="POST">
				<base:tableHeaderWrapper id="tableHeaderUtils"
					className="tableHeaderControls mobiHeaderControls">
					<base:tableHeaderActionButtons actionButtonsLabel="Actions"
						tableId="table1" tableBean="${tableBean}" />
				</base:tableHeaderWrapper>
				<base:bnkTable tableBean="${tableBean}" id="table1"
					className="table">
				</base:bnkTable>
			</base:form>


</base:bnkContainerPage>

<base:footerButtons>
	<c:forEach items="${page.footerButtons}" var="button">
		<base:footerButton text="${button.label}" formToSubmit="genericForm"
			url="${button.url}" />
	</c:forEach>
</base:footerButtons>

