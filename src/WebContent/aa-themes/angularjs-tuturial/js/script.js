// Code goes here

var app = angular.module( 'thor', ['ngRoute'] );
app.config( function($routeProvider) {
	$routeProvider
		.when( '/', {
			controller: 'SimpleController',
			templateUrl: 'fragments/peoplefilter.html'
		} )
		.when( '/graph', {
			controller: 'SimpleController',
			templateUrl: 'fragments/graph.html'
		})
		.otherwise( { redirectTo: '/' } );
} );

app.factory('nameFactory', function() {
    var names = [ {name:'Mike'},
                  {name:'Nats'},
                  {name:'Blake'},
                  {name:'Bryce'} ];

	var factory = {};
	factory.getNames = function() {
		return( names );
	};
	
	return( factory );
});

var controllers = {};
controllers.SimpleController = function($scope, nameFactory){
    $scope.customers = nameFactory.getNames();
    
    $scope.addName = function() {
    	$scope.customers.push( { name:$scope.newName.name } );
    	$scope.newName.name = '';
    };
};

app.controller( controllers );