function checkForUpdates() {
	console.log( "Checking for updates..." );
	ws.send( "ping" );
	console.log( "Ping sent!" );
}

function run() {
	success( 1 );
	success( 2 );
	success( 3 );
	success( 4 );
	success( 5 );
	success( 6 );
}

function success(x) {	
	var s = document.getElementById("success.holder").src;
	var div = document.getElementById(x);
	div.innerHTML = '<img src="' + s + '" style="border-radius: 50%" >';
}

function failed(x) {	
	var s = document.getElementById("failed.holder").src;
	var div = document.getElementById(x);
	div.innerHTML = '<img src="' + s + '" style="border-radius: 50%" >';
}

var wsUrl = 'ws://' + window.location.host + '/thor/echo';
console.log( 'WebSockets Url : ' + wsUrl );
var ws = new WebSocket( wsUrl );
 
ws.onopen = function(event){
    console.log( 'WebSocket connection started' );
};
 
ws.onclose = function(event){
     console.log( "Remote host closed or refused WebSocket connection" );
     console.log( event );
};
 
ws.onmessage = function(event){
    console.log( "onMessage: " + event.data );
    
    var jsonObj = JSON.parse( event.data );
    console.log( jsonObj );
    console.log( jsonObj.status );
    console.log( jsonObj.status[ 0 ].step );
    console.log( jsonObj.status[ 0 ].outcome );
    if( jsonObj.status[ 0 ].outcome == "true" ) {
    	console.log( "TRUE" );
    	success( jsonObj.status[ 0 ].step );
    }
    else {
    	console.log( "FALSE" );    	
    	failed( jsonObj.status[ 0 ].step );
    }
};