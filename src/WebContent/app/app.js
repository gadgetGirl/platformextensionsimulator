﻿var app = angular.module('pe', ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/simulator-config',
            {
                controller: 'AppConfigController',
                templateUrl: 'app/partials/simulator-config.html'
            })
        .when('/extension-config',
            {
                controller: 'AppConfigController',
                templateUrl: 'app/partials/extension-config.html'
            })
        .when('/customer-config',
            {
                controller: 'AppConfigController',
                templateUrl: 'app/partials/customer-config.html'
            })
        .when('/device-config',
            {
                controller: 'AppConfigController',
                templateUrl: 'app/partials/device-config.html'
            })
        .when('/channel-config',
            {
                controller: 'AppConfigController',
                templateUrl: 'app/partials/channel-config.html'
            })
        .when('/form-data',
            {
                controller: 'AppConfigController',
                templateUrl: 'app/partials/form-data.html'
            })
        .when('/timeout-config',
            {
                controller: 'AppConfigController',
                templateUrl: 'app/partials/timeout-config.html'
            })   
        .when('/mobi',
            {
                controller: 'AppConfigController',
                templateUrl: 'app/partials/mobi-simulator.html'
            })   
        .otherwise({ redirectTo: '/simulator-config' });
});
