﻿app.controller('AppConfigController', function ($scope, configFactory) { 
    init();
    getConfig();
    
    function init() {
    }

    function getConfig() {
    	console.log( "GET Config" );
    	configFactory.getConfig()
	    	.success(function(config) {
	    		$scope.config = config;

	    		$scope.yesno = [ {label: "No", value:false}, {label:"Yes", value:true}];

	    		$scope.gomez = $scope.config.gomezIP[ 0 ];
	    		$scope.fester = $scope.config.festerIP[ 0 ];
    			$scope.authenticated = $scope.yesno[ 0 ];
    			$scope.country = $scope.config.countryCode[ 0 ];
	    		$scope.voip = $scope.yesno[ 0 ];
    			$scope.vods = $scope.yesno[ 0 ];

	    		for( var i in $scope.config.countryCode ) {
	    			var o = $scope.config.countryCode[ i ];
	    		    if( o.selected == true ) {
	    	    		$scope.country = o; 
	    		    }
	    		}
    			
    			if( $scope.config.authenticated ) {
	    			$scope.authenticated = $scope.yesno[ 1 ];
	    		}
	    		
    			if( $scope.config.voipEnabled ) {
		    		$scope.voip = $scope.yesno[ 1 ];
	    		}
	    		
	    		if( $scope.config.vodsStandIn ) {
	    			$scope.vods = $scope.yesno[ 1 ];
	    		}

	    		for( var i in $scope.config.gomezIP ) {
	    			var o = $scope.config.gomezIP[ i ];
	    		    if( o.selected == true ) {
	    	    		$scope.gomez = o; 
	    		    }
	    		}
	    		
	    		for( var i in $scope.config.festerIP ) {
	    			var o = $scope.config.festerIP[ i ];
	    		    if( o.selected == true ) {
	    	    		$scope.fester = o; 
	    		    }
	    		}
	    			    		
	    	})
	    	.error(function(error) {
	    		$scope.status = "Failed to Update. Error: " + error.message;
	    	});
    }
        
    function saveSimulatorConfig() {
    	console.log( "saveSimulatorConfig" );
    	configFactory.saveSimulatorConfig($scope.config.gomezIP[gomez.value].value, $scope.config.festerIP[fester.value].value, $scope);
    }

    function saveExtensionConfig() {
    	console.log( "saveExtensionConfig" );
    	configFactory.saveExtensionConfig($scope);
    }

    function saveCustomerConfig() {
    	console.log( "saveCustomerConfig" );
    	configFactory.saveCustomerConfig($scope.yesno[authenticated.value].value, $scope.config.countryCode[country.value].value, $scope);
    }

    function saveDeviceConfig() {
    	console.log( "saveDeviceConfig" );
    	configFactory.saveDeviceConfig($scope.yesno[voip.value].value, $scope);
    }

    function saveChannelConfig() {
    	console.log( "saveChannelConfig" );
    	configFactory.saveChannelConfig($scope.yesno[vods.value].value, $scope);
    }
    
    function saveFormData() {
    	console.log( "saveFormData" );
    	configFactory.saveFormData($scope);
    }
    
   	function saveTimeoutConfig() {
   		console.log( "saveTimeoutConfig" );
   		configFactory.saveTimeoutConfig($scope);
   	}
   	
    function startSimulator() {
    	console.log( "start simulator..." );
    	$scope.saveSimulatorConfig();
    	configFactory.startSimulator($scope);
    	$scope.getConfig();
    }
    
   function stopSimulator() {
    	console.log( "stop simulator..." );
    	$scope.saveSimulatorConfig();
    	configFactory.stopSimulator($scope);
    	$scope.getConfig();
    }
   
    $scope.getConfig = getConfig;
    
    $scope.saveSimulatorConfig = saveSimulatorConfig;
    $scope.saveExtensionConfig = saveExtensionConfig;
    $scope.saveCustomerConfig = saveCustomerConfig;
    $scope.saveDeviceConfig = saveDeviceConfig;
    $scope.saveChannelConfig = saveChannelConfig;
    $scope.saveFormData = saveFormData;
    $scope.saveTimeoutConfig = saveTimeoutConfig;

    
    $scope.startSimulator = startSimulator; 
    $scope.stopSimulator = stopSimulator; 
    
});
