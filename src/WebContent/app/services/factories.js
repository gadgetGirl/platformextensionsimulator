app.factory('configFactory', ['$http', function($http) {

    var urlBase = '/admin/api';
    var configFactory = {};

    configFactory.getConfig = function () {
        return( $http.get( urlBase + "/config" ) );
    };

    configFactory.startSimulator = function ($scope) {
    	$http.get( urlBase + "/start" )
		.success(function() { 
			$scope.getConfig();
		});
    };

    configFactory.stopSimulator = function ($scope) {
    	$http.get( urlBase + "/stop" )
		.success(function() { 
			$scope.getConfig();
		});
    };

    configFactory.saveSimulatorConfig = function (selectedGomezIP, selectedFesterIP, $scope) {
    	$http.post( urlBase + "/config/simulator", { 
    				'gomezIP':selectedGomezIP,
    				'festerIP':selectedFesterIP 
    			})
    			.success(function() { 
    				$scope.getConfig();
    			});
    };

    configFactory.saveExtensionConfig = function ($scope) {
    	$http.post( urlBase + "/config/extension", { 
    				'extensionDescription':description.value,
    				'extensionStartUIID':startuiid.value,
    				'extensionUIRouting':uirouting.value,
    				'extensionModelRouting':modelrouting.value,
    				'extensionResourceRouting':resourcerouting.value,
    				'extensionURL':extensionurl.value
    			})
    			.success(function() { 
    				$scope.getConfig();
    			});
    };

    configFactory.saveCustomerConfig = function (isAuthenticated, selectedCountry, $scope) {
    	$http.post( urlBase + "/config/customer", { 
    				'authenticated':isAuthenticated,
    				'countryCode':selectedCountry,
    				'ucn':ucn.value
    			})
    			.success(function() { 
    				$scope.getConfig();
    			});
    };

    configFactory.saveDeviceConfig = function (isVOIPEnabled, $scope) {
    	$http.post( urlBase + "/config/device", {
    				'voipEnabled':isVOIPEnabled
    			})
    			.success(function() { 
    				$scope.getConfig();
    			});
    };

    configFactory.saveChannelConfig = function (isInStandIn, $scope) {
    	$http.post( urlBase + "/config/channel", {
    				'vodsStandIn':isInStandIn
    			})
    			.success(function() { 
    				$scope.getConfig();
    			});
    };
    
    configFactory.saveFormData = function ($scope) {
    	$http.post( urlBase + "/data/form", {
    				'key1':key1.value,
    				'value1':value1.value
    			})
    			.success(function() { 
    				$scope.getConfig();
    			});
    };
    
    configFactory.saveTimeoutConfig = function ($scope) {
    	$http.post( urlBase + "/config/timeout", { 
    				'timeout':timeout.value
    			})
    			.success(function() { 
    				$scope.getConfig();
    			});
    };
    
    return( configFactory );
}]);
