package za.co.fnb.pe.admin.config.save;

import za.co.fnb.pe.framework.entity.Entity;

@SuppressWarnings("serial")
public class SimConfig extends Entity {
	// simulator config
	private boolean started;
	private String gomezIP;
	private String festerIP;

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public String getGomezIP() {
		return gomezIP;
	}

	public void setGomezIP(String gomezIP) {
		this.gomezIP = gomezIP;
	}

	public String getFesterIP() {
		return festerIP;
	}

	public void setFesterIP(String festerIP) {
		this.festerIP = festerIP;
	}
}
