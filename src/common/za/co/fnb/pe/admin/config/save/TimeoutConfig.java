package za.co.fnb.pe.admin.config.save;

import za.co.fnb.pe.framework.entity.Entity;

@SuppressWarnings("serial")
public class TimeoutConfig extends Entity {
	// form data
	private String timeout;

	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}
}
