package za.co.fnb.pe.admin.config.save;

import za.co.fnb.pe.framework.entity.Entity;

@SuppressWarnings("serial")
public class DeviceConfig extends Entity {
	// device config
	private boolean voipEnabled;

	public boolean getVoipEnabled() {
		return voipEnabled;
	}

	public void setVoipEnabled(boolean voipEnabled) {
		this.voipEnabled = voipEnabled;
	}
}
