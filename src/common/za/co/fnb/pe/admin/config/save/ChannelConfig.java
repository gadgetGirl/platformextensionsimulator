package za.co.fnb.pe.admin.config.save;

import za.co.fnb.pe.framework.entity.Entity;

@SuppressWarnings("serial")
public class ChannelConfig extends Entity {
	// channel config
	private boolean vodsStandIn;

	public boolean getVodsStandIn() {
		return vodsStandIn;
	}

	public void setVodsStandIn(boolean vodsStandIn) {
		this.vodsStandIn = vodsStandIn;
	}
}
