package za.co.fnb.pe.admin.config.save;

import za.co.fnb.pe.framework.entity.Entity;

@SuppressWarnings("serial")
public class ExtensionConfig extends Entity {
	// extension config
	private String extensionDescription;
	private String extensionStartUIID;
	private String extensionUIRouting;
	private String extensionModelRouting;
	private String extensionResourceRouting;
	private String extensionURL;
	
	public String getExtensionDescription() {
		return extensionDescription;
	}

	public void setExtensionDescription(String extensionDescription) {
		this.extensionDescription = extensionDescription;
	}

	public String getExtensionStartUIID() {
		return extensionStartUIID;
	}

	public void setExtensionStartUIID(String extensionStartUIID) {
		this.extensionStartUIID = extensionStartUIID;
	}

	public String getExtensionUIRouting() {
		return extensionUIRouting;
	}

	public void setExtensionUIRouting(String extensionUIRouting) {
		this.extensionUIRouting = extensionUIRouting;
	}

	public String getExtensionModelRouting() {
		return extensionModelRouting;
	}

	public void setExtensionModelRouting(String extensionModelRouting) {
		this.extensionModelRouting = extensionModelRouting;
	}

	public String getExtensionResourceRouting() {
		return extensionResourceRouting;
	}

	public void setExtensionResourceRouting(String extensionResourceRouting) {
		this.extensionResourceRouting = extensionResourceRouting;
	}

	public String getExtensionURL() {
		return extensionURL;
	}

	public void setExtensionURL(String extensionURL) {
		this.extensionURL = extensionURL;
	}
}
