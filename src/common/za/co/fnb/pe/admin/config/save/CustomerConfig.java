package za.co.fnb.pe.admin.config.save;

import za.co.fnb.pe.framework.entity.Entity;

@SuppressWarnings("serial")
public class CustomerConfig extends Entity {
	// customer config
	private boolean authenticated;
	private String countryCode;
	private long ucn;

	public boolean getAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public long getUcn() {
		return ucn;
	}

	public void setUcn(long ucn) {
		this.ucn = ucn;
	}
}
