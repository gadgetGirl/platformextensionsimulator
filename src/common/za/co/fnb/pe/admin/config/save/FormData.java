package za.co.fnb.pe.admin.config.save;

import za.co.fnb.pe.framework.entity.Entity;

@SuppressWarnings("serial")
public class FormData extends Entity {
	// form data
	private String key1;
	private String value1;
	
	public String getKey1() {
		return key1;
	}
	public void setKey1(String key1) {
		this.key1 = key1;
	}
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}

	
}
