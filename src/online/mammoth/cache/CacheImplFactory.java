package mammoth.cache;


/**
 * Retrieve the cache that is used.
 * @author lmouton
 *
 */
public class CacheImplFactory {
	private static CacheImpl instance = null;
	private static boolean initialised = false;
	private static Object mutex = new Object();
	
	private CacheImplFactory() {
		super();
	}
	
	/**
	 * The configure method allows for the configuration of the DatabaseAccessor on the DatabaseAccessorFactory 
	 * @param accessor the DatabaseAccessor instance
	 */
	public static synchronized void configure(final CacheImpl cacheImple) {
		instance = cacheImple;
	}

	public static CacheImpl getInstance() {
		if (instance == null) {
		    synchronized (mutex) {
		        if(!initialised) {
		            instance = new CustomCache();
		            initialised = true;
		        }
            }
			
		}
		return instance;
	}
}
