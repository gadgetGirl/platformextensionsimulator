package mammoth.cache;

import java.util.Set;

/**
 * Wrappers the interaction with the cache MBean.
 */
public interface CacheImpl {
	/**
	 * Get an object from the cache by unique identifier. The object will still
	 * remain in cache. This method must be called when the put method was used 
	 * to add something to the cache.
	 */
	public Object get(String fqn, Object key) throws CacheImplementationException;

	/**
	 * Put an object on the cache by unique identifier.
	 * Objects  that is added to the cache using this method must be Serializable.
	 */
	public Object put(String fqn, Object key, Object value, boolean removeFromCacheFirst) throws CacheImplementationException;
	/**
	 * Removes an object on the cache by unique identifier.
	 */
	public Object remove(String fqn, Object key) throws CacheImplementationException;

	/**
	 * Get an object from the cache by unique identifier. This method is used with the putObject
	 * method.
	 */
	public Object getObject(String fqn, Object key) throws CacheImplementationException;
	
	/**
	 * Put an object in the cache by unique identifier. This method is used with the getObject
	 * method. 
	 */
	public Object putObject(String fqn, Object key, Object value) throws CacheImplementationException;
	/**
	 * Removes an object on the cache by unique identifier.
	 */
	public Object removeObject(String fqn, Object key) throws CacheImplementationException;
	/**
	 * Clear a specific cache.
	 */
	public void clear(String fqn) throws CacheImplementationException;
	/**
	 * Check if a specified object exists in the cache.
	 */
	public boolean containsKey(String fqn, Object key) throws CacheImplementationException;
	
	/**
	 * Returns all children of a given node
	 */
	public Set getChildrenNames(String fqn) throws CacheImplementationException;
	/**
	 * Returns all keys of a given node
	 */
	public Set getKeys(String fqn) throws CacheImplementationException;
}
