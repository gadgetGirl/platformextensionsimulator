package mammoth.cache;

public class CustomCacheObjectContainer {

	private long lastAccessed;
	private Object contents;
	
	public CustomCacheObjectContainer(Object objectToBeCached) {
		contents = objectToBeCached;
		lastAccessed = System.currentTimeMillis();
	}
	
	public long getLastAccessed() {
		return lastAccessed;
	}
	public void setLastAccessed(long lastAccessed) {
		this.lastAccessed = lastAccessed;
	}
	public Object getContents() {
		return contents;
	}
	public void setContents(Object contents) {
		this.contents = contents;
	}
	
	
}
