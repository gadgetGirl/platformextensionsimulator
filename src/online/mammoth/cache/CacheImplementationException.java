package mammoth.cache;

/**
 * Exception used when something goes wrong in the cache.
 * @author lmouton
 *
 */
public class CacheImplementationException extends Exception {
	private static final long serialVersionUID = 1L;

	public CacheImplementationException(String message) {
        super(message);
    }
	public CacheImplementationException(String message, Exception e) {
        super(message, e);
    }
	public CacheImplementationException(Exception e) {
        super(e);
    }
	
}
