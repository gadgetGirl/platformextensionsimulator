package mammoth.cache;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class CustomCache implements CacheImpl {
	Map theCache = null;
	
	public static final char REMOVE = 'R';
	public static final char CLEAR = 'C';

	@SuppressWarnings("unchecked")
	public CustomCache() {
		super();
		theCache = Collections.synchronizedMap(new HashMap());
		
	}
	
	
	@SuppressWarnings("unchecked")
	private Object put(Map map, StringTokenizer tokenizer, Object key, Object value) throws IOException, ClassNotFoundException {
		
		String token = tokenizer.nextToken();
		Map temp = (Map)map.get(token);
		if (temp == null) {
			temp = Collections.synchronizedMap(new HashMap());
			map.put(token, temp);
		}
		if (tokenizer.hasMoreTokens()) {
			return put(temp, tokenizer, key, value);
		}
		else {
			CustomCacheObjectContainer container = new CustomCacheObjectContainer(value);
			return temp.put(key.toString(), container);
		}
	}
	
	private Object get(Map map, StringTokenizer tokenizer, Object key) throws IOException, ClassNotFoundException {
		String token = tokenizer.nextToken();
		Map temp = (Map)map.get(token);
		if (temp != null) {
			if (tokenizer.hasMoreTokens()) {
				return get(temp, tokenizer, key);
			}
			else {
				CustomCacheObjectContainer container = (CustomCacheObjectContainer)temp.get(key.toString());
				if (container != null) {
					container.setLastAccessed(System.currentTimeMillis());
					return container.getContents();
				} else {
					return null;
				}
			}
		}
		else {
			return null;
		}
	}

	private boolean containsKey(Map map, StringTokenizer tokenizer, Object key) throws IOException, ClassNotFoundException {
		Object byteArray = get(map, tokenizer, key);
		if (byteArray != null) {
			return true;
		}
		else {
			return false;
		}
	}

	private Object remove(Map map, StringTokenizer tokenizer, Object key) throws IOException, ClassNotFoundException {
		String token = tokenizer.nextToken();
		Map temp = (Map)map.get(token);
		if (temp != null) {
			if (tokenizer.hasMoreTokens()) {
				return remove(temp, tokenizer, key);
			}
			else {
				temp.containsKey(key.toString());
				return temp.remove(key.toString());
			}
		}
		else {
			return null;
		}
	}
	
	public void clean(long duration) {
		clean(theCache, duration, "topLevel");
	}
	
	private void clean (Map toBeClean, long duration, String previousKey) {
		Object [] keys = toBeClean.keySet().toArray();
		int cleanCount = 0;
		int leftCount = 0;
		for (Object key : keys) {
			Object value = toBeClean.get(key.toString());
			if (value instanceof Map) {
				clean((Map) value, duration, previousKey+"/"+key.toString());
			} else if (value instanceof CustomCacheObjectContainer) {
				
				if (value != null && ((System.currentTimeMillis() - ((CustomCacheObjectContainer) value).getLastAccessed()) > duration)) {
					toBeClean.remove(key.toString());
					cleanCount++;
				} else {
					leftCount++;
				}
			}
		}
	}

	private void clear(Map map, StringTokenizer tokenizer) {
		String token = tokenizer.nextToken();
		Map temp = (Map)map.get(token);
		if (temp != null) {
			if (tokenizer.hasMoreTokens()) {
				clear(temp, tokenizer);
				if (temp.size() == 0) {
					temp.clear();
					map.remove(token);
				}
			}
			else {
				temp.clear();
				map.remove(token);
			}
		}
	}

	private Set getChildrenNames(Map map, StringTokenizer tokenizer) throws CacheImplementationException {
		if (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			Object temp = map.get(token);
			if (temp != null && temp instanceof Map) {
				if (tokenizer.hasMoreTokens()) {
					return getChildrenNames((Map)temp, tokenizer);
				}
				else {
					return ((Map)temp).keySet();
				}
			}
			else {
				return null;
			}
		}
		else {
			return map.keySet();
		}
	}

	public Object get(String fqn, Object key) throws CacheImplementationException {
//		long before = System.nanoTime();
		try {
			StringTokenizer tokenizer = new StringTokenizer(fqn, "/");
			Object object = get(theCache, tokenizer, key);
			
			if (object != null) {
				return object;
			}
			else {
				return null;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new CacheImplementationException(e);
		}
	}

	public Object put(String fqn, Object key, Object value, boolean removeFromCacheFirst) throws CacheImplementationException {
//		long before = System.nanoTime();
		try {
			StringTokenizer tokenizer = new StringTokenizer(fqn, "/");
			if (removeFromCacheFirst) {
				remove(fqn, key);
			}
			tokenizer = new StringTokenizer(fqn, "/");
			Object object = put(theCache, tokenizer, key, value);
			
			if (object != null) {
				return object;
			}
			else {
				return null;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new CacheImplementationException(e);
		}
	}

	public Object remove(String fqn, Object key) throws CacheImplementationException {
		Object answer = removeLocal(fqn, key);
		return answer;
	}
	
	public void clear(String fqn) throws CacheImplementationException {
		clearLocal(fqn);
	}
	
	public Object removeLocal(String fqn, Object key) throws CacheImplementationException {
//		long before = System.nanoTime();
		try {
			StringTokenizer tokenizer = new StringTokenizer(fqn, "/");
			Object object = remove(theCache, tokenizer, key);
			if (object != null) {
				return object;
			}
			else {
				return null;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new CacheImplementationException(e);
		}
	}

	public void clearLocal(String fqn) throws CacheImplementationException {
//		long before = System.nanoTime();
		try {
			StringTokenizer tokenizer = new StringTokenizer(fqn, "/");
			clear(theCache, tokenizer);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new CacheImplementationException(e);
		}
	}

	public boolean containsKey(String fqn, Object key) throws CacheImplementationException {
//		long before = System.nanoTime();
		try {
			StringTokenizer tokenizer = new StringTokenizer(fqn, "/");
			return containsKey(theCache, tokenizer, key);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new CacheImplementationException(e);
		}
	}

	//This method is un-implemente
	public Set getChildrenNames(String fqn) throws CacheImplementationException {
		StringTokenizer tokenizer = new StringTokenizer(fqn, "/");
		
		return getChildrenNames(theCache, tokenizer);
	}

	//This method is un-implemente
	public Set getKeys(String fqn) throws CacheImplementationException {
		return null;
	}

	//This method is un-implemente
	public Object getObject(String fqn, Object key) throws CacheImplementationException {
		return null;
	}

	//This method is un-implemente
	public Object putObject(String fqn, Object key, Object value) throws CacheImplementationException {
		return null;
	}

	//This method is un-implemente
	public Object removeObject(String fqn, Object key) throws CacheImplementationException {
		return null;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		CustomCache tester = new CustomCache();
		String fqn = "/account/preference/number/";
		StringTokenizer tokenizer = new StringTokenizer(fqn, "/");
		Map cache = Collections.synchronizedMap(new HashMap());
		
		try {
			System.out.println("***** put1");
			tester.put(cache, tokenizer, "12", "TheNumberValue".getBytes());
			System.out.println(cache.toString());
			System.out.println("***** put1\n");
	
			
			System.out.println("***** put2");
			String fqn2 = "/account/preference/";
			StringTokenizer tokenizer2 = new StringTokenizer(fqn2, "/");
			tester.put(cache, tokenizer2, "1", "ThePreferenceValue".getBytes());
			System.out.println(cache.toString());
			System.out.println("***** put2\n");
	
			System.out.println("***** get1");
			tokenizer = new StringTokenizer(fqn, "/");
			Object value = tester.get(cache, tokenizer, "12");
			System.out.println(value.toString());
			System.out.println("***** get1\n");
	
			System.out.println("***** containsKey1");
			tokenizer = new StringTokenizer(fqn, "/");
			boolean contained = tester.containsKey(cache, tokenizer, "12");
			System.out.println(contained);
			System.out.println("***** containsKey1\n");
	
			System.out.println("***** get2");
			tokenizer2 = new StringTokenizer(fqn2, "/");
			value = tester.get(cache, tokenizer2, "1");
			System.out.println(value.toString());
			System.out.println("***** get2\n");
		
			System.out.println("***** remove");
			tokenizer2 = new StringTokenizer(fqn2, "/");
			value = tester.remove(cache, tokenizer2, "1");
			System.out.println(value.toString());
			System.out.println(cache.toString());
			System.out.println("***** remove\n");
	
			System.out.println("***** clear");
			tokenizer = new StringTokenizer(fqn, "/");
			tester.clear(cache, tokenizer);
			System.out.println(cache.toString());
			System.out.println("***** clear\n");
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
