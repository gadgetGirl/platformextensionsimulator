package mammoth.cache;

import java.util.Set;
 
public class CacheViewer {
	private StringBuffer buffer = new StringBuffer();
	CacheImpl cache = null;
	public CacheViewer() {
		super();
	} 

	public void viewCache() {
		try {
			cache = CacheImplFactory.getInstance();
			addNode("");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public StringBuffer getBuffer() {
		return buffer;
	}

	private void addNode(java.lang.String fqn) throws Exception {
		Set children = null;
		String childName = null;
		buffer.append(fqn);
		buffer.append("<br/>");
		children = cache.getChildrenNames(fqn);
		if (children != null) {
			for (java.util.Iterator it = children.iterator(); it.hasNext();) {
				childName = it.next().toString();
				addNode(fqn + "/" + childName);
			}
		}
//		else {
//			Set keys = cache.getKeys(fqn);
//			if (keys != null) {
//				for (java.util.Iterator it = keys.iterator(); it.hasNext();) {
//					buffer.append((String)it.next());
//					buffer.append("<br/>");
//				}
//			}
//			buffer.append("<br/>");
//		}
	}
}
