package mammoth.jsp.viewbean;

import java.sql.Timestamp;

import fnb.online.bifrost.server.bean.ActionViewBean;
import fnb.online.bifrost.server.bean.ViewBean;
import fnb.online.bifrost.server.paging.SortedVector;
import fnb.online.tags.beans.button.ButtonBean;

/**
 * Removed the default display values and changing the pending flag to false for Chameleon... MG
 *
 */
public class ProcessingResultViewBean extends ViewBean implements Comparable<ProcessingResultViewBean>{

	private String processingMessage;

	private String errorMessage;

	private int errorCode;

	private String traceId;
	
	private Timestamp date;
	
	private boolean pending;
	
	private boolean partial;
 	
	private String source;

	public final static String DEFAULT_SUCCESS = "Your transaction was successful";
	
	public final static String DEFAULT_UNSUCCESS = "Your transaction was unsuccessful";
	
	public final static String REQUEST_SUCCESS = "Your request has been sent";

	//private static final String SYSTEM_UNAVAILABLE_PLEASE_TRY_LATER = "System unavailable - Please try later";
	
	private ButtonBean printButton;
	private ButtonBean downloadButton;
	private ButtonBean emailButton;
	
	private SortedVector<ActionViewBean> footerButtons;
	
	public ProcessingResultViewBean() {
		super();
		this.errorCode = 99999;
		this.processingMessage = "";
		this.errorMessage = "";
		this.traceId = "";
		this.pending = false;
		this.partial = false;
	}

	public int getErrorCode() {
		return this.errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getProcessingMessage() {
		return this.processingMessage;
	}

	public void setProcessingMessage(String processingMessage) {
		this.processingMessage = processingMessage;
	}

	public String getTraceId() {
		return this.traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
		if (traceId != null) {
			pending = false;
		}
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public ButtonBean getPrintButton() {
		return printButton;
	}

	public void setPrintButton(ButtonBean printButton) {
		this.printButton = printButton;
	}

	public ButtonBean getDownloadButton() {
		return downloadButton;
	}

	public void setDownloadButton(ButtonBean downloadButton) {
		this.downloadButton = downloadButton;
	}

	public ButtonBean getEmailButton() {
		return emailButton;
	}

	public void setEmailButton(ButtonBean emailButton) {
		this.emailButton = emailButton;
	}

	@Override
	public String toString() {
		return "ProcessingResultViewBean [processingMessage="
				+ processingMessage + ", errorMessage=" + errorMessage
				+ ", errorCode=" + errorCode + ", traceId=" + traceId
				+ ", date=" + date + ", pending=" + pending + ", source="
				+ source + "]";
	}

	@Override
	public int compareTo(ProcessingResultViewBean arg0) {
		return toString().compareTo(arg0.toString());
	}

	public boolean isPartial() {
		return partial;
	}

	public void setPartial(boolean partial) {
		this.partial = partial;
	}

	public SortedVector<ActionViewBean> getFooterButtons() {
		return footerButtons;
	}

	public void setFooterButtons(SortedVector<ActionViewBean> footerButtons) {
		this.footerButtons = footerButtons;
	}
}
