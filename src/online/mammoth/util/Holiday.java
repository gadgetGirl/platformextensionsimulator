/**
 * 
 */
package mammoth.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Holiday implements Comparable<Holiday>{
	/**
	 * 
	 */
	private HolidayRule rule;
	private int month;
	private int day;
	private int originalDay;
	private Date date;
	private Holiday nextHoliday;
	private Holiday previousHoliday;
	private Calendar cal = Calendar.getInstance();
	boolean computed;
	private boolean adjusted; 
	
	public Holiday(HolidayCalendar calendar, HolidayRule rule, int year, int month, int day) {
		this.rule = rule;
		GregorianCalendar newDate = new GregorianCalendar(year, month, day);
		this.month = newDate.get(Calendar.MONTH);
		this.day = newDate.get(calendar.DAY_OF_MONTH);
		this.originalDay = this.day;
		
//		switch(month){
//			case Calendar.JANUARY:
//			case Calendar.FEBRUARY:
//			case Calendar.MARCH:
//			case Calendar.APRIL:
//			case Calendar.MAY:
//			case Calendar.JUNE:
//			case Calendar.JULY:
//			case Calendar.AUGUST:
//			case Calendar.SEPTEMBER:
//			case Calendar.OCTOBER:
//			case Calendar.NOVEMBER:
//			case Calendar.DECEMBER:
//				this.month = month;
//				break;
//				default:
//					throw new IllegalArgumentException("Month must be one of the months in year. Use Calendar.JANUARY etc."); //$NON-NLS-1$
//		}
//		if(day > 0 && day <= 31){
//			this.day = day;
//			this.originalDay = day;
//		}else{
//			throw new IllegalArgumentException("Day must be one of the days in month."); //$NON-NLS-1$
//		}
	}

	public Holiday(HolidayCalendar calendar, int year, int month, int day) {
		this(calendar, HolidayRule.ON_SUNDAY_NEXT_DAY,year,month,day);
	}
	
	public Date getDate(int year) {
		computeDay(year);
		date = cal.getTime();
		return date;
	}
	
	void computeDay(int year) {
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.YEAR, year);
		switch (rule) {
			case ON_SUNDAY_NEXT_DAY:
				if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					cal.add(Calendar.DAY_OF_MONTH, 1);
					day++;
					adjusted = true;
//					if(nextHoliday != null)
//						nextHoliday.adjustDay();
				}
				break;
		}
		computed = true;
	}
	
	private void adjustDay() {
		if(previousHoliday != null && previousHoliday.month == month &&
				previousHoliday.day == day){
			day = day + 1;
			computeDay(cal.get(Calendar.YEAR));
			date = cal.getTime();
		}
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		this.day = day;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return the nextHoliday
	 */
	public Holiday getNextHoliday() {
		return nextHoliday;
	}

	/**
	 * @param nextHoliday the nextHoliday to set
	 */
	public void setNextHoliday(Holiday nextHoliday) {
		this.nextHoliday = nextHoliday;
		if(this.nextHoliday != null){
			this.nextHoliday.setPreviousHoliday(this);
			if(adjusted ){
				this.nextHoliday.adjustDay();
			}
		}
	}

	/**
	 * @return the originalDay
	 */
	public int getOriginalDay() {
		return originalDay;
	}

	/**
	 * @param originalDay the originalDay to set
	 */
	public void setOriginalDay(int originalDay) {
		this.originalDay = originalDay;
	}

	/**
	 * @return the previousHoliday
	 */
	public Holiday getPreviousHoliday() {
		return previousHoliday;
	}

	/**
	 * @param previousHoliday the previousHoliday to set
	 */
	public void setPreviousHoliday(Holiday previousHoliday) {
		this.previousHoliday = previousHoliday;
	}

	/**
	 * @return the rule
	 */
	public HolidayRule getRule() {
		return rule;
	}

	/**
	 * @param rule the rule to set
	 */
	public void setRule(HolidayRule rule) {
		this.rule = rule;
	}

	public int compareTo(Holiday o) {
		if(this.cal.get(Calendar.YEAR) < o.cal.get(Calendar.YEAR)){
			return -1;
		}else if(this.cal.get(Calendar.YEAR) > o.cal.get(Calendar.YEAR)){
			return 1;
		}else if(this.month < o.month){
			return -1;
		}else if(this.month > o.month){
			return 1;
		}else if(this.day < o.day){
			return -1;
		}else if(this.day > o.day){
			return 1;
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {			
		if(!(obj instanceof Holiday))
			return false;
		return this.compareTo((Holiday)obj) == 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if(!computed){
			computeDay(cal.get(Calendar.YEAR));
		}
		return String.format("%1$td %1$tB", cal); //$NON-NLS-1$
	}
			
}