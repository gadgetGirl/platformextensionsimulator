/**
 * 
 */
package mammoth.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

/**
 * @author F3199525
 *
 */
public class HolidayCalendar extends GregorianCalendar {
	private Holiday easterFriday;
	private Holiday easterMonday;
	private static final long serialVersionUID = -2016264543145276349L;
	private Set<Holiday> holidays;
	private Locale locale; 
	
	//default constructor for jsp pages
	public HolidayCalendar() {
	}
   
	public HolidayCalendar(Locale aLocale) {
		super(aLocale);
		locale = aLocale;
	}

	public HolidayCalendar(TimeZone zone, Locale aLocale) {
		super(zone, aLocale);
	}
	
	public static void main(String[] args) {
		HolidayCalendar hc = new HolidayCalendar(new Locale("en","ZA"));
		hc.set(Calendar.YEAR, 2007);
		hc.computeHolidays();
		SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd"); //$NON-NLS-1$
		Date date = null;
		try {
			date = f.parse("20070101"); //$NON-NLS-1$
			System.out.println(hc.isHoliday(date));
			date = f.parse("20070308"); //$NON-NLS-1$
			System.out.println(hc.isHoliday(date));
			date = f.parse("20071225"); //$NON-NLS-1$
			System.out.println(hc.isHoliday(date));
			date = f.parse("20071226"); //$NON-NLS-1$
			System.out.println(hc.isHoliday(date));
			date = f.parse("20070102"); //$NON-NLS-1$
			System.out.println(hc.isHoliday(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(hc.holidays);
	//	hc.calculateEaster(2007);
	}
	
	public void computeHolidays() {
		loadHolidayForLocale(locale);
//		holidays = new TreeSet<Holiday>();
//		Holiday current = new Holiday(Calendar.JANUARY,1);
//		Holiday next = new Holiday(Calendar.MARCH,8);
//		current.setNextHoliday(next);
//		holidays.add(current);
//		holidays.add(next);
//		current = next;
//		next = new Holiday(Calendar.DECEMBER,25);
//		current.setNextHoliday(next);
//		holidays.add(current);
//		holidays.add(next);
//		next = new Holiday(Calendar.DECEMBER,26);
//		current.setNextHoliday(next);
//		holidays.add(current);
//		holidays.add(next);
	}
	
	public boolean isHoliday(Date date){
		Calendar cal = Calendar.getInstance();		
		cal.setTime(date);
		if(holidays == null) computeHolidays();
		for(Holiday hday : holidays){
			if(!hday.computed){
				hday.computeDay(cal.get(Calendar.YEAR));
			}
			if(cal.get(Calendar.MONTH) == hday.getMonth() && cal.get(Calendar.DAY_OF_MONTH)==hday.getDay())
				return true;
		}
		return false;
	}
	/**
	 * This algorithm has been extracted from the "Astronomical Applications - The Date of Easter"
	 * The algorithm is due to J.-M. Oudin (1940).
	 * It calculates the date of easter given a year.
	 * 
	 * @param year
	 */
	private void calculateEaster(int year) {
		int y = year;
		int c = y / 100;
	    int n = y - 19 * ( y / 19 );
	    int k = ( c - 17 ) / 25;
	    int i = c - c / 4 - ( c - k ) / 3 + 19 * n + 15;
	    i = i - 30 * ( i / 30 );
	    i = i - ( i / 28 ) * ( 1 - ( i / 28 ) * ( 29 / ( i + 1 ) )
	        * ( ( 21 - n ) / 11 ) );
	    int j = y + y / 4 + i + 2 - c + c / 4;
	    j = j - 7 * ( j / 7 );
	    int l = i - j;
	    int m = 3 + ( l + 40 ) / 44;
	    int d = l + 28 - 31 * ( m / 4 );
	   
	    //Create Good Friday by subtracting 2 from the easter sunday
	    easterFriday = new Holiday(this, y,  m-1,d-2);
	    //Create Family day or Easter Monday by adding 1 to easter sunday
	    easterMonday = new Holiday(this,y, m-1,d+1);
	}
	
	private void loadHolidayForLocale(Locale locale) {
		String day_list = Holidays.getString("ALL") + "," + Holidays.getString(locale.getCountry()); //$NON-NLS-1$
		// The day is a month and a day separated by space, different days are
		// separated by commas
		String days[] = day_list.split(",");
		holidays = new TreeSet<Holiday>();
		for (String day : days) {
			if (day.equalsIgnoreCase("EASTER")) {
				calculateEaster(get(Calendar.YEAR));
				holidays.add(easterMonday);
				holidays.add(easterFriday);
			} else {
				String hd[] = day.split(" ");
				if (hd.length>1) {
					
				
				Holiday now = new Holiday(this, get(Calendar.YEAR), Integer.parseInt(hd[0])-1, Integer.parseInt(hd[1]));
				holidays.add(now);
				}
			}			
		}
		//Link the holidays
		Holiday current = null;
		for(Holiday day : holidays){
			if(current != null){
				current.setNextHoliday(day);
			}
			current = day;
		}
	}
	
	public Set<Holiday> getHolidays() {
		return holidays;
	}
	
	public boolean isWeekend(){
		int day = new Date().getDay();
		
		if(day == 0 || day == 6){
			return true;
		}else{
			return false;
		}
	}
	
}
