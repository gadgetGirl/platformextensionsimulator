package mammoth.utility;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import fnb.online.tags.beans.table.BeanUtilities;

public class HyphenString {
    
    /**
     * 
     */

    private static Logger log = Logger.getLogger(HyphenString.class.getName()); 
    
	private static final String VBSCRIPT = "VBSCRIPT";

	private static final String CASE_SENSITIVE_PROPERTIES = "CaseSensitive.properties";

	private static final String JAVASCRIPT = "JAVASCRIPT";

	private static final String FROM_CHAR_CODE = ".fromCharCode(";

	private java.lang.String str;

	public final static String FNB = "FNB";
	
	public final static String ZERO_STRING = "0";
	
	public final static String ONE_STRING = "1";	
	
	public final static String TWO_STRING = "2";	
   
	public final static String THREE_STRING = "3"; 
	
	public final static String FOUR_STRING = "4";
	
	public final static String FIVE_STRING = "5";
	
	public final static String SIX_STRING = "6";
	
	public final static String SEVEN_STRING = "7";
	
	public final static String EIGHT_STRING = "8";

	public final static String SPACE_STRING = " ";	

	public final static String DOT_STRING = ".";	

	public final static String EMPTY_STRING = "";	

	public final static String MINUS_ONE_STRING = "-1";	

	public final static String Y_STRING = "Y";		

	public final static String N_STRING = "N";		
	
	public final static String M_STRING = "M";
	
	public final static String O_STRING = "O";

	public final static String YES_STRING = "Yes";		

	public final static String NO_STRING = "No";	
	
	public final static String COMMA_STRING = ",";
	
	public final static String COLON_STRING = ":";
	
	public final static String SEMI_COLON_STRING = ";";
	
	public final static String HYPHEN_STRING = "-";
    
    public final static String FORWARD_SLASH = "/";
    
    public final static String LEFT_BRACKET = "\\(";
    
    public final static String RIGHT_BRACKET = "\\)";
    
    public final static String LEFT_SQUARE_BRACKET = "\\[";
    
    public final static String RIGHT_SQUARE_BRACKET = "\\]";
    
    public final static String UNDERSCORE = "_";
    
    public final static String PLUS = "+";
	
	public final static String QUESTION_MARK_STRING = "?";
	
	public final static String NEWLINE_STRING = "\n";
	
	public final static String CARRIAGE_RETURN = "\r";
	
	public final static String NEWLINE_CHARACTER = "\n";  //Used when sending string to pages.  This is JSP friendly		
	
	public final static String TAB_STRING = "\t";
	
	public final static String TAB_CHARACTER = "\t";  //Used when sending string to pages.  This is JSP friendly
	
	public final static String TRUE = "true";
	
	public final static String FALSE = "false";
        
    public final static String LOCALHOST = "127.0.0.1";

	private static final String HEX_DIGITS = "0123456789ABCDEF";
	
	private static final String NO_SPECIAL = "[a-zA-Z0-9]";
	
	public final static String MAMMOTH_NICENAME = "Online Banking";
	public static final String OTP_BRAND = "smartOTP";
	public static final String OTP_BRAND_HTML = "smartOTP<sup>TM</sup>";
	
	
	public final static String STAR = "*";
	public final static String HASH = "#";
	public final static String PIPE = "|";
	
	public final static String DATE_PATTERN_yyyy_MM_dd = "yyyy-MM-dd";
	public final static String DATE_PATTERN_yyyyMMdd = "yyyyMMdd";
	public final static String DATE_PATTERN_HHmmss = "HH:mm:ss";
	public final static String DATE_PATTERN_yyyyMMddHHmmss = "yyyyMMddHHmmss";
	public final static String DATE_PATTERN_yyyy_MM_dd_time = "yyyy-MM-dd HH:mm:ss";
	public final static String DATE_PATTERN_dd_MM_HHmm = "dd/MM HH:mm";
	public final static String DATE_PATTERN_EEE_dd_MMM_yyyy = "EEE dd MMM yyyy";
	public final static String DATE_PATTERN_EEE_dd_MMM_yy = "EEE dd MMM ''yy";
	
	private final static String DEFAULT_DECIMAL_FORMAT = "#,##0.00;-#,##0.00";	
	private final static String AMOUNT_DECIMAL_FORMAT = "###,###,##0.00";
	public final static String AMOUNT_ZERO = "0.00";
	
   
	// Regex for Email characters
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
    	
	//Account products
	public final static String DDA="DDA";
	public final static String ZFN="ZFN";
	
	// DFM investment account sub productcode
	public final static String AU="AU";
	
	// Regex for ACB characters
	public static final Pattern pattern;
	static {
		String re = "[^a-zA-Z0-9./\\-&\\*,() <>\\+\\$;=@\\?:%\\[\\]\\\\\\^_!\"#'\\n\\r\\f]";
		pattern = Pattern.compile(re);
	}
	
	// Regex for ACB characters  INDIA 83
	public static final Pattern pattern2;
	static {
		String re = "[^a-zA-Z0-9./\\-,() \\+\\?:\\'\\{\\}\\n\\r\\f]";
		pattern2 = Pattern.compile(re);
	} 
	
	public static final Pattern pattern3;
	static{
		String re = NO_SPECIAL;
		pattern3 = Pattern.compile(re);
	}
	
	public static final String CHARACTER_ACB = "[\\^a-zA-Z0-9./\\-&\\*,() <>\\+\\$;=@\\?:%\\[\\]\\\\\\^_!\"#'\\n\\r\\f]";
	
	//public static char PasswordPadChar = 3; //3 etx 
	public static char PasswordPadChar = 126; //126 = ~
	public static String PasswordPadString = PasswordPadChar+""; 
	
	public static String TABLE_HEADING_DAY_TO_DAY = "Day to Day";
	public static String TABLE_HEADING_INVESTMENTS = "Investments";
	public static String TABLE_HEADING_LOANS = "Loans";
	public static String TABLE_HEADING_PAYPAL = "PayPal";
	public static String TABLE_HEADING_CONNECT = "Connect";
	public static String TABLE_HEADING_BALANCE = "Balance";
	public static String TABLE_HEADING_AVAILABLE_BLANCE = "Available Balance";
	public static String TABLE_HEADING_EZI = "Ezi";
	public static String TABLE_HEADING_MORE = "More";
	
	private static final String PLACEHOLDER_REGEX = "(.*)\\{(.*)\\}(.*)";
	private static final String FIELDVALUE_REGEX = "=([^&]+)";
	
	private static DecimalFormat decimalFormat;
	
	private static final Pattern PLACE_HOLDERS_PATTERN_V2 = Pattern.compile("\\{(.*?)\\}");
	
    private static Pattern POPULATE_PLACEHOLDERS_PATTERN = Pattern.compile(PLACEHOLDER_REGEX);
    public static String HTML_SPACE_STRING = "&nbsp;";
	
	static{
		decimalFormat = (DecimalFormat) DecimalFormat.getInstance();
		decimalFormat.setDecimalSeparatorAlwaysShown(true);
		decimalFormat.applyPattern(DEFAULT_DECIMAL_FORMAT);	
		DecimalFormatSymbols decimalFormatSymbols = decimalFormat.getDecimalFormatSymbols();
		decimalFormatSymbols.setGroupingSeparator(' ');
		decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
	}
	
	
	/**
	 * HyphenString constructor comment.
	 */
	public HyphenString() {
		super();
	}
	
	/**
	 * Sets all characters after @ sign to lower case
	 * @param str java.lang.String
	 */
	 public static String formatEmail(String email){
		if (email ==  null || email.trim().length() == 0) {
			return email;
		}
        String[] split = email.trim().split("@");
        return split.length == 2 ? split[0]+"@"+(split[1].toLowerCase()) : split[0];
	 }
	/**
	 * Insert the method's description here.
	 * Creation date: (4/20/01 12:48:50 PM)
	 * @param str java.lang.String
	 */
	public HyphenString(String newStr) {
		str = newStr;
	}
	/**
	 * Return only the name of the class passed to it.  Does not return the
	 * fully-qualified name.
	 *
	 * E.g.  Class String is passed to it and it will return String not
	 * 		 java.lang.String as per normal.
	 *
	 * Creation date: (01/12/10 07:58:33)
	 * @return java.lang.String
	 * @param clazz java.lang.Class
	 */
	public static String className(Class clazz) {
		return clazz.getName().substring(clazz.getName().lastIndexOf(46) + 1);
	}

	/**
	 * Return only the name of the class passed to it.  Does not return the
	 * fully-qualified name.
	 *
	 * E.g.  java.lang.String is passed to it and it will return String not
	 * 		 java.lang.String as per normal.
	 *
	 * Creation date: (01/12/10 07:58:33)
	 * @return java.lang.String
	 * @param className java.lang.String
	 */
	public static String className(String className) {
		String rc = className;
		int idx;
		
		idx = className.lastIndexOf(46);
		if(idx > 0) {
			rc = className.substring(idx + 1);
		}
		return rc;
	}


	/**
	 * This method takes a String and replace all the "funny" characters with more
	 *  exceptable characters for SQL Queries.
	 * @return java.lang.String
	 * @param sqlString java.lang.String
	 */
	public static String fixString(String sqlString) {
		String result = "";
		for (int i = 0; i < sqlString.length(); i++) {
			switch (sqlString.charAt(i)) {
				case '\'' :
					{
						result += " ";
						break;
					}

				case ')' :
					{
						result += ")";
						break;
					}

				case '(' :
					{
						result += "(";
						break;
					}

				case '}' :
					{
						result += "}";
						break;
					}

				case '{' :
					{
						result += "{";
						break;
					}

				case '\\' :
				case '/' :
					{
						result += "/";
						break;
					}

				case ' ' :
					{
						result += " ";
						break;
					}
				case '\"' :
					{
						result += "``";
						break;
					}
				case '<' :
				case '>' :
				{
					result += "_";
					break;
				}
				default :
					{
						result += sqlString.charAt(i);
						break;
					}
			}
		}
		
		result = result.replaceAll(".fromCharCode", UNDERSCORE);
		result = result.replaceAll(".fromcharcode", UNDERSCORE);
		
		int javascript = result.toUpperCase().indexOf(JAVASCRIPT);
		if (javascript >= 0 && javascript < result.length()) {
			result = HyphenString.EMPTY_STRING;
		}
		javascript = result.toUpperCase().indexOf(VBSCRIPT);
		if (javascript >= 0 && javascript < result.length()) {
			result = HyphenString.EMPTY_STRING;
		}
		
		return result;
	}

	/**
	 * This method takes a String and replaces all the whitespaces with the given String
	 *  exceptable characters for SQL Queries.
	 * @return java.lang.String
	 * @param sqlString java.lang.String
	 */
	public static String removeWhiteSpaces(String source, String replaceWith)
	{
	    StringBuilder result = new StringBuilder();
		for (int i = 0; i < source.length(); i++)
		{
			switch (source.charAt(i))
			{
				case ' ' :
				{
					result.append(replaceWith);
					break;
				}
				default :
				{
					result.append(source.charAt(i));
					break;
				}
			}
		}
		return result.toString();
	}
	/**
	 * Insert the method's description here.
	 * Creation date: (4/20/01 12:58:26 PM)
	 * @return java.lang.String
	 */
	public String getString() {
		return str;
	}
	/**
	 * Insert the method's description here.
	 * Creation date: (4/3/01 7:48:52 AM)
	 * @return boolean
	 * @param incomingString java.lang.String
	 */
	public static boolean isNullOrSpaces(String incomingString) {
		boolean nullOrSpaces = true;
		if(incomingString != null){
			for (int i = 0; i < incomingString.length(); i++) {
				if (incomingString.charAt(i) == ' ') {
					//don't do anything
				}
				else {
					nullOrSpaces = false;
					return nullOrSpaces;
				}
			}
		}
		return nullOrSpaces;
	}
	
	
	private static JAXBContext initContext(Object obj) {
        try {
            return javax.xml.bind.JAXBContext.newInstance(obj.getClass());
        } catch (JAXBException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }
	
	
	/**
	 * Returns an XML representation of an object using JAXB. Ensure that object passed has required annotations, i.e XmlRootElement, XmlElement, etc   
	 * @param obj name of the object passed
	 * @return XML String
	 */
	public static String objectToXML(Object obj) {
        String xmlStr = null;

        try {
        	JAXBContext JAXBContext = initContext(obj);
        	final StringWriter stringWriter = new StringWriter();
        	Marshaller marshaller = JAXBContext.createMarshaller();
            
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            // Marshal the javaObject and write the XML to the stringWriter
            marshaller.marshal(obj, stringWriter);
            xmlStr = stringWriter.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return xmlStr;
    }
	
	public static Object toObject(Object obj, String xmlStr) {
        try {
        	String sanitizedString = xmlStr.toString().replaceAll("\\p{Cc}|\r|\n|\r\n", "");
        	JAXBContext JAXBContext = initContext(obj);
            final Unmarshaller unmarshaller = JAXBContext.createUnmarshaller();

            return unmarshaller.unmarshal(new StringReader(sanitizedString));
        } catch (JAXBException e) {
        	e.printStackTrace();
        }

        return null;
    }

	public static void cleanAllStringFields(Object objectToClean) throws IllegalArgumentException, IllegalAccessException{
    	for (Field field: objectToClean.getClass().getDeclaredFields()){
    		if (field.getType() == String.class){
				field.setAccessible(true);
				field.set(objectToClean, new String (((String)field.get(objectToClean)).replaceAll("\\p{Cc}|\r|\n|\r\n", "")));
    		}
    	}
    	
    }
	
	public static void cleanAllStringFields(Object objectToClean, String pattenToRemove, String replacePattern) throws IllegalArgumentException, IllegalAccessException{
    	for (Field field: objectToClean.getClass().getDeclaredFields()){
    		if (field.getType() == String.class){
				field.setAccessible(true);
				field.set(objectToClean, new String (((String)field.get(objectToClean)).replaceAll(pattenToRemove, replacePattern)));
    		}
    	}
    	
    }
	/*public static boolean isNullOrEmpty(String incomingString) 
	{
		boolean nullOrEmpty = true;
		
		if(incomingString != null)
		{
			if (incomingString.trim().equalsIgnoreCase("")) {
				//don't do anything
			}
			else 
			{
				nullOrEmpty = false;
				return nullOrEmpty;
			}
		}
		return nullOrEmpty;
	}*/
	/**
	 * Right justifies the string in the source parameter with character filler to the length <br/>
	 * specified.  The source is left unchanged.
	 * @return: String
	 * @param: source - string to justify 
	 * @param filler - character to fill with 
	 * @param length - required length
	 */
	public static String rightJustify(String source, String filler, int length) {
		int len = source.length();
		if (filler.length() > 1) {
			filler = filler.substring(0, 1);
		}
		
		if (len > length) {
			source = source.substring(len - length);
			len = length;
		}
		
		char fillerAsChar = filler.charAt(0);
		char[] temp = new char[length];
		for (int i = 0; i < length; i++) {
			temp[i] = fillerAsChar;
		}
		try {
			System.arraycopy(source.toCharArray(), 0, temp, (length - len), len);
		}
		catch (Exception e) {
			System.out.println("source = " + source);
		}
		
		return new String(temp);
	}

	/**
	 * Left justifies the string in the source parameter with character filler to the length <br/>
	 * specified.  The source is left unchanged.
	 * @return: String
	 * @param: source - string to justify 
	 * @param filler - character to fill with 
	 * @param length - required length
	 */
	public static String leftJustify(String source, String filler, int length) {
		int len = source.length();
		if (filler.length() > 1) {
			filler = filler.substring(0, 1);
		}
		char fillerAsChar = filler.charAt(0);
		char[] temp = new char[length];
		for (int i = 0; i < length; i++) {
			temp[i] = fillerAsChar;
		}
		
		if (len > length) {
			source = source.substring(0, length);
			len = length;
		}

		System.arraycopy(source.toCharArray(), 0, temp, 0, len);
		
		return new String(temp);
	}
	
	public static String trimLeadingZeros(String newStr) {
		for (int i = 0; i < newStr.length(); i++) {
			if (newStr.charAt(i) == '0')
				continue;
			else
				return newStr.substring(i);
		}

		return "";
	}
	
	public static String toSentenceCase(String value) {
		if (value.length() > 0) {
			value = Character.toUpperCase(value.charAt(0)) + value.substring(1).trim().toLowerCase();
			return value;
		} else
			return value;	
	}
	
	/**
	 * Takes in a string and attempts to parse it to a double, if
	 * this parsing is not successful it returns false. null strings 
	 * will return false.
	 */	
	public static boolean isNumeric(String source) {
		boolean result = false;
		
		if(isValidString(source)) {
			try {
				Double.parseDouble(source);
				result = true;
			}
			catch(Exception e) {}
		}
				
		return result;		
	}
	
	/**
	 * Takes in a string and checks to see that it is not null and 
	 * that its length is greater than zero.  It does not however check
	 * the contents of the string.  It can contain any kind of character.
	 */	
	public static boolean isValidString(String source) {
		return (source != null && source.length() > 0);		
	}
	
	public static String toHexString(byte[] v) 
	{
	    StringBuilder sb = new StringBuilder(v.length * 2);
		for (int i = 0; i < v.length; i++) 
		{
			int b = v[i] & 0xFF;
			sb.append(HEX_DIGITS.charAt(b >>> 4)).append(HEX_DIGITS.charAt(b & 0xF));        
		}
		return sb.toString();    
	}
	
	public static byte [] fromHex(String hexString) {
		byte[] c = new byte[hexString.length() / 2];

		for (int i = 0; i < hexString.length(); i += 2) {
			byte firstByte = charToByte(hexString.charAt(i));
			byte secondByte = charToByte(hexString.charAt(i + 1));

			if (firstByte == -1 || secondByte == -1) {
				return null;
			}

			secondByte += firstByte * 16;

			c[i / 2] = secondByte;
		}

		return c;
	}
	
	private static byte charToByte(char keyChar) {
		byte firstByte = (byte) keyChar;

		if (firstByte >= '0' && firstByte <= '9') {
			firstByte -= '0';
		}
		else if (firstByte >= 'A' && firstByte <= 'F') {
			firstByte = (byte) (firstByte - 'A' + 10);
		}
		else if (firstByte >= 'a' && firstByte <= 'f') {
			firstByte = (byte) (firstByte - 'a' + 10);
		}
		else {
			firstByte = -1;
		}
		return firstByte;
	}
	
	public static String dropSpace(String input) {
		String output = input.trim();
		
		int space = output.indexOf(' '); 
		while (space != -1) {
			output = output.substring(0, space) + output.substring(space+1, output.length());
			space = output.indexOf(' ');
		}
		
		return output;
	}
	
	public static String stripLeadingZeros(String input) {
		while (input.startsWith("0"))
			input = input.substring(1);
		return input;
	}
	/**
	 * Removes any occurances of a spicific character from the source.
	 * 
	 * @return
	 */
	public static String dropCharacter(String input, char character) {
		String output = input.trim();
		
		int space = output.indexOf(character); 
		while (space != -1) {
			output = output.substring(0, space) + output.substring(space+1, output.length());
			space = output.indexOf(character);
		}
		
		return output;
	}
	
	/**
	 * Converts the string <code>value</code> to a BigDecimal using <code>sign</code><br/>
	 * to determine the sign of the return value. <br/>
	 * <code>sign</code> has the following behaviour: <br/>
	 * <table>
	 * <tr><td><b>Value</b></td><td><b>Comment</b></td></tr>
	 * <tr><td>"D"</td><td>Negative value</td></tr>
	 * <tr><td>"  " </td><td>Negative value</td></tr>
	 * <tr><td>"C"</td><td>Positive value</td></tr>
	 * <tr><td>other</td><td>Positive value</td></tr>
	 * </table>
	 * The method will use <code>precision</code> digits at the end of the string as implied decimals.
	 * @return BigDecimal
	 * @param value String
	 * @param sign String
	 * @param precision int
	 */
	public static BigDecimal toBigDecimal(String value, String sign, int precision) 	{	
		String tempValue = value;
		BigDecimal bigD;
		
		// "D" is for all commads except the ones from FirstCard (TCS)
		// they used to use "  ", but now they use "DR"
		if(sign.toUpperCase().equals("D") || sign.toUpperCase().equals("  ") ||  sign.toUpperCase().equals("DR") ||  sign.toUpperCase().equals("DT"))
		{
			tempValue = "-" + value;
		}
				
		bigD = (new BigDecimal(tempValue)).movePointLeft(precision).setScale(precision,BigDecimal.ROUND_UP); 
		
		return bigD;
	}
	
	public static String bigDecimalToString(BigDecimal amount, int randLength, int centLength) {
		String amt = amount.toString();
		int point = amt.indexOf('.');
		if (point == -1) {
			point = amt.length();
		}
		String rands = amt.substring(0, point);
		String cents = HyphenString.EMPTY_STRING;
		if (point < amt.length()) {
			cents = amt.substring(point+1);
		}
		
		return rightJustify(rands, ZERO_STRING, randLength) + leftJustify(cents, ZERO_STRING, centLength);
	}
	
	public static String bigDecimalToStringWithDecimal(BigDecimal amount, int randLength, int centLength) {
		String amt = amount.toString();
		int point = amt.indexOf('.');
		if (point == -1) {
			point = amt.length();
		}
		String rands = amt.substring(0, point);
		String cents = HyphenString.EMPTY_STRING;
		if (point < amt.length()) {
			cents = amt.substring(point+1);
		}
		
		boolean randPartIsZero = false;
		boolean randStringConvertedToInt = false;
		try
		{
			randPartIsZero = Integer.parseInt(rands) == 0 ? true : false;
			
			rands = Integer.parseInt(rands) + EMPTY_STRING;
			
			randStringConvertedToInt = true;
		}
		catch (Exception e)
		{
			rands = ZERO_STRING;
			cents = ZERO_STRING;
		}
		
		if (randPartIsZero || randStringConvertedToInt)
		{
			return rands + DOT_STRING + leftJustify(cents, ZERO_STRING, centLength);
		}
		else
		{
			return rightJustify(rands, ZERO_STRING, randLength) + DOT_STRING + leftJustify(cents, ZERO_STRING, centLength);
		}
	}

	public static String capitaliseFirstChar(String str) {
		str = Character.toUpperCase(str.charAt(0))+str.substring(1, str.length());
		return str;
	}
	
	/**
	 * Takes a string in and checks if each substring value is not a special character. 
	 * If it finds a special character it will return false.
	 */		
	public static boolean containsNoSpecialChars(String str){
		boolean notSpecial = true;
		Pattern specialPattern;
		Matcher specialMatcher;		    
	    str.trim();
	    for(int i=0; i<str.length();i++){
		    specialPattern = Pattern.compile(NO_SPECIAL);
		    specialMatcher = specialPattern.matcher(str.substring(i, i+1));
		    notSpecial = specialMatcher.matches();
		    if (notSpecial == false) {
				break;
		    }
	    }
	    return notSpecial;
	}
	
	public static boolean isEmptyOrNull(String str){
		if(null == str || str.trim().equalsIgnoreCase("")){
			return true;
		}
		return false;
	}
	
	public static boolean isEmptyOrNullOrZero(String str)
	{
		if(null == str || str.trim().equalsIgnoreCase("") || str.trim().equalsIgnoreCase(ZERO_STRING)){
			return true;
		}
		return false;
	}
	
	
	/**
	 * Takes a value in and checks if the contents is all alphabets. 
	 * If it finds a Character or number it will return false.
	 */		
	public static boolean isAlphabetString(String source) {
		
		boolean result = false;
		boolean alphaT  = false;
		
		String[] alpha = {"a","b","c","d","e","f",
						  "g","h","i","j","k","l",
						  "m","n","o","p","q","r",
						  "s","t","u","v","w","x",
						  "y","z"};

		boolean[] checks;
		
		checks = new boolean[source.length()];
		
		
		for(int c=0;c<source.length();c++){
			for(int k=0;k<alpha.length;k++){
			    	
				if(source.substring(c, c+1).equalsIgnoreCase(alpha[k])){
					alphaT = true;
				}
			}
			
			checks[c]=alphaT;
			alphaT = false;
		}
        
		int counter = 0;
		
		for(int j=0;j<source.length();j++){
			if(checks[j]== true){
		      counter++;		
			}
		}
			
		if(counter == source.length()){
			result = true;
		}else{
			result = false;
		}
		
		return result;
				
	}
	
	public static boolean checkEmptyOrNullString(String str){
		boolean result = false;
		if(null != str && !str.trim().equalsIgnoreCase("")){
			result = true;
		}else{
			result = false;
		}
		return result;
	}
	
	public static boolean hasChar(String source) {
		
		boolean result = false;
		boolean alphaT  = false;
				
		String[] alpha = {"~","`","!","@","#","$",
						  "%","^","&","*",
						  "(",")","_","-",
						  "=","+","|","\\",
						  "]","[","{","}",
						  ";",":","'","&quot;",
						  ",","<",".",">",
						  "/","?"};
		


		boolean[] checks;
		
		checks = new boolean[source.length()];
		
		
		for(int c=0;c<source.length();c++){
			for(int k=0;k<alpha.length;k++){
			    	
				if(source.substring(c, c+1).equals(alpha[k])){
					alphaT = true;
				}
			}
			
			checks[c]=alphaT;
			alphaT = false;
		}
        
		int counter = 0;
		
		for(int j=0;j<source.length();j++){
			if(checks[j]== true){
		      counter++;		
			}
		}
		
		for(int m=0;m<source.length();m++){
			if(checks[m] == true){
				result = true;
			}
			
		}
		
		return result;
				
	}
	

	public static String removeNonPrintableChars(String str) {
		String asciiErrorMsg;
		asciiErrorMsg = Normalizer.normalize(str, Normalizer.Form.NFD);

		asciiErrorMsg = asciiErrorMsg.replaceAll("[^\\p{ASCII}]","");
		asciiErrorMsg = asciiErrorMsg.replaceAll("[^A-Za-z0-9\\.\\@_\\-~#\\ ]","");
		return asciiErrorMsg.trim();
	}
	
	public static String currentSystemTime() {
		  String TIME_FORMAT_NOW = "HH:mm:ss";

		  Calendar cal = Calendar.getInstance();
		  SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT_NOW);
		  return sdf.format(cal.getTime());

	}
	
	public static String getFieldValue(String originalString, String fieldName) {
		try{
			Pattern p = Pattern.compile(fieldName + FIELDVALUE_REGEX);
			Matcher m =  p.matcher(originalString);
			if(m.find()){
				return m.group(1);
			}else{
				return "";
			}
		}catch (Exception e){
			System.err.println(e.getMessage());
			return "";
		}
	}
	
	
	// This is a potential replacement for populatePlaceHolders 
	// TODO: replace implementation above with this one
	public static String populatePlaceHolders2(String originalString, Object ...items) {
	    return populatePlaceHolders(originalString,items);
	}
	
	public static String populatePlaceHolders(String originalString, Object ...items) {
	    if(originalString==null || items == null) {
	        return originalString;
	    }
	    try {
	        StringBuffer sb = new StringBuffer();

            Matcher m = PLACE_HOLDERS_PATTERN_V2.matcher(originalString);

            int count = 0;
            while (m.find()) {

                // What to replace
                String toReplace = m.group(1);

                // New value to insert
                String toInsert = null;
                
                for(Object item: items) {
                    if(item==null) {
                        continue;
                    }
                    try{
                        toInsert = BeanUtilities.fieldToString(item,toReplace);
                        break;
                    } catch (IllegalArgumentException|IllegalAccessException|NoSuchFieldException e) {
                        toInsert=null;
                    }
                }
                
                if(toInsert==null) {
                    toInsert="";
//                    if(VODSIMSSession.IS_TEST) log.info("\nYou have a place holder field that is not accessible!" + originalString + " field: " + toReplace);
                }

                // Parse toReplace (you probably want to do something better :)
//	        String[] parts = toReplace.split("\\+");
//	        if (parts.length > 1)
//	            toInsert += parts[1].trim();

                // Append replaced match.
                m.appendReplacement(sb, toInsert);
            }
            m.appendTail(sb);
            
            
            return sb.toString();
        } catch (Exception e) {
            log.log(Level.WARNING,"Error calling populatePlaceHolders on " + originalString,e);
            return originalString;
        } 
	    
	}
	
	public static String populatePlaceHolders(String originalString, String value) {
		try{
			Matcher m =  POPULATE_PLACEHOLDERS_PATTERN.matcher(originalString);
			if(m.matches()){
				String newString = m.group(1);
				newString += value;
				newString += m.group(3);
				//recursive call to ensure we get all the placeholders
				return populatePlaceHolders(newString,value);
			}else{
				return originalString;
			}
		}catch (Exception e){
			System.err.println(e.getMessage());
			return originalString;
		}
	}
	
	public static boolean isEmptyOrNullString(String str){
		boolean result = false;
		if(null == str || str.trim().equalsIgnoreCase("") || "".equals(str)){
			result = true;
		}else{
			result = false;
		}
		return result;
	}
	
	//This method returns the 'display' value for the currency type passed 
	//For now it just caters for ZAR...
	public static String getCurrencyDescription(String str){
		if (str == null){
			str="";
		}
		if ("ZAR".equalsIgnoreCase(str.trim())){
			return "R";
		}
		if ("USD".equalsIgnoreCase(str.trim())){
			return "$";
		}
		if ("GBP".equalsIgnoreCase(str.trim())){
			return "£";
		}
		if ("EB".equalsIgnoreCase(str.trim())){
			return "eB";
		}
		if ("EUR".equalsIgnoreCase(str.trim())){
			return "&euro;";
		}
		if ("INR".equalsIgnoreCase(str.trim())){
			return "Rs";
		}
		if ("AUD".equalsIgnoreCase(str.trim())){
			return "A$";
		}
		if ("NAD".equalsIgnoreCase(str.trim())){
			return "N$";
		}
		if ("BWP".equalsIgnoreCase(str.trim())){
			return "P";
		}
		if ("SZL".equalsIgnoreCase(str.trim())){
			return "E";
		}
		if ("LSL".equalsIgnoreCase(str.trim())){
			return "L";
		}
		if ("TZS".equalsIgnoreCase(str.trim())){
			return "TZ";
		}
		if ("ZMW".equalsIgnoreCase(str.trim())){
			return "ZK";
		}
		if ("ZMK".equalsIgnoreCase(str.trim())){
			return "ZK";
		}
		if ("CNY".equalsIgnoreCase(str.trim())){
			return "&yen;";
		}
		else{
			return str;
		}
	}
	
	public static String formatDate(String date, String fromFormat, String toFormat){
		try {
			SimpleDateFormat defaultFormat = new SimpleDateFormat(fromFormat);
			return formatDate(defaultFormat.parse(date), toFormat);
		} catch (ParseException e) {
			return date;
		}
	}
	
	public static String formatDate(Date date){
		return formatDate(date, DATE_PATTERN_yyyy_MM_dd);
	}
	
	public static String formatDate(Date date, String pattern){
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(date);
	}
	
	public static String formatPrettyDate(Date date){
		return formatDate(date, DATE_PATTERN_EEE_dd_MMM_yyyy);
	}
	public static String getStackTrace(Throwable throwable) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		return sw.getBuffer().toString();
	}
	
	public static String formatPrettyDate(String date){
		if (!HyphenString.isValidString(date)) return date;
		return formatDate(date, DATE_PATTERN_yyyy_MM_dd, DATE_PATTERN_EEE_dd_MMM_yyyy);
	}
	public static String formatPrettyDate(String date,String pattern){
	    if (!HyphenString.isValidString(date)) return date;
	    return formatDate(date, DATE_PATTERN_yyyy_MM_dd, pattern);
	}
	
	public static String formatPrettyDate(Timestamp date){
		if (date == null) return HyphenString.EMPTY_STRING;
		String formattedString = new SimpleDateFormat(DATE_PATTERN_yyyy_MM_dd).format(date);
		return formatDate(formattedString, DATE_PATTERN_yyyy_MM_dd, DATE_PATTERN_EEE_dd_MMM_yyyy);
	}
	
	
	public static String formatTime(String date){
		return formatDate(date, DATE_PATTERN_yyyy_MM_dd_time, DATE_PATTERN_HHmmss);
	}
	
	public static String formatTime(Date date){
		return formatDate(date, DATE_PATTERN_HHmmss);
	}
	
	
	public static String formatDecimal(BigDecimal decimal){
		return decimalFormat.format(decimal);
	}
	
	public static String formatDecimal(String currency, BigDecimal decimal){
		return getCurrencyDescription(currency) + " " + formatDecimal(decimal);
	}
	
	public static String formatAmount(BigDecimal amount) {
		String formattedAmount = "";

		try {
			final DecimalFormat amountFormat = new DecimalFormat(AMOUNT_DECIMAL_FORMAT);
			formattedAmount = amountFormat.format(amount);
		} catch (Exception e) {
		}

		return formattedAmount;
	}


	public static String removeDecimalsAndPoints(String number)
	{
		if (number != null && number.length() > 0)
		{
			number = number.replaceAll(COMMA_STRING, EMPTY_STRING);
			number = number.replaceAll("\\" + DOT_STRING, EMPTY_STRING);
			
			return number;
		}
		else
		{
			return EMPTY_STRING;
		}
	}


	
	/**
	 * Converts an integer to it's ordinal.
	 * @param i
	 * @return String.
	 */
	public static String ordinal(int i) {
	    String[] sufixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
	    switch (i % 100) {
	    case 11:
	    case 12:
	    case 13:
	        return i + "th";
	    default:
	        return i + sufixes[i % 10];

	    }
	}

	public static String commoDateFormat(String date){
		StringBuffer commonDateFormat = new StringBuffer();			
		commonDateFormat.append(date.substring(0,4));
		commonDateFormat.append("-");
		commonDateFormat.append(date.substring(4,6));
		commonDateFormat.append("-");
		commonDateFormat.append(date.substring(6,8));
		return commonDateFormat.toString();
	}
	
	/**
	 * Checks if the value taken in is a valid email address.  
	 * Checks value against regular expression
	 * Returns true if valid email
	 */	
	
	public static boolean isValidEmail(String email){
		
		Pattern emailPattern;
	    Matcher emailMatcher;
	    boolean isMatch = false;
	    
	    emailPattern = Pattern.compile(EMAIL_PATTERN);
	    emailMatcher = emailPattern.matcher(email.trim());
	    isMatch = emailMatcher.matches();
	 
		if (!isMatch) {
			return false;
		}
		else return true;
	}

	
}
