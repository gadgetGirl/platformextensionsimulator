package mammoth.utility;

import java.util.Collection;
import java.util.Comparator;
import java.util.Vector;

import fnb.online.bifrost.server.paging.DefaultComparator;
import fnb.online.bifrost.server.paging.GenericComparator;

/**
 * This class sorts a vector of elements that implement the HyphenComparable interface.
 * 
 * @author: Nico Kotze
 */
public class HyphenSortedVector<ElementType> extends Vector<ElementType> {
	private Comparator<ElementType> comparator = null;
	public static final int NOT_FOUND = -1;

/**
 * HyphenVectorSorter constructor. 
 */
public HyphenSortedVector() {
	super();
}
/**
 * HyphenVectorSorter constructor. 
 */
public HyphenSortedVector(int size) {
	super(size);
}


/**
 * Constructor HyphenSortedVector.
 * @param list
 */
public HyphenSortedVector(Collection<ElementType> collection) {
	super(collection);
}

/**
 * Implements the QuickSort algorithm.
 * Reference: Data Structures, an Object Oriented approach. p 380-384.  William J Collins.
 * 
 * @param first int
 * @param last int
 */
protected void executeQuickSort(int first, int last)
{
	int upCnt, downCnt, pivotIdx;
	ElementType pivotObj;
	
	if(last > first)
	{
		if(last-first > 1) {
			upCnt = first;
			downCnt = last;
			pivotIdx = (first + last) / 2;
			pivotObj = (ElementType)elementData[pivotIdx];
			do{
				// get upCnt
				while(comparator.compare((ElementType)elementData[upCnt], pivotObj) < 0)
				{
					upCnt++;
				} 
				// get downCnt
				while(comparator.compare((ElementType)elementData[downCnt], pivotObj) > 0)
				{
					downCnt--;
				}
	
				// swop
				if(upCnt <= downCnt)
				{
					if(upCnt < downCnt)
					{
						Object temp;
						temp =  elementData[upCnt];
						elementData[upCnt] = elementData[downCnt];
						elementData[downCnt] = temp;
					}
					upCnt++;
					downCnt--;
				}
			}while(upCnt < downCnt);
	
			//recurse
			executeQuickSort(first, downCnt);
			executeQuickSort(upCnt, last);
		}
		else {
			if(comparator.compare((ElementType)elementData[first], (ElementType)elementData[last]) > 0) {
				Object temp;
				temp =  elementData[first];
				elementData[first] = elementData[last];
				elementData[last] = temp;
			}
		}
	}
}

/** Function implementing a binary search
  * Input is a vector and the item of the same type to be found
  */
public int binarySearch(ElementType obj) {
	
	// Initialise high and low values
	int low = 0;
	int high = size() -1;

	// Varaibles to hold the indices of the ends of the  segments
	int half;
	int centre;
		
	while (low <= high) {
		// Find the values of the end points of the segments
		half = (high-low) / 2;
		centre = low + half;
		
		int compared = comparator.compare(obj, get(centre));
		// if x is less than a[firstThird], it occurs in the first third
		if (compared < 0)
			high = centre - 1;

		// if x > a[secondThird], x occurs in the last third
		else if (compared > 0)
			low = centre+1;

		// Test for equality to one of the segement ends
		else 
			return centre;
		}

		// If the function hasn't returned yet, x was not found
		return NOT_FOUND;
	}	

/**
 * Insert the method's description here.
 * Creation date: (12/18/2000 2:33:42 PM)
 * @param args java.lang.String[]
 */
public static void main(String[] args)
{
	//int cnt, j;
	//HyphenSortedVector v = new HyphenSortedVector();

////	for(j=0;j<3;j++)
	//for(cnt=11;cnt>0;cnt--)
	//{
		////if(cnt!=6)
		////{
			//Thing t = new Thing();
			////Random r = new Random();
			////t.i= (- r.nextInt()) + cnt;
			//t.i = cnt;
			//v.addElement(t);
		////}
		////else
		////{
			////Thing t2 = new Thing();
			////t2.i=16;
			////v.addElement(t2);
		////}
	//}     
	//Thing t2 = new Thing();
	//t2.i=-1;
	//v.addElement(t2);

	////t2 = new Thing();
	////t2.i=6;
	////v.addElement(t2);
	
		
	//System.out.println("Before");
	//Enumeration e = v.elements();
	//while(e.hasMoreElements())
	//{
		//Thing t1 = (Thing) e.nextElement();
		//System.out.println(t1.i);
	//}
	
	//v.sort();

	//System.out.println("After");
	//Enumeration e2 = v.elements();
	//while(e2.hasMoreElements())
	//{
		//Thing t1 = (Thing) e2.nextElement();
		//System.out.println(t1.i);
	//}
}
/**
 * Sorts the elements of the vector in ascending order using QuickSort.  
 * 
 */
public void sort() {
	if (comparator == null) {
		comparator = new DefaultComparator<ElementType>();
	}
	
	executeQuickSort(0, elementCount-1);
}

/**
 * The comparator that is going to be used for the sorting.
 * @param comparator The comparator to set
 */
public void setComparator(Comparator<ElementType> comparator) {
	this.comparator = comparator;
}

/** 
 * Return the name of the methoid used to sort the list at present
 */ 
public String getSortMethod() {
	if (comparator == null || !(comparator instanceof GenericComparator))
		return null;
	else
		return ((GenericComparator)comparator).getMethodName();
}

public void reverse() {
	int collectionSize = size();
	for (int i=0; i < collectionSize/2; i++) {
		ElementType temp = get(i);
		set(i, get(collectionSize - i - 1));
		set(collectionSize - i - 1, temp);
	}
	
}

public String[] toStringArray() {
	int collectionSize = size();
	String[] arr = new String[collectionSize];
	for (int i=0; i < collectionSize; i++) {
		ElementType temp = get(i);
		arr[i] = temp.toString();
	}
	return arr;
}

}
