package fnb.online.tags.validation.handlers;

import java.util.Arrays;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsDropdown;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

public class DropdownValidationHandler extends ValidationHandler{
    
    
    private ValidationsDropdown annotation = null;

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
     */
    @Override
    public boolean validate() throws FieldValidationException {
    	displayName = annotation.displayName();
    	String [] invalidOptions = annotation.invalidOptions();
    	if (!HyphenString.isValidString(fieldValue) || fieldValue.equals("-1") || fieldValue.equals("0") || Arrays.asList(invalidOptions).contains(fieldValue)) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please select a " + displayName + HyphenString.DOT_STRING);
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please select a valid " + displayName + HyphenString.DOT_STRING);
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,  "Please select a valid " + displayName + HyphenString.DOT_STRING);
		}
		
		return true;
    }

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java.lang.Object)
     */
    @Override
    public void setAnnotation(Object annotation) {
        this.annotation = (ValidationsDropdown) annotation;
        
    }
    
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}

	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }

}
