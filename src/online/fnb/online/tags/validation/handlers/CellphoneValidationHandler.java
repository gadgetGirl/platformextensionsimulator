/**
 * 
 */
package fnb.online.tags.validation.handlers;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsCellphone;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */
public class CellphoneValidationHandler extends ValidationHandler{

    private ValidationsCellphone annotation = null;

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
     */
    @Override
    public boolean validate() throws FieldValidationException {
    	displayName = annotation.displayName();
    	if (!HyphenString.isValidString(fieldValue)) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is empty.");
		}
    	
    	return true;
    }

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java.lang.Object)
     */
    @Override
    public void setAnnotation(Object annotation) {
        this.annotation = (ValidationsCellphone) annotation;
        
    }
    
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}

	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }

}
