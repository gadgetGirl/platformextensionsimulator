package fnb.online.tags.validation.handlers;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsToAccount;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

public class ToAccountValidationHandler extends ValidationHandler{
    
    
    private ValidationsToAccount annotation = null;

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
     */
    @Override
    public boolean validate() throws FieldValidationException {
    	displayName = annotation.displayName();
    	if (!HyphenString.isValidString(fieldValue)|| fieldValue.equals("-1")) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,"Please select a '" + displayName + "'" + HyphenString.DOT_STRING );
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please select a '" + displayName + "'" + HyphenString.DOT_STRING );
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please select a '" + displayName + "'" + HyphenString.DOT_STRING );
		}
		
		if (!HyphenString.isNumeric(fieldValue)) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please select a valid '" + displayName + "'" + HyphenString.DOT_STRING);
		}
		
		return true;
    }

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java.lang.Object)
     */
    @Override
    public void setAnnotation(Object annotation) {
        this.annotation = (ValidationsToAccount) annotation;
        
    }
    
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }


}
