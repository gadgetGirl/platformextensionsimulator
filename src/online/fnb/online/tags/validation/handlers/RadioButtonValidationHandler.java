/**
 * 
 */
package fnb.online.tags.validation.handlers;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsRadioButton;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 * 
 */
public class RadioButtonValidationHandler extends ValidationHandler {

	private ValidationsRadioButton annotation = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
	 */
	@Override
	public boolean validate() throws FieldValidationException {
		displayName = annotation.displayName();
		if (!HyphenString.isValidString(fieldValue) && annotation.minLength()>0) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,"'" +  displayName + "' not selected.");
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,"'" +  displayName + "' not selected.");
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,"'" +  displayName + "' not selected.");
		}
		
		boolean valid=false;
		boolean empty=true;
		for (String s : annotation.validOptions()) {
			if (!(s.equals(HyphenString.EMPTY_STRING))) {
				empty = false;
				if (fieldValue.equalsIgnoreCase(s)) {
					valid=true;
				}
			}
		}
		if (!empty && !valid) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " not selected.");
		}
		
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java
	 * .lang.Object)
	 */
	@Override
	public void setAnnotation(Object annotation) {
		this.annotation = (ValidationsRadioButton) annotation;

	}
	
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }


}
