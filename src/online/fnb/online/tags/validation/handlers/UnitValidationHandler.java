/**
 * 
 */
package fnb.online.tags.validation.handlers;

import java.math.BigDecimal;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsUnits;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */
public class UnitValidationHandler extends ValidationHandler{

    private ValidationsUnits annotation = null;

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
     */
    @Override
    public boolean validate() throws FieldValidationException {
    	displayName = annotation.displayName();
    	if (!HyphenString.isValidString(fieldValue)||fieldValue.equals("0.00")||fieldValue.equals("0")) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is empty.");
		}
    	BigDecimal value;
    	BigDecimal minValue;
    	BigDecimal maxValue; 
		try {
    		value = new BigDecimal(Double.valueOf(fieldValue));
	    	minValue = new BigDecimal(annotation.minValue());
	    	maxValue = new BigDecimal(annotation.maxValue());
		}
    	catch (Exception e) {
	    	throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is not numeric or contains invalid characters.");
	   	}	
    	if (value.compareTo(minValue) == -1) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' has a minimum value of " +  annotation.minValue() + ".");
		}
		
		if (value.compareTo(maxValue) == -11) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' has a maximum value of " +  annotation.maxValue() + ".");
		}
    	
    	return true;
    	
    }

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java.lang.Object)
     */
    @Override
    public void setAnnotation(Object annotation) {
        this.annotation = (ValidationsUnits) annotation;
        
    }
    
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }
}
