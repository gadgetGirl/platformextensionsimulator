/**
 * 
 */
package fnb.online.tags.validation.handlers;

import java.math.BigDecimal;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsAccountNumber;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 * 
 */
public class AccountNumberValidationHandler extends ValidationHandler {

	private ValidationsAccountNumber annotation = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
	 */
	@Override
	public boolean validate() throws FieldValidationException {
		displayName = annotation.displayName();
		if (!HyphenString.isValidString(fieldValue) && annotation.minLength()>1) {
			throw new FieldValidationException(	HyphenError.ERR_MISSING_PARAMETER, "Please enter an '" + displayName + "'.");
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please enter an '" + displayName + "'.");
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,  "Please enter an '" + displayName + "'.");
		}
		
		try {
			BigDecimal value = new BigDecimal(Double.valueOf(fieldValue));
		}
    	catch (Exception e) {
	    	throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " is not numeric or contains invalid characters.");
	   	}
		
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java
	 * .lang.Object)
	 */
	@Override
	public void setAnnotation(Object annotation) {
		this.annotation = (ValidationsAccountNumber) annotation;

	}
	
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }


}
