/**
 * 
 */
package fnb.online.tags.validation.handlers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsDate;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 * 
 */
public class DateValidationHandler extends ValidationHandler {

	private ValidationsDate annotation = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
	 */
	@Override
	public boolean validate() throws FieldValidationException {
		displayName = annotation.displayName();
		if (!HyphenString.isValidString(fieldValue) && annotation.minLength()>1) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please select a valid " + displayName + HyphenString.DOT_STRING);
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,  "Please select a valid " + displayName + HyphenString.DOT_STRING);
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,  "Please select a valid " + displayName + HyphenString.DOT_STRING);
		}
		
		Date executionDate = null;
			SimpleDateFormat sdf = new SimpleDateFormat(annotation.dateFormat());
			Date today;
			String todayStr = "";
			try {
				executionDate = sdf.parse(fieldValue);
				today = sdf.parse(sdf.format(new Date()));
				todayStr = sdf.format(today);
				
			} catch (ParseException e1) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,  "Please select a valid " + displayName);
			}
		
			if (!annotation.allowToday() && todayStr.equals(fieldValue)) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "The " + displayName + "  entered is today. " + displayName + " can only be effected into the future");
			}
			if (!annotation.allowPast() && (executionDate.before(today))) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "The " + displayName + "  entered is in the past. " + displayName + " can only be effected from today into the future");
			}
			if (!annotation.allowFuture() && executionDate.after(today)) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "The " + displayName + "  entered is in the future. A " + displayName + " cannot be future dated.");
			}
			today.setTime(today.getTime() + (annotation.maxFutureYears() * 365L * 24 * 60 * 60 * 1000));
			if (executionDate.after(today)) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "The " + displayName + "  is more than " + annotation.maxFutureYears() + " years in the future. " + displayName + " can only be scheduled" + annotation.maxFutureYears() + " years into the future");
			}
			
			return true;
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java
	 * .lang.Object)
	 */
	@Override
	public void setAnnotation(Object annotation) {
		this.annotation = (ValidationsDate) annotation;

	}
	
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }

}
