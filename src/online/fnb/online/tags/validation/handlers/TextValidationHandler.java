/**
 * 
 */
package fnb.online.tags.validation.handlers;

import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsText;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 * 
 */
public class TextValidationHandler extends ValidationHandler {

	private ValidationsText annotation = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
	 */
	@Override
	public boolean validate() throws FieldValidationException {
		displayName = annotation.displayName();
		if (fieldValue==null || (fieldValue.length() < 1) && annotation.minLength() > 0) {
			if (annotation.customMessage().length() > 0 ){
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, annotation.customMessage());
			}
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is empty.");
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is not long enough, it should be at least " +  annotation.minLength() + getCharacterString(annotation.minLength()) + " long.");
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is too long , it should be at most " +  annotation.maxLength() + getCharacterString(annotation.maxLength()) + " long.");
		}
		
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java
	 * .lang.Object)
	 */
	@Override
	public void setAnnotation(Object annotation) {
		this.annotation = (ValidationsText) annotation;

	}
	
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }

}
