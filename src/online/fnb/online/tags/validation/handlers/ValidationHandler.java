/**
 * 
 */
package fnb.online.tags.validation.handlers;

import fnb.online.tags.annotations.Validations;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */
public abstract class ValidationHandler {
	
	protected String fieldName = null;
	protected String displayName = null;
	protected String fieldValue = null;
	protected Validations annotation = null;
	protected boolean enabled = true;
	
	private static String CHARACTER = " character";
	private static String CHARACTERS = " characters";
	
	public abstract void setAnnotation(Object annotation);
    
    public abstract boolean validate() throws FieldValidationException;
    
    public void setFieldName(String fieldName){
    	this.fieldName = fieldName;
    }
    
    public void setValue(Object fieldValue) {
    	try {
    		this.fieldValue = (String) fieldValue;
    	}
    	catch (ClassCastException e) {
    		this.fieldValue = fieldValue.toString();
    	}
	}
    
    public String getValue() {
		return (String)this.fieldValue;
	}
    
    public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
    
    public Validations getAnnotation() {
    	return annotation;
    }
    
    
    public boolean isEnabled() {
		return enabled;
	}

    public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }
    
    public String getCharacterString(long characterLength) {
    	if (characterLength > 1) return CHARACTERS;
    			else return CHARACTER;
    }

}
