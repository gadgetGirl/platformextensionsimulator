package fnb.online.tags.validation.handlers;

import java.math.BigDecimal;
import java.util.Arrays;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsOTP;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

public class OTPValidationHandler extends ValidationHandler{
    
    
    private ValidationsOTP annotation = null;

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
     */
    @Override
    public boolean validate() throws FieldValidationException {
    	displayName = annotation.displayName();
    	String [] invalidOptions = annotation.invalidOptions();
    	if (!HyphenString.isValidString(fieldValue) || fieldValue.equals("-1") || fieldValue.equals("0") || Arrays.asList(invalidOptions).contains(fieldValue)) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please enter a valid " + displayName + ".");
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " is not long enough, it should be at least " +  annotation.minLength() + getCharacterString(annotation.minLength()) + " long.");
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " is too long , it should be at most " +  annotation.maxLength() + getCharacterString(annotation.maxLength()) + " long.");
		}
		
		try {
			BigDecimal value = new BigDecimal(Double.valueOf(fieldValue));
		}
    	catch (Exception e) {
	    	throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " is not numeric or contains invalid characters.");
	   	}
		
		return true;
    }

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java.lang.Object)
     */
    @Override
    public void setAnnotation(Object annotation) {
        this.annotation = (ValidationsOTP) annotation;
        
    }
    
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}

	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }

}
