/**
 * 
 */
package fnb.online.tags.validation.handlers;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsCheckBox;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 * 
 */
public class CheckBoxValidationHandler extends ValidationHandler {

	private ValidationsCheckBox annotation = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
	 */
	@Override
	public boolean validate() throws FieldValidationException {
		displayName = annotation.displayName();
		if (fieldValue==null || (!HyphenString.isValidString(fieldValue) && annotation.minLength() > 0)) {
			if (annotation.select()) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please select '" + displayName + "'" + HyphenString.DOT_STRING);
			}
			else if (annotation.customMessage().length() > 0 ) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, annotation.customMessage() );
			}
			else {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please accept '" + displayName + "'"  + HyphenString.DOT_STRING);
			}
				 
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please accept '" + displayName + "'"  + HyphenString.DOT_STRING);
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' submission is too long , it should be at most " +  annotation.maxLength() + " characters long.");
		}
		
		if (!fieldValue.equalsIgnoreCase(annotation.valueWhenTrue())) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please accept '" + displayName  + "'"  + HyphenString.DOT_STRING);
		}
		
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java
	 * .lang.Object)
	 */
	@Override
	public void setAnnotation(Object annotation) {
		this.annotation = (ValidationsCheckBox) annotation;

	}
	
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }

}
