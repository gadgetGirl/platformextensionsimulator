package fnb.online.tags.validation.handlers;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsFromAccount;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

public class FromAccountValidationHandler extends ValidationHandler{
    
    
    private ValidationsFromAccount annotation = null;

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
     */
    @Override
    public boolean validate() throws FieldValidationException {
    	displayName = annotation.displayName();
    	if (!HyphenString.isValidString(fieldValue) || fieldValue.equals("-1")) {
			throw new FieldValidationException(	HyphenError.ERR_MISSING_PARAMETER,"Please select a '" + displayName + "'." );
		}
    	
    	if (!HyphenString.isNumeric(fieldValue)) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please select a valid '" + displayName + "'.");
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' should be at least " +  annotation.minLength() + getCharacterString(annotation.minLength()) + " long.");
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' should be at most " +  annotation.maxLength() + getCharacterString(annotation.maxLength()) + " long.");
		}
		
		if (fieldValue.equals("0")) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "Please select a valid '" + displayName + "'.");
		}
		
		return true;
    }

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java.lang.Object)
     */
    @Override
    public void setAnnotation(Object annotation) {
        this.annotation = (ValidationsFromAccount) annotation;
        
    }
    
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }


}
