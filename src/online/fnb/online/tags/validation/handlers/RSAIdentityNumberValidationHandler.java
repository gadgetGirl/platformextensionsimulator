/**
 * 
 */
package fnb.online.tags.validation.handlers;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsRSAIdentityNumber;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 * 
 */
public class RSAIdentityNumberValidationHandler extends ValidationHandler {

	private ValidationsRSAIdentityNumber annotation = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
	 */
	@Override
	public boolean validate() throws FieldValidationException {
		displayName = annotation.displayName();
		if ((!HyphenString.isValidString(fieldValue) && annotation.minLength()>1)) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + " is empty.");
		}
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + " \"" + fieldValue + "\" is too short. Please check the number and re-enter.");
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER,  "'" + displayName + " \"" + fieldValue + "\" is too long. Please check the number and re-enter.");
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java
	 * .lang.Object)
	 */
	@Override
	public void setAnnotation(Object annotation) {
		this.annotation = (ValidationsRSAIdentityNumber) annotation;
	}
	
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }


}
