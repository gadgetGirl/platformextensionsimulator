/**
 * 
 */
package fnb.online.tags.validation.handlers;

import java.math.BigDecimal;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsBranchCode;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */
public class BranchCodeValidationHandler extends ValidationHandler{

    private ValidationsBranchCode annotation = null;

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
     */
    @Override
    public boolean validate() throws FieldValidationException {
    	displayName = annotation.displayName();
    	if (!HyphenString.isValidString(fieldValue)) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is empty.");
		}
    	if (!HyphenString.isNumeric(fieldValue)) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is not numeric.");
		}
    	BigDecimal value;
    	BigDecimal minValue;
    	BigDecimal maxValue; 
		try {
    		value = new BigDecimal(Double.valueOf(fieldValue));
	    	minValue = new BigDecimal(annotation.minValue());
	    	maxValue = new BigDecimal(annotation.maxValue());
		}
    	catch (Exception e) {
	    	throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " is not numeric or contains invalid characters.");
	   	}	
	    	
    	if (value.compareTo(minValue) == -1) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " has a minimum value of " +String.valueOf((minValue.add(new BigDecimal(1))).intValue()).length() + " digit(s).");
		}
		
		if (value.compareTo(maxValue) == 1) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " has a maximum value of " + String.valueOf(maxValue.intValue()).length() + " digit(s).");
		}
		
		if (fieldValue.length() < (annotation.minLength())) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " has a minimum length of " +String.valueOf((minValue.add(new BigDecimal(1))).intValue()).length() + " digit(s).");
		}
		
		if (fieldValue.length() > (annotation.maxLength())) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + " has a maximum length of " + String.valueOf(maxValue.intValue()).length() + " digit(s).");
		}
    	
    	return true;
    	
    }

    /* (non-Javadoc)
     * @see fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java.lang.Object)
     */
    @Override
    public void setAnnotation(Object annotation) {
        this.annotation = (ValidationsBranchCode) annotation;
        
    }
    
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }


}
