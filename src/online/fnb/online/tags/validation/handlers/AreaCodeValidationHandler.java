/**
 * 
 */
package fnb.online.tags.validation.handlers;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsAreaCode;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 * 
 */
public class AreaCodeValidationHandler extends ValidationHandler {

	private ValidationsAreaCode annotation = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
	 */
	@Override
	public boolean validate() throws FieldValidationException {
		displayName = annotation.displayName();
		if (!HyphenString.isValidString(fieldValue) && annotation.minLength()>0) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is empty.");
		}
		if (annotation.allowBrackets()) {
			fieldValue.replaceAll("\\(", "");
			fieldValue.replaceAll("\\)", "");
		}
		
		if (annotation.allowPlus()) {
			fieldValue.replaceAll("\\+", "");			
		}
		
		if (!HyphenString.isNumeric(fieldValue)) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is not numeric.");
		}
		
		
		if (fieldValue.length() < annotation.minLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + "  should be at least " +  annotation.minLength() + getCharacterString(annotation.minLength()) + " long.");
		}
		
		if (fieldValue.length() > annotation.maxLength()) {
			throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, displayName + "  should be at most " +  annotation.maxLength() + getCharacterString(annotation.maxLength()) + " long.");
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java
	 * .lang.Object)
	 */
	@Override
	public void setAnnotation(Object annotation) {
		this.annotation = (ValidationsAreaCode) annotation;

	}

	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }

}
