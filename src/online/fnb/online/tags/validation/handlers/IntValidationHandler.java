/**
 * 
 */
package fnb.online.tags.validation.handlers;

import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.HyphenError;
import fnb.online.tags.annotations.ValidationsInt;
import fnb.online.tags.annotations.exceptions.FieldValidationException;

/**
 * @author pholmes
 * 
 */
public class IntValidationHandler extends ValidationHandler {

	private ValidationsInt annotation = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fnb.online.tags.validationHandlers.ValidationHandler#validate()
	 */
	@Override
	public boolean validate() throws FieldValidationException {
			
			int value=0;
			displayName = annotation.displayName();
			if (! HyphenString.isValidString(fieldValue)) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is empty.");
			}
			
			try {
				value = Integer.valueOf(fieldValue);
			}catch (Exception e) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' is not a valid integer.");
			}
			
			if (value < annotation.minValue()) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' has a minimum value of " +  annotation.minValue() + ".");
			}
			
			if (value > annotation.maxValue()) {
				throw new FieldValidationException(	HyphenError.ERR_INVALID_PARAMETER, "'" + displayName + "' has a maximum value of " +  annotation.maxValue() + ".");
			}
		
		
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fnb.online.tags.validationHandlers.ValidationHandler#setAnnotation(java
	 * .lang.Object)
	 */
	@Override
	public void setAnnotation(Object annotation) {
		this.annotation = (ValidationsInt) annotation;

	}
	
	@Override
	public boolean isEnabled() {
		return this.annotation.enabled();
	}
	
	@Override
	public String getDependancyField() {
    	return annotation.dependancyField();
    }
    
	@Override
    public String[] getDependencyValues() {
    	return annotation.dependancyValues();
    }

}
