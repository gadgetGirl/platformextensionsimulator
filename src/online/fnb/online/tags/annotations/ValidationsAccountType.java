/**
 * 
 */
package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsAccountType{
	String displayName() default "Account Type";
    long minLength() default 1;
    long maxLength() default 1;
    boolean enabled() default true;
    String dependancyField() default "";
    String[] dependancyValues() default {};
}
