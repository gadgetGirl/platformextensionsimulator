/**
 * 
 */
package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Edzard Bosch
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsRSAIdentityNumber{
    String displayName() default "ID Number";
    double minLength() default 4;
    double maxLength() default 20;
    boolean enabled() default true;
    String dependancyField() default "";
    String[] dependancyValues() default {};
}
