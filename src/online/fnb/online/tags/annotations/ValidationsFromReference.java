/**
 * 
 */
package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsFromReference{
	String displayName() default "From Reference";
    long minLength() default 1;
    long maxLength() default 20;
    boolean enabled() default true;
    String dependancyField() default "";
    String[] dependancyValues() default {};
}

