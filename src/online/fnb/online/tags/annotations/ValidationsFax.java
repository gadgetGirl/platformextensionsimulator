/**
 * 
 */
package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsFax{
    
    String displayName() default "Fax Number";
    long minLength() default 5;
    long maxLength() default 50;
    boolean enabled() default true;
    String dependancyField() default "";
    String[] dependancyValues() default {};

}
