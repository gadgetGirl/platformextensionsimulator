package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FunctionActions {

	//This is for action bar items
	int actionParentRef() default -1;
	//If specified this means load subtabs
	int subTabParentRef() default -1;
	int subTabSelectedRef() default -1;
	//If page has action bar, then build it
	boolean hasActionBar() default false;
	//If page is process flow Hide the menu bar
	boolean hideActionBar() default false;
	//BeanName of stored Bean to use in place holders 
	String beanName() default "parameterViewBean";
	//More Options Label 
	String label() default "More Options";
}
	