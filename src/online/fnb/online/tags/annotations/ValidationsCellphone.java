/**
 * 
 */
package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsCellphone{
    
    String displayName() default "Cellphone Number";
    int minLength() default 5;
    int maxLength() default 10;
    boolean allowDashesChar() default true;
    boolean allowBracketsChar() default false;
    boolean allowPlusChar() default false;
    boolean enabled() default true;
    String dependancyField() default "";
    String[] dependancyValues() default {};

}
