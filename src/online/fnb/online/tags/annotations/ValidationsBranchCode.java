/**
 * 
 */
package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsBranchCode{
    
    String displayName() default "Branch Code";
    int minValue() default 0;
    int maxValue() default 999999;
    int minLength() default 1;
    int maxLength() default 6;
    boolean enabled() default true;
    String dependancyField() default "";
    String[] dependancyValues() default {};

}
