/**
 * 
 */
package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsDate{
    
    String displayName() default "Date";
    int minLength() default 10;
    int maxLength() default 10;
    boolean allowPast() default false;
    boolean allowFuture() default true;
    boolean allowToday() default true;
    int maxFutureYears() default 1;
    boolean enabled() default true;
    String dependancyField() default "";
    String[] dependancyValues() default {};
    String dateFormat() default "yyyy-MM-dd";
}
