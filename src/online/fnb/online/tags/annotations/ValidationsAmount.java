/**
 * 
 */
package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsAmount{
    
    String displayName() default "Amount";
    double minValue() default 0.01;
    double maxValue() default 9999999999.99;
    int decimalPlaces() default 3;
    boolean enabled() default true;
    String dependancyField() default "";
    String[] dependancyValues() default {};
}
