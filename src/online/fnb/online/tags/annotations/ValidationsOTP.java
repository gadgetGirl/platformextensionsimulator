/**
 * 
 */
package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsOTP{
    
	 String displayName() default "OTP Value";
	 boolean enabled() default true;
	 String dependancyField() default "";
	 String[] dependancyValues() default {};
	 int minLength() default 1;
	 int maxLength() default 8;
	 String[] invalidOptions() default {};
}
