package fnb.online.tags.annotations.exceptions;


public class FieldValidationException extends Exception {
	private static final long serialVersionUID = -7427133234095710091L;
	private int errorCode = -1;
	private String message = null;
	
	/*
     * Exception created with provided errorcode and message.
     * Message is NOT populated from the database.
     * 
     */
	
	public FieldValidationException(int errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}
	
	/*
	 * Exception created with errorcode <code>HyphenError.ERR_SYSTEM_TECHNICAL_ERROR</code>
	 * and provided error message
	 * 
	 */
	
	public FieldValidationException(String message) {
	    super(message);
	}
	
	/*
     * Exception created with provided errorcode.
     * Message is populated from the database.
     * 
     */
	public FieldValidationException(int errorCode) {
	    this.errorCode = errorCode;
	}

	public int getErrorCode() {
		return errorCode;
	}
	
	public String getMessage() {
	    String result = null;
	    if(message==null) {
	        result = super.getMessage();
	    }else {
	        result = message;
	    }
	    return result;
	}

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}