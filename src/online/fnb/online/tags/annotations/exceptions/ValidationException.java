/**
 * 
 */
package fnb.online.tags.annotations.exceptions;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */
public class ValidationException extends Exception {

	private static final long serialVersionUID = 5631327448139280474L;
	private HashMap<String, FieldValidationException> fieldValidations = new LinkedHashMap<String, FieldValidationException>();
	
	public ValidationException() {
		super();
	}
	
	public void addFieldValidations(HashMap<String, FieldValidationException> fieldValidations) {
		if (this.fieldValidations == null) this.fieldValidations = new LinkedHashMap<String, FieldValidationException>(); 
		this.fieldValidations.putAll(fieldValidations);
	}

	public HashMap<String, FieldValidationException> getFieldValidations() {
		return fieldValidations;
	}

	public void setFieldValidationException(
			String fieldName, FieldValidationException fieldValidation) {
		if(this.fieldValidations == null){
			this.fieldValidations = new LinkedHashMap<String, FieldValidationException>();
		}
		this.fieldValidations.put(fieldName, fieldValidation);
	}
	
	public void setFieldValidationException(
			String fieldName, int errorCode, String message) {
		if(this.fieldValidations == null){
			this.fieldValidations = new LinkedHashMap<String, FieldValidationException>();
		}
		this.fieldValidations.put(fieldName, new FieldValidationException(errorCode, message));
	}
	
}
