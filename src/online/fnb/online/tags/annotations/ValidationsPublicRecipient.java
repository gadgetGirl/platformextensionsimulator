package fnb.online.tags.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidationsPublicRecipient{
    
    String displayName() default "Public Recipient";
    int minLength() default 1;
    int maxLength() default 40;
    boolean enabled() default true;
    String dependancyField() default "";
    String[] dependancyValues() default {};

}
