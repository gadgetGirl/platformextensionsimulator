/**
 * 
 */
package fnb.online.tags.beans.radiogroup;

/**
 * @author Richard Brimson <RBrimson@fnb.co.za>
 *
 */
public class RadioItem{

    private String id = "";
    private String description = "";
    private String value = "";

	private Boolean selected = false;
	
	private String onclick="";
	
    public RadioItem(){}
    
    public RadioItem(String description, String value){
    	this("", description, value, "");
    }
    
    public RadioItem(String id, String description, String value){
    	this(id, description, value, "");
    }
    public RadioItem(String id, String description, String value, String onClick){
        this.id = id;
        this.description = description;
        this.value = value;
        this.onclick = onClick;
    }

    /**
     * @param id the id to be set
     */
	public String getId() {
		return id;
	}

    /**
     * @return the description
     */
	public void setId(String id) {
		this.id = id;
	}
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the css selector
     */
	public String getValue() {
		return value;
	}
	/**
     * @param css selector the css selector to set
     */
	public void setValue(String value) {
		this.value = value;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getOnclick() {
		return onclick;
	}

	public void setOnclick(String onclick) {
		this.onclick = onclick;
	}

}
