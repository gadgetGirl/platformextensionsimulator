/**
 * 
 */
package fnb.online.tags.beans.radiogroup;

/**
 * @author Richard Brimson <RBrimson@fnb.co.za>
 *
 */
import java.util.ArrayList;
import java.util.List;

import mammoth.utility.HyphenString;
import fnb.online.tags.beans.AbstractItem;

public class RadioGroup extends AbstractItem {
	private String id = "";
	private String name = "";
	private String label = "";
	private String selectedValue = "";
	List<RadioItem> radioItems = new ArrayList<RadioItem>();
	
	public RadioGroup() {
	}
	
	public RadioGroup(String idAndName, String label, String selectedValue, String toolTipContent) {
		this(idAndName,idAndName,label, selectedValue, toolTipContent);
	}
	
	public RadioGroup(String id, String name, String label, String selectedValue, String toolTipMessage) {
		this(id, new ArrayList<RadioItem>(), name, label);
		this.selectedValue = selectedValue;
		this.setToolTipMessage(toolTipMessage);
	}
	
	public RadioGroup(String id, List<RadioItem> radioItems, String name, String label) {
		this.id = id;
		this.radioItems = radioItems;
		this.name = name;
		this.label = label;
	}
	
	/**
     * @return the id
     */
	public String getId() {
		return id;
	}

	/**
     * @param description to set
     */
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
     * @return the radioItems list
     */
	public List<RadioItem> getRadioItems() {
		return radioItems;
	}

	/**
     * @param radioItems list to set
     */
	public void setRadioItems(List<RadioItem> radioItems) {
		this.radioItems = radioItems;
	}

	/**
     * @param radioItem to set
     */
	public void setRadioItem(RadioItem radioItem) {
		if (HyphenString.isEmptyOrNull(radioItem.getId())) {
			radioItem.setId(this.id);
		}
		this.radioItems.add(radioItem);
	}

	public String getSelectedValue() {
		return selectedValue;
	}

	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}

}
