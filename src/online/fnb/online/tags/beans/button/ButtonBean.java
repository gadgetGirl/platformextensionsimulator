package fnb.online.tags.beans.button;



public class ButtonBean 
{
	private static String EMPTY_STRING = "";
	
	private String labelCaption = EMPTY_STRING;
	private String formToSubmit = EMPTY_STRING;
	private String url = EMPTY_STRING;
	private String onClick = EMPTY_STRING;
	
	public ButtonBean() {
		
	}
	
	public ButtonBean(String labelCaption) {
		super();
		this.labelCaption = labelCaption;
	}

	public String getLabelCaption() {
		return labelCaption;
	}

	public String getFormToSubmit() {
		return formToSubmit;
	}

	public void setFormToSubmit(String formToSubmit) {
		this.formToSubmit = formToSubmit;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getOnClick() {
		return onClick;
	}

	public void setOnClick(String onClick) {
		this.onClick = onClick;
	}
	
}