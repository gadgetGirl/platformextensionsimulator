/**
 * 
 */
package fnb.online.tags.beans.actionbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */
public class ActionBlock{
    
    String heading = "";
    int columnIndex = 0;
    
    public int getColumnIndex() {
		return columnIndex;
	}

	public void setColumnIndex(int columnIndex) {
		this.columnIndex = columnIndex;
	}

	TreeMap<Integer, Action> actions = new TreeMap<Integer, Action>();
    
    public ActionBlock() {
    	actions = new TreeMap<Integer, Action>();
    }
    
    public ActionBlock(String heading, int columnIndex) {
    	this.heading = heading;
    	this.columnIndex = columnIndex;
    	actions = new TreeMap<Integer, Action>();
    }
    
    public void addAction(int order, Action action) {
    	if(actions.containsKey(order)){
    		actions.put(actions.lastKey()+1, action);
    	}else{
    		actions.put(order, action);
    	}
    }


    /**
     * @param actionBlocks the actionBlocks to set
     */
    public void setActionBlocks(TreeMap<Integer, Action> actionBlocks) {
        this.actions = actionBlocks;
    }

    /**
     * @return the heading
     */
    public String getHeading() {
        return heading;
    }

    /**
     * @param heading the heading to set
     */
    public void setHeading(String heading) {
        this.heading = heading;
    }

    /**
     * @return the actions
     */
    public List<Action> getActions() {
        return new ArrayList<Action>(actions.values());
    }
    
    public Map<Integer, Action> getActionsMap() {
        return actions;
    }
    
    public ActionBlock clone(){
    	ActionBlock clone = new ActionBlock();
    	clone.heading = this.heading;
    	clone.columnIndex = this.columnIndex;
    	
    	for(Integer order : actions.keySet()){
    		clone.addAction(order, actions.get(order).clone());
    	}
    	
    	return clone;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActionBlock [\n");
		builder.append(heading);
		builder.append(", ");
		builder.append(columnIndex);
		builder.append(", ");
		builder.append(actions);
		builder.append("]\n");
		return builder.toString();
	}
}
