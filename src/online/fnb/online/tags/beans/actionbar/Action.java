/**
 * 
 */
package fnb.online.tags.beans.actionbar;

import fnb.online.tags.beans.table.BeanUtilities;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */
public class Action{
    
    public static final int URL_TO_WORKSPACE = 0;
    public static final int URL_TO_PLACEHOLDER = 1;
    public static final int SHOW_CONTENT = 2;
    
    private String description = "";
    private String url = "";
    private String contentIdentifier = "";
    private int target = 0;
    private String comment = "";
    private int functionRef = 0;
    private boolean selected = false;
    private int functionActionRef = 0;
    
    public Action(){
     
    }
    
    public Action(String description, String url){
        this.description = description;
        this.url = url;
    }
    public Action(String description, String url, int target){
        this(description,url);
        this.target = target;
    }
    public Action(String description, String url, int actionType, String contentIdentifier){
        this(description,url,actionType);
        this.contentIdentifier = contentIdentifier;
    }
    
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }
    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
    /**
     * @return the contentIdentifier
     */
    public String getContentIdentifier() {
        return contentIdentifier;
    }
    /**
     * @param contentIdentifier the contentIdentifier to set
     */
    public void setContentIdentifier(String contentIdentifier) {
        this.contentIdentifier = contentIdentifier;
    }

    public Action clone(){
    	return (Action) BeanUtilities.copyObject(this, new Action());
    }

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getFunctionRef() {
		return functionRef;
	}

	public void setFunctionRef(int functionRef) {
		this.functionRef = functionRef;
	}

	public int getTarget() {
		return target;
	}

	public void setTarget(int target) {
		this.target = target;
	}
	
	public int getFunctionActionRef() {
		return functionActionRef;
	}

	public void setFunctionActionRef(int functionActionRef) {
		this.functionActionRef = functionActionRef;
	}

	public boolean setSelected(int farfn) {
		if (farfn == functionActionRef) {
			selected = true;
		}
		else {
			selected = false;
		}
		return selected;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Action [\n");
		builder.append(description);
		builder.append(", ");
		builder.append(functionRef);
		builder.append("]\n");
		return builder.toString();
	}


}
