/**
 * 
 */
package fnb.online.tags.beans.actionbar;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */
public class ActionBarBean {
    
	public static final int OPTION_COLUMN = 0; 
	public static final int MORE_COLUMN_ONE = 1;
	public static final int MORE_COLUMN_TWO = 2;
	public static final int MORE_COLUMN_THREE = 3;
	
	private int parentFunctionRef = -1;			
	private String optionHeading = "Options";
	private String moreOptionHeading = "More Options";
	private boolean displayMoreOptions = true;
	private boolean hideActionBar = false;
	
	private boolean optionHeadingSet = false;
	
    private Map<Integer, LinkedHashMap<String, ActionBlock>> actionBlocks = new TreeMap<Integer, LinkedHashMap<String, ActionBlock>>();
    
    public void addActionBlock(ActionBlock block) {
    	//if(!block.getActions().isEmpty()){
    		LinkedHashMap<String, ActionBlock> blockMap = actionBlocks.get(block.getColumnIndex());
	    	if(blockMap == null){
	    		blockMap =  new LinkedHashMap<String, ActionBlock>();
	    		actionBlocks.put(block.getColumnIndex(),blockMap);
	    	}
	    	blockMap.put(block.getHeading(), block);
    	//}
    }

    public ActionBarBean() {
    	super();
		actionBlocks = new TreeMap<Integer, LinkedHashMap<String, ActionBlock>>();
	}

	public ActionBarBean(int parentFunctionRef) {
		super();
		this.parentFunctionRef = parentFunctionRef;
		actionBlocks = new TreeMap<Integer, LinkedHashMap<String, ActionBlock>>();
	}

	public List<ActionBlock> getOptions() {
		if(actionBlocks.get(OPTION_COLUMN) != null){
			return new ArrayList<ActionBlock>(actionBlocks.get(OPTION_COLUMN).values());
		}else{
			return new ArrayList<ActionBlock>();
		}
    }
	
	public List<ActionBlock> getMoreOptionsColumnOne() {
		if(actionBlocks.get(MORE_COLUMN_ONE) != null){
			return new ArrayList<ActionBlock>(actionBlocks.get(MORE_COLUMN_ONE).values());
		}else{
			return new ArrayList<ActionBlock>();
		}
    }
	
	public List<ActionBlock> getMoreOptionsColumnTwo() {
		if(actionBlocks.get(MORE_COLUMN_TWO) != null){
			return new ArrayList<ActionBlock>(actionBlocks.get(MORE_COLUMN_TWO).values());
		}else{
			return new ArrayList<ActionBlock>();
		}
    }
	
	public List<ActionBlock> getMoreOptionsColumnThree() {
		if(actionBlocks.get(MORE_COLUMN_THREE) != null){
			return new ArrayList<ActionBlock>(actionBlocks.get(MORE_COLUMN_THREE).values());
		}else{
			return new ArrayList<ActionBlock>();
		}
    }
	
	public ActionBlock get(int columnIndex, String heading) {
		LinkedHashMap<String, ActionBlock> blockMap = actionBlocks.get(columnIndex);
    	if(blockMap != null){
    		return blockMap.get(heading);
    	}else{
    		return null;
    	}
    }
	
	public int getParentFunctionRef() {
		return parentFunctionRef;
	}

	public void setParentFunctionRef(int parentFunctionRef) {
		this.parentFunctionRef = parentFunctionRef;
	}

	public ActionBarBean clone(){
		ActionBarBean clone = new ActionBarBean();
		
		for (ActionBlock block: getOptions()){
			clone.addActionBlock(block.clone());
		}
		for (ActionBlock block: getMoreOptionsColumnOne()){
			clone.addActionBlock(block.clone());
		}
		for (ActionBlock block: getMoreOptionsColumnTwo()){
			clone.addActionBlock(block.clone());
		}
		for (ActionBlock block: getMoreOptionsColumnThree()){
			clone.addActionBlock(block.clone());
		}
		return clone;
	}

	public String getOptionHeading() {
		return optionHeading;
	}

	public void setOptionHeading(String optionHeading) {
		this.optionHeading = optionHeading;
		optionHeadingSet = true;
	}

	public String getMoreOptionHeading() {
		return moreOptionHeading;
	}

	public void setMoreOptionHeading(String moreOptionHeading) {
		this.moreOptionHeading = moreOptionHeading;
	}

	public boolean isDisplayMoreOptions() {
		return displayMoreOptions;
	}

	public void setDisplayMoreOptions(boolean displayMoreOptions) {
		this.displayMoreOptions = displayMoreOptions;
	}

	public void setMoreOptions(ActionBarBean master) {
		for (ActionBlock block: master.getMoreOptionsColumnOne()){
			this.addActionBlock(block.clone());
		}
		for (ActionBlock block: master.getMoreOptionsColumnTwo()){
			this.addActionBlock(block.clone());
		}
		for (ActionBlock block: master.getMoreOptionsColumnThree()){
			this.addActionBlock(block.clone());
		}
	}

	public void removeAll(int columnIndex, List<String> keys) {
		for (String key : keys) {
			actionBlocks.get(columnIndex).remove(key);
		}
	}
	
	public boolean isHideActionBar() {
		return hideActionBar;
	}

	public void setHideActionBar(boolean hideActionBar) {
		this.hideActionBar = hideActionBar;
	}
	
	public boolean isEmpty() {
		return getOptions().isEmpty() && getMoreOptionsColumnOne().isEmpty() && getMoreOptionsColumnTwo().isEmpty() && getMoreOptionsColumnThree().isEmpty();
	}

	public boolean isOptionHeadingSet() {
		return optionHeadingSet;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActionBarBean [\n");
		builder.append(parentFunctionRef);
		builder.append(", ");
		builder.append(optionHeading);
		builder.append(", ");
		builder.append(moreOptionHeading);
		builder.append(", ");
		builder.append(displayMoreOptions);
		builder.append(", ");
		builder.append(hideActionBar);
		builder.append(", ");
		builder.append(actionBlocks);
		builder.append("]\n");
		return builder.toString();
	}
    
}
