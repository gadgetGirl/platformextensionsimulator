package fnb.online.tags.beans.dropdown;

import java.util.LinkedHashMap;

import fnb.online.tags.beans.table.BeanUtilities;

public class DropDown extends AbstractDropdown{
	
	public LinkedHashMap<String, DropDownItem> items;
	//private String selectedValue;
	
	private boolean accountsDropDown=false;

	
	public DropDown() {
		setItemMap(new LinkedHashMap<String, DropDownItem>());
	}
	
	public DropDown(String id, String name) {
		super(id,name);
		setItemMap(new LinkedHashMap<String, DropDownItem>());
	}
	
	
	@Override
	public LinkedHashMap<String, ? extends DropDownItem> getItemMap() {
		return items;
	}

	@Override
	public void setItemMap(LinkedHashMap<String, ? extends DropDownItem> itemsMap) {
		this.items = (LinkedHashMap<String, DropDownItem>) itemsMap;
	}

	@Override
	public DropDownItem getSelectedItem() {
		return items.get(getSelectedValue());
	}

	@Override
	public void addItem(DropDownItem dropdownItem) {
		this.items.put(dropdownItem.getReturnValue(), (DropDownItem) dropdownItem);
	}
	
	public DropDown clone() throws CloneNotSupportedException {
		return (DropDown) BeanUtilities.copyObject(this,  new DropDown());
	}

	/*public String getSelectedValue() {
		return selectedValue;
	}

	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}*/
	
	public void setSelectedValue(int selectedValue) {
		this.selectedValue = String.valueOf(selectedValue);
	}
	public boolean isAccountsDropDown() {
		return accountsDropDown;
	}

	public void setAccountsDropDown(boolean accountsDropDown) {
		this.accountsDropDown = accountsDropDown;
	}
}
