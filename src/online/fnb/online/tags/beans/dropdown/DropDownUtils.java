/**
 * 
 */
package fnb.online.tags.beans.dropdown;


/**
 * @author kabelo
 *
 */
public class DropDownUtils {
	
	
	public static String getItemDisplayValueFromDropdown(String code, DropDown dropdown)
	{
		String value ="";
		
		if(!code.equalsIgnoreCase("0"))
		{
			for(DropDownItem item : dropdown.getItems())
			{
				if(item.getReturnValue().toString().equalsIgnoreCase(code))
				{
					value = item.getDisplayValue().toString();
					break;
				}
			}
		}
		
		return value;
	}

}
