package fnb.online.tags.beans.dropdown;

import java.util.Collection;
import java.util.LinkedHashMap;

import fnb.online.tags.beans.AbstractItem;


public abstract class AbstractDropdown extends AbstractItem implements Cloneable{
	
	private String label = "";
	protected String selectedValue = "";
	private boolean isAccounts=false;
	
	public AbstractDropdown() {}
	
	public AbstractDropdown(String id, String name) {
		super(id, name);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getSelectedValue() {
		return selectedValue;
	}

	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}

	public abstract LinkedHashMap<String, ? extends DropDownItem> getItemMap();
	
	public Collection<? extends DropDownItem> getItems(){
		return getItemMap().values();
	}
	
	public void setItems(Collection<? extends DropDownItem> items){
		for(DropDownItem item : items){
			addItem(item);
		}
	}

	public abstract void setItemMap(LinkedHashMap<String, ? extends DropDownItem> items);
	
	public abstract DropDownItem getSelectedItem() ;
	
	public abstract void addItem(DropDownItem dropdownItem);
	
	public String getSelectedDisplayValue() {
		if(getSelectedItem() != null){
			return getSelectedItem().getDisplayValue();	
		}else{
			return "";
		}
		
	}

	protected abstract AbstractDropdown clone() throws CloneNotSupportedException;

	public boolean isAccounts() {
		return isAccounts;
	}

	public void setAccounts(boolean isAccounts) {
		this.isAccounts = isAccounts;
	}

}
