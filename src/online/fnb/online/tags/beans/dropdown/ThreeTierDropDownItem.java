package fnb.online.tags.beans.dropdown;

public class ThreeTierDropDownItem extends DropDownItem{
	
	private String topHeading = "";
	private String topSubHeading = "";
	
	private String bottomLeftHeading = "";
	private String bottomLeftDisplayValue = "";
	
	private String bottomRightHeading = "";
	private String bottomRightDisplayValue = "";
	
	private Boolean selected = false;
	
	public String getTopHeading() {
		return topHeading;
	}
	public String getTopSubHeading() {
		return topSubHeading;
	}
	public void setTopSubHeading(String topSubHeading) {
		this.topSubHeading = topSubHeading;
	}
	public void setTopHeading(String topHeading) {
		this.topHeading = topHeading;
	}
	public String getBottomLeftHeading() {
		return bottomLeftHeading;
	}
	public void setBottomLeftHeading(String bottomLeftHeading) {
		this.bottomLeftHeading = bottomLeftHeading;
	}
	public String getBottomLeftDisplayValue() {
		return bottomLeftDisplayValue;
	}
	public void setBottomLeftDisplayValue(String bottomLeftDisplayValue) {
		this.bottomLeftDisplayValue = bottomLeftDisplayValue;
	}
	public String getBottomRightHeading() {
		return bottomRightHeading;
	}
	public void setBottomRightHeading(String bottomRightHeading) {
		this.bottomRightHeading = bottomRightHeading;
	}
	public String getBottomRightDisplayValue() {
		return bottomRightDisplayValue;
	}
	public void setBottomRightDisplayValue(String bottomRightDisplayValue) {
		this.bottomRightDisplayValue = bottomRightDisplayValue;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
}
