package fnb.online.tags.beans.dropdown;

import java.util.LinkedHashMap;

import fnb.online.tags.beans.table.BeanUtilities;

public class SingleTierDropDown extends AbstractDropdown{
	private LinkedHashMap<String, SingleTierDropDownItem> items;
	public SingleTierDropDown() {
		setItemMap(new LinkedHashMap<String, SingleTierDropDownItem>());
	}
	
	public SingleTierDropDown(String id, String name) {
		super(id,name);
		setItemMap(new LinkedHashMap<String, SingleTierDropDownItem>());
	}

	@Override
	public LinkedHashMap<String, ? extends DropDownItem> getItemMap() {
		return items;
	}

	@Override
	public void setItemMap(LinkedHashMap<String, ? extends DropDownItem> items) {
		this.items = (LinkedHashMap<String, SingleTierDropDownItem>) items;
	}

	@Override
	public DropDownItem getSelectedItem() {
		return items.get(getSelectedValue());
	}

	@Override
	public void addItem(DropDownItem dropdownItem) {
		this.items.put(dropdownItem.getReturnValue(), (SingleTierDropDownItem) dropdownItem);
	}

	public SingleTierDropDown clone() {
		return (SingleTierDropDown) BeanUtilities.copyObject(this, new SingleTierDropDown());
	}

}
