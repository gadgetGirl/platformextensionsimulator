package fnb.online.tags.beans.dropdown;

import java.util.Vector;

import fnb.online.tags.beans.AbstractItem;

public class OldDropdown extends AbstractItem {

	protected Vector<DropDownItem> items;

	private String selected;
	private String label;

	public OldDropdown() {}
	
	public OldDropdown(String id, String name) {
		super(id, name);
		this.items = new Vector<DropDownItem>();
	}

	public void addItem(DropDownItem item) {
		this.items.add(item);
	}

	/**
	 * @return
	 */
	public String getSelected() {
		return selected;
	}

	/**
	 * @param string
	 */
	public void setSelected(String selected) {
		this.selected = selected;
	}
	
	/**
	 * @param string
	 */
	public void setSelected(int selected) {
		setSelected(String.valueOf(selected));
	}


	/**
	 * @return
	 * 
	 */
	public Vector<DropDownItem> getItems() {
		return items;
	}

	/**
	 * @param vector
	 */
	public void setItems(Vector<DropDownItem> list) {
		items = list;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
