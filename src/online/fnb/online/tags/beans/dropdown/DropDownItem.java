package fnb.online.tags.beans.dropdown;


public class DropDownItem
{
	private String returnValue;
	private String displayValue;
	private String topHeading = "";
	private String topSubHeading = "";
	private String bottomLeftHeading = "";
	private String bottomLeftDisplayValue = "";
	private String bottomRightHeading = "";
	private String bottomRightDisplayValue = "";
	private Boolean selected = false;
	private String balancesUrl ="";

	
	public DropDownItem(){
		
	}
	
	public DropDownItem(String returnValue, String displayValue)
	{
		this.returnValue = returnValue;
		this.displayValue = displayValue;
	}
	
	/**
	 * Returns the displayValue.
	 * @return String
	 */
	public String getDisplayValue()
	{
		return displayValue;
	}


	
	public String getTopHeading() {
		return topHeading;
	}
	public String getTopSubHeading() {
		return topSubHeading;
	}
	public void setTopSubHeading(String topSubHeading) {
		this.topSubHeading = topSubHeading;
	}
	public void setTopHeading(String topHeading) {
		this.topHeading = topHeading;
	}
	public String getBottomLeftHeading() {
		return bottomLeftHeading;
	}
	public void setBottomLeftHeading(String bottomLeftHeading) {
		this.bottomLeftHeading = bottomLeftHeading;
	}
	public String getBottomLeftDisplayValue() {
		return bottomLeftDisplayValue;
	}
	public void setBottomLeftDisplayValue(String bottomLeftDisplayValue) {
		this.bottomLeftDisplayValue = bottomLeftDisplayValue;
	}
	public String getBottomRightHeading() {
		return bottomRightHeading;
	}
	public void setBottomRightHeading(String bottomRightHeading) {
		this.bottomRightHeading = bottomRightHeading;
	}
	public String getBottomRightDisplayValue() {
		return bottomRightDisplayValue;
	}
	public void setBottomRightDisplayValue(String bottomRightDisplayValue) {
		this.bottomRightDisplayValue = bottomRightDisplayValue;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	
	/**
	 * Returns the returnValue.
	 * @return String
	 */
	public String getReturnValue()
	{
		return returnValue;
	}

	/**
	 * Sets the displayValue.
	 * @param displayValue The displayValue to set
	 */
	public void setDisplayValue(String displayValue)
	{
		this.displayValue = displayValue;
	}

	/**
	 * Sets the returnValue.
	 * @param returnValue The returnValue to set
	 */
	public void setReturnValue(String returnValue)
	{
		this.returnValue = returnValue;
	}
	
	/**
	 * Sets the returnValue.
	 * @param returnValue The returnValue to set
	 */
	public void setReturnValue(int returnValue)
	{
		setReturnValue(String.valueOf(returnValue));
	}

	public String getBalancesUrl() {
		return balancesUrl;
	}

	public void setBalancesUrl(String balancesUrl) {
		this.balancesUrl = balancesUrl;
	}
}