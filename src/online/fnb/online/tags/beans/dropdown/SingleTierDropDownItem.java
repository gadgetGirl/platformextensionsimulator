package fnb.online.tags.beans.dropdown;


public class SingleTierDropDownItem extends DropDownItem
{
	
	public SingleTierDropDownItem(){
	}
	
	public SingleTierDropDownItem(String returnValue, String displayValue)
	{
		super(returnValue, displayValue);
	}

}