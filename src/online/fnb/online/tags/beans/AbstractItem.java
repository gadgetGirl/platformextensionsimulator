package fnb.online.tags.beans;


public abstract class AbstractItem {
	private String id = "";
	private String name = "";
	private String toolTipMessage = "";
	private int displayStyle = 0;
	protected String cssClass = "";
	protected int column = 0;
	
	
	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	private Object data;
		
	public AbstractItem() {}
	
	public AbstractItem(String id) {
		this(id, id);
	}
	
	public AbstractItem(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public AbstractItem(String id, String name, int column, String cssClass) {
		super();
		this.id = id;
		this.name = name;
		this.cssClass = cssClass;
		this.column = column;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

	public String getToolTipMessage() {
		return toolTipMessage;
	}

	public void setToolTipMessage(String toolTipMessage) {
		this.toolTipMessage = toolTipMessage;
	}

	public int getDisplayStyle() {
		return displayStyle;
	}

	public void setDisplayStyle(int displayStyle) {
		this.displayStyle = displayStyle;
	}
	
	
}
