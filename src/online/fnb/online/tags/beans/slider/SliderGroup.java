package fnb.online.tags.beans.slider;

import java.util.ArrayList;
import java.util.List;

import fnb.online.tags.beans.AbstractItem;

public class SliderGroup extends AbstractItem {
	
	private String sliderheading = "";
	private List<Slider>  sliders = new ArrayList<Slider>();

	public List<Slider> getSliders() {
		return sliders;
	}

	public void setSliders(List<Slider> sliders) {
		this.sliders = sliders;
	}

	public void addSlider(Slider slider) {
		sliders.add(slider);
	}

	public String getSliderheading() {
		return sliderheading;
	}

	public void setSliderheading(String sliderheading) {
		this.sliderheading = sliderheading;
	}

}
