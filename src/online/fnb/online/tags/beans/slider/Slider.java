package fnb.online.tags.beans.slider;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Slider implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type = "";
	private String label = "";
	private String usage = "";
	private String left = "";
	private String used = "";
	private String color = "";
	private String initialSnapPoint;
	private String snapPointSelected;
	private String disabled = "";
	private String hideButton = "";
	
	private ArrayList<SliderPoint> sliderPoints;
	private int sliderCount;
		
	public static final String COLOR_01 = "#AFCA36"; 
	public static final String COLOR_02 = "#66AFCF";
	public static final String COLOR_03 = "#FD9827";
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getUsage() {
		return usage;
	}
	public void setUsage(String usage) {
		this.usage = usage;
	}
	public String getLeft() {
		return left;
	}
	public void setLeft(String left) {
		this.left = left;
	}
	public String getUsed() {
		return used;
	}
	public void setUsed(String used) {
		this.used = used;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public ArrayList<SliderPoint> getSliderPoints() {
		return sliderPoints;
	}
	public void setSliderPoints(ArrayList<SliderPoint> sliderPoints) {
		this.sliderPoints = sliderPoints;
	}
	public int getSliderCount() {
		return sliderCount;
	}
	public void setSliderCount(int sliderCount) {
		this.sliderCount = sliderCount;
	}
	public String getSnapPointSelected() {
		return snapPointSelected;
	}
	public void setSnapPointSelected(String snapPointSelected) {
		this.snapPointSelected = snapPointSelected;
	}
	public String getDisabled() {
		return disabled;
	}
	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}
	public String getHideButton() {
		return hideButton;
	}
	public void setHideButton(String hideButton) {
		this.hideButton = hideButton;
	}
	public String getInitialSnapPoint() {
		return initialSnapPoint;
	}
	public void setInitialSnapPoint(String initialSnapPoint) {
		this.initialSnapPoint = initialSnapPoint;
	}
	
	public Slider deepCopy() throws IOException, ClassNotFoundException  {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(this);

		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bais);
		return (Slider) ois.readObject();

	}

}
