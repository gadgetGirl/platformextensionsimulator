package fnb.online.tags.beans.slider;

import java.io.Serializable;

public class SliderPoint implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String snapPointLabel = ""; // slider label
	private String buttonLabel = ""; // label of button that moves along top of slider
	private String value = "";
	private String rate = "";
	private String bundleRateLabel = "";
	private String bundleRateValue = "";
	private String productCode = "";
	private String productId = "";
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getSnapPointLabel() {
		return snapPointLabel;
	}
	public void setSnapPointLabel(String snapPointLabel) {
		this.snapPointLabel = snapPointLabel;
	}
	public String getButtonLabel() {
		return buttonLabel;
	}
	public void setButtonLabel(String buttonLabel) {
		this.buttonLabel = buttonLabel;
	}
	public String getBundleRateLabel() {
		return bundleRateLabel;
	}
	public void setBundleRateLabel(String bundleRateLabel) {
		this.bundleRateLabel = bundleRateLabel;
	}
	public String getBundleRateValue() {
		return bundleRateValue;
	}
	public void setBundleRateValue(String bundleRateValue) {
		this.bundleRateValue = bundleRateValue;
	}
	
}
