package fnb.online.tags.beans.date;

import fnb.online.tags.beans.AbstractItem;

/**
 * @author Richard Brimson <RBrimson@fnb.co.za>
 *
 */
public class DatePicker extends AbstractItem {

	private String label = "";
	private String value = "";
	
	public DatePicker(){
		super("","");
	}
	
	public DatePicker(String id, String name) {
		super(id, name);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
