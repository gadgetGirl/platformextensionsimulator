/**
 * 
 */
package fnb.online.tags.beans.tablecontrolsswitcher;

/**
 * @author Richard Brimson <RBrimson@fnb.co.za>
 *
 */
import java.util.ArrayList;
import java.util.List;

import fnb.online.bifrost.server.bean.ActionViewBean;

public class Switcher {
	private String id = "";
	List<ActionViewBean> actions = new ArrayList<ActionViewBean>();
	
	public Switcher() {
	}
	
	public Switcher(List<ActionViewBean> actions) {
        this.actions = actions;
    }
	public Switcher(String id, List<ActionViewBean> actions) {
		this.id = id;
		this.actions = actions;
	}
	
	/**
     * @return the id
     */
	public String getId() {
		return id;
	}

	/**
     * @param description to set
     */
	public void setId(String id) {
		this.id = id;
	}

    /**
     * @return the actions
     */
    public List<ActionViewBean> getActions() {
        return actions;
    }

    /**
     * @param actions the actions to set
     */
    public void setActions(List<ActionViewBean> actions) {
        this.actions = actions;
    }



}
