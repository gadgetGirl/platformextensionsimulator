/**
 * 
 */
package fnb.online.tags.beans.tablecontrolsswitcher;

/**
 *
 */
import java.util.ArrayList;
import java.util.List;

import fnb.online.bifrost.server.bean.ActionViewBean;

public class TableHeaderButtonGroup {
	private String groupHeading = "";
	private List<ActionViewBean> buttons = new ArrayList<ActionViewBean>();
    /**
     * @return the groupHeading
     */
    public String getGroupHeading() {
        return groupHeading;
    }
    /**
     * @param groupHeading the groupHeading to set
     */
    public void setGroupHeading(String groupHeading) {
        this.groupHeading = groupHeading;
    }
    /**
     * @return the buttons
     */
    public List<ActionViewBean> getButtons() {
        return buttons;
    }
    /**
     * @param buttons the buttons to set
     */
    public void setButtons(List<ActionViewBean> buttons) {
        this.buttons = buttons;
    }
	
	}
