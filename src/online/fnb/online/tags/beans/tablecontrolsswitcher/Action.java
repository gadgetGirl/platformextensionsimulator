/**
 * 
 */
package fnb.online.tags.beans.tablecontrolsswitcher;

/**
 * @author Richard Brimson <RBrimson@fnb.co.za>
 *
 */
public class Action{
    
    private String description = "";
    private String selector = "";
    
    public Action(){}
    
    public Action(String description, String selector){
        this.description = description;
        this.selector = selector;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the css selector
     */
	public String getSelector() {
		return selector;
	}
	/**
     * @param css selector the css selector to set
     */
	public void setSelector(String selector) {
		this.selector = selector;
	}

}
