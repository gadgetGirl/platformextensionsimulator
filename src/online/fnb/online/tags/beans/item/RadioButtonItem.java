package fnb.online.tags.beans.item;

import fnb.online.tags.beans.radiogroup.RadioGroup;

public class RadioButtonItem extends TextItem {

	public RadioGroup radioGroup = new RadioGroup();
	private boolean useCount = true;

	public boolean isUseCount() {
		return useCount;
	}

	public void setUseCount(boolean useCount) {
		this.useCount = useCount;
	}

	public RadioButtonItem() {
		super();
	}

	public RadioButtonItem(String id, String name) {
		super(id, name);
	}

	public RadioButtonItem(String id, String name, int columnId) {
		super(id, name, columnId);
	}

	public RadioGroup getRadioGroup() {
		return radioGroup;
	}

	public void setRadioGroup(RadioGroup radioGroup) {
		this.radioGroup = radioGroup;
	}

}