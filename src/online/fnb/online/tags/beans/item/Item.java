package fnb.online.tags.beans.item;

import fnb.online.tags.beans.AbstractItem;

public abstract class Item extends AbstractItem{

	 private String cssClass = "";
	 private int column = 0;

		public Item() {}
		
		public Item(String id) {
			super(id);
		}
		
		public Item(String id, String name) {
			super(id, name);
		}
			
		public Item(String id, String name, int column) {
			super(id, name);
			this.column = column;
		}
		
		public Item(String id, String name, int column, String cssClass) {
			super(id, name, column, cssClass);
		}
		 
		
		public abstract String  getCellValue();
		
		public abstract void  setCellValue(String value);

	    /**
	     * @return the cssClass
	     */
	    public String getCssClass() {
	        return cssClass;
	    }

	    /**
	     * @param cssClass the cssClass to set
	     */
	    public void setCssClass(String cssClass) {
	        this.cssClass = cssClass;
	    }

		public int getColumn() {
			return column;
		}

		public void setColumn(int column) {
			this.column = column;
		}
		
}
