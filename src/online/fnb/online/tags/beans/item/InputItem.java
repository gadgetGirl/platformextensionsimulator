package fnb.online.tags.beans.item;

public  class InputItem extends TextItem {

	private boolean hidden;
	private String inputType;
	private String maxlength;
	
	public InputItem() {
		super();
	}
	
	public InputItem(String id) {
		super(id);
	}
	
	public InputItem(String id, String name) {
		super(id, name);
	}
	
	public InputItem(String id, String name, int column) {
		super(id, name, column);
	}

	public InputItem(String id, String name, boolean hidden) {
		super(id, name);
		this.hidden = hidden;
	}
	
	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getMaxlength() {
		return maxlength;
	}

	public void setMaxlength(String maxlength) {
		this.maxlength = maxlength;
	}
	
	// override TextItem method and set value to null so that text is not set to a space
	@Override
	public void setCellValue(String value) {
		if (value.isEmpty()){
			value = null;
		}
		setText(value);
	}


}
