package fnb.online.tags.beans.item;

import fnb.online.tags.beans.slider.SliderGroup;

public class SliderItem extends TextItem {

	public SliderGroup sliderGroup = new SliderGroup();

	public SliderItem() {
		super();
	}

	public SliderItem(String id, String name) {
		super(id, name);
	}

	public SliderItem(String id, String name, int columnId) {
		super(id, name, columnId);
	}

	public SliderGroup getSliderGroup() {
		return sliderGroup;
	}

	public void setSliderGroup(SliderGroup sliderGroup) {
		this.sliderGroup = sliderGroup;
	}

}