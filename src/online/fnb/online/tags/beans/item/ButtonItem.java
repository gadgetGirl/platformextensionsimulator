package fnb.online.tags.beans.item;


	public class ButtonItem extends HyperLinkItem {
		
		private boolean disabled = false;
		
		public ButtonItem() {
			super();
		}
		
		public ButtonItem(String id, String name) {
			super(id, name);
		}
		
		public ButtonItem(String id, String name, int column) {
			super(id, name, column);
		}

		public boolean isDisabled() {
			return disabled;
		}

		public void setDisabled(boolean disabled) {
			this.disabled = disabled;
		}

		@Override
		public String getUrl() {
			if(!disabled){
				return super.getUrl();
			}else{
				return "";
			}
		}
		
		
		
	}