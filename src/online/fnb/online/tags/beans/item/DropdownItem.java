package fnb.online.tags.beans.item;

import fnb.online.tags.beans.dropdown.DropDown;

public class DropdownItem extends TextItem {

	private boolean disabled = false;

	DropDown dropDown = new DropDown();
	private String onchangeFunc="";

	public DropdownItem() {
		super();
	}

	public DropdownItem(String id, String name, int columnId) {
		super(id, name, columnId);
	}

	public DropDown getDropDown() {
		return dropDown;
	}

	public void setDropDown(DropDown dropDown) {
		this.dropDown = dropDown;
	}

	public String getOnchangeFunc() {
		return onchangeFunc;
	}

	public void setOnchangeFunc(String onchangeFunc) {
		this.onchangeFunc = onchangeFunc;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

}