package fnb.online.tags.beans.item;


public class TextItem extends Item {

private String text = "";
	
	public TextItem() {
		super();
	}

	public TextItem(String id) {
		super(id);
	}
	
	public TextItem(String id, String name) {
		super(id, name);
	}
	
	
	public TextItem(String id, String name, int column) {
		super(id, name, column);
	}
	
	public TextItem(String id, String name,int column, String cssClass) {
		super(id, name, column, cssClass);
	}


	public String getText() {
		return text;
	}

	public void setText(String text) {
		if(text == null){
			this.text = "";
		}else{
			this.text = (!text.trim().isEmpty())?text:"&nbsp;";
			this.text = this.text.replace("/", "&#47;").replace("<", "&#60;").replace(">", "&#62;");
		}
	}
	
	public void setTextWithHtml(String text) {
		if(text == null){
			this.text = "";
		}else{
			this.text = (!text.trim().isEmpty())?text:"&nbsp;";
		}
	}

	@Override
	public String getCellValue() {
		return getText();
	}

	@Override
	public void setCellValue(String value) {
		setText(value);
	}
	
	public String getValue() {
		return getText();
	}

	public void setValue(String value) {
		setText(value);
	}
}
