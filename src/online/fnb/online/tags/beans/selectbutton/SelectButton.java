/**
 * 
 */
package fnb.online.tags.beans.selectbutton;

/**
 * @author Richard Brimson <RBrimson@fnb.co.za>
 *
 */
public class SelectButton{
	private String id = "";
	private String name = "";
    private String value = "";
    private String checked = "";
    private String disabled = "";
    private String onText = "";
    private String offText = "";
    private String selectAllWithin = "";
    private String helpText = "";
    private String onchange = "";
    
    public SelectButton(){}
    
    public SelectButton(String id, String name, String value, String onText, String offText){
    	this.id = id;
    	this.name = name;
    	this.value = value;
    	this.onText = onText;
    	this.offText = offText;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getChecked() {
		return checked;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}

	public String getDisabled() {
		return disabled;
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}

	public String getOnText() {
		return onText;
	}

	public void setOnText(String onText) {
		this.onText = onText;
	}

	public String getOffText() {
		return offText;
	}

	public void setOffText(String offText) {
		this.offText = offText;
	}

	public String getSelectAllWithin() {
		return selectAllWithin;
	}

	public void setSelectAllWithin(String selectAllWithin) {
		this.selectAllWithin = selectAllWithin;
	}

	public String getHelpText() {
		return helpText;
	}

	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

}
