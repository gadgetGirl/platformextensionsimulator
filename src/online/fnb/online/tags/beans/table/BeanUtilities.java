/**
 * 
 */
package fnb.online.tags.beans.table;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import mammoth.cache.CacheImpl;
import mammoth.cache.CacheImplFactory;
import mammoth.cache.CacheImplementationException;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 * 
 */
public class BeanUtilities {

	private static CacheImpl cache = null;
	private static final String FQN_FIND_METHOD = "BEAN_UTILITIES/findMethod";
	private static final String FQN_FIND_FIELD = "BEAN_UTILITIES/findField";
	private static final String FQN_FIND_SETTER_NAME = "BEAN_UTILITIES/findSetterName";
	private static final String FQN_FIND_GETTER_NAME = "BEAN_UTILITIES/findGetterName";
	private static final String FQN_GET_ALL_METHODS = "BEAN_UTILITIES/getAllMethods";
	private static final String FQN_GET_ALL_FIELDS = "BEAN_UTILITIES/getAllFields";
	private static final String FQN_FIND_GETTER_METHOD_FOR_COPY = "BEAN_UTILITIES/findMatchingGetterMethodForCopyObject";
	private static final String FQN_FIND_SETTER_METHOD_FOR_COPY = "BEAN_UTILITIES/findMatchingSetterMethodForCopyObject";

	private static Logger log = Logger.getLogger(BeanUtilities.class.getName());
	
	private static HashMap<Class, Class> types = new HashMap<>();

	static {
		cache = CacheImplFactory.getInstance();
		
		types.put(boolean.class, Boolean.class);
		types.put(char.class, Character.class);
		types.put(byte.class, Byte.class);
		types.put(short.class, Short.class);
		types.put(short.class, Short.class);
		types.put(int.class, Integer.class);
		types.put(long.class, Long.class);
		types.put(float.class, Float.class);
		types.put(double.class, Double.class);
		types.put(void.class, Void.class);
		
	}

	public static HashMap<String, String> getFieldValuePairs(Object viewBean, boolean shouldCamelCase) {

		HashMap<String, String> hashmap = new HashMap<String, String>();

		if (viewBean == null) {
			return hashmap;
		}

		Field[] localFields = viewBean.getClass().getDeclaredFields();
		Field[] superFields = viewBean.getClass().getSuperclass().getDeclaredFields();
		Field[] fields = null;

		if (superFields.length > 0) {
			List<Field> fieldList = new ArrayList<Field>();
			for (int i = 0; i < localFields.length; i++) {
				Field field = localFields[i];
				fieldList.add(field);
			}
			for (int i = 0; i < superFields.length; i++) {
				Field field = superFields[i];
				fieldList.add(field);
			}
			fields = new Field[fieldList.size()];
			fieldList.toArray(fields);
		} else {
			fields = localFields;
		}

		String fieldName = "";
		String fieldValue = "";

		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];

			try {

				fieldName = field.getName();

				fieldValue = String.valueOf(accessField(field).get(viewBean));
				hashmap.put(shouldCamelCase ? stringToTitleCase(fieldName) : fieldName, fieldValue);

			} catch (Exception e) {
				// e.printStackTrace();
				log.log(Level.WARNING, "BeanUtilities: unable to access method " + fieldName + " on bean " + viewBean.getClass().getName() + ": " + e.getMessage());
			}
		}
		return hashmap;
	}

	public static String stringToTitleCase(String incomingString) {
		if (incomingString == null) {
			return "";
		}
		incomingString = incomingString.trim();
		String[] values = incomingString.split(" ");
		StringBuilder buffer = new StringBuilder();
		for (int i = 0; i < values.length; i++) {
			String string = values[i];
			string = string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
			string += " ";
			buffer.append(string);
		}
		return buffer.toString().trim();

	}

	/*---------------------- methods to interrogate fields and methods on an object. MG ----------------------*/

	public static Method findMethod(Class<?> cls, String methodName, Class... paramTypes) throws NoSuchMethodException {

		Method result = null;
		boolean foundInCache = false;

		// Try and find this in cache first
		try {
			if (cls != null && methodName != null && cache.containsKey(FQN_FIND_METHOD, cls.getName() + methodName)) {
				result = (Method) cache.get(FQN_FIND_METHOD, cls.getName() + methodName);
				foundInCache = true;
			}
		} catch (CacheImplementationException e) {
			log.log(Level.WARNING, e.getMessage(), e);
		}

		// if we didn't assign from the cache above
		if (!foundInCache) {
			try {
				result = cls.getDeclaredMethod(methodName, paramTypes);
				try {
					cache.put(FQN_FIND_METHOD, cls.getName() + methodName, result, false);
				} catch (CacheImplementationException e) {
					log.log(Level.WARNING, e.getMessage(), e);
				}

			} catch (NoSuchMethodException ex) {
				if ((cls = cls.getSuperclass()) == null) {
					throw ex;
				}
				result = findMethod(cls, methodName, paramTypes);
			}
		}

		return result;
	}

	public static ArrayList<Method> getAllMethods(Class<?> cls) {
		ArrayList<Method> list = new ArrayList<Method>();
		String clazzName = cls.getName();
		boolean foundInCache = false;
		// Try and find this in cache first
		try {
			if (cache.containsKey(FQN_GET_ALL_METHODS, cls.getName())) {
				list = (ArrayList<Method>) cache.get(FQN_GET_ALL_METHODS, cls.getName());
				foundInCache = true;
			}
		} catch (CacheImplementationException e) {
			log.log(Level.WARNING, e.getMessage(), e);
		}

		while (cls != null && !foundInCache) {
			Method[] methods = cls.getDeclaredMethods();
			for (Method m : methods) {
				list.add(m);
			}
			cls = cls.getSuperclass();
		}

		if (!foundInCache) {
			try {
				cache.put(FQN_GET_ALL_METHODS, clazzName, list, false);
			} catch (CacheImplementationException e) {
				log.log(Level.WARNING, e.getMessage(), e);
			}
		}

		return list;
	}

	public static int methodToInt(Object obj, String methodName) throws NumberFormatException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return Integer.parseInt(String.valueOf(findMethod(obj.getClass(), methodName).invoke(obj)));
	}

	public static boolean methodToBoolean(Object obj, String methodName) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return (Boolean) findMethod(obj.getClass(), methodName).invoke(obj);
	}

	public static BigDecimal methodToBigDecimal(Object obj, String methodName) throws NumberFormatException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return (BigDecimal) (findMethod(obj.getClass(), methodName).invoke(obj));
	}

	public static String methodToString(Object obj, String methodName) throws NumberFormatException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return String.valueOf(findMethod(obj.getClass(), methodName).invoke(obj));
	}

	public static int fieldToInt(Object obj, String fieldName) throws NumberFormatException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
		return Integer.parseInt(String.valueOf(accessField(findField(obj.getClass(), fieldName)).get(obj)));
	}

	public static String fieldToString(Object obj, String fieldName) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
		// The object passed is a map, return the corresponding values from the
		// map
		if (Map.class.isAssignableFrom(obj.getClass())) {
			return (String) ((Map) obj).get(fieldName);
		} else {
			return String.valueOf(accessField(findField(obj.getClass(), fieldName)).get(obj));
		}
	}

	public static Object fieldToObject(Object obj, String fieldName) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
		return accessField(findField(obj.getClass(), fieldName)).get(obj);
	}

	public static String fieldToString(Object obj, Field field) throws IllegalArgumentException, IllegalAccessException {
		return String.valueOf(accessField(field).get(obj));
	}

	public static Field findField(Class<?> cls, String fieldName) throws NoSuchFieldException {
		Field result = null;
		boolean foundInCache = false;

		try {
			// Try and find this in cache first
			if (cls != null && fieldName != null && cache.containsKey(FQN_FIND_FIELD, cls.getName() + fieldName)) {
				result = (Field) cache.get(FQN_FIND_FIELD, cls.getName() + fieldName);
				foundInCache = true;
			}
		} catch (CacheImplementationException e) {
			log.log(Level.WARNING, e.getMessage(), e);
		}

		// if we didn't assign from the cache above
		if (!foundInCache) {
			try {
				result = cls.getDeclaredField(fieldName);
				try {
					cache.put(FQN_FIND_FIELD, cls.getName() + fieldName, result, false);
				} catch (CacheImplementationException e) {
					log.log(Level.WARNING, e.getMessage(), e);
				}
			} catch (NoSuchFieldException ex) {
				if ((cls = cls.getSuperclass()) == null) {
					throw ex;
				}
				result = findField(cls, fieldName);
			}
		}

		return result;
	}

	public static Field accessField(Field field) {
		if (!Modifier.isPublic(field.getModifiers())) {
			field.setAccessible(true);
		}
		return field;
	}

	public static ArrayList<Field> getAllFields(Class<?> cls) {
		ArrayList<Field> list = new ArrayList<Field>();
		String clazzName = cls.getName();
		boolean foundInCache = false;

		// Try and find this in cache first
		try {
			if (cache.containsKey(FQN_GET_ALL_FIELDS, cls.getName())) {
				list = (ArrayList<Field>) cache.get(FQN_GET_ALL_FIELDS, cls.getName());
				foundInCache = true;
			}
		} catch (CacheImplementationException e) {
			log.log(Level.WARNING, e.getMessage(), e);
		}

		while (cls != null && !foundInCache) {
			Field[] flds = cls.getDeclaredFields();
			for (Field f : flds) {
				list.add(f);
			}
			cls = cls.getSuperclass();
		}

		if (!foundInCache) {
			try {
				cache.put(FQN_GET_ALL_FIELDS, clazzName, list, false);
			} catch (CacheImplementationException e) {
				log.log(Level.WARNING, e.getMessage(), e);
			}
		}

		return list;
	}

	public static ArrayList<Field> getAllUniqeNameFields(Class<?> cls) {
		ArrayList<Field> list = new ArrayList<Field>();
		ArrayList<String> fieldNames = new ArrayList<String>();
		ArrayList<Field> flds = getAllFields(cls);
		for (Field f : flds) {
			if (!fieldNames.contains(f.getName())) {
				list.add(f);
				fieldNames.add(f.getName());
			}
		}
		return list;
	}

	/*
	 * public static List<Annotation> findClassAnnotations(Class<?> cls){
	 * List<Annotation> results = new ArrayList<Annotation>();
	 * while (cls != null) {
	 * results.addAll(Arrays.asList(cls.getDeclaredAnnotations()));
	 * cls = cls.getSuperclass();
	 * }
	 * return results;
	 * }
	 */

	/**
	 * Returns the annotations present on the field on the FIRST class in the
	 * class hierarchy of the specified class, c,
	 * that matches the name and type of the specified Field, f.
	 * 
	 * @param cls
	 * @param f
	 * @return an array of annotations if found, otherwise an empty array if the
	 *         field is not found on the class hierarchy
	 * @author michael
	 */
	public static Annotation[] findAnnotationsOnClassHeirarchy(Class<?> c, Field f) {
		Field field;

		try {
			field = findField(c, f.getName());

			if (field.getType().equals(f.getType())) {
				if (field.getAnnotations().length > 0) {
					return field.getAnnotations();
				}
			}
		} catch (SecurityException e) { // ignore the ex
		} catch (NoSuchFieldException e) {// ignore the ex
		}

		return new Annotation[] {};
	}

	public static <T extends Annotation> T findAnnotation(Class<?> cls, Class<T> annotation) {
		T results = cls.getAnnotation(annotation);
		return results;
	}

	public static Object newInstance(String className) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		return Class.forName(className).newInstance();
	}

	public static Object newInstance(Type t) throws InstantiationException, IllegalAccessException {
		return t.getClass().newInstance();
	}

	public static Object newInstance(Class<?> c) throws InstantiationException, IllegalAccessException {
		return c.newInstance();
	}

	public static Object copyObject(Object source, Object destination) {
		return copyObject(source, destination, false, false);
	}

	public static Object copyObject(Object source, Object destination, boolean parseStrings) {
		return copyObject(source, destination, false, false);
	}

	/**
	 * Copies the values of fields on the source object to the corresponding
	 * fields on the destination object, if and
	 * only if the field is present on both objects
	 * 
	 * @param source
	 *            - the source object to copy from
	 * @param destination
	 *            - the destination object to copy to
	 * @param parseStrings
	 *            - if true, copies the STRING VALUE of the source field to the
	 *            destination field
	 * @param ignoreNullValues
	 *            - if true, ignores null fields on the source object
	 * @author michael
	 * @return
	 */
	public static Object copyObject(Object source, Object destination, boolean parseStrings, boolean ignoreNullValues) {

		ArrayList<Field> destFields = getAllFields(destination.getClass());
		Class<?> srcClass = source.getClass();
		Object srcValue;

		long before=System.currentTimeMillis();
		ArrayList<Method> sourceMethods = getAllMethods(source.getClass());
		ArrayList<Method> destMethods = getAllMethods(destination.getClass());
		for (Field destField : destFields) {
			if (!Modifier.isFinal(destField.getModifiers())) {
				try {

					Method getterMethod = findMatchingGetterMethodForCopyObject(sourceMethods, destField,source.getClass());
//					Method getterMethod = null;
//					for (Method m : sourceMethods) {
//		                if (m.getName().equals(findGetterName(destField))) {
//		                    getterMethod = m;
//		                    break;
//		                }
//		            }
//					log.info("getAllMethods time: " + (System.currentTimeMillis()-before));
					if (getterMethod == null) {
						continue;
					}

					srcValue = getterMethod.invoke(source);

					if (ignoreNullValues && srcValue == null) {
						continue;
					}

					Method setterMethod = findMatchingSetterMethodForCopyObject(srcValue, destMethods, destField,source.getClass());
					
//                   Method setterMethod = null;
//
//                    for (Method m : destMethods) {
//                        
//                        boolean isValidSetter = m.getParameterTypes().length==1;
//
//                        if (isValidSetter && m.getName().equals(findSetterName(destField))) {
//                            isValidSetter = isValidSetter && (srcValue==null 
//                                    || getBoxedType(srcValue.getClass()).isAssignableFrom(
//                                            getBoxedType(m.getParameterTypes()[0]))
//                                    || getBoxedType(m.getParameterTypes()[0]).isInstance(srcValue)
//                                            );
//                            if (isValidSetter) {
//                                setterMethod = m;
//                                break;
//                            }
//                        }
//                    }
					
					if (setterMethod == null) {
						continue;
					}

					setterMethod.invoke(destination, srcValue);

				} catch (IllegalAccessException e) {
					System.err.println("BeanUtilities: cannot access field " + destField.getName() + " on source object; " + e.toString());
				} catch (SecurityException e) {
					System.err.println("BeanUtilities: security exception accessing field " + destField.getName() + " on source object; " + e.toString());
				} catch (IllegalArgumentException | InvocationTargetException e) {
					// ignore - field doesnt exist on source
					//log.log(Level.WARNING,e.getMessage(),e);
				}
			}
		}
//		log.info("copy time: " + (System.currentTimeMillis()-before));
		return destination;

	}

    /**
     * @param srcValue
     * @param destMethods
     * @param destField
     * @return
     */
    private static Method findMatchingSetterMethodForCopyObject(Object srcValue, ArrayList<Method> destMethods, Field destField,Class clazz) {
        Method setterMethod = null;
        
        try {
            setterMethod = (Method) cache.get(FQN_FIND_SETTER_METHOD_FOR_COPY, destField.getName()+clazz.getName());
        } catch (CacheImplementationException e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        if(setterMethod==null) {

            for (Method m : destMethods) {
            	
            	boolean isValidSetter = m.getParameterTypes().length==1;
    
            	if (isValidSetter && m.getName().equals(findSetterName(destField))) {
            		isValidSetter = isValidSetter && (srcValue==null 
            				|| getBoxedType(srcValue.getClass()).isAssignableFrom(
            						getBoxedType(m.getParameterTypes()[0]))
            				|| getBoxedType(m.getParameterTypes()[0]).isInstance(srcValue)
            						);
            		if (isValidSetter) {
            			setterMethod = m;
            			break;
            		}
            	}
            }
         // put in cache
            try {
                cache.put(FQN_FIND_SETTER_METHOD_FOR_COPY,destField.getName()+clazz.getName(),setterMethod,false);
            } catch (CacheImplementationException e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
        }
        return setterMethod;
    }

    /**
     * @param methodList
     * @param destField
     * @return
     */
    private static Method findMatchingGetterMethodForCopyObject(ArrayList<Method> methodList, Field destField,Class clazz) {
        
        Method getterMethod = null;
//        log.info("looking for "+destField.hashCode());
        try {
            getterMethod = (Method) cache.get(FQN_FIND_GETTER_METHOD_FOR_COPY, destField.getName()+clazz.getName());
        } catch (CacheImplementationException e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        if(getterMethod==null) {
//            log.info("looking for "+methodList.hashCode());
            for (Method m : methodList) {
            	if (m.getName().equals(findGetterName(destField))) {
            		getterMethod = m;
            		break;
            	}
            }
            // put in cache
            try {
                cache.put(FQN_FIND_GETTER_METHOD_FOR_COPY,destField.getName()+clazz.getName(),getterMethod,false);
            } catch (CacheImplementationException e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
        }
        return getterMethod;
    }

	public static String findGetterName(Field field) {
		String methodName = null;
		
		try {
            // Try and find this in cache first
            if (field != null && cache.containsKey(FQN_FIND_GETTER_NAME, field)) {
                methodName = (String) cache.get(FQN_FIND_GETTER_NAME, field);
            }
        } catch (CacheImplementationException e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
		
		// was not in cache
        if(methodName==null) {
            
    		String fieldName = field.getName();
    		String fieldType = field.getType().getName();
    		if (fieldType.indexOf("boolean") >= 0) {
    			methodName = "is" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    		} else {
    			methodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    		}
    		try {
                cache.put(FQN_FIND_GETTER_NAME,field,methodName,false);
            } catch (CacheImplementationException e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
    		
        }

		return methodName;

	}

	public static String findSetterName(Field field) {
	    String methodName = null;
	    try {
            // Try and find this in cache first
            if (field != null && cache.containsKey(FQN_FIND_SETTER_NAME, field)) {
                methodName = (String) cache.get(FQN_FIND_SETTER_NAME, field);
            }
        } catch (CacheImplementationException e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
	    // was not in cache
	    if(methodName==null) {
	        String fieldName = field.getName();
	        methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	        try {
                cache.put(FQN_FIND_SETTER_NAME,field,methodName,false);
            } catch (CacheImplementationException e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
	    }
		
		return methodName;

	}

	public static void main(String args[]) {
		// System.out.println(BeanUtilities.findAnnotation(AccountMaintainSimpleViewNavigator.class,
		// NavigatorAnnotation.class));
	    
	    findChangesBetweenListsTest(); 
	}
	
	
	private static void findChangesBetweenListsTest() {
	       class Object1 {
	            String field1 = "";
	            String field2 = "";
	            String field3 = "";
	            String field4 = "";
	        }
	       
	       
	       List<Object1> list1 = new ArrayList<>();
	       Object1 o1 = new Object1();
	       o1.field1="field1";
	       o1.field2="field2";
	       o1.field3="field3";
	       o1.field4="field4";
	       list1.add(o1);
	       List<Object1> list2 = new ArrayList<>();
	       Object1 o2 = new Object1();
	       o2.field1="field1";
	       o2.field2="field4";
	       o2.field3="field3";
	       o2.field4="field4";
	       list2.add(o2);
	       
	       
	       
	       System.out.println("findChangesBetweenLists" + findChangesBetweenLists(list1,list2));
	       
	               
	       
	       
	}
	
	private static Class getBoxedType(Class inClass){
		
		if(!inClass.isPrimitive()){
			return inClass;
		}else{
			return types.get(inClass);
		}
		
	}
	
	/**
	 * Compares two lists of objects. (See @return for the structure of the return value.)</br>
	 * <b>Usage:</b> The returned <code>Map</code> must be compared with the <i>src</i>
	 * object which was passed as a parameter. This is because the object in the map of changes
	 * will share the same memory location as the object in <code>src</code> list. (This is unless
	 * the <code>equals()</code> method has been overridden.) So you would loop through the
	 * scr object list and see if the map contains that object key, if it does, you can check it's value
	 * of that key,  which is the list of differing fields to see if it contains the fieldname you want to check.
	 * 
	 * @param src
	 * @param dest
	 * @return  A <code>Map</code> keyed by the object, the value being a 
	 * <code>List</code> of <code>String</code>s being the field names that differ. Objects that do not differ
	 * will be excluded from the <code>Map</code>
	 */
	public static Map<Object,List<String>> findChangesBetweenLists(List<? extends Object> src,List<? extends Object> dest){
	    Map<Object, List<String>> results = new HashMap<Object, List<String>>();
	    
	    if(src!=null && dest !=null && src.size()==dest.size()) {
	        for(int i=0; i < src.size()  ; i++) {
	            HashMap<String, String>  srcObjectFields  = BeanUtilities.getFieldValuePairs(src.get(i), false);
	            HashMap<String, String>  destObjectFields  = BeanUtilities.getFieldValuePairs(dest.get(i), false);
	            
	            for(String fieldName:srcObjectFields.keySet()) {
	                if(!srcObjectFields.get(fieldName).equals(destObjectFields.get(fieldName))){
	                    if(!results.containsKey(src.get(i))) {
	                        results.put(src.get(i),new ArrayList<String>());
	                    }
	                    results.get(src.get(i)).add(fieldName);
	                }
	            }
	            
	        }
	    }
	    
        return results;
	    
	}
	
	public static ArrayList<String> getChangedFields(Object anObject, Object anotherObject){
		
		if (!anObject.getClass().getName().equals(anotherObject.getClass().getName())) {
			return null;
		}

		ArrayList<String> changedFields = new ArrayList<String>();

		HashMap<String, String>  activeFields = getFieldValuePairs(anObject, false);
		HashMap<String, String>  pendingFields = getFieldValuePairs(anotherObject, false);

		for(String key:activeFields.keySet()){
			if(pendingFields.containsKey(key)){
				if(!activeFields.get(key).equalsIgnoreCase(pendingFields.get(key)))changedFields.add(key);
			}
		}

		return changedFields;
	}
		

	// test classes

}
