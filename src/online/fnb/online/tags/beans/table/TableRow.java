package fnb.online.tags.beans.table;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fnb.online.tags.beans.table.exceptions.TableFieldNotFoundException;

public class TableRow {

	private Map<String, TableItem> itemMap = new LinkedHashMap<String, TableItem>();
	private List<String> fieldNames = new ArrayList<>();

	private boolean expandableItem = false;
	private String heading = "";
	private String nonGroupedHeading = "";

	private String groupCode = "";
	private int counter = -1;
	
	private String cssClass="";

	public TableRow() {

	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	/**
	 * @return the cell by field name
	 * @throws TableFieldNotFoundException
	 */
	public TableItem getItem(String fieldName)
			throws TableFieldNotFoundException {
		if (fieldName != null && fieldName.length() > 0	&& !itemMap.containsKey(fieldName)) {
			return new TableTextItem();
		}
		return itemMap.get(fieldName);
	}

	public void addItem(String fieldName, TableItem item) {
		itemMap.put(fieldName, item);
	}
	
	public void removeItem(String fieldName){
		itemMap.remove(fieldName);
	}
	
	public void addCell(TableItem item) {
		if (item != null) {
			addItem(item.getName(), item);
		}
	}

	public boolean contains(String fieldName) {
		return fieldName != null && fieldName.length() > 0
				&& itemMap.containsKey(fieldName);
	}

	@Override
	public String toString() {
		String keys = "Row field names : ";
		for (String key : itemMap.keySet()) {
			keys += key + ",";
		}
		return "TableRow [itemMap=" + itemMap.size() + " " + keys + "]";
	}
	
	public List<String> getFieldNames() {
		if(fieldNames.isEmpty()){
			for (TableItem item : itemMap.values()) {
				if(item instanceof TableDoubleItem){
					fieldNames.add(((TableDoubleItem)item).getTopItem().getName());
					fieldNames.add(((TableDoubleItem)item).getBottomItem().getName());
				}
				else if (!(item instanceof TableEziButtonItem ||
						item instanceof TableMoreOptionsItem ||
						item instanceof TableCheckBoxItem ||
						item instanceof TableButtonItem)){
					
						fieldNames.add(item.getName());
				}
			}
		}
		return fieldNames;
	}

	public boolean isExpandableItem() {
		return expandableItem;
	}

	public void setExpandableItem(boolean expandableItem) {
		this.expandableItem = expandableItem;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

    /**
     * @return the cssClass
     */
    public String getCssClass() {
        return cssClass;
    }

    /**
     * @param cssClass the cssClass to set
     */
    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

	public Map<String, TableItem> getItemMap() {
		return itemMap;
	}

	public String getNonGroupedHeading() {
		return nonGroupedHeading;
	}

	public void setNonGroupedHeading(String nonGroupedHeading) {
		this.nonGroupedHeading = nonGroupedHeading;
	}


}