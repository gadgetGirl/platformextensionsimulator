package fnb.online.tags.beans.table;


	public class TableCheckBoxItem extends TableTextItem {
		private boolean selected = false; 
		private String label = "";
		private String value = "";
		private String uncheckedValue = "";
		private boolean disabled = false;
		private boolean singleSelect = false;
		

		// if set to true, form data will be submitted as ${id}${count} ex: item1, item2 item3, etc
		// otherwise an array of values will be submitted.
		private boolean useCount = true;

		
		public TableCheckBoxItem(String id, String name) {
			super(id, name);
		}
		
		public TableCheckBoxItem() {
			super();
		}

		public boolean isSelected() {
			return selected;
		}

		public void setSelected(boolean selected) {
			this.selected = selected;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getUncheckedValue() {
			return uncheckedValue;
		}

		public void setUncheckedValue(String uncheckedValue) {
			this.uncheckedValue = uncheckedValue;
		}

        /**
         * @return the useCount
         */
        public boolean isUseCount() {
            return useCount;
        }

        /**
         * @param useCount the useCount to set
         */
        public void setUseCount(boolean useCount) {
            this.useCount = useCount;
        }

        public boolean isDisabled() {
        	return disabled;
        }
        
        public void setDisabled(boolean disabled) {
        	this.disabled = disabled;
        }

		public boolean isSingleSelect() {
			return singleSelect;
		}

		public void setSingleSelect(boolean singleSelect) {
			this.singleSelect = singleSelect;
		}
	}