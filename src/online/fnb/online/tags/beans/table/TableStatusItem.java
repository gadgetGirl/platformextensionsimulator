package fnb.online.tags.beans.table;

import mammoth.jsp.viewbean.ProcessingResultViewBean;

public class TableStatusItem extends TableTextItem{

	private String description = "";
	private int status;
	private ProcessingResultViewBean processingResultViewBean;
		
	public TableStatusItem() {
		super();
	}

	public TableStatusItem(String id, String name) {
		super(id, name);
	}
	
	public TableStatusItem(String id, String name, String description, int status) {
		this(id, name);
		this.description = description;
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ProcessingResultViewBean getProcessingResultViewBean() {
		return processingResultViewBean;
	}

	public void setProcessingResultViewBean(
			ProcessingResultViewBean processingResultViewBean) {
		this.processingResultViewBean = processingResultViewBean;
	}
	
	
}