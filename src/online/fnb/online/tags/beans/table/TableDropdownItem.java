package fnb.online.tags.beans.table;

import fnb.online.tags.beans.dropdown.DropDown;

	public class TableDropdownItem extends TableTextItem {
		private boolean disabled = false;
		
		DropDown dropDown = new DropDown();
		private String onchangeFunc="";
		
		public TableDropdownItem() {
			super();
		}
		
		public TableDropdownItem(String id, String name) {
			super(id, name);
		}

		public DropDown getDropDown() {
			return dropDown;
		}

		public void setDropDown(DropDown dropDown) {
			this.dropDown = dropDown;
		}

		public String getOnchangeFunc() {
			return onchangeFunc;
		}

		public void setOnchangeFunc(String onchangeFunc) {
			this.onchangeFunc = onchangeFunc;
		}

		public boolean isDisabled() {
			return disabled;
		}

		public void setDisabled(boolean disabled) {
			this.disabled = disabled;
		}
		
	}