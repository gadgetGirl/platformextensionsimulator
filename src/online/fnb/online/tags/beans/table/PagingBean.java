package fnb.online.tags.beans.table;

public interface PagingBean 
{
	
	public int getCurrentPageNumber();
	public int getNumberOfPages();
	public int getPageSize();
	public int getNumberOfItems();
	public String getUrl();
	public int getFirstPageNumber();
	public int getNextPageNumber();
	public int getPreviousPageNumber();
	public int getLastPageNumber();
	public int getStartPageNumber();
	public int getEndPageNumber();
		
}
