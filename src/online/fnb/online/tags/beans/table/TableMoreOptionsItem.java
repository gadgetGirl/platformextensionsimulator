package fnb.online.tags.beans.table;

import fnb.online.bifrost.server.bean.ActionViewBean;
import fnb.online.bifrost.server.paging.SortedVector;

public class TableMoreOptionsItem extends TableHyperLinkItem {

	private static String MORE_OPTIONS = "moreOptions";
	private static String URL = "/banking/Controller?nav=actions.navigator.MoreOptions";
	private static String ROW = "&rowKey=";
	private static String TABLE_KEY = "&tableKey=";
	private SortedVector<ActionViewBean> actions;

	public TableMoreOptionsItem(String tableKey, String rowKey, SortedVector<ActionViewBean> actions) {
		super(MORE_OPTIONS, MORE_OPTIONS);
		this.setActions(actions);
		setUrl(URL + ROW + rowKey + TABLE_KEY + tableKey);
	}

	public SortedVector<ActionViewBean> getActions() {
		return actions;
	}

	public void setActions(SortedVector<ActionViewBean> actions) {
		this.actions = actions;
	}


}