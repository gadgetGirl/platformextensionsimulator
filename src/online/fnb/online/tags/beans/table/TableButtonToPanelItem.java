package fnb.online.tags.beans.table;

import fnb.online.bifrost.server.service.BiFrostCodes;

	public class TableButtonToPanelItem extends TableButtonItem {
		
		
		
		public TableButtonToPanelItem() {
			super();
			setTarget(BiFrostCodes.ACTION_TARGET_SHOW_EZI_PANEL);
		}
		
		public TableButtonToPanelItem(String id, String name) {
			super(id, name);
			setTarget(BiFrostCodes.ACTION_TARGET_SHOW_EZI_PANEL);
		}

	}