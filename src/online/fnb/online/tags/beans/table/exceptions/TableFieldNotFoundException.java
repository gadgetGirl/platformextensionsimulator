package fnb.online.tags.beans.table.exceptions;

import fnb.online.tags.beans.table.TableColumnOptions;
import fnb.online.tags.beans.table.TableRow;

public class TableFieldNotFoundException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String message = TableColumnOptions.class.getName() + " contains a field name that is not mapped in your " + TableRow.class.getName();
	public TableFieldNotFoundException(String fieldName) {
		super(message + " field name : " + fieldName);
		
	}

}
