package fnb.online.tags.beans.table;

import java.util.LinkedList;
import java.util.List;

public class TableEziButtonItem extends TableTextItem {

	private List<TableButtonItem> buttons = new LinkedList<TableButtonItem>();

	private String type;

	public TableEziButtonItem() {
		super();
	}

	public TableEziButtonItem(String id, String name) {
		super(id, name);
	}

	public List<TableButtonItem> getButtons() {
		return buttons;
	}

	public void addButton(TableButtonItem button) {
		this.buttons.add(button);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setButtons(List<TableButtonItem> buttons) {
		this.buttons = buttons;
	}	
	

}