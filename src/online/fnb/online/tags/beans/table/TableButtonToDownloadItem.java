package fnb.online.tags.beans.table;

import fnb.online.bifrost.server.service.BiFrostCodes;


	public class TableButtonToDownloadItem extends TableButtonItem {
		
		public TableButtonToDownloadItem() {
			super();
			setTarget(BiFrostCodes.ACTION_TARGET_DOWNLOAD);
		}
		
		public TableButtonToDownloadItem(String id, String name) {
			super(id, name);
			setTarget(BiFrostCodes.ACTION_TARGET_DOWNLOAD);
		}

	}