package fnb.online.tags.beans.table;

import java.util.ArrayList;
import java.util.List;

import fnb.online.bifrost.server.service.AbstractTableRowResolver;

	public class TableSubRowItem extends TableTextItem {

		private List<TableRow> rows = new ArrayList<>();
		
		public TableSubRowItem() {
			super(AbstractTableRowResolver.SUBROW);
		}
				
		public void addNewSubRow(TableItem... tableItems) {
			TableRow subRow = new TableRow(); 
			subRow.setCssClass("tableSubRow");
			for(TableItem item : tableItems)
				subRow.addCell(item);
			rows.add(subRow);
		}
		
		public void addSubRow(TableRow subRow) {
			if(subRow != null)
				rows.add(subRow);
		}
		
		public void removeItem(TableRow subRow) {
			if(subRow != null)
				rows.remove(subRow);
		}

		public List<TableRow> getRows() {
			return rows;
		}

	}