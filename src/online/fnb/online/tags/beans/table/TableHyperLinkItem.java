package fnb.online.tags.beans.table;

import fnb.online.bifrost.server.service.BiFrostCodes;

	public class TableHyperLinkItem extends TableTextItem {
		
		private int childFunctionRef = 0;
		private String url;
		private int target = BiFrostCodes.ACTION_TARGET_URL_WORKSPACE;
		
		public TableHyperLinkItem() {
			super();
		}
		
		public TableHyperLinkItem(String id, String name) {
			super(id, name);
		}

		public String getUrl() {
			return url;
		}
		
		public void setUrl(String url) {
			this.url = url;
		}

		public int getTarget() {
			return target;
		}

		public void setTarget(int target) {
			this.target = target;
		}

		public int getChildFunctionRef() {
			return childFunctionRef;
		}

		public void setChildFunctionRef(int childFunctionRef) {
			this.childFunctionRef = childFunctionRef;
		}
		
	}