package fnb.online.tags.beans.table;

public class TableGraphicalItem extends TableTextItem {

	private String src;	
	private String alt;
	private String height;
	private String width;

	public TableGraphicalItem() {
		super();
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

}