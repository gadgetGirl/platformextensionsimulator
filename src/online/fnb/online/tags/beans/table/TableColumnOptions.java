package fnb.online.tags.beans.table;
public class TableColumnOptions {
		
		private String heading;
		private String shortHeading;
		private String fieldName;
		private String align;
		private String size;
		private String headingSize;
		private String topSize;
		private String bottomSize;
		private String cssClass;
		private boolean calculateTotal;
		private String total;
		private String hideColumn;
		private String calcFieldTotals;
		private String onClick;
		private String onKeyup;
		private String onChange;
		private String maxLength;
		private boolean selectAll = false;
		private String icon;
		private boolean checkBoxGroup = false;
		private String fixedTotal = "";
		
		public TableColumnOptions() {

		}  

		public TableColumnOptions(String heading) {
			this.heading = heading;
		}

		public TableColumnOptions(String heading,  String shortHeading,String fieldName, String align, String size,  
				String headingSize, String topSize, String bottomSize, String cssClass, boolean calculateTotal, 
				String hideColumn, String calcFieldTotals, String onClick, String onKeyup, String onChange, String maxLength, boolean selectAll,String icon, boolean checkBoxGroup) {
			
			this.heading = heading;
			this.setShortHeading(shortHeading);
			this.fieldName = fieldName;
			this.align = align;
			this.size = size;
			this.headingSize = headingSize;
			this.topSize = topSize; 
			this.bottomSize = bottomSize;
			this.cssClass = cssClass;
			this.calculateTotal = calculateTotal;
			this.hideColumn = hideColumn;
			this.calcFieldTotals = calcFieldTotals;
			this.onClick = onClick;
			this.onKeyup = onKeyup;
			this.onChange = onChange;	
			this.maxLength = maxLength;
			this.selectAll = selectAll;
			this.icon = icon;
			this.checkBoxGroup = checkBoxGroup;
	
		}

		public String getHeading() {
			return heading;
		}
		
		public void setHeading(String heading) {
			this.heading = heading;
		}
		
		public String getFieldName() {
			return fieldName;
		}
		
		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}
		
		public boolean isCalculateTotal() {
			return calculateTotal;
		}
		
		public void setCalculateTotal(boolean calculateTotal) {
			this.calculateTotal = calculateTotal;
		}

		public String getAlign() {
			return align;
		}

		public void setAlign(String align) {
			this.align = align;
		}
		
		
		public String getSize() {
			return size;
		}

		public void setSize(String size) {
			this.size = size;
		}

		public String getHeadingSize() {
			return headingSize;
		}

		public void setHeadingSize(String headingSize) {
			this.headingSize = headingSize;
		}


		public String getTopSize() {
			return topSize;
		}

		public void setTopSize(String topSize) {
			this.topSize = topSize;
		}

		public String getBottomSize() {
			return bottomSize;
		}

		public void setBottomSize(String bottomSize) {
			this.bottomSize = bottomSize;
		}

		public String getCssClass() {
			return cssClass;
		}

		public void setCssClass(String cssClass) {
			this.cssClass = cssClass;
		}

		public String getShortHeading() {
			return shortHeading;
		}

		public void setShortHeading(String shortHeading) {
			this.shortHeading = shortHeading;
		}

		public String getHideColumn() {
			return hideColumn;
		}

		public void setHideColumn(String hideColumn) {
			this.hideColumn = hideColumn;
		}

		public String getOnClick() {
			return onClick;
		}

		public void setOnClick(String onClick) {
			this.onClick = onClick;
		}

		public String getCalcColumnTotal() {
			return calcFieldTotals;
		}

		public void setCalcColumnTotal(String calcFieldTotals) {
			this.calcFieldTotals = calcFieldTotals;
		}

		public String getOnKeyup() {
			return onKeyup;
		}

		public void setOnKeyup(String onKeyup) {
			this.onKeyup = onKeyup;
		}

		public String getOnChange() {
			return onChange;
		}

		public void setOnChange(String onChange) {
			this.onChange = onChange;
		}

		public String getMaxLength() {
			return maxLength;
		}

		public void setMaxLength(String maxLength) {
			this.maxLength = maxLength;
		}

		public boolean isSelectAll() {
			return selectAll;
		}

		public void setSelectAll(boolean selectAll) {
			this.selectAll = selectAll;
		}

		public String getIcon() {
			return icon;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}

		public boolean isCheckBoxGroup() {
			return checkBoxGroup;
		}

		public void setCheckBoxGroup(boolean checkBoxGroup) {
			this.checkBoxGroup = checkBoxGroup;
		}

		public String getTotal() {
			return total;
		}

		public void setTotal(String total) {
			this.total = total;
		}

        /**
         * @return the fixedTotal
         */
        public String getFixedTotal() {
            return fixedTotal;
        }

        /**
         * @param fixedTotal the fixedTotal to set
         */
        public void setFixedTotal(String fixedTotal) {
            this.fixedTotal = fixedTotal;
        }
		
	}