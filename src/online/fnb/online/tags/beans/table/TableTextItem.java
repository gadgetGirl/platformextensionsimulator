package fnb.online.tags.beans.table;

import mammoth.utility.HyphenString;

public class TableTextItem extends TableItem{

	private String text = "";
	
	public TableTextItem() {
		super();
	}

	public TableTextItem(String id) {
		super(id);
	}
	
	public TableTextItem(String id, String name) {
		super(id, name);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		if(text == null){
			this.text = "";
		}else{
			this.text = HyphenString.isValidString(text.trim())?text:"&nbsp;";
			this.text = this.text.replace("/", "&#47;").replace("<", "&#60;").replace(">", "&#62;");
		}
	}
	
	public void setTextWithHtml(String text) {
		if(text == null){
			this.text = "";
		}else{
			this.text = HyphenString.isValidString(text.trim())?text:"&nbsp;";
		}
	}

	@Override
	public String getCellValue() {
		return getText();
	}

	@Override
	public void setCellValue(String value) {
		setText(value);
	}
	
	public String getValue() {
		return getText();
	}

	public void setValue(String value) {
		setText(value);
	}
	
}