package fnb.online.tags.beans.table;

import java.math.BigDecimal;

import mammoth.utility.HyphenString;

/**
 * 
 * @author michael
 *
 */
public class TableUtilities{
	
	public TableUtilities() {
		
	}
    
	static String currencyIndicator = "";
	
    public static String calculateColumnTotal(TableRowGroup rowGroup, String fieldToTotal) {
    	
    	BigDecimal total = new BigDecimal(0);
    	
		for(TableRow row : rowGroup.getTableRows()){
			if(row.contains(fieldToTotal)){
				try{
					total = total.add(new BigDecimal(removeNonDigits(row.getItem(fieldToTotal).getCellValue())));
				}catch (Exception e) {
					System.err.println("Could not locate field in row to add to total.");
				}
			}
		}
    	
    	return currencyIndicator+HyphenString.formatDecimal(total);
    }
    
    public static String removeNonDigits(String text) {
	     int length = text.length();
	     StringBuffer buffer = new StringBuffer(length);
	     for(int i = 0; i < length; i++) {
	         char ch = text.charAt(i);
	         
	         //Assume first character is the currency
	         if (HyphenString.isNumeric(text) && (i==0 && !Character.isDigit(ch))){
	        	 currencyIndicator = String.valueOf(ch); 
	         }
	         
	         if (Character.isDigit(ch) || ch == '.'|| ch == '-') {
	             buffer.append(ch);
	         }
	     }
	     return buffer.toString();
	 }

	public String getCurrencyIndicator() {
		return currencyIndicator;
	}

	public void setCurrencyIndicator(String currencyIndicator) {
		this.currencyIndicator = currencyIndicator;
	}

    
    
}

