package fnb.online.tags.beans.table;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import mammoth.utility.HyphenString;

public class TableRowGroup {

	private String heading = "";
	private String code = "";
	//These fields are used to override the group column header link for sorting with different functionality
	private Map<String, String> overrideColumnUrl = new HashMap<String, String>();
	 
	private List<TableRow> tableRows = new LinkedList<TableRow>();
	private List<TableButtonItem> tableGroupButtonItems = new LinkedList<TableButtonItem>();
	
	public TableRowGroup() {
		
	}

	public TableRowGroup(String heading, String code) {
		super();
		this.heading = heading;
		this.setCode(code);
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public List<TableRow> getTableRows() {
		return tableRows;
	}

	public void setTableRows(List<TableRow> tableRows) {
		this.tableRows = tableRows;
	}
	
	public void addTableRow(TableRow tableRow) {
		if(!HyphenString.isValidString(code) && HyphenString.isValidString(tableRow.getGroupCode())){
			setCode(tableRow.getGroupCode());
		}
		this.tableRows.add(tableRow);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOverrideColumnUrl(String fieldName) {
		if (HyphenString.isValidString(fieldName) && !overrideColumnUrl.containsKey(fieldName)) {
			return HyphenString.EMPTY_STRING;
		}
		return overrideColumnUrl.get(fieldName);
	}

	public void addOverrideColumnUrl(String fieldName, String url) {
		if (HyphenString.isValidString(fieldName) && HyphenString.isValidString(url)) {
			overrideColumnUrl.put(fieldName, url);
		}
	}
	
	public Map<String, String> getOverrideColumnUrlMap() {
		return overrideColumnUrl;
	}

	public List<TableButtonItem> getTableGroupButtonItems() {
		return tableGroupButtonItems;
	}

	public void setTableGroupButtonItems(List<TableButtonItem> tableGroupButtonItems) {
		this.tableGroupButtonItems = tableGroupButtonItems;
	}

}