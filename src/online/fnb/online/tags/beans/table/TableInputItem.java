package fnb.online.tags.beans.table;

	public class TableInputItem extends TableTextItem {

		private boolean hidden;
		
		public TableInputItem() {
			super();
		}
		
		public TableInputItem(String id) {
			super(id);
		}
		
		public TableInputItem(String id, String name) {
			super(id, name);
		}

		public TableInputItem(String id, String name, boolean hidden) {
			super(id, name);
			this.hidden = hidden;
		}
		
		public boolean isHidden() {
			return hidden;
		}

		public void setHidden(boolean hidden) {
			this.hidden = hidden;
		}

		
	}