package fnb.online.tags.beans.table;

	public class TableButtonItem extends TableHyperLinkItem {
		
		private boolean disabled = false;
		
		public TableButtonItem() {
			super();
		}
		
		public TableButtonItem(String id, String name) {
			super(id, name);
		}

		public boolean isDisabled() {
			return disabled;
		}

		public void setDisabled(boolean disabled) {
			this.disabled = disabled;
		}

		@Override
		public String getUrl() {
			if(!disabled){
				return super.getUrl();
			}else{
				return "";
			}
		}
		
		
		
	}