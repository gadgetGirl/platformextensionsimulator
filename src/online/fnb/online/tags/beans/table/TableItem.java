package fnb.online.tags.beans.table;

import fnb.online.tags.beans.AbstractItem;

public abstract class TableItem extends AbstractItem{
    
    private String cssClass = "";

	public TableItem() {}
	
	public TableItem(String id) {
		super(id);
	}
	
	public TableItem(String id, String name) {
		super(id, name);
	}
	
	public abstract String  getCellValue();
	
	public abstract void  setCellValue(String value);

    /**
     * @return the cssClass
     */
    public String getCssClass() {
        return cssClass;
    }

    /**
     * @param cssClass the cssClass to set
     */
    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }
	
}