package fnb.online.tags.beans.table;

import fnb.online.tags.beans.date.DatePicker;

	public class TableDateBoxItem extends TableTextItem {
		DatePicker datePicker = new DatePicker();
		
		public TableDateBoxItem() {
			super();
		}
		
		public TableDateBoxItem(String id, String name) {
			super(id, name);
		}

		public DatePicker getDatePicker() {
			return datePicker;
		}

		public void setDatePicker(DatePicker datePicker) {
			this.datePicker = datePicker;
		}

	}