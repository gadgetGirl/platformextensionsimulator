package fnb.online.tags.beans.table;

import java.util.List;

import fnb.online.bifrost.server.bean.PagingViewBean;
import fnb.online.tags.beans.dropdown.DropDown;

/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 * 
 */
public interface TableBean {

    
	public List<TableRowGroup> getRowGroups();
	
	public void setRowGroups(List<TableRowGroup> tableRowGroups);
	
	public void addRowGroup(TableRowGroup tableRowGroup);

	public TableOptions getTableOptions();

	public void setTableOptions(TableOptions tableOptions);
	
	public String getTableId();
	
	public void setTableId(String tableId);
	
	public DropDown getHeaderDropdown();
	
	public void setHeaderDropdown(DropDown dropdown);
	
	public PagingViewBean getPagingViewBean();
	
	public String getEnableJSobject();
	
	public void setEnableJSobject(String enableJSobject);
	
	public String getTableHeading();
	
	public void setTableHeading(String tableHeading);
	
	public String getOnRowClick();
	
	public void setOnRowClick(String onRowClick);
	
	public String getPhoneContent();
	
	public void setPhoneContent(String phoneContent);
	
	public boolean getOverwriteTableOptions();
	
}
