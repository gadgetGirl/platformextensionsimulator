package fnb.online.tags.beans.table;

import java.math.BigDecimal;

import mammoth.utility.HyphenString;

	public class TableAmountInputItem extends TableInputItem {

		private BigDecimal amount;
		
		public TableAmountInputItem() {
			super();
		}
		
		public TableAmountInputItem(String id) {
			super(id);
		}
		
		public TableAmountInputItem(String id, String name) {
			super(id, name);
		}

		public TableAmountInputItem(String id, String name, boolean hidden) {
			super(id, name,hidden);
		}

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}
		
		@Override
		public String getCellValue() {
			return HyphenString.formatDecimal(getAmount());
		}
		
		@Override
		public String getValue() {
			return getCellValue();
		}
		
		@Override
		public String getText() {
			return getCellValue();
		}

		
	}