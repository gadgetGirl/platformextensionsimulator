/**
 * 
 */
package fnb.online.tags.beans.table;

import java.util.LinkedList;
import java.util.List;

import mammoth.utility.HyphenString;

public class TableColumnGroup {

	private List<TableColumnOptions> columnOptions = new LinkedList<TableColumnOptions>();
    private String groupName = "";
    private String groupDescription = "";
    private boolean groupBorderLeft = false;
    private boolean groupBorderRight = false;
    private boolean hideForMobile = false;
     

	public TableColumnGroup() {
	
	}
    
    public TableColumnGroup(String groupName) {
		super();
		this.groupName = groupName;
	}
    
    public TableColumnGroup(String groupName, String groupDescription, String groupBorderLeft, String groupBorderRight, String hideForMobile ) {
		super();
		this.groupName = groupName;
		this.groupBorderLeft = Boolean.valueOf(groupBorderLeft);
		this.groupBorderRight = Boolean.valueOf(groupBorderRight);
		this.hideForMobile = Boolean.valueOf(hideForMobile);
		
		if(HyphenString.isValidString(groupDescription)){
				
			this.groupDescription = groupDescription;
		}
		
		
		
	}

	public List<TableColumnOptions> getColumnOptions() {
		return columnOptions;
	}

	public void setColumnOptions(List<TableColumnOptions> columnOptions) {
		this.columnOptions = columnOptions;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public boolean isGroupBorderLeft() {
		return groupBorderLeft;
	}

	public void setGroupBorderLeft(boolean groupBorderLeft) {
		this.groupBorderLeft = groupBorderLeft;
	}

	public boolean isGroupBorderRight() {
		return groupBorderRight;
	}

	public void setGroupBorderRight(boolean groupBorderRight) {
		this.groupBorderRight = groupBorderRight;
	}

	public boolean isHideForMobile() {
		return hideForMobile;
	}

	public void setHideForMobile(boolean hideForMobile) {
		this.hideForMobile = hideForMobile;
	}

	
	

}
