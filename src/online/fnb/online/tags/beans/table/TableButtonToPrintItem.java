package fnb.online.tags.beans.table;

import fnb.online.bifrost.server.service.BiFrostCodes;

	public class TableButtonToPrintItem extends TableButtonItem {
		
		public TableButtonToPrintItem() {
			super();
			setTarget(BiFrostCodes.ACTION_TARGET_PRINT_DIV);
		}
		
		public TableButtonToPrintItem(String id, String name) {
			super(id, name);
			setTarget(BiFrostCodes.ACTION_TARGET_PRINT_DIV);
		}

	}