package fnb.online.tags.beans.table;

	public class TableDoubleItem extends TableTextItem {

		private TableItem topItem;
		private TableItem bottomItem;
		
		public TableDoubleItem() {
			super();
		}
		
		public TableDoubleItem(String id, String name) {
			super(id, name);
		}

		public TableItem getBottomItem() {
			return bottomItem;
		}

		public void setBottomItem(TableItem bottomItem) {
			this.bottomItem = bottomItem;
		}

		public TableItem getTopItem() {
			return topItem;
		}

		public void setTopItem(TableItem topItem) {
			this.topItem = topItem;
		}
		
		@Override
		public String getCellValue() {
			return topItem.getCellValue();
		}
		
		
	}