/**
 * 
 */
package fnb.online.tags.beans.table;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import mammoth.utility.HyphenString;

public class TableOptions {
    
    private TreeMap<String, TableColumnGroup> columnGroups = new TreeMap<String, TableColumnGroup>();
    
    private String sortingCallbackFunction = "MammothTableUtility.sortTable";
    private String pagingCallbackFunction = "MammothTableUtility.pageTable";
    private String searchCallbackFunction = "MammothTableUtility.searchTable";
    private String changePageSizeCallbackFunction = "MammothTableUtility.changeTableSize";
    
    private String serverSideUrl;
    private boolean isPaging;
    //these values are used in paging, to validate and capture values when paging
    private String validationNavigator;
    private String formToSubmit;
    
    private boolean showSearchFilter;
    private boolean showAdvancedLink;
    private String advancedSearchUrl;
    
    private boolean showTotalFooter;
    private String emptyDataSetMessage;
    private boolean showColumnGroupDesc = false;
    
    public TableOptions(){}

	public  List<TableColumnGroup>  getColumnGroups() {
		return new LinkedList<TableColumnGroup>(columnGroups.values());
	}
	
	public  List<TableColumnOptions>  getColumnOptions(String groupKey) {
		return columnGroups.get(groupKey).getColumnOptions();
	}

	public void setColumnGroups( TreeMap<String, TableColumnGroup>  columnGroups) {
		this.columnGroups = columnGroups;
	}

	public String getSortingCallbackFunction() {
		return sortingCallbackFunction;
	}

	public void setSortingCallbackFunction(String sortingCallbackFunction) {
		if(HyphenString.isValidString(sortingCallbackFunction)){
			this.sortingCallbackFunction = sortingCallbackFunction;
		}
	}

	public String getPagingCallbackFunction() {
		return pagingCallbackFunction;
	}

	public void setPagingCallbackFunction(String pagingCallbackFunction) {
		if(HyphenString.isValidString(pagingCallbackFunction)){
			this.pagingCallbackFunction = pagingCallbackFunction;
		}
	}
	
	public String getSearchCallbackFunction() {
		return searchCallbackFunction;
	}

	public void setSearchCallbackFunction(String searchCallbackFunction) {
		if(HyphenString.isValidString(searchCallbackFunction)){
			this.searchCallbackFunction = searchCallbackFunction;
		}
	}   
	
	public String getServerSideUrl() {
		return serverSideUrl;
	}

	public void setServerSideUrl(String serverSideUrl) {
		this.serverSideUrl = serverSideUrl;
	}

	public boolean isPaging() {
		return isPaging;
	}

	public void setPaging(boolean isPaging) {
		this.isPaging = isPaging;
	}	
	
	public void addTableColumnGroup(TableColumnGroup tableColumnGroup) {
		columnGroups.put(tableColumnGroup.getGroupName(), tableColumnGroup);
		if(!showColumnGroupDesc && HyphenString.isValidString(tableColumnGroup.getGroupDescription())){
			showColumnGroupDesc = true;
		}
	}

	public boolean isShowSearchFilter() {
		return showSearchFilter;
	}

	public void setShowSearchFilter(boolean showSearchFilter) {
		this.showSearchFilter = showSearchFilter;
	}

	public boolean isShowAdvancedLink() {
		return showAdvancedLink;
	}

	public void setShowAdvancedLink(boolean showAdvancedLink) {
		this.showAdvancedLink = showAdvancedLink;
	}

	public String getAdvancedSearchUrl() {
		return advancedSearchUrl;
	}

	public void setAdvancedSearchUrl(String advancedSearchUrl) {
		this.advancedSearchUrl = advancedSearchUrl;
	}

	public boolean isShowTotalFooter() {
		return showTotalFooter;
	}

	public void setShowTotalFooter(boolean showTotalFooter) {
		this.showTotalFooter = showTotalFooter;
	}

	public String getEmptyDataSetMessage() {
		return emptyDataSetMessage;
	}

	public void setEmptyDataSetMessage(String emptyDataSetMessage) {
		this.emptyDataSetMessage = emptyDataSetMessage;
	}

	/**
	 * @return the validationNavigator
	 * these values are used in paging, to validate and capture values when paging
	 */
	public String getValidationNavigator() {
		return validationNavigator;
	}

	/**
	 * @param validationNavigator the validationNavigator to set
	 * these values are used in paging, to validate and capture values when paging
	 */
	public void setValidationNavigator(String validationNavigator) {
		this.validationNavigator = validationNavigator;
	}

	/**
	 * @return the formToSubmit
	 * these values are used in paging, to validate and capture values when paging
	 */
	public String getFormToSubmit() {
		return formToSubmit;
	}

	/**
	 * @param formToSubmit the formToSubmit to set
	 * these values are used in paging, to validate and capture values when paging
	 */
	public void setFormToSubmit(String formToSubmit) {
		this.formToSubmit = formToSubmit;
	}

	public String getChangePageSizeCallbackFunction() {
		return changePageSizeCallbackFunction;
	}

	public void setChangePageSizeCallbackFunction(String changePageSizeCallbackFunction) {
		if(HyphenString.isValidString(changePageSizeCallbackFunction)){
			this.changePageSizeCallbackFunction = changePageSizeCallbackFunction;
		}
	}

	public boolean isShowColumnGroupDesc() {
		return showColumnGroupDesc;
	}

	public void setShowColumnGroupDesc(boolean showColumnGroupDesc) {
		this.showColumnGroupDesc = showColumnGroupDesc;
	}

	
	
}
