package fnb.online.tags.beans.table;

import fnb.online.tags.beans.radiogroup.RadioGroup;

	public class TableRadioButtonItem extends TableTextItem {
		public RadioGroup radioGroup = new RadioGroup();
		private boolean useCount = true;
		
		public boolean isUseCount() {
			return useCount;
		}

		public void setUseCount(boolean useCount) {
			this.useCount = useCount;
		}

		public TableRadioButtonItem() {
			super();
		}
		
		public TableRadioButtonItem(String id, String name) {
			super(id, name);
		}

		public RadioGroup getRadioGroup() {
			return radioGroup;
		}

		public void setRadioGroup(RadioGroup radioGroup) {
			this.radioGroup = radioGroup;
		}

	}