package fnb.online.tags.beans.form;

public class Form {
	private String id = "";
	private String name = "";
	private String action = "";
	private String onsubmit = "";
	private String method = "";
	
	public Form() {
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Form(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getOnsubmit() {
		return onsubmit;
	}
	public void setOnsubmit(String onsubmit) {
		this.onsubmit = onsubmit;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}

}
