/**
 * 
 */
package fnb.online.tags.beans;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fnb.online.tags.annotations.ValidationsAccountNumber;
import fnb.online.tags.annotations.ValidationsAccountType;
import fnb.online.tags.annotations.ValidationsAmount;
import fnb.online.tags.annotations.ValidationsAreaCode;
import fnb.online.tags.annotations.ValidationsBranchCode;
import fnb.online.tags.annotations.ValidationsCellphone;
import fnb.online.tags.annotations.ValidationsCheckBox;
import fnb.online.tags.annotations.ValidationsDate;
import fnb.online.tags.annotations.ValidationsDropdown;
import fnb.online.tags.annotations.ValidationsEmail;
import fnb.online.tags.annotations.ValidationsFax;
import fnb.online.tags.annotations.ValidationsFromAccount;
import fnb.online.tags.annotations.ValidationsFromReference;
import fnb.online.tags.annotations.ValidationsInt;
import fnb.online.tags.annotations.ValidationsOTP;
import fnb.online.tags.annotations.ValidationsPublicRecipient;
import fnb.online.tags.annotations.ValidationsRSAIdentityNumber;
import fnb.online.tags.annotations.ValidationsRadioButton;
import fnb.online.tags.annotations.ValidationsRecipientName;
import fnb.online.tags.annotations.ValidationsTelephone;
import fnb.online.tags.annotations.ValidationsText;
import fnb.online.tags.annotations.ValidationsToAccount;
import fnb.online.tags.annotations.ValidationsToReference;
import fnb.online.tags.annotations.ValidationsUnits;
import fnb.online.tags.beans.table.BeanUtilities;
import fnb.online.tags.validation.handlers.AccountNumberValidationHandler;
import fnb.online.tags.validation.handlers.AccountTypeValidationHandler;
import fnb.online.tags.validation.handlers.AmountValidationHandler;
import fnb.online.tags.validation.handlers.AreaCodeValidationHandler;
import fnb.online.tags.validation.handlers.BranchCodeValidationHandler;
import fnb.online.tags.validation.handlers.CellphoneValidationHandler;
import fnb.online.tags.validation.handlers.CheckBoxValidationHandler;
import fnb.online.tags.validation.handlers.DateValidationHandler;
import fnb.online.tags.validation.handlers.DropdownValidationHandler;
import fnb.online.tags.validation.handlers.EmailValidationHandler;
import fnb.online.tags.validation.handlers.FaxValidationHandler;
import fnb.online.tags.validation.handlers.FromAccountValidationHandler;
import fnb.online.tags.validation.handlers.FromReferenceValidationHandler;
import fnb.online.tags.validation.handlers.IntValidationHandler;
import fnb.online.tags.validation.handlers.OTPValidationHandler;
import fnb.online.tags.validation.handlers.PublicRecipientValidationHandler;
import fnb.online.tags.validation.handlers.RSAIdentityNumberValidationHandler;
import fnb.online.tags.validation.handlers.RadioButtonValidationHandler;
import fnb.online.tags.validation.handlers.RecipientNameValidationHandler;
import fnb.online.tags.validation.handlers.TelephoneValidationHandler;
import fnb.online.tags.validation.handlers.TextValidationHandler;
import fnb.online.tags.validation.handlers.ToAccountValidationHandler;
import fnb.online.tags.validation.handlers.ToReferenceValidationHandler;
import fnb.online.tags.validation.handlers.UnitValidationHandler;
import fnb.online.tags.validation.handlers.ValidationHandler;


/**
 * @author Kevin Mitchell <kevin.mitchell@fnb.co.za>
 *
 */
public abstract class AbstractViewBean {
    
	private int counter = -1;
	private String tableBeanName = "";
	private String populatingNavigator = "";
	
	 private static Map<Class,Class<? extends ValidationHandler>> validationHandlers = new HashMap<Class, Class<? extends ValidationHandler>>();
	    
	    static {
	        validationHandlers.put(ValidationsAmount.class,AmountValidationHandler.class);
	        validationHandlers.put(ValidationsText.class,TextValidationHandler.class);
	        validationHandlers.put(ValidationsDropdown.class,DropdownValidationHandler.class);
	        validationHandlers.put(ValidationsAccountType.class,AccountTypeValidationHandler.class);
	        validationHandlers.put(ValidationsAreaCode.class,AreaCodeValidationHandler.class);
	        validationHandlers.put(ValidationsBranchCode.class,BranchCodeValidationHandler.class);
	        validationHandlers.put(ValidationsCellphone.class,CellphoneValidationHandler.class);
	        validationHandlers.put(ValidationsDate.class,DateValidationHandler.class);
	        validationHandlers.put(ValidationsEmail.class,EmailValidationHandler.class);
	        validationHandlers.put(ValidationsFax.class,FaxValidationHandler.class);
	        validationHandlers.put(ValidationsFromAccount.class,FromAccountValidationHandler.class);
	        validationHandlers.put(ValidationsFromReference.class,FromReferenceValidationHandler.class);
	        validationHandlers.put(ValidationsRadioButton.class,RadioButtonValidationHandler.class);
	        validationHandlers.put(ValidationsRecipientName.class,RecipientNameValidationHandler.class);
	        validationHandlers.put(ValidationsTelephone.class,TelephoneValidationHandler.class);
	        validationHandlers.put(ValidationsToAccount.class,ToAccountValidationHandler.class);
	        validationHandlers.put(ValidationsToReference.class,ToReferenceValidationHandler.class);
	        validationHandlers.put(ValidationsUnits.class,UnitValidationHandler.class);
	        validationHandlers.put(ValidationsAccountNumber.class,AccountNumberValidationHandler.class);
	        validationHandlers.put(ValidationsInt.class,IntValidationHandler.class);
	        validationHandlers.put(ValidationsCheckBox.class,CheckBoxValidationHandler.class);
	        validationHandlers.put(ValidationsRSAIdentityNumber.class,RSAIdentityNumberValidationHandler.class);
	        validationHandlers.put(ValidationsPublicRecipient.class,PublicRecipientValidationHandler.class);
	        validationHandlers.put(ValidationsOTP.class,OTPValidationHandler.class);
	        
	    }
	    
	    
	    public Map<String, ValidationHandler> getFieldValidationHandlers() throws InstantiationException, IllegalAccessException {
	        
	        Map<String , ValidationHandler>  result = new HashMap<String, ValidationHandler>();
	 
	        ArrayList<Field> allFields = BeanUtilities.getAllUniqeNameFields(getClass());

	        for(Field f: allFields) {
	            
	        	Annotation[] fieldAnnotations = f.getDeclaredAnnotations();
	        	
	        	//if(fieldAnnotations.length == 0 && ! Modifier.isPrivate(f.getModifiers())){
	        		//fieldAnnotations = BeanUtilities.findAnnotationsOnClassHeirarchy(getClass().getSuperclass(), f);
	        	//}
	            
	            for(Annotation a: fieldAnnotations) {
	                
	            	for(Class annotationClass : validationHandlers.keySet()) {
	                    if(a.annotationType().equals(annotationClass)) {
	                        ValidationHandler handler = validationHandlers.get(annotationClass).newInstance(); 
	                        handler.setAnnotation(a);
	                        handler.setFieldName(f.getName());
	                        result.put(f.getName(),handler);
	                        break;
	                    }
	                }
	            }
	        }        
	        
	        return result;
	        
	    }
	    
	public int getCounter() {
		return counter;
	}


	public void setCounter(int counter) {
		this.counter = counter;
	}


	public String getTableBeanName() {
		return tableBeanName;
	}


	public void setTableBeanName(String tableBeanName) {
		this.tableBeanName = tableBeanName;
	}

	public String getPopulatingNavigator() {
		return populatingNavigator;
	}


	public void setPopulatingNavigator(String populatingNavigator) {
		this.populatingNavigator = populatingNavigator;
	}


}
