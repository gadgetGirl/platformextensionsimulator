package fnb.online.tags.beans.definitionList;

import java.util.Vector;

public class DefinitionList extends AbstractDefinitionList{
	private Vector<DefinitionListItem> items = new Vector<DefinitionListItem>();

	public DefinitionList() {}
	
	public DefinitionList(String id, String name) {
		super(id,name);
		this.items = new Vector<DefinitionListItem>();
	}

	public void addItem(DefinitionListItem item) {
		this.items.add(item);
	}


	/**
	 * @return
	 * 
	 */
	public Vector<DefinitionListItem> getItems() {
		return items;
	}

	/**
	 * @param vector
	 */
	public void setItems(Vector<DefinitionListItem> list) {
		items = list;
	}


}
