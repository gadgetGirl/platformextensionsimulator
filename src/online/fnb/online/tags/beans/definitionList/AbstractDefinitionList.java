package fnb.online.tags.beans.definitionList;

import fnb.online.tags.beans.AbstractItem;


public abstract class AbstractDefinitionList extends AbstractItem {
	public AbstractDefinitionList() {}
	
	public AbstractDefinitionList(String id, String name) {
		super(id, name);
	}

}
