package fnb.online.tags.beans.definitionList;


public class DefinitionListItem
{
	private String value;
	private String description;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DefinitionListItem(){
		
	}
	
	public DefinitionListItem(String value, String description)
	{
		this.value = value;
		this.description = description;
	}


}