package fnb.online.bifrost.server.navigator;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fnb.online.bifrost.server.dto.Page;
import fnb.online.bifrost.server.service.BiFrostContext;
import fnb.online.bifrost.server.service.BiFrostService;
import fnb.online.bifrost.server.xml.XStreamUtility;

public class BiFrostNavigator extends Navigator {

	public boolean performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		BiFrostService service = BiFrostService.getInstance();
		BiFrostContext context = new BiFrostContext();
		for (String parmName : req.getParameterMap().keySet()) {
			context.addRequestParm(parmName, req.getParameter(parmName));
		}

		String serviceResponse = service.doExtensionRequest(context);
		
		try {
			Page page = XStreamUtility.getInstance().getPage(serviceResponse);
			//XStreamUtility.getInstance().writeFile("/home/pieter/workspace/luna/BiFrostClient/WebContent/WEB-INF/xml/" + page.getHeader() + ".xml", serviceResponse);
			processPage(page);
		} catch (Exception e) {
			e.printStackTrace();
			setJspName("main.jsp");
		}
		return true;

	}

}