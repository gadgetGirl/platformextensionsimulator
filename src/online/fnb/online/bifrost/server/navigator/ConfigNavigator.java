package fnb.online.bifrost.server.navigator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fnb.online.bifrost.server.dto.FooterButton;
import fnb.online.bifrost.server.dto.Form;
import fnb.online.bifrost.server.dto.FormDropdown;
import fnb.online.bifrost.server.dto.FormField;
import fnb.online.bifrost.server.dto.FormRow;
import fnb.online.bifrost.server.dto.Page;
import fnb.online.bifrost.server.system.Controller;

public class ConfigNavigator extends Navigator {

	public boolean performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {
			String endPointName = req.getParameter("endPointName");
			
			Page page = new Page();
			page.setHeader("BiFrost - Config");
			page.setForm(new Form());
			page.getForm().setAction("banking/Controller");

			FormRow formRow = new FormRow();

			formRow.setHeading("Client Server Details");
			FormField field = new FormField();
			field.setId("endPointName");
			field.setName("endPointName");
			field.setLabel("Name");
			field.setType("dropdown");
			field.setValue("");

			String value ="";
			String urlValue = "";
			Map<String, String> endPoints = getEndPoints();
			for (String key : endPoints.keySet()) {
				FormDropdown dd = new FormDropdown();
				//set to the first value
				value = endPoints.get(key);
				if(urlValue.isEmpty()){
					urlValue = value;
				}
				//set to the first value
				if(endPointName == null || endPointName.isEmpty()){
					endPointName = key;
				}
				dd.setValue(key);
				dd.setDisplayValue(key);
				if (key.equals(endPointName)) {
					dd.setSelected(true);
					urlValue = value;
				}
				field.getDropdown().add(dd);
				field.setUrl("/BiFrostServer/Controller?config=true");
			}
			
			formRow.setField1(field);

			field = new FormField();
			field.setId("endPointURL");
			field.setName("endPointURL");
			field.setLabel("URL");
			field.setType("input");
			field.setValue(urlValue);
			formRow.setField2(field);
			page.getForm().getRows().add(formRow);

			FooterButton button = new FooterButton();
			button.setLabel("Go");
			button.setUrl("/BiFrostServer/Controller?EndPoint=" + endPointName);
			page.getFooterButtons().add(button);
			
			processPage(page);
		} catch (Exception e) {
			e.printStackTrace();
			setJspName("main.jsp");
		}
		return true;

	}

	public Map<String, String> getEndPoints() {
		String file = Controller.getDataPath() +  "/endpoint.properties";
		Properties properties = new Properties();
		BufferedReader reader;
		Map<String, String> map = new HashMap<String,String>();
 		try {
			reader = new BufferedReader(new FileReader(file));
			properties.load(reader);
			for(Object key : properties.keySet()){
				map.put((String)key, properties.getProperty((String)key));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
 		return map;
	}
	
	public void storeEndPoint(String endPointName, String url) {
		String file = Controller.getDataPath() +  "/endpoint.properties";
		Properties properties = new Properties();
		BufferedReader reader;
 		try {
			reader = new BufferedReader(new FileReader(file));
			properties.load(reader);
			properties.put(endPointName, url);
			OutputStream out = new FileOutputStream(file);
			properties.store(out, "");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}