package fnb.online.bifrost.server.navigator;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mammoth.jsp.viewbean.ProcessingResultViewBean;
import mammoth.utility.HyphenString;
import fnb.online.bifrost.server.bean.ActionViewBean;
import fnb.online.bifrost.server.bean.TableViewBean;
import fnb.online.bifrost.server.dto.Form;
import fnb.online.bifrost.server.dto.FormDropdown;
import fnb.online.bifrost.server.dto.FormField;
import fnb.online.bifrost.server.dto.FormRow;
import fnb.online.bifrost.server.dto.Menu;
import fnb.online.bifrost.server.dto.MenuColumns;
import fnb.online.bifrost.server.dto.MenuGroup;
import fnb.online.bifrost.server.dto.MenuItem;
import fnb.online.bifrost.server.dto.Page;
import fnb.online.bifrost.server.dto.ProcessingResult;
import fnb.online.bifrost.server.dto.SubTabs;
import fnb.online.bifrost.server.dto.Table;
import fnb.online.bifrost.server.paging.SortedVector;
import fnb.online.bifrost.server.service.TableRowBuilder;
import fnb.online.tags.beans.actionbar.Action;
import fnb.online.tags.beans.actionbar.ActionBarBean;
import fnb.online.tags.beans.actionbar.ActionBlock;
import fnb.online.tags.beans.dropdown.DropDown;
import fnb.online.tags.beans.dropdown.DropDownItem;
import fnb.online.tags.beans.table.BeanUtilities;

public abstract class Navigator {

	protected static final String ACTION_BAR_BEAN = "ACTION_BAR_BEAN";
	protected static final String FOOTER_BUTTONS = "footerButtons";
	protected static final String SUB_TABS = "subTabs";
	protected static final String TABLE_VIEW_BEAN = "tableBean";
	protected static final String PAGE = "page";
	protected static final String TECHNICAL_REFERENCE = "technicalReference";

	private String jspName = "fnb/online/bifrost/server/GenericDisplay.jsp";
	protected transient HttpServletRequest reqObject;
	protected transient HttpServletResponse respObject;
	protected transient ServletContext servletContext;
	protected transient HttpSession session;

	public boolean initialize(HttpServletRequest req, ServletContext context, HttpServletResponse resp) {
		respObject = resp;
		reqObject = req;
		servletContext = context;
		session = req.getSession(true);
		return (session != null);
	}

	/**
	 * Processes the data passed in through the HttpServletRequest and returns
	 * results in the HttpServletResponse.
	 * 
	 * @return boolean
	 * @param req
	 *            javax.servlet.http.HttpServletRequest
	 * @param resp
	 *            javax.servlet.http.HttpServletResponse
	 */
	public abstract boolean performTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException;

	public String getJspName() {

		return jspName;
	}

	public void setJspName(String jspName) {
		this.jspName = jspName;
	}

	/**
	 * Stores a bean on the request
	 */
	protected void storeBean(String key, Object bean) {
		reqObject.setAttribute(key, bean);
	}

	public void storeActionBar(Menu menu) {
		ActionBarBean actionBar = new ActionBarBean();
		// Hide until we add something
		actionBar.setHideActionBar(true);
		ActionBlock actionBlock;
		if (menu != null) {
			for (MenuColumns column : menu.getColumns()) {
				if (column != null && !column.getGroup().isEmpty()) {
					actionBar.setHideActionBar(false);
					for (MenuGroup menuGroup : column.getGroup()) {
						int order = 0;
						actionBlock = actionBar.get(column.getColumnIndex(), menuGroup.getHeading());
						if (actionBlock == null) {
							actionBlock = new ActionBlock(menuGroup.getHeading(), column.getColumnIndex());
							actionBar.addActionBlock(actionBlock);
						}
						for (MenuItem menuItem : menuGroup.getMenuItem()) {
							Action action = new Action(menuItem.getDescription(), menuItem.getLink(), menuItem.getTarget());
							actionBlock.addAction(order++, action);
						}
					}
				}
			}
		}

		storeBean(ACTION_BAR_BEAN, actionBar);
	}

	public void storeSubtabs(List<SubTabs> tabs) {
		SortedVector<ActionViewBean> subTabs = new SortedVector<ActionViewBean>();
		for (SubTabs tab : tabs) {
			ActionViewBean action = new ActionViewBean();
			action.setLabel(tab.getLabel());
			action.setUrl(tab.getLink());
			action.setTarget(tab.getTarget());
			action.setSelected(tab.isSelected());
			subTabs.add(tab.getOrder(), action);
		}
		storeBean(SUB_TABS, subTabs);
	}

	protected void storeTable(Table table) throws Exception {
		if (table != null) {
			TableViewBean tableViewBean = new TableViewBean();
			tableViewBean.buildTableOptions(table.getColumnHeadings());
			tableViewBean.getTableOptions().setShowSearchFilter(Boolean.parseBoolean(table.getSearch()));
			tableViewBean.setItems(table.getRows());
			tableViewBean.setRowGroups(TableRowBuilder.getInstance().buildRows(tableViewBean));
			storeBean(TABLE_VIEW_BEAN, tableViewBean);
		}
	}

	protected void processForm(Form form) throws Exception {
		if (form != null) {
			for (FormRow row : form.getRows()) {
				processField(row.getField1());
				processField(row.getField2());
			}
		}
	}

	private void processField(FormField field) {
		if(field != null && field.getType() !=null){
			switch (field.getType()) {
			case "dropdown":
				DropDown dropDown = new DropDown(field.getId(), field.getName());
				dropDown.setLabel(field.getLabel());
				for (FormDropdown dd : field.getDropdown()) {
	
					DropDownItem dropdownItem = new DropDownItem(dd.getValue(), dd.getDisplayValue());
					if (dd.isSelected()) {
						dropDown.setSelectedValue(dd.getValue());
					}
					dropdownItem.setSelected(dd.isSelected());
					dropDown.addItem(dropdownItem);
	
				}
				field.setDropdownBean(dropDown);
				break;
			default:
				break;
			}
		}

	}
	
	private void processResult(ProcessingResult result) {
		if(result != null){
			ProcessingResultViewBean processingResultViewBean = new ProcessingResultViewBean();
			BeanUtilities.copyObject(result, processingResultViewBean);
			processingResultViewBean.setErrorCode(Integer.valueOf(result.getErrorCode()));
			try{
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				processingResultViewBean.setDate(new Timestamp(dateFormat.parse(result.getDate()).getTime()));
			}catch(Exception e){
				
			}
			storeBean("processingResult",processingResultViewBean);
		}else{
			storeBean("processingResult",null);
		}
		
	}

	public void createTechnicalReference() {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(HyphenString.DATE_PATTERN_yyyyMMddHHmmss);
			String date = "";
			date = dateFormat.format(new Date());
			storeBean(TECHNICAL_REFERENCE, "TECH REF - BF - " + date);
		} catch (Exception e) {
			storeBean(TECHNICAL_REFERENCE, "TECH REF - BF");
		}
	}

	protected void processPage(Page page) throws Exception {

		storeSubtabs(page.getSubTabs());

		storeActionBar(page.getMenu());

		processForm(page.getForm());
		
		processResult(page.getResult());

		storeTable(page.getTable());

		storeBean(PAGE, page);
		
		if(page.getTable() != null){
			setJspName("fnb/online/bifrost/server/GenericTableDisplay.jsp");
		}else if (page.getResult() != null){
			setJspName("fnb/online/bifrost/server/GenericResultDisplay.jsp");
		}else{
			setJspName("fnb/online/bifrost/server/GenericDisplay.jsp");
		}

	}



}
