package fnb.online.bifrost.server.system;

import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fnb.online.bifrost.server.navigator.BiFrostNavigator;
import fnb.online.bifrost.server.navigator.ConfigNavigator;
import fnb.online.bifrost.server.navigator.Navigator;

@WebServlet(urlPatterns = "/Controller")
public class Controller extends HttpServlet {

	private static Logger log = Logger.getLogger(Controller.class.getName());
	private final static String fs_text_html = "text/html";
	private final static String fs_nocache = "no-cache";
	private final static String fs_cache_control = "Cache-Control";
	private final static String fs_pragma = "Pragma";
	private final static String fs_expires = "Expires";
	private final static String fs_jsp = ".jsp";
	private final static String fs_html = ".html";
	private static final String FORWARD_SLASH = "/";
	private static String dataPath;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		log.info("Starting up");
		setDataPath(config.getServletContext().getRealPath("WEB-INF/datafiles"));
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		Navigator thisNavigator;
		if("true".equalsIgnoreCase(req.getParameter("config"))){
			thisNavigator = new ConfigNavigator();
		}else{
			thisNavigator = new BiFrostNavigator();
		}
		

		boolean navRc = true; // good rc default if no navigator defined

		if (thisNavigator != null) {

			try {
				if (thisNavigator.initialize(req, getServletContext(), resp)) {
					// Actual call of the navigator. All navigators return a
					// bool which indicates
					// success or failure
					navRc = thisNavigator.performTask(req, resp);
					thisNavigator.createTechnicalReference();
				}

			} catch (Exception e) {
				// Generic error handling for all navigators. Any uncaught
				// exception in the navigator
				// is caught here, the stack printed, a FAS sent and a record
				// made that the navigator failed

				e.printStackTrace();
				navRc = false;
			}
		}

		if (navRc) {
			Entry<String, Long> jspNameAndResponseTime = sendContentToBrowser(thisNavigator, req, resp);
		}

	}

	private Map.Entry<String, Long> sendContentToBrowser(
			Navigator thisNavigator, HttpServletRequest req,
			HttpServletResponse resp) {

		// invoke JSP

		long jspTime = 0;
		long jspSize = 0;
		String jspName;
		if (thisNavigator != null) {
			jspName = thisNavigator.getJspName();
		} else {
			jspName = null;
		}

		// Check if JSP file exists.
		// If not, remove the "/navigator/" portion.
		// This is only needed while we're refactoring for new chameleon
		// naming convention
		String jspRealPath = getServletContext().getRealPath(jspName);
		if (jspRealPath != null && !new File(jspRealPath).exists()) {
			jspName = jspName.replaceAll("/navigator", "");
		}
		// It is valid that there may not be a jsp, so we check first before
		// trying
		// to get it
		// We also skip if we're going to a next state

		// Disable caching. Given the navigation in BUS, the returned info
		// should
		// not be cached by the browser, so tell browser not to cache
		resp.setContentType(fs_text_html);
		resp.setHeader(fs_pragma, fs_nocache); // no caching
		resp.setHeader(fs_cache_control, fs_nocache);
		resp.setDateHeader(fs_expires, 0); // expires immediately

		try {

			RequestDispatcher rd = null;

			// Logging for TUSAGE ito of jsp time
			long startTime = System.currentTimeMillis();

			// Get jsp name and put jsp in a RequestDispatcher supplied by
			// the app server
			if (jspName.indexOf(fs_jsp) != -1 || jspName.indexOf(fs_html) != -1)
				rd = getServletContext().getRequestDispatcher(
						FORWARD_SLASH + jspName);
			else
				rd = getServletContext().getRequestDispatcher(
						FORWARD_SLASH + jspName + fs_jsp);

			// Compile the jsp and send it to the in memory stream
			rd.forward(req, resp);

			jspTime = System.currentTimeMillis() - startTime;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new AbstractMap.SimpleEntry<String, Long>(jspName, jspTime);

	}

	public static String getDataPath() {
		return dataPath;
	}

	public static void setDataPath(String dataPath) {
		Controller.dataPath = dataPath;
	}

}
