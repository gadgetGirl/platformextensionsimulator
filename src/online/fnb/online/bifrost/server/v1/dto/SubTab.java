package fnb.online.bifrost.server.v1.dto;

import java.util.ArrayList;
import java.util.List;
public class SubTab {

    protected List<SubTabItem> items;

    /**
     * Gets the value of the items property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the items property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubTabItem }
     * 
     * 
     */
    public List<SubTabItem> getItems() {
        if (items == null) {
            items = new ArrayList<SubTabItem>();
        }
        return this.items;
    }

	@Override
	public String toString() {
		return "SubTab [items=" + items + "]";
	}

}
