package fnb.online.bifrost.server.v1.dto;

import java.util.ArrayList;
import java.util.List;

public class TableRow {

	protected List<TableRow.Item> items;

	/**
	 * Gets the value of the item property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the item property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getItem().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TableRow.Item }
	 * 
	 * 
	 */
	public List<TableRow.Item> getItems() {
		if (items == null) {
			items = new ArrayList<TableRow.Item>();
		}
		return this.items;
	}

	public static class Item {

		protected String columnHeading;
		protected String rowGrouping;
		protected String fieldName;
		protected String value;
		protected String type;
		protected boolean rightAlign;

		public String getColumnHeading() {
			return columnHeading;
		}

		public void setColumnHeading(String columnHeading) {
			this.columnHeading = columnHeading;
		}

		public String getRowGrouping() {
			return rowGrouping;
		}

		public void setRowGrouping(String rowGrouping) {
			this.rowGrouping = rowGrouping;
		}

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public boolean isRightAlign() {
			return rightAlign;
		}

		public void setRightAlign(boolean rightAlign) {
			this.rightAlign = rightAlign;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getType() {
			return type;
		}

		public void setType(String value) {
			this.type = value;
		}

		@Override
		public String toString() {
			return "Item [columnHeading=" + columnHeading + ", fieldName="
					+ fieldName + ", value=" + value + ", type=" + type
					+ ", rightAlign=" + rightAlign + "]";
		}

	}

	@Override
	public String toString() {
		return "TableRow [item=" + items + "]";
	}

}
