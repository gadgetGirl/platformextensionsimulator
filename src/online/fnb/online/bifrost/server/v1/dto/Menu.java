
package fnb.online.bifrost.server.v1.dto;



public class Menu {
	
    protected Column column1;
    protected Column column2;
    protected Column column3;
    protected Column column4;
    protected String header;


    public Column getColumn1() {
        if (column1 == null) {
            column1 = new Column();
        }
        return this.column1;
    }


    public Column getColumn2() {
        if (column2 == null) {
            column2 = new Column();
        }
        return this.column2;
    }
    
    public Column getColumn3() {
        if (column3 == null) {
            column3 = new Column();
        }
        return this.column3;
    }
    
    public Column getColumn4() {
        if (column4 == null) {
            column4 = new Column();
        }
        return this.column4;
    }
    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeader(String value) {
        this.header = value;
    }


	@Override
	public String toString() {
		return "Menu [column1=" + column1 + ", column2=" + column2
				+ ", column3=" + column3 + ", column4=" + column4 + ", header=" + header + "]";
	}

}
