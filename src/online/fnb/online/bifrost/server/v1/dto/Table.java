package fnb.online.bifrost.server.v1.dto;

import java.util.ArrayList;
import java.util.List;

public class Table {

    protected List<TableRow> rows;
    protected String emptyMessage;
    protected Boolean search;
    protected Boolean print;
    protected Boolean download;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TableRow }
     * 
     * 
     */
    public List<TableRow> getRows() {
        if (rows == null) {
            rows = new ArrayList<TableRow>();
        }
        return this.rows;
    }

    /**
     * Gets the value of the emptyMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmptyMessage() {
        return emptyMessage;
    }

    /**
     * Sets the value of the emptyMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmptyMessage(String value) {
        this.emptyMessage = value;
    }

    /**
     * Gets the value of the search property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearch() {
        return search;
    }

    /**
     * Sets the value of the search property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearch(Boolean value) {
        this.search = value;
    }

    /**
     * Gets the value of the print property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrint() {
        return print;
    }

    /**
     * Sets the value of the print property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrint(Boolean value) {
        this.print = value;
    }

    /**
     * Gets the value of the download property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDownload() {
        return download;
    }

    /**
     * Sets the value of the download property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDownload(Boolean value) {
        this.download = value;
    }

	@Override
	public String toString() {
		return "Table [rows=" + rows + ", emptyMessage=" + emptyMessage
				+ ", search=" + search + ", print=" + print + ", download="
				+ download + "]";
	}

}
