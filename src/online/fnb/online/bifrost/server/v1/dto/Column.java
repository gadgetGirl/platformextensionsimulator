package fnb.online.bifrost.server.v1.dto;

import java.util.ArrayList;
import java.util.List;

public class Column {
	
    protected List<Column.MenuItem> menuItems;

    /**
     * Gets the value of the menuItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the menuItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMenuItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Column.MenuItem }
     * 
     * 
     */
    public List<Column.MenuItem> getMenuItems() {
        if (menuItems == null) {
            menuItems = new ArrayList<Column.MenuItem>();
        }
        return this.menuItems;
    }



    public static class MenuItem {

        protected String group;
        protected String label;
        protected String link;

        /**
         * Gets the value of the group property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroup() {
            return group;
        }

        /**
         * Sets the value of the group property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroup(String value) {
            this.group = value;
        }

        /**
         * Gets the value of the label property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLabel() {
            return label;
        }

        /**
         * Sets the value of the label property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLabel(String value) {
            this.label = value;
        }

        /**
         * Gets the value of the link property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLink() {
            return link;
        }

        /**
         * Sets the value of the link property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLink(String value) {
            this.link = value;
        }
        
        @Override
    	public String toString() {
    		return "MenuItem [group=" + group + ", label=" + label + ",link=" + link + "]";
    	}

    }



	@Override
	public String toString() {
		return "Column [menuItem=" + menuItems + "]";
	}

}
