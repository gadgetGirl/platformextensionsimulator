package fnb.online.bifrost.server.xml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.NullConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import fnb.online.bifrost.server.dto.CellItem;
import fnb.online.bifrost.server.dto.ColumnHeader;
import fnb.online.bifrost.server.dto.FooterButton;
import fnb.online.bifrost.server.dto.Form;
import fnb.online.bifrost.server.dto.FormDropdown;
import fnb.online.bifrost.server.dto.FormField;
import fnb.online.bifrost.server.dto.FormRow;
import fnb.online.bifrost.server.dto.Menu;
import fnb.online.bifrost.server.dto.MenuColumns;
import fnb.online.bifrost.server.dto.MenuGroup;
import fnb.online.bifrost.server.dto.MenuItem;
import fnb.online.bifrost.server.dto.Page;
import fnb.online.bifrost.server.dto.ProcessingResult;
import fnb.online.bifrost.server.dto.RowItem;
import fnb.online.bifrost.server.dto.SubTabs;
import fnb.online.bifrost.server.dto.Table;
import fnb.online.bifrost.server.dto.TableColumnHeader;
import fnb.online.bifrost.server.dto.TableRow;

public class XStreamUtility {

	private XStream xstream = new XStream();
	private static XStreamUtility instance = new XStreamUtility();
	boolean useXStream = false;
	
	private void initXStream(){
		
		
		xstream = new XStream(new StaxDriver() {
		    public HierarchicalStreamWriter createWriter(Writer out) {
		    	return new PrettyPrintWriter(out);
		    }
		}); 
		
		useXStream = true;
		
		xstream.registerConverter(new NullConverter());

		xstream.aliasType("topItem", CellItem.class);
		xstream.aliasType("bottomItem", CellItem.class);
		xstream.aliasType("columnHeading", ColumnHeader.class);
		xstream.aliasType("menu", Menu.class);
		xstream.aliasType("columns", MenuColumns.class);
		xstream.aliasType("group", MenuGroup.class);
		xstream.aliasType("menuItem", MenuItem.class);
		xstream.aliasType("Page", Page.class);
		xstream.aliasType("item", RowItem.class);
		xstream.aliasType("subTabs", SubTabs.class);
		xstream.aliasType("table", Table.class);
		xstream.aliasType("columnHeadings", TableColumnHeader.class);
		xstream.aliasType("rows", TableRow.class);
		xstream.aliasType("formRow", FormRow.class);
		xstream.aliasType("footerButton", FooterButton.class);
		xstream.aliasType("formDropdown", FormDropdown.class);
		xstream.aliasType("form", Form.class);
		xstream.aliasType("formField", FormField.class);
		xstream.aliasType("processingResult", ProcessingResult.class);
		xstream.aliasType("table", Table.class);
		
	}

	private XStreamUtility() {
		super();
		initXStream();
	}
	
	public static synchronized XStreamUtility getInstance() {
		if(instance == null){
			instance = new XStreamUtility();
		}
		return instance;
	}
	
	
	public Page getPage(String xml) {
		return (Page) xstream.fromXML(xml);
	}
	
	
	public String getXMl(Page page) {
		String result;
		
		if(useXStream){
			return xstream.toXML(page);
		}
		
		try {
			StringWriter sw = new StringWriter();
			JAXBContext carContext = JAXBContext.newInstance(Page.class);
			Marshaller carMarshaller = carContext.createMarshaller();
			carMarshaller.marshal(page, sw);
			result = sw.toString();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}

		return result;
	}
	
	public boolean validatedXMLString(String xml, String xsd) {
		Source xmlFile = new StreamSource(new StringReader(xml));
		try {
			
			SchemaFactory schemaFactory = SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File(xsd));
			Validator validator = schema.newValidator();
			validator.validate(xmlFile);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	public static void main(String[] args) {

		String responseXML;
//		try {
//			responseXML = readFile("/home/pieter/workspace/projects/BiFrostClient/WebContent/WEB-INF/xml/test.xml");
//			System.err.println(XStreamUtility.getInstance().getPage(responseXML));
//			System.err.println(XStreamUtility.getInstance().getXMl(XStreamUtility.getInstance().getPage(responseXML)));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		Page page = new Page();
//		SubTabs tab = new SubTabs();
//		for (int i = 0; i < 3 ; i++){
//			tab = new SubTabs();
//			tab.setLink("http://localhost:8080/banking/banking/Controller?nav=accounts.summaryofaccountbalances.navigator.SummaryOfAccountBalances&amp;FARFN=899");
//			
//			tab.setLabel("My Accounts" + i);
//			tab.setTarget(0);
//			tab.setOrder(i);
//			tab.setSelected((i == 1)?"true":"false");
//			page.getSubTabs().add(tab);
//		}
//		
//		System.err.println(XStreamUtility.getInstance().getXMl(page));;
//		
		Page page = new Page();
		page.setTable(new Table());
		TableColumnHeader header = new TableColumnHeader();
		ColumnHeader heading =  new ColumnHeader();
		heading.setLabel("Name");
		heading.setRowItemName("nickname");
		header.getColumnHeading().add(heading);
		heading =  new ColumnHeader();
		heading.setLabel("Balance");
		heading.setRowItemName("ledgerBalance");
		header.getColumnHeading().add(heading);
		heading =  new ColumnHeader();
		heading.setLabel("Available Balance");
		heading.setRowItemName("availablebalance");
		header.getColumnHeading().add(heading);
			
		page.getTable().setColumnHeadings(header);
		
		TableRow row = new TableRow();
		RowItem item = new RowItem();
		CellItem topCellItem = new CellItem();
		topCellItem.setName("nickname");
		topCellItem.setText("Business Cheque Account");
		topCellItem.setTarget(0);
		topCellItem.setType("link");
		topCellItem.setLink("http://localhost:8080/banking/Controller?nav=navigator.AccountView&amp;accountRefNum=-5285038&amp;productCode=DDA&amp;action=prepareMaintainAccount&amp;FARFN=89");
		
		item.setTopItem(topCellItem);
		CellItem bottomCellItem = new CellItem();
		bottomCellItem.setName("accountNumber");
		bottomCellItem.setText("62003444297");
		bottomCellItem.setType("text");
		item.setBottomItem(bottomCellItem);
		row.getItem().add(item);
		
		item = new RowItem();
		topCellItem = new CellItem();
		topCellItem.setName("ledgerBalance");
		topCellItem.setText("R -138 411.34");
		topCellItem.setType("text");
		item.setTopItem(topCellItem);
	
		row.getItem().add(item);
		
		item = new RowItem();
		topCellItem = new CellItem();
		topCellItem.setName("availablebalance");
		topCellItem.setText("R -138 411.34");
		topCellItem.setType("link");
		topCellItem.setLink("http://localhost:8080/banking/Controller?nav=transactionhistory.navigator.TransactionHistoryRedirect&amp;productCode=DDA&amp;anrfn=-5285038&amp;initial=initial&amp;FARFN=90");
		item.setTopItem(topCellItem);
	
		row.getItem().add(item);
		page.getTable().getRows().add(row);
		
		item = new RowItem();
		topCellItem = new CellItem();
		topCellItem.setName("nickname");
		topCellItem.setText("FNB Gold Cheque Account");
		topCellItem.setTarget(0);
		topCellItem.setType("link");
		topCellItem.setLink("http://localhost:8080/banking/Controller?nav=navigator.AccountView&amp;accountRefNum=-5285038&amp;productCode=DDA&amp;action=prepareMaintainAccount&amp;FARFN=89");
		
		item.setTopItem(topCellItem);
		bottomCellItem = new CellItem();
		bottomCellItem.setName("accountNumber");
		bottomCellItem.setText("62003444297");
		bottomCellItem.setType("text");
		item.setBottomItem(bottomCellItem);
		row.getItem().add(item);
		
		item = new RowItem();
		topCellItem = new CellItem();
		topCellItem.setName("ledgerBalance");
		topCellItem.setText("R 1");
		topCellItem.setType("text");
		item.setTopItem(topCellItem);
	
		row.getItem().add(item);
		
		item = new RowItem();
		topCellItem = new CellItem();
		topCellItem.setName("availablebalance");
		topCellItem.setText("R 1");
		topCellItem.setType("link");
		topCellItem.setLink("http://localhost:8080/banking/Controller?nav=transactionhistory.navigator.TransactionHistoryRedirect&amp;productCode=DDA&amp;anrfn=-5285038&amp;initial=initial&amp;FARFN=90");
		item.setTopItem(topCellItem);
	
		row.getItem().add(item);
		page.getTable().getRows().add(row);
		
		System.err.println(XStreamUtility.getInstance().getXMl(page));;
		
	}
	
	private static String readFile( String file ) throws IOException {
	    BufferedReader reader = new BufferedReader( new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");

	    while( ( line = reader.readLine() ) != null ) {
	        stringBuilder.append( line );
	        stringBuilder.append( ls );
	    }

	    return stringBuilder.toString();
	}
	
	public static void writeFile(String fileName, String xml ) throws IOException {
		FileOutputStream fo;
		try {
			fo = new FileOutputStream(fileName);
			fo.write(xml.getBytes());
			fo.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			fo = null;
		}
	}
	
	

}
