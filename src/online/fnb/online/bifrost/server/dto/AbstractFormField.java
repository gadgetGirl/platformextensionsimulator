
package fnb.online.bifrost.server.dto;

import fnb.online.tags.beans.dropdown.AbstractDropdown;

public class AbstractFormField {
	protected AbstractDropdown dropdownBean;

	public AbstractDropdown getDropdownBean() {
		return dropdownBean;
	}

	public void setDropdownBean(AbstractDropdown dropdownBean) {
		this.dropdownBean = dropdownBean;
	}
	
	
}
