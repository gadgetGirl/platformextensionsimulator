package fnb.online.bifrost.server.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import mammoth.utility.HyphenString;
import fnb.online.tags.beans.radiogroup.RadioGroup;
import fnb.online.tags.beans.table.TableAmountInputItem;
import fnb.online.tags.beans.table.TableCheckBoxItem;
import fnb.online.tags.beans.table.TableDateBoxItem;
import fnb.online.tags.beans.table.TableDoubleItem;
import fnb.online.tags.beans.table.TableHyperLinkItem;
import fnb.online.tags.beans.table.TableInputItem;
import fnb.online.tags.beans.table.TableItem;
import fnb.online.tags.beans.table.TableRadioButtonItem;
import fnb.online.tags.beans.table.TableRow;
import fnb.online.tags.beans.table.TableTextItem;

public abstract class AbstractTableRowResolver{	
	
	private int parentFunction = -1;
	private int anrfn = 0;
    protected Logger log = null;
    private boolean showGroupButtons = false;
    public static String SUBROW = "subRow";
	
	/**
     * 
     */
    public AbstractTableRowResolver() {
        log  = Logger.getLogger(getClass().getName());
    }

	public abstract TableRow resolveRow(Object item) throws Exception;
	
	public int getParentFunction() {
		return parentFunction;
	}

	public void setParentFunction(int parentFunction) {
		this.parentFunction = parentFunction;
	}

	public int getAnrfn() {
		return anrfn;
	}

	public void setAnrfn(int anrfn) {
		this.anrfn = anrfn;
	}

	protected TableItem buildText(String fieldName, String value) {
		TableTextItem cell = new TableTextItem(fieldName, fieldName);
		if (value != null) cell.setCellValue(value);
		else {
			cell.setCellValue(HyphenString.EMPTY_STRING);	
		}
		return cell;
	}
	
	protected TableItem buildInput(String fieldName, String value) {
		TableInputItem cell = new TableInputItem(fieldName, fieldName);
		cell.setCellValue(value);
		return cell;
	}
	
	protected TableItem buildInput(String fieldName, String value, boolean hidden) {
		TableInputItem cell = new TableInputItem(fieldName, fieldName, hidden);
		cell.setCellValue(value);
		return cell;
	}
	
	protected TableItem buildAmountInput(String fieldName, BigDecimal value) {
		TableAmountInputItem cell = new TableAmountInputItem(fieldName);
		cell.setAmount(value);
		return cell;
	}
	
	protected TableItem buildRadioButton(String fieldName, String value, RadioGroup radioGroup) {
		TableRadioButtonItem cell = new TableRadioButtonItem(fieldName, fieldName);
		cell.setRadioGroup(radioGroup);
		cell.setCellValue(value);
		return cell;
	}
	
	protected TableItem buildRadioButton(String fieldName, String value, RadioGroup radioGroup, boolean useCount) {
		TableRadioButtonItem cell = new TableRadioButtonItem(fieldName, fieldName);
		cell.setUseCount(useCount);
		cell.setRadioGroup(radioGroup);
		cell.setCellValue(value);
		return cell;
	}
	
	protected TableItem buildCheckbox(String fieldName, String value) {
		return buildCheckbox(fieldName, value, false);
	}
	
	protected TableItem buildCheckbox(String fieldName, String value, boolean checked) {
		return buildCheckbox(fieldName, value,"", checked);
	}
	
	protected TableItem buildCheckbox(String fieldName, String value, String uncheckedValue) {
		return buildCheckbox(fieldName, value,uncheckedValue, false);
	}
	
	protected TableItem buildCheckbox(String fieldName, String value, String uncheckedValue, boolean checked) {
		return buildCheckbox(fieldName, value, uncheckedValue, checked, false, false);
	}
	
	protected TableItem buildCheckbox(String fieldName, String value, String uncheckedValue, boolean checked, boolean disabled) {
		return buildCheckbox(fieldName, value, uncheckedValue, checked, disabled, false);
	}
	
	protected TableItem buildCheckbox(String fieldName, String value, String uncheckedValue, boolean checked, boolean disabled, boolean singleSelect) {
		TableCheckBoxItem cell = new TableCheckBoxItem(fieldName, fieldName);
		cell.setSelected(checked);
		cell.setValue(value);
		cell.setUncheckedValue(uncheckedValue);
		cell.setDisabled(disabled);
		cell.setSingleSelect(singleSelect);
		return cell;
	}

	protected TableItem buildDateBox(String fieldName, Date value) {
		TableDateBoxItem cell = new TableDateBoxItem(fieldName, fieldName);
		cell.setCellValue(HyphenString.formatPrettyDate(value));
		return cell;
	}

	protected TableItem buildHyperLink(String fieldName, String value, String url){
		TableHyperLinkItem cell = new TableHyperLinkItem(fieldName, fieldName);
		cell.setUrl(url);
		cell.setCellValue(value);
		return cell;
	}
	
	protected TableItem buildHyperLink(String fieldName, String value, String url, int target){
		TableHyperLinkItem cell = new TableHyperLinkItem(fieldName, fieldName);
		cell.setUrl(url);
		cell.setCellValue(value);
		cell.setTarget(target);
		return cell;
	}
	
	protected TableItem buildDouble(String fieldName, String value, TableItem topItem, TableItem bottomItem) {
		TableDoubleItem cell = new TableDoubleItem(fieldName, fieldName);
		cell.setTopItem(topItem);
		cell.setBottomItem(bottomItem);
		cell.setCellValue(value);
		return cell;
	}
	
		
	protected boolean isChanged(Object item, String fieldName, Map<Object,List<String>> changes) {
		if (changes != null && changes.size() > 0) {
			return changes.containsKey(item) && changes.get(item).contains(fieldName);
		}
		return false;
	}
	
	public boolean isShowGroupButtons() {
		return showGroupButtons;
	}

	public void setShowGroupButtons(boolean showGroupButtons) {
		this.showGroupButtons = showGroupButtons;
	}

}