package fnb.online.bifrost.server.service;

import fnb.online.bifrost.server.dto.CellItem;
import fnb.online.bifrost.server.dto.RowItem;
import fnb.online.tags.beans.table.TableItem;
import fnb.online.tags.beans.table.TableRow;

public class TableRowResolver extends AbstractTableRowResolver {

	@Override
	public TableRow resolveRow(Object row) throws Exception {

		TableRow tableRow = new TableRow();
		tableRow.setHeading(((fnb.online.bifrost.server.dto.TableRow) row)
				.getHeading());

		for (RowItem rowItem : ((fnb.online.bifrost.server.dto.TableRow) row)
				.getItem()) {
			if (rowItem.getTopItem() != null && rowItem.getBottomItem() != null) {
				tableRow.addCell(buildDouble(rowItem.getTopItem().getName(), rowItem.getTopItem().getText(),
						buildCell(rowItem.getTopItem()), buildCell(rowItem.getBottomItem())));
			} else if (rowItem.getTopItem() != null) {
				tableRow.addCell(buildCell(rowItem.getTopItem()));
			}

		}

		return tableRow;
	}

	private TableItem buildCell(CellItem cellItem) {
		if(cellItem.getType() != null){
			switch (cellItem.getType().toLowerCase()) {
			case "link":
				return buildHyperLink(cellItem.getName(),
						cellItem.getText(), cellItem.getLink(), cellItem.getTarget());
			default:
				return buildText(cellItem.getName(), cellItem.getText());
			}
		}
		return buildText(cellItem.getName(), cellItem.getText());

	}

}