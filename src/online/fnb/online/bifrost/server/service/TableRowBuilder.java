package fnb.online.bifrost.server.service;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import fnb.online.bifrost.server.bean.TableViewBean;
import fnb.online.tags.beans.AbstractViewBean;
import fnb.online.tags.beans.table.TableRow;
import fnb.online.tags.beans.table.TableRowGroup;

public class TableRowBuilder {

	private static TableRowBuilder instance = null;
	protected Logger log = null;

	private TableRowBuilder() {
		log = Logger.getLogger(getClass().getName());
	}

	public static TableRowBuilder getInstance() {
		if (instance == null) {
			instance = new TableRowBuilder();
		}
		return instance;
	}

	public List<TableRowGroup> buildRows(TableViewBean tableViewBean) throws Exception {
		long starttime = System.currentTimeMillis();
		Map<String, TableRowGroup> groupMap = new LinkedHashMap<String, TableRowGroup>();
		Collection<AbstractViewBean> collection = tableViewBean.getItems();
		if(collection == null){
			return new LinkedList<TableRowGroup>();
		}
		
		AbstractTableRowResolver rowResolver = tableViewBean.getTableRowResolver();
		
		TableRow tableRow;

		for (Object row : collection) {
			int cntr = 0;
							
			tableRow = rowResolver.resolveRow(row);
			
			// a null row is an indicator to not add the row
			if(tableRow==null) {
			    continue;
			}
			
			tableRow.setCounter(cntr++);
			
			if(!tableViewBean.isIgnoreRowGroups()){
				//Only group by heading if the collection has a default sort method and table search or sorting was not requested.
				addToRowGroup(groupMap, tableRow);
			}else{
				tableRow.setHeading("");
				tableRow.setGroupCode("");
				addToRowGroup(groupMap, tableRow);
			}
				
		}
		tableViewBean.overrideColumnHeaderURL(groupMap);
		return new LinkedList<TableRowGroup>(groupMap.values());

	}

	private void addToRowGroup(Map<String, TableRowGroup> groupMap,
			TableRow tableRow) {
		if (!groupMap.containsKey(tableRow.getHeading())) {
			TableRowGroup tableRowGroup = new TableRowGroup(
					tableRow.getHeading(), tableRow.getGroupCode());
			tableRowGroup.setHeading(tableRow.getHeading()
					+ tableRow.getNonGroupedHeading());
			groupMap.put(tableRow.getHeading(), tableRowGroup);
		}
		groupMap.get(tableRow.getHeading()).addTableRow(tableRow);
	}

}
