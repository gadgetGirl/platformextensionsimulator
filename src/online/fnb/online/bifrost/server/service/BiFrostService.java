package fnb.online.bifrost.server.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import fnb.online.bifrost.server.system.Controller;

public class BiFrostService {

	private static HttpClient client;
	private static BiFrostService instance;
	private static EndPointConfig endPointConfig;

	private BiFrostService() {
		// TODO : rework the HTTPClient pooling to create an instance for the
		// http request to maintain data and Session integrity
		client = new HttpClient();
	}

	public synchronized static BiFrostService getInstance() {
		if (instance == null) {
			instance = new BiFrostService();
		}
		return instance;
	}

	public String doExtensionRequest(BiFrostContext context) {

		String result = "";

		client.getParams().setParameter("http.useragent", "BiFrost Service");
		PostMethod postRequest = new PostMethod(findEndPointConfig(context).getUrl());

		for (String k : context.getRequestParameters().keySet()) {
			for (String parmValue : context.getRequestParameters().get(k)) {
				postRequest.addParameter(k, parmValue);
			}

		}

		try {
			client.executeMethod(postRequest);
			result = postRequest.getResponseBodyAsString();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			postRequest.releaseConnection();
		}

		return result;

	}

	private EndPointConfig findEndPointConfig(BiFrostContext context) {
		if (endPointConfig == null || !endPointConfig.getEndPointName().equals(context.getRequestParameters().get("EndPoint"))) {
			synchronized (context) {
				endPointConfig = new EndPointConfig(context.getRequestParameters().get("EndPoint"));
			}
					}
		return endPointConfig;
	}

	class EndPointConfig {

		private String endPointName;
		private String url;
		private String file = Controller.getDataPath() + "/endpoint.properties";

		public EndPointConfig(List<String> list) {
			Properties properties = new Properties();
			BufferedReader reader;
			if (list != null && list.size() == 1) {
				try {
					reader = new BufferedReader(new FileReader(file));
					properties.load(reader);
					endPointName = list.get(0);
					url = properties.getProperty(endPointName, "http://localhost:8080/BiFrostServer/Controller");
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (endPointName == null) {
				endPointName = "Default";
				url = "http://localhost:8080/BiFrostServer/Controller?nav=Config";
			}
		}

		public String getEndPointName() {
			return endPointName;
		}

		public void setEndPointName(String endPointName) {
			this.endPointName = endPointName;
		}

		public String getUrl() {
			System.err.println(url);
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
}
