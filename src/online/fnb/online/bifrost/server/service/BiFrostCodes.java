package fnb.online.bifrost.server.service;


public class BiFrostCodes{


	//Function Action types --333
	public final static int ACTION_TYPE = 333;
	public final static int ACTION_TYPE_TOPMENU   = 0;
	public final static int ACTION_TYPE_OPTIONS   = 1;
	public final static int ACTION_TYPE_SUBTAB  = 2;
	public final static int ACTION_TYPE_HYPERLINK  = 3;
	public final static int ACTION_TYPE_TABLE_SWITCH  = 4;
	public final static int ACTION_TYPE_EZI_BUTTONS = 5;
	public final static int ACTION_TYPE_EXPANDABLE_LINK = 6;
	public final static int ACTION_TYPE_FOOTER_BUTTON = 7;
	public final static int ACTION_TYPE_MORE_OPTIONS = 8;
	public final static int ACTION_TYPE_ROW_OPTIONS = 9;
	public final static int ACTION_TYPE_TOPMENU_OPTIONS = 10;
	public final static int ACTION_TYPE_TABLE_HEADER_BUTTON = 11;
	public final static int ACTION_TYPE_GROUP_BUTTON = 12;
	
	
	//Action Bar attributes --334
	public static final int ACTION_TARGET= 334;
    public static final int ACTION_TARGET_URL_WORKSPACE = 0;
    public static final int ACTION_TARGET_URL_PLACEHOLDER = 1;
    public static final int ACTION_TARGET_SHOW_CONTENT = 2;
    public static final int ACTION_TARGET_SHOW_EZI_PANEL = 3;
    public static final int ACTION_TARGET_PRINT_DIV = 4;
    public static final int ACTION_TARGET_DOWNLOAD = 5;
    public static final int ACTION_TARGET_REDIRECT = 6;
    public static final int ACTION_TARGET_POPUP = 7;
    public static final int ACTION_TARGET_NEW_WINDOW = 8;
}
