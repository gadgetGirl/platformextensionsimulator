package fnb.online.bifrost.server.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BiFrostContext {

	private Map<String, List<String>> requestParameters = new HashMap<String, List<String>>();

	public void addRequestParm(String parmName, String parmValue) {

		List<String> parmValueList = null;
		if (!requestParameters.containsKey(parmName)) {
			parmValueList = new ArrayList<String>();
			requestParameters.put(parmName, parmValueList);
		}

		parmValueList.add(parmValue);

	}

	public Map<String, List<String>> getRequestParameters() {
		return requestParameters;
	}
}
