package fnb.online.bifrost.server.paging;

import java.util.logging.Logger;

public class PagingAndSortingHandler {

	public final static String TABLEBEAN = "tableBean";
	public final static String SEARCH_TABLE_BY_VALUE = "searchTableByValue";
	public final static String CHANGE_PAGE_SIZE = "changePageSize";
	public final static String SORTMETHOD = "SORTMETHOD";
	private int newPageSize = -1;
	private String viewBeanKey;
	private boolean ignoreRowGroups = false;
	private static Logger log = Logger.getLogger(PagingAndSortingHandler.class.getName()); 
		
	public PagingAndSortingHandler(String viewBeanKey) {
		this.viewBeanKey = viewBeanKey;
	}

//	public void processContainer(ResultContainer resultContainer, User user) {
//		if(user == null){
//			user = new User(new HyphenParameter(), 0);
//			
//		}
//		doSearch(resultContainer);
//		
//		changePageSize(resultContainer, user);
//		
//		doPagingAndSorting(resultContainer, user);
//	}
//	
//	private void doPagingAndSorting(ResultContainer resultContainer, User user) {
//		PagingViewBean pvb = new PagingViewBean();
//		pvb.setResultContainerKey(resultContainer.getKey());
//		if (viewBeanKey != null ) {
//			// If it was set, get both the Pager object and the ViewBean
//			Pager pager = resultContainer.getPager(viewBeanKey);
//			String sortingMethodName = HyphenString.EMPTY_STRING;
//			if (resultContainer.getBean(PagingAndSortingHandler.SORTMETHOD) != null && HyphenString.isValidString((String)resultContainer.getBean(PagingAndSortingHandler.SORTMETHOD))) {
//				sortingMethodName = (String)resultContainer.getBean(PagingAndSortingHandler.SORTMETHOD);
//				resultContainer.removeBean(PagingAndSortingHandler.SORTMETHOD);
//			}
//			ViewBean bean = resultContainer.getViewBean(viewBeanKey);
//			pvb.setPagingOff(bean.isPagingOff());
//			// If the new sort method is the same as the sort method already applied, set the reverseOrder flag in
//			// in the ResultConatiner so that order will be reversed rather than the data resorted
//			
//			
//			if(HyphenString.isValidString(sortingMethodName)){
//				if(pager.setComparator(sortingMethodName)){
//					if (pager.getSortMethod() != null && pager.getSortMethod().equals(sortingMethodName)) {
//						pager.setReverseOrder(!pager.isReverseOrder());
//					} else {
//						pager.setReverseOrder(false);
//						pager.setSortMethod(sortingMethodName);
//					}
//					if (pager.isReverseOrder()) {
//						pager.reverseOrder();
//					} else {
//						pager.sort();
//					}
//
//					// After a sort, always go to page one unless we don't want
//					// to page for a specific reason - a zero indicates no
//					// paging
//					if (pager.getCurrentPage() != Pager.NO_PAGING) {
//						pager.setCurrentPage(1);
//						// Change contents of the ViewBean to be the 1st page of
//						// the sorted data
//						bean.setItems(pager.current());
//						// Clear sorting flags
//					} else {
//						bean.setItems(pager.getAll());
//					}
//					ignoreRowGroups = true;
//				}
//			}
//
//			boolean paging = pager.getCurrentPage() != Pager.NO_PAGING;
//			
//			if (pager.getCurrentPage() > Pager.NO_PAGING) {
//				// set new page number and the put the right page in the Viewbean
//				bean.setItems(pager.current());
//			} 
//			// Set the paging view bean for this page
//			if (paging) {
//				pvb.setNumberOfPages(pager.getNumberOfPages());
//				pvb.setCurrentPageNumber(pager.getCurrentPage());
//				pvb.setNumberOfItems(pager.getCollectionSize());
//				pvb.setItems(pager.getAll());
//				pvb.setPageSize(pager.getPageSize());
//			} else {
//				pvb.setNumberOfPages(1);
//				pvb.setCurrentPageNumber(1);
//				pvb.setNumberOfItems(pager.getCollectionSize());
//				pvb.setItems(pager.getAll());
//				pvb.setPageSize(pager.getPageSize());
//			}
//			pvb.setMaxNumberofLimiterItems(pager.getMaxNumberofLimiterItems());
//			pvb.setSearchTableByValue(pager.getSearchTableByValue());
//			pvb.setViewBeanKey(viewBeanKey);
//			
//			if(bean instanceof TableViewBean){
//				
//				try {
//					((TableViewBean)bean).setPagingViewBean(pvb);
//					if(ignoreRowGroups){
//						//Do not reset it to false if it has potentially been set already 
//						((TableViewBean)bean).setIgnoreRowGroups(true);
//					}
//					((TableViewBean)bean).setRowGroups(TableRowBuilder.getInstance().buildRows((TableViewBean)bean, user));
//					calculateTotals(((TableViewBean)bean).getTableOptions(), pager.getAll());
//					resultContainer.addBean(TABLEBEAN, bean);
//				} catch (TableRowException e) {
//					System.err.println("!!!! Could not store TableViewBean !!!!");
//					e.printStackTrace();
//				}
//				
//			}
//		}
//		
//	}
//	
//	private void changePageSize(ResultContainer resultContainer, User user) {
//		String changePageSize = (String) resultContainer.getBean(CHANGE_PAGE_SIZE); 
//		
//		if (changePageSize != null && changePageSize.length() > 0) {
//			newPageSize = Integer.parseInt(changePageSize);
//			resultContainer.getPager(viewBeanKey).setCurrentPage(1);
//			resultContainer.getPager(viewBeanKey).setPageSize(newPageSize);
//			resultContainer.getViewBean(viewBeanKey).setItems(resultContainer.getPager(viewBeanKey).current());
//			resultContainer.getPager(viewBeanKey).setChangedPageSize(true);
//			resultContainer.removeBean(CHANGE_PAGE_SIZE);
//			user.setNumResultsToDisplay(newPageSize);
//		} else {
//			if (viewBeanKey != null) {
//				resultContainer.getPager(viewBeanKey).setChangedPageSize(false);
//			}
//		}
//		
//	}
//
//	public void doSearch(ResultContainer resultContainer) {
//
//		String searchTableByValue = (String) resultContainer.getBean(SEARCH_TABLE_BY_VALUE);
//		resultContainer.removeBean(SEARCH_TABLE_BY_VALUE);
//		
//		FieldFilter filter = null;
//
//		if (HyphenString.isValidString(searchTableByValue)
//				&& searchTableByValue.equals(Pager.NO_SEARCH_VALUE_ENTERED)) {
//			resultContainer.getPager(viewBeanKey).setSearchTableByValue(
//					HyphenString.EMPTY_STRING);
//			if (resultContainer.getPager(viewBeanKey).getUnfilteredList() != null) {
//				resultContainer.getPager(viewBeanKey).setCollectionToPage(
//						resultContainer.getPager(viewBeanKey)
//								.getUnfilteredList());
//			}
//			resultContainer.getPager(viewBeanKey).setCurrentPage(1);
//			resultContainer.getViewBean(viewBeanKey).setItems(
//					resultContainer.getPager(viewBeanKey).current());
//			searchTableByValue = HyphenString.EMPTY_STRING;
//		}
//		
//		if (HyphenString.isValidString(searchTableByValue) &&
//				resultContainer.getBean(viewBeanKey) != null) {
//
//			TableViewBean tableViewBean = (TableViewBean) resultContainer.getBean(viewBeanKey);
//			filter = new FieldFilter();
//			filter.setBeanKey(viewBeanKey);
//			
//			//Replaced the section above to allow for double fields to be searched aswell
//			for(String fieldName : tableViewBean.getFilterFieldNames()){
//				filter.addCriteriaValue(searchTableByValue, Filter.FUZZY);
//				filter.addCriteriaField(fieldName);
//			}
//		}
//
//		if (resultContainer.getFilter()!=null){
//			Filter mainFilter = (Filter) resultContainer.getFilter();
//			Pager pager = resultContainer.getPager(mainFilter.getBeanKey());
//			HyphenSortedVector toPage;
//			
//			if (pager.getUnfilteredList() == null)
//			{
//				toPage = pager.getCollectionToPage();
//			}
//			else
//			{
//				toPage = pager.getUnfilteredList();
//			}
//			pager.setUnfilteredList((HyphenSortedVector)toPage.clone());
//			FilteredVector fv = new FilteredVector(toPage);
//			fv.setFilter(mainFilter);
//			if (searchTableByValue != null && searchTableByValue.length() > 0) {
//				pager.setSearchTableByValue(searchTableByValue);
//				fv.searchTableFilterElements();
//			} else {
//				fv.filterElements();
//			}
//			
//			pager.setCollectionToPage(fv);
//			resultContainer.setFilter(null);
//		}
//		
//		if (filter != null) {
//			Pager pager = resultContainer.getPager(filter.getBeanKey());
//			HyphenSortedVector toPage;
//			if (pager.getUnfilteredList() == null) {
//				toPage = pager.getCollectionToPage();
//			} else {
//				toPage = pager.getUnfilteredList();
//			}
//			pager.setUnfilteredList((HyphenSortedVector) toPage.clone());
//			FilteredVector fv = new FilteredVector(toPage);
//			fv.setFilter(filter);
//			if (searchTableByValue != null && searchTableByValue.length() > 0) {
//				pager.setSearchTableByValue(searchTableByValue);
//				fv.searchTableFilterElements();
//			} else {
//				fv.filterElements();
//			}
//
//			pager.setCollectionToPage(fv);
//			if (pager.getCurrentPage() != Pager.NO_PAGING) {
//				pager.setCurrentPage(1);
//			}
//			ignoreRowGroups = true;
//		}
//	}
//	
//	private void calculateTotals(TableOptions tableOptions, HyphenSortedVector all) {
//		Object column;
//		for(TableColumnGroup group : tableOptions.getColumnGroups()){
//			for(TableColumnOptions columnOption : group.getColumnOptions()){
//				BigDecimal total = null;
//				if(columnOption.isCalculateTotal()){
//					total = BigDecimal.ZERO;
//					for(Object obj : all){
//						try {
//							column = BeanUtilities.fieldToString(obj, columnOption.getFieldName());
//							if(column != null){
//								total = total.add(new BigDecimal(column.toString()));
//							}
//						} catch (Exception e) {
//							column = null;
//						}
//					}
//					//Always pass strings to frontend
//					columnOption.setTotal(total.toString());
//				}
//			}
//		}
//		
//	}

	public int newPageSize() {
		return newPageSize;
	}
	

}
