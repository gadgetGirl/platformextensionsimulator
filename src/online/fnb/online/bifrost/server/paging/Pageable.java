package fnb.online.bifrost.server.paging;
/**
 * @author lmouton
 *
 * Objects that implement this interface will be abled to be paged by the Pager. 
 */
public interface Pageable {}
