package fnb.online.bifrost.server.paging;
import java.sql.Connection;
import java.sql.SQLException;

import fnb.online.tags.beans.AbstractViewBean;
import fnb.online.tags.beans.table.BeanUtilities;

/**
 * @author lmouton
 *
 * Pages a given object. If an object implements the Pageable interface this class will
 * be able to page it. The collectionToPage collection may be different from the collection
 * in the objectToPage. 
 * This object can also sort the collectionToPage collection if the comparator has been set
 */
public class Pager {
	private static final int ELEC_SUBS_PAGE_SIZE = 20;
	// Changed to be able to hold any vector of objects - in the paging, it checks if the objects implement Pageable
	private Object objectToPage = null;
	// Although this is a sorted vetcor, its only difference from a standard Vector is that it has methods to sort
	// It adds no data to the std Vector and hence can hold things that are not sortable  
	private SortedVector collectionToPage = null;
	private SortedVector unfilteredList = null;
	// Used to indicate when the data set should be paged
	public final static int DEFAULT_MAX_LIMITER_ITEM = 150;
	public final static int NO_PAGING = 0;
	public final static int DONT_CHANGE_PAGE = -1;
	public final static int DEFAULT_PAGE_SIZE = 150;
	
	public final static int TYPE_DEFAULT = 0;
	public final static int TYPE_USER_PREF = 1;
	public final static String NO_SEARCH_VALUE_ENTERED = "noSearchValueEntered"; 

	private int pageSize = 0;
	private int collectionSize = 0;
	private int currentPageIndex = 0;
	private int numberOfPages = 0;
	private boolean hasNext = true;
	private boolean hasPrevious = false;
	private boolean atStart = false;
	private boolean beforeStart = true;
	private GenericComparator comparator = null;
	private boolean needToSort = false;
	private String searchTableByValue;
	private int maxNumberofLimiterItems = 0;
	private boolean changedPageSize;
	
	private String sortMethod = "";
	private boolean reverseOrder = false;	// Whether to reverse the order of the list
	
	/**
	 * Constructor for Pager.
	 * @param parm
	 */
	public Pager() {
		clear(0, 0);
	}
	
	/**
	 * Contractor that also sets the collection to be held as well as just initialising the para object. The objectToPage
	 * does not need to be set independently as this is determined from the collection object
	 * @param parm		A HyphenParameter object that is used to determine the curent user by the composed Pager
	 * 					objects created - they need this to determine the page size to break the collection into
	 * @param collection	The collection to be stored
	 */
	public Pager(SortedVector collection, int type, int itemsPerPage) {
		clear(type, itemsPerPage);
		if (collection != null) {
			setCollectionToPage(collection);
		}
		
	}
	
	/*
	 * Setting all the values to have a clean objec.
	 */
	public void clear(int type, int itemsPerPage) {
		if (type != 0) {
			if (type == TYPE_DEFAULT) {
				pageSize = DEFAULT_PAGE_SIZE;
			} 
		} else if (itemsPerPage != 0) {
			pageSize = itemsPerPage;
		} else {
			pageSize = DEFAULT_PAGE_SIZE;
		}
		
		if (pageSize == 0)
			pageSize = DEFAULT_PAGE_SIZE;
		
		maxNumberofLimiterItems = 150;
		objectToPage = null;
		collectionToPage = null;
		collectionSize = 0;
		currentPageIndex = 1;
		hasNext = true;
		hasPrevious = false;
		atStart = false;
		beforeStart = true;
		needToSort = false; 	// Always assume not sortable until a Comparator is set
	}
	/**
	 * Returns the collectionToPage.
	 * @return SortedVector
	 */
	public SortedVector getCollectionToPage() {
		return collectionToPage;
	}

	/**
	 * Returns the objectToPage.
	 * @return Pageable
	 */
	public Object getObjectToPage() {
		return objectToPage;
	}

	/**
	 * Sets the collectionToPage. Also determines the total collection size and the number of pages that this comprises
	 * If objectToPage is null and the new collection is not, objectToPage is set as the first object in the collection
	 * This prevents the need for a second call to set the object - this only need be done if the first object is not the right on
	 * @param collectionToPage The collectionToPage to set
	 */
	public void setCollectionToPage(SortedVector collectionToPage) {
		this.collectionToPage = collectionToPage;
		collectionSize = collectionToPage.size();
		// Determine number of pages
		numberOfPages = collectionSize / pageSize;
		// If the collection does not fit exactly on the last page, there will be extra items on another page
		if (collectionSize % pageSize != 0)
			numberOfPages++;
		// If there are no items in the collection, there is still '1 page' which tells the user this
		if (collectionSize == 0)
			numberOfPages = 1;
		// Sets objectToPage as discussed above
		if (objectToPage == null && collectionToPage.size() > 0)
			setObjectToPage(collectionToPage.get(0));
		// If there is no objectToPage or the objects are not pageable, then there will always be only one page
		if (objectToPage == null || !(objectToPage instanceof Pageable)){
			numberOfPages = 1;
		} 
		
		if(objectToPage instanceof AbstractViewBean){
			if(((AbstractViewBean)objectToPage).getCounter() == -1){
				setCounters(collectionToPage);
			}
		}
		
			
	}

	private void setCounters(SortedVector collectionToPage2) {
		for(int counter = 0; counter < collectionToPage.size(); counter++ ){
			((AbstractViewBean) collectionToPage.get(counter)).setCounter(counter);
		}
	}

	/**
	 * Sets the objectToPage. This is only necessary if the 1st object in the collection is not the same as the rest
	 * Otherwise setting the collection will also set the objectTopage
	 * @param objectToPage The objectToPage to set
	 */
	public void setObjectToPage(Object objectToPage) {
		this.objectToPage = objectToPage;
	}
	/**
	 * Pages forward through a collection of objects. This merely increments the current page refernce and calls current()
	 */
	public SortedVector next() {

		setCurrentPage(currentPageIndex + 1);
		return current();
	}
	
	/**
	 * Pages backwords through a collection of objects. This merely decrements the current page index and calls current()
	 */
	public SortedVector previous() {

		setCurrentPage(currentPageIndex -1);
		return current();
	}

		/**
	 * Return the currentsubSet.
	 */
	public SortedVector current() {
		// If there is only one page or the collection is not pageable objects, return the whole vector unchanged
		// otherwise only return the subset
		if (numberOfPages == 1 || !(objectToPage instanceof Pageable)) {
			return (SortedVector) collectionToPage;
		// If there are multiple pages, just check that collection is valid and then page
		} else if (collectionToPage != null && collectionToPage.size() > 0) {
			
			SortedVector subList = null;
			// Start at item 
			int startIndex = (currentPageIndex-1) * pageSize;
			// End item - chekc whether this is the end of a page or if we are on the last page, don't go past end of data
			int endIndex = (currentPageIndex == numberOfPages) ? collectionToPage.size() : currentPageIndex * pageSize;
		
			// Create the sublist to be returned
			subList = new SortedVector(collectionToPage.subList(startIndex, endIndex));
		
			return subList;
		} else {
			return null;
		}
	}
	
	/** 
	 * Returns the full lsit - this is called when the calling app works out that for some specific reason paging should not be
	 * involked.
	 */ 
	public SortedVector getAll() {
		return collectionToPage;
	}
	/**
	 * Returns the hasNext.
	 * @return boolean
	 */
	public boolean hasNext() {
		return hasNext;
	}

	/**
	 * Returns the hasPrevious.
	 * @return boolean
	 */
	public boolean hasPrevious() {
		return hasPrevious;
	}

	/**
	 * @see mammoth.performance.cache.TransactionCachable#refresh(Connection)
	 */
	public void refresh(Connection conn) throws SQLException {}

	/**
	 * Returns the atStart.
	 * @return boolean
	 */
	public boolean isAtStart() {
		return atStart;
	}
	/*
	 * Moves the index so that the last subset of items are displayed. Merely sets current page indicator to the last page and calls current()
	 */
	public SortedVector pageToEnd() {

		currentPageIndex = numberOfPages;
		return current();
	} 

		/*
	 * Moves the index so that the first subset of items are displayed. Merely sets current page index to first page and calls current()
	 */
	public SortedVector pageToStart() {

		currentPageIndex = 1;
		return current();		
	} 

	/**
	 * Returns the beforeStart.
	 * @return boolean
	 */
	public boolean isBeforeStart() {
		return beforeStart;
	}

	/**
	 * Returns the collectionSize.
	 * @return int
	 */
	public int getCollectionSize() {
		return collectionSize;
	}

	/**
	 * Returns the pageSize.
	 * @return int
	 */
	public int getPageSize() {
		return pageSize;
	}
	
	/**
	 * Returns the current page index within the collection
	 */
	public int getCurrentPage() {
		return currentPageIndex;
	}
	
	/**
	 * Returns the total number of pages available
	 */
	public int getNumberOfPages() {
		return numberOfPages;
	}

	/**
	 * Sets the current page. This ethod also does range checking to ensure that the requested page exists. If it does not, this is handled
	 * as folows: If the page is set to zero or a negative number, it is actually set to page one; if a page beyond the end is requested
	 * then the last page is returned. No indication is given to the caller that these adjustments have been made. These adjustments
	 * however ensure that the page returned by current() is a valid page. The currentPageIndex field should not be modified outisde
	 * this method as all other methods assume that it is in range
	 * @param newCurrentPage	The page to go to
	 */
	public void setCurrentPage(int newCurrentPage) {
		// Set the new page number, after doing range checking as explained above
		if (newCurrentPage <= 0)
			currentPageIndex = 1;
		else if (newCurrentPage > numberOfPages)
			currentPageIndex = numberOfPages;
		else
			currentPageIndex = newCurrentPage;
			
		// Set flags for start and end
		hasNext = currentPageIndex < numberOfPages;
		hasPrevious = currentPageIndex > 1;
		atStart = currentPageIndex == 1;
		beforeStart = false;
	}
	
	/** 
	 * Sets the compparator object in this pager. This method checks that the method used by the Comparator is valid for the object type
	 * stored in the collection. If a comparator exists, the method is merely changed, else a new comparator is created. This allows only
	 * sorting in ascending order as it is assumed that the user will consistently get data back in ascending order the first time a 
	 * sort is requested. The pager method reverseOrder() should be called to reverse the order, ie. don't resort, just reverse
	 * @param method	The name oif the method to be used to get the field on which the sort is to be performed
	 * @returns			Whetehr the comparator was validly set or not
	 */
	public boolean setComparator(String method) {
		try {
		
			// Reset the need to sort - not necessary unless comparator changes
			needToSort = false;
			// Check if this is a method of the class and that it takes no parameters, ie. that is can be used by the Comparator
			// If it is not valid, an exception is thrown which is caught below and returns false
			try{
				
				BeanUtilities.findMethod(objectToPage.getClass(), method);
				// Check how the Vector is currently sorted
				// If it has no Comparator, then a sort is now required and a new Comparator is created to do this
				if (comparator == null) {
					comparator = new GenericComparator(method, true);
					needToSort = true;
				// If there is a comparator, just change the method it uses
				} else {
					comparator.setAscending(true);
					comparator.setMethodName(method);
					needToSort = true;
				}
				//reset field as we are sorting by method
				comparator.setFieldName(null);
			}catch (NoSuchMethodException e) {
				try{
					BeanUtilities.findField(objectToPage.getClass(), method);
					// Check how the Vector is currently sorted
					// If it has no Comparator, then a sort is now required and a new Comparator is created to do this
					if (comparator == null) {
						comparator = new GenericComparator();
						comparator.setFieldName(method);
						needToSort = true;
					// If there is a comparator, just change the method it uses
					} else {
						comparator.setAscending(true);
						comparator.setFieldName(method);
						needToSort = true;
					}
					//reset method as we are sorting by field
					comparator.setMethodName(null);
				} catch (NoSuchFieldException nsfe) {
					BeanUtilities.findMethod(objectToPage.getClass(), "getObject", String.class);
					// Check how the Vector is currently sorted
					// If it has no Comparator, then a sort is now required and a new Comparator is created to do this
					if (comparator == null) {
						comparator = new GenericComparator();
						comparator.setInvokeGet(true);
						comparator.setFieldName(method);
						needToSort = true;
					// If there is a comparator, just change the method it uses
					} else {
						comparator.setAscending(true);
						comparator.setInvokeGet(true);
						comparator.setFieldName(method);
						needToSort = true;
					}
					//reset method as we are sorting by field retrieved from the get method
					comparator.setMethodName(null);
				}
			}
			
			return true;
			
		} catch (Exception nsme) {
			// An exception could be thrown if either objectToPage is null
			return false;
		}
	}
				
	/**
	 * Used to change the order of the items stored in the Pager. This is used rather than a new sort
	 */
	public void reverseOrder() {
		if (collectionToPage != null) {
			int collectionSize = collectionToPage.size();
			for (int i=0; i < collectionSize/2; i++) {
				Object temp = collectionToPage.get(i);
				collectionToPage.set(i, collectionToPage.get(collectionSize - i - 1));
				collectionToPage.set(collectionSize - i - 1, temp);
			}
		} 
	}
	
	/** 
	 * Used to actually sort the collection. This uses the sort provided in SortedVector (based on QuickSort). It first checks
	 * whether a new sort is required and that both the comparator and collection are valid
	 */
	public void sort() {
		try{
			if (needToSort && comparator != null && collectionToPage != null) {
				collectionToPage.setComparator(comparator);
				collectionToPage.sort();
			}
			// It has now been sorted, so don't try again until needToSort is reset by the setComparator method
			needToSort = false;
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
			
	/**
	 * @return
	 */
	public SortedVector getUnfilteredList()
	{
		return unfilteredList;
	}

	/**
	 * @param vector
	 */
	public void setUnfilteredList(SortedVector vector)
	{
		unfilteredList = vector;
	}

	/**
	 * @param i
	 */
	public void setPageSize(int i)
	{
		pageSize = i;
		
		numberOfPages = collectionSize / pageSize;
		// If the collection does not fit exactly on the last page, there will be extra items on another page
		if (collectionSize % pageSize != 0) {
			numberOfPages++;
		}
			
		// If there are no items in the collection, there is still '1 page' which tells the user this
		if (collectionSize == 0) {
			numberOfPages = 1;
		}
		
		// Sets objectToPage as discussed above
		if (objectToPage == null && collectionToPage != null && collectionToPage.size() > 0) {
			setObjectToPage(collectionToPage.get(0));
		}
		
		// If there is no objectToPage or the objects are not pageable, then there will always be only one page
		if (objectToPage == null || !(objectToPage instanceof Pageable)) {
			numberOfPages = 1;
		}
		
		
	}

	public String getSearchTableByValue() {
		return searchTableByValue;
	}

	public void setSearchTableByValue(String searchTableByValue) {
		this.searchTableByValue = searchTableByValue;
	}

	public int getMaxNumberofLimiterItems() {
		return maxNumberofLimiterItems;
	}

	public void setMaxNumberofLimiterItems(int maxNumberofLimiterItems) {
		this.maxNumberofLimiterItems = maxNumberofLimiterItems;
	}

	public boolean isChangedPageSize() {
		return changedPageSize;
	}

	public void setChangedPageSize(boolean changedPageSize) {
		this.changedPageSize = changedPageSize;
	}

	public String getSortMethod() {
		return sortMethod;
	}

	public void setSortMethod(String sortMethod) {
		this.sortMethod = sortMethod;
	}

	public boolean isReverseOrder() {
		return reverseOrder;
	}

	public void setReverseOrder(boolean reverseOrder) {
		this.reverseOrder = reverseOrder;
	}

	public boolean isNeedToSort() {
		return needToSort;
	}

	public void setNeedToSort(boolean needToSort) {
		this.needToSort = needToSort;
	}


}
