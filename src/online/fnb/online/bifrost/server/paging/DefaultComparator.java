package fnb.online.bifrost.server.paging;

import java.util.Comparator;

/**
 * @author lmouton
 *
 * This is the default comparator. This will ensure that all code not
 * using this, ie existing code(before this change), will still work.
 */
public class DefaultComparator<Element> implements Comparator<Element> {
	/**
	 * Constructor for DefaultComparator.
	 */
	public DefaultComparator() {
		super();
	}

	/**
	 * @see java.util.Comparator#compare(Object, Object)
	 */
	public int compare(Element arg0, Element arg1) {
		return ((Comparable) arg0).compareTo((Comparable)arg1);
	}
}
