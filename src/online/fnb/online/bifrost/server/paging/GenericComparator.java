package fnb.online.bifrost.server.paging;
import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import fnb.online.bifrost.server.bean.ViewBean;
import fnb.online.tags.beans.table.BeanUtilities;

/*
 * This comparator is used to sort any value. The comparator is used by setting the method name
 * used to get the value to sort on.
 * NB. The object returned by this method must implement Comparable.
 *
 * For example.
 * - If a collection of AccountViewBeans must be sorted by account number. The get-method
 * for getting the value of the viewBean's account number, ie getAccountNumber
 * NB. The method can't take any parameters.
 */
public class GenericComparator<ElementType> implements Comparator<ElementType> {
	private String methodName = null;
	private String fieldName = null;
	private Class classObjectToSort = null;
	private boolean ascending = true;
	private boolean invokeGet = false;

	public GenericComparator() {
		super();
	}

	public GenericComparator(String method, boolean b) {
		this.setMethodName(method);
		this.setAscending(b);
	}

	/*
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 *
	 * The object returned by the get
	 */
	@Override
	public int compare(ElementType o1, ElementType o2) {
		int returnValue = 0;
		// Get the class type of the objects being compared

		try {

			// Check that neither object is null - if it is, it is sorted before non-null objects
			if (o1 == null && o2 == null) {
				returnValue = 0;
			}
			else if (o1 == null) {
				returnValue = -1;
			}
			else if (o2 == null) {
				returnValue = 1;
			}
			else{
				Comparable c1 = this.retrieveValue(o1);
				Comparable c2 = this.retrieveValue(o2);
				// Check that the object returned by the method is not null. If it is, sort it before
				// non-null retrning objects
				if (c1 == null && c2 == null) {
					returnValue = 0;
				}
				else if (c1 == null) {
					returnValue = -1;
				}
				else if (c2 == null) {
					returnValue = 1;
				}
				else if (c2 instanceof String) {
					// If we are comparing strings, use ignore case
					returnValue = ((String)c1).compareToIgnoreCase((String)c2);
				}
				else {
					// If there are no nulls, do a proper compare
					returnValue = c1.compareTo(c2);
				}
			}

			// If descending order is required, do a normal asceding compare above and then
			// reverse the answer here
			if (!this.ascending) {
				returnValue *= -1;
			}

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
			returnValue = 0;
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}

		return returnValue;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Comparable retrieveValue(ElementType o) throws IllegalAccessException, InvocationTargetException,NoSuchMethodException, IllegalArgumentException, NoSuchFieldException {
		this.classObjectToSort = o.getClass();
 		if(this.invokeGet)
			return (Comparable)this.classObjectToSort.getMethod("getObject", String.class).invoke(o, this.fieldName);
		else if(this.methodName != null){
			try{
				return (Comparable)this.classObjectToSort.getMethod(this.methodName, null).invoke(o, null);
			}catch (Exception e) {
				if(o instanceof ViewBean)
					return (Comparable)this.classObjectToSort.getMethod(((ViewBean)o).getDefaultSortingMethod(), null).invoke(o, null);
				else
					throw e;
			}
		}
		else
			return (Comparable) BeanUtilities.fieldToObject(o, this.fieldName);
	}

	/**
	 * @param string
	 */
	public void setMethodName(String string) {
		this.methodName = string;
	}

	/**
	 * @param b
	 */
	public void setAscending(boolean b) {
		this.ascending = b;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public boolean isAscending() {
		return this.ascending;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setInvokeGet(boolean invokeGet) {
		this.invokeGet = invokeGet;
	}

}
