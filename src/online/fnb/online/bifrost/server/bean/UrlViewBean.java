package fnb.online.bifrost.server.bean;

import java.sql.Date;

public class UrlViewBean extends ViewBean {

	private int urlRfn = 0;
	private String url = "";
	private Date createDate = new Date(System.currentTimeMillis());
	private boolean newEntry = false;
	

	public UrlViewBean() {
		super();
	}
	protected UrlViewBean(UrlViewBean clone) {
	    super();
	    this.urlRfn = clone.urlRfn;
	    this.url = clone.url;
	    this.createDate = clone.createDate;
	    this.newEntry = clone.newEntry;
	}

	public UrlViewBean clone() {
		UrlViewBean clone = new UrlViewBean(this);
		return clone;
	}

	public int getUrlRfn() {
		return urlRfn;
	}

	public void setUrlRfn(int urlRfn) {
		this.urlRfn = urlRfn;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isNewEntry() {
		return newEntry;
	}

	public void setNewEntry(boolean newEntry) {
		this.newEntry = newEntry;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
