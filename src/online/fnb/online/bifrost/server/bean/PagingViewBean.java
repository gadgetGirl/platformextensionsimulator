package fnb.online.bifrost.server.bean;

import java.util.Collection;

import fnb.online.bifrost.server.paging.Pager;
import fnb.online.tags.beans.table.PagingBean;


/**
 * 
 * @author: Frans Stofberg
 */
public class PagingViewBean extends ViewBean implements PagingBean
{
	private static String URL = "/banking/Controller?nav=navigator.PagingAndSorting"; 
	private static int PAGENUMBERLIMIT = 10;
	private String resultContainerKey;
	private String viewBeanKey;
	private int numberOfPages;
	private int currentPageNumber;
	private int numberOfItems;
	private int pageSize;
	private String searchTableByValue = "";
	private int maxNumberofLimiterItems;
	private int changePageSize = 0;
	private String sortingMethodName;
	private int goToPage = Pager.DONT_CHANGE_PAGE;
	private boolean pagingOff = false;
	
	Collection<?> items;
	

	@Override
	public Collection getItems() {
		return items;
	}
	@Override
	public void setItems(Collection collection) {
		items = collection;
	}
	/**
	 * PagingViewBean constructor comment.
	 */
	public PagingViewBean()
	{
		super();
	}
	/**
	 * @return
	 */
	public int getCurrentPageNumber()
	{
		return currentPageNumber;
	}

	/**
	 * @return
	 */
	public int getNumberOfPages()
	{
		return pagingOff ? 1 : numberOfPages;
	}

	/**
	 * @param currentPageNumber
	 */
	public void setCurrentPageNumber(int currentPageNumber)
	{
		this.currentPageNumber = currentPageNumber;
	}

	/**
	 * @param numberOfPages
	 */
	public void setNumberOfPages(int numberOfPages)
	{
		this.numberOfPages = numberOfPages;
	}

	/**
	 * @return
	 */
	public String getResultContainerKey()
	{
		return resultContainerKey;
	}

	/**
	 * @return
	 */
	public String getViewBeanKey()
	{
		return viewBeanKey;
	}

	/**
	 * @param string
	 */
	public void setResultContainerKey(String string)
	{
		resultContainerKey = string;
	}

	/**
	 * @param string
	 */
	public void setViewBeanKey(String string)
	{
		viewBeanKey = string;
	}

	/**
	 * @return
	 */


	/**
	 * @return
	 */
	public int getPageSize()
	{
		return pageSize;
	}

	/**
	 * @return
	 */
	public int getNumberOfItems()
	{
		return pagingOff ? 0 : numberOfItems;
	}

	/**
	 * @param i
	 */
	public void setPageSize(int i)
	{
		pageSize = i;
	}

	/**
	 * @param i
	 */
	public void setNumberOfItems(int i)
	{
		numberOfItems = i;
	}
	
	public String getSearchTableByValue() {
		return searchTableByValue;
	}
	public void setSearchTableByValue(String searchTableByValue) {
		this.searchTableByValue = searchTableByValue;
	}
	public int getMaxNumberofLimiterItems() {
		return maxNumberofLimiterItems;
	}
	public void setMaxNumberofLimiterItems(int maxNumberofLimiterItems) {
		this.maxNumberofLimiterItems = maxNumberofLimiterItems;
	}
	public boolean isChangedPageSize() {
		return changePageSize > 0;
	}
	public int getChangePageSize() {
		return changePageSize;
	}
	public void setChangePageSize(int changedPageSize) {
		this.changePageSize = changedPageSize;
	}

	public String getUrl() {
		return URL+ "&resultContainerKey=" + getResultContainerKey() 
				+ "&viewBeanKey=" + getViewBeanKey(); 
	}
	
	public int getFirstPageNumber()
	{
		return 1;
	}
	
	public int getNextPageNumber()
	{
		return getCurrentPageNumber() +1;
	}
	
	public int getPreviousPageNumber()
	{
		return getCurrentPageNumber() -1;
	}
	
	public int getLastPageNumber()
	{
		return getNumberOfPages();
	}
	
	public int getStartPageNumber()
	{	
		if(getCurrentPageNumber() % PAGENUMBERLIMIT == 0 || getCurrentPageNumber() ==1){
			return getCurrentPageNumber();
		}else if(getNumberOfPages() > PAGENUMBERLIMIT && ((getCurrentPageNumber() / PAGENUMBERLIMIT) * PAGENUMBERLIMIT > 0)){
			return (getCurrentPageNumber() / PAGENUMBERLIMIT) * PAGENUMBERLIMIT ;
		}else{
			return getFirstPageNumber();
		}
	}
	
	public int getEndPageNumber()
	{	
		if(getStartPageNumber() + PAGENUMBERLIMIT > getLastPageNumber()){
			return getLastPageNumber();
		}else {
			return getStartPageNumber() + PAGENUMBERLIMIT -1;
		}
	}
	public String getSortingMethodName() {
		return sortingMethodName;
	}
	public void setSortingMethodName(String sortingMethodName) {
		this.sortingMethodName = sortingMethodName;
	}
	public int getGoToPage() {
		return goToPage;
	}
	public void setGoToPage(int goToPage) {
		this.goToPage = goToPage;
	}
	public boolean isPagingOff() {
		return pagingOff;
	}
	public void setPagingOff(boolean pagingOff) {
		this.pagingOff = pagingOff;
	}
	
	
		
}
