package fnb.online.bifrost.server.bean;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fnb.online.bifrost.server.dto.ColumnHeader;
import fnb.online.bifrost.server.dto.TableColumnHeader;
import fnb.online.bifrost.server.paging.Pageable;
import fnb.online.bifrost.server.service.AbstractTableRowResolver;
import fnb.online.bifrost.server.service.TableRowResolver;
import fnb.online.tags.beans.AbstractViewBean;
import fnb.online.tags.beans.date.DatePicker;
import fnb.online.tags.beans.dropdown.DropDown;
import fnb.online.tags.beans.table.TableBean;
import fnb.online.tags.beans.table.TableColumnGroup;
import fnb.online.tags.beans.table.TableColumnOptions;
import fnb.online.tags.beans.table.TableOptions;
import fnb.online.tags.beans.table.TableRowGroup;
import fnb.online.tags.beans.tablecontrolsswitcher.Switcher;
import fnb.online.tags.beans.tablecontrolsswitcher.TableHeaderButtonGroup;

public class TableViewBean extends ViewBean implements TableBean, Pageable {

	private int parentFunction = -1;
	private TableOptions tableOptions = new TableOptions();
	private boolean overwriteTableOptions = true;
	private String tableId = "";
	private List<TableRowGroup> tableRowGroups = new LinkedList<TableRowGroup>();
	private DropDown headerDropdown = new DropDown();
	private boolean ignoreRowGroups = false;
	private Switcher switcher = new Switcher();
	private DatePicker datePicker = new DatePicker();
	private String targetUrl = "";
	private PagingViewBean pagingViewBean;
	private String enableJSobject = "";
	private String tableHeading = "";
	private String onRowClick	= "";
	private String phoneContent	= "";
	/* This specifically for lists of links on an eazi panel to make it work with paging. */
	private boolean eziItemClickLoad = false;
	
	private Map<String,TableHeaderButtonGroup> tableHeaderButtons = new LinkedHashMap<String, TableHeaderButtonGroup>(); 
	
	private int switcherSelectedFunction=-1;
	
	private AbstractTableRowResolver tableRowResolver = new TableRowResolver();
	private List<String> filterFieldNames = new ArrayList<String>();


	/**
	 * Only call this from Navigator Base Class, storeTableViewBean(String name,
	 * TableViewBean tableViewBean ) If you need to call this from your own
	 * implementation ensure that you comment it appropriately
	 */
	@Override
	public List<TableRowGroup> getRowGroups() {
		return tableRowGroups;
	}
	@Override
	public void setRowGroups(List<TableRowGroup> tableRowGroups) {
		this.tableRowGroups = tableRowGroups;
	}

	@Override
	public void addRowGroup(TableRowGroup tableRowGroup) {
		if (tableRowGroups == null) {
			tableRowGroups = new LinkedList<TableRowGroup>();
		}
		tableRowGroups.add(tableRowGroup);
	}

	/**
	 * Only to be used by the jsp page ... as this gets set from the jsp page
	 */
	public TableOptions getTableOptions() {
		return tableOptions;
	}

	/**
	 * Only to be used by the jsp page ... as this gets set from the jsp page
	 */
	public void setTableOptions(TableOptions tableOptions) {
		this.tableOptions = tableOptions;
	}
	
	public void buildTableOptions(TableColumnHeader header) {
		setTableOptions(new TableOptions());
		TableColumnGroup tableColumnGroup = new TableColumnGroup("group1");
		tableOptions.addTableColumnGroup(tableColumnGroup);
		for(ColumnHeader columnHeader : header.getColumnHeading()){
			TableColumnOptions tableColumnOptions = new TableColumnOptions();
			tableColumnOptions.setHeading(columnHeader.getLabel());
			tableColumnOptions.setFieldName(columnHeader.getRowItemName());
			tableColumnOptions.setAlign((columnHeader.isAlignRight())?"right":"left");
			
			tableOptions.getColumnOptions("group1").add(tableColumnOptions);
		}
		
	}

	@Override
	public String getTableId() {
		return tableId;
	}

	@Override
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public int getParentFunction() {
		return parentFunction;
	}

	public void setParentFunction(int parentFunction) {
		this.parentFunction = parentFunction;
	}

	@Override
	public DropDown getHeaderDropdown() {
		return headerDropdown;
	}

	@Override
	public void setHeaderDropdown(DropDown dropdown) {
		headerDropdown = dropdown;

	}

	public boolean isIgnoreRowGroups() {
		return ignoreRowGroups;
	}

	public void setIgnoreRowGroups(boolean ignoreRowGroups) {
		this.ignoreRowGroups = ignoreRowGroups;
	}

	public Switcher getSwitcher() {
		return switcher;
	}

	public void setSwitcher(Switcher switcher) {
		this.switcher = switcher;
	}

	public DatePicker getDatePicker() {
		return datePicker;
	}

	public void setDatePicker(DatePicker datePicker) {
		this.datePicker = datePicker;
	}

	public String getTargetUrl() {
		return targetUrl;
	}

	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}

	public PagingViewBean getPagingViewBean() {
		return pagingViewBean;
	}

	public void setPagingViewBean(PagingViewBean pagingViewBean) {
		this.pagingViewBean = pagingViewBean;
	}

	public List<String> getFilterFieldNames() {
		return filterFieldNames;
	}

	public String getEnableJSobject() {
		return enableJSobject;
	}

	public void setEnableJSobject(String enableJSobject) {
		this.enableJSobject = enableJSobject;
	}

	public String getTableHeading() {
		return tableHeading;
	}

	public void setTableHeading(String tableHeading) {
		this.tableHeading = tableHeading;
	}

	public String getOnRowClick() {
		return onRowClick;
	}

	public void setOnRowClick(String onRowClick) {
		this.onRowClick = onRowClick;
	}

	public String getPhoneContent() {
		return phoneContent;
	}

	public void setPhoneContent(String phoneContent) {
		this.phoneContent = phoneContent;
	}
	
	public void overrideColumnHeaderURL(Map<String, TableRowGroup> groupMap) {
		 //override this method if you want to change the column heading not to sort but perform a different function
	}

	public boolean getOverwriteTableOptions() {
		return overwriteTableOptions;
	}

	public void setOverwriteTableOptions(boolean overwriteTableOptions) {
		this.overwriteTableOptions = overwriteTableOptions;
	}

    /**
     * @return the switcherSelectedFunction
     */
    public int getSwitcherSelectedFunction() {
        return switcherSelectedFunction;
    }

    /**
     * @param switcherSelectedFunction the switcherSelectedFunction to set
     */
    public void setSwitcherSelectedFunction(int switcherSelectedFunction) {
        this.switcherSelectedFunction = switcherSelectedFunction;
    }

    /**
     * @return the tableHeaderButtons
     */
    public Map<String, TableHeaderButtonGroup> getTableHeaderButtons() {
        return tableHeaderButtons;
    }

    /**
     * @param tableHeaderButtons the tableHeaderButtons to set
     */
    public void setTableHeaderButtons(Map<String, TableHeaderButtonGroup> tableHeaderButtons) {
        this.tableHeaderButtons = tableHeaderButtons;
    }
    
    public boolean isEziItemClickLoad() {
		return eziItemClickLoad;
	}

	public void setEziItemClickLoad(boolean eziItemClickLoad) {
		this.eziItemClickLoad = eziItemClickLoad;
	}

	public final void addNewItem() {
    	if(!getItems().isEmpty() && getItems().toArray()[0] != null){
			Class viewBeanClass = getItems().toArray()[0].getClass();
	    	Object newItem;
			try {
				newItem = viewBeanClass.newInstance();
				((AbstractViewBean)newItem).setCounter(getItems().size());
		    	getItems().add(newItem);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
    }

	public AbstractTableRowResolver getTableRowResolver() {
		return tableRowResolver;
	}

	public void setTableRowResolver(AbstractTableRowResolver tableRowResolver) {
		this.tableRowResolver = tableRowResolver;
	}

	
}
