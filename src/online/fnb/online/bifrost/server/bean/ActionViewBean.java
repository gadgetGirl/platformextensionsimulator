package fnb.online.bifrost.server.bean;


public class ActionViewBean extends UrlViewBean implements Comparable<ActionViewBean> {

	private int rfn = 0;
	private int childFunctionRef = 0;
	private int parentFunctionRef = 0;
	private String heading = "";
	private String label = "";
	private int type = 0;
	private int target = 0;
	private int order = 0;
	private int column = 0;
	private boolean selected = false;
	private String formName = "";
	private String term = "";
	private String topMenuParent = "";
	private String styleClass = "";
	private int version = 1;
	private int status = 1;
	private boolean disabled = false;

	public ActionViewBean() {
		super();
	}
	protected ActionViewBean(ActionViewBean clone) {
	    super(clone);
	    this.rfn = clone.rfn;
	    this.childFunctionRef = clone.childFunctionRef;
	    this.parentFunctionRef = clone.parentFunctionRef;
	    this.heading= clone.heading;
	    this.label = clone.label;
	    this.type = clone.type;
	    this.target = clone.target;
	    this.order = clone.order;
	    this.column = clone.column;
	    this.selected = clone.selected;
	    this.formName = clone.formName;
	    this.term = clone.term;
	    this.topMenuParent = clone.topMenuParent;
	    this.styleClass = clone.styleClass;
	    this.version = clone.version;
	    this.status = clone.status;
	}

	public ActionViewBean clone() {
		ActionViewBean clone = new ActionViewBean(this);
		return clone;
	}

	public int getRfn() {
		return rfn;
	}

	public void setRfn(int rfn) {
		this.rfn = rfn;
	}

	public int getChildFunctionRef() {
		return childFunctionRef;
	}

	public void setChildFunctionRef(int childFunctionRef) {
		this.childFunctionRef = childFunctionRef;
	}

	public int getParentFunctionRef() {
		return parentFunctionRef;
	}

	public void setParentFunctionRef(int parentFunctionRef) {
		this.parentFunctionRef = parentFunctionRef;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getTarget() {
		return target;
	}

	public void setTarget(int target) {
		this.target = target;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append(parentFunctionRef +", ");
		str.append(rfn +", ");
		str.append(childFunctionRef +", ");
		str.append(heading+", ");
		str.append(label+", ");
		str.append(type +", ");
		str.append(target +", ");
		str.append(order +", ");
		str.append(status +", ");
		str.append(version +", ");
		str.append(column +", ");
		str.append(selected+", ");
		str.append(styleClass);
		return str.toString();
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getTerm() {
		return this.term ;
	}
	
	public void setTerm(String term) {
		this.term = term;
	}

	public String getTopMenuParent() {
		return topMenuParent;
	}

	public void setTopMenuParent(String topMenuParent) {
		this.topMenuParent = topMenuParent;
	}
	public String getStyleClass() {
		return styleClass;
	}
	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
    /**
     * @return the disabled
     */
    public boolean isDisabled() {
        return disabled;
    }
    /**
     * @param disabled the disabled to set
     */
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
	@Override
	/**
	 * Note: If you're sorting ActionViewBeans,
	 * make sure that you have set the faorder in the db,
	 * or have set the order on this object.
	 */
	public int compareTo(ActionViewBean o) {
		return o.getOrder() - getOrder();
	}
	
	

}
