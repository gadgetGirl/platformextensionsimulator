package fnb.online.bifrost.server.bean;

import java.util.HashMap;

/** 
 * This method holds error information
 * necessary for the ResultsScreen
 * to use about the completion of a task.
 *
 * Creation date: (6/27/00 1:24:16 PM)
 * @author: Luciano Chavez
 */
public class HyphenError  {
	public int errorCode; // supplied error code
	public int screenId; // screen id of screen originating error
	public String[] subStrArray = null; // array of substitution strings for error message
	private int codeType = 1; // error code type/class
	private HashMap<String, String> errorMessages;
	private String errorMessage;
	private String detailMessage;
	private int severity;
	private boolean pooled;

	/*
	 * NELEBAS error codes
	 */
	
	// ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
//	 ADD NEW CODES NEAR THE END. LOOK FOR THE MESSAGE. DON'T DO IT ELSEWHERE
	 
	public static final String UNKNOWN_ERROR = "An unknown error occurred";
	
	public static final int ERR_SYSTEM_TECHNICAL_ERROR = -1; // System encountered a technical Error
	public final static int NULL_IN_RESULT_CONTAINER = -13;	// Double clicking
	public final static int SUCCESS = 0; // No error encountered
	public final static int ERR_SQL_EXCEPTION = 1; // An SQL exception occurred
	public final static int ERR_NO_CONNECTION = 2; // No database connections available
	public final static int ERR_PASSWORD_CHANGE = 3; // An error occurred changing user's password
	public final static int ERR_INVALID_PARAMETER = 4; // Invalid parameter
	public final static int ERR_MISSING_PARAMETER = 5; // Expected parameter(s) not supplied
	public final static int ERR_USER_NOT_FOUND = 6; // The requested user was not found
	public final static int ERR_INVALID_SIGNATURE = 7; // The digital signature failed verification
	public final static int ERR_GENERATING_VIEW = 8; // An error occurred generating signed transaction view
	public final static int ERR_INVALID_LOGIN = 10; // Invalid login
	public final static int ERR_USER_DELETED = 20; // User revoked
	public final static int ERR_USER_PASSWORD_RESET = 26; // The user's password has been reset to the value %1.	
	public final static int ERR_USER_PASSWORD_NOT_INITIALIZED = 27; // User default password not changed yet
	public final static int ERR_NOT_ADMIN = 40; // User does not have administrator access
	public final static int ERR_MULTI_TO_SINGLE_ADMIN = 41; // Trying to go from multiple to single admin
	public final static int ERR_CHANGE_ADMIN_STATUS_FOR_SUPER_USER = 42; // Deadministrating a super user
	public final static int ERR_DISABLE_SUPER_USER = 43; // Disabled a super user
	public final static int ERR_CERT_INVALID = 60; // Certificate invalid
	public final static int ERR_INVALID_PASSWORD = 80; // Password do not match
	public final static int ERR_CERT_INFO_EXPIRED = 89; // Digital certificate info needed
	public final static int ERR_CERT_INFO_NEEDED = 90; // Digital certificate info needed
	public final static int ERR_UNABLE_TO_EXTRACT_CERT_DETAILS = 92; // Can't create digital certificate due to authority problems.
	public final static int ERR_UNABLE_TO_STORE_CERT_DETAILS = 93; // Can't create digital certificate due to authority problems.
	public final static int ERR_NO_CERTIFICATE_DETAILS = 94; // No Certificate Details found for user.
	public final static int ERR_USER_HAS_ACTIVE_CERTIFICATE = 95; // The user already have active certificate.
	public final static int ERR_USER_NOT_PERMITTED = 96; // User not permitted to access this process.
	public final static int ERR_CUSTOMER_NOT_PERMITTED = 97; // The selected function is not available for Customer.
	public final static int ERR_PASSWORD_TOO_RECENT = 81; // Password matches a recently used password.
	public final static int ERR_SERVICE_UNAVAILABLE = 100; // Service temporarily unavailable
	public final static int ERR_TRANS_UNAVAILABLE = 120; // Current transactions not available
	public final static int ERR_AUTH_AMOUNT_EXCEEDED = 140; // An item amount exceeds the authoriser limit
	public final static int ERR_INVALID_ACCOUNT = 160; // Invalid third party branch or account number
	public final static int ERR_INVALID_ACCOUNT_NUMBER = 501; // Invalid third party branch or account number
	public final static int ERR_ENTER_ADDRESS_OR_NUMBER = 167; // Please enter address or number
	public final static int ERR_TEMPLATE_NOT_FOUND = 180; // Template not found
	public final static int ERR_TEMPLATE_HEADER_NOT_VALID = 185; // Template header not valid
	public final static int ERR_ACTION_NOT_VALID = 190; // The action the user requested (modify, delete, auth etc) is not allowed here.
	public final static int ERR_AUTH_NOT_PERMITTED = 200; // Authorisation not permitted
	public final static int ERR_ACB_CUTOFF_MISSED = 201; // Authorisation not because ACB Cut-Off was missed.
	public final static int ERR_FNB_CUTOFF_MISSED = 202; // Authorisation not because FNB Cut-Off was missed.	
	public final static int WARN_AFTER_ACB_CUTOFF_PAYMENTS = 203; // Authorisation not because ACB Cut-Off was missed.
	public final static int WARN_AFTER_FNB_CUTOFF_PAYMENTS = 204; // Authorisation not because FNB Cut-Off was missed.	
	public final static int ERR_PERMISSION_NOT_GRANTED = 205; // Permission not granted
	public final static int WARN_AFTER_ACB_CUTOFF_COLLECTIONS = 206; // Authorisation not because ACB Cut-Off was missed.
	public final static int WARN_AFTER_FNB_CUTOFF_COLLECTIONS = 207; // Authorisation not because FNB Cut-Off was missed.
	public final static int ERR_AUTH_NEEDED = 210; // Authorisation Needed
	public final static int ERR_AUTH_NEEDED_FOR_LINK_ACCEPT = 110031; // Authorisation Needed
	public final static int ERR_TEMPLATE_IN_USE = 220; // Deletion not permitted - template in use
	public final static int ERR_BATCH_IN_USE = 240; // Deletion not permitted - batch in use
	public final static int ERR_BATCH_NOT_FOUND = 250; // Batch Not Found
	public final static int ERR_BATCH_NAME_NOT_SUPPLIED = 251; //Batch Name not supplied
	public final static int ERR_ACCOUNT_NOT_FOUND = 260; // Account Not Found
	public final static int ERR_USERNAME_ALREADY_EXIST = 270; //UserID already exist for current customer
	public final static int ERR_EB4B_WEB_SERVICE_ERROR=275; //Error was returned from the eB4B Web Service system.
	public final static int ERR_CUSTOMER_ALREADY_EXIST = 280; //Customer number already exit
	public final static int ERR_CUSTOMER_NOT_ACTIVE = 285; // Customer not enabled.
	public final static int ERR_INVALID_CHEQUE_ACCOUNT = 290; // Not a valid FNB cheque account
	public final static int ERR_DUPLICATE_RECORD = 300; // System Error - Insert a record that already exists
	public final static int ERR_INVALID_ACCOUNT_STATUS = 310; // Account not eligible for enrolment due to status
	public final static int ERR_INVALID_USERID_OR_PASSWORD = 320; // Account not eligible for enrolment due to status
	public final static int ERR_CAPTURE_AMOUNT_EXCEEDED = 141; // An item amount exceeds the capture limit
	public final static int ERR_DEBIT_CAPTURE_AMOUNT_EXCEEDED = 142; // The debit amount exceeds the capture limit
	public final static int ERR_DEBIT_AUTH_AMOUNT_EXCEEDED = 143; // The debit amount exceeds the authorise limit in consolidated batches
	public final static int ERR_BATCH_EXPIRED = 281; //Batch authorisation date has expired
	public final static int ERR_RESULTPF_UPDATE_FAILED = 330; // An update of IBRESULTPF with the MQ return errors failed
	public final static int ERR_MQ_CONNECTION_EXCEPTION = 340; // MQ exception errorcod
	public final static int ERR_PARTIALY_PROCESSED = 350; // Batch partialy processed
	public final static int ERR_FULLY_PROCESSED = 360; // Batch fully processed
	public final static int ERR_VALIDATION_ERROR = 370; // Account validation errors occurred
	public final static int ERR_PROCESSING_FAILED = 380; // Batch process failed
	public final static int ERR_AUTHORISED_UNABLE_PROCESS = 390; // Batch authorised but unable to perform MQ
	public final static int ERR_UNABLE_TO_PROCESS_NOW = 400; //When Hogan sends a VODS error, the batch may be send again
	public final static int ERR_NO_QUESTIONS_FOR_USER = 410; //When there is no questions for the user to answer
	public final static int ERR_ANSWER_WRONG = 420; //The answer supplied by the user was wrong
	public final static int ERR_ALREADY_AUTH = 430; //Error occurs when you try to authorise a batch that is already auhted
	public final static int SUCCESSFULL_AUTH = 440; //Code for all successfull authorisations
	public final static int ERR_FUNDS_NOT_AVAIL = 450; //Code for all insufficient funds after processing of a batch
    public final static int ERR_SETTLEMENT_LIMIT_EXCEEDED = 451; //Code for all insufficient funds after processing of a batch
	public final static int ERR_USER_DISABLED = 460; //Error code used when user tries to do something, but is disabled(by somebody else)
	public final static int ERR_CUSTOMER_ACCOUNT_NOT_FOUND = 470; //Error code used when specific account for a customer is not found
	public final static int ERR_GLOBAL_ACCOUNT_NOT_FOUND = 480; //Error code used when a global account could not be found on our system
	public final static int ERR_FORMATTER_BATCH_ITEM_AMOUNT_INVALID = 490; //Error code used when the incoming batch item amount from the broken up MQ message is not valid
	public final static int ERR_BATCH_ITEM_NOT_FOUND = 500; //Error code when deleting a n batch item and the item is not found
	public final static int ERR_FORMATTER_BATCH_EXECUTION_DATE_INVALID = 510; //Error code when the incoming batch execution date from incoming MQ message is invalid/cannot be converted
	public final static int ERR_FORMATTER_BATCH_SIZE_INVALID = 520; //Error code when the incoming batch size indicator from incoming MQ message cannot be converted
	public final static int ERR_FORMATTER_CUST_ID_INVALID = 530; //Incoming custID from incoming MQ message cannot be converted
	public final static int ERR_FORMATTER_REMADVID_INVALID = 540; //Incoming Remittance Advice ID from incoming MQ message cannot be converted
	public final static int ERR_FORMATTER_SERVICE_PROVIDER_ID_INVALID = 550; //Incoming Service Provider ID from incoming MQ message cannot be converted
	public final static int ERR_FORMATTER_GLOBAL_ID_INVALID = 560; //Incoming Global ID from incoming MQ message cannot be converted
	public final static int ERR_DIPSWITCH_NOT_FOUND = 570; //Error code used when a select from IBDPBTHPF returns no results
	public final static int ERR_FORMATTER_BATCH_TYPE_INVALID = 580; //used when batch type cannot be converted to int
	public final static int ERR_FORMATTER_ACB_BATCH_EXECUTION_DATE_INVALID = 590; //Error code when the incoming batch execution date from incoming ACB file is invalid/cannot be converted to int.
	public final static int ERR_FORMATTER__ACB_BATCH_ITEM_AMOUNT_INVALID = 600; //Error code when the incoming batch item amount from incoming ACB file cannot be converted to BigDecimal.	
	public final static int ERR_FORMATTER_NO_SUCH_FORMATTER_FOUND = 610; //Used when no such instance of the data formatter can be found
	public final static int ERR_FORMATTER_EMPTY_FIELD = 620; // Used when the incoming field is null
	public final static int ERR_FORMATTER__FROM_ACC_BRANCH_NUMBER_INVALID = 630; //Error code when the incoming batch from account branch not numeric.
	public final static int ERR_FORMATTER_ABBREVIATED_NAME_NOT_PROVIDED = 640; //This is when the abbreviatyed name is incorrect or missing.
	public final static int ERR_UNABLE_TO_PROCESS_FILE = 700; // Unable to process file.
	public final static int ERR_UNABLE_TO_INFLATE_FILE = 710; // Unable to UNZIP the file on the server side.
	public final static int UPLOAD_SUCCESSFUL_BATCH_SUBMITTED_FOR_PROCESSING = 720; // Unable to UNZIP the file on the server side.
	public final static int UPLOAD_SUCCESSFUL = 730; // Unable to UNZIP the file on the server side.
	public final static int ERR_MQ_TIMEOUT = 740; // MQ timed out while waiting for a response from Hogan.	
	public final static int ERR_DAILY_LIMIT_EXCEEDED = 750; // The user exceeded his daily limit.
	public final static int ERR_AUTH_SHORTCUT = 800; // Authorization shortcut after financial modify (apply and auth)
	public final static int ERR_NOTIFY = 810; // Notify the authoriser.
	public final static int ERR_NOTHING_TO_AUTH = 830; // Nothing to authorise.
	public final static int ERR_SERVER_BEING_MAINTAINED = 840; // The function selected is not available because of maintenance being done
	public final static int ERR_SSO_SERVER_BEING_MAINTAINED = 841; // The function selected is not available because of maintenance being done
	public final static int ERR_FORMATTER_INVALID_FROM_ACCOUNT = 850; //Used when from account number invalid (in file upload)
	public final static int ERR_FORMATTER_INVALID_TO_ACCOUNT_TYPE = 860; // Used when the to account type invalid.
	public final static int ERR_BATCH_TOTAL_EXCEED_CAPT_LIMIT = 930; //The batch total exceeds the capture limit
	public final static int ERR_SUPPORT_REQUEST_EXPIRED = 940; //The support request expired
	public final static int ERR_SUPPORT_REQUEST_NOT_VALID = 950; //The support request does not exist
	public final static int ERR_SUPPORT_REQUEST_ALLREADY_LOGGEDON = 960; //Trying to logon, but there is allready a request logged on
	public final static int ERR_INVALID_VIDEOBANK_USERID = 1000; //  Videobank User ID provided is not valid.
	public final static int ERR_VIDEOBANK_USER_NOT_ACTIVATED = 1010; //Videobank User not activated
	public final static int ERR_VIDEOBANK_PASSWORD_INVALID = 1020; //Videobank Password provided invalid.
	public final static int ERR_VIDEOBANK_PASSWORD_REVOKED = 1030; //Videobank Password revoked
	public final static int ERR_VIDEOBANK_USER_NO_ACCESSIBLE_ACCOUNTS = 1040; //Videobank User has no accessible acounts.
	public final static int ERR_VIDEOBANK_DATABASE_ACCESS_ERROR = 1050; //Videobank has problems - database errors etc.
	public final static int EXTRA_AUTHERISERS_REQUIRED = 1060; //Extra Videobank autherisers required
	public final static int ERR_UNABLE_TO_CONVERT_CUSTOMER = 1070; //Customer can't go through the VideoBank Conversion due to multiple Users on Videobank.
	public final static int ERR_SUPPORT_USER_NOT_FOUND = 1080; //Error when resolvong from a supportUserID to a supportUserRFN
	public final static int ERR_VIDEOBANK_CUSTOMER_CONVERTED = 1090; //Videobank customer already converted.
	public final static int ERR_VIDEOBANK_CUSTOMER_LOGON_SUSPENDED = 1100; //Videobank customer suspended from logging on for 24 hours.
	public final static int ERR_UNABLE_TO_CREATE_CUSTOMER = 1110; //Unable to create BANKit customer during enrollment or conversion.
	public final static int ERR_NO_BILLING_ACCOUNT_AVAILABLE = 1120; //No Billing Account available, so we cannot convert cutomer.
	public final static int ERR_DUPLICATE_BATCH_NAME = 1130; //Batch saved with duplicate batch name
	public final static int ERR_PARTIALLY_DELETED_MULTIPLE_BATCHES = 1140; //On multiple Batch delete, not all batches are deleted... some failed
	public final static int ERR_NPS_NOT_ALLOWED_FOR_THIS_COUNTRY = 1150;	
	public final static int ERR_INVALID_NUMBER_OF_AUTHORISERS = 1160; //Number of authorisers not allowed.
	public final static int ERR_CAPTURE_AMOUNT_NOT_ALLOWED = 1170; // The item Amount is not allowed
	public final static int ERR_INFLUENCE_NUMBER_OF_AUTHORISERS = 1180; // When the user tries to delete an user, and it has an effect on the amount of authorisers.
	public final static int ERR_ACTION_ALREADY_PERFORMED = 1190; // When the user double-click on the 'Apply' button.
	public final static int ERR_UNABLE_TO_CREATE_BATCH = 1200; //When a batch can't be created from a FinancialCrossLinker
	public final static int ERR_CERT_ALREADY_EXISTS = 1210; // When the user double-click on the 'Apply' button.
	public final static int ERR_BATCH_TOTAL = 1220; //Error code for displaying batch total
	public final static int ERR_NO_ITEMS = 1230; //Error code used when, after loading, there is no items in a collection
	public final static int ERR_NO_ITEMS_WITH_AMOUNTS = 1240; //Used when a payment batch is auth requested and no item amounts where specified
	public final static int ERR_NO_PAYMENTS_PERMISSION = 1250; //Used when an account/user has no payment permission	
	public final static int ERR_DUPLICATE_DESCRIPTION = 1260; //Duplicate description on transaction query is not allowed
	public final static int ERR_TRANSACTION_NULL = 1270; //Transaction null 
	public final static int ERR_BATCH_EXECUTION_DATE_MORE_THAN_YEAR_IN_ADVANCE = 1280; //Used when a user wants to upload a uap with a date that is more than 1 year in advance.
	public final static int ERR_NEED_A_SIGNATORY = 1290; // Used when we are the last authorisation, and no user of type 'A' Signatory has authorised.
	public final static int ERR_NO_CAPTURE_PERMISSION = 1500;//User has no capture permission
	public final static int ERR_ADMIN_NOT_KYC_VERIFIED = 2000; // Administrator still needs to be KYC verified
	public final static int ERR_CUSTOMER_NOT_KYC = 2010; // Used when we are the last authorisation, and no user of type 'A' Signatory has authorised.
	public final static int ERR_PARTIALLY_SUBMITTED_MULTIPLE_BATCHES = 2020; //On multiple Batch submit, not all batches are submitted ... some failed.
	public final static int ERR_DEATHORIZE_RECURRING_PAYMENT = 2021; //Recurring payment can not be de-authorised.
	public final static int ERR_PARTIALLY_REPROCESS_MULTIPLE_BATCHES = 2030; //On multiple Batch reprocess, not all batches are reprocessed ... some failed.
	public final static int ERR_PARTIALLY_DEAUTH_MULTIPLE_BATCHES = 2040; //On multiple Batch reprocess, not all batches are deauthorised ... some failed.
	public final static int ERR_PAYMENT_COUNT_EXCEEDED = 2050; //Daily payment count exceeded
	public final static int ERR_COLLECTION_COUNT_EXCEEDED = 2051; //Daily collection count exceeded
	public final static int ERR_BATCH_EXECUTION_DATE_IN_PAST = 2060; //Execution date is ahead of current date
	public final static int ERR_BATCH_EXECUTION_DATE_INVALID_FORMAT = 2070; //The format of the date is incorrect
	public final static int ERR_LIMIT_FOR_PRINTVIEW_EXCEEDED = 1530; //Please use the PDF View for all Payments including more than 100 items
	public final static int ERR_NO_REMITTANCE_ADVICES_IN_PDF = 1540; //Payment has no payment advices
	public final static int CHECK_INBOX = 1541; //Please check your inbox for the download
	public final static int ERR_MORE_INFO_NEEDED = 1542;
	public final static int ERR_ID_NOT_MATCHED = 5509; //You may have more than one Online Banking profile linked to your ID / Passport number. Please call Online Assistance to have your access details reset.
	public final static int ERR_ONLY_INDIVIDUAL_ALLOWED = 5510; //This card is linked to a business account. Please use a card that is linked to a personal account.
	public final static int ERR_INVALID_CARD_PIN_COMBINATION = 5511; //Either the card number or PIN entered was invalid hence you could not be validated. Please contact your branch if this problem persists
	public final static int ERR_ZOB_STATUS_FRAUD_RELATED = 5512; //Your profile has been blocked. Please contact the Fraud Prevention Centre on 087 575 0011.
	public final static int ERR_NAMIBIA_CRYPTO_NOT_READY = 5513; //Still waiting for error message to set
	public final static int ERR_INVALID_CUSTOMER_STATUS = 5514; // The CUSTOMER is not in an ACTIVE status and cannot be retrieved.
	public final static int ERR_INVALID_GTS_MSG_TYPE = 5515; // 
	public final static int ERR_UNABLE_GTS_MOVE_ENTITY = 5520; // 
	public final static int ERR_UCN_NOT_FOUND = 5516; // The UCN could not be found 
	public final static int ERR_CUSTOMER_DOESNT_EXIST = 5517; // The CUSTOMER Does'nt exist in the Database.
	public final static int ERR_STATUS_CHANGE_NOT_ALLOWED = 5518; // The request for a status change is not allowed.
	public final static int ERR_CASCADING_NOT_ALLOWED_FOR_ACTION = 5519; // Cascading not allowed, therefore action cannot be processed.
	public final static int DUPLICATE_ENTRY = -1;
	public final static int ERR_CRYPTO_HTTP_CALL_FAILED = 5521;
	/*
	 * Series of codes between 1300 and 1499 is for certificate issue only 
	 */
	public final static int ERR_UNABLE_TO_VALIDATE_CERT_REQUEST = 1300; // Unable to validate the signature of request.
	public final static int ERR_UNABLE_TO_READ_SID_PUBLICKEY = 1310; // Unable to access/find SID publickey from the DB.
	public final static int ERR_UNABLE_TO_PARSE_REQUEST = 1320; // Unable to parse XML REquest Document.
	public final static int ERR_UNABLE_GENERATE_X509_CERTIFICATE = 1330; // Unable to generate a X509 certificate from the request received.
	public final static int ERR_UNABLE_EXTRACT_DETAILS_FOR_X509_CERTIFICATE = 1340; // Unable to retrieve details for certificate.	
	public final static int ERR_UNABLE_TO_ACCESS_CERT_ISSUE = 1350; // Unknown fatal problem occurred.
	public final static int ERR_UNABLE_TO_SET_SERIAL_NUMBER = 1360; // Certificate issued successffully but unable to return serialnumber	
	public final static int ERR_UNEXPECTED_CLIENT_SIDE_ERROR = 1400; // Unexpected error caused the client side process to fail.
	public final static int ERR_ACTION_CANCELLED = 1410; // Action cancelled on applet side	
	public final static int ERR_INVALID_RUNTIME_ENVIRONMENT = 1420; // Client side Java is not on correct version.
	public final static int ERR_INVALID_AUTHENTICATION_CERT_ISSUE = 1430; // Invalid auth codes entered at cert issue - log off

	/*
	 * Series of codes between 1501 and 2000 are reserved for file upload
	 */
	public final static int ERR_INVALID_RECORD_IDENTIFIER=1501;
	public final static int ERR_INVALID_FROM_ACCOUNT_NUMBER=1502;
	public final static int ERR_INVALID_TO_ACCOUNT_NUMBER=1503;
	public final static int ERR_INVALID_TO_ACCOUNT_BRANCH_CODE=1504;
	public final static int ERR_INVALID_FROM_ACCOUNT_BRANCH_CODE=1505;
	public final static int ERR_INVALID_ITEM_AMOUNT=1506;
	public final static int ERR_UNABLE_TO_EXTRACT_VALID_TO_ACCOUNT_NUMBER=1507;
	public final static int ERR_INVALID_RECORD_IDENTIFIER_ZERO_NOT_ALLOWED=1508;
	public final static int ERR_INVALID_FROM_ACCOUNT_NUMBER_ZERO_NOT_ALLOWED=1509;
	public final static int ERR_INVALID_TO_ACCOUNT_NUMBER_ZERO_NOT_ALLOWED=1510;
	public final static int ERR_INVALID_TO_ACCOUNT_BRANCH_CODE_ZERO_NOT_ALLOWED=1511;
	public final static int ERR_INVALID_FROM_ACCOUNT_BRANCH_CODE_ZERO_NOT_ALLOWED=1512;
	public final static int ERR_INVALID_RECORD_IDENTIFIER_EMPTY=1513;
	public final static int ERR_INVALID_FROM_ACCOUNT_NUMBER_EMPTY=1514;
	public final static int ERR_INVALID_TO_ACCOUNT_NUMBER_EMPTY=1515;
	public final static int ERR_INVALID_ACCOUNT_TYPE_EMPTY=1516;
	public final static int ERR_INVALID_TO_ACCOUNT_BRANCH_CODE_EMPTY=1517;
	public final static int ERR_INVALID_ITEM_AMOUNT_EMPTY=1518;
	public final static int ERR_INVALID_TO_ACCOUNT_REFERENCE_EMPTY=1519;
	public final static int ERR_INVALID_PAYEE_NAME_EMPTY=1520;
	public final static int ERR_RECORD_CONTAINS_MULTIPLE_ERRORS=1521;
	public final static int ERR_INVALID_ITEM_AMOUNT_NEGATIVE_NOT_ALLOWED=1522;
	public final static int ERR_EITHER_ID_NUMBER_OR_OWNER_NAME_MUST_BE_ENTERED=1523;
	public final static int ERR_INVALID_ACCOUNT_TYPE=1524;
	public final static int ERR_MULTIPLE_EXECUTION_DATES_NOT_ALLOWED=1572;
	public final static int ERR_INVALID_EXECUTION_DATE=1573;
	
	public final static int ERR_PASSWORD_ID_SAME = 1531;
	public final static int ERR_PASSWORD_NAME_SAME = 1532;
	public final static int ERR_PASSWORD_SURNAME_SAME = 1533;
	public final static int ERR_PASSWORD_HAS_SPACES = 1534;
	public final static int ERR_PASSWORD_LENGTH = 1535;
	public final static int ERR_PASSWORD_STARTS_NUMBER = 1536;
	public final static int ERR_PASSWORD_HAS_DUP_CHARS = 1537;
	public final static int ERR_PASSWORD_HAS_NON_ALPHA = 1538;
	public final static int ERR_OTP_PARAM_MISSING = 1542;
	public final static int ERR_OTP_NOT_SENT = 1543;
	public final static int ERR_OTP_SENT = 1544;
	public final static int ERR_OTP_INCORRECT = 1545;
	public final static int ERR_INVALID_CARD_BIN = 1546;
	public final static int ERR_INVALID_CARD_NO_OR_PIN = 1547;
	public final static int REQUEST_RECEIVED = 1548;
	public final static int ERR_OTP_CAUSED_DISABLE = 1549;
	public final static int ERR_ADMIN_REACT_INPROGRESS = 1550;
	public final static int ERR_DAILY_CHANNEL_LIMIT_EXCEEDED = 1551;
	public static final int ERR_DAILY_CHANNEL_LIMIT_EXCEEDED_HOST = 99026; //Hogan Channel Limit error
	public final static int ERR_SHED_DATE_TOO_DISTANT = 1552; //Start date must not be more that 1 year from current date
	public final static int ERR_SHED_DATE_START_GREATER_THAN_END = 1553;//Start date is greater than end date
	public final static int ERR_OTP_ABOUT_TO_DISABLE = 1554;
	public final static int ERR_OTP_MISSING = 1555;
	public final static int ERR_PASSWORD_USERNAME_SIMILAR = 1556;
	public final static int ERR_PASSWORD_NAME_SIMILAR = 1557;
	public final static int ERR_USERNAME_LENGTH_ERROR = 1558;
	public final static int ERR_USERNAME_INVALID_CHARACTERS = 1559;
	public final static int ERR_UNABLE_TO_ALLOCATE_LOCK = 1560;
	public final static int ERR_MAX_PAYMENTS_EXCEEDED = 1561;
	public final static int ERR_NO_PERMISSIONS_GRANTED = 1562;
	/* Cellphone Validation */
	public static final int CELLPHONE_NUMBER_EMPTY = 1563;
	public static final int CELLPHONE_PREFIX_INVALID = 1564;
	public static final int CELLPHONE_LENGTH_INVALID = 1565;
	public static final int CELLPHONE_NOT_NUMERIC = 1566;
	public static final int CELLPHONE_CANNOT_BE_VALIDATED = 1567;
	public static final int CELLPHONE_NUMBER_INVALID = 1569;
	public static final int ERR_INVALID_TO_FROM_ACCOUNT_NUMBER=1570;
	public final static int ERR_SHED_DATE_GREATER_TODAY = 1571;
	public static final int ERR_MULTIPLE_EXECUTION_DATES = 1572;
	public static final int ERR_INVALID_EXECUTION_DATE_APPLET = 1573;
	public static final int ERR_DMT_ERROR = 1574;
	/*Forex error codes*/
	public static final int ERR_FOREX_DEAL_AMOUNT_EXCEEDED = 1575;
	public static final int ERR_FOREX_PERCENTAGE_EXCEEDED = 1576;
	public static final int ERR_FOREX_DATE_OVER_60 = 1577;
	public static final int ERR_FOREX_DATE_LESS_THAN_DEFAULT = 1578;
	public static final int ERR_FOREX_DATE_HOLIDAY = 1579;
	public static final int ERR_FOREX_DATE_WEEKEND = 1580;
	public static final int ERR_FOREX_DATE_SUNDAY = 1581;
	public static final int ERR_FOREX_ACCEPT_TIMEOUT = 1582;
	public static final int ERR_FOREX_DORMANT_ACCOUNT = 1583;
	public static final int ERR_FOREX_INSUFFICIENT_FUNDS = 1584;
	public static final int ERR_FOREX_UNAVAILABLE = 1585;
	public static final int ERR_FOREX_FUNCTION = 1588;
	public static final int ERR_FOREX_ID_DOB_MATCH = 1589;
	public static final int ERR_FOREX_INVALID_ALLOCATION = 1590;
	public static final int ERR_FOREX_INVALID_MIN_AMOUNT = 1591;
	public static final int ERR_FOREX_NO_CURRENCY = 1592;
	public static final int ERR_FOREX_NO_ITEMS = 1593;
	public static final int ERR_FOREX_WRONG_DATE_FORMAT = 1594;
	public static final int ERR_FOREX_INVALID_AMOUNT = 1595;
	public static final int ERR_FOREX_TERMS = 1596;
	public static final int ERR_FOREX_GET_QUOTE = 1597;
	public static final int ERR_FOREX_INVALID_CURRENCY_PROD_COMB = 1598;
	public static final int ERR_FOREX_DUPLICATE = 1599;
	public static final int ERR_FOREX_DUPLICATE_ID = 1600;
	public static final int ERR_FOREX_INV_COLLECTION_DATE = 1601;
	public static final int ERR_FOREX_ANNUAL_FOREX_LIMIT_EXCEEDED = 1602;
	public static final int ERR_IMAGES_NONE_AVAILABLE = 1614;//No images available at present. 
	public static final int ERR_IMAGES_REPOSITORY_UNAVAILABLE = 1603;//Image repository unavailable.
	public static final int ERR_FOREX_AFTER_HOURS = 87511;
	
	public static final int ERR_USER_NOT_FOUND_GENERIC = 1586;
	public static final int ERR_DIGICODE_INVALID = 1587;
	public static final int ERR_DIGICODE_ALREADY_ACTIVE = 1613;
	public static final int ERR_CONFIRM_AUTOBUMP = 1604;
	public static final int ERR_SEARCH_RESULTS_EXCEEDED = 1605;
	public static final int ERR_IN_PROGRESS = 1606;
	public static final int ERR_USER_NOT_FOUND_LOGIN = 1607;
	public static final int ERR_BACK_BUTTON_LOGIN_ATTEMPT = 1608;
	public final static int ERR_TO_ACCOUNT_REFERENCE_INVALID=1609;
	public final static int WES_TRANS_HIST_UNAVAILABLE = 1610; // Wesbank transaction history unavailable
	public final static int WES_DETAILED_BAL_UNAVAILABLE = 1611; // Wesbank detailed balance unavailable
	public final static int ERR_STATUS_UPDATING = 1612; // The status of the item selected is currently updating and cannot be maintained. Please try again later.
	//Letter of Credit
	public final static int ERR_QUALIFYING_ACCOUNT_NOT_FOUND = 1655; // You don't have any qualifying accounts to capture Once-Off Application
	
	//Manage User Access
	public final static int ERR_USER_ACCESS_RESTRICT_TIME = 1615; // Your access details have been entered incorrectly. Please note on your third failed attempt your profile will be blocked.
	public final static int ERR_USER_ACCESS_AUTH = 1616; // As per your specific security requirements, you cannot perform this function. For further details please contact FNB Online Assistance.
	public final static int ERR_USER_ACCESS_RESTRICT_IP = 1617; // Your access details have been entered incorrectly. Please note on your third failed attempt your profile will be blocked.
	public final static int ERR_USER_ACCESS_RESTRICTED = 1618; // Your access details have been entered incorrectly. Please note on your third failed attempt your profile will be blocked.
	public final static int ERR_USER_ACCESS_NOT_TRUSTED_PROFILE = 1619; 
	public final static int ERR_SECURITY_DETAILS_COULD_NOT_BE_LOADED = 1620;
	public final static int ERR_USER_ACCESS_VERIFICATION_PROBLEM_BROWSER_IE = 1621;
	public final static int ERR_USER_ACCESS_VERIFICATION_PROBLEM_BROWSER_FIREFOX = 1622;
	public final static int ERR_USER_ACCESS_VERIFICATION_PROBLEM_OTHER_BROWSER = 1623;
	public final static int ERR_USER_ACCESS_NOT_MATCHING_PROFILE_CEILING = 1624;
	public final static int ERR_USER_ACCESS_NOT_MATCHING_PROFILE_FLOOR = 1625;
	public final static int ERR_USER_ACCESS_NOT_MATCHING_PROFILE = 1626;
	public final static int ERR_USER_ACCESS_NO_PROFILES_EXIST_FOR_USER = 1627;
	public final static int ERR_SECURITY_CODE_UNABLE_TO_GENERATE = 1628;
	public final static int ERR_SECURITY_CODE_UNABLE_TO_DECRYPT = 1629;
	public final static int ERR_URL_IDENTIFIER_UNABLE_TO_DECRYPT = 1630;
	public final static int ERR_URL_ERR_UNABLE_TO_DECRYPT = 1630;
	public final static int ERR_SECURITY_CODE_INVALID = 1631;
	
	public final static int ERR_USER_ACCESS_PENDING_TOKEN_ADD = 1649;
	public final static int ERR_USER_ACCESS_MUA_SETTINGS_UPDATE = 1650;
	
	public final static int ERR_PKI_FAILED_TO_ACTIVATE_CERTIFICATE = 1632;
	
	// Revolving Loan Decrease Error Codes
	public final static int ERR_REV_LOAN_DECREASE_AMOUNT_THOUSANDS = 1633;
	public final static int ERR_REV_LOAN_DECREASE_AMOUNT_HIGH = 1634;
	public final static int ERR_REV_LOAN_DECREASE_AMOUNT_LOW_BALANCE = 1635;
	public final static int ERR_REV_LOAN_DECREASE_AMOUNT_LOW_MINIMUM = 1636;
	public final static int ERR_REV_LOAN_DECREASE_DEPT_PROTECT_SELECT = 1637;
	
	public final static int ERR_REV_LOAN_DECREASE_DISCLAIMER_ACCEPT = 1638;
	public final static int ERR_REV_LOAN_DECREASE_TC_ACCEPT = 1639;
	public final static int ERR_REV_LOAN_DECREASE_DEPT_PROTECT_TC_ACCEPT = 1640;
	// Revolving Loan Settle Error Codes
	public final static int ERR_REV_LOAN_SETTLE_OPTION_SELECT = 1641;
	public final static int ERR_REV_LOAN_SETTLE_FROM_ACCOUNT_SELECT = 1642;
	public final static int ERR_REV_LOAN_SETTLE_FROM_ACCOUNT_REF = 1643;
	public final static int ERR_REV_LOAN_SETTLE_TO_ACCOUNT_REF = 1644;
	public final static int ERR_REV_LOAN_SETTLED_ALREADY = 1646;
	public final static int ERR_REV_LOAN_SETTLE_NOT_ALLOWED = 1647;
	public final static int ERR_REV_LOAN_CLOSE_NOT_ALLOWED = 1648;
	// Revolving Loan Hogan Genic Error Code
	public final static int ERR_REV_LOAN_HOGAN = 1645;
	
	//Reset Daily Limit or UserID and password - Business Standin Error Code
	public final static int ERR_BUSINESS_RESET_HOGAN = 1651;
	public final static int ERR_BUSINESS_CARD_PIN = 1652;
	public final static int ERR_SAVINGS_POCKET_TERMS_AND_CONDITIONS = 1653;
	
	//Lotto user has no valid winnings pay-into accounts. MG
	public final static int ERR_NO_LOTTO_WINNINGS_ACCOUNT = 1654;
	
	
	//Self-service reactivation
	public final static int ERR_SSREACTICATION_SSEM_02 = 1656; //Invalid passcodes
	public final static int ERR_SSREACTICATION_SSEM_04 = 1657; //Not a trusted profile
	public final static int ERR_SSREACTICATION_SSEM_05 = 1658; //Invalid certificate
	
	
	// ADD NEW ONES HERE ONLY - not at the top, not at the bottom
	
	public static final int ERR_FOREX_HOST_QOUTE_EXPIRY_TIMEOUT = 1700;  //Forex host quote expiry timeout
	public static final int ERR_PASSWORD_MUST_CONTAIN_ATLEAST_ONE_UPPERCASE_LOWERCASE_NUMERIC_SPECIAL_CHARACTER = 1701;
	public static final int ERR_RTC_NOT_ALLOWED = 1702;
	public static final int ERR_FUNCTION_NOT_ALLOWED = 1703;
	
	//CUCU - Related Parties
	public static final int ERR_MAINTAIN_CUCU_SIEBEL_REQUEST_FAILED = 1800;
	public static final int ERR_CUCU_FILE_UPLOAD_FAILED = 1801; //Most likely due to the DAO's
	public static final int ERR_CUCU_FILE_UPLOAD_FUS_FAILED = 1802; //Coming back from FUS. File Upload Failed
	public static final int ERR_RELATED_PARTY_DELETE_REQUEST_FAILED = 110032;

	
	public final static int ERR_TRANS_HIST_NOT_FOUND = 2078; //TCS tran hist not found
	public final static int ERR_TEMP_PASSWRD_EXPIRED = 2080; //Temporary password expired 
	public final static int ERR_NOT_ADMIN_AND_NO_ZAP = 2081; //User cannot access payments tab if no zap number and not an administrator.
	
	public final static int ERR_INVALID_NAEDO_BRANCH = 3000; //The branch selected is not a valid NAEDO branch
	public final static int ERR_NO_ABBR_NAMES = 3001; //Abbreviated name has to be set up for NAEDO collections
	
	//Ras error codes.
	public final static int ERR_RAS_TECHNICAL_ERROR = 4000; //Your requested results could not be obtained at this stage, please try again later or contact us at ras@firstrandbank.co.za.
	public final static int ERR_RAS_SUN_HOL_ERROR = 4001;//Date entered is a Sunday or Public Holiday. Please refine your search criteria.
	public final static int ERR_RAS_FUTURE_DATE = 4002;//A future date has been selected, please enter a valid date and search again. 
	public final static int ERR_RAS_SUN_HOL_PREV_BUS_DAY_ERROR = 4003;//Date entered is a Sunday or Public holiday, the previous business day results will be shown.
	public final static int ERR_RAS_DETAILED_BAL_ERROR = 4004;//No results are available for the search criteria.
	
	//Web Service error codes.
	public final static int ERR_WEB_SERVICE_TECHNICAL_ERROR = 4005; //Web Service is unavailable.

	//Wealth error codes.
	public final static int ERR_WEALTH_ERROR = 4006; //Error was returned from the Wealth Web Service system. 
	
	//FNB Connect error codes.
	public final static int ERR_CONNECT_TRANS_CANCELLED = 4007; //A time-out has occurred and the transaction was cancelled. Please would you repeat the transaction.
	public final static int ERR_CONNECT_TECHNICAL_ERROR = 4008; //Sorry, but at the moment we're unable to process your request. Please try again later. 

	//Siebel error codes.
	public final static int ERR_SIEBEL_ERROR = 4011; //Error was returned from the SIEBEL Web Service system. 
	
	//Hierarchy error codes
	public final static int ERR_HIERARCHY_GROUP_NOT_FOUND = 5000; //The requested hierarchy group could not be found.
	public final static int ERR_UNABLE_TO_AUTH_INSTRUCTION = 5001; //The account instruction could not be authorised.
	
	//Settlement Limits
	public final static int ERR_SLE_CHECK_LIMIT 			= 6000; //Please check your limits
	public final static int ERR_SLE_STANDIN 				= 6001; //The Limit System is currently unavailable. Please try submitting your transaction later.
	public final static int ERR_SLE_INVALID_SERVICE_TYPE 	= 6002; //Your current limit status does not permit this service type. Please re-select.
	public final static int ERR_SLE_ENTITY_NOT_FOUND 		= 6003; //There are no limits for this entity
	public final static int ERR_SLE_LIMIT_BALANCE_NOT_AV 	= 6004; //Unable to retrieve Available Settlement Limit for account
	public final static int ERR_SLE_INVALID_ENTITY_NUMBER 	= 6005; //Please select an Entity number
	public final static int ERR_SLE_LIMIT_UTIL_NOT_FOUND 	= 6006; //Limit Utilisation Not Found.
	public final static int ERR_SLE_LIMIT_NOT_FOUND_ACC 	= 6007; //'Transactions against this account or entity do not use a limit. There is therefore no 
																	//limit utilisation available' where ercoderfn=6007
	public final static int ERR_SLE_INVL_FUTURE_DATE	 	= 6008; //Limit Utilisation Not Found.
	public final static int ERR_SLE_SUBMIT_STANDIN			= 6032; //The submit payment functionality is not currently available for this payment type, please try again later.
	public final static int ERR_SLE_AUTH_STANDIN			= 6033; //The authorise payment functionality is not currently available for this payment type, please try again later.
	public final static int ERR_SLE_REJECT_STANDIN			= 6034; //The reject a payment functionality is not currently available for this payment type, please try again later.
	
	//Card Maintenance
	public final static int ERR_CARD_MAINT_LIMITS_UNAVAILABLE 	         = 6009; //Invalid Card Number.
	public final static int ERR_CARD_MAINT_LIMITS_CARD_NO_EMPTY_OR_NULL	 = 6010; //Card Number is null or empty.
	public final static int ERR_CARD_MAINT_LIMITS_INVALID_CARD_NUMBER    = 6011;//Invalid Card Number.
	public final static int ERR_CARD_MAINT_INVALID_CARD_STATUS           = 6025;//Card Status Is Not Active'
	public final static int ERR_CARD_MAINT_WRONG_RELATIONSHIP_FOR_CARD_TYPE     = 616;//Wrong Relationship for Card Type
	public final static int ERR_CARD_MAINT_ACCOUNT_NOT_ACTIVE           = 10224;//Account not active status
	
	
	
	public final static int ERR_CARD_MAINT_NOT_ALLOWED    = 6012;//, 'ATM Limit Maintenance not allowed on card selected.', 1);
	public final static int ERR_CARD_MAINT_RECLACEMENT_STATUS    = 6013;// 'Replacement Card cannot be ordered on active card selected.', 1);

	public final static int ERR_CARD_MAINT_SELECT_CARD    = 6014;// 'Please select a card you would like to maintain.', 1);
	public final static int ERR_CARD_MAINT_CARD_ALREADY_CANCELLED    = 6015;// 'Card already cancelled.', 1);
	public final static int ERR_CARD_MAINT_LIMIT_MIN_MAX    = 6016;// 'The limit entered must be between xxmin and xxmax.', 1);
	public final static int ERR_CARD_MAINT_INVALID_LIMIT    = 6017;// 'Invalid limit entered.', 1);
	public final static int ERR_CARD_MAINT_SELECT_FNB_BRANCH    = 6018;// 'Please select an FNB branch.',1);
	public final static int ERR_CARD_MAINT_INVALID_RES_ADD    = 6019; //'Please provide a valid residential address.', 1);
	public final static int ERR_CARD_MAINT_INVALID_RES_ADDR    = 6020;// 'Residential Address must be between 2 and 40 characters long.', 1);
	public final static int ERR_CARD_MAINT_INVALID_SUBURB_NAME    = 6021;// 'Please provide a valid suburb name.', 1);
	public final static int ERR_CARD_MAINT_INVALID_SUBURB_LENGTH    = 6022;// 'Suburb must be between 2 and 40characters long.', 1);
	public final static int ERR_CARD_MAINT_INVALID_CITY_NAME    = 6023;// 'Please provide a valid city name.', 1);
	public final static int ERR_CARD_MAINT_INVALID_CITY_LENGTH    = 6024;// 'City must be between 2 and 40 characters long', 1);
	public final static int ERR_CARD_MAINT_INVALID_POSTAL_CODE    = 6026;// 'Postal Code must be between 4 and 5 characters long', 1);
	public final static int ERR_CARD_MAINT_SELECT_PROVINCE    = 6027;// 'Please select a province.', 1);
	public final static int ERR_CARD_MAINT_SELECT_CARD_TO_CANCEL    = 6028;// 'Please select a card you wish to cancel before proceeding.'; 
	public final static int ERR_CARD_MAINT_SELECT_REASON    = 6029;// 'Please select a reason for cancelling your Card before proceeding.; 
	public final static int ERR_CARD_MAINT_UNABLE_TO_RETRIEVE    = 6030;// We were unable to retrieve the cards linked to your profile, please retry.
	public final static int ERR_CARD_MAINT_NO_CARDS_LINKED    = 6031;//There are no cards linked to your profile.
	public final static int ERR_CARD_MAINT_ONLY_ATM_DAILY    = 6051;//, 'Only Atm Daily Local - Cr Card Allow', 1
	public final static int ERR_CARD_MAINT_REPLACEMENT_NO_BRANCH = 6048; //No branch provided by host.
	public final static int ERR_CARD_MAINT_REPLACEMENT_NO_CARD_TYPES = 6049; //Failed to retrieve card types.
	public final static int ERR_CARD_MAINT_REPLACEMENT_BRS_SCORING = 6050; //BRS SCORING IS BAD - ZERO FLOOR LIMITS
	public final static int ERR_EXPORT_PROFILE_ENTITY = 6052;//Please select an entity.
	public final static int ERR_CARD_ACTIVATED = 6072; // Card activation sucessful
	public final static int ERR_CARD_NOT_ACTIVATED = 6073; // Card activation failed
	public final static int ERR_ZOB_ON_ZBI_BOX = 6074;
	public final static int ERR_ZBI_ON_ZOB_BOX = 6075;
	
	//Hash Total
	public final static int ERR_HASH_TOTAL_CONFLICT = 6070;
	public final static int ERR_HASH_TOTAL_CONFLICT_WARN = 6071;
	
	//TRAFFIC FINES
	public final static int ERR_TRAFFIC_FINES_BASE = 7000;
	public final static int ERR_TRAFFIC_FINES_INVALID_REF =  1 + ERR_TRAFFIC_FINES_BASE;//Invalid Reference
	public final static int ERR_TRAFFIC_FINES_UNPAYABLE =  2 + ERR_TRAFFIC_FINES_BASE; //Traffic fine unpayable
	public final static int ERR_TRAFFIC_FINES_PAID =  3 + ERR_TRAFFIC_FINES_BASE; //Traffic fine paid
	public final static int ERR_TRAFFIC_FINES_OVERDUE =  4 + ERR_TRAFFIC_FINES_BASE; //Traffic fine overdue
	public final static int ERR_TRAFFIC_FINES_POLICE =  5 + ERR_TRAFFIC_FINES_BASE; //must pay manually police involvement
	public final static int ERR_TRAFFIC_FINES_WARRANT =  6 + ERR_TRAFFIC_FINES_BASE; //warrant of arrest
	public final static int ERR_TRAFFIC_FINES_CANCELLED =  7 + ERR_TRAFFIC_FINES_BASE; //traffic fine cancelled	
	public final static int ERR_TRAFFIC_FINES_GENERAL =  98 + ERR_TRAFFIC_FINES_BASE; //general error	
	public final static int ERR_TRAFFIC_FINES_PROVIDER =  99 + ERR_TRAFFIC_FINES_BASE; //Third party (TCS, Traffman, eNatis) is unavailable
	public final static int ERR_TRAFFIC_FINES_ASYNCH_TIMEOUT = 99051 + ERR_TRAFFIC_FINES_BASE; //traffic fine cancelled
	public final static int ERR_TRAFFIC_FINES_INVALID_PURCHASE = 578 + ERR_TRAFFIC_FINES_BASE; //Invalid Purchase Information
	
	//PAYPAL Errors
	public final static int ERR_PAYPAL_BASE                      = 8000;
	public final static int ERR_PAYPAL_HOST_MIN                  = 1 + 	ERR_PAYPAL_BASE;//Message number 00001 – 00029: Display message from host.
	public final static int ERR_PAYPAL_HOST_MAX                  = 29 + ERR_PAYPAL_BASE;//Message number 00001 – 00029: Display message from host.
	public final static int ERR_PAYPAL_IB_VALIDATION             = 30 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_ACCOUNT_UNVERIFIED        = 38 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_HOST_OVER_LIMIT           = 43 + ERR_PAYPAL_BASE;//Over Limit.
	public final static int ERR_PAYPAL_HOST_INSUFFICIENT_FUNDS   = 47 + ERR_PAYPAL_BASE;//Insufficient Funds
	public final static int ERR_PAYPAL_HOST_INVALID_PAYPAL_EMAIL = 54 + ERR_PAYPAL_BASE;//Over Limit.
	public final static int ERR_PAYPAL_SOLE_PROPRIETOR   		 = 60 + ERR_PAYPAL_BASE;//As the FNB account you are attempting to link reflects as a �sole proprietor� we require your tax number or VAT number in order to l ink your PayPal wallet. These details are required by the South African Reserve Bank for transaction reporting purposes and can only be updated at an FNB branch
	public final static int ERR_PAYPAL_PASSPORT_NUMBER   		 = 61 + ERR_PAYPAL_BASE;//You are registered on an FNB system with a passport number. Passport numbers can t be used for PayPal transactions
	public final static int ERR_PAYPAL_INVALID_ID   		 	 = 62 + ERR_PAYPAL_BASE;//You have presented an invalid ID document for use with PayPal.You ll need to take your Temporary Resident Permit documentation to the branch to update your profile
	public final static int ERR_PAYPAL_NON_ZA_RESIDENT 			 = 63 + ERR_PAYPAL_BASE;//This service is only available to South African Residents
	public final static int ERR_PAYPAL_INCORRECT_PAYPAL_EMAIL  	 = 64 + ERR_PAYPAL_BASE;//This service is only available to South African Residents
	public final static int ERR_PAYPAL_CUSTOMER_NOT_KYCD	 	 = 66 + ERR_PAYPAL_BASE;//You are unable to link your PayPal profile as there are certain FICA requirements outstanding on your account/s please contact your nearest branch to complete these
	public final static int ERR_PAYPAL_TRUSTS_NOT_ALLOWED	 	 = 67 + ERR_PAYPAL_BASE;//The linking of your FNB account to your PayPal account was unsuccessful.  Unfortunately, registered trusts may not use the FNB PayPal Service.  Please contact 0861 729 725 for more information
	public final static int ERR_PAYPAL_NO_NPO_COMPANY_REG	 	 = 68 + ERR_PAYPAL_BASE;//Linking unsuccessful � please contact your nearest branch to have either your NPO registration number or tax reference number captured

	public final static int ERR_PAYPAL_THIRD_PARTY_IDTYPE_INCORRECT	 = 75 + ERR_PAYPAL_BASE;//Third party validations for id Types
	public final static int ERR_PAYPAL_THIRD_PARTY_PASSPT_NO_INVALID = 79 + ERR_PAYPAL_BASE;//Third party validations for passports
	
	public final static int ERR_PAYPAL_IB_UNAVAILABLE            = 100 + ERR_PAYPAL_BASE; //Message number 00100: Display normal ‘system is not available’ message. IB's not available.
	public final static int ERR_PAYPAL_HOST_QOUTE_EXPIRY_TIMEOUT = 111 + ERR_PAYPAL_BASE;//Quote Expiry Timeout
	public final static int ERR_PAYPAL_REQUEST_NEW_QUOTE 		 = 112 + ERR_PAYPAL_BASE;//Please request a new quote by clicking on Get Quote. You have changed the amount to withdraw and must get a new quote
	public final static int ERR_PAYPAL_INVALID_SESSION 			 = 113 + ERR_PAYPAL_BASE;//The session is invalid. Please return to Online Banking and repeat the process.
	public final static int ERR_PAYPAL_INVALID_TOKEN 			 = 114 + ERR_PAYPAL_BASE;//PP Token was invalid?
	public final static int ERR_PAYPAL_INVALID_PAYPAL_EMAIL 	 = 116 + ERR_PAYPAL_BASE;//Your PayPal login address does not match the primary PayPal email adress you provided during registration.
	public final static int ERR_PAYPAL_ASYNCH_TIMEOUT_RETRY      = 117 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_ACSYNCH_TIMEOUT_DO_NOT_RETRY  = 118 + ERR_PAYPAL_BASE;
	
	public final static int ERR_PAYPAL_IB_VALIDATION_MIN		 = 101 + ERR_PAYPAL_BASE;	//101 - 198 The error message will repeat 10 times
	public final static int ERR_PAYPAL_TOPUP_QOUTE_MAX		     = 102 + ERR_PAYPAL_BASE;	//101 - 198 The error message will repeat 10 times
	public final static int ERR_PAYPAL_IB_VALIDATION_MAX		 = 198 + ERR_PAYPAL_BASE;	//101 - 198 The error message will repeat 10 times
	public final static int ERR_PAYPAL_SYSTEM_NOT_AVAILABLE		 = 199 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_DUPLICATE_PPA_ACCOUNT     = 96206 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_ASYNCH_TIMEOUT            = 99051 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_ACCOUNT_SUSPENDED         = 96114 + ERR_PAYPAL_BASE;
	
	//Additional PayPal/Hogan Payment Engine Errors (last minute, as usual...) - 
	public final static int ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_1  = 8572 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_2  = 17745 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_3  = 50749 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_4  = 50756 + ERR_PAYPAL_BASE;
	
	public final static int ERR_PAYPAL_PAY_ENGINE_MIN_AMOUNT_ALLOWED  = 5078 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_MAX_AMOUNT_ALLOWED  = 5079 + ERR_PAYPAL_BASE;
	
	public final static int ERR_PAYPAL_PAY_ENGINE_INSUFFICIENT_FUNDS_1 = 5144 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_INSUFFICIENT_FUNDS_2 = 47005 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_INSUFFICIENT_FUNDS_3 = 50670 + ERR_PAYPAL_BASE;
	
	//Multi Currency Account Transfers
	public final static int ERR_MCA_TRANSFERS_BASE                                    = 9000;
	public final static int ERR_MCA_TRANSFERS_INVALID_CHARACTER_IN_FROM_ACC_REFERENCE = 180 + ERR_MCA_TRANSFERS_BASE;

	//Pay Way error codes.
	public final static int ERR_PAY_WALLET_BASE = 4050;
	public final static int ERR_PAY_WALLET_INVALID_CONTRACT = ERR_PAY_WALLET_BASE + 1;
	public final static int ERR_PAY_WALLET_NO_CONTRACTS = ERR_PAY_WALLET_BASE + 2;
	public final static int ERR_PAY_WALLET_RESTORE_DELETE_FAILED = ERR_PAY_WALLET_BASE + 3;
	public final static int ERR_PAY_WALLET_RETRIEVE_DELETED_FAILED = ERR_PAY_WALLET_BASE + 4;
	public final static int ERR_PAY_WALLET_DATABASE_DELETE_FAILED = ERR_PAY_WALLET_BASE + 5;
	public final static int ERR_PAY_WALLET_RETRIEVE_ACTIVE = ERR_PAY_WALLET_BASE + 6;
	public final static int ERR_PAY_WALLET_HOST_DETAILS = ERR_PAY_WALLET_BASE + 7;
	public final static int ERR_PAY_WALLET_DUPLICATE = ERR_PAY_WALLET_BASE + 8;
	public final static int ERR_PAY_WALLET_IMS_TIMEOUT = ERR_PAY_WALLET_BASE + 9;
	public final static int ERR_PAY_WALLET_UNABLE_TO_RETRIEVE_DETAILS = ERR_PAY_WALLET_BASE + 10;
	public final static int ERR_PAY_WALLET_UNABLE_TO_RETRIEVE_CONTRACTS = ERR_PAY_WALLET_BASE + 11;

	//Share Switching Error Codes.
	//public final static int ERR_SHARE_SWITCH_BASE = 8050;
	public final static int ERR_SHARE_SWITCH_BASE = 0;
	public final static int ERR_SHARE_00143 = 143 + ERR_SHARE_SWITCH_BASE;
	public final static int ERR_SHARE_05114 = 5114 + ERR_SHARE_SWITCH_BASE;
	//public final static int ERR_SHARE_05138 = ERR_SHARE_SWITCH_BASE + 5138 ;
	public final static int ERR_SHARE_05115 = 5115 + ERR_SHARE_SWITCH_BASE;
	
	public final static int ERR_SHARE_05186 = 5186 + ERR_SHARE_SWITCH_BASE;
	public final static int ERR_SHARE_14323 = 14323 + ERR_SHARE_SWITCH_BASE;
	
	public final static int ERR_SHARE_05155 = 5155 + ERR_SHARE_SWITCH_BASE;
	
	public final static int ERR_SHARE_COMPOSITE_1 = 5155 + ERR_SHARE_SWITCH_BASE;
	public final static int ERR_SHARE_COMPOSITE_2 = 5155 + ERR_SHARE_SWITCH_BASE;
	public final static int ERR_SHARE_COMPOSITE_3 = 5155 + ERR_SHARE_SWITCH_BASE;
	public final static int ERR_SHARE_COMPOSITE_4 = 5155 + ERR_SHARE_SWITCH_BASE;
	public final static int ERR_SHARE_COMPOSITE_5 = 5155 + ERR_SHARE_SWITCH_BASE;
	public final static int ERR_SHARE_COMPOSITE_6 = 5155 + ERR_SHARE_SWITCH_BASE;
	public final static int ERR_SHARE_COMPOSITE_7 = 5155 + ERR_SHARE_SWITCH_BASE;
	
	
	//Multicurrency error codes
	//public final static int ERR_MULTICURRENCY_BASE = 5050;
	public final static int ERR_MULTICURRENCY_INVALID_ACCOUNT = 181;
	
	
	//user validation - generic message.
	public static final int ERR_SYSTEM_UNAVAILABLE = 90909;
	public static final int ERR_USR_NOT_FOUND_GENERIC = 90910; //this is a generic message when user is not found.
	public static final int ERR_USER_ACC_LIMIT_EXCEEDED = 99012;
	public static final int ERR_FOREX_SYSTEM_UNAVAILABLE = 99051;
	public static final int ERR_REQUEST_INCOMPLETE = 107052;
	public static final int ERR_COMPLEX_USER_LOGON = 90909999;
	
	
	
	

	/*
	 * Channel Standin ErrorCode
	 */
	public static final int ERR_CHANNELSTANDIN_LOGON_NOT_ALLOWED = 1999;
	public static final int ERR_CHANNELSTANDIN_DIGICODE = 1998;
		
	/*
	 * SSO ERROR codes
	 */
	public final static int SSO_SUCCESS = 0;
	public final static int SSO_INVALID_CHANNEL_LOGON_DETAILS = 1;
	public final static int SSO_MESSAGE_TYPE_NOT_VALID_FOR_CHANNEL = 2;
	public final static int SSO_INVALID_PORT_USED_BY_CHANNEL = 3;
	public final static int SSO_INVALID_LOGONID = 4;
	public final static int SSO_INVALID_LOGON_PASSWORD = 5;
	public final static int SSO_AUTHENTICATION_ERROR = 6;
	public final static int SSO_DUPLICATE_USER_ID = 7;
	public final static int SSO_PASSWORD_MATCHED_HISTORY = 8;
	public final static int SSO_UNSUPPORTED_MESSAGE = 9;
	public final static int SSO_USER_NOT_FOUND = 10;
	public final static int SSO_INVALID_PIN_NUMBER = 11;
	public final static int SSO_INVALID_ACCESS_NUMBER = 12;
	public final static int SSO_INVALID_LOOKUP_DETAILS = 13;
	public final static int SSO_ROLE_ALLREADY_EXISTS = 14;
	public final static int SSO_INVALID_PIN_COUNT_EXCEEDED = 15;
	public final static int SSO_INVALID_MESSAGE_LAYOUT = 16;
	public final static int SSO_INVALID_DESTINATION = 17;
	public final static int SSO_ROLES_DOES_NOT_EXIST = 18;
	public final static int SSO_HIGHER_SECURITY_ROLE_EXISTS = 19;
	public final static int SSO_DIGICODE_INVALID = 20;
	public final static int SSO_DIGICODE_NOT_NECESSARY = 21;
	public final static int SSO_DIGITAG_SYNCH_FAILED = 22;
	public final static int SSO_DIGITAG_UNLOCK_FAILED = 23;
	public final static int SSO_USER_DISABLED = 24;
	public final static int SSO_DUPLICATE_USER_ID_ID_NUMBER_COMBO = 25;
	public final static int SSO_UNDEFINED_ERROR_CODE = 8888;
	public final static int SSO_INVALID_DIGITAG_SERIAL_NUMBER = 26;
	public final static int SSO_PASSWORD_TO_SHORT = 27;
	public final static int SSO_PASSWORD_TO_LONG = 28;
	public final static int SSO_PASSWORD_SAME_AS_USERNAME = 29;
	public final static int SSO_PASSWORD_SIMILAR_TO_NAME = 30;
	public final static int SSO_PASSWORD_REPEATING_CHARACTERS = 31;
	public final static int SSO_PASSWORD_INVALID_CHARACTERS = 32;
	public final static int SSO_NO_ENCODER_FOUND_FOR_CHANNEL = 33;
	public final static int SSO_ERR_DECODING_PASSWORD = 34;
	public final static int SSO_PASSWORD_INVALID_ENCODING = 35;
	public final static int SSO_PASSWORD_SIMILAR_TO_USERNAME = 36;
	public final static int SSO_USERNAME_LENGTH_ERROR = 37;
	public final static int SSO_USERNAME_INVALID_CHARACTERS = 38;
	public final static int SSO_PASSWORD_MUST_CONTAIN_ATLEAST_ONE_UPPERCASE_LOWERCASE_NUMERIC_SPECIAL_CHARACTER = 40;
	public final static int SSO_TEMPORARY_PASSWORD_EXPIRED = 41;
	
	public static final int ERR_SYSTEM_NOT_AVLBL = 99051;
	public static final int ERR_DOWNLOAD_FORMAT  = 9032; //FORMAT TYPE NOT CURRENTLY SUPPORTED
	public static final int ERR_DOWNLOAD_LATER = 9033;
	
	public final static int ERR_INVALID_DATE = 102;
	
	public static final int ERR_PROCESS_FAILED = 99052;
	
	public static final int ERR_CAPTCHA_CODE = 107052;
	
	public static final int ERR_SCHEDULED_PREPAID_EXPIRY_DATE = 107053; //Scheduled Prepaid
	
	/*
	 * PKI ERROR CODES
	 */
	public final static int PKI_ERR_BASE = 110000;
	public final static int PKI_ERR_PASSCODE_MISMATCH = PKI_ERR_BASE + 1;
	public final static int PKI_ERR_AUTHCODE_MISSING =  PKI_ERR_BASE + 2;
	public final static int PKI_ERR_REFCODE_MISSING =   PKI_ERR_BASE + 3;
	public final static int PKI_ERR_PASSCODES_SENT =   PKI_ERR_BASE + 4;
	public final static int PKI_ERR_PASSCODES_NOT_SENT =   PKI_ERR_BASE + 5;
	
	/*
	 * DFM Error Codes.
	 */
	public final static int DFM_CLOSURE_INCORRECT_TIME = 4800;
	
	//TPFM Error Codes
	public final static int TPFM_DB_ERROR = 4801;//Debash: change this
	
	// no historical statements available.
	public static final int NO_DOCUMENT_FOUND = 110006;
	/*
	 * Yodlee error codes
	 */
	public final static int ERR_TWENTY2SEVEN_1 = 912;
	public final static int ERR_TWENTY2SEVEN_2 = 913;
	
	/*
	 * AutoPayment Enhancements Error Codes
	 */
	public final static int  AUTOPAYMENT_FIXED_AMOUNT_EXCEED_ALLOWED_PRODUCT_AMOUNT_LIMIT= 5662;
	
	// invalid billing account error code
	public final static int  ERR_INVALID_BILLING_ACCOUNT = 110007;	//Your profile has been blocked. Your system administrator must provide a new billing account.  Please call Online Assistance for support.
	
	//Insurance Error Codes
	public final static int ERR_INSURANCE_FNB_LIFE = 110020;
	public final static int ERR_INSURANCE_ONLINE_LIFE = 110021;
	public final static int ERR_INSURANCE_VEHICLE_AND_HOME = 110022;
	public final static int ERR_INSURANCE_FNB_LIFE_AND_ONLINE_LIFE = 110023;
	public final static int ERR_INSURANCE_FNB_LIFE_CAL_POLICY = 110024;
	
	//Forex Inwards
	public final static int ERR_FOREX_INWARDS_OFFSHORE_ACCOUNTS = 110030;
	
	//Wesbank - DR
	public final static int NO_STATEMENT_AVAIL_WES = 13333; //No statement available
	public final static int WES_STATEMENT_LIST_UNAVAILABLE = 13332; // Wesbank Statement History List unavailable
	public final static int WES_SETTLEMENT_QUOTE_UNAVAILABLE = 13331; // Wesbank Settlement Quote unavailable
	public final static int WES_STATEMENT_DELIVERY_OPTION_UNAVAILABLE = 13334; // Wesbank Statement Delivery Option unavailable
	
	public final static int MERCHANT_SERVICES_DELINK_ERROR = 4100;

	//Hogan Error Codes
	public final static int ERR_HGN_STAND_IN = 6024 ; //6024_1 System temporarily unavailable.  Please try again later. 
	
	//SmartDevices
	public static final int MOBILE_DEVICE_TECHNICAL_ERROR = 4200;
	public static final int MOBILE_DEVICE_RETRIEVE_FAILED = 4201;
	public static final int ERR_INVESTMENT_REDIRECT_OPTION = 5248;

	//Wesbank Error Message Codes
	public static final int WESBANK_TOYOTA_LEASE_ERROR = 82484349;
	public static final int WESBANK_TOYOTA_INSTALMENT_ERROR = 82484346;
	public static final int WESBANK_LEXUS_LEASE_ERROR = 82484094;
	public static final int WESBANK_LEXUS_INSTALMENT_ERROR = 82484098;
	
	// IPH/SSA Acount opener Error Message Codes
	public static final int IPH_USER_NOT_FOUND= 100;
	public static final int IPH_TOKEN_VALIDATION_FAILED = 200;
	public static final int IPH_FUNDING_BATCH_FAILED = 300;
	public static final int IPH_RECURRING_BATCH_FAILED = 400;
	public static final int IPH_INVALID_BATCH_EXECUTION_TIME = 500;
	public static final int IPH_MISSING_PAYLOAD_DATA = 600;
	
	/**
	 * This constructor should <b>only</b> be used in cases where the object pool
	 * cannot be used.
	 * HyphenError zero-argument constructor.
	 */
	public HyphenError() {
		super();
		errorMessages = null;
		errorMessage = null;
	}

	/**
	 * @see mammoth.performance.pool.ObjectPoolItem
	 */
	public void clear() {
		errorCode = 0; // supplied error code
		screenId = 0; // screen id of screen originating error
		subStrArray = null; // array of substitution strings for error message
		codeType = 1; // error code type/class
		errorMessages = null;
		errorMessage = null;
	}

	/**
	 * This method retrieves the code type.
	 *
	 * Creation date: (6/27/00 4:22:34 PM)
	 * @return int
	 */
	public int getCodeType() {
		return codeType;
	}
	
	/**
	 * This method retrieves the error code.
	 *
	 * Creation date: (6/27/00 3:08:24 PM)
	 * @return int
	 */
	public int getErrorCode() {
		return errorCode;
	}
	/**
	 * This method retrieves the screen id.
	 *
	 * Creation date: (6/27/00 3:09:33 PM)
	 * @return int
	 */
	public int getScreenId() {
		return screenId;
	}
	/**
	 * This method retrieves the substitution
	 * string array.
	 *
	 * Creation date: (6/27/00 3:12:01 PM)
	 * @return java.lang.String
	 */
	public String[] getSubStrArray() {
		return subStrArray;
	}
	/**
	 * 
	 * @return boolean
	 */
	public boolean isPooled() {
		return pooled;
	}
	/**
	 * This method sets the code type.
	 *
	 * Creation date: (6/29/00 10:19:06 AM)
	 * @param newCodeType int
	 */
	public void setCodeType(int newCodeType) {
		codeType = newCodeType;
	}
	/**
	 * 
	 * 
	 * @param newDetailMessage java.lang.String
	 */
	public void setErrorMessages(HashMap<String, String> newDetailMessage) {
		errorMessages = newDetailMessage;
	}
	/**
	 * This method sets the error code.
	 *
	 * Creation date: (6/29/00 10:19:42 AM)
	 * @param newErrorCode int
	 */
	public void setErrorCode(int newErrorCode) {
		errorCode = newErrorCode;
	}
	/**
	 * 
	 * @param newPooled boolean
	 */
	public void setPooled(boolean newPooled) {
		pooled = newPooled;
	}
	/**
	 * This method sets the screen id.
	 *
	 * Creation date: (6/29/00 10:20:27 AM)
	 * @param newScreenId int
	 */
	public void setScreenId(int newScreenId) {
		screenId = newScreenId;
	}
	/**
	 * This method sets the substitution string array.
	 *
	 * Creation date: (6/29/00 10:22:16 AM)
	 * @param newSubStrArray int
	 */
	public void setSubStrArray(String[] newSubStrArray) {
		subStrArray = newSubStrArray;
	}
	/**
	 * Returns true if Controller must dump for this error.
	 * @return int
	 */
	public boolean isLoggable() {
		return (severity > 0);
	}

	/**
	 * Sets the severity.
	 * @param severity The severity to set
	 */
	public void setSeverity(int severity) {
		this.severity = severity;
	}
	/**
	 * @return
	 */
	public String getDetailMessage() {
		return detailMessage;
	}

	/**
	 * @param string
	 */
	public void setDetailMessage(String string) {
		detailMessage = string;
	}
	
	public static int mapSSOError(int ssoError) {
		int result = ssoError;
		switch (ssoError) {
			case SSO_SUCCESS:
				result = SUCCESS;
				break;
			case SSO_PASSWORD_INVALID_CHARACTERS : 
				result = ERR_PASSWORD_HAS_NON_ALPHA; 
				break;
			case SSO_PASSWORD_REPEATING_CHARACTERS : 
				result = ERR_PASSWORD_HAS_DUP_CHARS; 
				break;
			case SSO_PASSWORD_SAME_AS_USERNAME : 
				result = ERR_PASSWORD_ID_SAME; 
				break;
			case SSO_PASSWORD_SIMILAR_TO_NAME : 
				result = ERR_PASSWORD_NAME_SAME; 
				break;
			case SSO_PASSWORD_TO_LONG : 
				result = ERR_PASSWORD_LENGTH; 
				break;
			case SSO_PASSWORD_TO_SHORT : 
				result = ERR_PASSWORD_LENGTH; 
				break;
			case SSO_USERNAME_LENGTH_ERROR : 
				result = ERR_USERNAME_LENGTH_ERROR; 
				break;
			case SSO_USERNAME_INVALID_CHARACTERS : 
				result = ERR_USERNAME_INVALID_CHARACTERS; 
				break;
			case SSO_PASSWORD_MUST_CONTAIN_ATLEAST_ONE_UPPERCASE_LOWERCASE_NUMERIC_SPECIAL_CHARACTER : 
				result = ERR_PASSWORD_MUST_CONTAIN_ATLEAST_ONE_UPPERCASE_LOWERCASE_NUMERIC_SPECIAL_CHARACTER; 
				break;			
		}
		return result;
	}	
	
	/*
	 * public final static int ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_1  = 8572 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_2  = 17745 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_3  = 50749 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_4  = 50756 + ERR_PAYPAL_BASE;
	
	public final static int ERR_PAYPAL_PAY_ENGINE_MIN_AMOUNT_ALLOWED  = 5078 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_MAX_AMOUNT_ALLOWED  = 5079 + ERR_PAYPAL_BASE;
	
	public final static int ERR_PAYPAL_PAY_ENGINE_INSUFFICIENT_FUNDS_1 = 5144 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_INSUFFICIENT_FUNDS_2 = 47005 + ERR_PAYPAL_BASE;
	public final static int ERR_PAYPAL_PAY_ENGINE_INSUFFICIENT_FUNDS_3 = 50670 + ERR_PAYPAL_BASE;
	 */
	
	public static boolean isListedPayPalError(int paypalError) {
		switch(paypalError){
			case ERR_PAYPAL_ACCOUNT_UNVERIFIED:
			case ERR_PAYPAL_HOST_OVER_LIMIT:
			case ERR_PAYPAL_HOST_INSUFFICIENT_FUNDS:
			case ERR_PAYPAL_HOST_INVALID_PAYPAL_EMAIL:
			case ERR_PAYPAL_SOLE_PROPRIETOR:
			case ERR_PAYPAL_PASSPORT_NUMBER:
			case ERR_PAYPAL_INVALID_ID:
			case ERR_PAYPAL_NON_ZA_RESIDENT:
			case ERR_PAYPAL_INCORRECT_PAYPAL_EMAIL:
			case ERR_PAYPAL_CUSTOMER_NOT_KYCD:
			case ERR_PAYPAL_TRUSTS_NOT_ALLOWED:
			case ERR_PAYPAL_NO_NPO_COMPANY_REG:
			case ERR_PAYPAL_IB_UNAVAILABLE:
			case ERR_PAYPAL_HOST_QOUTE_EXPIRY_TIMEOUT:
			case ERR_PAYPAL_REQUEST_NEW_QUOTE: 
			case ERR_PAYPAL_INVALID_SESSION:
			case ERR_PAYPAL_INVALID_TOKEN:
			case ERR_PAYPAL_INVALID_PAYPAL_EMAIL:
			case ERR_PAYPAL_ACSYNCH_TIMEOUT_DO_NOT_RETRY:
			case ERR_PAYPAL_ASYNCH_TIMEOUT_RETRY:
			case ERR_PAYPAL_SYSTEM_NOT_AVAILABLE:
			case ERR_PAYPAL_DUPLICATE_PPA_ACCOUNT:
			case ERR_PAYPAL_ASYNCH_TIMEOUT:
			case ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_1:
			case ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_2:
			case ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_3:
			case ERR_PAYPAL_PAY_ENGINE_UNAVAILABLE_4:
				
			case ERR_PAYPAL_PAY_ENGINE_MIN_AMOUNT_ALLOWED:
			case ERR_PAYPAL_PAY_ENGINE_MAX_AMOUNT_ALLOWED:
				
			case ERR_PAYPAL_PAY_ENGINE_INSUFFICIENT_FUNDS_1:
			case ERR_PAYPAL_PAY_ENGINE_INSUFFICIENT_FUNDS_2:
			case ERR_PAYPAL_PAY_ENGINE_INSUFFICIENT_FUNDS_3:	
			case ERR_PAYPAL_ACCOUNT_SUSPENDED:
				return true;
		}		
		return false;		
	}
	public HashMap<String, String> getErrorMessages() {
		return errorMessages;
	}
}
