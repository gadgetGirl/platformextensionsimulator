package fnb.online.bifrost.server.bean;

import java.util.Collection;
import java.util.Vector;

import fnb.online.bifrost.server.paging.Pageable;
import fnb.online.tags.beans.AbstractViewBean;

public abstract class ViewBean<T> extends AbstractViewBean implements Pageable
{
	
	private String key = null;
	private String defaultSortingMethod = null;
	private boolean reverseSort = false;
	private boolean pagingOff = false;
	private Collection<T> items = new Vector<T>();
	
	public void setItems(Collection<T> collection) {
		items = collection;
	}
	
	public Collection getItems() {
		return items;
	}
	
	/**
	 * The key stored in the ViewBean is the key to find the ResultConatiner object in the Transaction object
	 * This is needed for all sortable and pagable result sets and the paging and sorting jsp tags read it from here
	 * @param k	The key to find the ResultContainer object that was used to create this ViewBean
	 */
	public void setKey(String k) {
		key = k;
	}
	
	/**
	 * Returns the key to the ResultContainer holding this ViewBean
	 */
	public String getKey() {
		return key;
	}

	public String getDefaultSortingMethod() {
		return defaultSortingMethod;
	}

	public void setDefaultSortingMethod(String defaultSortingMethod) {
		this.defaultSortingMethod = defaultSortingMethod;
	}

	public boolean isReverseSort() {
		return reverseSort;
	}

	public void setReverseSort(boolean reverseSort) {
		this.reverseSort = reverseSort;
	}

	public boolean isPagingOff() {
		return pagingOff;
	}

	public void setPagingOff(boolean pagingOff) {
		this.pagingOff = pagingOff;
	}
	
	

}
