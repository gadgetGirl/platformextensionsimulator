package za.co.fnb.hawkeye.utils;

import java.net.ServerSocket;
import java.net.Socket;

public class SocketUtil {
	public static String getPrintableLocalAddress(Socket socket) {
		return( socket.getInetAddress().getHostName() + ":" + socket.getLocalPort() );
	}

	public static String getPrintableLocalAddress(ServerSocket socket) {
		return( socket.getInetAddress().getHostName() + ":" + socket.getLocalPort() );
	}

	public static void close(Socket socket) {
		try {
			if( socket != null ) {
				socket.close();
			}
		}
		catch(Exception e) {}
		socket = null;
	}
}
