package za.co.fnb.hawkeye.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class LLLLSocket {
	private DataOutputStream dos;
	private DataInputStream dis;
	
	public LLLLSocket(Socket socket) throws IOException {
		this.dos = new DataOutputStream( socket.getOutputStream() );
		this.dis = new DataInputStream( socket.getInputStream() );
	}

	public String read() throws IOException {
		int ll = this.dis.readInt();
		byte[] data = new byte[ ll ];
		this.dis.readFully( data );
		String message =  new String( data );
		return( message );
	}

	public void write(String message) throws IOException {
		byte[] data = message.getBytes();
		this.dos.writeInt( data.length );
		this.dos.write( data );
	}
}
