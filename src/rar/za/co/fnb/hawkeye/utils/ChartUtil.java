package za.co.fnb.hawkeye.utils;

import java.util.Iterator;

import org.apache.log4j.Logger;

import za.co.fnb.hawkeye.config.ChartConfig;
import za.co.fnb.pe.framework.template.elements.ChartLabels;
import za.co.fnb.pe.framework.template.elements.XLabel;
import za.co.fnb.pe.framework.template.elements.YLabel;

public class ChartUtil {
	private static final Logger LOG = Logger.getLogger(ChartUtil.class);

	public static void cleanChartXLabels(ChartLabels chartLabels, String deviceHeight) {
		ChartConfig config = ChartConfig.getChartConfig(deviceHeight);

		// If we need not skip any labels, just return.
		if (config.getAmountOfXLabelsToSkip() == 0 || chartLabels.getXlabels().size() % config.getAmountOfXLabelsToSkip() != 0 || chartLabels.getXlabels().size() / 2 <= config.getAmountOfXLabelsToSkip()) {
			LOG.debug("Not removing any xlables.");
			return;
		}

		LOG.debug("Going to skip every [" + config.getAmountOfXLabelsToSkip() + "] xlabels.");

		int counter = 1;
		Iterator<XLabel> x = chartLabels.getXlabels().iterator();
		while (x.hasNext()) {
			x.next();
			if (counter == config.getAmountOfXLabelsToSkip()) {
				x.remove();
				counter = 1;
			}
			else {
				counter++;
			}
		}
	}

	public static void cleanChartYLabels(ChartLabels chartLabels, String resolution) {
		ChartConfig config = ChartConfig.getChartConfig(resolution);

		// If we need not skip any labels, just return.
		if (config.getAmountOfYLabelsToSkip() == 0 || chartLabels.getYlabels().size() % config.getAmountOfYLabelsToSkip() != 0 || chartLabels.getYlabels().size() / 2 <= config.getAmountOfYLabelsToSkip()) {
			LOG.debug("Not removing any ylables.");
			return;
		}

		LOG.debug("Going to skip every [" + config.getAmountOfYLabelsToSkip() + "] ylabels.");

		int counter = 1;
		Iterator<YLabel> y = chartLabels.getYlabels().iterator();
		while (y.hasNext()) {
			y.next();
			if (counter == config.getAmountOfYLabelsToSkip()) {
				y.remove();
				counter = 1;
			}
			else {
				counter++;
			}
		}
	}

	public static void cleanAllChartLabels(ChartLabels chartLabels, String resolution) {
		cleanChartXLabels(chartLabels, resolution);
		cleanChartYLabels(chartLabels, resolution);
	}

}
