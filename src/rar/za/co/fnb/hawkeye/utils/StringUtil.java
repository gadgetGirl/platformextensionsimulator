package za.co.fnb.hawkeye.utils;

public class StringUtil {
	/**
	 * Replaces a variable of the format ${variable} in a specified string
	 * 
	 * @param str - String containing a variable to replace
	 * @param variable - variable name to replace
	 * @param value - value to replace variablewith
	 * @return - The original String with the variable replaced.
	 */	
	public static String replaceVar(String str, String variable, String value) {
		if( value == null ) {
			value = "";
		}
		String fullVar = "\\$\\{" + variable + "\\}";
		if (value != null
				&& (value.indexOf('\\') >= 0 || value.indexOf('$') >= 0)) {
			fullVar = "${" + variable + "}";

			if (str == null) {
				
				return null;
			}

			int i = str.indexOf(fullVar);
			if (i < 0) {
				return str;
			}

			int len = fullVar.length();
			return str.substring(0, i) + value + str.substring(i + len);
		} else {
			return str != null ? str.replaceAll(fullVar, value) : null;
		}
	}
}
