package za.co.fnb.hawkeye.jca;

import java.io.Serializable;

import javax.resource.ResourceException;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.InvalidPropertyException;
import javax.resource.spi.ResourceAdapter;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.utils.toString;

@SuppressWarnings("serial")
public class DigitalSimulatorActivationSpec implements ActivationSpec, Serializable {
	private static final Logger LOG = Logger.getLogger( DigitalSimulatorActivationSpec.class );
	
	private DigitalSimulatorResourceAdapter ra = null;

	public DigitalSimulatorActivationSpec() {
		LOG.info( "DigitalSimulatorActivationSpec()" );
	}

	public ResourceAdapter getResourceAdapter() {
		LOG.info( "getResourceAdapter()" );
		return ra;
	}

	public void setResourceAdapter(ResourceAdapter resourceAdapter) throws ResourceException {
		LOG.info( "setResourceAdapter(resourceAdapter[" + resourceAdapter + "])" );
		this.ra = (DigitalSimulatorResourceAdapter)resourceAdapter;
	}

	public void validate() throws InvalidPropertyException {
		LOG.info( "validate()" );
	}

	boolean accepts(String recipientAddress) throws InvalidPropertyException {
		LOG.info( "accepts(recipientAddress[" + recipientAddress + "])" );
		return( true ); // accept anything
	}

	@Override
	public String toString() {
		return( new toString( this ).toString() );
	}

	@Override
	public boolean equals(Object other) {
		LOG.info( "equals(other[" + other + "])" );
		if( other instanceof DigitalSimulatorActivationSpec ) 
			return( true );		
		return( false );
	}
	
	@Override
	public int hashCode() {
		LOG.info( "hashCode()" );
		return( super.hashCode() );
	}
}
