package za.co.fnb.hawkeye.jca;

public interface SimulatorMessageEndpoint {
	public void onMessage() throws Exception;
}
