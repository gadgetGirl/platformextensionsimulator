package za.co.fnb.hawkeye.jca;

import java.util.ArrayList;
import java.util.List;

import javax.resource.ResourceException;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.BootstrapContext;
import javax.resource.spi.ResourceAdapter;
import javax.resource.spi.ResourceAdapterInternalException;
import javax.resource.spi.endpoint.MessageEndpointFactory;
import javax.resource.spi.work.Work;
import javax.resource.spi.work.WorkManager;
import javax.transaction.xa.XAResource;

import org.apache.log4j.Logger;

import za.co.fnb.hawkeye.simulators.FesterSimulator;
import za.co.fnb.hawkeye.simulators.GomezSimulator;
import za.co.fnb.pe.framework.config.ActionListener;
import za.co.fnb.pe.framework.config.SimulatorConfig;
import za.co.fnb.pe.framework.utils.toString;

public class DigitalSimulatorResourceAdapter implements ResourceAdapter, ActionListener {
	private static final Logger LOG = Logger.getLogger( DigitalSimulatorResourceAdapter.class );

	private WorkManager workManager;
	private List<Work> simulators;
	private boolean started = false;
	
	public DigitalSimulatorResourceAdapter() {
		LOG.info( "DigitalSimulatorResourceAdapter()" );
	}

	public void start(BootstrapContext ctx) throws ResourceAdapterInternalException {
		LOG.info( "start(ctx[" + ctx + "])" );
    	this.workManager = ctx.getWorkManager();
    	this.simulators = new ArrayList<Work>();
		SimulatorConfig.getInstance().addListener( this );
		
		// auto-start the simulator
		try {
			this.start();
			SimulatorConfig.getInstance().getConfig().setStarted( true );
		}
		catch(Exception e) {
			LOG.error( "Failed to auto start the simulator!" );
		}
	}

	@Override
	public void start() {
		if( this.started ) {
			LOG.warn( "ALREADY STARTED" );
			return;
		}
    	try {    		
			try {
				// start gomez simulator
				Work gomezSimulator = new GomezSimulator();
				this.workManager.startWork( gomezSimulator );
				this.simulators.add( gomezSimulator );
			}
			catch(Exception e) {
				LOG.error( "Failed to initialize GOMEZ simulator!", e );
				throw new ResourceAdapterInternalException( "Failed to initialize GOMEZ Simulator!", e );
			}
	
			try {
				// start fester simulator
				Work festerSimulator = new FesterSimulator();				
				this.workManager.startWork( festerSimulator );
				this.simulators.add( festerSimulator );
				
			}
			catch(Exception e) {
				LOG.error( "Failed to initialize FESTER simulator!", e );
				throw new ResourceAdapterInternalException( "Failed to initialize FESTER Simulator!", e );
			}
			
			this.started = true;
    	}
    	catch(Exception e) {
    		// if we have an exception I just need to stop any already started simulators
    		this.stop();
    	}
	}
	
	public void stop() {
		LOG.info( "stop()" );
		if( this.simulators != null ) {
			for( Work item : this.simulators ) {
				item.release();
			}
		}
		this.started = false;
	}

	public void endpointActivation(MessageEndpointFactory messageEndpointFactory, ActivationSpec activationSpec) throws  ResourceException {
		LOG.info( "endpointActivation(messageEndpointFactory[" + messageEndpointFactory + "], activationSpec[" + activationSpec + "])" );
	}

	public void endpointDeactivation(MessageEndpointFactory endpointFactory, ActivationSpec spec) {
		LOG.info( "endpointDeactivation(endpointFactory[" + endpointFactory + "], spec[" + spec + "])" );
		stop();
	}

	public XAResource[] getXAResources(ActivationSpec[] arg0) throws ResourceException {
		LOG.info( "getXAResources()" ); 
		return( new XAResource[0] ); // XA is unsupported
	}
	
	@Override
	public String toString() {
		return( new toString( this ).toString() );
	}

	@Override
	public boolean equals(Object other) {
		LOG.info( "equals(other[" + other + "])" );
		if( other instanceof DigitalSimulatorResourceAdapter ) 
			return( true );		
		return( false );
	}
	
	@Override
	public int hashCode() {
		LOG.info( "hashCode()" );
		return( super.hashCode() );
	}
}
