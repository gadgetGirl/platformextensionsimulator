package za.co.fnb.hawkeye.simulators;

import java.net.Socket;

import org.apache.log4j.Logger;

import za.co.fnb.hawkeye.config.ConfigConstants;
import za.co.fnb.hawkeye.config.ConfigFactory;
import za.co.fnb.hawkeye.utils.LLLLSocket;
import za.co.fnb.pe.framework.config.IPUtil;
import za.co.fnb.pe.framework.config.SimulatorConfig;

public class GomezClientConnectionHandler implements Runnable {
	private static final Logger LOG = Logger.getLogger( GomezClientConnectionHandler.class );

	private Socket client;
	 
	public GomezClientConnectionHandler(Socket client) {
		LOG.debug( "GomezClientConnectionHandler(client[" + client + "])" );
		this.client = client;
	}
	
	public void run() {
		LOG.debug( "Gomez Simulator Client Connection Handler[" + client + "]..." );
		try {
			// get redirect ip and port settings
			String festerIP = IPUtil.getSelectedIP( SimulatorConfig.getInstance().getConfig().getFesterIP() );
			int festerPort = ConfigFactory.getConfiguration().getInt( ConfigConstants.CONFIG_FESTER_PORT );
			
			// create a client helper wrapper to that I can read and send messages
			// nice and easily
			LLLLSocket helper = new LLLLSocket( client );
			String request = helper.read();			
			LOG.info( "GOMEZ Request[" + request + "]" );
			String response = "<response code=\"0\" message=\"\" type=\"fester.connection.details\" >" + 
					 			"<fester host=\"" + festerIP + "\" port=\"" + festerPort + "\" />" + 
					 		  "</response>";
			LOG.info( "GOMEZ Response[" + response + "]" );
			helper.write( response );
		}
		catch(Throwable e) {
			LOG.error( "Failed to respond to client!", e );
		}
		finally {
			if( client != null ) {
				try {
					client.close();
				}
				catch(Throwable e) {}
			}
			client = null;
		}	
	}
}
