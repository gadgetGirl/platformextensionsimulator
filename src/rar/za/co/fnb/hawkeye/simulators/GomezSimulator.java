package za.co.fnb.hawkeye.simulators;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.resource.spi.work.Work;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;

import za.co.fnb.hawkeye.config.ConfigConstants;
import za.co.fnb.hawkeye.config.ConfigFactory;
import za.co.fnb.hawkeye.utils.SocketUtil;
import za.co.fnb.pe.framework.config.IPUtil;
import za.co.fnb.pe.framework.config.SimulatorConfig;
import za.co.fnb.pe.framework.utils.SocketFactory;

public class GomezSimulator implements Work {
	private static final Logger LOG = Logger.getLogger( GomezSimulator.class );
	
	private ServerSocket server;
	private ExecutorService threadPool;
	
	public GomezSimulator() throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException, CertificateException, IOException, ConfigurationException {
		LOG.debug( "GomezSimulator()" );
		String bindIP = IPUtil.getSelectedIP( SimulatorConfig.getInstance().getConfig().getGomezIP() );
		int bindPort = ConfigFactory.getConfiguration().getInt( ConfigConstants.CONFIG_GOMEZ_PORT );		
		String keystore = ConfigFactory.getConfiguration().getString( ConfigConstants.CONFIG_SSL_KEYSTORE );
		String password = ConfigFactory.getConfiguration().getString( ConfigConstants.CONFIG_SSL_KEYSTORE_PASSWORD );
		
		LOG.debug( "Starting Gomez Simulator Server using config host[" + bindIP + "], port[" + bindPort + "], keystore[" + keystore + "], password[" + password + "]" );		
		this.server = SocketFactory.createSocketServer( bindIP, bindPort, keystore, password );
		this.threadPool = Executors.newFixedThreadPool( 10 );
	}
	
	@Override
	public void run() {
		LOG.debug( "run()" );
		Thread.currentThread().setName( "Gomez Simulator[" + SocketUtil.getPrintableLocalAddress( this.server ) + "]" );
		LOG.info( "Gomez Simulator started and accepting clients..." );
		try { 
			while( this.isConnected() ) {
				LOG.debug( "Waiting for client connection..." );
				Socket client = this.server.accept();
				
				LOG.debug( "Client connection received, handing of to be processed..." );
				this.threadPool.execute( new GomezClientConnectionHandler( client ) );
			}
		}
		catch(Exception e) {
			LOG.error( "Unexpected error while servicing clients!", e );
		}
		LOG.debug( "Bye Bye Gomez Simulator!" );
		this.shutdown();		
	}

	@Override
	public void release() {
		LOG.debug( "release()" );	
		this.shutdown();
	}
	
	public void shutdown() {
		LOG.debug( "shutting down gomez simulator..." );
		if( this.server != null ) {
			try {
				this.server.close();
			}
			catch(Exception e) {}		
		}
		this.server = null;
	}

	private boolean isConnected() {
		return( this.server != null && !this.server.isClosed() );
	}	
}
