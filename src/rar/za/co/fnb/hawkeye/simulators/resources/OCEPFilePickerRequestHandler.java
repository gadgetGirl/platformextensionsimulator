package za.co.fnb.hawkeye.simulators.resources;

import java.util.Map;

import za.co.fnb.hawkeye.config.ConfigConstants;
import za.co.fnb.pe.framework.config.ConfigFactory;
import za.co.fnb.pe.framework.template.elements.Upload;
import za.co.fnb.pe.framework.template.phone.FilePickerViewTemplate;
import za.co.fnb.pe.framework.template.phone.OCEPFilePickerViewTemplate;

public class OCEPFilePickerRequestHandler {

	public static FilePickerViewTemplate getView(Map<String, Object> cache, OCEPFilePickerViewTemplate ocepTemplate) throws Exception {
		FilePickerViewTemplate template = new FilePickerViewTemplate();

		// Map the UI stuff straight as there are no changes here.
		template.setId(ocepTemplate.getId());
		template.setRealUIID(ocepTemplate.getRealUIID());
		template.setTabsOn(ocepTemplate.getTabsOn());
		template.setPersistance(ocepTemplate.getPersistance());

		// Set the template name
		template.setTemplate("ui.templates.file.picker.view");

		// Map the Accept and Cancel buttons straight as there are no changes here.
		template.setAccept(ocepTemplate.getAccept());
		template.setCancel(ocepTemplate.getCancel());

		// Map the upload stuff we got from OCEP
		Upload upload = new Upload();
		upload.setParent(ocepTemplate.getUpload().getParent());
		upload.setKey(ocepTemplate.getUpload().getKey());
		upload.setMediaType(ocepTemplate.getUpload().getMediaType());
		upload.setCompletedUiid(ocepTemplate.getUpload().getCompletedUiid());
		upload.setMaxSize(ocepTemplate.getUpload().getMaxSize());
		cache.put("UploadCompletedUiid",ocepTemplate.getUpload().getCompletedUiid());
		cache.put("UploadCompletedUiid","key");

		// NOW, map our defaults
		String hostConfig = ConfigFactory.getConfiguration().getString(ConfigConstants.CONFIG_THING_MESSAGING_MEDIA_HOST);
		upload.setIp(hostConfig.split("/")[0]);
		upload.setPort(hostConfig.split("/")[1]);

		upload.setUsername("");
		upload.setPassword("");

		upload.setMaxHeight("1024");
		upload.setMaxWidth("1024");

		upload.setCompletedUiid("ui.bifrost.upload.controller");
		upload.setErrorUiid("ui.bifrost.upload.error");
		upload.setMaxSizeUiid("ui.bifrost.upload.document.too.large");

		template.setUpload(upload);

		return template;
	}

}
