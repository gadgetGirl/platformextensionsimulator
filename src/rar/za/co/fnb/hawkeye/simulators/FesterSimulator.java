package za.co.fnb.hawkeye.simulators;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.resource.spi.work.Work;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import za.co.fnb.hawkeye.config.ConfigConstants;
import za.co.fnb.hawkeye.config.ConfigFactory;
import za.co.fnb.hawkeye.simulators.app.controllers.BifrostController;
import za.co.fnb.hawkeye.simulators.app.models.BifrostModel;
import za.co.fnb.hawkeye.utils.SocketUtil;
import za.co.fnb.pe.framework.UIControllerRequestHandler;
import za.co.fnb.pe.framework.UIFramework;
import za.co.fnb.pe.framework.UIModelRequestHandler;
import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIModel;
import za.co.fnb.pe.framework.config.BankingAppConfigurationParser;
import za.co.fnb.pe.framework.config.BankingAppUIConfiguration;
import za.co.fnb.pe.framework.config.IPUtil;
import za.co.fnb.pe.framework.config.SimulatorConfig;
import za.co.fnb.pe.framework.utils.SocketFactory;

public class FesterSimulator implements Work {
	private static final Logger LOG = Logger.getLogger(FesterSimulator.class);

	private ServerSocket server;
	private ExecutorService threadPool;
	private UIFramework framework;

	public FesterSimulator() throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException, CertificateException, IOException, ConfigurationException, SAXException, ParserConfigurationException,
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		LOG.info("FesterSimulator()");
		String bindIP = IPUtil.getSelectedIP(SimulatorConfig.getInstance().getConfig().getGomezIP());
		int bindPort = ConfigFactory.getConfiguration().getInt(ConfigConstants.CONFIG_FESTER_PORT);
		String keystore = ConfigFactory.getConfiguration().getString(ConfigConstants.CONFIG_SSL_KEYSTORE);
		String password = ConfigFactory.getConfiguration().getString(ConfigConstants.CONFIG_SSL_KEYSTORE_PASSWORD);
		LOG.debug("Starting Fester Simulator Server using config host[" + bindIP + "], port[" + bindPort + "], keystore[" + keystore + "], password[" + password + "]");
		this.server = SocketFactory.createSocketServer(bindIP, bindPort, keystore, password);
		this.threadPool = Executors.newFixedThreadPool(10);

		LOG.info("XML Configuration Loaded");
		String configurationXML = this.loadXMLConfiguration();

		// parse XML configuration
		BankingAppConfigurationParser parser = new BankingAppConfigurationParser();
		BankingAppUIConfiguration configuration = parser.parse(configurationXML);

		// inject extension handling model and controller
		UIModel model = new BifrostModel();
		UIModelRequestHandler modelHandler = new UIModelRequestHandler("bifrost.model", model, false);
		String modelRouting = SimulatorConfig.getInstance().getConfig().getExtensionModelRouting();
		String modelID = modelRouting.substring(0, modelRouting.length() - 1);
		configuration.getStarModels().put(modelID, modelHandler);

		UIController controller = new BifrostController();
		String uiRouting = SimulatorConfig.getInstance().getConfig().getExtensionUIRouting();
		UIControllerRequestHandler controllerHandler = new UIControllerRequestHandler(controller, uiRouting, false);
		controllerHandler.getMappings().put("success", "ui.bifrost.view");
		controllerHandler.getMappings().put("failed", "ui.bifrost.error.view");
		controllerHandler.getMappings().put("tabletfailed", "ui.bifrost.tablet.error.view");
		controllerHandler.getMappings().put("redirect", "ui.bifrost.redirect.controller");
		String controllerID = uiRouting.substring(0, uiRouting.length() - 1);
		configuration.getStarControllers().put(controllerID, controllerHandler);

		// UIModel resource = new BifrostModel();
		// UIModelRequestHandler resourceHandler = new UIModelRequestHandler( resource );
		// String resourceRouting =
		// SimulatorConfig.getInstance().getConfig().getExtensionResourceRouting();
		// String resourceID = resourceRouting.substring( 0, resourceRouting.length() - 1 );
		// configuration.getStarModels().put( resourceID, resourceHandler );

		// initialize UI frameowrk
		this.framework = new UIFramework();
		this.framework.initialize(configuration);
	}

	@Override
	public void run() {
		LOG.info("run()");
		Thread.currentThread().setName("Fester Simulator[" + SocketUtil.getPrintableLocalAddress(this.server) + "]");

		// Set the MOBI properties for GOMEZ
		System.getProperties().setProperty("gomez.host", "127.0.0.1");
		System.getProperties().setProperty("gomez.port", "36400");
		System.getProperties().setProperty("server.http.connector.resolver", "WILDFLY");

		LOG.info("Fester Simulator started and accepting clients...");
		try {
			while (this.isConnected()) {
				LOG.info("Waiting for client connection...");
				Socket client = this.server.accept();

				LOG.info("Client connection received, handing of to be processed...");
				this.threadPool.execute(new FesterClientConnectionHandler(this.framework, client));
			}
		}
		catch (Exception e) {
			LOG.error("Unexpected error while servicing clients!", e);
		}
		LOG.info("Fester Simulator!");
		this.shutdown();
	}

	@Override
	public void release() {
		LOG.info("release()");
		this.shutdown();
	}

	public void shutdown() {
		LOG.info("shutting down fester simulator...");
		if (this.server != null) {
			try {
				this.server.close();
			}
			catch (Exception e) {
			}
		}
		this.server = null;
	}

	private boolean isConnected() {
		return this.server != null && !this.server.isClosed();
	}

	private String loadXMLConfiguration() throws SAXException, IOException {
		DataInputStream dis = new DataInputStream(this.getClass().getClassLoader().getResourceAsStream("ui-config-all.xml"));
		byte[] data = new byte[dis.available()];
		dis.readFully(data);
		dis.close();
		dis = null;
		String configuration = new String(data);
		return configuration;
	}
}
