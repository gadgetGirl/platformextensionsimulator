package za.co.fnb.hawkeye.simulators.utils;


public class UIExtensionFramework {
//	private static final Logger LOG = Logger.getLogger( UIExtensionFramework.class );
//	
//	public static void handleUIRequest(Map<String, Object> sessionCache, Request request, Response response, String extensionURL) {
//		LOG.info( "Handling UI Request[" + extensionURL + "]" );
//		try {
//			String url = extensionURL;
//			url += "/ui/" + request.getKey().getId();
//			LOG.info( "Requesting URL[" + url + "]" );
//			
//			Map<String, String> form = new HashMap<String, String>();
//			form.put( "requestxml", request.toString() );
//			
//			POSTResponse postResponse = RESTUtil.POST( url, form, (String)sessionCache.get( "COOKIE" ) );
//			sessionCache.put( "COOKIE", postResponse.getCookies() );
//			
//			String responseXML = postResponse.getResponse();
//			LOG.debug( "Platform Extension Response[" + response + "]" );
//			
//			String responseUI = null;
//			if( responseXML.contains( "<ui" ) ) {
//				responseUI = responseXML.substring( responseXML.indexOf( "<ui" ) );
//				responseUI = responseUI.replaceAll( "</response>", "" );
//			}
//			else if( responseXML.contains( "<dialog" ) ) {
//				responseUI = responseXML.substring( responseXML.indexOf( "<dialog" ) );
//				responseUI = responseUI.replaceAll( "</response>", "" );
//			}
//			
//			// create XML UI wrapper
//			ExtensionViewTemplate view = new ExtensionViewTemplate();
//			view.setXML( responseUI );
//			
//			// add to response
//			response.getEntities().add( view );
//		}
//		catch(Exception e) {
//			LOG.error( "Failed to obtain requested view!", e );
//		}
//	}
//
//	public static void handleModelRequest(Map<String, Object> sessionCache, Request request, Response response, String extensionURL) {
//		LOG.info( "Handling Model Request[" + extensionURL + "]" );
//		try {
//			String url = extensionURL;			
//			url += "/data/" + request.getType();
//			LOG.info( "Requesting URL[" + url + "]" );
//			
//			Map<String, String> form = new HashMap<String, String>();
//			form.put( "requestxml", request.toString() );
//			
//			POSTResponse postResponse = RESTUtil.POST( url, form, (String)sessionCache.get( "COOKIE" ) );
//			sessionCache.put( "COOKIE", postResponse.getCookies() );
//
//			String responseXML = postResponse.getResponse();
//			LOG.info( "Platform Extension Response[" + response + "]" );
//			
//			String responseData = responseXML.substring( responseXML.indexOf( ">" ) + 1 );
//			responseData = responseData.replaceAll( "</response>", "" );
//			
//			// add the response to a special bifrost entity handler
//			response.getEntities().add( new PlatformExtensionData( responseData ) );
//		}
//		catch(Exception e) {
//			LOG.error( "Failed to obtain requested view!", e );
//		}
//	}
//
//	public static void handleResourceRequest(Map<String, Object> sessionCache, Request request, Response response, String extensionURL) {
//		LOG.info( "Handling Resource Request[" + extensionURL + "]" );		
//	}
}
