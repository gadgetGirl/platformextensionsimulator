package za.co.fnb.hawkeye.simulators.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.config.ConfigFactory;
import za.co.fnb.pe.framework.config.CoreConfigConstants;
import za.co.fnb.pe.framework.entity.DevicePlatforms;
import za.co.fnb.pe.framework.entity.Response;
import za.co.fnb.pe.framework.entity.header.ExtensionRequest;
import za.co.fnb.pe.framework.template.elements.PlatformExtensionData;
import za.co.fnb.pe.framework.utils.FileUtil;


public class UIFrameworkHelper {
	private static final Logger LOG = Logger.getLogger( UIFrameworkHelper.class );

	private static String imagesDir;

	public static Response resolveLocalIcon(ExtensionRequest request) throws ConfigurationException, Exception {
		Response response = new Response();
		response.setResponseTimestamp( System.currentTimeMillis() );
		response.setCode( 0 );
		response.setMessage( "" );
		response.getEntities().add( loadIconResource( request ) );
		return response;
	}

	private static PlatformExtensionData loadIconResource(ExtensionRequest request) throws ConfigurationException, Exception {
		String skin = request.getChannel().getSkin().toLowerCase();
		String deviceType = DevicePlatforms.isTablet( request.getDevice().getPlatform() ) ? "tablet" : "phone";
		String platform = request.getDevice().getPlatform().toString().toLowerCase();
		String resourceName = request.getId();

		// resources need to be stored in the following directory structure
		// <images_base_dir>/<skin>/<type>/<platform>/<filename>.<extension>
		// ./fnb/phone/iphone/image.png
		String fullPath;
		if (!resourceName.endsWith(".png") && !resourceName.endsWith(".gif")) {
			fullPath = getResourcesDirectory() + "/" + skin + "/" + deviceType + "/" + platform + "/" + resourceName + "." + getResourceExtension();
		} else {
			fullPath = getResourcesDirectory() + "/" + skin + "/" + deviceType + "/" + platform + "/" + resourceName;
		}

		LOG.info( "Full Image Path[" + fullPath + "]" );

		// load file and base64 encode for transport
		byte[] fileContents = FileUtil.readFileContents( fullPath );
		String encoded = new String( Base64.encodeBase64( fileContents ) );

		// wrap data in object to be sent to client
		String xml = "<icon id=\"" + resourceName + "\" contents=\"" + encoded + "\" />";
		PlatformExtensionData data = new PlatformExtensionData( xml );
		return data;
	}

	private static String getResourcesDirectory() throws ConfigurationException {
		if( imagesDir == null ) {
			imagesDir = ConfigFactory.getConfiguration().getString( CoreConfigConstants.IMAGES_BASE_DIRECTORY, "./images" );
		}
		return imagesDir;
	}

	private static String getResourceExtension() {
		return "png";
	}
}
