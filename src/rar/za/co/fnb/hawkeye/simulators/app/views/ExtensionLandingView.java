package za.co.fnb.hawkeye.simulators.app.views;

import za.co.fnb.hawkeye.utils.StringUtil;
import za.co.fnb.pe.framework.api.UIViewContext;
import za.co.fnb.pe.framework.config.SimulatorConfig;
import za.co.fnb.pe.framework.template.elements.Comms;
import za.co.fnb.pe.framework.template.phone.CommsViewTemplate;
import za.co.fnb.pe.framework.util.views.CommsView;

public class ExtensionLandingView extends CommsView {

	@Override
	public void processMessage(UIViewContext context) {
		// configure extension comms item
		Comms extension = new Comms();
		extension.setParent( "me" );
		extension.setIconid( "ui.comms.get.started" );
		extension.setUiid( SimulatorConfig.getInstance().getConfig().getExtensionStartUIID() );
		extension.setText( "comms" );

		CommsViewTemplate ui = (CommsViewTemplate)context.getUI();
		ui.getComms().add( extension );
		
		// now that I have added extension comms button 
		// process view
		super.processMessage( context );
		
		// finally replace ${text} variable with description
		extension.setText( StringUtil.replaceVar( extension.getText(), "text", SimulatorConfig.getInstance().getConfig().getExtensionDescription() ) );
	}
}
