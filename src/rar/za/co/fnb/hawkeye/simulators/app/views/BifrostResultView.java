package za.co.fnb.hawkeye.simulators.app.views;

import za.co.fnb.pe.framework.api.UIView;
import za.co.fnb.pe.framework.api.UIViewContext;
import za.co.fnb.pe.framework.template.phone.ExtensionViewTemplate;

public class BifrostResultView implements UIView {

	@Override
	public void processMessage(UIViewContext context) {
		ExtensionViewTemplate view = (ExtensionViewTemplate)context.getUI();
		view.setXML( (String)context.getClipboard().get( "bifrost_result_ui" ) );
	}

	@Override
	public void postProcessMessage(UIViewContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setupFireBaseEvent(UIViewContext viewContext) {
		
	}
}
