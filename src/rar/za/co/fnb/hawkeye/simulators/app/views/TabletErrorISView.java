package za.co.fnb.hawkeye.simulators.app.views;

import java.util.List;
import java.util.Map;

import za.co.fnb.hawkeye.utils.StringUtil;
import za.co.fnb.pe.framework.api.UIView;
import za.co.fnb.pe.framework.api.UIViewContext;
import za.co.fnb.pe.framework.entity.Control;
import za.co.fnb.pe.framework.entity.Layout;
import za.co.fnb.pe.framework.template.elements.HtmlLabel;
import za.co.fnb.pe.framework.template.tablet.TabletInstantServiceViewTemplate;
import za.co.fnb.pe.framework.utils.UIMessageConfig;

public class TabletErrorISView implements UIView {
	@Override
	public void processMessage(UIViewContext context) {
		
		TabletInstantServiceViewTemplate view = (TabletInstantServiceViewTemplate)context.getUI();
		
		view.getTitle().setText( this.formatContent( view.getTitle().getText(), context ) );
		
		Layout layout = view.getLayout();
		List<Control> controls = layout.getControls();
		for (Control control : controls) {
			if(control instanceof HtmlLabel){
				HtmlLabel label = (HtmlLabel)control;
				if(label.getKey().equalsIgnoreCase("error")){
					label.setText(this.formatContent( label.getText(), context ));
					label.setText( UIMessageConfig.getMessage( context.getRequest().getChannel().getSkin(), context.getRequest().getDevice().getPlatform().toString(), label.getText() ) );
					label.setText( this.formatContent( label.getText(), context ) );
				}
			}
		}
	
	}

	protected String formatContent(String html, UIViewContext context) {
		for( String key : context.getClipboard().keySet() ) {
			html = StringUtil.replaceVar( html, key, getString( context.getClipboard(), key ) );
		}
		return( html );
	}

	private String getString(Map<String, Object> clipboard, String key) {
		Object o = clipboard.get( key );
		if( o != null && o instanceof String ) {
			return( (String)o );
		}
		else if( o != null ) {
			return( o.toString() );
		}
		return( null );
	}

	@Override
	public void postProcessMessage(UIViewContext context) {
		
	}

	@Override
	public void setupFireBaseEvent(UIViewContext viewContext) {
		
	}
}
