package za.co.fnb.hawkeye.simulators.app.controllers;

import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIControllerContext;
import za.co.fnb.pe.framework.config.BifrostUIParser;
import za.co.fnb.pe.framework.entity.ParsedExtensionResponse;
import za.co.fnb.pe.framework.template.elements.PlatformExtensionData;
import za.co.fnb.pe.framework.utils.UICssConfig;

public class UIStartController implements UIController {
	@Override
	public String processMessage(UIControllerContext context) {
		String skin = context.getRequest().getChannel().getSkin().toLowerCase();
		String devicePlatform = context.getRequest().getDevice().getPlatform().toString();
		
//		// add CSS based on the skin and platform
//		context.getData().add( UICssConfig.getStyleSheetSetting( skin.toLowerCase(), devicePlatform.toLowerCase() ) );
		
		// add CSS based on the skin and platform
		try {
			ParsedExtensionResponse paresedRepsonse = new BifrostUIParser().parse(UICssConfig.getStyleSheetSetting( skin.toLowerCase(), devicePlatform.toLowerCase() ).toString());
			context.getData().add( (PlatformExtensionData)paresedRepsonse.getRepsonseEntities().get(0) );
		} catch (Exception e) {
		} 

		return( CONTINUE );
	}

}
