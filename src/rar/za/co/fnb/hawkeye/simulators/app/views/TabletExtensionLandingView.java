package za.co.fnb.hawkeye.simulators.app.views;

import za.co.fnb.pe.framework.api.UIView;
import za.co.fnb.pe.framework.api.UIViewContext;
import za.co.fnb.pe.framework.config.SimulatorConfig;
import za.co.fnb.pe.framework.entity.Control;
import za.co.fnb.pe.framework.template.elements.Group;
import za.co.fnb.pe.framework.template.elements.SelectItem;
import za.co.fnb.pe.framework.template.tablet.TabletInstantServiceViewTemplate;

public class TabletExtensionLandingView implements UIView {

	@Override
	public void processMessage(UIViewContext context) {
		SelectItem extension = new SelectItem();
		extension.setKey("sim");
		extension.setText(SimulatorConfig.getInstance().getConfig().getExtensionDescription());
		extension.setUiid( SimulatorConfig.getInstance().getConfig().getExtensionStartUIID() );
		extension.setCoordinate("0,0");
		extension.setValign("center");
		extension.setHalign("center");
		extension.setRspan("1");
		extension.setCspan("1");
		

		TabletInstantServiceViewTemplate ui = (TabletInstantServiceViewTemplate)context.getUI();
		for (Control control : ui.getLayout().getControls()) {
			if(control instanceof Group){
				Group group = (Group) control;
				group.getLayout().getControls().add(extension);
				break;
			}
		}
	}

	@Override
	public void postProcessMessage(UIViewContext context) {
		
	}

	@Override
	public void setupFireBaseEvent(UIViewContext viewContext) {
		
	}
}
