package za.co.fnb.hawkeye.simulators.app.views;

import java.util.List;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.api.UIView;
import za.co.fnb.pe.framework.api.UIViewContext;
import za.co.fnb.pe.framework.template.elements.ControlType;
import za.co.fnb.pe.framework.template.elements.FormControl;
import za.co.fnb.pe.framework.template.phone.PopupViewTemplate;
import za.co.fnb.pe.framework.utils.UIMessageConfig;

public class PopupView implements UIView {
	private static final Logger LOG = Logger.getLogger(PopupView.class);

	@Override
	public void processMessage(UIViewContext context) {
		PopupViewTemplate view = (PopupViewTemplate) context.getUI();
		if (view.getContent().getText() != null) {
			view.getContent().setText(UIMessageConfig.getMessage(context.getRequest().getChannel().getSkin(), context.getRequest().getDevice().getPlatform().toString(), view.getContent().getText()));
			view.getContent().setText(this.formatContent(view.getContent().getText(), context));
		}
		
		List<FormControl> controls = view.getControls();
		for (FormControl control : controls) {
			try {
				if (control.getType().equals(ControlType.LABEL)) {
					String labelId = control.getLabel();
					String html = UIMessageConfig.getMessage(context.getRequest().getChannel().getSkin(), context.getRequest().getDevice().getPlatform().toString(), labelId);
					control.setLabel(html);
				}
			}
			catch (Exception exception) {
				LOG.error("unable to set html content for " + control.getType().name(), exception);
			}
		}
	}

	protected String formatContent(String html, UIViewContext context) {
		return (html);
	}

	@Override
	public void postProcessMessage(UIViewContext context) {
		
	}

	@Override
	public void setupFireBaseEvent(UIViewContext viewContext) {
		
	}
}