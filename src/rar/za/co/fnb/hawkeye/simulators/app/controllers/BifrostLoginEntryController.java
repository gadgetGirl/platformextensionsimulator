package za.co.fnb.hawkeye.simulators.app.controllers;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIControllerContext;

public class BifrostLoginEntryController implements UIController {
	private static final Logger LOG = Logger.getLogger(BifrostLoginEntryController.class);
	@Override
	public String processMessage(UIControllerContext context) {
		LOG.info("ENTERING THE LOGON FLOW...");
		LOG.info("SETTING CUSTOMER AS AUTENTICATED...");
		context.getRequest().getCustomer().setAuthenticated(true);
		LOG.info("CUSTOMER LOGGED ON :-)");
		return CONTINUE;
	}
}
