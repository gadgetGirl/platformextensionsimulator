package za.co.fnb.hawkeye.simulators.app.views;

import java.io.DataInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.api.UIView;
import za.co.fnb.pe.framework.api.UIViewContext;
import za.co.fnb.pe.framework.template.tablet.XMLViewTemplate;

public class XMLConfigFileView implements UIView {
	private static final Logger LOG = Logger.getLogger( XMLConfigFileView.class );
	
	@Override
	public void processMessage(UIViewContext context) {
		try {
			InputStream is = this.getClass().getClassLoader().getResourceAsStream( "ui-start.xml" );
			DataInputStream dis = new DataInputStream( is );
			byte[] data = new byte[ dis.available() ];
			dis.readFully( data );
			String xml = new String( data );
			
			XMLViewTemplate view = (XMLViewTemplate)context.getUI();
			view.setXML( xml );
		}
		catch(Exception e) {
			LOG.error( "Failed to populate XML in response!", e );
		}
	}

	@Override
	public void postProcessMessage(UIViewContext context) {
		
	}

	@Override
	public void setupFireBaseEvent(UIViewContext viewContext) {
		
	}
}
