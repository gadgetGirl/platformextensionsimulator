package za.co.fnb.hawkeye.simulators.app.controllers;

import za.co.fnb.pe.framework.api.UIControllerContext;
import za.co.fnb.pe.framework.api.UIController;

public class DoNothingController implements UIController {
	@Override
	public String processMessage(UIControllerContext context) {
		return( CONTINUE );
	}
}
