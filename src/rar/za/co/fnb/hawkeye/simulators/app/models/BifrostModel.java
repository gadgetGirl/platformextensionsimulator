package za.co.fnb.hawkeye.simulators.app.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import za.co.fnb.hawkeye.utils.ChartUtil;
import za.co.fnb.pe.framework.api.UIModel;
import za.co.fnb.pe.framework.api.UIModelContext;
import za.co.fnb.pe.framework.api.UIModelResponse;
import za.co.fnb.pe.framework.config.BifrostUIParser;
import za.co.fnb.pe.framework.config.SimulatorConfig;
import za.co.fnb.pe.framework.entity.POSTResponse;
import za.co.fnb.pe.framework.entity.ParsedExtensionResponse;
import za.co.fnb.pe.framework.entity.UIUpdate;
import za.co.fnb.pe.framework.entity.XMLEntity;
import za.co.fnb.pe.framework.entity.header.ContextCache;
import za.co.fnb.pe.framework.entity.header.RequestType;
import za.co.fnb.pe.framework.entity.mm.Add;
import za.co.fnb.pe.framework.entity.mm.Clear;
import za.co.fnb.pe.framework.entity.mm.Delete;
import za.co.fnb.pe.framework.entity.mm.SymbolUpdate;
import za.co.fnb.pe.framework.entity.mm.Update;
import za.co.fnb.pe.framework.template.elements.FormControl;
import za.co.fnb.pe.framework.template.elements.OverviewPanel;
import za.co.fnb.pe.framework.template.elements.OverviewPanelType;
import za.co.fnb.pe.framework.template.elements.PlatformExtensionAction;
import za.co.fnb.pe.framework.template.elements.PlatformExtensionData;
import za.co.fnb.pe.framework.utils.ExtensionUtil;
import za.co.fnb.pe.framework.utils.RESTUtil;

public class BifrostModel implements UIModel {
	private static final Logger LOG = Logger.getLogger(BifrostModel.class);

	@Override
	public UIModelResponse processMessage(UIModelContext context) {
		LOG.debug("Bifrost Model");
		try {
			String url = SimulatorConfig.getInstance().getConfig().getExtensionURL();
			url += "/" + RequestType.DATA + "/" + context.getRequest().getType();
			LOG.debug("Requesting URL[" + url + "]");

			Map<String, String> form = new HashMap<String, String>();

			context.getRequest().setContextCache((ContextCache) context.getSessionCache().get("contextcache"));

			Gson gson = new Gson();

			form.put("header", gson.toJson(context.getRequest()));

			POSTResponse response = RESTUtil.POST(url, form, (String) context.getSessionCache().get("cookie"));
			context.getSessionCache().put("cookie", response.getCookies());
			LOG.debug("Platform Extension Response[" + response.getResponse() + "]");

			ParsedExtensionResponse paresedRepsonse = new ParsedExtensionResponse();
			try {
				paresedRepsonse = new BifrostUIParser().parse(response.getResponse());// parse the response into an object
			}
			catch (Exception e) {
				LOG.error("Could not process UI from Extension server", e);
			}

			context.getSessionCache().put("contextcache", paresedRepsonse.getContextCache());

			List<XMLEntity> repsonseEntities = paresedRepsonse.getRepsonseEntities();

			ExtensionUtil.validateParsedEntites(repsonseEntities);

			for (XMLEntity entity : repsonseEntities) {
				if (entity instanceof PlatformExtensionData) {
					PlatformExtensionData data = (PlatformExtensionData) entity;
					ExtensionUtil.handleXMLEntity(context, data);
					context.getData().add(data);
				}
				else if (entity instanceof PlatformExtensionAction) {
					PlatformExtensionAction action = (PlatformExtensionAction) entity;
					ExtensionUtil.handleXMLEntity(context, action);
					context.getModelUIActions().add(action);
				}
				else if (entity instanceof UIUpdate) {
					UIUpdate update = (UIUpdate) entity;
					for (XMLEntity updateEntity : update.getEntities()) {
						// If FormControl in the Update
						if (updateEntity instanceof FormControl) {
							ExtensionUtil.handleXMLEntity(context, updateEntity);
						}
						else if (updateEntity instanceof OverviewPanel) {
							OverviewPanel panel = (OverviewPanel) updateEntity;
							if (panel.getType() == OverviewPanelType.LINE_CHART) {
								LOG.debug("Found a Line chart, cleaning the labels...");
								ChartUtil.cleanAllChartLabels(panel.getLabels(), context.getRequest().getDevice().getResolution());
							}
						}
					}
					context.getUpdate().add(update);
				}
				else if (entity instanceof SymbolUpdate) {
					SymbolUpdate symbolUpdate = (SymbolUpdate) entity;
					for (Update update : symbolUpdate.getUpdate()) {
						ExtensionUtil.handleXMLEntity(context, update);
					}
					for (Add add : symbolUpdate.getAdd()) {
						ExtensionUtil.handleXMLEntity(context, add);
					}
					for (Delete delete : symbolUpdate.getDelete()) {
						ExtensionUtil.handleXMLEntity(context, delete);
					}
					for (Clear clear : symbolUpdate.getClear()) {
						ExtensionUtil.handleXMLEntity(context, clear);
					}
					context.getSymbolUpdate().add(symbolUpdate);
				}
			}

			return new UIModelResponse(paresedRepsonse.getResponseCode(), paresedRepsonse.getResponseMessage());
		}
		catch (Exception e) {
			LOG.error("Failed to get requested UI from extended platform!", e);
			return SERVICE_UNAVAILABLE;
		}
	}
}
