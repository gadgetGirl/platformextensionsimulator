package za.co.fnb.hawkeye.simulators.app.controllers;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIControllerContext;
import za.co.fnb.pe.framework.entity.Redirect;

public class BifrostRedirectController implements UIController {
	private static final Logger LOG = Logger.getLogger(BifrostRedirectController.class);

	@Override
	public String processMessage(UIControllerContext context) {
		String uiid =  ((Redirect) context.getSessionCache().get("redirect")).getUiid();
		LOG.info("Redirecting to uiid[" + uiid + "]");

		/*
		 * This does not require a mapping, as this is a LITERAL. We will be navigating to this UIID directly.
		 */
		return uiid;
	}
}
