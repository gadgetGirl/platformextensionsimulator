package za.co.fnb.hawkeye.simulators.app.controllers;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIControllerContext;
import za.co.fnb.pe.framework.entity.Redirect;
import za.co.fnb.pe.framework.entity.RedirectFormValue;
import za.co.fnb.pe.framework.exceptions.ui.SystemCurrentlyUnavailableException;

public class BifrostLinkingEntryController implements UIController {
	private static final Logger LOG = Logger.getLogger(BifrostLinkingEntryController.class);

	@Override
	public String processMessage(UIControllerContext context) {
		Redirect redirect = (Redirect) context.getSessionCache().get("redirect");
		String username = "";
		String firstName = "";

		try {
			// Grab the form values from the request
			for (RedirectFormValue form : redirect.getForm()) {
				if (form.getKey().equalsIgnoreCase("username")) {
					username = form.getValue();
				}
				else if (form.getKey().equalsIgnoreCase("firstname")) {
					firstName = form.getValue();
				}

			}

			LOG.debug("Username[" + username + "], firstName[" + firstName + "] for OCEP App Pairing.");

			if (username.isEmpty() || firstName.isEmpty()) {
				throw new SystemCurrentlyUnavailableException();
			}

			// Add FirstName to clipboard
			context.getClipboard().put("name", firstName);

			return CONTINUE;
		}
		catch (Exception e) {
			LOG.error("Failed to get the username from the redirect's form values!", e);
			context.getClipboard().put("error", e);
			return ERROR;
		}
	}
}
