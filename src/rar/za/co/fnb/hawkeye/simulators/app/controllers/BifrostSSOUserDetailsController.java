package za.co.fnb.hawkeye.simulators.app.controllers;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIControllerContext;
import za.co.fnb.pe.framework.entity.header.Form;

public class BifrostSSOUserDetailsController implements UIController {
	private static final Logger LOG = Logger.getLogger(BifrostSSOUserDetailsController.class);

	@Override
	public String processMessage(UIControllerContext context) {
		LOG.info("ADDING SSO USER DETAILS TO THE FORM VALUES...");
		context.getRequest().getFormValues().add(new Form("usrRef", "12345679"));
		context.getRequest().getFormValues().add(new Form("custRef", "987654321"));
		LOG.info("SUCCESSFULLY ADDED SSO USER DETAILS TO THE FORM VALUES :-)");
		return CONTINUE;
	}

}
