package za.co.fnb.hawkeye.simulators.app.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import za.co.fnb.hawkeye.config.ConfigConstants;
import za.co.fnb.hawkeye.simulators.resources.OCEPCameraRequestHandler;
import za.co.fnb.hawkeye.simulators.resources.OCEPFilePickerRequestHandler;
import za.co.fnb.hawkeye.simulators.resources.OCEPGalleryRequestHandler;
import za.co.fnb.pe.framework.UIViewRequestHandler;
import za.co.fnb.pe.framework.actions.SimDismissDialog;
import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIControllerContext;
import za.co.fnb.pe.framework.api.UIViewContext;
import za.co.fnb.pe.framework.config.BankingAppUIConfiguration;
import za.co.fnb.pe.framework.config.BifrostUIParser;
import za.co.fnb.pe.framework.config.ConfigFactory;
import za.co.fnb.pe.framework.config.ExtensionData;
import za.co.fnb.pe.framework.config.SimulatorConfig;
import za.co.fnb.pe.framework.entity.DevicePlatforms;
import za.co.fnb.pe.framework.entity.Dialog;
import za.co.fnb.pe.framework.entity.POSTResponse;
import za.co.fnb.pe.framework.entity.ParsedExtensionResponse;
import za.co.fnb.pe.framework.entity.Redirect;
import za.co.fnb.pe.framework.entity.UI;
import za.co.fnb.pe.framework.entity.UIElement;
import za.co.fnb.pe.framework.entity.XMLEntity;
import za.co.fnb.pe.framework.entity.header.ContextCache;
import za.co.fnb.pe.framework.entity.header.RequestType;
import za.co.fnb.pe.framework.entity.mm.ActionType;
import za.co.fnb.pe.framework.entity.mm.Column;
import za.co.fnb.pe.framework.entity.mm.ColumnContainer;
import za.co.fnb.pe.framework.entity.mm.Container;
import za.co.fnb.pe.framework.entity.mm.StructuredContainer;
import za.co.fnb.pe.framework.entity.mm.Symbol;
import za.co.fnb.pe.framework.entity.mm.UploadEnabledSymbol;
import za.co.fnb.pe.framework.entity.xml.menu.Menu;
import za.co.fnb.pe.framework.entity.xml.menu.MenuHeader;
import za.co.fnb.pe.framework.entity.xml.menu.MenuHeaderItem;
import za.co.fnb.pe.framework.template.elements.PlatformExtensionAction;
import za.co.fnb.pe.framework.template.elements.PlatformExtensionData;
import za.co.fnb.pe.framework.template.phone.MagicMountainViewTemplate;
import za.co.fnb.pe.framework.template.phone.OCEPCameraViewTemplate;
import za.co.fnb.pe.framework.template.phone.OCEPFilePickerViewTemplate;
import za.co.fnb.pe.framework.template.phone.OCEPGalleryViewTemplate;
import za.co.fnb.pe.framework.utils.ExtensionUtil;
import za.co.fnb.pe.framework.utils.RESTUtil;

public class BifrostController implements UIController {
	private static final Logger LOG = Logger.getLogger(BifrostController.class);

	@Override
	public String processMessage(UIControllerContext context) {
		LOG.debug("Bifrost Controller");
		try {

			Map<String, Object> sessionCache = context.getSessionCache();
			String url = SimulatorConfig.getInstance().getConfig().getExtensionURL();
			final String requestID = StringUtils.isNotBlank(context.getControllerRequest()) ? context.getControllerRequest() : context.getRequest().getId();
			if (DevicePlatforms.isTablet(context.getRequest().getDevice().getPlatform())) {
				url += "/" + RequestType.VIEW + "/" + requestID;
			}
			else {

				url += "/" + RequestType.UI + "/" + requestID;
			}

			LOG.debug("Requesting URL[" + url + "]");

			Map<String, String> form = new HashMap<String, String>();

			context.getRequest().setContextCache((ContextCache) context.getSessionCache().get("contextcache"));

			Gson gson = new Gson();

			// Mock location
			// -26.119026, 27.950565
			context.getRequest().getLocation().setLatitude(-26.119029);
			context.getRequest().getLocation().setLongitude(27.950555);

			form.put("header", gson.toJson(context.getRequest()));

			POSTResponse response = RESTUtil.POST(url, form, (String) context.getSessionCache().get("cookie"));
			context.getSessionCache().put("cookie", response.getCookies());
			LOG.debug("Platform Extension Response[" + response.getResponse() + "]");

			ParsedExtensionResponse paresedRepsonse = new ParsedExtensionResponse();
			try {
				paresedRepsonse = new BifrostUIParser().parse(response.getResponse());// parse the response into an object
			}
			catch (Exception e) {
				LOG.error("Could not process UI from Extension server", e);
			}

			context.getSessionCache().put("contextcache", paresedRepsonse.getContextCache());

			List<XMLEntity> repsonseEntities = paresedRepsonse.getRepsonseEntities();

			ExtensionUtil.validateParsedEntites(repsonseEntities);

			ExtensionData eData = (ExtensionData) context.getSessionCache().get("edata");

			if (eData == null) {
				eData = new ExtensionData();
				context.getSessionCache().put("edata", eData);
			}

			// Redirect Request
			Redirect redirectRequest = paresedRepsonse.getRedirectRequest();

			if (redirectRequest != null) {
				context.getSessionCache().put("redirect", redirectRequest);
				LOG.debug("Redirect detected! Going to BifrostRedirectController...");
				return "redirect";
			}

			String biFrostResult = "";
			Map<String, UIViewRequestHandler> extensionTemplates = ((BankingAppUIConfiguration) context.getUIConfiguration()).getExtensionTemplates();

			for (XMLEntity entity : repsonseEntities) {
				if (entity instanceof UI) {
					UI responseUI = (UI) entity;

					if (responseUI instanceof OCEPCameraViewTemplate) {
						LOG.debug("Found OCEPCameraViewTemplate doing switch around");
						OCEPCameraViewTemplate testResponse = (OCEPCameraViewTemplate) responseUI;
						responseUI = OCEPCameraRequestHandler.getView(sessionCache, testResponse);
					}
					else if (responseUI instanceof OCEPFilePickerViewTemplate) {
						LOG.debug("Found OCEPFilePickerViewTemplate doing switch around");
						OCEPFilePickerViewTemplate testResponse = (OCEPFilePickerViewTemplate) responseUI;
						responseUI = OCEPFilePickerRequestHandler.getView(sessionCache, testResponse);
					}
					else if (responseUI instanceof OCEPGalleryViewTemplate) {
						LOG.debug("Found OCEPGalleryViewTemplate doing switch around");
						OCEPGalleryViewTemplate testResponse = (OCEPGalleryViewTemplate) responseUI;
						responseUI = OCEPGalleryRequestHandler.getView(sessionCache, testResponse);
					}
					else if (responseUI instanceof MagicMountainViewTemplate) {
						MagicMountainViewTemplate view = (MagicMountainViewTemplate) responseUI;
						resolveUploadSymbol(context, view);
					}

					if (extensionTemplates.containsKey(responseUI.getTemplate())) {
						UIViewRequestHandler extentionTemplate = extensionTemplates.get(responseUI.getTemplate());
						UIViewContext viewContext = new UIViewContext(context);
						viewContext.setUI(extentionTemplate.getUI().clone());
						viewContext.setExtensionUI(responseUI.clone());

						if (responseUI.getDialog()) {

							if (!eData.getPossibleNavigationUIs().contains(responseUI.getId())) {
								eData.setDialogOpen(false);
							}

							eData.setPossibleNavigationUIs(new ArrayList<String>());
							ExtensionUtil.createAnimationsAndMakeListOfAllPossibleNavigation(context, viewContext.getExtensionUI());

							extentionTemplate.process(viewContext);

							if (!eData.isDialogOpen()) {
								Dialog dialog = ExtensionUtil.createDialog();
								dialog.getLayout().getUis().addAll(viewContext.getProcessedUIs());
								eData.setDialogOpen(true);
								biFrostResult += dialog.toString();
							}
							else {
								for (UI ui : viewContext.getProcessedUIs()) {
									biFrostResult += ui.toString();
								}
							}

						}
						else {
							if (eData.isDialogOpen()) {
								context.getActions().add(new SimDismissDialog());
							}

							eData.setDialogOpen(false);

							eData.setPossibleNavigationUIs(new ArrayList<String>());
							ExtensionUtil.createAnimationsAndMakeListOfAllPossibleNavigation(context, viewContext.getExtensionUI());

							extentionTemplate.process(viewContext);

							for (UI ui : viewContext.getProcessedUIs()) {
								biFrostResult += ui.toString();
							}
						}

					}
					else {
						biFrostResult += responseUI.toString();// if there is no extensionid, this means we must just send the UI as it was given to
																// us by the extension fester
					}

				}
				else if (entity instanceof PlatformExtensionData) {
					PlatformExtensionData uiData = (PlatformExtensionData) entity;
					ExtensionUtil.handleXMLEntity(context, uiData);
					context.getData().add(uiData);
				}
				else if (entity instanceof PlatformExtensionAction) {
					PlatformExtensionAction uiAction = (PlatformExtensionAction) entity;
					context.getActions().add(uiAction);
				}
				
				else if (entity instanceof Menu || entity instanceof MenuHeader || entity instanceof MenuHeaderItem || entity instanceof MenuHeaderItem) {
					context.getUIElements().add((UIElement) entity);
				}
			}

			context.getClipboard().put("bifrost_result_ui", biFrostResult);
			return SUCCESS;
		}
		catch (Exception e) {
			LOG.error("Failed to get requested UI from extended platform!", e);
			return FAILED;
		}
	}

	public static void resolveUploadSymbol(UIControllerContext context, MagicMountainViewTemplate view) throws ConfigurationException, IOException {
		for (Symbol symbol : view.getRootContainer().getSymbols()) {
			resolveUploadSymbol(context, symbol);
		}
	}

	private static void resolveUploadSymbol(UIControllerContext context, Symbol child) throws ConfigurationException, IOException {
		if (child instanceof Container) {
			for (Symbol s : ((Container) child).getSymbols()) {
				resolveUploadSymbol(context, s);
			}
		}
		else if (child instanceof StructuredContainer) {
			StructuredContainer sc = (StructuredContainer) child;
			resolveUploadSymbol(context, sc.getHeader());
			resolveUploadSymbol(context, sc.getBody());
			resolveUploadSymbol(context, sc.getBottom());
			resolveUploadSymbol(context, sc.getFooter());
		}
		else if (child instanceof ColumnContainer) {
			ColumnContainer cc = (ColumnContainer) child;
			for (Column column : cc.getColumns()) {
				resolveUploadSymbol(context, column);
			}
		}
		else if (child instanceof UploadEnabledSymbol) {
			Map<String, Object> cache = context.getSessionCache();
			UploadEnabledSymbol uploadSymbol = (UploadEnabledSymbol) child;

			String hostConfig = ConfigFactory.getConfiguration().getString(ConfigConstants.CONFIG_THING_MESSAGING_MEDIA_HOST);
			String maxsize = ConfigFactory.getConfiguration().getString(ConfigConstants.CONFIG_THING_MESSAGING_MAX_SIZE);
			uploadSymbol.setIp(hostConfig.split("/")[0]);
			uploadSymbol.setPort(hostConfig.split("/")[1]);
			uploadSymbol.setMaxSize(maxsize);

			// Store the uiid we need to go to once we are done
			cache.put("UploadCompletedUiid", uploadSymbol.getUiid());
			cache.put("symbolKey", uploadSymbol.getKey());

			// Override some of the values and go to our bifrost upload controller
			uploadSymbol.setType(ActionType.SUBMIT);
			uploadSymbol.setFileNamePrefix("profilepic.");
			uploadSymbol.setUiid("ui.bifrost.upload.controller");

		}
	}
}
