package za.co.fnb.hawkeye.simulators.app.controllers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.log4j.Logger;

import za.co.fnb.hawkeye.config.ConfigConstants;
import za.co.fnb.hawkeye.config.ConfigFactory;
import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIControllerContext;
import za.co.fnb.pe.framework.entity.header.Form;

public class BifrostUploadController implements UIController {
	private static final Logger LOG = Logger.getLogger(BifrostUploadController.class);

	@Override
	public String processMessage(UIControllerContext context) {
		String uiid = (String) context.getSessionCache().get("UploadCompletedUiid");
		String symbolKey = (String) context.getSessionCache().get("symbolKey");
		List<String> fileNames = new ArrayList<>();
		String scandata = "";

		// Get the documents name
		for (Form form : context.getRequest().getFormValues()) {
			if (form.getKey().startsWith(symbolKey)) {
				String[] keys = form.getKey().split(":");
				if (keys.length >= 2) {
					if (keys[1].equalsIgnoreCase("scandata")) {
						scandata = form.getValue();
					}
					else if (keys[1].startsWith("filename")) {
						fileNames.add(form.getValue());
					}
				}
				else {
					fileNames.add(form.getValue());
				}
			}
		}

		for (String fileName : fileNames) {
			try {
				// Encode to Base64 and delete the file
				File file = new File(ConfigFactory.getConfiguration().getString(ConfigConstants.CONFIG_PROFILE_PIC_IMAGES_DIR) + "/" + fileName);
				String base64 = encode(file);

				LOG.debug("Adding base64 encoded file to the formValues [" + base64 + "]");
				context.getRequest().getFormValues().add(new Form(fileName.replace("profilepic.", ""), base64));
				deleteFile(file);
			}
			catch (Exception e) {
				LOG.error("Failed to upload the file to the OCEP!", e);
				context.getClipboard().put("error", e);
				return ERROR;
			}
		}

		if (!scandata.isEmpty()) {
			context.getRequest().getFormValues().add(new Form(symbolKey + ":scandata", scandata));
		}

		/*
		 * This does not require a mapping, as this is a LITERAL. We will be navigating to this UIID directly.
		 */
		LOG.debug("Redirecting to uiid[" + uiid + "]");
		return uiid;
	}

	/**
	 * Encodes the file to base64 string
	 *
	 * @param file
	 * @return
	 * @throws Exception
	 */
	private String encode(File file) throws Exception {
		if (file.exists()) {
			int length = (int) file.length();
			BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
			byte[] bytes = new byte[length];
			reader.read(bytes, 0, length);
			reader.close();

			return new String(Base64.getEncoder().encode(bytes));
		}
		return "";
	}

	/**
	 * Deletes the file from the shared drive
	 *
	 * @param file
	 * @throws Exception
	 */
	private void deleteFile(File file) throws Exception {
		if (file.exists()) {
			LOG.debug("DELETEING FILE [" + file.getPath() + "]...");
			file.delete();
			LOG.debug("FILE DELETED!");
		}
	}
}
