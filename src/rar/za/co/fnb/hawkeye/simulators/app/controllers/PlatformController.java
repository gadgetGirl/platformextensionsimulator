package za.co.fnb.hawkeye.simulators.app.controllers;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIControllerContext;

public class PlatformController implements UIController {
	private static final Logger LOG = Logger.getLogger( PlatformController.class );
	
	@Override
	public String processMessage(UIControllerContext context) {
		String platform = context.getRequest().getDevice().getPlatform().toString().toUpperCase();
		LOG.info( "PlatformController returning[" + platform + "]" );
		return( platform );
	}
}
