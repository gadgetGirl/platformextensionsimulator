package za.co.fnb.hawkeye.simulators.app.controllers;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.api.UIController;
import za.co.fnb.pe.framework.api.UIControllerContext;
import za.co.fnb.pe.framework.entity.Redirect;
import za.co.fnb.pe.framework.entity.RedirectFormValue;
import za.co.fnb.pe.framework.entity.header.Parent;


public class BifrostRedirectReturnController implements UIController {
	private static final Logger LOG = Logger.getLogger(BifrostRedirectReturnController.class);

	@Override
	public String processMessage(UIControllerContext context) {
		String returnUIID = ((Redirect) context.getSessionCache().get("redirect")).getReturnuiid();

		// Check for a null UIID
		if (returnUIID == null || returnUIID.isEmpty()) {
			LOG.error("Return UIID is either null or blank!");
			return ERROR;
		}

		// Set the parent
		for (RedirectFormValue form : ((Redirect) context.getSessionCache().get("redirect")).getForm()) {
			if (form.getKey().equalsIgnoreCase("parent")) {
				Parent parent = new Parent();
				parent.setId(form.getValue());
				context.getRequest().setParent(parent);
				break;
			}
		}

		LOG.info("Redirect returning to uiid[" + returnUIID + "]");
		return returnUIID;
	}
}
