package za.co.fnb.hawkeye.simulators;

import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import za.co.fnb.hawkeye.simulators.utils.UIFrameworkHelper;
import za.co.fnb.hawkeye.utils.LLLLSocket;
import za.co.fnb.pe.framework.FrameworkSessionContext;
import za.co.fnb.pe.framework.UIFramework;
import za.co.fnb.pe.framework.config.CountryUtil;
import za.co.fnb.pe.framework.config.SimulatorConfig;
import za.co.fnb.pe.framework.entity.DevicePlatforms;
import za.co.fnb.pe.framework.entity.FNBChannel;
import za.co.fnb.pe.framework.entity.Request;
import za.co.fnb.pe.framework.entity.Response;
import za.co.fnb.pe.framework.entity.header.Channel;
import za.co.fnb.pe.framework.entity.header.Customer;
import za.co.fnb.pe.framework.entity.header.Device;
import za.co.fnb.pe.framework.entity.header.ExtensionRequest;
import za.co.fnb.pe.framework.entity.header.Form;
import za.co.fnb.pe.framework.entity.header.Location;
import za.co.fnb.pe.framework.entity.request.MoreData;
import za.co.fnb.pe.framework.parsers.XMLRequestParser;

public class FesterClientConnectionHandler implements Runnable {
	private static final Logger LOG = Logger.getLogger(FesterClientConnectionHandler.class);

	private UIFramework framework;
	private Socket client;
	private FrameworkSessionContext sessionContext = new FrameworkSessionContext();
	private Map<String, Object> sessionCache = new HashMap<String, Object>();

	public FesterClientConnectionHandler(UIFramework framework, Socket client) throws IOException, SAXException, ParserConfigurationException, ConfigurationException {
		LOG.debug("FesterClientConnectionHandler(client[" + client + "])");
		this.framework = framework;
		this.client = client;
	}

	@Override
	public void run() {
		LOG.debug("Connection Monitoring Multi Version Round robin distributing client[" + this.client + "]...");
		try {
			// create a client helper wrapper to that I can read and send messages
			// nice and easily and start the session off by sending welcome
			LLLLSocket helper = new LLLLSocket(this.client);
			helper.write("<welcome message=\"Welcome to FNB. How can we help you?\" systemtime=\"E61A4808F9BCBB85E15D9535DFE6F0053A3EC40A\" />");

			// loop while the client is connected
			while (this.isConnected()) {
				LOG.debug("Waiting for message from client...");
				String requestXML = helper.read();

				// handle requests or events
				if (requestXML.startsWith("<event")) {
					// handle ping's
					if (requestXML.equals("<event type=\"ping\" value=\"ping\" />")) {
						helper.write("<event type=\"ping\" />");
					}
				}
				else if (requestXML.startsWith("<request")) {
					LOG.info("FESTER Request[" + requestXML + "]");

					// generate extension header from request header and simulator saved properties
					Request request = new XMLRequestParser().createRequest(requestXML);
					ExtensionRequest header = createExtensionHeader(request);

					// process UI, Model and Resource requests
					String requestType = request.getType();
					Response uiResponse = new Response();
					LOG.info("Request Type[" + requestType + "]");
					if (requestType.equals("fester.ui.get")) {
						if (DevicePlatforms.isTablet(request.getDevice().getPlatform())) {
							uiResponse = this.framework.processViewRequest(header, this.sessionContext, this.sessionCache);
						}
						else {
							uiResponse = this.framework.processUIRequest(header, this.sessionContext, this.sessionCache);
						}
					}
					else if (requestType.equals("fester.icons.get")) {
						// first try resolve the icon from local image resources directory
						// if this does not work, then resolve the icon using the standard
						// icon model request handler that will basically try load locally
						// and then request from OCEP partner if that is required
						try {
							uiResponse = UIFrameworkHelper.resolveLocalIcon(header);
						}
						catch (Exception e) {
							LOG.debug("Failed to load icon locally, gonno try icon request handler!");
							uiResponse = this.framework.processModelRequest(header, this.sessionContext, this.sessionCache);
						}
					}
					else if (requestType.equals("fester.sounds.get")) {
						uiResponse = this.framework.processModelRequest(header, this.sessionContext, this.sessionCache);
					}
					else {
						uiResponse = this.framework.processModelRequest(header, this.sessionContext, this.sessionCache);
					}

					uiResponse.setId(request.getID());

					String response = uiResponse.toString();

					LOG.info("FESTER Response[" + response + "]");
					helper.write(response);
				}
			}
		}
		catch (EOFException e) {
			LOG.warn("Client disconncted! Session Finished");
		}
		catch (Throwable e) {
			LOG.error("Failed to respond to client!", e);
		}
		finally {
			if (this.client != null) {
				try {
					this.client.close();
				}
				catch (Throwable e) {
				}
			}
			this.client = null;
		}
	}

	private boolean isConnected() {
		return this.client != null && this.client.isConnected();
	}

	public static ExtensionRequest createExtensionHeader(Request request) {


		// channel settings
		Channel channel = new Channel();
		if (DevicePlatforms.isTablet(request.getDevice().getPlatform())) {
			channel.setChannel(FNBChannel.TABLET_APP);
		}
		else {
			channel.setChannel(FNBChannel.PHONE_APP);
		}
		channel.setVersion(request.getVersion().getMessage());
		channel.setSkin(request.getVersion().getSkin());
		channel.setVodsstandin(SimulatorConfig.getInstance().getConfig().isVodsStandIn());
		channel.setCountry(Integer.parseInt(CountryUtil.getSelectedCountry(SimulatorConfig.getInstance().getConfig().getCountryCode())));

		// customer settings
		Customer customer = new Customer();
		customer.setAuthenticated(SimulatorConfig.getInstance().getConfig().isAuthenticated());
		customer.setCompany(Integer.parseInt(CountryUtil.getSelectedCountry(SimulatorConfig.getInstance().getConfig().getCountryCode())));
		customer.setUcn(SimulatorConfig.getInstance().getConfig().getUcn());

		// device settings
		Device device = new Device();
		device.setPlatform(DevicePlatforms.derivePlatform(request.getDevice().getPlatform()));
		device.setModel(request.getDevice().getDevice());
		device.setGsm(request.getDevice().isGsmEnabled());
		device.setUniqueid(request.getDevice().getDeviceID());
		device.setHeight(request.getDevice().getHeight());
		device.setWidth(request.getDevice().getWidth());
		device.setResolution(request.getDevice().getResolution());

		// location settings
		Location location = new Location();
		location.setLatitude(request.getGPS().getLatitude());
		location.setLongitude(request.getGPS().getLongitude());
		location.setAltitude(request.getGPS().getAltitude());

		// MoreData settings
		MoreData moreData = new MoreData();
		moreData.setKey(request.getMoreData().getKey());
		moreData.setType(request.getMoreData().getType());
		moreData.setId(request.getMoreData().getId());
		moreData.setParent(request.getMoreData().getParent());

		// get form values been submitted
		List<Form> formValues = new ArrayList<Form>();
		Form configData = new Form();

		if (SimulatorConfig.getInstance().getConfig().getKey1() != null && SimulatorConfig.getInstance().getConfig().getKey1().length() > 0 && SimulatorConfig.getInstance().getConfig().getValue1() != null && SimulatorConfig.getInstance().getConfig().getValue1().length() > 0) {
			configData.setKey(SimulatorConfig.getInstance().getConfig().getKey1());
			configData.setValue(SimulatorConfig.getInstance().getConfig().getValue1());
		}

		formValues.add(configData);
		for (za.co.fnb.pe.framework.entity.request.Form form : request.getFormValues()) {
			Form f = new Form();
			f.setKey(form.getKey());
			f.setValue(form.getValue());
			formValues.add(f);
		}

		// now put together header
		ExtensionRequest header = new ExtensionRequest();
		header.getParent().setId(request.getParent().getId());
		header.setId(request.getKey().getId());
		header.setType(request.getType());
		header.setChannel(channel);
		header.setDevice(device);
		header.setLocation(location);
		header.setFormValues(formValues);
		header.setCustomer(customer);
		header.setMoreData(moreData);
		header.setSessionID(request.getID() != null ? request.getID() : "");
		header.setRequestID(UUID.randomUUID().toString());
		header.getParent().setId(request.getParent().getId());
		header.setInitiatedsymbol(request.getInitiatedSymbol());
		return header;
	}
}
