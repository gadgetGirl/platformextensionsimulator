package za.co.fnb.hawkeye.config;

import org.apache.commons.configuration.ConfigurationFactory;

public class EnvironmentConfigurationFactory extends ConfigurationFactory {
	public EnvironmentConfigurationFactory(String configurationFileName) {
		super( configurationFileName );
	}
}
