package za.co.fnb.hawkeye.config;

public class ConfigConstants {
	public static final String CONFIG_GOMEZ_PORT = "simulator.gomez.bind.port";
	
	public static final String CONFIG_FESTER_PORT = "simulator.fester.bind.port";

	public static final String CONFIG_SSL_KEYSTORE = "simulator.ssl.keystore";
	public static final String CONFIG_SSL_KEYSTORE_PASSWORD = "simulator.ssl.keystore.password";

	public static final String CONFIG_PROFILE_PIC_IMAGES_DIR = "profile.pic.images.dir";
	
	public static final String CONFIG_THING_MESSAGING_MEDIA_HOST = "thing.messaging.media.host";
	public static final String CONFIG_THING_MESSAGING_MAX_SIZE = "thing.messaging.max.size";
	public static final String CONFIG_THING_FILE_PICKER_MAX_SIZE = "thing.file.picker.max.size";
}
