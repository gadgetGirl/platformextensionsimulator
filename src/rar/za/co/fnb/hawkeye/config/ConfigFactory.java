package za.co.fnb.hawkeye.config;


import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConfigurationFactory;

import za.co.fnb.pe.framework.config.EnvironmentConfigurationDelegate;


public class ConfigFactory {
	public static final String ENVIRONMENT_SYSTEM_PROPERTY = "PLATFORM_EXTENSION_SIMULATOR_ENVIRONMENT";
	
	private static Configuration instance = null;
	private static final Object mutex = new Object();
	
	private ConfigFactory() {
	}

	public static Configuration getConfiguration() throws ConfigurationException {
		if( instance == null ) {
			synchronized( mutex ) {
				if( instance == null ) {
					String ENVIRONMENT = System.getProperty( ENVIRONMENT_SYSTEM_PROPERTY );
					if( !System.getProperties().keySet().contains( ENVIRONMENT_SYSTEM_PROPERTY ) ) {
						System.err.println( ENVIRONMENT_SYSTEM_PROPERTY + " environment variable not defined, please set to enable ENVIRONMENT specific configurations" );
						ENVIRONMENT = "UNKNOWN";
					}
					ConfigurationFactory factory = new ConfigurationFactory( "config.xml" );
					instance = new EnvironmentConfigurationDelegate( factory.getConfiguration(), ENVIRONMENT );
				}
			}
		}
		return( instance );
	}
}
