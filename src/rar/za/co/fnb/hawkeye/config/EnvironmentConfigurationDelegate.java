package za.co.fnb.hawkeye.config;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.configuration.Configuration;

public class EnvironmentConfigurationDelegate implements Configuration {
	private Configuration configuration;
	private String ENVIRONMENT;
	
	public EnvironmentConfigurationDelegate(Configuration configuration, String ENVIRONMENT) {
		this.configuration = configuration;
		this.ENVIRONMENT = ENVIRONMENT;
	}
	
	@Override
	public void addProperty(String key, Object value) {
		this.configuration.addProperty( key, value );
	}

	@Override
	public void clear() {
		this.configuration.clear();
	}

	@Override
	public void clearProperty(String key) {
		this.configuration.clearProperty( this.resolveKey( key ) );
	}

	@Override
	public boolean containsKey(String key) {
		return( this.configuration.containsKey( this.resolveKey( key ) ) );
	}

	@Override
	public BigDecimal getBigDecimal(String key) {
		return( this.configuration.getBigDecimal( this.resolveKey( key ) ) );
	}

	@Override
	public BigDecimal getBigDecimal(String key, BigDecimal defaultValue) {
		return( this.configuration.getBigDecimal( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public BigInteger getBigInteger(String key) {
		return( this.configuration.getBigInteger( this.resolveKey( key ) ) );
	}

	@Override
	public BigInteger getBigInteger(String key, BigInteger defaultValue) {
		return( this.configuration.getBigInteger( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public boolean getBoolean(String key) {
		return( this.configuration.getBoolean( this.resolveKey( key ) ) );
	}

	@Override
	public boolean getBoolean(String key, boolean defaultValue) {
		return( this.configuration.getBoolean( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public Boolean getBoolean(String key, Boolean defaultValue) {
		return( this.configuration.getBoolean( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public byte getByte(String key) {
		return( this.configuration.getByte( this.resolveKey( key ) ) );
	}

	@Override
	public byte getByte(String key, byte defaultValue) {
		return( this.configuration.getByte( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public Byte getByte(String key, Byte defaultValue) {
		return( this.configuration.getByte( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public double getDouble(String key) {
		return( this.configuration.getDouble( this.resolveKey( key ) ) );
	}

	@Override
	public double getDouble(String key, double defaultValue) {
		return( this.configuration.getDouble( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public Double getDouble(String key, Double defaultValue) {
		return( this.configuration.getDouble( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public float getFloat(String key) {
		return( this.configuration.getFloat( this.resolveKey( key ) ) );
	}

	@Override
	public float getFloat(String key, float defaultValue) {
		return( this.configuration.getFloat( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public Float getFloat(String key, Float defaultValue) {
		return( this.configuration.getFloat( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public int getInt(String key) {
		return( this.configuration.getInt( this.resolveKey( key ) ) );
	}

	@Override
	public int getInt(String key, int defaultValue) {
		return( this.configuration.getInt( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public Integer getInteger(String key, Integer defaultValue) {
		return( this.configuration.getInteger( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	@SuppressWarnings({ "rawtypes" })
	public Iterator getKeys() {
		return( this.configuration.getKeys() );
	}

	@Override
	@SuppressWarnings({ "rawtypes" })
	public Iterator getKeys(String prefix) {
		return( this.configuration.getKeys() );
	}

	@Override
	@SuppressWarnings({ "rawtypes" })
	public List getList(String key) {
		return( this.configuration.getList( this.resolveKey( key ) ) );
	}

	@Override
	@SuppressWarnings({ "rawtypes" })
	public List getList(String key, List defaultValue) {
		return( this.configuration.getList( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public long getLong(String key) {
		return( this.configuration.getLong( this.resolveKey( key ) ) );
	}

	@Override
	public long getLong(String key, long defaultValue) {
		return( this.configuration.getLong( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public Long getLong(String key, Long defaultValue) {
		return( this.configuration.getLong( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public Properties getProperties(String key) {
		return( this.configuration.getProperties( this.resolveKey( key ) ) );
	}

	@Override
	public Object getProperty(String key) {
		return( this.configuration.getProperty( this.resolveKey( key ) ) );
	}

	@Override
	public short getShort(String key) {
		return( this.configuration.getShort( this.resolveKey( key ) ) );
	}

	@Override
	public short getShort(String key, short defaultValue) {
		return( this.configuration.getShort( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public Short getShort(String key, Short defaultValue) {
		return( this.configuration.getShort( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public String getString(String key) {
		return( this.configuration.getString( this.resolveKey( key ) ) );
	}

	@Override
	public String getString(String key, String defaultValue) {
		return( this.configuration.getString( this.resolveKey( key ), defaultValue ) );
	}

	@Override
	public String[] getStringArray(String key) {
		return( this.configuration.getStringArray( this.resolveKey( key ) ) );
	}

	@Override
	public boolean isEmpty() {
		return( this.configuration.isEmpty() );
	}

	@Override
	public void setProperty(String key, Object value) {
		this.configuration.setProperty( this.resolveKey( key ), value );
	}

	@Override
	public Configuration subset(String prefix) {
		return( this.configuration.subset( prefix ) );
	}
	
	private String resolveKey(String key) {		
		if( this.configuration.containsKey( ENVIRONMENT + "." + key ) ) {
			return( ENVIRONMENT + "." + key );
		}
		return( key );
	}
}
