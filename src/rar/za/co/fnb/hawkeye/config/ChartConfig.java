package za.co.fnb.hawkeye.config;

import za.co.fnb.pe.framework.config.CoreConfigConstants;
import za.co.fnb.pe.framework.config.CoreConfigUtil;

public enum ChartConfig {
	MDPI("mdpi"), HDPI("hdpi"), XHDPI("xhdpi"), XXHDPI("xxhdpi"), XXXHDPI("xxxhdpi"), IPHONE_2X("@2x"), IPHONE_3X("@3x");

	private String resolution;
	private int amountOfXLabelsToSkip;
	private int amountOfYLabelsToSkip;

	private ChartConfig(String resolution) {
		this.resolution = resolution;
		this.amountOfXLabelsToSkip = CoreConfigUtil.getInt(CoreConfigConstants.CONFIG_DEVICE_CHART_X_LABEL_SKIP_AMOUNT + this.name(), 0);
		this.amountOfYLabelsToSkip = CoreConfigUtil.getInt(CoreConfigConstants.CONFIG_DEVICE_CHART_Y_LABEL_SKIP_AMOUNT + this.name(), 0);
	}

	public static ChartConfig getChartConfig(String height) {
		for (ChartConfig config : ChartConfig.values()) {
			if (config.getResolution().equalsIgnoreCase(height))
				return config;
		}
		return MDPI;
	}

	public String getResolution() {
		return this.resolution;
	}

	/**
	 * Get's the value from the properties file {@link chart.properties}<br/>
	 * <strong>NB:</strong> This will round to the highest even number closest to the selected
	 * uneven number.<br/>
	 * If the selected number is already an even number, no rounding will be done
	 *
	 * @return the amount of xlabels to skip
	 */
	public int getAmountOfXLabelsToSkip() {
		return (int) (Math.round((this.amountOfXLabelsToSkip + 0.1) / 2) * 2);
	}

	/**
	 * Get's the value from the properties file {@link chart.properties}<br/>
	 * <strong>NB:</strong> This will round to the highest even number closest to the selected
	 * uneven number.<br/>
	 * If the selected number is already an even number, no rounding will be done
	 *
	 * @return the amount of ylabels to skip
	 */
	public int getAmountOfYLabelsToSkip() {
		return (int) (Math.round((this.amountOfYLabelsToSkip + 0.1) / 2) * 2);
	}
}
