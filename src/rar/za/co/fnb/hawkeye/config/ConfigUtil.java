package za.co.fnb.hawkeye.config;

import org.apache.log4j.Logger;

public class ConfigUtil {
	private static final Logger LOG = Logger.getLogger( ConfigUtil.class );
	
	public static final boolean is(String constant) {
		try {
			return( ConfigFactory.getConfiguration().getBoolean( constant ) );
		}
		catch(Exception e) {
			LOG.error( "Failed to derive settings!", e );
		}
		return( false );
	}
}
