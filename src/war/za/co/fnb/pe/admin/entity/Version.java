package za.co.fnb.pe.admin.entity;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import za.co.fnb.pe.framework.entity.Entity;

@SuppressWarnings("serial")
public class Version extends Entity {
	private static final Logger LOG = Logger.getLogger( Version.class );
	
	private int major = 0;
	private int minor = 1;
	private static int build = -1;
	
	public int getMajor() {
		return major;
	}

	public void setMajor(int major) {
		this.major = major;
	}

	public int getMinor() {
		return minor;
	}

	public void setMinor(int minor) {
		this.minor = minor;
	}

	public int getBuild() {
		if( build == -1 ) {
			this.loadBuildNumber();
		}
		return( build );
	}
	
	private void loadBuildNumber() {
		LOG.debug( "Loading build number..." );
		try {			
	        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream( "build.version" );
	        Properties properties = new Properties();
	        properties.load(inputStream);
	        String propValue = properties.getProperty( "build.number" );
	        build = Integer.parseInt( propValue );
	        LOG.debug( "Build number loaded[" + build + "]" );
		}
		catch(Exception e) {
			LOG.error( "Failed to load build number!", e );
			build = 0;
		}
	}
}
