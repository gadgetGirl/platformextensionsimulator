package za.co.fnb.pe.admin.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import za.co.fnb.pe.admin.entity.Version;

public class Base {
	private static final Logger LOG = Logger.getLogger( Base.class );
	
	@GET
	@Path("version")
	@Produces(MediaType.APPLICATION_JSON)
	public Response version() {
	    LOG.debug( "GET version" );
		try {			
			return( Response.ok( new Version() ).build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to delete application!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}
}
