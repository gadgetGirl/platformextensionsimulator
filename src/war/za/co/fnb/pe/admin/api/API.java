package za.co.fnb.pe.admin.api;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import za.co.fnb.pe.admin.config.save.ChannelConfig;
import za.co.fnb.pe.admin.config.save.CustomerConfig;
import za.co.fnb.pe.admin.config.save.DeviceConfig;
import za.co.fnb.pe.admin.config.save.ExtensionConfig;
import za.co.fnb.pe.admin.config.save.FormData;
import za.co.fnb.pe.admin.config.save.SimConfig;
import za.co.fnb.pe.admin.config.save.TimeoutConfig;
import za.co.fnb.pe.framework.config.ActionListener;
import za.co.fnb.pe.framework.config.CountryUtil;
import za.co.fnb.pe.framework.config.IPUtil;
import za.co.fnb.pe.framework.config.SimulatorConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/api")
public class API extends Base {
	private static final Logger LOG = Logger.getLogger( API.class );

	@GET
	@Path("start")
	public Response start() {
	    LOG.info( "GET start" );
		try {			
			for( ActionListener listener : SimulatorConfig.getInstance().getListeners() ) {
				listener.start();
			}
			SimulatorConfig.getInstance().getConfig().setStarted( true );
			return( Response.ok().build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to start simulators!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}

	@GET
	@Path("stop")
	public Response stop() {
	    LOG.info( "GET stop" );
		try {			
			for( ActionListener listener : SimulatorConfig.getInstance().getListeners() ) {
				listener.stop();
			}
			SimulatorConfig.getInstance().getConfig().setStarted( false );
			return( Response.ok().build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to stop simulators!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}
	
	@GET
	@Path("config")
	@Produces(MediaType.APPLICATION_JSON)
	public Response config() {
	    LOG.debug( "GET config" );
		try {			
			Gson gson = new GsonBuilder().create();
		    String json = gson.toJson( SimulatorConfig.getInstance().getConfig() );
			return( Response.ok( json ).build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to get config!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}
	
	@POST
	@Path("config/simulator")
	public Response saveSimulatorConfig(String configJSON) {
	    LOG.debug( "POST simulator config" );
		try {			
			LOG.info( "config: " + configJSON );
			SimConfig config = new Gson().fromJson( configJSON, SimConfig.class );
			IPUtil.selectIP( SimulatorConfig.getInstance().getConfig().getGomezIP(), config.getGomezIP() );
			IPUtil.selectIP( SimulatorConfig.getInstance().getConfig().getFesterIP(), config.getFesterIP() );
			SimulatorConfig.getInstance().storeSavedCache();
			return( Response.ok().build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to save config!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}	

	@POST
	@Path("config/extension")
	public Response saveExtensionConfig(String configJSON) {
	    LOG.debug( "POST extension config" );
		try {			
			LOG.info( "config: " + configJSON );
			ExtensionConfig config = new Gson().fromJson( configJSON, ExtensionConfig.class );
			SimulatorConfig.getInstance().getConfig().setExtensionDescription( config.getExtensionDescription() );			
			SimulatorConfig.getInstance().getConfig().setExtensionStartUIID( config.getExtensionStartUIID() );			
			SimulatorConfig.getInstance().getConfig().setExtensionUIRouting( config.getExtensionUIRouting() );			
			SimulatorConfig.getInstance().getConfig().setExtensionModelRouting( config.getExtensionModelRouting() );			
			SimulatorConfig.getInstance().getConfig().setExtensionResourceRouting( config.getExtensionResourceRouting() );			
			SimulatorConfig.getInstance().getConfig().setExtensionURL( config.getExtensionURL() );			
			SimulatorConfig.getInstance().storeSavedCache();
			return( Response.ok().build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to save config!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}	

	@POST
	@Path("config/customer")
	public Response saveCustomerConfig(String configJSON) {
	    LOG.debug( "POST customer config" );
		try {			
			LOG.info( "config: " + configJSON );
			CustomerConfig config = new Gson().fromJson( configJSON, CustomerConfig.class );
			SimulatorConfig.getInstance().getConfig().setAuthenticated( config.getAuthenticated() );
			CountryUtil.selectCountry( SimulatorConfig.getInstance().getConfig().getCountryCode() , config.getCountryCode() );
			SimulatorConfig.getInstance().getConfig().setUcn( config.getUcn() );			
			SimulatorConfig.getInstance().storeSavedCache();
			return( Response.ok().build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to save config!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}	

	@POST
	@Path("config/device")
	public Response saveDeviceConfig(String configJSON) {
	    LOG.debug( "POST device config" );
		try {			
			LOG.info( "config: " + configJSON );
			DeviceConfig config = new Gson().fromJson( configJSON, DeviceConfig.class );
			SimulatorConfig.getInstance().getConfig().setVoipEnabled( config.getVoipEnabled() );			
			SimulatorConfig.getInstance().storeSavedCache();
			return( Response.ok().build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to save config!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}	

	@POST
	@Path("config/channel")
	public Response saveChannelConfig(String configJSON) {
	    LOG.debug( "POST channel config" );
		try {			
			LOG.info( "config: " + configJSON );
			ChannelConfig config = new Gson().fromJson( configJSON, ChannelConfig.class );
			SimulatorConfig.getInstance().getConfig().setVodsStandIn( config.getVodsStandIn() );			
			SimulatorConfig.getInstance().storeSavedCache();
			return( Response.ok().build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to save config!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}	
	
	@POST
	@Path("data/form")
	public Response saveFormData(String configJSON) {
	    LOG.debug( "POST form data" );
		try {			
			LOG.info( "config: " + configJSON );
			FormData data = new Gson().fromJson( configJSON, FormData.class );
			SimulatorConfig.getInstance().getConfig().setKey1(data.getKey1());	
			SimulatorConfig.getInstance().getConfig().setValue1(data.getValue1());
			SimulatorConfig.getInstance().storeSavedCache();
			return( Response.ok().build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to save config!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}
	
	@POST
	@Path("config/timeout")
	public Response saveTimeoutConfig(String configJSON) {
	    LOG.debug( "POST timeout config" );
		try {			
			LOG.info( "config: " + configJSON );
			TimeoutConfig config = new Gson().fromJson( configJSON, TimeoutConfig.class );
			SimulatorConfig.getInstance().getConfig().setTimeout(Integer.parseInt(config.getTimeout()));
			SimulatorConfig.getInstance().storeSavedCache();
			return( Response.ok().build() );		
		}
		catch(Throwable e) {
			LOG.debug( "Failed to save timeout config!", e );
			return( Response.status( Response.Status.NOT_FOUND ).build() );
		}
	}	

}
