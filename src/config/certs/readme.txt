
Generate CA and Private key:

  openssl req -new -x509 -extensions v3_ca -keyout rootca.key -out rootca.crt -days 3653 -config openssl.cnf

Generate Public key from Private:

  openssl rsa -pubout -in fnb.key -out fnb_public.key

Export CA and Private key to p12 keystore:

  openssl pkcs12 -export -name fnb -in fnb-ca.crt -inkey fnb_private.key -out keystore.p12

Export p12 to JKS:

  keytool -importkeystore -destkeystore mykeystore.jks -srckeystore keystore.p12 -srcstoretype pkcs12 -alias fnb

List:

  keytool -list -v -keystore keystore.jks